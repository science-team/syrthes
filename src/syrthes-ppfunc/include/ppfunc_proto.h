int rep_ch(char*,char*);
void extr_motcle_(char*,char*,int*,int*);
void extr_motcle(char*,char*,int*,int*);

void ecrire_include();
void ecrit_function(FILE*,char*,char*);

void lire_ecrire_cini();
void lire_ecrire_prophy();
void lire_ecrire_chmt();
void lire_ecrire_condvol();
void lire_ecrire_rescon();
void lire_ecrire_rescon_hmt();

void cini_xx(char*,char*);
void clim_xx(int,int,int,char*,char*,char*);
void clim_hmt_hhh();
void condvol_xx(char*,char*,char*);
void rescon();
void rescon_hmt();

void ecrire_vide_condvol();
void ecrire_vide_clim();
void ecrire_vide_cphy();
void ecrire_vide_transfo_perio();
void ecrire_vide_rescon();
void ecrire_vide_rescon_hmt();
void ecrire_vide_ray();
void ecrire_vide_solaire();
void ecrire_vide_propincidence();

int quel_model();
