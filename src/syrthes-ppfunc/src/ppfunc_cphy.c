/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
#include "ppfunc_const.h"

#define NBFORM 100

extern FILE *fdata,*ff;
char motcle[CHLONG];
char ch[CHLONG];
char chs[CHLONG];

int list_ilist[NBFORM][100];   /* 100 listes de 100 entiers */

int  list_formule_type[NBFORM]; /* type des 100 formules (iso, ortho ou aniso) */

char list_formule_rho[NBFORM][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule_cp[NBFORM][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */

char list_formule_kiso[NBFORM][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule_kortho0[NBFORM][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule_kortho1[NBFORM][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule_kortho2[NBFORM][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule_kaniso0[NBFORM][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule_kaniso1[NBFORM][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule_kaniso2[NBFORM][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule_kaniso3[NBFORM][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule_kaniso4[NBFORM][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule_kaniso5[NBFORM][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */

char list_condi[NBFORM][CHLONG];    /* 100 listes de chaines de CHLONG caracteres */

/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Interpretation des proprietes physiques                              |
  |======================================================================| */
void lire_ecrire_prophy()
{
  int i,j,nbcoef,ndim;
  int i1,i2,i3,i4,id,ifin,ifin2,ok=1,ii,nb,nr,n,pos;
  double val;
  char *suite,*egal,*chfin="\\";
  int nbliste,mc_ok,prem;
  int numlist;



  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"void user_cphyso_fct(struct Maillage maillnodes,\n");
  fprintf(ff,"                     double *tmps,struct Prophy physol,double tempss)\n");
  fprintf(ff,"/*******************************************************************/ \n");
  fprintf(ff,"{\n  int i,j,ne,nr;\n");
  fprintf(ff,"  double x,y,z,T,tt;\n\n");
  fprintf(ff,"  tt=tempss;\n\n");



  /* lecture de toutes les conditions physiques dans le fichier */
  /* ========================================================== */
  numlist=0;
  for (i=0;i<NBFORM;i++) list_formule_type[i]=0;


  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  /* traitement des suites de lignes */
	lignesuite2 : suite=strchr(ch,chfin[0]);
	  if (suite)
	    {
	      strncpy(suite," \0",2); /* on remplace l'\ de la chaine ch par un blanc */
	      fgets(chs,CHLONG,fdata);
	      strcat(ch,chs);
	      goto lignesuite2;
	    }
	  
	  
	  extr_motcle_(motcle,ch,&i1,&i2);
	  id=i2+1;
	  mc_ok=0;

	  if (!strcmp(motcle,"CPHY_MAT_ISO_FCT")) 
	    {
	      mc_ok=1;  /* c'est un mot-cle a traiter */
	      list_formule_type[numlist]=1;
	      ifin=rep_ch(list_formule_rho[numlist],ch+id);         
	      ifin2=rep_ch(list_formule_cp[numlist],ch+id+ifin);   ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kiso[numlist],ch+id+ifin); ifin+=ifin2;
	    }
	  else if (!strcmp(motcle,"CPHY_MAT_ORTHO_2D_FCT")) 
	    {
	      mc_ok=1;  /* c'est un mot-cle a traiter */
	      list_formule_type[numlist]=2;
	      ifin=rep_ch(list_formule_rho[numlist],ch+id);         
	      ifin2=rep_ch(list_formule_cp[numlist],ch+id+ifin);      ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kortho0[numlist],ch+id+ifin); ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kortho1[numlist],ch+id+ifin); ifin+=ifin2;
	    }

	  else if (!strcmp(motcle,"CPHY_MAT_ORTHO_3D_FCT")) 
	    {
	      mc_ok=1;  /* c'est un mot-cle a traiter */
	      list_formule_type[numlist]=3;
	      ifin=rep_ch(list_formule_rho[numlist],ch+id);         
	      ifin2=rep_ch(list_formule_cp[numlist],ch+id+ifin);      ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kortho0[numlist],ch+id+ifin); ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kortho1[numlist],ch+id+ifin); ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kortho2[numlist],ch+id+ifin); ifin+=ifin2;
	    }

	  else if (!strcmp(motcle,"CPHY_MAT_ANISO_2D_FCT")) 
	    {
	      mc_ok=1;  /* c'est un mot-cle a traiter */
	      list_formule_type[numlist]=4;
	      ifin=rep_ch(list_formule_rho[numlist],ch+id);         
	      ifin2=rep_ch(list_formule_cp[numlist],ch+id+ifin);      ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kaniso0[numlist],ch+id+ifin); ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kaniso1[numlist],ch+id+ifin); ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kaniso2[numlist],ch+id+ifin); ifin+=ifin2;
	    }
	  else if (!strcmp(motcle,"CPHY_MAT_ANISO_3D_FCT")) 
	    {
	      mc_ok=1;  /* c'est un mot-cle a traiter */
	      list_formule_type[numlist]=5;
	      ifin=rep_ch(list_formule_rho[numlist],ch+id);         
	      ifin2=rep_ch(list_formule_cp[numlist],ch+id+ifin);      ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kaniso0[numlist],ch+id+ifin); ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kaniso1[numlist],ch+id+ifin); ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kaniso2[numlist],ch+id+ifin); ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kaniso3[numlist],ch+id+ifin); ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kaniso4[numlist],ch+id+ifin); ifin+=ifin2;
	      ifin2=rep_ch(list_formule_kaniso5[numlist],ch+id+ifin); ifin+=ifin2;
	    }


	  if (mc_ok)
	    {
	      rep_listint(list_ilist[numlist],&nb,ch+id+ifin);
	      
	      if (list_ilist[numlist][0]==-1)
		{
		  sprintf(list_condi[numlist],"1==1");
		}
	      else
		for (pos=n=0;n<nb;n++) 
		  {
		    sprintf(list_condi[numlist]+pos,"nr==%2d",list_ilist[numlist][n]); pos+=6;
		    if (n!=nb-1) {sprintf(list_condi[numlist]+pos," || ");pos+=4;}
		    else sprintf(list_condi[numlist]+pos,"\0");
		  }
	      numlist++;
	    }
	}
    }
  

  /* ecriture de toutes les conditions physiques dans le fichier */
  /* =========================================================== */

  /* ecriture des fonctions sur rho */
  /* ------------------------------ */
  if (numlist>0)
    {
      fprintf(ff,"  for (i=0;i<physol.nelem;i++) \n");
      fprintf(ff,"      { \n");
      fprintf(ff,"        data_element_moy(i,maillnodes,tmps,&nr,&x,&y,&z,&T);\n");

      for (j=0;j<numlist;j++)
	{
	  if (list_ilist[j][0]==-1) 
	    fprintf(ff,"        physol.rho[i]=%s; \n",list_formule_rho[j]);
	  else
	    {
	      if (j==0) fprintf(ff,"        if (%s) {\n",list_condi[j]);
	      else fprintf(ff,"        else if (%s) {\n",list_condi[j]);
	      fprintf(ff,"           physol.rho[i]=%s; \n",list_formule_rho[j]);
	      fprintf(ff,"        } \n");
	    }
	}

      fprintf(ff,"      } \n\n");
    }


  /* ecriture des fonctions sur CP */
  /* ----------------------------- */

  if (numlist>0)
    {
      fprintf(ff,"  for (i=0;i<physol.nelem;i++) \n");
      fprintf(ff,"      { \n");
      fprintf(ff,"        data_element_moy(i,maillnodes,tmps,&nr,&x,&y,&z,&T);\n");

      for (j=0;j<numlist;j++)
	{
	  if (list_ilist[j][0]==-1) 
	    fprintf(ff,"        physol.cp[i]=%s; \n",list_formule_cp[j]);
	  else
	    {
	      if (j==0) fprintf(ff,"        if (%s) {\n",list_condi[j]);
	      else fprintf(ff,"        else if (%s) {\n",list_condi[j]);
	      fprintf(ff,"           physol.cp[i]=%s; \n",list_formule_cp[j]);
	      fprintf(ff,"        } \n");
	    }
	}
  
      fprintf(ff,"      } \n\n");
    }

  /* ecriture des fonctions sur  K isotro */
  /* ------------------------------------ */

  /* presence de conditions isotrope ? */
  for (ok=0,i=0;i<NBFORM;i++) if (list_formule_type[i]==1) ok++;

  if (ok)
    {
      fprintf(ff,"  for (i=0;i<physol.kiso.nelem;i++) \n");
      fprintf(ff,"      { \n");
      fprintf(ff,"        data_element_moy(physol.kiso.ele[i],maillnodes,tmps,&nr,&x,&y,&z,&T);\n");
      
      for (prem=1,j=0;j<numlist;j++)
	{
	  if (list_formule_type[j]==1)
	    {
	      if (list_ilist[j][0]==-1) 
		fprintf(ff,"        physol.kiso.k[i]=%s; \n",list_formule_kiso[j]);
	      else
		{
		  if (prem) fprintf(ff,"        if (%s) {\n",list_condi[j]);
		  else fprintf(ff,"        else if (%s) {\n",list_condi[j]);
		  prem=0;
		  fprintf(ff,"           physol.kiso.k[i]=%s; \n",list_formule_kiso[j]);
		  fprintf(ff,"        } \n");
		}
	    }
	}
      
      fprintf(ff,"      } \n\n");
    }

  /* ecriture des fonctions sur  K ortho */
  /* ----------------------------------- */

  /* presence de conditions isotrope ? */
  for (ok=0,i=0;i<NBFORM;i++) if (list_formule_type[i]==2 || list_formule_type[i]==3) ok=1;

  if (ok)
    {
      fprintf(ff,"  for (i=0;i<physol.kortho.nelem;i++) \n");
      fprintf(ff,"      { \n");
      fprintf(ff,"        data_element_moy(physol.kortho.ele[i],maillnodes,tmps,&nr,&x,&y,&z,&T);\n");

      for (prem=1,j=0;j<numlist;j++)
	{
	  if (list_formule_type[j]==2 || list_formule_type[j]==3)
	    {
	      if (list_ilist[j][0]==-1) {
		fprintf(ff,"           physol.kortho.k11[i]=%s; \n",list_formule_kortho0[j]);
		fprintf(ff,"           physol.kortho.k22[i]=%s; \n",list_formule_kortho1[j]);
		if (list_formule_type[j]==3)
		  fprintf(ff,"           physol.kortho.k33[i]=%s; \n",list_formule_kortho2[j]);
	      }
	      else
		{
		  if (prem) fprintf(ff,"        if (%s) {\n",list_condi[j]);
		  else fprintf(ff,"        else if (%s) {\n",list_condi[j]);
		  prem=0;
		  fprintf(ff,"           physol.kortho.k11[i]=%s; \n",list_formule_kortho0[j]);
		  fprintf(ff,"           physol.kortho.k22[i]=%s; \n",list_formule_kortho1[j]);
		  if (list_formule_type[j]==3)
		    fprintf(ff,"           physol.kortho.k33[i]=%s; \n",list_formule_kortho2[j]);
		  fprintf(ff,"        } \n");
		}
	    }
	}
      
      fprintf(ff,"      } \n\n");
    }


  /* ecriture des fonctions sur  K aniso */
  /* ----------------------------------- */

  /* presence de conditions isotrope ? */
  for (ok=0,i=0;i<NBFORM;i++) if (list_formule_type[i]==4 || list_formule_type[i]==5) ok=1;

  if (ok)
    {
      fprintf(ff,"  for (i=0;i<physol.kaniso.nelem;i++) \n");
      fprintf(ff,"      { \n");
      fprintf(ff,"        data_element_moy(physol.kaniso.ele[i],maillnodes,tmps,&nr,&x,&y,&z,&T);\n");

      for (prem=1,j=0;j<numlist;j++)
	{
	  if (list_formule_type[j]==4 || list_formule_type[j]==5)
	    {
	      if (list_ilist[j][0]==-1) {
		fprintf(ff,"              physol.kaniso.k11[i]=%s; \n",list_formule_kaniso0[j]);
		fprintf(ff,"              physol.kaniso.k22[i]=%s; \n",list_formule_kaniso1[j]);
		if (list_formule_type[j]==5)
		  fprintf(ff,"              physol.kaniso.k12[i]=%s; \n",list_formule_kaniso2[j]);
	      }
	      else
		{
		  if (prem) fprintf(ff,"        if (%s) {\n",list_condi[j]);
		  else fprintf(ff,"        else if (%s) {\n",list_condi[j]);
		  prem=0;
		  fprintf(ff,"           physol.kaniso.k11[i]=%s; \n",list_formule_kaniso0[j]);
		  fprintf(ff,"           physol.kaniso.k22[i]=%s; \n",list_formule_kaniso1[j]);
		  if (list_formule_type[j]==4)
		    fprintf(ff,"           physol.kaniso.k12[i]=%s; \n",list_formule_kaniso2[j]);
		  else{
		    fprintf(ff,"           physol.kaniso.k33[i]=%s; \n",list_formule_kaniso2[j]);
		    fprintf(ff,"           physol.kaniso.k12[i]=%s; \n",list_formule_kaniso3[j]);
		    fprintf(ff,"           physol.kaniso.k13[i]=%s; \n",list_formule_kaniso4[j]);
		    fprintf(ff,"           physol.kaniso.k23[i]=%s; \n",list_formule_kaniso5[j]);
		  }
		  fprintf(ff,"        } \n");
		}
	    }
	}
      
      fprintf(ff,"      } \n\n");
    }

  fprintf(ff,"}\n"); /* fin du programme */

}

