/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
#include "ppfunc_proto.h"
#include "ppfunc_const.h"


extern FILE *fdata,*ff;
extern int model;

char motcle[CHLONG];
char ch[CHLONG];
char chs[CHLONG];

int list_ilist[100][100];   /* 100 listes de 100 entiers */
char list_formule[100][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule1[100][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_formule2[100][CHLONG]; /* 100 listes de chaines de CHLONG caracteres */
char list_condi[100][CHLONG];   /* 100 listes de chaines de CHLONG caracteres */

/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Interpretation des conditions de flux volumiques                     |
  |======================================================================| */

void lire_ecrire_condvol()
{
  int i,j,nbcoef,ndim;
  int i1,i2,i3,i4,id,ifin,ifin2,ok=1,ii,nb,nr,n,pos;
  double val;
  char *suite,*chfin="\\";
  int nbliste,numlist;


  if (model==0) /* pas de hmt */
    {
      fprintf(ff,"/*******************************************************************/ \n");
      fprintf(ff,"void user_cfluvs_fct(struct Maillage maillnodes,\n");
      fprintf(ff,"                     double *t,struct Cvol fluxvol_t,double tempss)\n");
      fprintf(ff,"/*******************************************************************/ \n");
      fprintf(ff,"{\n  int i,j,ne,nr;\n");
      fprintf(ff,"  double x,y,z,T,tt;\n\n");
      fprintf(ff,"  tt=tempss;\n\n");

      /* lecture de tous les flux volumiques sur T dans le fichier */
      condvol_xx("CVOL_T_FCT","fluxvol_t","t");
    }
  else
    {
      fprintf(ff,"/************************************************************************/ \n");
      fprintf(ff,"void user_hmt_cfluvs_fct(struct Maillage maillnodes,\n");
      fprintf(ff,"			 double *t, struct Cvol fluxvol_t ,\n");
      fprintf(ff,"			 double *pv,struct Cvol fluxvol_pv,\n");
      fprintf(ff,"			 double *pt,struct Cvol fluxvol_pt,double tempss)\n");
      fprintf(ff,"/************************************************************************/ \n");
      fprintf(ff,"{\n  int i,j,ne,nr;\n");
      fprintf(ff,"  double x,y,z,T,PV,PT,tt;\n\n");
      fprintf(ff,"  tt=tempss;\n\n");

      /* lecture de tous les flux volumiques sur T dans le fichier */

      condvol_xx("CVOL_T_FCT","fluxvol_t","t");
      condvol_xx("CVOL_PV_FCT","fluxvol_pv","pv");
      condvol_xx("CVOL_PT_FCT","fluxvol_pt","pt");
    }

  fprintf(ff,"}\n");

}

/*|======================================================================|
  | SYRTHES 4.3/PPFONC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture/Ecriture des conditions de flux volumiques                   |
  |======================================================================| */
 void condvol_xx(char *motclevol,char *nomtab,char *nomvar)
{
  int i,j,nbcoef,ndim;
  int i1,i2,i3,i4,id,ifin,ifin2,ok=1,ii,nb,nr,n,pos;
  double val;
  char *suite,*chfin="\\";
  int nbliste,numlist;


  /* lecture de flux volumiques */
  /* -------------------------- */

  numlist=0;  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  /* traitement des suites de lignes */
	lignesuite2 : suite=strchr(ch,chfin[0]);
	  if (suite)
	    {
	      strncpy(suite," \0",2); /* on remplace l'\ de la chaine ch par un blanc */
	      fgets(chs,CHLONG,fdata);
	      strcat(ch,chs);
	      goto lignesuite2;
	    }
	  
	  
	  extr_motcle_(motcle,ch,&i1,&i2);
	  id=i2+1;
	  if (!strcmp(motcle,motclevol)) 
	    {
	      ifin=rep_ch(list_formule1[numlist],ch+id);
	      ifin2=rep_ch(list_formule2[numlist],ch+id+ifin); ifin+=ifin2;
	      rep_listint(list_ilist[numlist],&nb,ch+id+ifin);

	      if (list_ilist[numlist][0]!=-1)
		for (pos=n=0;n<nb;n++) 
		  {
		    sprintf(list_condi[numlist]+pos,"nr==%2d",list_ilist[numlist][n]); pos+=6;
		    if (n!=nb-1) {sprintf(list_condi[numlist]+pos," || ");pos+=4;}
		    else sprintf(list_condi[numlist]+pos,"\0");
		  }
	      numlist++;
	    }
	}
    }
  

  /* ecriture des flux volumiques */
  /* ---------------------------- */
  if (numlist==1 && list_ilist[0][0]==-1)
    {
      fprintf(ff,"  for (i=0;i<%s.nelem;i++) \n",nomtab);
      fprintf(ff,"    { \n");
      fprintf(ff,"      data_element_moy(%s.nume[i],maillnodes,%s,&nr,&x,&y,&z,&T);\n",nomtab,nomvar);
      fprintf(ff,"      %s.val1[i]=%s; \n",nomtab,list_formule[0]);
      fprintf(ff,"    } \n\n");
    }
  
  else if (numlist>=1)
    {
      fprintf(ff,"  for (i=0;i<%s.nelem;i++) \n",nomtab);
      fprintf(ff,"    { \n");
      fprintf(ff,"     data_element_moy(%s.nume[i],maillnodes,%s,&nr,&x,&y,&z,&T);\n",nomtab,nomvar);
      fprintf(ff,"     if (%s) {\n",list_condi[0]);
      fprintf(ff,"        %s.val1[i]=%s; \n",nomtab,list_formule1[0]);
      fprintf(ff,"        %s.val2[i]=%s; \n",nomtab,list_formule2[0]);
      fprintf(ff,"     } \n");
      for (j=1;j<numlist;j++)
	{
	  fprintf(ff,"     else if (%s) {\n",list_condi[j]);
	  fprintf(ff,"         %s.val1[i]=%s; \n",nomtab,list_formule1[j]);
	  fprintf(ff,"         %s.val2[i]=%s; \n",nomtab,list_formule2[j]);
	  fprintf(ff,"     } \n");
	}
      fprintf(ff,"    } \n\n");
    }


}
