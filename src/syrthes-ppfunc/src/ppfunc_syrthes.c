/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppfunc_usertype.h"
#include "ppfunc_proto.h"
#include "ppfunc_const.h"

FILE  *fdata,*ff;

int model;

char motcle[CHLONG];
char ch[CHLONG];
char chs[CHLONG];


/*|======================================================================|
  | SYRTHES 4.3/PPFUNC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Pre-processeur de SYRTHES pour le traitement des fonctions           |
  | interpretees dans le fichier de donnees utilisateur (syrthes.data)   |
  | Programme principal                                                  |
  |======================================================================| */

main(argc,argv)
int     argc;
char    *argv[];
{         
  
  char *s;
 
  int i1,i2,i3,i4,id,ok=0;
  int cini,cphy,cvol,clim,climhmt,resconhmt,rescon,numarg;
  char motcle[CHLONG],ch[CHLONG],*suite,*chfin="\\";
  char nomdata[CHLONG];



  printf("    ****************************************************************\n");
  printf("    *                                                              *\n");
  printf("    *   SSSSS  YY    YY  RRRRRR  TTTTTTTT HH   HH  EEEEEE   SSSSS  *\n");
  printf("    *  SS       YY  YY   RR   RR    TT    HH   HH  EE      SS      *\n");
  printf("    *  SS        YYYY    RR   RR    TT    HH   HH  EE      SS      *\n");
  printf("    *   SSSS      YY     RRRRRR     TT    HHHHHHH  EEEE     SSSS   *\n");
  printf("    *      SS     YY     RR  RR     TT    HH   HH  EE          SS  *\n");
  printf("    *      SS     YY     RR   RR    TT    HH   HH  EE          SS  *\n");
  printf("    *  SSSSS      YY     RR   RR    TT    HH   HH  EEEEEE  SSSSS   *\n");
  printf("    *                                                              *\n");
  printf("    ****************************************************************\n");
  printf("    *                                                              *\n");
  if (SYRTHES_LANG == FR)
  printf("    *           PRE TRAITEMENT DES FONCTIONS INTERPRETEES          * \n");
  else if (SYRTHES_LANG == EN)
  printf("    *               PRE PROCESSOR FOR USER FUNCTIONS               * \n");
  printf("    *                                                              *\n");
  printf("    ****************************************************************\n\n\n");


  s=(char*)malloc(10*sizeof(char));  
  numarg=0;
  while (++numarg < argc) {
    
    s = argv[numarg];

    if (strcmp (s, "-h") == 0) 
      {
	ok=1;
	if (SYRTHES_LANG == FR)
	  {
	    printf("\n ppfunc_syrthes : pre-traitement des fonctions interpretees pour SYRTHES\n");
	    printf("                  usage   : ppfunc_syrthes -d fichier_de_donnees\n");
	    printf("                  exemple : ppfunc_syrthes -d syrthes.data\n\n");
	  }
	else if (SYRTHES_LANG == EN)
	  {
	    printf("\n ppfunc_syrthes : preliminary traitement of data file functions for SYRTHES\n");
	    printf("\n                  usage   : ppfunc_syrthes -d data_file \n");
	    printf("\n                  example : ppfunc_syrthes -d syrthes.data\n\n");
	  }
	exit(0);
      }
    else if (strcmp (s, "-d") == 0) 
      {
	ok=1;
        s = argv[++numarg];
	strncpy(nomdata,s,CHLONG) ;
	if ((fdata=fopen(nomdata,"r")) == NULL)
	  {
	    if (SYRTHES_LANG == FR)
	      printf("\n\n SYRTHES : Erreur d'ouverture du fichier %s\n",nomdata);
	    else if (SYRTHES_LANG == EN)
	      printf("\n\n SYRTHES : Error during the opening of file %s\n",nomdata);
	    exit(0);
	  }
      }
    
  }

  if (!ok){
    if (SYRTHES_LANG == FR)
      {
	printf("ppfunc_syrthes : mauvais arguments !\n");
	printf("                 pour des informations --> ppfunc_syrthes -h\n");
      }
    else if (SYRTHES_LANG == EN)
      {
	printf("ppfunc_syrthes : wrong arguments !\n");
	printf("                 for information --> ppfunc_syrthes -h\n");
      }
    exit(0);
  }

  /* quel type de calcul ?  (conduction ou HMT) */
  model=quel_model();


  cini=cphy=cvol=clim=climhmt=rescon=resconhmt=0;

  /* premier parcours du fichier pour savoir s'il faut creer un fichier source */
  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);

	  if (!strcmp(motcle,"CINI_T_FCT") ||
	      !strcmp(motcle,"CINI_PV_FCT")||
	      !strcmp(motcle,"CINI_PT_FCT"))  cini=1;

	  else if (!strcmp(motcle,"CPHY_MAT_ISO_FCT") ||
		   !strcmp(motcle,"CPHY_MAT_ORTHO_2D_FCT") ||
		   !strcmp(motcle,"CPHY_MAT_ORTHO_3D_FCT") ||
		   !strcmp(motcle,"CPHY_MAT_ANISO_2D_FCT") ||
		   !strcmp(motcle,"CPHY_MAT_ANISO_3D_FCT"))
		   cphy=1;

	  else if (!strcmp(motcle,"CLIM_T_FCT"))
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      if (!strcmp(motcle,"FLUX") ||
		  !strcmp(motcle,"DIRICHLET")||
		  !strcmp(motcle,"COEF_ECH") ||
		  !strcmp(motcle,"RAY_INFINI")) clim=1;
	      
	      else if (!strcmp(motcle,"RES_CONTACT")) rescon=1;
	      
	    }

	  else if ( !strcmp(motcle,"CVOL_T_FCT") ||
		    !strcmp(motcle,"CVOL_PV_FCT")||
		    !strcmp(motcle,"CVOL_PT_FCT") ) cvol=1;

	  else if ( !strcmp(motcle,"CLIM_HMT_FCT")) {
	    extr_motcle(motcle,ch+i2+1,&i3,&i4);
	    id=i2+1+i4+1;
	    if (!strcmp(motcle,"RES_CONTACT")) resconhmt=1;
	    else if (!strcmp(motcle,"HHH")) climhmt=1;
	  }

	  lignesuite : suite=strchr(ch,chfin[0]);
	  if (suite) {fgets(ch,CHLONG,fdata); goto lignesuite;}
	}
    }
  
  
  if (cini+cphy+cvol+clim+climhmt+rescon+resconhmt>0)
    {
  
      if ((ff=fopen("user_fct.c","w")) == NULL)
	{
	  if (SYRTHES_LANG == FR)
	    printf("Impossible d'ouvrir le fichier user_fct.c\n");
	  else if (SYRTHES_LANG == EN)
	    printf("Can't open file user_fct.c\n");
	  exit(1) ;
	}
      
      ecrire_include();
      
      
      /* conditions initiales */
      /* -------------------- */
      if (model==0 && cini) {
	if (SYRTHES_LANG == FR)
	  printf("  *** Traitement des conditions initiales\n");
	else if (SYRTHES_LANG == EN)
	  printf("  *** Initial conditions\n");
	lire_ecrire_cini();
      }
      else 
	ecrire_vide_cini();


      /* proprietes physiques */
      /* -------------------- */
      if (cphy) {
	if (SYRTHES_LANG == FR)
	  printf("  *** Traitement des proprietes physiques\n");
	else if (SYRTHES_LANG == EN)
	  printf("  *** Material properties processing\n");
	lire_ecrire_prophy();
      }
      else 
	ecrire_vide_prophy();



      /* Conditions aux limites */
      /* ---------------------- */
      if (model==0 && clim) {
	if (SYRTHES_LANG == FR)
	  printf("  *** Traitement des conditions aux limites\n");
	else if (SYRTHES_LANG == EN)
	  printf("  *** Boundary conditions processing\n");
	lire_ecrire_clim(); 
      }
      else 
	ecrire_vide_clim(); 


      /* flux volumiques en conduction */
      /* ----------------------------- */
      if (model==0 && cvol)
	{
	  if (SYRTHES_LANG == FR)
	    printf("  *** Traitement des flux volumiques\n");
	  else if (SYRTHES_LANG == EN)
	    printf("  *** Volumic flux processing\n");
	  lire_ecrire_condvol();
	}
      else 
	ecrire_vide_condvol();

   
      /* Resisances de contact */
      /* --------------------- */
      if (model==0 && rescon)
	{
	  if (SYRTHES_LANG == FR)
	    printf("  *** Traitement des resistances de contact\n");
	  else if (SYRTHES_LANG == EN)
	    printf("  *** Contact resistance processing\n");
	  lire_ecrire_rescon(); 
	}
      else 
	ecrire_vide_rescon(); 
      

      /* conditions initiales */
      /* -------------------- */
      if (model>0 && cini) {
	if (SYRTHES_LANG == FR)
	  printf("  *** Traitement des conditions initiales\n");
	else if (SYRTHES_LANG == EN)
	  printf("  *** Initial conditions\n");
	lire_ecrire_cini();
      }
      else 
	ecrire_vide_cini_hmt();

      /* Conditions aux limites HMT */
      /* -------------------------- */
      if (model>0 && climhmt) {
	if (SYRTHES_LANG == FR)
	  printf("  *** Traitement des conditions aux limites transferts couples\n");
	else if (SYRTHES_LANG == EN)
	  printf("  *** Heat and mass transfer boundary conditions processing\n");
	lire_ecrire_chmt(); 
      }
      else 
	ecrire_vide_chmt(); 

      /* flux volumiques HMT */
      /* ------------------- */
      if (model>0 && cvol)
	{
	  if (SYRTHES_LANG == FR)
	    printf("  *** Traitement des flux volumiques\n");
	  else if (SYRTHES_LANG == EN)
	    printf("  *** Volumic flux processing\n");
	  lire_ecrire_condvol();
	}
      else 
	ecrire_vide_condvol_hmt();

      
      /* Resisances de contact HMT */
      /* ------------------------- */
      if (resconhmt) {
	if (SYRTHES_LANG == FR)
	  printf("  *** Traitement des resistances de contact transferts couples\n");
	else if (SYRTHES_LANG == EN)
	  printf("  *** Heat and mass transfer contact resistance processing\n");
	lire_ecrire_rescon_hmt(); 
      }
      else 
	ecrire_vide_rescon_hmt(); 

      

      /* Pour le moment ce qui n'est pas pris en compte... */
      /* ------------------------------------------------- */
      ecrire_vide_transfo_perio();
      ecrire_vide_ray();
      ecrire_vide_solaire();
      ecrire_vide_propincidence();
      
    }
  
  else
    {
      if (SYRTHES_LANG == FR)
	printf("    ---> Il n'y a pas de fonctions interpretees dans le fichier de donnees\n");
      else if (SYRTHES_LANG == EN)
	printf("    ---> There isn't any user function in the user data file\n");
    }
  
  printf("\n\n\n");
  printf("    ****************************************************************\n");
  printf("    *                                                              *\n");
  printf("    *           SSS   Y   Y   RRR  TTTTT  H H   EEE   SSS          *\n");
  printf("    *           S      Y Y    R R    T    H H   E     S            *\n");
  printf("    *           SSS     Y     RRR    T    HHH   EEE   SSS          *\n");
  printf("    *             S     Y     RR     T    H H   E       S          *\n");
  printf("    *           SSS     Y     R R    T    H H   EEE   SSS          *\n");
  printf("    *                                                              *\n");
  printf("    ****************************************************************\n");
  printf("    *                                                              *\n");
  if (SYRTHES_LANG == FR)
  printf("    *FIN NORMALE DU PRE-PROCESSING POUR LES FONCTIONS INTERPRETEES *\n");
  else if (SYRTHES_LANG == EN)
  printf("    *     PRE PROCESSOR FOR USER FUNCTIONS : NORMAL END OF TASK    * \n");
  printf("    *                                                              *\n");
  printf("    ****************************************************************\n\n\n");

}  

/*|======================================================================|
  | SYRTHES 4.3/PPFUNC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |======================================================================| */
void ecrire_include()
{
  fprintf(ff,"# include <stdio.h>\n");
  fprintf(ff,"# include <stdlib.h>\n");
  fprintf(ff,"# include <math.h>\n");
  fprintf(ff,"#include \"syr_tree.h\"\n");
  fprintf(ff,"#include \"syr_bd.h\"\n"); 
  fprintf(ff,"#include \"syr_abs.h\"\n");
  fprintf(ff,"#include \"syr_option.h\"\n"); 
  fprintf(ff,"#include \"syr_const.h\"\n");
  fprintf(ff,"#include \"syr_proto.h\"\n"); 
  fprintf(ff,"#include \"syr_hmt_bd.h\"\n"); 
  fprintf(ff,"#include \"syr_hmt_proto.h\"\n"); 
  fprintf(ff,"\n\n");
}

/*|======================================================================|
  | SYRTHES 4.3/PPFUNC         2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | detection du type de calcul : conduction ou HMT ?                    |
  |======================================================================| */
int quel_model()
{
  int model,i1,i2,id;
  char *suite,*chfin="\\";

  model=0;
 
  fseek(fdata,0,SEEK_SET);
  
  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  /* traitement des suites de lignes */
	lignesuite2 : suite=strchr(ch,chfin[0]);
	  if (suite)
	    {
	      strncpy(suite," \0",2); /* on remplace l'\ de la chaine ch par un blanc */
	      fgets(chs,CHLONG,fdata);
	      strcat(ch,chs);
	      goto lignesuite2;
	    }
	  extr_motcle_(motcle,ch,&i1,&i2);
	  id=i2+1;
	  if (!strcmp(motcle,"MODELISATION DES TRANSFERTS D HUMIDITE")) 
	    model=rep_int(ch+i2+1);
	}
    }
  
  return model;
}
