/*-----------------------------------------------------------------------

SYRTHES version 4.3
-------------------

This file is part of the SYRTHES Kernel, element of the
thermal code SYRTHES.

Copyright (C) 1988-2008 EDF S.A., France

contact: syrthes-support@edf.fr


The SYRTHES Kernel is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

The SYRTHES Kernel is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with the SYRTHES Kernel; if not, write to the
Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor,
Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "med.h"

/* max length for each read line of the file */
#define LONGUEUR_LIGNE 1001

FILE  *fsyrg,*fsyrr;
med_idt fmed;
char nom_maill[MED_NAME_SIZE+1]="Calcul SYRTHES";

void entete_syr(int *,int *,int *,int *,int *,int *,int *);
void ecrit_syrthes(int,int,int,int,int,double,double*);
void coord_syr(int,int,double*,int*);
void elem_syr(int,int,int*,int*);
void elembord_syr(int,int,int*,int*);
void entete_resu_syr(int *,double *,double *);
int  entete_champ_resu_syr(int *,int *,char *);
void resu_syr(int,double*);
void ecrit_coord_med(int,int,double*,int*);
void ecrit_elem_med(int,int,int,int*,int*,int*);
void ecrit_elembord_med(int,int,int,int*,int*,int);
void ecrit_resu_med(int,int,int,double,double*,char*,int,int,int);
med_int * convertit_int_en_med_int(int*,int,int*);
med_float * convertit_double_en_med_float(double*,int,int*);
char * convertit_nomfam_en_nommed(char *);

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  :                                                           |
  |======================================================================|
  | syrthes4_to_med23                                                    |
  |        Convert SYRTHES 4.x results files into MED 3.0 format         |
  |======================================================================| */

main (argc,argv)

     int   argc;
     char *argv[];
{
  int numarg,i,ndim,ndimelem,nelem,nelembord,nbno,nbnobord,npoin,nt,nb,type,nbfelem;
  int *node,*nrefn,*nrefe,nbenr,acreer;
  int *nodebord,*nrefnbord,*nrefebord;
  int ok,isResu,isGeo,isMed;
  double temps,dt,*coord,*var;
  char *s,ch[LONGUEUR_LIGNE];
  med_int ndim_med;
  med_err ret_med;
  char nom_champ[MED_NAME_SIZE+1];
  char nomutilmed[400],nomutilgeom[400],nomutilresu[400];


  numarg=0;
  isResu=isGeo=isMed=0;

  while (++numarg < argc)
    {

      s = argv[numarg];

      if (strcmp (s, "-h") == 0) 
        {
          printf("\n syrthes4med30 : transform results from Syrthes 4.x to MED format 3.0\n");
          printf("       Usage : syrthes4med30  -h  : help\n");
          printf("               syrthes4med30  -m mesh.syr -o mesh.med \n");
          printf("               syrthes4med30  -m mesh.syr -r resu -o resu.med \n");
          exit(0);
        }

      else if (strcmp (s, "-m") == 0){
	s = argv[++numarg];
	strcpy(nomutilgeom,argv[numarg]);
	isGeo=1;
      }
      else if (strcmp (s, "-r") == 0){
	s = argv[++numarg];
	strcpy(nomutilresu,argv[numarg]);
	isResu=1;
      }
      else if (strcmp (s, "-o") == 0){
	s = argv[++numarg];
	strcpy(nomutilmed,argv[numarg]);
	isMed=1;
      }
    }


  if ((fsyrg=fopen(nomutilgeom,"r")) == NULL)
    {
      printf("Unable to open file %s\n",nomutilgeom);	
      exit(1) ;
    }
  
  if (isResu > 0){
    if ((fsyrr=fopen(nomutilresu,"r")) == NULL)
      {
        printf("Unable to open file %s\n",nomutilresu);	
        exit(1) ;
      }  
  }
  
  if (isMed > 0){
      if ((fmed=MEDfileOpen(nomutilmed,MED_ACC_CREAT)) < 0) 
         {
          printf("Unable to open file %s\n",nomutilmed);	
          exit(1) ;
         }
  }

  ok=1;
  if (isGeo==0) {printf("  You have to give at least a geometric file\n"); ok=0;}
  if (isMed==0) {printf("  Med file name is missing\n"); ok=0;}
  if (!ok){
      printf("  --> type 'syrthes4med30 -h' for help\n");
      exit(1);
    }


  printf("\n\n");
  printf("===============================================================\n");
  printf("                         SYRTHES4MED30                       \n");
  printf("                  SYRTHES --> MED  INTERFACE                   \n");
  printf("===============================================================\n");

  printf("\n\nTransform SYRTHES results files to MED format\n");

  printf("\n\nRead Syrthes geometry (head)...\n");
  entete_syr(&ndim,&ndimelem,&npoin,&nelem,&nelembord,&nbno,&nbnobord);

  /* Coordinates */
  printf("\n\nRead Syrthes geometry (coordinates)...\n");
  coord=(double*)malloc(ndim*npoin * sizeof(double));
  nrefn=(int*)malloc(npoin * sizeof(int));
  coord_syr(ndim,npoin,coord,nrefn);
  printf("\nWriting MED coordinates... ");
  ecrit_coord_med(ndim,npoin,coord,nrefn);
  free(coord);
  free(nrefn);
  printf("ok\n");
  
  /* Elements */
  printf("\n\nRead Syrthes geometry (elements)...\n");
  node=(int*)malloc(nbno*nelem * sizeof(int));
  nrefe=(int*)malloc(nelem * sizeof(int));
  elem_syr(nelem,nbno,node,nrefe);
  printf("\nWriting MED connectivity for elements... ");
  ecrit_elem_med(ndim,nelem,nbno,node,nrefe,&nbfelem);
  free(node);
  free(nrefe);
  printf("ok\n");
  
  /* Elements of bound */
  if (nelembord > 0)
  {
    printf("\n\nRead Syrthes geometry (elements of bound)...\n");
    nodebord=(int*)malloc(nbnobord*nelembord * sizeof(int));
    nrefebord=(int*)malloc(nelembord * sizeof(int));
    elembord_syr(nelembord,nbnobord,nodebord,nrefebord);
    printf("\nWriting MED connectivity for elements of bound... ");
    ecrit_elembord_med(ndim,nelembord,nbnobord,nodebord,nrefebord,nbfelem);
    free(nodebord);
    free(nrefebord);
    printf("ok\n");
  }


  printf("\n\n Geometry conversion completed\n\n");

  if(isResu)
    {
      printf("\n===============================================================\n");
      printf("\nResults conversion...\n\n");
      var=(double*)malloc(npoin * sizeof(double));
      
      nbenr=0;
      fgets(ch,LONGUEUR_LIGNE,fsyrr);
      while (fgets(ch,LONGUEUR_LIGNE,fsyrr))
        {
           nbenr++;
           if (nbenr==1) acreer=1; else acreer=0;
           entete_resu_syr(&nt,&temps,&dt);
	   while (!entete_champ_resu_syr(&nb,&type, nom_champ))
             {
		/* type = 1 : results on elements of bound */
		/* type = 2 : results on elements */
		/* type = 3 : results on nodes */
                printf("      MED write: timestep=%d time=%f field=%s %d values ",
                         nt,temps,nom_champ,nb);
		/* allocation for values */
                var=(double*)malloc(nb * sizeof(double));
                resu_syr(nb,var);
                ecrit_resu_med(ndimelem,nb,nt,temps,var,nom_champ,nbenr,acreer,type);
		free(var);
             }
        }
     printf("\n Results conversion completed\n\n");
    }

  printf("\n\n");
  printf("===============================================================\n");
  printf("                       SYRTHES4_TO_MED30                       \n");
  printf("                        End of process                         \n");
  printf("===============================================================\n");
  printf("\n");

  fclose(fsyrg);
  if (isResu) fclose(fsyrr);
  ret_med=MEDfileClose(fmed);

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  :                                                           |
  |======================================================================|
  | entete                                                               |
  |        read head of SYRTHES geometry file                            |
  |======================================================================| */

void entete_syr(int *ndim,int *ndimelem,int *npoin,int *nelem,int *nelembord,int *nbno,int *nbnobord)
{
  char ch[LONGUEUR_LIGNE];
  char chaine[90];

  fgets(ch,LONGUEUR_LIGNE,fsyrg);
  fgets(ch,LONGUEUR_LIGNE,fsyrg);
  fgets(ch,LONGUEUR_LIGNE,fsyrg);
  fgets(ch,LONGUEUR_LIGNE,fsyrg);
  sscanf(ch,"%s%s%s%d",chaine,chaine,chaine,ndim);
  fgets(ch,LONGUEUR_LIGNE,fsyrg);
  sscanf(ch,"%s%s%s%s%s%d",chaine,chaine,chaine,chaine,chaine,ndimelem);
  fgets(ch,LONGUEUR_LIGNE,fsyrg);
  sscanf(ch,"%s%s%s%s%s%d",chaine,chaine,chaine,chaine,chaine,npoin);
  fgets(ch,LONGUEUR_LIGNE,fsyrg);
  if (sscanf(ch,"%s%s%s%s%d",chaine,chaine,chaine,chaine,nelem)!=5) {
    printf("\n ERROR syrthes4_to_med30 : head is incorrect for SYRTHES4 file\n\n");
    exit(1);
  }
  fgets(ch,LONGUEUR_LIGNE,fsyrg);
  sscanf(ch,"%s%s%s%s%s%s%d",chaine,chaine,chaine,chaine,chaine,chaine,nelembord);
  fgets(ch,LONGUEUR_LIGNE,fsyrg);
  sscanf(ch,"%s%s%s%s%s%s%s%d",chaine,chaine,chaine,chaine,chaine,chaine,chaine,nbno);

  /* number of nodes per element of bound */
  *nbnobord=*nbno-1;

  printf("   Dimension                    %d\n",*ndim);
  printf("   Dimension of elements        %d\n",*ndimelem);
  printf("   Number of nodes              %d\n",*npoin);
  printf("   Number of elements           %d\n",*nelem);
  printf("   Number of elements of bound  %d\n",*nelembord);
  printf("   Number of nodes per element  %d\n",*nbno);
  
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  :                                                           |
  |======================================================================|
  | coord_syr                                                            |
  |        read SYRTHES geometry file (coordinates)                      |
  |======================================================================| */

void coord_syr(int ndim,int npoin,double *coord, int *nrefn)
{
  char ch[LONGUEUR_LIGNE];
  double z;
  int n,i;

  fgets(ch,LONGUEUR_LIGNE,fsyrg); 
  fgets(ch,LONGUEUR_LIGNE,fsyrg); 
  fgets(ch,LONGUEUR_LIGNE,fsyrg); 
  fgets(ch,LONGUEUR_LIGNE,fsyrg); 

  /* nodes coordinates of SYRTHES mesh */

  if (ndim==2) 
    {
      for (n=0;n<npoin;n++) 
        {
          fgets(ch,LONGUEUR_LIGNE,fsyrg); 
          sscanf(ch,"%d%d%lf%lf%lf",&i,(nrefn+n),(coord+n),(coord+n+npoin),&z);
        }
    }
  else
    {
      for (n=0;n<npoin;n++) 
        {
          fgets(ch,LONGUEUR_LIGNE,fsyrg); 
          sscanf(ch,"%d%d%lf%lf%lf",&i,(nrefn+n),(coord+n),(coord+n+npoin),(coord+n+npoin*2));
        }
    }

  printf("\n   --> End of Syrthes nodes coordinates read\n");

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  :                                                           |
  |======================================================================|
  | elem_syr                                                             |
  |        read SYRTHES geometry file (elements)                         |
  |======================================================================| */

void elem_syr(int nelem,int nbno,int *node,int *nrefe)
{
  char ch[LONGUEUR_LIGNE];
  int n,i;

  /* SYRTHES connectivity */

  fgets(ch,LONGUEUR_LIGNE,fsyrg); 
  fgets(ch,LONGUEUR_LIGNE,fsyrg); 
  fgets(ch,LONGUEUR_LIGNE,fsyrg); 

  if (nbno==4)

    /* --- tetras 4 */

    for (i=0;i<nelem;i++)

      {

        /* Numerotation inchangee, le sens n'a pas d'importance pour Syrthes */
        fgets(ch,LONGUEUR_LIGNE,fsyrg);
	sscanf(ch,"%d%d%d%d%d%d",&n,(nrefe+i),
	       (node+i),(node+i+nelem),(node+i+nelem*2),(node+i+nelem*3));  
      }

  else if (nbno==3)

  /* --- triangles 3  */

    for (i=0;i<nelem;i++)
      {
        /* Numerotation inchangee, le sens n'a pas d'importance pour Syrthes */
        fgets(ch,LONGUEUR_LIGNE,fsyrg);
        sscanf(ch,"%d%d%d%d%d",&n,(nrefe+i),
               (node+i),(node+i+nelem),(node+i+nelem*2)); 
      }

  else if (nbno==2)

  /* --- segments 2  */

    for (i=0;i<nelem;i++)
      {
        /* Numerotation inchangee, le sens n'a pas d'importance pour Syrthes */
        fgets(ch,LONGUEUR_LIGNE,fsyrg);
        sscanf(ch,"%d%d%d%d%d",&n,(nrefe+i),
               (node+i),(node+i+nelem)); 
      }

  else
    {
  
      printf("\n ERROR syrthes4_to_med30 : number of nodes per element is incorrect (nbno=%d)\n\n",nbno);
      exit(1);
   
    }
   
  printf("\n   --> End of Syrthes connectivity read for elements\n");
  
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  :                                                           |
  |======================================================================|
  | elem_syr                                                             |
  |        read SYRTHES geometry file (elements)                         |
  |======================================================================| */

void elembord_syr(int nelembord,int nbnobord,int *nodebord,int *nrefebord)
{
  char ch[LONGUEUR_LIGNE];
  int n,i;

  /* SYRTHES connectivity for elements of bound */

  fgets(ch,LONGUEUR_LIGNE,fsyrg); 
  fgets(ch,LONGUEUR_LIGNE,fsyrg); 
  fgets(ch,LONGUEUR_LIGNE,fsyrg); 

  if (nbnobord==3)

  /* --- triangles 3  */

    for (i=0;i<nelembord;i++)
      {
        /* Numerotation inchangee, le sens n'a pas d'importance pour Syrthes */
        fgets(ch,LONGUEUR_LIGNE,fsyrg);
        sscanf(ch,"%d%d%d%d%d",&n,(nrefebord+i),
               (nodebord+i),(nodebord+i+nelembord),(nodebord+i+nelembord*2)); 
      }

  else if (nbnobord==2)

    /* --- segments 2 */

    for (i=0;i<nelembord;i++)

      {
        fgets(ch,LONGUEUR_LIGNE,fsyrg);
	sscanf(ch,"%d%d%d%d%d%d",&n,(nrefebord+i),
	       (nodebord+i),(nodebord+i+nelembord));  
      }
   
  else
    {
  
      printf("\n ERROR syrthes4_to_med30 : number of nodes per element is incorrect (nbnobord=%d)\n\n",nbnobord);
      exit(1);
   
    }
   
  printf("\n   --> End of Syrthes connectivity read for elements of bound\n");
  
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  :                                                           |
  |======================================================================|
  | entete_resu_syr                                                      |
  |        read head of SYRTHES results file                             |
  |======================================================================| */

void entete_resu_syr(int *nt,double *temps,double *dt)
{
  char ch[90];
  char chlong[LONGUEUR_LIGNE];

  fgets(chlong,LONGUEUR_LIGNE,fsyrr);
  fgets(chlong,LONGUEUR_LIGNE,fsyrr);
  if (sscanf(chlong,"%s %d %s %lf %s %lf\n",ch,nt,ch,temps,ch,dt)!=6) {
  	printf("\n ERROR syrthes4_to_med30 : head of results file problem\n");
	exit(1);
  }
  fgets(chlong,LONGUEUR_LIGNE,fsyrr);

  printf("\n *** timestep=%d, time=%f, delta t=%f ***\n\n",*nt, *temps, *dt);
  
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  :                                                           |
  |======================================================================|
  | entete_champ_resu_syr                                                |
  |        read field-head SYRTHES results file                          |
  |======================================================================| */

int entete_champ_resu_syr(int *nb,int *type,char *nom_champ)
{
  /* if 6 values read return 0, else return 1 */
  char ch[90];
  char chlong[LONGUEUR_LIGNE];
  int lus;

  fgets(chlong,LONGUEUR_LIGNE,fsyrr);
  lus = sscanf(chlong,"%s%s%s%d%s%d",ch,nom_champ,ch,type,ch,nb);
  if (lus==6) {
  	printf("VAR=%s, type=%d, nb=%d\n",nom_champ, *type, *nb);
	return 0;
  }
  else
  	return 1;
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  :                                                           |
  |======================================================================|
  | resu_syr                                                             |
  |        read SYRTHES results file                                     |
  |======================================================================| */

void resu_syr(int nb,double *var)
{
  int nbl,i,j;
  char chlong[LONGUEUR_LIGNE];
  
  /* 6 values per line */
  nbl = nb/6;
  for (i=0;i<nbl;i++) 
    {
      j=i*6;
      fgets(chlong,LONGUEUR_LIGNE,fsyrr);      
      sscanf(chlong,"%lf %lf %lf %lf %lf %lf",
             (var+j),(var+j+1),(var+j+2),
             (var+j+3),(var+j+4),(var+j+5));
    }
    
  /* the last line */
  j=nbl*6;
  switch(nb%6) {
    case 1 : fgets(chlong,LONGUEUR_LIGNE,fsyrr); 
             sscanf(chlong,"%lf",(var+j));
	     break;
    case 2 : fgets(chlong,LONGUEUR_LIGNE,fsyrr); 
             sscanf(chlong,"%lf %lf",(var+j),(var+j+1));
	     break;
    case 3 : fgets(chlong,LONGUEUR_LIGNE,fsyrr); 
             sscanf(chlong,"%lf %lf %lf",
	            (var+j),(var+j+1),(var+j+2));
	     break;
    case 4 : fgets(chlong,LONGUEUR_LIGNE,fsyrr); 
             sscanf(chlong,"%lf %lf %lf %lf",
	            (var+j),(var+j+1),(var+j+2),(var+j+3));
	     break;
    case 5 : fgets(chlong,LONGUEUR_LIGNE,fsyrr); 
             sscanf(chlong,"%lf %lf %lf %lf %lf",
	            (var+j),(var+j+1),(var+j+2),
		    (var+j+3),(var+j+4));
	     break;
  }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  :                                                           |
  |======================================================================|
  | ecrit_coord_med                                                      |
  |        write MED geometry (coordinates)                              |
  |======================================================================| */
void ecrit_coord_med(int ndim,int npoin,double *coord, int *nrefn)
{
  int i,libere,nf;
  int nbf,nrefmax=200;
  int *itrav,*ir,*ifa;

  med_err ret_med=0;
  med_int ndim_med,npoin_med,natt_med,attide_med,attval_med,ngro_med;
  med_int numfam_med;
  //med_int *nrefn_med,*itrav_med;
  med_int *itrav_med;
  med_float *coord_med;
  
  char nom_coo2[2*MED_SNAME_SIZE+1]= "x               y               " ;
  char nom_coo3[3*MED_SNAME_SIZE+1]= "x               y               z               " ;
  char uni_coo2[2*MED_SNAME_SIZE+1]= "m               m               " ;
  char uni_coo3[3*MED_SNAME_SIZE+1]= "m               m               m               " ;

  char nom_fam[MED_NAME_SIZE+1];
  char attdes[MED_COMMENT_SIZE+1]="Reference";
  char gro[MED_COMMENT_SIZE+1]=" ";
  char des[MED_COMMENT_SIZE+1]=" ";
  char dtunit[MED_NAME_SIZE+1]=" ";

  /* initialisations */
  ndim_med=(med_int)ndim;
  npoin_med=(med_int)npoin;

  coord_med=convertit_double_en_med_float(coord,npoin*ndim,&libere);
  //nrefn_med=convertit_int_en_med_int(nrefn,npoin,&libere);

  /* creation du maillage*/

  if (ndim == 2)
    ret_med=MEDmeshCr(fmed,
		      nom_maill,ndim_med,
		      ndim_med,MED_UNSTRUCTURED_MESH,
		      des,dtunit,
		      MED_SORT_DTIT,
		      MED_CARTESIAN,nom_coo2,
		      uni_coo2);
  else
    ret_med=MEDmeshCr(fmed,
		      nom_maill,ndim_med,
		      ndim_med,MED_UNSTRUCTURED_MESH,
		      des,dtunit,
		      MED_SORT_DTIT,
		      MED_CARTESIAN,nom_coo3,
		      uni_coo3);
    

  /*ecriture des noeuds */
  ret_med=MEDmeshNodeCoordinateWr(fmed,
				  nom_maill,
				  MED_NO_DT,
				  MED_NO_IT,
				  MED_NO_DT,
				  MED_NO_INTERLACE,
				  npoin_med,
				  coord_med);

  /* Families creation for nodes */
  nbf=0;
  itrav=(int*)malloc(npoin*sizeof(int));
  ir=(int*)malloc(nrefmax*sizeof(int));
  ifa=(int*)malloc(nrefmax*sizeof(int));
  for (i=0;i<nrefmax;i++) ir[i]=ifa[i]=0;
  for (i=0;i<npoin;i++) itrav[i]=0;
  for (i=0;i<npoin;i++) 
    if (nrefn[i]!=0 && itrav[nrefn[i]]==0)
      {
        itrav[nrefn[i]]=1;
        nbf++;
        ir[nbf]=nrefn[i];
      }

  for (i=0;i<nrefmax;i++)  ifa[ir[i]]=i;

  for (i=0;i<npoin;i++) 
    {itrav[i]=0; if (nrefn[i]!=0) itrav[i]=ifa[nrefn[i]];}

  /* creating family 0 */
  strcpy(nom_fam,"FAM_0");
  numfam_med=0;
  ret_med=MEDfamilyCr(fmed,
		      nom_maill,
		      nom_fam,
		      numfam_med,
		      0,
		      gro);

  /* creating the other families of nodes */
  attide_med=1; natt_med=1; ngro_med=0;
  strcpy(attdes,"Node color");
  for (nf=0;nf<nbf;nf++)
    {
      numfam_med=nf+1;
      ngro_med=0;
      strcpy(nom_fam,"FAM_NODE_");
      sprintf(nom_fam,"%s%d",nom_fam,numfam_med);
      sprintf(nom_fam,"%s_FROM_REF_%d",nom_fam,ir[nf+1]);
      attval_med=ir[nf+1];
      ret_med=MEDfamilyCr(fmed,
			  nom_maill,
			  nom_fam,
			  numfam_med,
			  1,
			  convertit_nomfam_en_nommed(nom_fam));
    } 

  /* writting nodes families */
  itrav_med=convertit_int_en_med_int(itrav,npoin,&libere);

  ret_med=MEDmeshEntityFamilyNumberWr(fmed,
				      nom_maill,
				      MED_NO_DT,
				      MED_NO_IT,
				      MED_NODE,
				      0,
				      npoin_med,
				      itrav_med);
  
  //free(itrav);
  free(ir);
  free(ifa);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  :                                                           |
  |======================================================================|
  | ecrit_elem_med                                                       |
  |        write MED geometry (elements)                                 |
  |======================================================================| */
void ecrit_elem_med(int ndim,int nelem,int nbno,int *node,int *nrefe, int *nbf)
{
  int i,libere,nf;
  int nrefmax=200;
  int *itrav,*ir,*ifa;

  med_err ret_med=0;
  med_int ndim_med,nelem_med,natt_med,attide_med,attval_med,ngro_med;
  med_int numfam_med;
  med_int *node_med,*itrav_med;
  
  char nom_coo2[2*MED_SNAME_SIZE+1]= "x       y       " ;
  char nom_coo3[3*MED_SNAME_SIZE+1]= "x       y       z       " ;

  char nom_fam[MED_NAME_SIZE+1];
  char attdes[MED_COMMENT_SIZE+1]="Reference";
  char gro[MED_COMMENT_SIZE+1]=" ";
  char des[MED_COMMENT_SIZE+1]=" ";


  /* initializations */
  ndim_med=(med_int)ndim;
  nelem_med=(med_int)nelem;

  node_med=convertit_int_en_med_int(node,nbno*nelem,&libere);
  
  /* writing elements */

  if (nbno==4)
  {
  /* elts Tetra   */
    ret_med=MEDmeshElementConnectivityWr(fmed,
					 nom_maill,
					 MED_NO_DT,
					 MED_NO_IT,
					 MED_NO_DT,
					 MED_CELL,
					 MED_TETRA4,
					 MED_NODAL,
					 MED_NO_INTERLACE,
					 nelem_med,
					 node_med);
  }
  else if (nbno==3)
  {
  /* elts Tria */
    ret_med=MEDmeshElementConnectivityWr(fmed,
					 nom_maill,
					 MED_NO_DT,
					 MED_NO_IT,
					 MED_NO_DT,
					 MED_CELL,
					 MED_TRIA3,
					 MED_NODAL,
					 MED_NO_INTERLACE,
					 nelem_med,
					 node_med);
  }
  else if (nbno==2)
  {
  /* elts Seg */
    ret_med=MEDmeshElementConnectivityWr(fmed,
					 nom_maill,
					 MED_NO_DT,
					 MED_NO_IT,
					 MED_NO_DT,
					 MED_CELL,
					 MED_SEG2,
					 MED_NODAL,
					 MED_NO_INTERLACE,
					 nelem_med,
					 node_med);
  }
  else
    {printf("\n syrthes4_to_med only deals with SEG2, TRIA3, TETRA4\n");
      exit(0);}

 
  /* creating families for elements */
  
  itrav=(int*)malloc(nelem*sizeof(int));
  ir=(int*)malloc(nrefmax*sizeof(int));
  ifa=(int*)malloc(nrefmax*sizeof(int));

  (*nbf)=0;
  for (i=0;i<nrefmax;i++) ir[i]=ifa[i]=0;
  for (i=0;i<nelem;i++) itrav[i]=0;
  for (i=0;i<nelem;i++) 
    if (nrefe[i]!=0 && itrav[nrefe[i]]==0)
      {
        itrav[nrefe[i]]=1;
        (*nbf)++;
        ir[*nbf]=nrefe[i];
      }

  for (i=0;i<nrefmax;i++)  ifa[ir[i]]=i;

  /* numbers of elements families are non-positive */
  for (i=0;i<nelem;i++) 
    {itrav[i]=0; if (nrefe[i]!=0) itrav[i]=-ifa[nrefe[i]];}


  /* creating the other families of elements*/
  attide_med=1; natt_med=1; ngro_med=0;
  strcpy(attdes,"Couleur d element");
  for (nf=0;nf<(*nbf);nf++)
    {
      numfam_med=-(nf+1);
      strcpy(nom_fam,"FAM_ELEM_");
      sprintf(nom_fam,"%s%d",nom_fam,(nf+1));
      sprintf(nom_fam,"%s_FROM_REF_%d",nom_fam,ir[nf+1]);
      attval_med=ir[nf+1];
      ret_med=MEDfamilyCr(fmed,
			  nom_maill,
			  nom_fam,
			  numfam_med,
			  1,
			  convertit_nomfam_en_nommed(nom_fam));
      //      printf();

    } 

  /* writing families of elements */
  itrav_med=convertit_int_en_med_int(itrav,nelem,&libere);

  if (nbno==4)
    ret_med=MEDmeshEntityFamilyNumberWr(fmed,
					nom_maill,
					MED_NO_DT,
					MED_NO_IT,
					MED_CELL,
					MED_TETRA4,
					nelem_med,
					itrav_med);
  else if (nbno==3)
    ret_med=MEDmeshEntityFamilyNumberWr(fmed,
					nom_maill,
					MED_NO_DT,
					MED_NO_IT,
					MED_CELL,
					MED_TRIA3,
					nelem_med,
					itrav_med);
  else if (nbno==2)
    ret_med=MEDmeshEntityFamilyNumberWr(fmed,
					nom_maill,
					MED_NO_DT,
					MED_NO_IT,
					MED_CELL,
					MED_SEG2,
					nelem_med,
					itrav_med);
  else
    {printf("\n syrthes4_to_med only deals with SEG2,TRIA3,TETRA4 for elements\n");
      exit(0);}
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  :                                                           |
  |======================================================================|
  | ecrit_elembord_med                                                       |
  |        write MED geometry (elements of bound)                        |
  |======================================================================| */
void ecrit_elembord_med(int ndim,int nelembord,int nbnobord,int *nodebord,int *nrefebord,int nbfelem)
{
  int i,libere,nf;
  int nbf,nrefmax=200;
  int *itrav,*ir,*ifa;

  med_err ret_med=0;
  med_int ndim_med,nelembord_med,natt_med,attide_med,attval_med,ngro_med;
  med_int numfam_med;
  med_int *nodebord_med,*itrav_med;
  
  char nom_coo2[2*MED_SNAME_SIZE+1]= "x       y       " ;
  char nom_coo3[3*MED_SNAME_SIZE+1]= "x       y       z       " ;

  char nom_fam[MED_NAME_SIZE+1];
  char attdes[MED_COMMENT_SIZE+1]="Reference";
  char gro[MED_COMMENT_SIZE+1]=" ";
  char des[MED_COMMENT_SIZE+1]=" ";


  /* initializations */
  ndim_med=(med_int)ndim;
  nelembord_med=(med_int)nelembord;

  nodebord_med=convertit_int_en_med_int(nodebord,nbnobord*nelembord,&libere);
  
  /* writting elements of bound */
  
  if (nbnobord==3)
  {
  /* elts Triangles 2   */
    ret_med=MEDmeshElementConnectivityWr(fmed,
					 nom_maill,
					 MED_NO_DT,
					 MED_NO_IT,
					 MED_NO_DT,
					 MED_CELL,
					 MED_TRIA3,
					 MED_NODAL,
					 MED_NO_INTERLACE,
					 nelembord_med,
					 nodebord_med);
  }
  else if (nbnobord==2)
  {
  /* elts Segments 2 */
    ret_med=MEDmeshElementConnectivityWr(fmed,
					 nom_maill,
					 MED_NO_DT,
					 MED_NO_IT,
					 MED_NO_DT,
					 MED_CELL,
					 MED_SEG2,
					 MED_NODAL,
					 MED_NO_INTERLACE,
					 nelembord_med,
					 nodebord_med);
  }
  else
    {printf("\n syrthes4_to_med only deals with SEG2, TRIA3 for elements of bound\n");
      exit(0);}

 
  /* creating families for elements of bound */
  
  itrav=(int*)malloc(nelembord*sizeof(int));
  ir=(int*)malloc(nrefmax*sizeof(int));
  ifa=(int*)malloc(nrefmax*sizeof(int));

  nbf=0;
  for (i=0;i<nrefmax;i++) ir[i]=ifa[i]=0;
  for (i=0;i<nelembord;i++) itrav[i]=0;
  for (i=0;i<nelembord;i++) 
    if (nrefebord[i]!=0 && itrav[nrefebord[i]]==0)
      {
        itrav[nrefebord[i]]=1;
        nbf++;
        ir[nbf]=nrefebord[i];
      }

  for (i=0;i<nrefmax;i++)  ifa[ir[i]]=i;

  /* numbers of elements families are non-positive */
  /* numbering from -(1+ number of families of elements) */
  for (i=0;i<nelembord;i++) 
    {itrav[i]=0; if (nrefebord[i]!=0) itrav[i]=-(ifa[nrefebord[i]] + nbfelem);}


  /* creating families of elements of bound */
  attide_med=1; natt_med=1; ngro_med=0;
  strcpy(attdes,"Couleur d element bord");
  for (nf=0;nf<nbf;nf++)
    {
      /* numbering from -(1+ number of families of elements) */
      numfam_med=-(nf+1+nbfelem);
      strcpy(nom_fam,"FAM_ELEM_B_");
      sprintf(nom_fam,"%s%d",nom_fam,(nf+1));
      sprintf(nom_fam,"%s_FROM_REF_%d",nom_fam,ir[nf+1]);
      attval_med=ir[nf+1];

      ret_med=MEDfamilyCr(fmed,
			  nom_maill,
			  nom_fam,
			  numfam_med,
			  1,
			  convertit_nomfam_en_nommed(nom_fam));
    } 

  /* writting families of elements of bound */
  itrav_med=convertit_int_en_med_int(itrav,nelembord,&libere);
  if (nbnobord==2)
    ret_med=MEDmeshEntityFamilyNumberWr(fmed,
					nom_maill,
					MED_NO_DT,
					MED_NO_IT,
					MED_CELL,
					MED_SEG2,
					nelembord_med,
					itrav_med);
  else if (nbnobord==3)
    ret_med=MEDmeshEntityFamilyNumberWr(fmed,
					nom_maill,
					MED_NO_DT,
					MED_NO_IT,
					MED_CELL,
					MED_TRIA3,
					nelembord_med,
					itrav_med);
  else
    {printf("\n syrthes4_to_med only deals with SEG2,TRIA3 for elements of bound\n");
      exit(0);}
  free(itrav_med);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  :                                                           |
  |======================================================================|
  | ecrit_resu__med                                                      |
  |        write MED results                                             |
  |======================================================================| */
void ecrit_resu_med(int ndimelem,int npoin,int nt,double temps,
                    double *var,char *nom_champ,int num_ordre,int acreer,
                    int type)
{
  int libere;
  med_err ret_med=0;
  med_int npoin_med;
  med_float *var_med;
  med_int nb_comp=1;
  
  char nom_comp[MED_SNAME_SIZE+1]="Temp          ";
  char nom_unit[MED_SNAME_SIZE+1]="SI            ";


  /* initialization */
  strncpy(nom_comp,nom_champ,MED_SNAME_SIZE);

  var_med=convertit_double_en_med_float(var,npoin,&libere);
  npoin_med=(med_int)npoin;



  /* creating field */
	    
  if (acreer) ret_med=MEDfieldCr(fmed,
				 nom_champ,
				 MED_FLOAT64,
				 nb_comp,
				 nom_comp,
				 nom_unit,
				 "",
				 nom_maill);
  /* writting field */
  switch (type) {
  case 1 :
  /* writting field on elements of bound*/
  	printf("on elements of bound\n\n");
	switch (ndimelem-1) {
	case 1 :
	  ret_med=MEDfieldValueWithProfileWr(fmed,
					     nom_champ,
					     nt,
					     MED_NO_IT,
					     temps,
					     MED_CELL,
					     MED_SEG2,
					     MED_UNDEF_STMODE,
					     MED_NO_PROFILE,
					     MED_NO_LOCALIZATION,
					     MED_NO_INTERLACE,
					     MED_ALL_CONSTITUENT, 
					     npoin_med,
					     (unsigned char*)var_med);
		break;
	case 2 :
	  ret_med=MEDfieldValueWithProfileWr(fmed,
					     nom_champ,
					     nt,
					     MED_NO_IT,
					     temps,
					     MED_CELL,
					     MED_TRIA3,
					     MED_UNDEF_STMODE,
					     MED_NO_PROFILE,
					     MED_NO_LOCALIZATION,
					     MED_NO_INTERLACE,
					     MED_ALL_CONSTITUENT, 
					     npoin_med,
					     (unsigned char*)var_med);
		break;
  	default : 
  		printf("\n\n ERROR syrthes4_to_med : dimension of elements of bound = %d unknown for writting field\n",ndimelem-1);
      		exit(1);
	}
	break;
	
  case 2 : 
  /* writting field on elements*/
  	printf("on elements\n\n");
	switch (ndimelem) {
	case 1 :
	  ret_med=MEDfieldValueWithProfileWr(fmed,
					     nom_champ,
					     nt,
					     MED_NO_IT,
					     temps,
					     MED_CELL,
					     MED_SEG2,
					     MED_UNDEF_STMODE,
					     MED_NO_PROFILE,
					     MED_NO_LOCALIZATION,
					     MED_NO_INTERLACE,
					     MED_ALL_CONSTITUENT, 
					     npoin_med,
					     (unsigned char*)var_med);
		break;
	case 2 :
	  ret_med=MEDfieldValueWithProfileWr(fmed,
					     nom_champ,
					     nt,
					     MED_NO_IT,
					     temps,
					     MED_CELL,
					     MED_TRIA3,
					     MED_UNDEF_STMODE,
					     MED_NO_PROFILE,
					     MED_NO_LOCALIZATION,
					     MED_NO_INTERLACE,
					     MED_ALL_CONSTITUENT, 
					     npoin_med,
					     (unsigned char*)var_med);
		break;
	case 3 :
	  ret_med=MEDfieldValueWithProfileWr(fmed,
					     nom_champ,
					     nt,
					     MED_NO_IT,
					     temps,
					     MED_CELL,
					     MED_TETRA4,
					     MED_UNDEF_STMODE,
					     MED_NO_PROFILE,
					     MED_NO_LOCALIZATION,
					     MED_NO_INTERLACE,
					     MED_ALL_CONSTITUENT, 
					     npoin_med,
					     (unsigned char*)var_med);
		break;
  	default : 
  		printf("\n\n ERROR syrthes4_to_med : dimension of elements = %d unknown for writting field\n",ndimelem);
      		exit(1);
	}
	break;
	
  case 3 :
  /* writting field on nodes*/
  	printf("on nodes\n\n");
	  ret_med=MEDfieldValueWithProfileWr(fmed,
					     nom_champ,
					     nt,
					     MED_NO_IT,
					     temps,
					     MED_NODE,
					     MED_NONE,
					     MED_UNDEF_STMODE,
					     MED_NO_PROFILE,
					     MED_NO_LOCALIZATION,
					     MED_NO_INTERLACE,
					     MED_ALL_CONSTITUENT, 
					     npoin_med,
					     (unsigned char*)var_med);
	break;
	
  default : 
  	printf("\n\n ERROR syrthes4_to_med : type=%d unknown\n",type);
      	exit(1);
  }
	
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | convertit....                                                        |
  |        conversion to MED internal format                             |
  |======================================================================| */

med_int* convertit_int_en_med_int(int *tab,int nb,int* libere)
{
  int i;
  med_int *tab_med;

  if (sizeof(med_int) == sizeof(int)) 
    {
      *libere=0;
      return (med_int*)tab ;
    }
  else
    {
      tab_med=(med_int*)malloc(nb*sizeof(med_int));
      *libere=1;
      for (i=0;i<nb;i++) tab_med[i]=(med_int)tab[i];
      free(tab);
      return tab_med;
    }
}

med_float* convertit_double_en_med_float(double *tab,int nb,int* libere)
{
  int i;
  med_float *tab_med;

  if (sizeof(med_float) == sizeof(double)) 
    {
      *libere=0;
      return (med_float*)tab ;
    }
  else
    {
      tab_med=(med_float*)malloc(nb*sizeof(med_float));
      *libere=1;
      for (i=0;i<nb;i++) tab_med[i]=(med_float)tab[i];
      free(tab);
      return tab_med;
    }
}

char * convertit_nomfam_en_nommed(char * nomfam) {
  char * nommed=0;
  nommed = strchr(nomfam,'_');
  if (nommed)
    {
      nommed = nommed+1;
      nommed = strchr(nommed,'_');
    }
  if (nommed)
    nommed = nommed+1;
  else
    nommed = nomfam;
  return nommed;
}
