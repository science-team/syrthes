/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 1988-2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/
/* Conversions de maillages */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "convert2syrthes.h"

/* longueur maximale d'une ligne du maillage a lire */
#define LONGUEUR_LIGNE 1001
#define MAX_REF 1000
#define SIZE_32 4
#define SIZE_64 8

void fic_bin_f__endswap(void*,size_t,size_t);

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Ouverture du fichier                                                |
  |======================================================================| */
int ouvrir_sim(struct typ_maillage *maillage, /* INOUT structure de maillage */
               char *nomfich) {               /* IN nom de fichier maillage a lire */
   /* ouverture du fichier de maillage .des en lecture */
   (*maillage).fichier_ext = fopen(nomfich,"r"); /* descripteur de fichier d'entree */
   if ( (*maillage).fichier_ext == NULL ) {
     printf("ERROR : unable to open the file %s\n",nomfich);
     return 1;
   }
   printf("SIMAIL file opened : %s\n",nomfich); 
   return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Fermeture du fichier                                                |
  |======================================================================| */
int fermer_sim(struct typ_maillage *maillage) {
   /* fermeture du fichier .des */ 
   if ( (*maillage).fichier_ext == NULL ) {
     printf("ERROR :  unable to close the file .des \n");
     return 1;
   }
   printf("SIMAIL file closed\n");
   fclose((*maillage).fichier_ext);
   return 0;
}





int lire_entete_sim(struct typ_maillage *maillage, int * itmp, int * ficswap)

{

  /* Teste si le fichier binaire est "big endian" ou "little endian"    */
  /* On sait que le premier enregistrement du fichier est de longueur 4 */
  /* -----------------------------------------------------------------  */
  fread(itmp,4,1,(*maillage).fichier_ext);
  *ficswap = 0;
  if (*itmp != 56 && *itmp != 208) 
    {
      fic_bin_f__endswap(itmp, 4, 1); // Soyons prudent !

      if (*itmp == 56 || *itmp == 208)
	*ficswap = 1;
      else
	{
	  printf("\n %%%% ERROR wrong type of binary file coding (little/big endian)");
	  return 1;
	}
    }
  
  if (*itmp == 56) 
    printf("\n INFO Reading a 32 bit SIMAIL file\n");
  else if (*itmp == 208)
    printf("\n INFO Reading a 64 bit SIMAIL file\n");

  /* On remet le pointeur en debut de fichier */
  fseek((*maillage).fichier_ext,0L,SEEK_SET);  
}


int max(int a, int b) {
  if(a < b)
    return b;
  return a;
}

void lecture_tableau_sim_entier
(
 FILE       * fic_maillage,   /*  -> Descripteur du fichier  */
 int        elt_size,       /*  -> Taille d'un élément     */
 int        n_elt,          /*  -> Taille du tableau       */
 int        n_seg,          /*  -> N. segments             */
 int        l_seg,          /*  -> Taille d'un segment     */
 int        * * tableau         /* <-  Valeurs du tableau      */
 )
{
  *tableau=(int*)malloc(n_elt*sizeof(int));
  
  if (elt_size == 4) {

    int i, j;
    int l_rem = n_elt - n_seg*l_seg;
    int l_max = max(l_seg, l_rem);
    int *_tab = NULL;

    _tab = (int *)malloc((l_max + 1)*sizeof(int));

    for (i = 0; i < n_seg; i++) {

      fread(_tab, elt_size, l_seg + 1, fic_maillage);
      printf("_tab[%d] = %d\n",0,_tab[0]);
      if ((int) (_tab[0]) != l_seg) {
	printf("%d != %d\n", _tab[0],l_seg);
	printf("Error\n");
	return;
      }
      for (j = 0; j < l_seg; j++)
        (*tableau)[l_seg*i + j] = _tab[j+1];

    }

    if (l_rem > 0) {

      fread(_tab, elt_size, l_rem + 1, fic_maillage);
      printf("_tab[%d] = %d\n",0,_tab[0]);
      if ((int) (_tab[0]) != l_rem) {
	printf("%d != %d\n", _tab[0],l_rem);
	printf("Error\n");
	return;
      }
      for (j = 0; j < l_rem; j++)
        (*tableau)[l_seg*n_seg + j] = _tab[j+1];

    }
    free(_tab);
  }
  
  else if (elt_size == 8) {

    int i, j;
    int l_rem = n_elt - n_seg*l_seg;
    int l_max = max(l_seg, l_rem);
    long *_tab = NULL;

    _tab = (long*)malloc( (l_max + 1)*sizeof(long));

    for (i = 0; i < n_seg; i++) {

      fread(_tab, elt_size, l_seg + 1, fic_maillage);
      for (j = 0 ; j < l_seg + 1 ; j++)
	printf("_tab[%d] = %d\n",j,_tab[j]);
      if ((int) (_tab[0]) != l_seg) {
	printf("%d != %d\n",_tab[0],l_seg);
	printf("Error\n");
	return;
      }
      for (j = 0; j < l_seg; j++) {
        (*tableau)[l_seg*i + j] = _tab[j+1];
	printf("(*tableau)[%d*%d + %d] = %d\n",l_seg,i,j,(*tableau)[l_seg*i + j]);
	printf("_tab[%d+1] = %d\n",j,_tab[j+1]);
      }
    }

    if (l_rem > 0) {
      fread(_tab, elt_size, l_rem + 1, fic_maillage);
      for (i = 0 ; i < l_seg + 1 ; i++)
	printf("_tab[%d] = %d\n",i,_tab[0]);
      
      if ((int) (_tab[0]) != l_rem) {
	printf("Error");
	return;
      }
      for (j = 0; j < l_rem; j++)
        (*tableau)[l_seg*n_seg + j] = _tab[j+1];
    }
    free(_tab);
  }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | Lecture du fichier                                                   |
  |======================================================================| */
int lire_sim(struct typ_maillage *maillage,
	     struct Maillage *maillnodes,struct MaillageBord *maillnodebord)
{
  int ideb,idebe,nmae,ining,nno,npo,nare,i,j,le,nbegm;
  int ln0,ln1,ln2,ln3,ln4,ln5;
  int typmaill,nbtetra,nbtria,nbseg;
  int ne0,ne1,ne2,ne3,ne4,ne5;
  int le0,le1,le2,le3,le4,le5;
  int *tsim0,*tsim1,*tsim2,*tsim3,*tsim4,*tsim5;
  int /**m,*rien,*taille,*/*it5,nfsy[4],**nrefac;
  int * tmpread;
  int *cmptref,*cmptssd;
  double *co;
  int itmp,ficswap;
  int value_test;
  int taille_elt = 4; /*32 bits int size*/

  /* Teste si le fichier binaire est "big endian" ou "little endian"    */
  /* On sait que le premier enregistrement du fichier est de longueur 4 */
  /* -----------------------------------------------------------------  */
  
  fread(&itmp,4,1,(*maillage).fichier_ext);
  ficswap = 0;
  if (itmp != 56 && itmp != 208) 
    {
      fic_bin_f__endswap(&itmp, 4, 1); // Soyons prudent !

      if (itmp == 56 || itmp == 208)
	ficswap = 1;
      else
	{
	  printf("\n %%%% ERROR wrong type of binary file coding (little/big endian)");
	  return 1;
	}
    }
  
  if (itmp == 56) 
    printf("\n INFO Reading a 32 bit SIMAIL file\n");
  else if (itmp == 208)
    printf("\n INFO Reading a 64 bit SIMAIL file\n");
  
  /* On remet le pointeur en debut de fichier */
  fseek((*maillage).fichier_ext,0L,SEEK_SET);


  /* Teste si le fichier binaire est "big endian" ou "little endian"    */
  /* On sait que le premier enregistrement du fichier est de longueur 4 */

  /* Lecture de l'enregistrement avant le tableau 0 */
  /* ---------------------------------------------  */

  if (itmp == 56) {
    int taille[100];
    int rien[50];
    int m[50];
    fread(m,sizeof(int),2,(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(m,sizeof(int),2);
    
    /*
      m[0] = taille du tableau 0 
      m[1] = nomber d'elements du tableau 0 
     */

    fread(taille,taille_elt,m[1],(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(taille,taille_elt,m[1]);
    fread(&value_test,taille_elt,1,(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(&value_test, taille_elt,1);
    /*
      value_test = taille du tableau 0 (pour verification)
    */
    if (value_test != 56) {
      printf("%% ERROR lire_simail : invalid value\n");
    }
    ne0=ne1=ne2=ne3=ne4=ne5=0;
    le0=le1=le2=le3=le4=le5=0;
    ln0=taille[1];
    ln1=taille[2];
    ln2=taille[3];
    ln3=taille[4];
    ln4=taille[5];
    ln5=taille[6];    
  }
  else if (itmp == 208) {
    long taille[100];
    long rien[50];
    int m[50];
    fread(m,sizeof(int),3,(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(m,sizeof(int),2);
    /*
      m[0] = taille du tableau 0 
      m[1] = nombre d'elements du tableau 0 
      m[2] = taille des entiers (en bits)
      Attention, ce sont des entiers 32  bits !
     */
    
    if(m[2] == 64)
      taille_elt = 8 ; /*64 bits int size*/
    fread(taille,taille_elt,m[1]-1,(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(taille,taille_elt,m[1]-1);

    fread(&(value_test),sizeof(int),1,(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(&(value_test),taille_elt,1);
    /*
      value_test = taille du tableau 0 (pour verification). 
      attention, c'est un entier 4 octets !
    */
    if (value_test != 208) {
      printf("%% ERROR lire_simail : invalid value\n");
    }

    ne0=taille[13];   le0=taille[14];
    ne1=taille[15];   le1=taille[16];
    ne2=taille[17];   le2=taille[18];
    ne3=taille[19];   le3=taille[20];
    ne4=taille[21];   le4=taille[22];
    ne5=taille[23];   le5=taille[24];

    ln0=taille[1];
    ln1=taille[2];
    ln2=taille[3];
    ln3=taille[4];
    ln4=taille[5];
    ln5=taille[6];    

  }

  /* taille[0]=6                                                                     */
  /* taille[1]=nopo0=32              taille[2]=nopo1(tab assoc)   taille[3]=nopo2=27 */
  /* taille[4]=nopo3-elt-grossiers   taille[5]=nopo4-coord        taille[6]=nopo5    */ 

  if (itmp == 56) {
    int taille[100];
    int rien[50];
    int m[50];

    /*
      rien[0] = 132 ?
      rien[1] = nombre d'elements du tableau 0
     */
    fread(rien,taille_elt,2,(*maillage).fichier_ext); 
    fread(m,taille_elt,ln0,(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(m,taille_elt,ln0);
    
    printf("m[   30] NIVEAU = %d\n",  m[29]);
    printf("m[   31] ETAT   = %d\n",  m[30]);
    printf("m[   32] NTACM  = %d\n",  m[31]);
    
    if (m[31]) {
      printf("\n %%%% ERROR lire_simail : Usage of non compatible elements\n");
      return 1;}
  }

  else if(itmp == 208) {
    long taille[100];
    int rien[50];
    long m[50];
    long tmp[50];
    int ind_seg;

    fread(rien,sizeof(int),1,(*maillage).fichier_ext); 
    /*
      rien[0] = 264

     */

    for (i=0 ; i < ne0 ; i++) {
      fread(tmp,taille_elt,ln0+1,(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(m,taille_elt,ln0);
      /*
	tmp[0] = nombre d'element du tableau
       */
      if((int)tmp[0] != le0) {
	printf("\n %%%% ERROR lire_simail : Wrong number of elements for array 0.\n");
	return 1;
      }
      for (j=0 ; j < le0 ; j++) {
	m[le0*i+j] = tmp[j+1];
      }
    }
    printf("m[   30] NIVEAU = %d\n",  m[29]);
    printf("m[   31] ETAT   = %d\n",  m[30]);
    printf("m[   32] NTACM  = %d\n",  m[31]);
    
    if (m[31]) {
      printf("\n %%%% ERROR lire_simail : Usage of non compatible elements\n");
      return 1;}
    

  }


  /* Lecture du tableau 2  */
  /* --------------------  */
  printf("Lecture du tableau 2\n");
  if (itmp == 56) {
    int taille[100];
    int rien[50];
    int * m = (int*) malloc(50*sizeof(int));
    fread(rien,sizeof(int),3,(*maillage).fichier_ext);
    /*
      rien[0] = 132 (delimiteur ?)
      rien[1] = 112
      rien[2] = nombre d'elements du tableau 2
    */
    fread(m,sizeof(int),ln2,(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(m,sizeof(int),ln2);
    if (m[3]==0) {
      printf("\n %%%% ERROR lire_simail : the mesh given is not P1 element (linear) \n\n"); 
      return 1;}
    
    nbtetra=m[9];
    nbtria=m[7];
    nbseg=m[6];
    maillnodes->ndim=m[0];
    
    if      (maillnodes->ndim==2 && nbtria)  {maillnodes->ndmat=3;  maillnodes->nbface=3; nare=3; maillnodes->ndiele=2; typmaill=3;}
    else if (maillnodes->ndim==2 && nbseg)   {maillnodes->ndmat=2;  maillnodes->nbface=0; nare=1; maillnodes->ndiele=1; typmaill=2;}
    else if (maillnodes->ndim==3 && nbtetra) {maillnodes->ndmat=4;  maillnodes->nbface=4; nare=6; maillnodes->ndiele=3; typmaill=3;}
    else if (maillnodes->ndim==3 && nbtria)  {maillnodes->ndmat=3;  maillnodes->nbface=0; nare=3; maillnodes->ndiele=2; typmaill=2;}
    else{
      printf(" Simail mesh file : wrong type of elements found)\n");
      return 0;
    }
    maillnodes->nelem=m[4];
    maillnodes->npoin=m[14];
    /* if (m[3]) */
  }
  else if (itmp == 208) {
    long taille[100];
    int rien[50];
    long m[50];

    fread(rien,sizeof(int),4,(*maillage).fichier_ext);
    /*
      rien[0] = 264 (delimiteur ?)
      rien[1] = 224
      rien[2] = nombre d'elements du tableau 2
    */
    fread(m,sizeof(long),ln2,(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(m,sizeof(long),ln2);
    if (m[3]==0) {
      printf("\n %%%% ERROR lire_simail : the mesh given is not P1 element (linear) \n\n"); 
      return 1;}
    
    nbtetra=m[9];
    nbtria=m[7];
    nbseg=m[6];
    maillnodes->ndim=m[0];
    
    if      (maillnodes->ndim==2 && nbtria)  {maillnodes->ndmat=3;  maillnodes->nbface=3; nare=3; maillnodes->ndiele=2; typmaill=3;}
    else if (maillnodes->ndim==2 && nbseg)   {maillnodes->ndmat=2;  maillnodes->nbface=0; nare=1; maillnodes->ndiele=1; typmaill=2;}
    else if (maillnodes->ndim==3 && nbtetra) {maillnodes->ndmat=4;  maillnodes->nbface=4; nare=6; maillnodes->ndiele=3; typmaill=3;}
    else if (maillnodes->ndim==3 && nbtria)  {maillnodes->ndmat=3;  maillnodes->nbface=0; nare=3; maillnodes->ndiele=2; typmaill=2;}
    else{
      printf(" Simail mesh file : wrong type of elements found)\n");
      return 0;
    }
    maillnodes->nelem=m[4];
    maillnodes->npoin=m[14];

  }


  /* Lecture du tableau 3  tableaux associes */
  /* --------------------------------------  */
  printf("Lecture du tableau 3\n");
  if (ln3)
    { 
      if(itmp == 56) {
	int taille[100];
	int rien[50];
	int * m = (int*) malloc(50*sizeof(int));
	fread(rien,sizeof(int),3,(*maillage).fichier_ext);
	
	m=(int*)realloc(m,ln3*sizeof(int));
	fread(m,sizeof(int),ln3,(*maillage).fichier_ext); 
      }
      else if(itmp == 208) {
	long taille[100];
	int rien[50];

	fread(rien,sizeof(int),4,(*maillage).fichier_ext);
	
	long * m=(long*)malloc((ln3)*sizeof(long));
	fread(m,sizeof(long),ln3,(*maillage).fichier_ext); 
      }
    }

  int taille[100];
  int rien[50];
  int * m = (int*) malloc(50*sizeof(int));
  
  /* Lecture du tableau 4  */
  /* --------------------  */
  printf("Lecture du tableau 4\n");
  co = (double*) malloc(ln4*sizeof(double));
  if(itmp == 56) {
    float * coord=(float*)malloc(ln4*sizeof(float)); 
    if (!coord) {
      printf("\n %%%% ERROR lire_simail : Memory allocation problem\n");
      return 1;}
    fread(rien,sizeof(int),3,(*maillage).fichier_ext);
    
    fread(coord,sizeof(float),ln4,(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(coord,sizeof(float),ln4);
    
    for(i=0 ; i < ln4 ; i++) {
      co[i] = coord[i];
    }
  }
  else if(itmp == 208) {
    double * coord=(double*)malloc(ln4*sizeof(double)); 
    if (!coord) {
      printf("\n %%%% ERROR lire_simail : Memory allocation problem\n");
      return 1;}
    fread(rien,sizeof(int),4,(*maillage).fichier_ext);
    
    fread(coord,sizeof(double),ln4,(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(coord,sizeof(double),ln4);
    
    for(i=0 ; i < ln4 ; i++) {
      co[i] = coord[i];
    }
  }
  
  /* Lecture du tableau 5  */
  /* --------------------  */
  it5=(int*)malloc(ln5*sizeof(int)); 
  if (!it5){
    printf("ERROR : allocation error for array 5\n");
    return 1;
  }

  if(itmp == 56) {
    int * it5_tmp = (int*) malloc(ln5*sizeof(int));
    fread(rien,sizeof(int),3,(*maillage).fichier_ext);
    fread(it5_tmp,sizeof(int),ln5,(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(it5_tmp,sizeof(int),ln5);
    for (i = 0 ; i < ln5 ; i++) {
      it5[i] = it5_tmp[i];
    }
  }
  else if (itmp ==208) {
    long * it5_tmp = (long*) malloc(ln5*sizeof(long));
    fread(rien,sizeof(int),4,(*maillage).fichier_ext);
    fread(it5_tmp,sizeof(long),ln5,(*maillage).fichier_ext); if (ficswap) fic_bin_f__endswap(it5_tmp,sizeof(long),ln5);
    for (i = 0 ; i < ln5 ; i++) {
      it5[i] = it5_tmp[i];
    }
  }

/*    printf(">>> apres lecture i=%d\n",i);
  for (i=0;i<ln5;i++) printf("i=%d it5=%d\n",i,it5[i]); */

  /* Allocations  */
  /* -----------  */

  maillnodes->coord=(double**)malloc(maillnodes->ndim*sizeof(double*));
  for (i=0;i<maillnodes->ndim;i++)  {
    maillnodes->coord[i]=(double*)malloc(maillnodes->npoin*sizeof(double));
    if (!maillnodes->coord[i]){
      printf("ERROR : allocation error for the coordinates\n");
      return 1;
    }
  }

  maillnodes->node=(int**)malloc(maillnodes->ndmat*sizeof(int*));
  for (i=0;i<maillnodes->ndmat;i++) {
    maillnodes->node[i]=(int*)malloc(maillnodes->nelem*sizeof(int)); 
    if (!maillnodes->node[i]){
      printf("ERROR : allocation error for the connectivity\n");
      return 1;
    }
  }

  maillnodes->nref=(int*)malloc(maillnodes->npoin*sizeof(int));
  if (!maillnodes->nref){
    printf("ERROR : allocation error for the node references\n");
    return 1;
  }

  maillnodes->nrefe=(int*)malloc(maillnodes->nelem*sizeof(int));
  if (!maillnodes->nrefe){
    printf("ERROR : allocation error for the element references\n");
    return 1;
  }
  for (i=0;i<maillnodes->nelem;i++) maillnodes->nrefe[i]=0;

      
  for (j=0;j<maillnodes->ndim;j++)
    for (i=0;i<maillnodes->npoin;i++)
      maillnodes->coord[j][i] = (double)co[i*maillnodes->ndim+j];

  if (maillnodes->ndim==3)  {nfsy[0]=0; nfsy[1]=2; nfsy[2]=1; nfsy[3]=3;}

  
  /* tableau de travail pour les ref des faces */
  if (typmaill==3)
    {
      nrefac=(int**)malloc((maillnodes->ndim+1)*sizeof(int*));
      for (i=0;i<maillnodes->ndim+1;i++) {
	nrefac[i]=(int*)malloc(maillnodes->nelem*sizeof(int)); 
	if (!nrefac[i]){
	  printf("ERROR : allocation error for face references\n");
	  return 1;
	}
      }

      for (j=0;j<maillnodes->ndim+1;j++)
	for (i=0;i<maillnodes->nelem;i++)
	  nrefac[j][i]=0;
    }


/* dans un permier temps, on recupere les references des faces sur chaque element */
  ideb=0;
  for (i=0;i<maillnodes->nelem;i++)
    {
      ideb++;
      nmae=it5[ideb];ideb++;
      maillnodes->nrefe[i]=*(it5+ideb);ideb++;
      nno= it5[ideb];ideb++;
      idebe=ideb;
      for (j=0;j<nno;j++) maillnodes->node[j][i]=it5[ideb+j]; 
      ideb+=nno;
      if (nmae) 
	{
	  ining = it5[ideb];ideb++;  
	  if (ining==1 && maillnodes->ndim==3) /* ref de faces, aretes, sommets */
	    {
	      for (j=0;j<maillnodes->nbface;j++) nrefac[nfsy[j]][i]=it5[ideb+j];
	      ideb+=maillnodes->nbface;
	      ideb+=nare; 
              /* on met les ref des noeuds sommet */
	      for (j=0;j<nno;j++) maillnodes->nref[maillnodes->node[j][i]-1]=it5[ideb+j];
	      ideb+=nno; 
	    }
          else if (ining==2) /* ref de aretes, sommets */
	    {
	      if (maillnodes->ndim==3 && typmaill==3) /* tetra en 3D*/
		{
		  for (j=0;j<maillnodes->nbface;j++) nrefac[nfsy[j]][i]=0; /* pas de ref de face */
		  ideb+=nare; 
		  /* on met les ref des noeuds sommet */
		  for (j=0;j<nno;j++)  maillnodes->nref[maillnodes->node[j][i]-1]=it5[ideb+j];
		  ideb+=nno;
		}
	      else if(maillnodes->ndim==2 && typmaill==3) /* triangles en 2D */
		{
		  /* les ref d'aretes sont les ref de face  */
		  for (j=0;j<maillnodes->nbface;j++) nrefac[j][i]=it5[ideb+j];
		  ideb+=maillnodes->nbface; 
		  /* on met les ref des noeuds sommet */
		  for (j=0;j<nno;j++)  maillnodes->nref[maillnodes->node[j][i]-1]=it5[ideb+j];
		  ideb+=nno;
		}
	      else if(maillnodes->ndim==3 && typmaill==2) /* triangles en 3D */
		{
		  /* pas de ref de face, pas de ref d'aretes  */
		  ideb+=nare; 
		  /* on met les ref des noeuds sommet */
		  for (j=0;j<nno;j++)  maillnodes->nref[maillnodes->node[j][i]-1]=it5[ideb+j];
		  ideb+=nno;
		}
	      else if(maillnodes->ndim==2 && typmaill==2) /* segments en 2D */
		{
		  /* pas de ref de face, pas de ref d'aretes  */
		  ideb+=nare; 
		  /* on met les ref des noeuds sommet */
		  for (j=0;j<nno;j++)  maillnodes->nref[maillnodes->node[j][i]-1]=it5[ideb+j];
		  ideb+=nno;
		}
	    }
	  else if (ining==3) /* ref de sommets */
	    {
	      /* on met les ref des noeuds sommet */
	      for (j=0;j<nno;j++)  maillnodes->nref[maillnodes->node[j][i]-1]=it5[ideb+j];
	      ideb+=nno; 
	    }
	}

    }


  free(m); free(it5); free(co);


  /* creation du maillage de bord */
  /* ---------------------------- */
  if (typmaill==3)
    {
      if (maillnodes->ndim==2)
	extrbord2(*maillnodes,maillnodebord,nrefac);
      else
	extrbord3(*maillnodes,maillnodebord,nrefac);

      for (i=0;i<maillnodes->ndim+1;i++) free(nrefac[i]);
      free(nrefac);
    }
  /* recuperation des infos concernant les references du maillage */
  /* ------------------------------------------------------------ */
  cmptssd=(int*)malloc(MAX_REF*sizeof(int)); 
  for (i=0;i<MAX_REF;i++) cmptssd[i]=0;
  for (i=0;i<maillnodes->nelem;i++) cmptssd[maillnodes->nrefe[i]]++;

  if (typmaill==3)
    {
      cmptref=(int*)malloc(MAX_REF*sizeof(int)); 
      for (i=0;i<MAX_REF;i++) cmptref[i]=0;
      for (i=0;i<maillnodebord->nelem;i++) cmptref[maillnodebord->nrefe[i]]++;
    }


  /* mise a jour de la structure type_maill pour recoller au reste de la base de donnee*/
  maillage->dimension=maillnodes->ndim;
  maillage->dim_elem=maillnodes->ndiele;
  maillage->nbnoeuds=maillnodes->npoin; 
  maillage->nbelem=maillnodes->nelem;
  maillage->nbnoeuds_par_elem=maillnodes->ndmat;

  maillage->xcoord=maillnodes->coord[0];
  maillage->ycoord=maillnodes->coord[1];
  if (maillnodes->ndim==3) maillage->zcoord=maillnodes->coord[2];
  maillage->coul_noeud=maillnodes->nref;
  maillage->coul_elem=maillnodes->nrefe;

  if (typmaill==3)
    {
      maillage->nbelem_de_bord=maillnodebord->nelem; 
      maillage->coul_elembord=maillnodebord->nrefe;
    }
  else
    {
      maillage->nbelem_de_bord=0;
      maillage->coul_elembord=NULL;
    }


  /* pour la connectivite, il faut changer l'ordre du stockage */
  maillage->liste_elem=(int**)malloc(maillnodes->nelem*sizeof(int*));
  for (i=0;i<maillnodes->nelem;i++) {
    maillage->liste_elem[i]=(int*)malloc(maillnodes->ndmat*sizeof(int)); 
    if (!maillage->liste_elem[i]){
      printf("ERROR : allocation error for the connectivity\n");
      return 1;
    }
  }
  for (i=0;i<maillnodes->nelem;i++)
    for (j=0;j<maillnodes->ndmat;j++)
      maillage->liste_elem[i][j]=maillnodes->node[j][i];


  for (j=0;j<maillnodes->ndmat;j++)     free(maillnodes->node[j]);
  free(maillnodes->node);


  /* pour la connectivite de bord, il faut changer l'ordre du stockage */
  if (typmaill==3)
    {
      maillage->liste_elembord=(int**)malloc(maillnodebord->nelem*sizeof(int*));
      for (i=0;i<maillnodebord->nelem;i++) {
	maillage->liste_elembord[i]=(int*)malloc(maillnodebord->ndmat*sizeof(int)); 
	if (!maillage->liste_elembord[i]){
	  printf("ERROR : allocation error for the connectivity\n");
	  return 1;
	}
      }
      for (i=0;i<maillnodebord->nelem;i++)
	for (j=0;j<maillnodebord->ndmat;j++)
	  maillage->liste_elembord[i][j]=maillnodebord->node[j][i];
      
      for (j=0;j<maillnodebord->ndmat;j++) free(maillnodebord->node[j]);
      free(maillnodebord->node);
    }

  printf("\n\n *** SIMAIL MESH :\n");
  if (typmaill==2){
    printf("                           |--------------------|\n");
    printf("                           |    Surfacic mesh   |\n");
    printf("      ---------------------|--------------------|\n");
    printf("      | Dimension          |    %8d        |\n",maillage->dimension);
    printf("      | Nodes number       |    %8d        |\n",maillage->nbnoeuds);
    printf("      | Elements number    |    %8d        |\n",maillage->nbelem);
    printf("      | Nb nodes per elt   |    %8d        |\n",maillage->nbnoeuds_par_elem);
    printf("      |--------------------|--------------------|\n");
    printf("      |    Material        |--------------------|\n");    
    printf("      |--------------------|--------------------|\n");
    for (i=1;i<MAX_REF;i++)
      if (cmptssd[i]>0)
	printf("      |        %2d          |    %8d        |\n",i,cmptssd[i]);
    printf("      |--------------------|--------------------|\n");
  }
  else {
    printf("                           |--------------------|------------------|\n");
    printf("                           |    Volumic mesh    |   Boundary mesh  |\n");
    printf("      ---------------------|--------------------|------------------|\n");
    printf("      | Dimension          |    %8d        |    %8d      |\n",maillage->dimension,maillage->dimension-1);
    printf("      | Nodes number       |    %8d        |    not used      |\n",maillage->nbnoeuds);
    printf("      | Elements number    |    %8d        |    %8d      |\n",maillage->nbelem,maillage->nbelem_de_bord);
    printf("      | Nb nodes per elt   |    %8d        |    %8d      |\n",maillage->nbnoeuds_par_elem,maillage->dimension);
    printf("      |--------------------|--------------------|------------------|\n");
    printf("      |    Material        |---------------------------------------|\n");    
    printf("      |--------------------|--------------------|------------------|\n");
    for (i=1;i<MAX_REF;i++)
      if (cmptssd[i]>0)
	printf("      |        %2d          |    %8d        |         ---      |\n",i,cmptssd[i]);
    printf("      |--------------------|--------------------|------------------|\n");
    printf("      | Boundary references  ---------------------------------------|\n");    
    printf("      |--------------------|--------------------|------------------|\n");
    for (i=1;i<MAX_REF;i++)
      if (cmptref[i]>0)
	printf("      |        %2d          |         ---        |    %8d      |\n",i,cmptref[i]);
    printf("      |--------------------|--------------------|------------------|\n");
  }


  free(cmptssd); 
  if (typmaill==3) free(cmptref);
  return 0;
}



/*----------------------------------------------------------------------------*/
/* Permutation des octets pour passage de "little endian" a "big endian"      */
/* (Y. Fournier)                                                              */
/*----------------------------------------------------------------------------*/
void fic_bin_f__endswap
(
 void  *buf,   /* Tampon contenant les elements                               */
 size_t size,  /* Taille d'un element                                         */
 size_t nitems /* Nombre d'elements                                           */
)
{
  char  tmpswap;
  char *ptr = (char *)buf;

  size_t i, j, shift;
  for (j = 0; j < nitems; j++) {
    shift = j * size;
    for (i = 0; i < (size / 2); i++) {
      tmpswap = *(ptr + shift + i);
      *(ptr + shift + i ) = *(ptr + shift + (size - 1) - i);
      *(ptr + shift + (size - 1) - i) = tmpswap;
    }
  }
}

