/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 1988-2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/
/* Conversions de maillages */
/* routines d'ecrire au format Syrthes */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "convert2syrthes.h"

/* longueur maximale d'une ligne du maillage a lire */
#define LONGUEUR_LIGNE 1001

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion du format SIMAIL                                         |
  |======================================================================| */
int ouvrir_syr(struct typ_maillage *maillage, /* INOUT structure de maillage */
               char *nomfich) {               /* IN nom de fichier maillage a ecrire */
   /* ouverture du fichier de maillage .syr en ecriture */
   if (maillage->mode_ecriture == 0) 
     (*maillage).fichier_syr = fopen(nomfich,"w"); /* descripteur de fichier d'entree */
   else if (maillage->mode_ecriture == 1)
     (*maillage).fichier_syr = fopen(nomfich,"wb"); /* descripteur de fichier d'entree */
   if ( (*maillage).fichier_syr == NULL ) {
     printf("ERROR : unable to open the file %s\n",nomfich);
     return 1;
   }
   printf("SYRTHES file opened : %s\n",nomfich); 
   return 0;
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion du format SIMAIL                                         |
  |======================================================================| */
int reouvrir_syr(struct typ_maillage *maillage, /* INOUT structure de maillage */
               char *nomfich) {                 /* IN nom de fichier maillage a reecrire */
   /* ouverture du fichier de maillage .syr en lecture/ecriture */
   (*maillage).fichier_syr = fopen(nomfich,"r+"); /* descripteur de fichier d'entree */
   if ( (*maillage).fichier_syr == NULL ) {
     printf("ERROR : unable to open again the file %s \n",nomfich);
     return 1;
   }
   /* printf("SYRTHES file opened again : %s\n",nomfich);  */
   return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion du format SIMAIL                                         |
  |======================================================================| */
int fermer_syr(struct typ_maillage *maillage) {
   /* fermeture du fichier .syr */ 
   if ( (*maillage).fichier_syr == NULL ) {
     printf("ERROR : unable to close SYRTHES file\n");
     return 1;
   }
   /* printf("SYRTHES file closed\n"); */
   fclose((*maillage).fichier_syr);
   return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion du format SIMAIL                                         |
  |======================================================================| */
int ecrire_entete_syr(struct typ_maillage *maillage) {
/* ecriture de l'entete format Syrthes */
  
  printf("\nWriting the head of the file..\n");

  if(maillage->mode_ecriture == 1) {
    fwrite(&(maillage->dimension), sizeof(int),1,maillage->fichier_syr);
    fwrite(&(maillage->dim_elem), sizeof(int),1,maillage->fichier_syr);
    fwrite(&(maillage->nbnoeuds), sizeof(int),1,maillage->fichier_syr);
    fwrite(&(maillage->nbelem), sizeof(int),1,maillage->fichier_syr);
    fwrite(&(maillage->nbelem_de_bord), sizeof(int),1,maillage->fichier_syr);
    fwrite(&(maillage->nbnoeuds_par_elem), sizeof(int),1,maillage->fichier_syr);
    return 0;
  }
  
  fprintf((*maillage).fichier_syr,"C*V4.0*******************************************C\n");
  fprintf((*maillage).fichier_syr,"C           FICHIER GEOMETRIQUE SYRTHES          C\n");
  fprintf((*maillage).fichier_syr,"C************************************************C\n");
  fprintf((*maillage).fichier_syr,"C  DIMENSION =%2i\n",(*maillage).dimension);
  fprintf((*maillage).fichier_syr,"C  DIMENSION DES ELTS =%2i\n",(*maillage).dim_elem);
  fprintf((*maillage).fichier_syr,"C  NOMBRE DE NOEUDS =   %10i\n",(*maillage).nbnoeuds);
  fprintf((*maillage).fichier_syr,"C  NOMBRE D'ELEMENTS =  %10i\n",(*maillage).nbelem);
  fprintf((*maillage).fichier_syr,"C  NOMBRE D'ELEMENTS DE BORD =  %10i\n",(*maillage).nbelem_de_bord);
  fprintf((*maillage).fichier_syr,"C  NOMBRE DE NOEUDS PAR ELEMENT = %3i\n",(*maillage).nbnoeuds_par_elem);
  fprintf((*maillage).fichier_syr,"C************************************************C\n");

  fflush(maillage->fichier_syr);
  fflush(stdout);

  return 0;
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion du format SIMAIL                                         |
  |======================================================================| */
int ecrire_coord_syr(struct typ_maillage *maillage) {
/* ecriture des coordonnees des noeuds format Syrthes */
/* prerequis : (*maillage).coul_noeud, (*maillage).xcoord, (*maillage).ycoord  (et (*maillage).zcoord) existent */

  int i; /*indice de boucle */
  
  printf("\nWriting coordinates...\n");

  if (maillage->mode_ecriture == 1) {
    fwrite(maillage->coul_noeud, sizeof(int),maillage->nbnoeuds, maillage->fichier_syr);
    fwrite(maillage->xcoord,sizeof(double),maillage->nbnoeuds, maillage->fichier_syr);
    fwrite(maillage->ycoord,sizeof(double),maillage->nbnoeuds, maillage->fichier_syr);
    free(maillage->xcoord);
    free(maillage->ycoord);
    
    if(maillage->dimension == 3) {
      fwrite(maillage->zcoord,sizeof(double),maillage->nbnoeuds, maillage->fichier_syr);
      free(maillage->zcoord);
    }
    return 0;
  }
 
  fprintf((*maillage).fichier_syr,"C\n");
  fprintf((*maillage).fichier_syr,"C$ RUBRIQUE = NOEUDS\n");
  fprintf((*maillage).fichier_syr,"C\n");
  
  if ((*maillage).dimension == 2) {      /* 3e coordonnee nulle */
    for (i=0;i<(*maillage).nbnoeuds;i++)
      fprintf((*maillage).fichier_syr,"%10i%4i %14.7E %14.7E %14.7E \n", i+1, (*maillage).coul_noeud[i], 
	      (*maillage).xcoord[i], (*maillage).ycoord[i], 0.0);
    
    /* liberation de la memoire */
    free((*maillage).xcoord);
    free((*maillage).ycoord);
  }
  
  else {                                  /* 3D */
    for (i=0;i<(*maillage).nbnoeuds;i++)
      fprintf((*maillage).fichier_syr,"%10i%4i %14.7E %14.7E %14.7E \n", i+1, (*maillage).coul_noeud[i], 
	      (*maillage).xcoord[i], (*maillage).ycoord[i], (*maillage).zcoord[i]);
    
    /* liberation de la memoire */
    free((*maillage).xcoord);
    free((*maillage).ycoord);
    free((*maillage).zcoord);
  }

  fflush(maillage->fichier_syr);
  fflush(stdout);

  return 0;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion du format SIMAIL                                         |
  |======================================================================| */
int ecrire_elem_syr(struct typ_maillage *maillage) {
/* ecriture des elements format Syrthes */
/* prerequis : (*maillage).coul_elem, (*maillage).liste_elem existent */

  int i; /* indice de boucle */
  
  printf("Writing elements...\n");

  if (maillage->mode_ecriture == 1) {
    fwrite(maillage->coul_elem, sizeof(int),maillage->nbelem, maillage->fichier_syr);
    for(i=0; i<maillage->nbelem; i++)
      fwrite(maillage->liste_elem[i], sizeof(int), maillage->nbnoeuds_par_elem, maillage->fichier_syr);
    
    /* liberation des couleurs des elements */
    free((*maillage).coul_elem);
    return 0;
  }

  fprintf((*maillage).fichier_syr,"C\n");
  fprintf((*maillage).fichier_syr,"C$ RUBRIQUE = ELEMENTS\n");
  fprintf((*maillage).fichier_syr,"C\n");


  switch ((*maillage).nbnoeuds_par_elem) {
  case  4 : /* 4 noeuds a ecrire */
              for (i=0;i<(*maillage).nbelem;i++)
                fprintf((*maillage).fichier_syr,"%10i%4i%10i%10i%10i%10i\n",
	                        i+1, (*maillage).coul_elem[i], 
                                (*maillage).liste_elem[i][0], (*maillage).liste_elem[i][1], (*maillage).liste_elem[i][2],
                                (*maillage).liste_elem[i][3]);
	    break;
  case  3 : /* 3 noeuds a ecrire */
              for (i=0;i<(*maillage).nbelem;i++)
                fprintf((*maillage).fichier_syr,"%10i%4i%10i%10i%10i\n",
	                        i+1, (*maillage).coul_elem[i], 
                                (*maillage).liste_elem[i][0], (*maillage).liste_elem[i][1], (*maillage).liste_elem[i][2]);
	    break;
  case  2 : /* 2 noeuds a ecrire */
              for (i=0;i<(*maillage).nbelem;i++)
                fprintf((*maillage).fichier_syr,"%10i%4i%10i%10i\n",
	                        i+1, (*maillage).coul_elem[i], 
                                (*maillage).liste_elem[i][0], (*maillage).liste_elem[i][1]);
	    break;
  default :
    printf("ERROR : wrong number of nodes per elements (%i)\n",(*maillage).nbnoeuds_par_elem);
    return 1;
  }


  fflush(maillage->fichier_syr);
  fflush(stdout);

  /* liberation des couleurs des elements */
  free((*maillage).coul_elem);
  return 0;
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion du format SIMAIL                                         |
  |======================================================================| */
int ecrire_elem_de_bord_syr(struct typ_maillage *maillage) {
/* ecriture des elements de bord pour Syrthes 4.0 */
/* prerequis : (*maillage).liste_elembord  (*maillage).coul_elembord  (*maillage).nbelem_de_bord existent */

  int i; /* indice de boucle */
  int nbnoeuds_elembord = 0; /* nombre de noeuds par element de bord */
  
  printf("\nWriting boundary elements...\n");



  switch ((*maillage).nbnoeuds_par_elem) {
  case 4 :  /* 3 noeuds sommets (triangles)*/
             nbnoeuds_elembord = 3;
	     break;
  case  3 : /* 2 noeuds sommets (aretes) */
             nbnoeuds_elembord = 2;
	     break;
  default :
    printf("ERROR : wrong number of nodes per elements (%i)\n",(*maillage).nbnoeuds_par_elem);
    return 1;
  }

  if (maillage->mode_ecriture == 1) {
    fwrite(maillage->coul_elembord, sizeof(int),maillage->nbelem_de_bord, maillage->fichier_syr);
    for(i=0; i<maillage->nbelem_de_bord; i++)
      fwrite(maillage->liste_elembord[i], sizeof(int), nbnoeuds_elembord, maillage->fichier_syr);
    
    /* liberation de la memoire pour liste_elembord*/
    for (i=0;i<(*maillage).nbelem_de_bord;i++) free((*maillage).liste_elembord[i]);
    free ((*maillage).liste_elembord);
    /* liberation de la memoire pour coul_elembord*/
    free ((*maillage).coul_elembord);    
    return 0;
  }

  fprintf((*maillage).fichier_syr,"C\n");
  fprintf((*maillage).fichier_syr,"C$ RUBRIQUE = ELEMENTS DE BORD\n");
  fprintf((*maillage).fichier_syr,"C\n");
  
  if (nbnoeuds_elembord == 3) {
    for (i=0;i<(*maillage).nbelem_de_bord;i++)
      fprintf((*maillage).fichier_syr,"%10i%4i%10i%10i%10i\n",
	                        i+1, (*maillage).coul_elembord[i],
                                (*maillage).liste_elembord[i][0], (*maillage).liste_elembord[i][1],
                                (*maillage).liste_elembord[i][2]);
  }
  else {
    for (i=0;i<(*maillage).nbelem_de_bord;i++)
      fprintf((*maillage).fichier_syr,"%10i%4i%10i%10i\n",
	                        i+1, (*maillage).coul_elembord[i],
                                (*maillage).liste_elembord[i][0], (*maillage).liste_elembord[i][1]);
  }
  

  fflush(maillage->fichier_syr);
  fflush(stdout);

  /* liberation de la memoire pour liste_elembord*/
  for (i=0;i<(*maillage).nbelem_de_bord;i++) free((*maillage).liste_elembord[i]);
  free ((*maillage).liste_elembord);
  /* liberation de la memoire pour coul_elembord*/
  free ((*maillage).coul_elembord);
  
  return 0;

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  |  Conversion du format SIMAIL                                         |
  |======================================================================| */
int ecrire_reffaces_syr(struct typ_maillage *maillage) {
/* ecriture des references aux faces format Syrthes 3.4 */
/* prerequis : (*maillage).liste_reffaces existe */

  int i; /* indice de boucle */
  int nbfaces_par_elem = 0; /* nombre de faces par element */
  
  printf("\nWriting faces references...\n");



  switch ((*maillage).nbnoeuds_par_elem) {
  case 4 :  /* 4 faces pour un tetraedre */
             nbfaces_par_elem = 4;
	     break;
  case  3 : /* 3 aretes pour un triangle */
             nbfaces_par_elem = 3;
	     break;
  default :
    printf("ERROR : wrong number of nodes per elements (%i)\n",(*maillage).nbnoeuds_par_elem);
    return 1;
  }

  if(maillage->mode_ecriture == 1)
  {
    for(i=0; i<maillage->nbelem_de_bord; i++)
      fwrite(maillage->liste_reffaces[i], sizeof(int), nbfaces_par_elem, maillage->fichier_syr);
    
    /* liberation de la memoire */
    for (i=0;i<(*maillage).nbelem;i++) free((*maillage).liste_reffaces[i]);
    free ((*maillage).liste_reffaces);
    return 0;
  }
  
  fprintf((*maillage).fichier_syr,"C\n");
  fprintf((*maillage).fichier_syr,"C$ RUBRIQUE = REFERENCES DES FACES\n");
  fprintf((*maillage).fichier_syr,"C\n");
  
  if (nbfaces_par_elem == 4) {
    for (i=0;i<(*maillage).nbelem;i++)
      fprintf((*maillage).fichier_syr,"%10i %5i%5i%5i%5i\n",
	                        i+1, 
                                (*maillage).liste_reffaces[i][0], (*maillage).liste_reffaces[i][1],
                                (*maillage).liste_reffaces[i][2], (*maillage).liste_reffaces[i][3]);
  }
  else {
    for (i=0;i<(*maillage).nbelem;i++)
      fprintf((*maillage).fichier_syr,"%10i %5i%5i%5i\n",
	                        i+1, 
                                (*maillage).liste_reffaces[i][0], (*maillage).liste_reffaces[i][1],
                                (*maillage).liste_reffaces[i][2]);
  }
  

  fflush(maillage->fichier_syr);
  fflush(stdout);

  /* liberation de la memoire */
  for (i=0;i<(*maillage).nbelem;i++) free((*maillage).liste_reffaces[i]);
  free ((*maillage).liste_reffaces);
  
  return 0;
}


