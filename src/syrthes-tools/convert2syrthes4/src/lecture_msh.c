/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 1988-2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/
/* Conversions de maillages */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "convert2syrthes.h"

/* longueur maximale d'une ligne du maillage a lire */
#define LONGUEUR_LIGNE 1001

int ouvrir_msh(struct typ_maillage *maillage, /* INOUT structure de maillage */
               char *nomfich) {               /* IN nom de fichier maillage a lire */
   /* ouverture du fichier de maillage .msh en lecture */
   (*maillage).fichier_ext = fopen(nomfich,"r"); /* descripteur de fichier d'entree */
   if ( (*maillage).fichier_ext == NULL ) {
     printf("ERROR : unable to open the file %s\n",nomfich);
     return 1;
   }
   printf("GMSH file opened : %s\n",nomfich); 
   return 0;
}

int fermer_msh(struct typ_maillage *maillage) {
   /* fermeture du fichier .msh */ 
   if ( (*maillage).fichier_ext == NULL ) {
     printf("ERROR :  unable to close the file .msh \n");
     return 1;
   }
   printf("GMSH file closed\n");
   fclose((*maillage).fichier_ext);
   return 0;
}

int lire_entete_msh(struct typ_maillage *maillage) {
/* rien a lire */  
  
  printf(" Dimension : %i\n", (*maillage).dimension);
  if ((*maillage).dimension != 2 && (*maillage).dimension != 3) {
     printf("ERROR : non valid dimension\n");
     return 1;
  }
  return 0;
}

int lire_coord_msh(struct typ_maillage *maillage) {
  /* lecture de maillage GMSH       */
  /* coordonnees */
  /*  ATTENTION : le numero du noeud est considere comme croissant d'un a un en partant de 1 */
  
  char chaine[LONGUEUR_LIGNE];  /* une ligne du fichier                */
  char mot1[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  int i; /* indice de boucle */
  int entier1;
  int numnoeud_moinsun; /* numero du noeud - 1 */
  
  printf("  Reading coordinates...\n");

  strcpy(mot1,"\0");
  /* recherche du debut de chapitre */
  /* selon version, $Nodes ou $NOD */
  while (strcmp(mot1,"$Nodes") != 0 && strcmp(mot1,"$NOD") != 0) {
    if (fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext) == NULL) {
      printf("ERROR : coordinates not found\n");
      return 1;
    }
    (*maillage).numligne++;
    sscanf(chaine, "%s", mot1);
  }

  /* nombre de noeuds */
  fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
  (*maillage).numligne++;
  sscanf(chaine, "%i", &((*maillage).nbnoeuds));
  
  switch ((*maillage).dimension) {
  case 2 : 
    /* allocations et verifications */
    ((*maillage).xcoord) = (double *) malloc (sizeof(double) * ((*maillage).nbnoeuds));
    if ((*maillage).xcoord == NULL) {
      printf("ERROR : allocation error for the x coordinate, needed size : %zu \n", sizeof(double) * ((*maillage).nbnoeuds));
      return 1;
    }
    ((*maillage).ycoord) = (double *) malloc (sizeof(double) * ((*maillage).nbnoeuds));
    if ((*maillage).ycoord == NULL) {
      printf("ERROR : allocation error for the y coordinate, needed size : %zu \n", sizeof(double) * ((*maillage).nbnoeuds));
      return 1;
    }   
    numnoeud_moinsun = 0;
    for (numnoeud_moinsun = 0; numnoeud_moinsun < (*maillage).nbnoeuds ; numnoeud_moinsun++) {
      fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
      (*maillage).numligne++;
      sscanf(chaine, "%i %lf %lf", &entier1, &((*maillage).xcoord[numnoeud_moinsun]), &((*maillage).ycoord[numnoeud_moinsun]));
    }
    break;
  case 3 : 
    /* allocations et verifications */
    ((*maillage).xcoord) = (double *) malloc (sizeof(double) * ((*maillage).nbnoeuds));
    if ((*maillage).xcoord == NULL) {
      printf("ERROR : allocation error for the x coordinate, needed size : %zu \n", sizeof(double) * ((*maillage).nbnoeuds));
      return 1;
    }
    ((*maillage).ycoord) = (double *) malloc (sizeof(double) * ((*maillage).nbnoeuds));
    if ((*maillage).ycoord == NULL) {
      printf("ERROR : allocation error for the y coordinate, needed size : %zu \n", sizeof(double) * ((*maillage).nbnoeuds));
      return 1;
    }   
    ((*maillage).zcoord) = (double *) malloc (sizeof(double) * ((*maillage).nbnoeuds));
    if ((*maillage).zcoord == NULL) {
      printf("ERROR : allocation error for the z coordinate, needed size : %zu \n", sizeof(double) * ((*maillage).nbnoeuds));
      return 1;
    }   
    for (numnoeud_moinsun = 0; numnoeud_moinsun < (*maillage).nbnoeuds ; numnoeud_moinsun++) {
      fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
      (*maillage).numligne++;
      sscanf(chaine, "%i %lf %lf %lf", &entier1, &((*maillage).xcoord[numnoeud_moinsun]), 
                                                 &((*maillage).ycoord[numnoeud_moinsun]),
					         &((*maillage).zcoord[numnoeud_moinsun]));
    }
    break;
  default :  /* probleme sur dimension */
    printf("ERROR : dimension is : %i\n",(*maillage).dimension);
    return -1;
  } 

  if (entier1 != (*maillage).nbnoeuds) {
    printf("ERROR : number of the last node isn't equal to the number of nodes : %i != %i\n",entier1,(*maillage).nbnoeuds);
    return -1;
  }  
  return 0;
}

/*-------------------------------------------------------------------------------------------*/

int lire_noeuds_colores_msh(struct typ_maillage *maillage) {
/* lecture des noeuds colores : rien a lire*/
/* allocation et initialisation du tableau des couleurs des noeuds */

  int i; /* indice de boucle */
  
  /* allocation */
  ((*maillage).coul_noeud) = (int *) malloc (sizeof(int) * ((*maillage).nbnoeuds));
  if ((*maillage).coul_noeud == NULL) {
    printf("ERROR : allocation error for nodes color, needed size : %zu \n", sizeof(int) * ((*maillage).nbnoeuds));
    return 1;
  }   
  /* initialisation a zero */
  for (i=0;i<(*maillage).nbnoeuds;i++) (*maillage).coul_noeud[i] = 0;
  
  return 0;
}

/*-------------------------------------------------------------------------------------------*/
 
int lire_elem_msh(struct typ_maillage *maillage) {
/* lecture des elements dans le fichier GMSH */
/* noeuds colores, elements et elements de bord */
  char chaine[LONGUEUR_LIGNE];  /* une ligne du fichier                */
  char mot1[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  int entier1, entier2, entier3, entier4, entier5, entier6;
  int typeelem; /* type d'elements a lire */
  int nbelemtotal; /* nombre d'elements total a lire */
  int nbnoeuds_par_elembord = 0; /* nombre de noeuds par element de bord */
  int numnoeud; /* numero de noeud colore */
  int cpt_noeudcolore; /* compteur des noeuds colores */
  int couleur; /* couleur lue */
  int tetraedreP2; /* presence de tetraedres P2 */
  int tetraedreP1; /* presence de tetraedres P1 */
  int triangleP2; /* presence de triangles P2 */
  int triangleP1; /* presence de triangles P1 */
  int segmentP2; /* presence de segments P2 */
  int segmentP1; /* presence de segments P1 */
  int i,j; /* indices de boucle */
  int* coul_elem_tetraedre; /* tableau local des couleurs d'elements tetraedres */
  int* coul_elem_triangle; /* tableau local des couleurs d'elements triangles */
  int* coul_elem_segment; /* tableau local des couleurs d'elements triangles */
  int **elem_tetraedre; /* liste des elements tetraedres */
  int **elem_triangle; /* liste des elements triangles */
  int **elem_segment; /* liste des elements segments */
  
  printf("  Reading elements ...\n");
  
  strcpy(mot1,"\0");
  /* recherche du debut de chapitre */
  /* selon version, $Elements ou $ELM */
  while (strcmp(mot1,"$Elements") != 0 && strcmp(mot1,"$ELM") != 0) {
    if (fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext) == NULL) {
      printf("ERROR : elements not found\n");
      return 1;
    }
    (*maillage).numligne++;
    sscanf(chaine, "%s", mot1);
  }
  fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
  (*maillage).numligne++;
  /* determination du nombre total d'elements */
  sscanf(chaine, "%i", &nbelemtotal);
  
  /* initialisations */
  cpt_noeudcolore = 0;
  tetraedreP2 = 0;
  tetraedreP1 = 0;
  triangleP2  = 0;
  triangleP1  = 0;
  segmentP2   = 0;
  segmentP1   = 0;
  
  /* allocation et verification */
  elem_tetraedre = (int **) malloc (sizeof(int *) * nbelemtotal);
  elem_triangle = (int **) malloc (sizeof(int *) * nbelemtotal);
  elem_segment = (int **) malloc (sizeof(int *) * nbelemtotal);
  coul_elem_tetraedre = (int *) malloc (sizeof(int ) * nbelemtotal);
  coul_elem_triangle = (int *) malloc (sizeof(int ) * nbelemtotal);
  coul_elem_segment = (int *) malloc (sizeof(int ) * nbelemtotal);
  if (elem_tetraedre == NULL || elem_triangle == NULL || elem_segment == NULL) {
    printf("ERROR : allocation error (lecture_msh), needed size for each elem_ : %zu \n", sizeof(int *) * (nbelemtotal));
    return 1;
  }
  /* les allocations plus basses de ces tableaux seront faites en fonction des elements trouves (elements de bord ou non) */
  if (coul_elem_tetraedre == NULL || coul_elem_triangle == NULL || coul_elem_segment == NULL) {
    printf("ERROR : allocation error for the elements color, needed size : %zu \n", sizeof(int) * (nbelemtotal));
    return 1;
  }
  
  /* initialisations des couleurs */
  
  for (i=0;i<nbelemtotal;i++) coul_elem_tetraedre[i] = 0;
  for (i=0;i<nbelemtotal;i++) coul_elem_triangle[i] = 0;
  for (i=0;i<nbelemtotal;i++) coul_elem_segment[i] = 0;
  
  fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
  (*maillage).numligne++;
  /* determination du type d'elements et du nombre de noeuds par element */
  sscanf(chaine, "%i %i", &entier1, &typeelem);
  
  /* rien ne dit que les elements se trouvent ranges d'une certaine maniere */
  /* on est donc oblige de regarder le type de l'element sur chaque ligneee */
  
  for(i=0;i<nbelemtotal;i++){
  
    switch (typeelem) {
    case 11 : /* tetraedre a 10 noeuds => dimension des elements = 3 */
          /* ATTENTION : noeud 9 et 10 a intervertir!! */
          elem_tetraedre[tetraedreP2] = (int *) malloc (sizeof(int ) * 10);
          /* lecture de l'element, meme numerotation que Syrthes*/
          sscanf(chaine, "%i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i", &entier1, &entier2, &entier3, &couleur, &entier5, &entier6, 
	                          &(elem_tetraedre[tetraedreP2][0]),
	                          &(elem_tetraedre[tetraedreP2][1]),
	                          &(elem_tetraedre[tetraedreP2][2]),				  
	                          &(elem_tetraedre[tetraedreP2][3]),				  
	                          &(elem_tetraedre[tetraedreP2][4]),				  
	                          &(elem_tetraedre[tetraedreP2][5]),				  
	                          &(elem_tetraedre[tetraedreP2][6]),				  
	                          &(elem_tetraedre[tetraedreP2][7]),				  
	                          &(elem_tetraedre[tetraedreP2][9]),				  
	                          &(elem_tetraedre[tetraedreP2][8]));
	  coul_elem_tetraedre[tetraedreP2] = couleur;
          tetraedreP2++;
	  break;
    case 4 : /* tetraedre a 4 noeuds => dimension des elements = 3 */
          elem_tetraedre[tetraedreP1] = (int *) malloc (sizeof(int ) * 4);
          /* lecture de l'element, meme numerotation que Syrthes*/
          sscanf(chaine, "%i %i %i %i %i %i %i %i %i %i", &entier1, &entier2, &entier3, &couleur, &entier5, &entier6, 
	                          &(elem_tetraedre[tetraedreP1][0]),
	                          &(elem_tetraedre[tetraedreP1][1]),
	                          &(elem_tetraedre[tetraedreP1][2]),				  
	                          &(elem_tetraedre[tetraedreP1][3]));
	  coul_elem_tetraedre[tetraedreP1] = couleur;
          tetraedreP1++;
	  break;
    case 9 : /* triangle a 6 noeuds => */
          elem_triangle[triangleP2] = (int *) malloc (sizeof(int ) * 6);
          /* lecture de l'element, meme numerotation que Syrthes*/
          sscanf(chaine, "%i %i %i %i %i %i %i %i %i %i %i %i", &entier1, &entier2, &entier3, &couleur, &entier5, &entier6, 
	                          &(elem_triangle[triangleP2][0]),
	                          &(elem_triangle[triangleP2][1]),
	                          &(elem_triangle[triangleP2][2]),				  
	                          &(elem_triangle[triangleP2][3]),				  
	                          &(elem_triangle[triangleP2][4]),				  
	                          &(elem_triangle[triangleP2][5]));
	  coul_elem_triangle[triangleP2] = couleur;
          triangleP2++;
	  break;
    case 2 : /* triangle a 3 noeuds  */
          elem_triangle[triangleP1] = (int *) malloc (sizeof(int ) * 3);
          /* lecture de l'element, meme numerotation que Syrthes*/
          sscanf(chaine, "%i %i %i %i %i %i %i %i %i", &entier1, &entier2, &entier3, &couleur, &entier5, &entier6, 
	                          &(elem_triangle[triangleP1][0]),
	                          &(elem_triangle[triangleP1][1]),
		                  &(elem_triangle[triangleP1][2]));				  
	  coul_elem_triangle[triangleP1] = couleur;
          triangleP1++;
 	  break;
    case 8 : /* segment 3 noeuds  */
          elem_segment[segmentP2] = (int *) malloc (sizeof(int ) * 3);
          /* lecture de l'element, meme numerotation que Syrthes*/
          sscanf(chaine, "%i %i %i %i %i %i %i %i %i", &entier1, &entier2, &entier3, &couleur, &entier5, &entier6, 
	                          &(elem_segment[segmentP2][0]),
	                          &(elem_segment[segmentP2][1]),
		                  &(elem_segment[segmentP2][2]));				  
	  coul_elem_segment[segmentP2] = couleur;
          segmentP2++;
	  break;
    case 1 : /* segment 2 noeuds  */
          elem_segment[segmentP1] = (int *) malloc (sizeof(int ) * 2);
          /* lecture de l'element, meme numerotation que Syrthes*/
          sscanf(chaine, "%i %i %i %i %i %i %i %i", &entier1, &entier2, &entier3, &couleur, &entier5, &entier6, 
	                          &(elem_segment[segmentP1][0]),
		                  &(elem_segment[segmentP1][1]));				  
	  coul_elem_segment[segmentP1] = couleur;
          segmentP1++;
	  break;
    case 15 : /* noeud colore */
          /* lecture du numero du noeud et de la couleur*/
          sscanf(chaine, "%i %i %i %i %i %i %i", &entier1, &entier2, &entier3, &couleur, &entier5, &entier6, &numnoeud);
	  (*maillage).coul_noeud[numnoeud-1] = couleur;
          cpt_noeudcolore++;
	  break;
    default : 
  	  printf("ERROR : unknown element type line %i\n",(*maillage).numligne);
	  return 1;
    }
    
    fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
    (*maillage).numligne++;
    /* determination du type d'elements*/
    sscanf(chaine, "%i %i", &entier1, &typeelem);
  } /* Fin de la boucle sur les elements a lire */

  if (segmentP1+segmentP2+triangleP1+triangleP2+tetraedreP1+tetraedreP2+cpt_noeudcolore != nbelemtotal) {
    printf("ERROR : wrong total number of elements\n");
    return 1;
  }
  
  if ( (segmentP2>0 || triangleP2>0 || tetraedreP2>0) && ((*maillage).version_syr == 4) ) {
    printf("ERROR : element P2 found - This is not allowed with SYRTHES 4.0, Stop.\n");
    return 1;
  }

  if ( (tetraedreP2>0 || tetraedreP1>0)&& ((*maillage).dimension != 3) ) {
    printf("ERROR : tetrahedron element found in dimension %i, stop.\n", (*maillage).dimension);
    return 1;
  }

  if ( tetraedreP2>0 ) { /* dimension de elements vaut 3 et le nombre de noeuds par element vaut 10*/
    (*maillage).dim_elem = 3;
    (*maillage).nbnoeuds_par_elem = 10;
    (*maillage).nbelem = tetraedreP2;
    (*maillage).nbelem_de_bord = triangleP2;
    nbnoeuds_par_elembord = 6;
  }
  else if  ( tetraedreP1>0 ) { /* dimension de elements vaut 3 et le nombre de noeuds par element vaut 4*/
    (*maillage).dim_elem = 3;
    (*maillage).nbnoeuds_par_elem = 4;
    (*maillage).nbelem = tetraedreP1;
    (*maillage).nbelem_de_bord = triangleP1;
    nbnoeuds_par_elembord = 3;
  }
  else if  ( triangleP2>0 ) { /* dimension de elements vaut 2 et le nombre de noeuds par element vaut 6*/
    (*maillage).dim_elem = 2;
    (*maillage).nbnoeuds_par_elem = 6;
    (*maillage).nbelem = triangleP2;
    (*maillage).nbelem_de_bord = segmentP2;
    nbnoeuds_par_elembord = 3;
  }
  else if  ( triangleP1>0 ) { /* dimension de elements vaut 2 et le nombre de noeuds par element vaut 3*/
    (*maillage).dim_elem = 2;
    (*maillage).nbnoeuds_par_elem = 3;
    (*maillage).nbelem = triangleP1;
    (*maillage).nbelem_de_bord = segmentP1;
    nbnoeuds_par_elembord = 2;
  }
  else if  ( segmentP2>0 ) { /* dimension de elements vaut 1 et le nombre de noeuds par element vaut 3*/
    (*maillage).dim_elem = 1;
    (*maillage).nbnoeuds_par_elem = 3;
    (*maillage).nbelem = segmentP2;
    (*maillage).nbelem_de_bord = 0;
    nbnoeuds_par_elembord = 0;
    if ((*maillage).dimension != 2) {
      printf("\nWARNING : elements dimension is 1 while space dimension is %i !!\n\n", (*maillage).dimension);
    }
  }
  else if  ( segmentP1>0 ) { /* dimension de elements vaut 1 et le nombre de noeuds par element vaut 2*/
    (*maillage).dim_elem = 1;
    (*maillage).nbnoeuds_par_elem = 2;
    (*maillage).nbelem = segmentP1;
    (*maillage).nbelem_de_bord = 0;
    nbnoeuds_par_elembord = 0;
    if ((*maillage).dimension != 2) {
      printf("\nWARNING : elements dimension is 1 while space dimension is %i !!\n\n", (*maillage).dimension);
    }
  }
  
  if (tetraedreP2>0 || tetraedreP1>0) {
    (*maillage).liste_elem = (int **) malloc (sizeof(int *) * (*maillage).nbelem);
    (*maillage).liste_elembord = (int **) malloc (sizeof(int *) * (*maillage).nbelem_de_bord);
    (*maillage).coul_elem = (int *) malloc (sizeof(int *) * (*maillage).nbelem);
    (*maillage).coul_elembord = (int *) malloc (sizeof(int *) * (*maillage).nbelem_de_bord);
    if ((*maillage).liste_elem == NULL ||  (*maillage).coul_elem == NULL ) {
      printf("ERROR : allocation error (lecture_msh) for the structure\n");
      return 1;
    }
    /* stockage des elements dans la structure */
    for(i=0;i<(*maillage).nbelem;i++) {
      (*maillage).liste_elem[i] = (int *) malloc (sizeof(int ) * ((*maillage).nbnoeuds_par_elem));
      for(j=0;j<(*maillage).nbnoeuds_par_elem;j++) (*maillage).liste_elem[i][j] = elem_tetraedre[i][j];
      free(elem_tetraedre[i]);
      (*maillage).coul_elem[i] = coul_elem_tetraedre[i];
    }/* stockage des elements de bord dans la structure */
    for(i=0;i<(*maillage).nbelem_de_bord;i++) {
      (*maillage).liste_elembord[i] = (int *) malloc (sizeof(int ) * nbnoeuds_par_elembord);
      for(j=0;j<nbnoeuds_par_elembord;j++) (*maillage).liste_elembord[i][j] = elem_triangle[i][j];
      free(elem_triangle[i]);
      (*maillage).coul_elembord[i] = coul_elem_triangle[i];
    }
  }
  else if (triangleP2>0 || triangleP1>0) {
    (*maillage).liste_elem = (int **) malloc (sizeof(int *) * (*maillage).nbelem);
    (*maillage).liste_elembord = (int **) malloc (sizeof(int *) * (*maillage).nbelem_de_bord);
    (*maillage).coul_elem = (int *) malloc (sizeof(int *) * (*maillage).nbelem);
    (*maillage).coul_elembord = (int *) malloc (sizeof(int *) * (*maillage).nbelem_de_bord);
    if ((*maillage).liste_elem == NULL || (*maillage).coul_elem == NULL) {
      printf("ERROR : allocation error (lecture_msh) for the structure\n");
      return 1;
    }
    /* stockage des elements dans la structure */
    for(i=0;i<(*maillage).nbelem;i++) {
      (*maillage).liste_elem[i] = (int *) malloc (sizeof(int ) * ((*maillage).nbnoeuds_par_elem));
      for(j=0;j<(*maillage).nbnoeuds_par_elem;j++) (*maillage).liste_elem[i][j] = elem_triangle[i][j];
      free(elem_triangle[i]);
      (*maillage).coul_elem[i] = coul_elem_triangle[i];
    }/* stockage des elements de bord dans la structure */
    for(i=0;i<(*maillage).nbelem_de_bord;i++) {
      (*maillage).liste_elembord[i] = (int *) malloc (sizeof(int ) * nbnoeuds_par_elembord);
      for(j=0;j<nbnoeuds_par_elembord;j++) (*maillage).liste_elembord[i][j] = elem_segment[i][j];
      free(elem_segment[i]);
      (*maillage).coul_elembord[i] = coul_elem_segment[i];
    }
  }
  else if  (segmentP2>0 || segmentP1>0) {
    (*maillage).liste_elem = (int **) malloc (sizeof(int *) * (*maillage).nbelem);
    (*maillage).liste_elembord = (int **) malloc (sizeof(int *) * (*maillage).nbelem_de_bord);
    (*maillage).coul_elem = (int *) malloc (sizeof(int *) * (*maillage).nbelem);
    (*maillage).coul_elembord = (int *) malloc (sizeof(int *) * (*maillage).nbelem_de_bord);
    if ((*maillage).liste_elem == NULL || (*maillage).coul_elem == NULL ) {
      printf("ERROR : allocation error (lecture_msh) for the structure\n");
      return 1;
    }
    /* stockage des elements dans la structure */
    for(i=0;i<(*maillage).nbelem;i++) {
      (*maillage).liste_elem[i] = (int *) malloc (sizeof(int ) * ((*maillage).nbnoeuds_par_elem));
      for(j=0;j<(*maillage).nbnoeuds_par_elem;j++) (*maillage).liste_elem[i][j] = elem_segment[i][j];
      free(elem_segment[i]);
      (*maillage).coul_elem[i] = coul_elem_segment[i];
    }
  }
  else {
    printf("ERROR : no element found line %i\n",(*maillage).numligne);
    return 1;
  }
   
  

  /* liberation de la memoire */
  free(elem_tetraedre);
  free(elem_triangle);
  free(elem_segment);
  free(coul_elem_tetraedre);
  free(coul_elem_triangle);
  free(coul_elem_segment);

  return 0;
}

int lire_zones_msh(struct typ_maillage *maillage) {
/* rien a lire ni a faire */
  return 0;
}


int lire_cl_msh(struct typ_maillage *maillage) {
/* rien a lire */
/* Pour Syrthes 4.0 : rien a faire */
  return 0;
}


/*-------------------------------------------------------------------------------------------*/

int modif_noeuds_mshsyr(struct typ_maillage *maillage) {
/* modification de la couleur des noeuds qui ont une couleur differente de 0 */
/* valable uniquement pour GMSH */
/* pourra etre ameliore en ne reparcourant que le fichier Syrthes mais probleme de saut de ligne pour l'instant */

  char chaine[LONGUEUR_LIGNE];  /* une ligne du fichier                */
  char mot1[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot2[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot3[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  char mot4[LONGUEUR_LIGNE];    /* mot dans la ligne                   */
  int i; /* indice de boucle */
  int j; /* indice de boucle */
  int numnoeud; /* numero du noeud lu */
  int entier2; /* couleur lue ignoree */
  double xcoord, ycoord, zcoord; /* coordonnees */
  
  /* initialisations */
  strcpy(mot1,"\0");
  strcpy(mot4,"\0");
  /* recherche du debut du chapitre des coordonnees dans le fichier Syrthes */
  while (strcmp(mot1,"C$") != 0 && strcmp(mot4,"NOEUDS") != 0) {
    if (fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_syr) == NULL) {
      printf("ERROR : coordinates not found\n");
      return 1;
    }
    sscanf(chaine, "%s %s %s %s", mot1, mot2, mot3, mot4);
  }
  fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_syr);
  
  /* initialisations */
  strcpy(mot1,"\0");
  /* recherche du debut de chapitre des coordonnes pour le fichier GMSH */
  while (strcmp(mot1,"$Nodes") != 0 && strcmp(mot2,"$NOD") != 0) {
    if (fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext) == NULL) {
      printf("ERROR : coordinates not found\n");
      return 1;
    }
    sscanf(chaine, "%s", mot1);
  }
  fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
  
  /* recherche des noeuds qui ont change de couleurs */
  for (i=0;i<(*maillage).nbnoeuds;i++) {
    if ((*maillage).coul_noeud[i] == 0) {
      fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
      fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_syr);
    }
    else {
      /* si le noeud a une couleur */
      fgets(chaine, LONGUEUR_LIGNE , (*maillage).fichier_ext);
      /* lecture des coordonnees dans le fichier GMSH */    
      sscanf(chaine, "%i %lf %lf %lf", &numnoeud, &xcoord, &ycoord, &zcoord);
      /* ecriture des coordonnees dans le fichier Syrthes */    
      fprintf((*maillage).fichier_syr,"%10i%4i %14.7E %14.7E %14.7E \n",
	      numnoeud, (*maillage).coul_noeud[i], 
	      xcoord, ycoord, zcoord);
    }
  }       

  return 0;
}

