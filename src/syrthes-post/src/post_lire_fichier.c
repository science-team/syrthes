/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

# include "post_usertype.h"
# include "post_bd.h"
# include "post_proto.h"


extern char nomgeom[200];


/*|======================================================================|
  | SYRTHES 4.3                2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture d'un maillage au format Syrthes                              |
  |======================================================================| */
void lire_syrthes(FILE *fgeom,struct Maillage *maillnodes,struct Maillage *maillnodebord,
		  struct SDparall *sdparall )
{
  char ch[200],ch1[200],chnum[5];
  rp_int ndmats_lu,npoin_lu,nelem_lu,nelemb_lu,ndim_lu,bidon;
  rp_int i,j,n,nr,nv,nb,rubrc,rubperio;
  rp_int *ne0,*ne1,*ne2,*ne3;
  rp_int nptot,netot,nebtot;
  double x,y,z;
  char *jysuis;


  rewind(fgeom);

  fgets(ch,90,fgeom);
  fgets(ch,90,fgeom);
  fgets(ch,90,fgeom);
  
  if (POST_TYPEWIDTH == 32)
    {
      fscanf(fgeom,"%s%s%s%d",ch,ch,ch,&(maillnodes->ndim));
      fscanf(fgeom,"%s%s%s%s%s%d",ch,ch,ch,ch,ch,&(maillnodes->ndiele));
      fscanf(fgeom,"%s%s%s%s%s%d",ch,ch,ch,ch,ch,&(maillnodes->npoin));
      fscanf(fgeom,"%s%s%s%s%d",ch,ch,ch,ch,&(maillnodes->nelem));
      fscanf(fgeom,"%s%s%s%s%s%s%d",ch,ch,ch,ch,ch,ch,&(maillnodebord->nelem));
      fscanf(fgeom,"%s%s%s%s%s%s%s%d",ch,ch,ch,ch,ch,ch,ch,&(maillnodes->ndmat));
    }
  else
    {
      fscanf(fgeom,"%s%s%s%ld",ch,ch,ch,&(maillnodes->ndim));
      fscanf(fgeom,"%s%s%s%s%s%ld",ch,ch,ch,ch,ch,&(maillnodes->ndiele));
      fscanf(fgeom,"%s%s%s%s%s%ld",ch,ch,ch,ch,ch,&(maillnodes->npoin));
      fscanf(fgeom,"%s%s%s%s%ld",ch,ch,ch,ch,&(maillnodes->nelem));
      fscanf(fgeom,"%s%s%s%s%s%s%ld",ch,ch,ch,ch,ch,ch,&(maillnodebord->nelem));
      fscanf(fgeom,"%s%s%s%s%s%s%s%ld",ch,ch,ch,ch,ch,ch,ch,&(maillnodes->ndmat));
    }
  
  maillnodebord->ndim  = maillnodes->ndim;
  maillnodebord->ndmat = maillnodes->ndmat-1;
  maillnodebord->ndiele= maillnodes->ndiele-1;
  maillnodebord->npoin = 0;   


  maillnodes->coord=(double**)malloc(maillnodes->ndim*sizeof(double*));
  for (i=0;i<maillnodes->ndim;i++)  maillnodes->coord[i]=(double*)malloc(maillnodes->npoin*sizeof(double)); 
  verif_alloue_double2d(maillnodes->ndim,"lire_syrthes",maillnodes->coord);

  maillnodes->node=(rp_int**)malloc(maillnodes->ndmat*sizeof(rp_int*));
  for (i=0;i<maillnodes->ndmat;i++) maillnodes->node[i]=(rp_int*)malloc(maillnodes->nelem*sizeof(rp_int)); 
  verif_alloue_int2d(maillnodes->ndmat,"lire_syrthes",maillnodes->node);

  maillnodes->nref=(rp_int*)malloc(maillnodes->npoin*sizeof(rp_int));
  verif_alloue_int1d("lire_syrthes",maillnodes->nref);

  maillnodes->nrefe=(rp_int*)malloc(maillnodes->nelem*sizeof(rp_int));
  verif_alloue_int1d("lire_syrthes",maillnodes->nrefe);

  if (maillnodebord->nelem>0){
      maillnodebord->node=(rp_int**)malloc(maillnodebord->ndmat*sizeof(rp_int*));
      for (i=0;i<maillnodebord->ndmat;i++) maillnodebord->node[i]=(rp_int*)malloc(maillnodebord->nelem*sizeof(rp_int)); 
      verif_alloue_int2d(maillnodebord->ndmat,"lire_syrthes",maillnodebord->node);
      
      maillnodebord->nrefe=(rp_int*)malloc(maillnodebord->nelem*sizeof(rp_int));
      verif_alloue_int1d("lire_syrthes",maillnodebord->nrefe);
    }

  jysuis=NULL;
  do  {fgets(ch,90,fgeom); jysuis=strstr(ch,"RUBRIQUE = NOEUDS");} while (!jysuis); 
  fgets(ch,90,fgeom);

  /* coordonnees noeuds maillage SYRTHES */
  if (POST_TYPEWIDTH == 32)
    {
      if (maillnodes->ndim==2) 
	for (i=0;i<maillnodes->npoin;i++)
	  fscanf(fgeom,"%d%d%14lf%14lf%14lf",&n,maillnodes->nref+i,
		 (maillnodes->coord[0]+i),(maillnodes->coord[1]+i),&z);
      else
	for (i=0;i<maillnodes->npoin;i++)
	  fscanf(fgeom,"%d%d%14lf%14lf%14lf",&n,maillnodes->nref+i,
		 (maillnodes->coord[0]+i),(maillnodes->coord[1]+i),(maillnodes->coord[2]+i));
    }
  else
    {
      if (maillnodes->ndim==2) 
	for (i=0;i<maillnodes->npoin;i++)
	  fscanf(fgeom,"%ld%ld%14lf%14lf%14lf",&n,maillnodes->nref+i,
		 (maillnodes->coord[0]+i),(maillnodes->coord[1]+i),&z);
      else
	for (i=0;i<maillnodes->npoin;i++)
	  fscanf(fgeom,"%ld%ld%14lf%14lf%14lf",&n,maillnodes->nref+i,
		 (maillnodes->coord[0]+i),(maillnodes->coord[1]+i),(maillnodes->coord[2]+i));
    }
  
  /* connectivite maillage volumique SYRTHES */
  jysuis=NULL;
  do  {fgets(ch,90,fgeom); jysuis=strstr(ch,"RUBRIQUE = ELEMENTS");} while (!jysuis); 
  fgets(ch,90,fgeom);
  
  if (POST_TYPEWIDTH == 32)
    {
      if (maillnodes->ndmat==3)
	for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],ne2=maillnodes->node[2];
	     i<maillnodes->nelem;
	     i++,ne0++,ne1++,ne2++)
	  fscanf(fgeom,"%d%d%d%d%d",&n,maillnodes->nrefe+i,ne0,ne1,ne2);
      
      else
	for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],ne2=maillnodes->node[2],ne3=maillnodes->node[3];
	     i<maillnodes->nelem;
	     i++,ne0++,ne1++,ne2++,ne3++)
	  fscanf(fgeom,"%d%d%d%d%d%d",&n,maillnodes->nrefe+i,ne0,ne1,ne2,ne3);
    }
  else
    {
      if (maillnodes->ndmat==3)
	for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],ne2=maillnodes->node[2];
	     i<maillnodes->nelem;
	     i++,ne0++,ne1++,ne2++)
	  fscanf(fgeom,"%ld%ld%ld%ld%ld",&n,maillnodes->nrefe+i,ne0,ne1,ne2);
      
      else
	for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],ne2=maillnodes->node[2],ne3=maillnodes->node[3];
	     i<maillnodes->nelem;
	     i++,ne0++,ne1++,ne2++,ne3++)
	  fscanf(fgeom,"%ld%ld%ld%ld%ld%ld",&n,maillnodes->nrefe+i,ne0,ne1,ne2,ne3);
    }

  
  
  if (maillnodebord->nelem>0){
    /* connectivite maillage de bord SYRTHES */
    jysuis=NULL;
    do  {fgets(ch,90,fgeom); jysuis=strstr(ch,"RUBRIQUE = ELEMENTS DE BORD");} while (!jysuis); 
    fgets(ch,90,fgeom);
    
    if (POST_TYPEWIDTH == 32)
      {
	if (maillnodebord->ndmat==3)
	  for (i=0,ne0=maillnodebord->node[0],ne1=maillnodebord->node[1],ne2=maillnodebord->node[2];
	       i<maillnodebord->nelem;
	       i++,ne0++,ne1++,ne2++)
	    fscanf(fgeom,"%d%d%d%d%d",&n,maillnodebord->nrefe+i,ne0,ne1,ne2);
	
	else
	  for (i=0,ne0=maillnodebord->node[0],ne1=maillnodebord->node[1];
	       i<maillnodebord->nelem;
	       i++,ne0++,ne1++)
	    fscanf(fgeom,"%d%d%d%d",&n,maillnodebord->nrefe+i,ne0,ne1);
      }
    else
      {
	if (maillnodebord->ndmat==3)
	  for (i=0,ne0=maillnodebord->node[0],ne1=maillnodebord->node[1],ne2=maillnodebord->node[2];
	       i<maillnodebord->nelem;
	       i++,ne0++,ne1++,ne2++)
	    fscanf(fgeom,"%ld%ld%ld%ld%ld",&n,maillnodebord->nrefe+i,ne0,ne1,ne2);
	
	else
	  for (i=0,ne0=maillnodebord->node[0],ne1=maillnodebord->node[1];
	       i<maillnodebord->nelem;
	       i++,ne0++,ne1++)
	    fscanf(fgeom,"%ld%ld%ld%ld",&n,maillnodebord->nrefe+i,ne0,ne1);
      }
  }

  /*    imprime_maillage_ref(*maillnodes); */
  /* imprime_maillage_ref(*maillnodebord);*/


  /* numerotation des noeuds a partir de 0 */
  for (i=0;i<maillnodes->nelem;i++)
    for (j=0;j<maillnodes->ndmat;j++)
      maillnodes->node[j][i]--;

  if (maillnodebord->nelem>0){
    for (i=0;i<maillnodebord->nelem;i++)
      for (j=0;j<maillnodebord->ndmat;j++)
	maillnodebord->node[j][i]--;
  }

  /* -------------------------------------------------------- */
  /* cas du parallelisme : on lit les tableau complementaires */
  /* -------------------------------------------------------- */
  if (sdparall->nparts>=1)
    {

      sdparall->nbele=maillnodes->nelem;


      /* C$ RUBRIQUE = NBNO PARALLELISME  */
      jysuis=NULL;
      do  {fgets(ch,90,fgeom); jysuis=strstr(ch,"RUBRIQUE = PARALLELISME - NBNO");} while (!jysuis); 
      fgets(ch,90,fgeom);

      if (POST_TYPEWIDTH == 32)
	{
	  fscanf(fgeom,"%s%s%d",ch,ch,&(sdparall->nbno_interne));
	  fscanf(fgeom,"%s%s%d",ch,ch,&(sdparall->nbno_front));
	  fscanf(fgeom,"%s%s%d",ch,ch,&(sdparall->nbno_global));
	}
      else
	{
	  fscanf(fgeom,"%s%s%ld",ch,ch,&(sdparall->nbno_interne));
	  fscanf(fgeom,"%s%s%ld",ch,ch,&(sdparall->nbno_front));
	  fscanf(fgeom,"%s%s%ld",ch,ch,&(sdparall->nbno_global));
	}

      /* C$ RUBRIQUE = PARALLELISME - NFOISFRONT */
      jysuis=NULL;
      do  {fgets(ch,90,fgeom); jysuis=strstr(ch,"RUBRIQUE =  PARALLELISME - NFOISFRONT");} while (!jysuis); 
      fgets(ch,90,fgeom);

      sdparall->nfoisfront=(rp_int*)malloc(sdparall->nbno_front*sizeof(rp_int));
      nv=sdparall->nbno_front/12;
      if (POST_TYPEWIDTH == 32)
	{
	  for (i=0;i<nv*12;i+=12) 
	    fscanf(fgeom,"%d%d%d%d%d%d%d%d%d%d%d%d\n",
		   sdparall->nfoisfront+i,  sdparall->nfoisfront+i+1,sdparall->nfoisfront+i+2,
		   sdparall->nfoisfront+i+3,sdparall->nfoisfront+i+4,sdparall->nfoisfront+i+5,
		   sdparall->nfoisfront+i+6,sdparall->nfoisfront+i+7,sdparall->nfoisfront+i+8,
		   sdparall->nfoisfront+i+9,sdparall->nfoisfront+i+10,sdparall->nfoisfront+i+11);
	  for (i=nv*12;i<sdparall->nbno_front;i++) fscanf(fgeom,"%d",sdparall->nfoisfront+i);
	}
      else
	{
	  for (i=0;i<nv*12;i+=12) 
	    fscanf(fgeom,"%ld%ld%ld%ld%ld%ld%ld%ld%ld%ld%ld%ld\n",
		   sdparall->nfoisfront+i,  sdparall->nfoisfront+i+1,sdparall->nfoisfront+i+2,
		   sdparall->nfoisfront+i+3,sdparall->nfoisfront+i+4,sdparall->nfoisfront+i+5,
		   sdparall->nfoisfront+i+6,sdparall->nfoisfront+i+7,sdparall->nfoisfront+i+8,
		   sdparall->nfoisfront+i+9,sdparall->nfoisfront+i+10,sdparall->nfoisfront+i+11);
	  for (i=nv*12;i<sdparall->nbno_front;i++) fscanf(fgeom,"%ld",sdparall->nfoisfront+i);
	}
/*       for (i=0; i<sdparall->nbno_front;i++) */
/* 	printf("i=%d sdparall->nfoisfront=%d\n",i,sdparall->nfoisfront[i]); */


      /* C$ RUBRIQUE = PARALLELISME  - NOMBRE DE NOEUDS COMMUNS AVEC LES AUTRES PARTITIONS */
      jysuis=NULL;
      do  {fgets(ch,90,fgeom); jysuis=strstr(ch,"RUBRIQUE = PARALLELISME  - NOMBRE DE NOEUDS COMMUNS AVEC LES AUTRES PART");} while (!jysuis); 
      fgets(ch,90,fgeom);

      sdparall->nbcommun=(rp_int*)malloc(sdparall->nparts*sizeof(rp_int));
      sdparall->adrcommun=(rp_int*)malloc(sdparall->nparts*sizeof(rp_int));
      nv=sdparall->nparts/6;
      if (POST_TYPEWIDTH == 32)
	{
	  for (i=0;i<nv*6;i+=6) 
	    fscanf(fgeom,"%d%d%d%d%d%d",
		   sdparall->nbcommun+i,  sdparall->nbcommun+i+1,sdparall->nbcommun+i+2,
		   sdparall->nbcommun+i+3,sdparall->nbcommun+i+4,sdparall->nbcommun+i+5);
	  for (i=nv*6;i<sdparall->nparts;i++) fscanf(fgeom,"%d",sdparall->nbcommun+i);
	}
      else
	{
	  for (i=0;i<nv*6;i+=6) 
	    fscanf(fgeom,"%ld%ld%ld%ld%ld%ld",
		   sdparall->nbcommun+i,  sdparall->nbcommun+i+1,sdparall->nbcommun+i+2,
		   sdparall->nbcommun+i+3,sdparall->nbcommun+i+4,sdparall->nbcommun+i+5);
	  for (i=nv*6;i<sdparall->nparts;i++) fscanf(fgeom,"%d",sdparall->nbcommun+i);
	}

      for (sdparall->adrcommun[0]=0,i=1;i<sdparall->nparts;i++)  
	sdparall->adrcommun[i]=sdparall->adrcommun[i-1]+sdparall->nbcommun[i-1];

      /* printf("nombre de noeuds commun avec les autres part\n");
      for (i=0;i<sdparall->nparts;i++) printf(" %d",sdparall->nbcommun[i]);printf("\n"); */

      /* C$ RUBRIQUE = PARALLELISME  - NOEUDS COMMUNS AVEC LES AUTRES PARTITIONS */
      jysuis=NULL;
      do  {fgets(ch,90,fgeom); jysuis=strstr(ch,"RUBRIQUE = PARALLELISME  - NOEUDS COMMUNS AVEC LES AUTRES PART");} while (!jysuis); 
      fgets(ch,90,fgeom);

      for (nb=0,i=0;i<sdparall->nparts;i++) nb+=sdparall->nbcommun[i];
      if (nb>0)
	{
	  sdparall->tcommun=(rp_int*)malloc(nb*sizeof(rp_int));
	  nv=nb/6;
	  if (POST_TYPEWIDTH == 32)
	    {
	      for (i=0;i<nv*6;i+=6) 
		fscanf(fgeom,"%d%d%d%d%d%d",
		       sdparall->tcommun+i,  sdparall->tcommun+i+1,sdparall->tcommun+i+2,
		       sdparall->tcommun+i+3,sdparall->tcommun+i+4,sdparall->tcommun+i+5);
	      for (i=nv*6;i<nb;i++) fscanf(fgeom,"%d",sdparall->tcommun+i);
	    }
	  else
	    {
	      for (i=0;i<nv*6;i+=6) 
		fscanf(fgeom,"%ld%ld%ld%ld%ld%ld",
		       sdparall->tcommun+i,  sdparall->tcommun+i+1,sdparall->tcommun+i+2,
		       sdparall->tcommun+i+3,sdparall->tcommun+i+4,sdparall->tcommun+i+5);
	      for (i=nv*6;i<nb;i++) fscanf(fgeom,"%ld",sdparall->tcommun+i);
	    }
	  for (i=0;i<nb;i++) sdparall->tcommun[i]--;
	}
/*       printf("liste des noeuds communs avec les autres part\n"); */
/*       for (i=0;i<nb;i++) printf(" %d",sdparall->tcommun[i]);printf("\n"); */


      /* C$ RUBRIQUE = NUMEROS GLOBAUX DES NOEUDS */
      jysuis=NULL;
      do  {fgets(ch,90,fgeom); jysuis=strstr(ch,"RUBRIQUE = NUMEROS GLOBAUX DES NOEUDS");} while (!jysuis); 
      fgets(ch,90,fgeom);

      sdparall->npglob=(rp_int*)malloc(maillnodes->npoin*sizeof(rp_int));
      nv=maillnodes->npoin/6;
      if (POST_TYPEWIDTH == 32)
	{
	  for (i=0;i<nv*6;i+=6) 
	    fscanf(fgeom,"%d%d%d%d%d%d",
		   sdparall->npglob+i,  sdparall->npglob+i+1,sdparall->npglob+i+2,
		   sdparall->npglob+i+3,sdparall->npglob+i+4,sdparall->npglob+i+5);
	  for (i=nv*6;i<maillnodes->npoin;i++) fscanf(fgeom,"%d",sdparall->npglob+i);
	}
      else
	{
	  for (i=0;i<nv*6;i+=6) 
	    fscanf(fgeom,"%ld%ld%ld%ld%ld%ld",
		   sdparall->npglob+i,  sdparall->npglob+i+1,sdparall->npglob+i+2,
		   sdparall->npglob+i+3,sdparall->npglob+i+4,sdparall->npglob+i+5);
	  for (i=nv*6;i<maillnodes->npoin;i++) fscanf(fgeom,"%ld",sdparall->npglob+i);
	}
      for (i=0;i<maillnodes->npoin;i++) sdparall->npglob[i]--;


    }/* fin du parallelisme */


  if (SYRTHES_LANG == FR)
    {
      printf("                           |--------------------|------------------|\n");
      printf("                           | Maillage volumique | Maillage de bord |\n");
      printf("      ---------------------|--------------------|------------------|\n");
      printf("      | Dimension          |    %8d        |    %8d      |\n",maillnodes->ndim,maillnodebord->ndim);
      printf("      | Nombre de noeuds   |    %8d        |     inusite      |\n",maillnodes->npoin);
      printf("      | Nombre d'elements  |    %8d        |    %8d      |\n",maillnodes->nelem,maillnodebord->nelem);
      printf("      | Nb noeuds par elt  |    %8d        |    %8d      |\n",maillnodes->ndmat,maillnodebord->ndmat);
      printf("      ---------------------|--------------------|------------------|\n");
    }
  else if (SYRTHES_LANG == EN)
    {
      printf("                           |--------------------|------------------|\n");
      printf("                           |   Volumic mesh     |  Boundary mesh   |\n");
      printf("      ---------------------|--------------------|------------------|\n");
      printf("      | Dimension          |    %8d        |    %8d      |\n",maillnodes->ndim,maillnodebord->ndim);
      printf("      | Number of nodes    |    %8d        |     unused      |\n",maillnodes->npoin);
      printf("      | Number of elements |    %8d        |    %8d      |\n",maillnodes->nelem,maillnodebord->nelem);
      printf("      | Nb nodes per elt   |    %8d        |    %8d      |\n",maillnodes->ndmat,maillnodebord->ndmat);
      printf("      ---------------------|--------------------|------------------|\n");
    }

  fclose(fgeom);

}


