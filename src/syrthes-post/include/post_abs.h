#ifndef _ABS_H_
#define _ABS_H_

#define min(a,b) (a<b  ? a:b)
#define max(a,b) (a>b  ? a:b)
#define abs(a)   (a>=0 ? a:-(a))

#endif    
