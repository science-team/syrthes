#ifndef _POSTBD_H
#define _POSTBD_H

#include "post_usertype.h"

struct SDparall
{
  rp_int nparts,rang;
  rp_int nbele;                 /* nbre d'elements                                         */
  rp_int nbno_interne;          /* nbre de noeuds internes                                 */
  rp_int nbno_front;            /* nbre de noeuds frontiere                                */
  rp_int nbno_global;           /* nbre de noeuds global (sur le maill entier)             */
  rp_int *nfoisfront;           /* nbre part auxquelles appartient un noeud frontiere      */
  rp_int *npglob;               /* numero global des noeuds                                */

  rp_int *nbcommun;             /* [npart] nombre de noeuds communs avec les autres part   */
  rp_int *adrcommun;            /* numero de depart des noeuds de la part i dans tcommun   */
  rp_int *tcommun;              /* numeros de noeuds communs                               */
} ;



#define MAX_REF 100
struct Maillage
{
  rp_int ndim,ndiele,nelem,npoin,ndmat,nbface;
  rp_int ncoema,iaxisy;
  double **coord,*volume,**xnf;
  rp_int **node;
  rp_int *nref,*nrefe,*type;
  rp_int **nvoisin; /* seulement pour les mst */
  rp_int nbarete,**arete, **eltarete;   /* seulement pour maillnodes */
};



struct Cperio
{
  rp_int existglob;
  rp_int nelem,ndmat,npoin;
  rp_int *nump,*numc;
  double *trav;
};


#endif
