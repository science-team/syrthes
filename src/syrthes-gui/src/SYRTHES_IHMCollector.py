# -*- coding: utf-8 -*-
#SYRTHES_IHMCollector.py


import os
import sys
#from PyQt5.QtCore import *
#from PyQt5.QtGui import *
#from PyQt5.QtWidgets import *

from syrthesIHMContext import syrthesIHMContext
from savingtools import boolFilledTable, saveTable, boolFilledRow, boolEmptyRow

from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSignal

from ui_Advanced_mode_form import Ui_Advanced_mode_form # classe de la vue du mode Advanced - accept all commands in the file .syd
from ui_Boundary_conditions_cond_form import Ui_Boundary_conditions_cond_form #classe de la vue des conditions aux limites en conduction
from ui_Boundary_conditions_rad_form import Ui_Boundary_conditions_rad_form # classe de la vue des conditions aux limites en rayonnement
from ui_Boundary_conditions_TPv_form import Ui_Boundary_conditions_TPv_form # classe de la vue des conditions aux limites en humidités modèles 2 équations
from ui_Boundary_conditions_TPvPt_form import Ui_Boundary_conditions_TPvPt_form # classe de la vue des conditions aux limites en humidités modèles 3 équations

from ui_Conjugate_heat_transfer_form import Ui_Conjugate_heat_transfer_form # classe de la vue du transfert de chaleur conjugué
from ui_Control_form import Ui_Control_form #classe de la vue du contrôle des pas de temps
from ui_Fake_form import Ui_Fake_form # classe leurre
from ui_Filename_form import Ui_Filename_form #classe de la vue des nom de fichier
from ui_Home_form import Ui_Home_form
from ui_Initial_conditions_cond_form import Ui_Initial_conditions_cond_form # classe de la vue des conditions initiales en conduction
from ui_Initial_conditions_hum_TPv_form import Ui_Initial_conditions_hum_TPv_form # classe cde la vue des conditions initiiales en humidités modèles 2 équations
from ui_Initial_conditions_hum_TPvPt_form import Ui_Initial_conditions_hum_TPvPt_form # classe cde la vue des conditions initiiales en humidités modèles 3 équations
from ui_Material_humidity_properties_2D_form import Ui_Material_humidity_properties_2D_form  # classe de la vue des propriétes des matériaux en humidité
from ui_Material_humidity_properties_3D_form import Ui_Material_humidity_properties_3D_form  # classe de la vue des propriétes des matériaux en humidité
from ui_Material_radiation_properties_form import Ui_Material_radiation_properties_form # classe de la vue des propriétés des matériaux en rayonnement
from ui_New_dialog import Ui_dialogNew # classe de la vue à l'ouverture de l'IHM et à l'activation du bouton "New"
from ui_Output_2D_form import Ui_Output_2D_form # classe de la vue des sorties en 2D
from ui_Output_3D_form import Ui_Output_3D_form # classe de la vue des sorties en 3D
from ui_Periodicity_2D_form import Ui_Periodicity_2D_form #classe de la vue de la périodicity en 2D
from ui_Periodicity_3D_form import Ui_Periodicity_3D_form #classe de la vue de la périodicity en 3D
from ui_Physical_prop_2D_form import Ui_Physical_prop_2D_form #classe de la vue des propriété physique en 2D
from ui_Physical_prop_3D_form import Ui_Physical_prop_3D_form #classe de la vue des propriété physique en 3D
from ui_Running_options_form import Ui_Running_options_form # classe de la vue des options de lancement
from ui_Solar_aspect_form import Ui_Solar_aspect_form # classe de la vue des aspects solaires
from ui_Spectral_parameters_form import Ui_Spectral_parameters_form # classe de la vue des paramètres spectraux
from ui_SyrthesMainwin80060023 import Ui_Syrthes_Mainwindow
from ui_User_C_function_form import Ui_User_C_function_form # Select an "User C function" for editing
from ui_View_factor_2D_form import Ui_View_factor_2D_form # classe de la vue des facteurs de forme en 2D
from ui_View_factor_3D_form import Ui_View_factor_3D_form # classe de la vue des facteurs de forme en 3D
from ui_Volumetric_conditions_cond_form import Ui_Volumetric_conditions_cond_form #classe de la vue des condition volumique en conduction
from ui_Volumetric_conditions_hum_TPv_form import Ui_Volumetric_conditions_hum_TPv_form  # classe de la vue des conditions volumqieus en humidités modèles 2 équations
from ui_Volumetric_conditions_hum_TPvPt_form import Ui_Volumetric_conditions_hum_TPvPt_form  # classe de la vue des conditions volumqieus en humidités modèles 3 équations
from ui_Contact_resistance_humidity_TPv_form import Ui_Contact_resistance_humidity_TPv_form
from ui_Contact_resistance_humidity_TPvPt_form import Ui_Contact_resistance_humidity_TPvPt_form
# fluid1d
from ui_Initial_conditions_fluid1d_form import Ui_Initial_conditions_fluid1d_form #
from ui_Boundary_conditions_fluid1d_3D_form import Ui_Boundary_conditions_fluid1d_3D_form  #
from ui_Physical_prop_fluid1d_form import Ui_Physical_prop_fluid1d_form #
from ui_Geometrie_fluid1d_form import Ui_Geometrie_fluid1d_form #
from ui_Volumetric_conditions_fluid1d_form import Ui_Volumetric_conditions_fluid1d_form #
from ui_Control_fluid1d_form import Ui_Control_fluid1d_form  #
#
# fluid0d
from ui_Boundary_conditions_fluid0d_form import Ui_Boundary_conditions_fluid0d_form
from ui_Physical_properties_fluid0d_form import Ui_Physical_properties_fluid0d_form
from ui_Geometrie_fluid0d_form import Ui_Geometrie_fluid0d_form #
from ui_Volumetric_conditions_fluid0d_form import Ui_Volumetric_conditions_fluid0d_form

#from SyrthesInSalome import SALOME_Selector

class Advanced_mode_formImpl(QtWidgets.QDialog, Ui_Advanced_mode_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Advanced_mode_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Advanced_mode_formHandler(Advanced_mode_formImpl):
    def __init__(self, parent=None):
        Advanced_mode_formImpl.__init__(self, parent)

    def save(self, savfil): # fonction de sauvegarde des mots clés uniquement compréhensibles par SYRTHES
        keyWord = ['']
        Table = self.Advanced_cmd_table
        nbRef = 0 # number of references
        boolCombo = False # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)

#------------------------------------------------------------------------------------------------------

class Boundary_conditions_cond_formImpl(QtWidgets.QDialog, Ui_Boundary_conditions_cond_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Boundary_conditions_cond_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Boundary_conditions_cond_formHandler(Boundary_conditions_cond_formImpl):
    def __init__(self, parent=None):
        Boundary_conditions_cond_formImpl.__init__(self, parent)

    def save(self, savfil): # fonction de la sauvegarde des données des conditions aux limites en conduction
        tableList = [self.Heat_ex_table,
                     self.Cont_res_table,
                     self.Flux_cond_table,
                     self.Diric_cond_table,
                     self.Inf_rad_table]

        # list de keywords for each table
        correspKeyWord_Heat_ex=['CLIM_T= COEF_ECH ','CLIM_T_FCT= COEF_ECH ', 'CLIM_T_PROG= COEF_ECH ']
        correspKeyWord_Cont_res=['CLIM_T= RES_CONTACT ','CLIM_T_FCT= RES_CONTACT ', 'CLIM_T_PROG= RES_CONTACT ']
        correspKeyWord_Flux_cond=['CLIM_T= FLUX ','CLIM_T_FCT= FLUX ', 'CLIM_T_PROG= FLUX ']
        correspKeyWord_Diric_cond=['CLIM_T= DIRICHLET ','CLIM_T_FCT= DIRICHLET ', 'CLIM_T_PROG= DIRICHLET ']
        correspKeyWord_Inf_rad=['CLIM_T= RAY_INFINI ','CLIM_T_FCT= RAY_INFINI ', 'CLIM_T_PROG= RAY_INFINI ']

        # list of lists of keywords, with the same order as in tableList
        correspKeyWordList = [correspKeyWord_Heat_ex, correspKeyWord_Cont_res, correspKeyWord_Flux_cond, correspKeyWord_Diric_cond, correspKeyWord_Inf_rad]

        nbRefList = [1, 2, 1, 1, 1] # list of number of references, with the same order as in tableList
        boolCombo = True # the combobox exists in the 2nd column

        for k in range(len(tableList)):
            Table = tableList[k]
            keyWord= correspKeyWordList[k]
            nbRef = nbRefList[k]
            saveTable(Table, keyWord, savfil, nbRef, boolCombo) # calling a external function for every table


#------------------------------------------------------------------------------------------------------

class Boundary_conditions_rad_formImpl(QtWidgets.QDialog, Ui_Boundary_conditions_rad_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Boundary_conditions_rad_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Boundary_conditions_rad_formHandler(Boundary_conditions_rad_formImpl):
    def __init__(self, parent=None):
        Boundary_conditions_rad_formImpl.__init__(self, parent)

    def save(self, savfil):
        if str(self.Bcr_Rf_te.toPlainText())!='': # Détection d'explications sur les références couplé conduction/rayonnement
            text=self.Bcr_Rf_te.toPlainText()
            line=str(text).splitlines(True)
            i=0
            while i<len(line): # boucle sur le champ d'édition de texte d'explications sur les références couplé conduction/rayonnement
                savfil.write("/CC ") # Ecriture du mot clé des explications sur les références couplé  conduction/rayonnement
                savfil.write(line[i])
                i=i+1
            savfil.write("\n")

        if str(self.Bcr_Rf_lne.text())!='': # Détection de références couplé conduction/rayonnement
            savfil.write("CLIM_RAYT= COUPLAGE_CONDUCTION ") # Ecriture du mot clé CLIM_RAYT= COUPLAGE_CONDUCTION
            savfil.write(self.Bcr_Rf_lne.text())
            savfil.write("\n")

        if str(self.Bcr_Scf_te.toPlainText())!='': # Détection d'explications sur les références couplé rayonnement/conduction
            text=self.Bcr_Scf_te.toPlainText()
            line=str(text).splitlines(True)
            i=0
            while i<len(line):
                savfil.write("/CR ") # Ecriture du mot clé des explications sur les références couplé  rayonnement/conduction
                savfil.write(line[i])
                i=i+1
            savfil.write("\n")

        if str(self.Bcr_Scf_lne.text())!='': # Détection de références couplé rayonnement/conduction
            savfil.write("CLIM= COUPLAGE_RAYONNEMENT ") # Ecriture du mot clé CLIM= COUPLAGE_RAYONNEMENT
            savfil.write(self.Bcr_Scf_lne.text())
            savfil.write("\n")


        Table=self.Bcr_Irt_table
        keyWord = ['CLIM_RAYT= TEMPERATURE_IMPOSEE ']
        nbRef = 1 # number of references
        boolCombo = False # the combobox doesn't exist in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)

        Table=self.Bcr_Irf_table
        keyWord = ['CLIM_RAYT= FLUX_IMPOSE_PAR_BANDE ']
        nbRef = 1 # number of references
        boolCombo = False # the combobox doesn't exist in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)

        savfil.write("DOMAINE DE RAYONNEMENT CONFINE OUVERT SUR L EXTERIEUR= ") # Ecriture du mot clé DOMAINE DE RAYONNEMENT CONFINE OUVERT SUR L EXTERIEUR
        if self.Bcr_Rpa_chb.isChecked():
            savfil.write("OUI")
            savfil.write("\n")
        else:
            savfil.write("NON")
            savfil.write("\n")
        if self.Bcr_Rpa_le.text()!='': # Détection de la présence de température infinis
            savfil.write("RAYT= TEMPERATURE_INFINI ") # Ecriture du mot clé RAYT= TEMPERATURE_INFINI
            savfil.write(self.Bcr_Rpa_le.text())
            savfil.write("\n")

#------------------------------------------------------------------------------------------------------

class Boundary_conditions_TPv_formImpl(QtWidgets.QDialog, Ui_Boundary_conditions_TPv_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Boundary_conditions_TPv_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Boundary_conditions_TPv_formHandler(Boundary_conditions_TPv_formImpl):
    def __init__(self, parent=None):
        Boundary_conditions_TPv_formImpl.__init__(self, parent)

    def save(self, savfil):
        Table = self.Bc_TPv_table
        # keywords for the table
        keyWord = ['CLIM_HMT= HH ', 'CLIM_HMT_FCT= HH ', 'CLIM_HMT_PROG= HH ']

        nbRef = 1 # number of reference fields, with the same order as in tableList
        boolCombo = True # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo) # calling a external function for every table
#------------------------------------------------------------------------------------------------------

class Boundary_conditions_TPvPt_formImpl(QtWidgets.QDialog, Ui_Boundary_conditions_TPvPt_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Boundary_conditions_TPvPt_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Boundary_conditions_TPvPt_formHandler(Boundary_conditions_TPvPt_formImpl):
    def __init__(self, parent=None):
        Boundary_conditions_TPvPt_formImpl.__init__(self, parent)

    def save(self, savfil):
        # Détection du type de modèle (2, 3 équations) pour sélection du tableau correspondant
        Table = self.Bc_TPvPt_table
        # keywords for the table
        keyWord = ['CLIM_HMT= HHH ', 'CLIM_HMT_FCT= HHH ', 'CLIM_HMT_PROG= HHH ']

        nbRef = 1 # number of reference fields, with the same order as in tableList
        boolCombo = True # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo) # calling a external function for every table

#------------------------------------------------------------------------------------------------------

class Conjugate_heat_transfer_formImpl(QtWidgets.QDialog, Ui_Conjugate_heat_transfer_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Conjugate_heat_transfer_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Conjugate_heat_transfer_formHandler(Conjugate_heat_transfer_formImpl):
    def __init__(self, parent=None):
        Conjugate_heat_transfer_formImpl.__init__(self, parent)

    def save(self, savfil): # fonction de sauvegarde des transfert de chaleur conjugué
        tableList = [self.Cht_Sc_table, self.Cht_Vc_table] #list of tables
        correspKeyWodList = [['CLIM= COUPLAGE_SURF_FLUIDE'], ['CLIM= COUPLAGE_VOL_FLUIDE']]
        nbRefList = [1, 1] # list of number of reference fields, with the same order as in tableList
        boolCombo = False # the combobox doesn't exist in the 2nd column

        for k in range(len(tableList)):
            Table = tableList[k]
            keyWord= correspKeyWodList[k]
            nbRef = nbRefList[k]
            saveTable(Table, keyWord, savfil, nbRef, boolCombo)

#------------------------------------------------------------------------------------------------------

class Control_formImpl(QtWidgets.QDialog, Ui_Control_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Control_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Control_formHandler(Control_formImpl):
    def __init__(self, parent=None):
        Control_formImpl.__init__(self, parent)

    def save(self, savfil):
        savfil.write("SUITE DE CALCUL= ") # Ecriture du mot clé SUITE DE CALCUL
        if self.Ch_res_cal.isChecked()== True: # Détection d'un suivis de calcul
            savfil.write("OUI\n")
        else:
            savfil.write("NON\n")
        savfil.write("/\n")
        savfil.write("/ Pas de temps\n")
        savfil.write("/---------------\n")

        if self.Ch_Sr_cb.isChecked()==True: # Détection d'un nouveau temps initial
            if self.lineEdit_39.text() != '' :
                savfil.write("SUITE : NOUVEAU TEMPS INITIAL= ") # Ecriture du mot clé SUITE : NOUVEAU TEMPS INITIAL
                savfil.write(self.lineEdit_39.text())
                savfil.write("\n")

        if self.Le_Nts.text()!='': # Détection du nombre de pas de temp solide
            savfil.write("NOMBRE DE PAS DE TEMPS SOLIDES= ") # Ecriture du mot clé NOMBRE DE PAS DE TEMPS SOLIDES
            savfil.write(self.Le_Nts.text())
            savfil.write("\n")

        if self.comb_time_st.currentIndex()==0: # Détection du type de pas de temps (pas de temps constant)
            if self.Le_const_Ts.text()!='':
                savfil.write("PAS DE TEMPS SOLIDE= ") # Ecriture du mot clé PAS DE TEMPS SOLIDE
                savfil.write(self.Le_const_Ts.text())
                savfil.write("\n")
        elif self.comb_time_st.currentIndex()==1: # Détection du type de pas de temps (pas de temps automatique)
            savfil.write("PAS DE TEMPS SOLIDE= ") # Ecriture du mot clé PAS DE TEMPS SOLIDE
            savfil.write(self.Le_auto_It.text())
            savfil.write("\n")
            savfil.write("PAS DE TEMPS AUTOMATIQUE= ") # Ecriture du mot clé PAS DE TEMPS AUTOMATIQUE
            savfil.write(self.Le_auto_Mt.text())
            savfil.write(" ")

            if not self.Pv_Pt_var_gb.isHidden() :
                if self.Le_auto_Mpv.text() != '': # Détection de la présence de modèles d'humidité 3 équations
                    savfil.write(self.Le_auto_Mpv.text())
                    savfil.write(" ")
                    pass
                    if self.Le_auto_Mpt.isEnabled() and self.Le_auto_Mpt.text() != '': # Détection de la présence de modèles d'humidité 3 équations
                        savfil.write(self.Le_auto_Mpt.text())
                        savfil.write(" ")
                        pass
                    pass
                pass
            savfil.write(self.Le_auto_Mts.text())
            savfil.write("\n")


        elif self.comb_time_st.currentIndex()==2: # Détection du type de pas de temps (pas de temps multiple)
            Table = self.By_Block_table
            #Table.setCurrentCell(0, 0) # Positionnement du tableau des conditions initial à la première case à exploiter (?)

            for i in range(Table.rowCount()):
                check = boolFilledRow(Table, i, 0)
                if check :
                    if self.By_Block_table.item(i, 2)!=None:
                          savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                          Item=self.By_Block_table.item(i, 2)
                          savfil.write(Item.text())
                          savfil.write("\n")
                    savfil.write("PAS DE TEMPS MULTIPLES= ") # Ecriture du mot clé PAS DE TEMPS MULTIPLES
                    Item=self.By_Block_table.item(i, 0)
                    savfil.write(Item.text())
                    savfil.write(" ")
                    Item=self.By_Block_table.item(i, 1)
                    savfil.write(Item.text())
                    savfil.write("\n")
                if (not boolFilledRow(Table, i, 0)) and (not boolEmptyRow(Table, i, 0)) :
                    # not filled but not empty
                    syrthesIHMContext.notFullyFilledException = True
            i=0

        savfil.write("NOMBRE ITERATIONS SOLVEUR TEMPERATURE= ") # Ecriture du mot clé NOMBRE ITERATIONS SOLVEUR TEMPERATURE
        savfil.write(self.Le_Mni.text())
        savfil.write("\n")
        savfil.write("PRECISION POUR LE SOLVEUR TEMPERATURE= ") # Ecriture du mot clé PRECISION POUR LE SOLVEUR TEMPERATURE
        savfil.write(self.lineEdit_43.text())
        savfil.write("\n")
        if not self.Vt_Vap_gb.isHidden() :
            savfil.write("NOMBRE ITERATIONS SOLVEUR PRESSION VAPEUR= ") # Ecriture du mot clé NOMBRE ITERATIONS SOLVEUR PRESSION VAPEUR
            savfil.write(self.Vap_Mn_le.text())
            savfil.write("\n")
            savfil.write("PRECISION POUR LE SOLVEUR PRESSION VAPEUR= ") # Ecriture du mot clé PRECISION POUR LE SOLVEUR PRESSION VAPEUR
            savfil.write(self.Vap_Sp_le.text())
            savfil.write("\n")
            if self.Ap_Mn_le.isEnabled() : # Détection de la présence de modèles d'humidité 3 équations
                savfil.write("NOMBRE ITERATIONS SOLVEUR PRESSION TOTALE= ") # Ecriture du mot clé NOMBRE ITERATIONS SOLVEUR PRESSION TOTALE
                savfil.write(self.Ap_Mn_le.text())
                savfil.write("\n")
                savfil.write("PRECISION POUR LE SOLVEUR PRESSION TOTALE= ") # Ecriture du mot clé PRECISION POUR LE SOLVEUR PRESSION TOTALE
                savfil.write(self.Ap_Sp_le.text())
                savfil.write("\n")

#------------------------------------------------------------------------------------------------------

class DialogNewImpl(QtWidgets.QDialog, Ui_dialogNew):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_dialogNew.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class DialogNewHandler(DialogNewImpl): # with modification
    newRejected = pyqtSignal()
    welcomeRejected = pyqtSignal()

    def __init__(self, parent=None):
        DialogNewImpl.__init__(self, parent)
        self.setModal(True)

        # Renvoie la taille de l'écran. Cette ligne nous permettra par la suite de connaître la hauteur et la largeur de l'écran.
        size_ecran = QtWidgets.QDesktopWidget().screenGeometry()
        # Même chose que ci-dessus mais avec la fenêtre de l'application.
        size_fenetre = self.geometry()
        # La fonction move() permet de déplacer la fenêtre aux coordonnées passées en arguments.
        self.move((size_ecran.width()-size_fenetre.width())//2,
                  (size_ecran.height()-size_fenetre.height()//2))

        ## self.connect(self, SIGNAL("rejected()"), self.myRejected)
        self.rejected.connect(self.myRejected)

    def myRejected(self):
        if self.windowTitle() == "New" :
            self.newRejected.emit()
            ## self.emit(SIGNAL("newRejected"))
        else :
            self.welcomeRejected.emit()
            ## self.emit(SIGNAL("welcomeRejected"))

#------------------------------------------------------------------------------------------------------

class Fake_formImpl(QtWidgets.QDialog, Ui_Fake_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Fake_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Fake_formHandler(Fake_formImpl):
    def __init__(self, parent=None):
        Fake_formImpl.__init__(self, parent)

#------------------------------------------------------------------------------------------------------

class Filename_formImpl(QtWidgets.QDialog, Ui_Filename_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Filename_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Filename_formHandler(Filename_formImpl):
    def __init__(self, parent=None):
        Filename_formImpl.__init__(self, parent)

    def save(self, savfil): # fonction de sauvegarde des noms de fichier
        if self.Fn_Cd_lne.text()!='': # Détection de la saisie d'un maillage de conduction
            savfil.write("MAILLAGE CONDUCTION= ") # Ecriture du mot clé MAILLAGE CONDUCTION=
            savfil.write(self.Fn_Cd_lne.text())
            savfil.write("\n")

        if self.Fn_Rm_lne.text()!='': # Détection de la saisie d'un maillage de rayonnement
            savfil.write("MAILLAGE RAYONNEMENT= ") # Ecriture du mot clé MAILLAGE RAYONNEMENT=
            savfil.write(self.Fn_Rm_lne.text())
            savfil.write("\n")

        if self.Fn_fluid1d_lne.text()!='': # Détection de la saisie d'un maillage FLUIDE 1D
            savfil.write("MAILLAGE FL1D= ") # Ecriture du mot clé MAILLAGE FL1D=
            savfil.write(self.Fn_fluid1d_lne.text())
            savfil.write("\n")

        if self.Fn_Rs_lne.text()!='' and self.Fn_Rs_lne.isEnabled() : # Détection d'un préfixe résultat pour suite de calcul
# isa            savfil.write("PREFIXE DU RESULTAT PRECEDENT POUR SUITE DE CALCUL= ") # Ecriture du mot clé PREFIXE DU RESULTAT PRECEDENT POUR SUITE DE CALCUL=
            savfil.write("RESULTAT PRECEDENT POUR SUITE DE CALCUL= ") # Ecriture du mot clé PREFIXE DU RESULTAT PRECEDENT POUR SUITE DE CALCUL=
            savfil.write(self.Fn_Rs_lne.text())
            savfil.write("\n")

        if self.Fn_Mt_lne.text()!='': # Détection de la saisie dun fichier météo
            savfil.write("FICHIER METEO= ") # Ecriture du mot clé FICHIER METEO=
            savfil.write(self.Fn_Mt_lne.text())
            savfil.write("\n")

        if self.Fn_Rnp_lne.text()!='': # Détection d'un préfixe de fichier résultat
            savfil.write("PREFIXE DES FICHIERS RESULTATS= ") # Ecriture du mot clé PREFIXE DES FICHIERS RESULTATS=
            savfil.write(self.Fn_Rnp_lne.text())
            savfil.write("\n")

    def PublishInSalome(self, caseName, caseRep):
        print("PublishInSalome ", caseName, caseRep)
        from SYRTHESGUI import publishCase
        #  convertir les QString en Python string avec str()
        caseName = str(caseName)
        meshCond = str(self.Fn_Cd_lne.text())
        meshRay = str(self.Fn_Rm_lne.text())
        if meshCond != '':
            meshCond = os.path.join(caseRep, meshCond) # when meshCond is an aboslute path, join() returns meshCond
            print("conduction", meshCond)
            if os.path.exists(meshCond):
                # si il y a un fichier .med de path identique, le publier
                meshMed = os.path.splitext(meshCond)[0] + '.med'
                if os.path.exists(meshMed):
                    meshCond = meshMed
                    pass
            else:
                QtWidgets.QMessageBox.information(self, "Error message", meshCond + " not found")
                meshCond = ''
                return
        if meshRay != '':
            meshRay = os.path.join(caseRep, meshRay) # when meshRay is an aboslute path, join() returns meshRay
            print("rayonnement", meshRay)
            if os.path.exists(meshRay):
                # si il y a un fichier .med de path identique, le publier
                meshMed = os.path.splitext(meshRay)[0] + '.med'
                if os.path.exists(meshMed):
                    meshRay = meshMed
                    pass
            else:
                QtWidgets.QMessageBox.information(self, "Error message", meshRay + " not found")
                meshRay = ''
                return
        # supprimer le cas dans l'object browser, le recréer
        try :
            publishCase(caseName, os.path.join(caseRep,meshCond), os.path.join(caseRep,meshRay))
        except :
            QtWidgets.QMessageBox.information(self, "Message", "Mesh filename does not exist")

#------------------------------------------------------------------------------------------------------

class Home_formImpl(QtWidgets.QDialog, Ui_Home_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Home_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Home_formHandler(Home_formImpl):
    def __init__(self, parent=None):
        Home_formImpl.__init__(self, parent)

    def save(self, savfil) : # fonction de sauvegarde des données de la vue principale
        i=0
        if self.lineEdit_7.text()!='': # Détection d'un titre d'étude
            savfil.write("TITRE ETUDE= ") # Ecriture du mot clé TITRE ETUDE
            savfil.write(self.lineEdit_7.text())
            savfil.write("\n")
        text=self.Ho_Ds_te.toPlainText()
        if text!='': # Détection des commentaires de l'étude
            line=str(text).splitlines(True)
            while i<len(line):
                savfil.write("// ") # Ecriture des commentaires de l'étude
                savfil.write(line[i])
                i=i+1
            if line[i-1].find("\n")==-1:
                savfil.write("\n")
            i=0
        savfil.write("DIMENSION DU PROBLEME= ") # Ecriture du mot clé DIMENSION DU PROBLEME
        savfil.write(str(self.Dim_Comb.currentText()).upper())
        savfil.write("\n")
        savfil.write("/\n")
        savfil.write("/ Rayonnement\n")
        savfil.write("/------------\n")
        savfil.write("PRISE EN COMPTE DU RAYONNEMENT CONFINE= ") # Ecriture du mot clé PRISE EN COMPTE DU RAYONNEMENT CONFINE
        if self.Ho_Tr_ch.isChecked()== True: # Détection de l'état de la prise en compte du transfert d'humidité
            savfil.write("OUI\n")
        else:
            savfil.write("NON\n")
        savfil.write("/\n")
        savfil.write("/ Fluide 1D\n")
        savfil.write("/------------\n")
        savfil.write("MODELE 1D FLUIDE= ") # Ecriture du mot clé MODELE 1D FLUIDE
        if self.Ho_fluid1d_ch.isChecked()== True: # Détection de l'état de la prise en compte du modele 1D fluide
            savfil.write("OUI\n")
        else:
            savfil.write("NON\n")
        savfil.write("/\n")
        savfil.write("/ Fluide 0D\n")
        savfil.write("/------------\n")
        savfil.write("MODELE 0D FLUIDE= ") # Ecriture du mot clé MODELE 1D FLUIDE
        if self.Ho_fluid0d_ch.isChecked()== True: # Détection de l'état de la prise en compte du modele 1D fluide
            savfil.write("OUI\n")
        else:
            savfil.write("NON\n")
        savfil.write("/\n")
        savfil.write("/Transferts couples\n")
        savfil.write("/------------------\n")
        savfil.write("MODELISATION DES TRANSFERTS D HUMIDITE= ") # Ecriture du mot clé MODELISATION DES TRANSFERTS D HUMIDITE
        if self.Ho_Hm_ch.isChecked()==True:
            if self.Hm_cmb.currentIndex()==0:
                savfil.write("2\n")
            else:
                savfil.write("3\n")
        else:
            savfil.write("0\n")
        savfil.write("/\n")

#------------------------------------------------------------------------------------------------------

class Initial_conditions_cond_formImpl(QtWidgets.QDialog, Ui_Initial_conditions_cond_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Initial_conditions_cond_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Initial_conditions_cond_formHandler(Initial_conditions_cond_formImpl):
    def __init__(self, parent=None):
        Initial_conditions_cond_formImpl.__init__(self, parent)

    def save(self, savfil) :
        keyWord = ['CINI_T=','CINI_T_FCT=', 'CINI_T_PROG=']
        Table = self.Init_T_table
        nbRef = 1 # number of references
        boolCombo = True # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)
#------------------------------------------------------------------------------------------------------

class Initial_conditions_hum_TPv_formImpl(QtWidgets.QDialog, Ui_Initial_conditions_hum_TPv_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Initial_conditions_hum_TPv_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Initial_conditions_hum_TPv_formHandler(Initial_conditions_hum_TPv_formImpl):
    def __init__(self, parent=None):
        Initial_conditions_hum_TPv_formImpl.__init__(self, parent)

    def save(self, savfil) :
        Table=self.Vch_Ic_TPv_table
        #Table.setCurrentCell(0,3) # Positionnement du tableau des conditions initiales en humidité à la première case à exploiter

        for i in range(Table.rowCount()): # loop through rows
        # refresh "check" for a new loop
        # Choice of value in combobox (if any) 0 : CONT ; 1 : FUNC ; 2 : PROG ; by default : 0
        # and choice of begin column where the first value is supposed to be
            checkCombo = Table.cellWidget(i,1).currentIndex()
            beginCol = 3 # there is one combobox, table with 2 comboboxes can't be treated by this function
            if checkCombo == 2 : # combobox of type "Program"
                check = Table.item(i,Table.columnCount()-2)!=None and Table.item(i,Table.columnCount()-2).text()!=str('')
            else: # Constant or FCT or other type of combobox
                check = boolFilledRow(Table, i, beginCol) # check = true only if all cells are filled in

            if check : # if the line i is fully filled
                savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                if Table.item(i, 5)!=None:
                    Item=Table.item(i, 5)
                    savfil.write(Item.text())
                    savfil.write("\n")
                if Table.cellWidget(i, 0).isChecked()==False:
                    savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                if Table.cellWidget(i, 2).currentIndex()==0: # Détection du type d'équation (T)
                    savfil.write("CINI_T") # Ecriture du mot clé CINI_T
                elif Table.cellWidget(i, 2).currentIndex()==1: # Détection du type d'équation (PV)
                    savfil.write("CINI_PV") # Ecriture du mot clé CINI_PV
                else: # Détection du type d'équation (PT)
                    savfil.write("CINI_PT") # Ecriture du mot clé CINI_PT
                if Table.cellWidget(i, 1).currentIndex()==0: # Détection du type de donnée pour la ligne (constant)
                    savfil.write("= ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())

                elif Table.cellWidget(i, 1).currentIndex()==1: # Détection du type de donnée pour la ligne (fonction)
                    savfil.write("_FCT= ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                else: # Détection du type de donnée pour la ligne (sous-programme)
                    savfil.write("_PROG= ")
                    savfil.write(Table.item(i, 4).text())
                savfil.write("\n")
            if (not boolFilledRow(Table, i, beginCol)) and (not boolEmptyRow(Table, i, beginCol)) and checkCombo != 2:
                # not filled but not empty AND the row is not of type PROGRAM
                syrthesIHMContext.notFullyFilledException = True

#------------------------------------------------------------------------------------------------------

class Initial_conditions_hum_TPvPt_formImpl(QtWidgets.QDialog, Ui_Initial_conditions_hum_TPvPt_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Initial_conditions_hum_TPvPt_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Initial_conditions_hum_TPvPt_formHandler(Initial_conditions_hum_TPvPt_formImpl):
    def __init__(self, parent=None):
        Initial_conditions_hum_TPvPt_formImpl.__init__(self, parent)

    def save(self, savfil) :
        Table=self.Vch_Ic_TPvPt_table
        #Table.setCurrentCell(0,3) # Positionnement du tableau des conditions initiales en humidité à la première case à exploiter

        for i in range(Table.rowCount()): # loop through rows
        # refresh "check" for a new loop
        # Choice of value in combobox (if any) 0 : CONT ; 1 : FUNC ; 2 : PROG ; by default : 0
        # and choice of begin column where the first value is supposed to be
            checkCombo = Table.cellWidget(i,1).currentIndex()
            beginCol = 3 # there is one combobox, table with 2 comboboxes can't be treated by this function
            if checkCombo == 2 : # combobox of type "Program"
                check = Table.item(i,Table.columnCount()-2)!=None and Table.item(i,Table.columnCount()-2).text()!=str('')
            else: # Constant or FCT or other type of combobox
                check = boolFilledRow(Table, i, beginCol) # check = true only if all cells are filled in

            if check : # if the line i is fully filled
                savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                if Table.item(i, 5)!=None:
                    Item=Table.item(i, 5)
                    savfil.write(Item.text())
                    savfil.write("\n")
                if Table.cellWidget(i, 0).isChecked()==False:
                    savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                if Table.cellWidget(i, 2).currentIndex()==0: # Détection du type d'équation (T)
                    savfil.write("CINI_T") # Ecriture du mot clé CINI_T
                elif Table.cellWidget(i, 2).currentIndex()==1: # Détection du type d'équation (PV)
                    savfil.write("CINI_PV") # Ecriture du mot clé CINI_PV
                else: # Détection du type d'équation (PT)
                    savfil.write("CINI_PT") # Ecriture du mot clé CINI_PT
                if Table.cellWidget(i, 1).currentIndex()==0: # Détection du type de donnée pour la ligne (constant)
                    savfil.write("= ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())

                elif Table.cellWidget(i, 1).currentIndex()==1: # Détection du type de donnée pour la ligne (fonction)
                    savfil.write("_FCT= ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                else: # Détection du type de donnée pour la ligne (sous-programme)
                    savfil.write("_PROG= ")
                    savfil.write(Table.item(i, 4).text())
                savfil.write("\n")
            if (not boolFilledRow(Table, i, beginCol)) and (not boolEmptyRow(Table, i, beginCol)) and checkCombo != 2:
                # not filled but not empty AND the row is not of type PROGRAM
                syrthesIHMContext.notFullyFilledException = True

#------------------------------------------------------------------------------------------------------

class Material_humidity_properties_2D_formImpl(QtWidgets.QDialog, Ui_Material_humidity_properties_2D_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Material_humidity_properties_2D_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Material_humidity_properties_2D_formHandler(Material_humidity_properties_2D_formImpl):
    def __init__(self, parent=None):
        Material_humidity_properties_2D_formImpl.__init__(self, parent)

    def save(self, savfil) :
        #list of tables
        tableList = [self.Mhp_iso_2D_table, self.Mhp_aniso_2D_table]

        # list de keywords for each table
        correspKeyWord_Iso='HMT_MAT= '
        correspKeyWord_ANISO_2D = 'HMT_MAT_ANISO= '
        correspKeyWordList = [correspKeyWord_Iso, correspKeyWord_ANISO_2D]
        nbValue = [0,1]
        nbRef = 1 # number of references
        boolCombo = True # the combobox exists in the 2nd column

        for k in range(len(tableList)):
            Table = tableList[k]
            nv = nbValue[k]
            keyWord = correspKeyWordList[k]
            for i in range(Table.rowCount()): # loop through rows
                check = (Table.item(i, 2)!=None) and (Table.item(i, 2).text() != str(''))
                if check : # if the line i is fully filled
                    if Table.item(i, 2+nv+1)!=None:
                        savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                        Item=Table.item(i, 2+nv+1)
                        savfil.write(Item.text())
                        savfil.write("\n")
                    if Table.cellWidget(i, 0).isChecked()==False:
                        savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                    savfil.write(keyWord) # Ecriture du mot clé
                    savfil.write(Table.cellWidget(i, 1).currentText())
                    savfil.write(" ")
                    for j in range(nv):
                        savfil.write(Table.item(i, 2+j).text())
                        savfil.write(" ")
                    savfil.write(Table.item(i, 2+nv).text())
                    savfil.write("\n")
                    pass
                pass


#------------------------------------------------------------------------------------------------------

class Material_humidity_properties_3D_formImpl(QtWidgets.QDialog, Ui_Material_humidity_properties_3D_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Material_humidity_properties_3D_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Material_humidity_properties_3D_formHandler(Material_humidity_properties_3D_formImpl):
    def __init__(self, parent=None):
        Material_humidity_properties_3D_formImpl.__init__(self, parent)

    def save(self, savfil) :
        #list of tables
        tableList = [self.Mhp_iso_3D_table, self.Mhp_aniso_3D_table]

        # list de keywords for each table
        correspKeyWord_Iso='HMT_MAT= '
        correspKeyWord_ANISO_3D = 'HMT_MAT_ANISO= '
        correspKeyWordList = [correspKeyWord_Iso, correspKeyWord_ANISO_3D]
        nbValue=[0,9]
        nbRef = 1 # number of references
        boolCombo = True # the combobox exists in the 2nd column

        for k in range(len(tableList)):
            Table = tableList[k]
            nv = nbValue[k]
            keyWord = correspKeyWordList[k]
            for i in range(Table.rowCount()): # loop through rows
                check = (Table.item(i, 2)!=None) and (Table.item(i, 2).text() != str(''))
                if check : # if the line i is fully filled
                    if Table.item(i, 2+nv+1)!=None:
                        savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                        Item=Table.item(i, 2+nv+1)
                        savfil.write(Item.text())
                        savfil.write("\n")
                    if Table.cellWidget(i, 0).isChecked()==False:
                        savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                    savfil.write(keyWord) # Ecriture du mot clé
                    savfil.write(Table.cellWidget(i, 1).currentText())
                    savfil.write(" ")
                    for j in range(nv):
                        savfil.write(Table.item(i, 2+j).text())
                        savfil.write(" ")
                    savfil.write(Table.item(i, 2+nv).text())
                    savfil.write("\n")
                    pass
                pass

#------------------------------------------------------------------------------------------------------

class Contact_resistance_humidity_TPvPt_formImpl(QtWidgets.QDialog, Ui_Contact_resistance_humidity_TPvPt_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Contact_resistance_humidity_TPvPt_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)
        pass
    pass
class Contact_resistance_humidity_TPvPt_formHandler(Contact_resistance_humidity_TPvPt_formImpl):
    def __init__(self, parent=None):
        Contact_resistance_humidity_TPvPt_formImpl.__init__(self, parent)
        pass

    def save(self, savfil) :
        keyWord=['CLIM_HMT= RES_CONTACT ','CLIM_HMT_FCT= RES_CONTACT ', 'CLIM_HMT_PROG= RES_CONTACT ']
        Table = self.Cont_res_hum_TPvPt_table
        nbRef = 2 # number of references
        boolCombo = True # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)
        pass
    pass
#------------------------------------------------------------------------------------------------------

class Contact_resistance_humidity_TPv_formImpl(QtWidgets.QDialog, Ui_Contact_resistance_humidity_TPv_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Contact_resistance_humidity_TPv_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)
        pass
    pass
class Contact_resistance_humidity_TPv_formHandler(Contact_resistance_humidity_TPv_formImpl):
    def __init__(self, parent=None):
        Contact_resistance_humidity_TPv_formImpl.__init__(self, parent)
        pass

    def save(self, savfil) :
        keyWord=['CLIM_HMT= RES_CONTACT ','CLIM_HMT_FCT= RES_CONTACT ', 'CLIM_HMT_PROG= RES_CONTACT ']
        Table = self.Cont_res_hum_TPv_table
        nbRef = 2 # number of references
        boolCombo = True # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)
        pass
    pass

#------------------------------------------------------------------------------------------------------
# fluid1d

class Initial_conditions_fluid1d_formImpl(QtWidgets.QDialog, Ui_Initial_conditions_fluid1d_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Initial_conditions_fluid1d_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Initial_conditions_fluid1d_formHandler(Initial_conditions_fluid1d_formImpl):
    def __init__(self, parent=None):
        Initial_conditions_fluid1d_formImpl.__init__(self, parent)

    def save(self, savfil) :
        keyWord = ['CINI_FL1D_T= ','CINI_FL1D_T_FCT= ', 'CINI_FL1D_T_PROG= ']
        Table = self.Init_TV_table
        nbRef = 1 # number of references
        boolCombo = True # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)
        savfil.write("/\n")


#------------------------------------------------------------------------------------------------------

class Boundary_conditions_fluid1d_3D_formImpl(QtWidgets.QDialog, Ui_Boundary_conditions_fluid1d_3D_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Boundary_conditions_fluid1d_3D_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Boundary_conditions_fluid1d_3D_formHandler(Boundary_conditions_fluid1d_3D_formImpl):
    def __init__(self, parent=None):
        Boundary_conditions_fluid1d_3D_formImpl.__init__(self, parent)

    def save(self, savfil): # fonction de la sauvegarde des données des conditions aux limites fluid1d 3D

        # coupling

        if str(self.Bcfluid1d_1dfUc_lne.text())!='': # Détection d'explications sur les références couplé conduction/fluid1d
            text=self.Bcfluid1d_1dfUc_lne.text()
            line=str(text).splitlines(True)
            i=0
            while i<len(line): # boucle sur le champ d'édition de texte d'explications sur les références couplé conduction/fluid1d
                savfil.write("/CCFL1D ") # Ecriture du mot clé des explications sur les références couplé  conduction/fluid1d
                savfil.write(line[i])
                i=i+1
            savfil.write("\n")

        if str(self.Bcfluid1d_1df_lne.text())!='': # Détection de références couplées conduction/fluid1d
            savfil.write("CLIM_FL1D_T= COUPLAGE_CONDUCTION ") # Ecriture du mot clé CLIM_FL1D_T= COUPLAGE_CONDUCTION
            savfil.write(self.Bcfluid1d_1df_lne.text())
            savfil.write("\n")

        Table = self.Bcfluid1d_Coupling_table

        for i in range(Table.rowCount()):
        # refresh "check" for a new loop
        # Choice of value in combobox (if any) 0 : CONT ; 1 : FUNC ; 2 : PROG ; by default : 0
        # and choice of begin column where the first value is supposed to be

            checkCombo = Table.cellWidget(i,1).currentIndex()
            beginCol = 2
            if checkCombo == 2 or checkCombo == 0: # combobox of type "Program" ou H_COLBURN
                check = Table.item(i,3)!=None and Table.item(i,3).text()!=str('')
            else: # H_CONSTANT
                check = boolFilledRow(Table, i, beginCol) # check = true only if all cells are filled in

            if check : # if the line i is fully filled
                if Table.item(i, 4)!=None:
                    savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                    Item=Table.item(i, 4)
                    savfil.write(Item.text())
                    savfil.write("\n")
                if Table.cellWidget(i, 0).isChecked()==False:
                    savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                if Table.cellWidget(i, 1).currentIndex()==1: # Détection du type de donnée pour la ligne H_COLBURN
                    savfil.write("CLIM= COUPLAGE_FLUIDE_1D H_COLBURN ")
                    savfil.write(Table.item(i, 3).text())
                elif Table.cellWidget(i, 1).currentIndex()==0: # Détection du type de donnée pour la ligne H_CONSTANT
                    savfil.write("CLIM= COUPLAGE_FLUIDE_1D H_CONSTANT ")
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                else:
                    savfil.write("CLIM_PROG= COUPLAGE_FLUIDE_1D ") # Détection du type de donnée pour la ligne (sous-programme)
                    savfil.write(Table.item(i, 3).text())
                savfil.write("\n")

            if (not boolFilledRow(Table, i, beginCol)) and (not boolEmptyRow(Table, i, beginCol)) and checkCombo != 2 and checkCombo != 0:
                # not filled but not empty AND the row is not of type PROGRAM ou H_COLBURN
                syrthesIHMContext.notFullyFilledException = True

        # boundary conditions

        tableList = [self.Heat_ex_table,
                     self.Flux_cond_table]

        # list de keywords for each table
        correspKeyWord_Heat_ex=['CLIM_FL1D_T= ECHANGE ','CLIM_FL1D_T_FCT= ECHANGE ', 'CLIM_FL1D_T_PROG= ECHANGE ']
        correspKeyWord_Flux_cond=['CLIM_FL1D_T= FLUX_IMPOSE ','CLIM_FL1D_T_FCT= FLUX_IMPOSE ', 'CLIM_FL1D_T_PROG= FLUX_IMPOSE ']

        # list of lists of keywords, with the same order as in tableList
        correspKeyWordList = [correspKeyWord_Heat_ex,
                              correspKeyWord_Flux_cond]

        nbRefList = [1, 1] # list of number of references, with the same order as in tableList
        boolCombo = True # the combobox exists in the 2nd column

        for k in range(len(tableList)):
            Table = tableList[k]
            keyWord= correspKeyWordList[k]
            nbRef = nbRefList[k]
            saveTable(Table, keyWord, savfil, nbRef, boolCombo) # calling a external function for every table

        #Tous les cas sans reference mais avec des coordonnees

        #Entree 3D

        Table = self.Bcfluid1d_Inlet_3D_table

        for i in range(Table.rowCount()):
        # refresh "check" for a new loop
        # Choice of value in combobox (if any) 0 : CONT ; 1 : FUNC ; 2 : PROG ; by default : 0
        # and choice of begin column where the first value is supposed to be

            checkCombo = Table.cellWidget(i,1).currentIndex()
            beginCol = 2
            if checkCombo == 2 : # combobox of type "Program"
                check = Table.item(i,2)!=None and Table.item(i,2).text()!=str('') and Table.item(i,3)!=None and Table.item(i,3).text()!=str('') and Table.item(i,4)!=None and Table.item(i,4).text()!=str('')
            else: # Constant or FCT or other type of combobox
                check = boolFilledRow(Table, i, beginCol) # check = true only if all cells are filled in

            if check : # if the line i is fully filled
                if Table.item(i, 7)!=None:
                    savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                    Item=Table.item(i, 7)
                    savfil.write(Item.text())
                    savfil.write("\n")
                if Table.cellWidget(i, 0).isChecked()==False:
                    savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                if Table.cellWidget(i, 1).currentIndex()==0: # Détection du type de donnée pour la ligne (constant)
                    savfil.write("CLIM_FL1D_T= ENTREE_3D ")
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 5).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 6).text())
                elif Table.cellWidget(i, 1).currentIndex()==1: # Détection du type de donnée pour la ligne (fonction)
                    savfil.write("CLIM_FL1D_T_FCT= ENTREE_3D ")
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 5).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 6).text())
                else:
                    savfil.write("CLIM_FL1D_T_PROG= ENTREE_3D ") # Détection du type de donnée pour la ligne (sous-programme)
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                savfil.write("\n")
            if (not boolFilledRow(Table, i, beginCol)) and (not boolEmptyRow(Table, i, beginCol)) and checkCombo != 2:
                # not filled but not empty AND the row is not of type PROGRAM
                syrthesIHMContext.notFullyFilledException = True


        #Loop

        Table = self.Bcfluid1d_Q_table_cl

        for i in range(Table.rowCount()):
        # refresh "check" for a new loop
        # Choice of value in combobox (if any) 0 : CONT ; 1 : FUNC ; 2 : PROG ; by default : 0
        # and choice of begin column where the first value is supposed to be

            checkCombo = Table.cellWidget(i,1).currentIndex()
            beginCol = 2
            if checkCombo == 2 : # combobox of type "Program"
                check = Table.item(i,2)!=None and Table.item(i,2).text()!=str('') and Table.item(i,3)!=None and Table.item(i,3).text()!=str('') and Table.item(i,4)!=None and Table.item(i,4).text()!=str('')
            else: # Constant or FCT or other type of combobox
                check = boolFilledRow(Table, i, beginCol) # check = true only if all cells are filled in

            if check : # if the line i is fully filled
                if Table.item(i, 9)!=None:
                    savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                    Item=Table.item(i, 9)
                    savfil.write(Item.text())
                    savfil.write("\n")
                if Table.cellWidget(i, 0).isChecked()==False:
                    savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                if Table.cellWidget(i, 1).currentIndex()==0: # Détection du type de donnée pour la ligne (constant)
                    savfil.write("CLIM_FL1D_T= LOOP ")
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 5).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 6).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 7).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 8).text())
                elif Table.cellWidget(i, 1).currentIndex()==1: # Détection du type de donnée pour la ligne (fonction)
                    savfil.write("CLIM_FL1D_T_FCT= LOOP ")
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 5).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 6).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 7).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 8).text())
                else:
                    savfil.write("CLIM_FL1D_T_PROG= LOOP ") # Détection du type de donnée pour la ligne (sous-programme)
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                savfil.write("\n")
            if (not boolFilledRow(Table, i, beginCol)) and (not boolEmptyRow(Table, i, beginCol)) and checkCombo != 2:
                # not filled but not empty AND the row is not of type PROGRAM
                syrthesIHMContext.notFullyFilledException = True

        #Thermal Loop

        Table = self.Bcfluid1d_Q_table_th_cl

        for i in range(Table.rowCount()):
        # refresh "check" for a new loop
        # Choice of value in combobox (if any) 0 : CONT ; 1 : FUNC ; 2 : PROG ; by default : 0
        # and choice of begin column where the first value is supposed to be

            checkCombo = Table.cellWidget(i,1).currentIndex()
            beginCol = 2
            if checkCombo == 2 : # combobox of type "Program"
                check = Table.item(i,2)!=None and Table.item(i,2).text()!=str('') and Table.item(i,3)!=None and Table.item(i,3).text()!=str('') and Table.item(i,4)!=None and Table.item(i,4).text()!=str('')
            else: # Constant or FCT or other type of combobox
                check = boolFilledRow(Table, i, beginCol) # check = true only if all cells are filled in

            if check : # if the line i is fully filled
                if Table.item(i, 5)!=None:
                    savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                    Item=Table.item(i, 5)
                    savfil.write(Item.text())
                    savfil.write("\n")
                if Table.cellWidget(i, 0).isChecked()==False:
                    savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                if Table.cellWidget(i, 1).currentIndex()==0: # Détection du type de donnée pour la ligne (constant)
                    savfil.write("CLIM_FL1D_T= THERMAL_LOOP ")
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                elif Table.cellWidget(i, 1).currentIndex()==1: # Détection du type de donnée pour la ligne (fonction)
                    savfil.write("CLIM_FL1D_T_FCT= THERMAL_LOOP ")
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                else:
                    savfil.write("CLIM_FL1D_T_PROG= THERMAL_LOOP ") # Détection du type de donnée pour la ligne (sous-programme)
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                savfil.write("\n")
            if (not boolFilledRow(Table, i, beginCol)) and (not boolEmptyRow(Table, i, beginCol)) and checkCombo != 2:
                # not filled but not empty AND the row is not of type PROGRAM
                syrthesIHMContext.notFullyFilledException = True

        #Delta Pressure

        Table = self.Bcfluid1d_table_DP

        for i in range(Table.rowCount()):
        # refresh "check" for a new loop
        # Choice of value in combobox (if any) 0 : CONT ; 1 : FUNC ; 2 : PROG ; by default : 0
        # and choice of begin column where the first value is supposed to be

            checkCombo = Table.cellWidget(i,1).currentIndex()
            beginCol = 2
            if checkCombo == 2 : # combobox of type "Program"
                check = Table.item(i,2)!=None and Table.item(i,2).text()!=str('') and Table.item(i,3)!=None and Table.item(i,3).text()!=str('') and Table.item(i,4)!=None and Table.item(i,4).text()!=str('')
            else: # Constant or FCT or other type of combobox
                check = boolFilledRow(Table, i, beginCol) # check = true only if all cells are filled in

            if check : # if the line i is fully filled
                if Table.item(i, 8)!=None:
                    savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                    Item=Table.item(i, 8)
                    savfil.write(Item.text())
                    savfil.write("\n")
                if Table.cellWidget(i, 0).isChecked()==False:
                    savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                if Table.cellWidget(i, 1).currentIndex()==0: # Détection du type de donnée pour la ligne (constant)
                    savfil.write("CLIM_FL1D_T= DELTA_P ")
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 5).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 6).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 7).text())
                elif Table.cellWidget(i, 1).currentIndex()==1: # Détection du type de donnée pour la ligne (fonction)
                    savfil.write("CLIM_FL1D_T_FCT= DELTA_P ")
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 5).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 6).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 7).text())
                else:
                    savfil.write("CLIM_FL1D_T_PROG= DELTA_P ") # Détection du type de donnée pour la ligne (sous-programme)
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                savfil.write("\n")
            if (not boolFilledRow(Table, i, beginCol)) and (not boolEmptyRow(Table, i, beginCol)) and checkCombo != 2:
                # not filled but not empty AND the row is not of type PROGRAM
                syrthesIHMContext.notFullyFilledException = True

        savfil.write("/\n")


#------------------------------------------------------------------------------------------------------
class Physical_prop_fluid1d_formImpl(QtWidgets.QDialog, Ui_Physical_prop_fluid1d_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Physical_prop_fluid1d_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Physical_prop_fluid1d_formHandler(Physical_prop_fluid1d_formImpl):
    def __init__(self, parent=None):
        Physical_prop_fluid1d_formImpl.__init__(self, parent)

    def save(self, savfil) :

        # Gravity
        if self.Le_Gravity_x.text()!='' and self.Le_Gravity_y.text()!='' and self.Le_Gravity_z.text()!='' :
            savfil.write("GRAVITE_FL1D= ") # Ecriture du mot clé GRAVITE_FL1D
            savfil.write(self.Le_Gravity_x.text())
            savfil.write(" ")
            savfil.write(self.Le_Gravity_y.text())
            savfil.write(" ")
            savfil.write(self.Le_Gravity_z.text())
            savfil.write("\n")

        # Physical properties
        keyWord = ['CPHY_FL1D_T= ','CPHY_FL1D_T_FCT= ', 'CPHY_FL1D_T_PROG= ']
        Table = self.Prop_fluid1d_table
        nbRef = 1 # number of references
        boolCombo = True # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)
        savfil.write("/\n")

#------------------------------------------------------------------------------------------------------
class Geometrie_fluid1d_formImpl(QtWidgets.QDialog, Ui_Geometrie_fluid1d_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Geometrie_fluid1d_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Geometrie_fluid1d_formHandler(Geometrie_fluid1d_formImpl):
    def __init__(self, parent=None):
        Geometrie_fluid1d_formImpl.__init__(self, parent)

    def save(self, savfil):
        keyWord = ['GEOM_FL1D= ']
        Table = self.Geom_fluid1d_table
        nbRef = 1 # number of references
        boolCombo = False # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)
        savfil.write("/\n")

#------------------------------------------------------------------------------------------------------
class Geometrie_fluid0d_formImpl(QtWidgets.QDialog, Ui_Geometrie_fluid0d_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Geometrie_fluid0d_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Geometrie_fluid0d_formHandler(Geometrie_fluid0d_formImpl):
    def __init__(self, parent=None):
        Geometrie_fluid0d_formImpl.__init__(self, parent)

    def save(self, savfil):
        keyWord = ['GEOM_FL0D= ']
        Table = self.Geom_fluid0d_table
        nbRef = 1 # number of references
        boolCombo = False # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)
        savfil.write("/\n")

#------------------------------------------------------------------------------------------------------

class Volumetric_conditions_fluid1d_formImpl(QtWidgets.QDialog, Ui_Volumetric_conditions_fluid1d_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Volumetric_conditions_fluid1d_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Volumetric_conditions_fluid1d_formHandler(Volumetric_conditions_fluid1d_formImpl):
    def __init__(self, parent=None):
        Volumetric_conditions_fluid1d_formImpl.__init__(self, parent)

    def save(self, savfil):
        keyWord = ['PDC_LIN_FL1D_T= ','PDC_LIN_FL1D_T_FCT= ', 'PDC_LIN_FL1D_T_PROG= ']
        Table = self.Linear_head_fluid1d_table
        nbRef = 1 # number of references
        boolCombo = True # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)
        keyWord = ['PDC_SING_FL1D_T= ','PDC_SING_FL1D_T_FCT= ', 'PDC_SING_FL1D_T_PROG= ']
        Table = self.Singular_head_fluid1d_table
        nbRef = 1 # number of references
        boolCombo = True # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)
        keyWord = ['CVOL_FL1D_T= ','CVOL_FL1D_T_FCT= ', 'CVOL_FL1D_T_PROG= ']
        Table = self.Source_fluid1d_table
        nbRef = 1 # number of references
        boolCombo = True # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)
        savfil.write("/\n")

#------------------------------------------------------------------------------------------------------
class Control_fluid1d_formImpl(QtWidgets.QDialog, Ui_Control_fluid1d_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Control_fluid1d_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Control_fluid1d_formHandler(Control_fluid1d_formImpl):
    def __init__(self, parent=None):
        Control_fluid1d_formImpl.__init__(self, parent)

    def save(self, savfil):
        if self.Le_const_Ts_fluid1d.text()!='':
            savfil.write("PAS DE TEMPS FLUIDE 1D= ") # Ecriture du mot clé PAS DE TEMPS FLUIDE 1D
            savfil.write(self.Le_const_Ts_fluid1d.text())
            savfil.write("\n")
            savfil.write("/\n")
            pass
        pass


#------------------------------------------------------------------------------------------------------

class Material_radiation_properties_formImpl(QtWidgets.QDialog, Ui_Material_radiation_properties_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Material_radiation_properties_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Material_radiation_properties_formHandler(Material_radiation_properties_formImpl):
    def __init__(self, parent=None):
        Material_radiation_properties_formImpl.__init__(self, parent)

    def save(self, savfil):
        Table=self.Mrp_table
        #Table.setCurrentCell(0,2) # Positionnement du tableau des conditions initiales en humidité à la première case à exploiter

        for i in range(Table.rowCount()): # loop through rows
            beginCol = 1
            check = boolFilledRow(Table, i, beginCol) # check = true only if all cells are filled in

            if check : # if the line i is fully filled
                if Table.item(i, Table.columnCount()-1)!=None:
                    savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                    Item=Table.item(i, Table.columnCount()-1)
                    savfil.write(Item.text())
                    savfil.write("\n")
                if Table.cellWidget(i, 0).isChecked()==False:
                    savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                savfil.write("RAYT= ETR ") # Ecriture du mot clé RAYT= ETR
                savfil.write(Table.item(i, 1).text())
                savfil.write(" ")
                savfil.write(Table.item(i, 2).text())
                savfil.write(" ")
                savfil.write("0 ")
                savfil.write(str(1-float(Table.item(i, 2).text())))
                savfil.write(" ")
                savfil.write(Table.item(i, 3).text())
                savfil.write("\n")
            if (not boolFilledRow(Table, i, beginCol)) and (not boolEmptyRow(Table, i, beginCol)) :
                # not filled but not empty
                syrthesIHMContext.notFullyFilledException = True

#------------------------------------------------------------------------------------------------------

class Output_2D_formImpl(QtWidgets.QDialog, Ui_Output_2D_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Output_2D_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Output_2D_formHandler(Output_2D_formImpl):
    def __init__(self, parent=None):
        Output_2D_formImpl.__init__(self, parent)

    def save(self, savfil):
        if self.Tf_cb_2D_Op.isChecked()==True:
            if self.Cb_2D_Op.currentIndex()==1:
                savfil.write("PAS DES SORTIES CHRONO SOLIDE SECONDES= ") # Ecriture du mot clé PAS DES SORTIES CHRONO SOLIDE SECONDES=
            elif self.Cb_2D_Op.currentIndex()==0:
                savfil.write("PAS DES SORTIES CHRONO SOLIDE ITERATIONS= ") # Ecriture du mot clé PAS DES SORTIES CHRONO SOLIDE ITERATIONS=
            else:
                savfil.write("INSTANTS SORTIES CHRONO SOLIDE SECONDES= ") # Ecriture du mot clé INSTANTS SORTIES CHRONO SOLIDE SECONDES=
            savfil.write(self.Le_2D_Op.text())
            savfil.write("\n")



        if self.Le2_2D_Op.text()!='': # Détection d'une fréquence de sortie
            CB2 = self.Cb2_2D_Op
            LE2 = self.Le2_2D_Op
            Table = self.Op_Dc_2D_table

            if boolFilledTable(Table,1): # ne rien écrire si le tableau des coordonnées est vide
                if CB2.currentIndex() == 0:
                    savfil.write("HIST= FREQ_ITER ") # Ecriture du mot clé HIST= FREQ_ITER
                elif CB2.currentIndex() == 1:
                    savfil.write("HIST= FREQ_SECONDS ")
                else:
                    savfil.write("HIST= FREQ_LIST_TIMES ")
                FR=str(LE2.text()).strip(' ')
                savfil.write(FR)
                savfil.write("\n")

        rep = "NON"
        if self.Hf_cb_2D_Op.isChecked():
            rep = "OUI"
            pass
        savfil.write('CHAMP DE FLUX THERMIQUE= ')
        savfil.write(rep)
        savfil.write("\n")

        rep = "NON"
        if self.Mt_cb_2D_Op.isChecked():
            rep = "OUI"
            pass
        savfil.write('CHAMP DE TEMPERATURES MAXIMALES= ')
        savfil.write(rep)
        savfil.write("\n")

        rep = "NON"
        if self.F_cb_2D_Op.isChecked():
            rep = "OUI"
            pass
        savfil.write('SUPPRESSION DU CHAMP RESULTAT FINAL= ')
        savfil.write(rep)
        savfil.write("\n")

        tableList_2D = [self.Op_Dc_2D_table, self.Op_Sb_2D_table, self.Op_Vb_2D_table]
        tableList = tableList_2D

        correspKeyWordList = [['HIST= COORD '], ['BILAN FLUX SURFACIQUES= '], ['BILAN FLUX VOLUMIQUES= ']]

        nbRefList = [0, 1, 1] # number of references
        boolCombo = False # the combobox doesn't exist in the 2nd column

        # explore 3 tables
        for k in range(len(tableList)):
            Table = tableList[k]
            keyWord= correspKeyWordList[k]
            nbRef = nbRefList[k]
            saveTable(Table, keyWord, savfil, nbRef, boolCombo) # calling a external function for every table

#------------------------------------------------------------------------------------------------------

class Output_3D_formImpl(QtWidgets.QDialog, Ui_Output_3D_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Output_3D_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Output_3D_formHandler(Output_3D_formImpl):
    def __init__(self, parent=None):
        Output_3D_formImpl.__init__(self, parent)

    def save(self, savfil):
        if self.Tf_cb_3D_Op.isChecked()==True:
            if self.Cb_3D_Op.currentIndex()==1:
                savfil.write("PAS DES SORTIES CHRONO SOLIDE SECONDES= ") # Ecriture du mot clé PAS DES SORTIES CHRONO SOLIDE SECONDES=
            elif self.Cb_3D_Op.currentIndex()==0:
                savfil.write("PAS DES SORTIES CHRONO SOLIDE ITERATIONS= ") # Ecriture du mot clé PAS DES SORTIES CHRONO SOLIDE ITERATIONS=
            else:
                savfil.write("INSTANTS SORTIES CHRONO SOLIDE SECONDES= ") # Ecriture du mot clé INSTANTS SORTIES CHRONO SOLIDE SECONDES=
            savfil.write(self.Le_3D_Op.text())
            savfil.write("\n")

        if self.Le2_3D_Op.text()!='' : # Détection d'une fréquence de sortie
            CB2 = self.Cb2_3D_Op
            LE2 = self.Le2_3D_Op
            Table = self.Op_Dc_3D_table

            if boolFilledTable(Table,1): # ne rien écrire si le tableau des coordonnées est vide
                if CB2.currentIndex() == 0:
                    savfil.write("HIST= FREQ_ITER ") # Ecriture du mot clé HIST= FREQ_ITER
                elif CB2.currentIndex() == 1:
                    savfil.write("HIST= FREQ_SECONDS ")
                else:
                    savfil.write("HIST= FREQ_LIST_TIMES ")
                FR=str(LE2.text()).strip(' ')
                savfil.write(FR)
                savfil.write("\n")

        rep = "NON"
        if self.Hf_cb_3D_Op.isChecked():
            rep = "OUI"
            pass
        savfil.write('CHAMP DE FLUX THERMIQUE= ')
        savfil.write(rep)
        savfil.write("\n")

        rep = "NON"
        if self.Mt_cb_3D_Op.isChecked():
            rep = "OUI"
            pass
        savfil.write('CHAMP DE TEMPERATURES MAXIMALES= ')
        savfil.write(rep)
        savfil.write("\n")

        rep = "NON"
        if self.F_cb_3D_Op.isChecked():
            rep = "OUI"
            pass
        savfil.write('SUPPRESSION DU CHAMP RESULTAT FINAL= ')
        savfil.write(rep)
        savfil.write("\n")

        tableList_3D = [self.Op_Dc_3D_table, self.Op_Sb_3D_table, self.Op_Vb_3D_table]
        tableList = tableList_3D

        correspKeyWordList = [['HIST= COORD '], ['BILAN FLUX SURFACIQUES= '], ['BILAN FLUX VOLUMIQUES= ']]

        nbRefList = [0, 1, 1] # number of references
        boolCombo = False # the combobox doesn't exist in the 2nd column

        # explore 3 tables
        for k in range(len(tableList)):
            Table = tableList[k]
            keyWord= correspKeyWordList[k]
            nbRef = nbRefList[k]
            saveTable(Table, keyWord, savfil, nbRef, boolCombo) # calling a external function for every table

#------------------------------------------------------------------------------------------------------

class Periodicity_2D_formImpl(QtWidgets.QDialog, Ui_Periodicity_2D_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Periodicity_2D_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Periodicity_2D_formHandler(Periodicity_2D_formImpl):
    def __init__(self, parent=None):
        Periodicity_2D_formImpl.__init__(self, parent)

    def save(self, savfil) :
        #list of tables
        tableList = [self.Per_2D_rot_table, self.Per_2D_tra_table]

        # list de keywords for each table
        correspKeyWord_ROT_2D = ['CLIM= PERIODICITE_2D R ']
        correspKeyWord_TRA_2D = ['CLIM= PERIODICITE_2D T ']
        correspKeyWordList = [correspKeyWord_ROT_2D, correspKeyWord_TRA_2D]

        nbRefList = [2, 2] # list of number of reference fields, with the same order as in tableList
        boolCombo = False # the combobox doesn't exist in the 2nd column

        for k in range(len(tableList)):
            Table = tableList[k]
            keyWord= correspKeyWordList[k]
            nbRef = nbRefList[k]
            saveTable(Table, keyWord, savfil, nbRef, boolCombo) # calling a external function for every

#------------------------------------------------------------------------------------------------------

class Periodicity_3D_formImpl(QtWidgets.QDialog, Ui_Periodicity_3D_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Periodicity_3D_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Periodicity_3D_formHandler(Periodicity_3D_formImpl):
    def __init__(self, parent=None):
        Periodicity_3D_formImpl.__init__(self, parent)

    def save(self, savfil) :
        #list of tables
        tableList = [self.Per_3D_rot_table, self.Per_3D_tra_table]

        # list de keywords for each table
        correspKeyWord_ROT_3D = ['CLIM= PERIODICITE_3D R ']
        correspKeyWord_TRA_3D = ['CLIM= PERIODICITE_3D T ']
        correspKeyWordList = [correspKeyWord_ROT_3D, correspKeyWord_TRA_3D]

        nbRefList = [2, 2] # list of number of reference fields, with the same order as in tableList
        boolCombo = False # the combobox doesn't exist in the 2nd column

        for k in range(len(tableList)):
            Table = tableList[k]
            keyWord= correspKeyWordList[k]
            nbRef = nbRefList[k]
            saveTable(Table, keyWord, savfil, nbRef, boolCombo) # calling a external function for every table

#------------------------------------------------------------------------------------------------------

class Physical_prop_2D_formImpl(QtWidgets.QDialog, Ui_Physical_prop_2D_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Physical_prop_2D_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Physical_prop_2D_formHandler(Physical_prop_2D_formImpl):
    def __init__(self, parent=None):
        Physical_prop_2D_formImpl.__init__(self, parent)

    def save(self, savfil) :
        #list of tables
        tableList = [self.Iso_cond_2D_table, self.Ort_cond_2D_table, self.Ani_cond_2D_table]

        # list de keywords for each table
        correspKeyWord_Iso=['CPHY_MAT_ISO=','CPHY_MAT_ISO_FCT=', 'CPHY_MAT_ISO_PROG=']
        correspKeyWord_ORTHO_2D = ['CPHY_MAT_ORTHO_2D=','CPHY_MAT_ORTHO_2D_FCT=', 'CPHY_MAT_ORTHO_2D_PROG=']
        correspKeyWord_ANISO_2D = ['CPHY_MAT_ANISO_2D=','CPHY_MAT_ANISO_2D_FCT=', 'CPHY_MAT_ANISO_2D_PROG=']
        correspKeyWordList = [correspKeyWord_Iso, correspKeyWord_ORTHO_2D, correspKeyWord_ANISO_2D]

        nbRef = 1 # number of references
        boolCombo = True # the combobox exists in the 2nd column

        for k in range(len(tableList)):
            Table = tableList[k]
            keyWord= correspKeyWordList[k]
            saveTable(Table, keyWord, savfil, nbRef, boolCombo) # calling a external function for every table

#------------------------------------------------------------------------------------------------------

class Physical_prop_3D_formImpl(QtWidgets.QDialog, Ui_Physical_prop_3D_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Physical_prop_3D_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Physical_prop_3D_formHandler(Physical_prop_3D_formImpl):
    def __init__(self, parent=None):
        Physical_prop_3D_formImpl.__init__(self, parent)

    def save(self, savfil) :
        #list of tables
        tableList = [self.Iso_cond_3D_table, self.Ort_cond_3D_table, self.Ani_cond_3D_table]

        # list de keywords for each table
        correspKeyWord_Iso=['CPHY_MAT_ISO=','CPHY_MAT_ISO_FCT=', 'CPHY_MAT_ISO_PROG=']
        correspKeyWord_ORTHO_3D = ['CPHY_MAT_ORTHO_3D=','CPHY_MAT_ORTHO_3D_FCT=', 'CPHY_MAT_ORTHO_3D_PROG=']
        correspKeyWord_ANISO_3D = ['CPHY_MAT_ANISO_3D=','CPHY_MAT_ANISO_3D_FCT=', 'CPHY_MAT_ANISO_3D_PROG=']
        correspKeyWordList = [correspKeyWord_Iso, correspKeyWord_ORTHO_3D, correspKeyWord_ANISO_3D]

        nbRef = 1 # number of references
        boolCombo = True # the combobox exists in the 2nd column

        for k in range(len(tableList)):
            Table = tableList[k]
            keyWord= correspKeyWordList[k]
            saveTable(Table, keyWord, savfil, nbRef, boolCombo) # calling a external function for every table

#------------------------------------------------------------------------------------------------------

class Running_options_formImpl(QtWidgets.QDialog, Ui_Running_options_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Running_options_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Running_options_formHandler(Running_options_formImpl):
    def __init__(self, parent=None):
        Running_options_formImpl.__init__(self, parent)

    def save(self, savfil):
        savfil.write("/*******/ NBPROC_COND= ") # Ecriture du mot clé Nombre de processeurs conduction
        savfil.write(str(self.Ro_Np_sb_cd.value()))
        savfil.write("\n")

        savfil.write("/*******/ NBPROC_RAD= ") # Ecriture du mot clé Nombre de processeurs radiation
        savfil.write(str(self.Ro_Np_sb_ry.value()))
        savfil.write("\n")

        if self.Ro_Ln_le.text() != '': # Détection d'un suivis de calcul
            savfil.write("/*******/ LISTING= ") # Ecriture du mot clé Listing
            savfil.write(self.Ro_Ln_le.text())
            savfil.write("\n")

        savfil.write("/*******/ DOMAIN_POS= ") # Ecriture du mot clé Domain positioning
        savfil.write(str(self.Ro_Dp_cb.currentIndex()))
        savfil.write("\n")

        savfil.write("/*******/ C_RESULT= ") # Ecriture du mot clé Convert result for softwares
        savfil.write(str(self.Ro_Cr_cb.currentIndex()))
        savfil.write("\n")

#------------------------------------------------------------------------------------------------------

class Solar_aspect_formImpl(QtWidgets.QDialog, Ui_Solar_aspect_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Solar_aspect_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Solar_aspect_formHandler(Solar_aspect_formImpl):
    def __init__(self, parent=None):
        Solar_aspect_formImpl.__init__(self, parent)

    def save(self, savfil):
        savfil.write("PRISE EN COMPTE DU RAYONNEMENT SOLAIRE= ") # Ecriture du mot clé PRISE EN COMPTE DU RAYONNEMENT SOLAIRE
        if self.Sa_chb.isChecked():
            savfil.write("OUI\n")
            if self.Sa_cmb.currentIndex()==0: # Détection du type de prise en compte de rayonnement solaire
                savfil.write("SOLAIRE= SOURCE_AZIMUTH_H ") # Ecriture du mot clé SOLAIRE= SOURCE_AZIMUTH_H
                savfil.write(str(self.spinBox.value()))
                savfil.write(" ")
                savfil.write(str(self.spinBox_2.value()))
                savfil.write("\n")
                savfil.write("SOLAIRE= SOURCE_REPART_AUTO_SUR_LES_BANDES ") # Ecriture du mot clé SOLAIRE= SOURCE_REPART_AUTO_SUR_LES_BANDES
                if self.Csm_cmb.currentIndex()==0: # Détection de la distribution du flux (constant)
                    savfil.write("OUI\n")
                    savfil.write("SOLAIRE= SOURCE_FLUX_CONSTANT_PAR_BANDE 1 ") # Ecriture du mot clé SOLAIRE= SOURCE_FLUX_CONSTANT_PAR_BANDE 1
                    savfil.write(" ")
                    savfil.write(self.lineEdit.text())
                    savfil.write("\n")
                else:
                    savfil.write("NON\n")
                    Table=self.Csm_Db_table
                    keyWord = ['SOLAIRE= SOURCE_FLUX_CONSTANT_PAR_BANDE ']
                    nbRef = 0 # number of references
                    boolCombo = False # the combobox doesn't exist in the 2nd column
                    saveTable(Table, keyWord, savfil, nbRef, boolCombo)

            elif self.Sa_cmb.currentIndex()==1: # Détection de la distribution du flux (position astronomique)
                savfil.write("SOLAIRE= LATITUDE_DU_LIEU ") # Ecriture du mot clé SOLAIRE= LATITUDE_DU_LIEU
                savfil.write(str(self.spinBox_3.value()))
                savfil.write(" ")
                savfil.write(str(self.spinBox_4.value()))
                savfil.write("\n")
                savfil.write("SOLAIRE= LONGITUDE_DU_LIEU ") # Ecriture du mot clé SOLAIRE= LONGITUDE_DU_LIEU
                savfil.write(str(self.spinBox_5.value()))
                savfil.write(" ")
                savfil.write(str(self.spinBox_6.value()))
                savfil.write("\n")
                savfil.write("SOLAIRE= MOIS_JOUR_HEURE_MINUTE ") # Ecriture du mot clé SOLAIRE= MOIS_JOUR_HEURE_MINUTE
                savfil.write(str(self.dateTimeEdit.date().month()))
                savfil.write(" ")
                savfil.write(str(self.dateTimeEdit.date().day()))
                savfil.write(" ")
                savfil.write(str(self.dateTimeEdit.time().hour()))
                savfil.write(" ")
                savfil.write(str(self.dateTimeEdit.time().minute()))
                savfil.write("\n")
            else:
                pass

            savfil.write("SOLAIRE= COEFFICIENTS_DE_CLARTE_DU_CIEL ") # Ecriture du mot clé SOLAIRE= COEFFICIENTS_DE_CLARTE_DU_CIEL
            savfil.write(str(self.doubleSpinBox_5.value()))
            savfil.write(" ")
            savfil.write(str(self.doubleSpinBox_6.value()))
            savfil.write("\n")

            savfil.write("SOLAIRE= REPARTITION_AUTOMATIQUE_DU_FLUX_SOLAIRE_CONSTANT_SUR_LES_BANDES ") # Ecriture du mot clé SOLAIRE= REPARTITION_AUTOMATIQUE_DU_FLUX_SOLAIRE_CONSTANT_SUR_LES_BANDES
            if self.Csm_cmb.currentIndex()==0:
                savfil.write("OUI")
            else:
                savfil.write("NON")
            savfil.write("\n")
        else:
            savfil.write("NON\n")


        tableList = [self.Sa_Hm_table, self.Sa_Sm_table, self.Sa_Sht_table]
        correspKeyWordList = [['CLIM_RAYT= HORIZON '], ['CLIM_RAYT= OMBRAGE '], ['SOLAIRE= BILAN ']]
        nbRefList = [1, 1, 1]
        boolCombo = False # the combobox doesn't exist in the 2nd column

        for k in range(len(tableList)):
            Table = tableList[k]
            keyWord= correspKeyWordList[k]
            nbRef = nbRefList[k]
            saveTable(Table, keyWord, savfil, nbRef, boolCombo) # calling a external function for every table

#------------------------------------------------------------------------------------------------------

class Spectral_parameters_formImpl(QtWidgets.QDialog, Ui_Spectral_parameters_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Spectral_parameters_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Spectral_parameters_formHandler(Spectral_parameters_formImpl):
    def __init__(self, parent=None):
        Spectral_parameters_formImpl.__init__(self, parent)

    def save(self, savfil):
        keyWord = ['RAYT= BANDES_SPECTRALES ']
        Table = self.Rp_Sb_table
        nbRef = 0 # number of references
        boolCombo = False # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)

#------------------------------------------------------------------------------------------------------

class User_C_function_formImpl(QtWidgets.QDialog, Ui_User_C_function_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_User_C_function_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class User_C_function_formHandler(User_C_function_formImpl):
    def __init__(self, parent=None):
        User_C_function_formImpl.__init__(self, parent)

#------------------------------------------------------------------------------------------------------

class View_factor_2D_formImpl(QtWidgets.QDialog, Ui_View_factor_2D_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_View_factor_2D_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class View_factor_2D_formHandler(View_factor_2D_formImpl):
    def __init__(self, parent=None):
        View_factor_2D_formImpl.__init__(self, parent)

    def save(self, savfil):
        cmb=self.Vfm_2D_cmb

        savfil.write("LECTURE DES FACTEURS DE FORME SUR FICHIER= ") # Ecriture du mot clé LECTURE DES FACTEURS DE FORME SUR FICHIER
        if cmb.currentIndex()==0:
            savfil.write("NON\n")
        else:
            savfil.write("OUI\n")

        #list of tables
        tableList_2D = [self.Vf_Ip_2D_table, self.Vf_Sy_2D_table, self.Vf_Pe_2D_table]

        # list de keywords for each table
        correspKeyWord_Ip = ['RAYT= VOLUME_CONNEXE ']
        correspKeyWord_Sy_2D = ['RAYT= SYMETRIE_2D ']
        correspKeyWord_Pe_2D = ['RAYT= PERIODICITE_2D ']

        # check for probleme dimension
        # and list of lists of keywords, with the same order as in tableList
        tableList = tableList_2D
        correspKeyWordList = [correspKeyWord_Ip, correspKeyWord_Sy_2D, correspKeyWord_Pe_2D]

        nbRefList = [0, 0, 0] # list of number of reference fields, with the same order as in tableList
        boolCombo = False # the combobox doesn't exist in the 2nd column

        for k in range(len(tableList)):
            Table = tableList[k]
            keyWord= correspKeyWordList[k]
            nbRef = nbRefList[k]
            saveTable(Table, keyWord, savfil, nbRef, boolCombo) # calling a external function for every

#------------------------------------------------------------------------------------------------------

class View_factor_3D_formImpl(QtWidgets.QDialog, Ui_View_factor_3D_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_View_factor_3D_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class View_factor_3D_formHandler(View_factor_3D_formImpl):
    def __init__(self, parent=None):
        View_factor_3D_formImpl.__init__(self, parent)

    def save(self, savfil) :
        cmb=self.Vfm_3D_cmb

        savfil.write("LECTURE DES FACTEURS DE FORME SUR FICHIER= ") # Ecriture du mot clé LECTURE DES FACTEURS DE FORME SUR FICHIER
        if cmb.currentIndex()==0:
            savfil.write("NON\n")
        else:
            savfil.write("OUI\n")

        #list of tables
        tableList_3D = [self.Vf_Ip_3D_table, self.Vf_Sy_3D_table, self.Vf_Pe_3D_table]

        # list de keywords for each table
        correspKeyWord_Ip = ['RAYT= VOLUME_CONNEXE ']
        correspKeyWord_Sy_3D = ['RAYT= SYMETRIE_3D ']
        correspKeyWord_Pe_3D = ['RAYT= PERIODICITE_3D ']

        # check for probleme dimension
        # and list of lists of keywords, with the same order as in tableList
        tableList = tableList_3D
        correspKeyWordList = [correspKeyWord_Ip, correspKeyWord_Sy_3D, correspKeyWord_Pe_3D]

        nbRefList = [0, 0, 0] # list of number of reference fields, with the same order as in tableList
        boolCombo = False # the combobox doesn't exist in the 2nd column

        for k in range(len(tableList)):
            Table = tableList[k]
            keyWord= correspKeyWordList[k]
            nbRef = nbRefList[k]
            saveTable(Table, keyWord, savfil, nbRef, boolCombo) # calling a external function for every


#------------------------------------------------------------------------------------------------------

class Volumetric_conditions_cond_formImpl(QtWidgets.QDialog, Ui_Volumetric_conditions_cond_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Volumetric_conditions_cond_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Volumetric_conditions_cond_formHandler(Volumetric_conditions_cond_formImpl):
    def __init__(self, parent=None):
        Volumetric_conditions_cond_formImpl.__init__(self, parent)

    def save(self, savfil) :
        keyWord = ['CVOL_T=','CVOL_T_FCT=', 'CVOL_T_PROG=']
        Table = self.Vol_so_table
        nbRef = 1 # number of references
        boolCombo = True # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)

#------------------------------------------------------------------------------------------------------

class Volumetric_conditions_hum_TPv_formImpl(QtWidgets.QDialog, Ui_Volumetric_conditions_hum_TPv_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Volumetric_conditions_hum_TPv_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Volumetric_conditions_hum_TPv_formHandler(Volumetric_conditions_hum_TPv_formImpl):
    def __init__(self, parent=None):
        Volumetric_conditions_hum_TPv_formImpl.__init__(self, parent)

    def save(self, savfil):
        Table=self.Vch_St_TPv_table
        #Table.setCurrentCell(0,3) # Positionnement du tableau des conditions volumiques en humidité à la première case à exploiter

        for i in range(Table.rowCount()):
        # refresh "check" for a new loop
        # Choice of value in combobox (if any) 0 : CONT ; 1 : FUNC ; 2 : PROG ; by default : 0
        # and choice of begin column where the first value is supposed to be
            checkCombo = Table.cellWidget(i,1).currentIndex()
            beginCol = 3
            if checkCombo == 2 : # combobox of type "Program"
                check = Table.item(i,Table.columnCount()-2)!=None and Table.item(i,Table.columnCount()-2).text()!=str('')
            else: # Constant or FCT or other type of combobox
                check = boolFilledRow(Table, i, beginCol) # check = true only if all cells are filled in

            if check : # if the line i is fully filled
                if Table.item(i, 6)!=None:
                    savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                    Item=Table.item(i, 6)
                    savfil.write(Item.text())
                    savfil.write("\n")
                if Table.cellWidget(i, 0).isChecked()==False:
                    savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                if Table.cellWidget(i, 2).currentIndex()==0:
                    savfil.write("CVOL_T") # Ecriture du mot clé CVOL_T
                elif Table.cellWidget(i, 2).currentIndex()==1:
                    savfil.write("CVOL_PV") # Ecriture du mot clé CVOL_PV
                else:
                    savfil.write("CVOL_PT") # Ecriture du mot clé CVOL_PT
                if Table.cellWidget(i, 1).currentIndex()==0: # Détection du type de donnée pour la ligne (constant)
                    savfil.write("= ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 5).text())
                elif Table.cellWidget(i, 1).currentIndex()==1: # Détection du type de donnée pour la ligne (fonction)
                    savfil.write("_FCT= ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 5).text())
                else:
                    savfil.write("_PROG= ") # Détection du type de donnée pour la ligne (sous-programme)
                    savfil.write(Table.item(i, 5).text())
                savfil.write("\n")
            if (not boolFilledRow(Table, i, beginCol)) and (not boolEmptyRow(Table, i, beginCol)) and checkCombo != 2:
                # not filled but not empty AND the row is not of type PROGRAM
                syrthesIHMContext.notFullyFilledException = True

#------------------------------------------------------------------------------------------------------

class Volumetric_conditions_hum_TPvPt_formImpl(QtWidgets.QDialog, Ui_Volumetric_conditions_hum_TPvPt_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Volumetric_conditions_hum_TPvPt_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Volumetric_conditions_hum_TPvPt_formHandler(Volumetric_conditions_hum_TPvPt_formImpl):
    def __init__(self, parent=None):
        Volumetric_conditions_hum_TPvPt_formImpl.__init__(self, parent)

    def save(self, savfil):
        Table=self.Vch_St_TPvPt_table
        #Table.setCurrentCell(0,3) # Positionnement du tableau des conditions volumiques en humidité à la première case à exploiter

        for i in range(Table.rowCount()):
        # refresh "check" for a new loop
        # Choice of value in combobox (if any) 0 : CONT ; 1 : FUNC ; 2 : PROG ; by default : 0
        # and choice of begin column where the first value is supposed to be

            checkCombo = Table.cellWidget(i,1).currentIndex()
            beginCol = 3
            if checkCombo == 2 : # combobox of type "Program"
                check = Table.item(i,Table.columnCount()-2)!=None and Table.item(i,Table.columnCount()-2).text()!=str('')
            else: # Constant or FCT or other type of combobox
                check = boolFilledRow(Table, i, beginCol) # check = true only if all cells are filled in

            if check : # if the line i is fully filled
                if Table.item(i, 6)!=None:
                    savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                    Item=Table.item(i, 6)
                    savfil.write(Item.text())
                    savfil.write("\n")
                if Table.cellWidget(i, 0).isChecked()==False:
                    savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                if Table.cellWidget(i, 2).currentIndex()==0:
                    savfil.write("CVOL_T") # Ecriture du mot clé CVOL_T
                elif Table.cellWidget(i, 2).currentIndex()==1:
                    savfil.write("CVOL_PV") # Ecriture du mot clé CVOL_PV
                else:
                    savfil.write("CVOL_PT") # Ecriture du mot clé CVOL_PT
                if Table.cellWidget(i, 1).currentIndex()==0: # Détection du type de donnée pour la ligne (constant)
                    savfil.write("= ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 5).text())
                elif Table.cellWidget(i, 1).currentIndex()==1: # Détection du type de donnée pour la ligne (fonction)
                    savfil.write("_FCT= ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 5).text())
                else:
                    savfil.write("_PROG= ") # Détection du type de donnée pour la ligne (sous-programme)
                    savfil.write(Table.item(i, 5).text())
                savfil.write("\n")
            if (not boolFilledRow(Table, i, beginCol)) and (not boolEmptyRow(Table, i, beginCol)) and checkCombo != 2:
                # not filled but not empty AND the row is not of type PROGRAM
                syrthesIHMContext.notFullyFilledException = True

#------------------------------------------------------------------------------------------------------
# fluid0d

class Boundary_conditions_fluid0d_formImpl(QtWidgets.QDialog, Ui_Boundary_conditions_fluid0d_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Boundary_conditions_fluid0d_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Boundary_conditions_fluid0d_formHandler(Boundary_conditions_fluid0d_formImpl):
    def __init__(self, parent=None):
        Boundary_conditions_fluid0d_formImpl.__init__(self, parent)

    def save(self, savfil):

        keyWord = ['CLIM_T= ECH_OD','CLIM_T_FCT= ECH_OD', 'CLIM_T_PROG= ECH_OD']
        Table = self.Heat_ex_fluid0d_table
        nbRef = 1 # number of references
        boolCombo = True # the combobox exists in the 2nd column
        saveTable(Table, keyWord, savfil, nbRef, boolCombo)
        savfil.write("/\n")



#------------------------------------------------------------------------------------------------------
class Physical_properties_fluid0d_formImpl(QtWidgets.QDialog, Ui_Physical_properties_fluid0d_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Physical_properties_fluid0d_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Physical_properties_fluid0d_formHandler(Physical_properties_fluid0d_formImpl):
    def __init__(self, parent=None):
        Physical_properties_fluid0d_formImpl.__init__(self, parent)

    def save(self, savfil):

        Table = self.Prop_fluid0d_table

        for i in range(Table.rowCount()):
        # refresh "check" for a new loop
        # Choice of value in combobox (if any) 0 : CONT ; 1 : FUNC ; 2 : PROG ; by default : 0
        # and choice of begin column where the first value is supposed to be

            checkCombo = Table.cellWidget(i,1).currentIndex()
            beginCol = 2
            if checkCombo == 2 : # combobox of type "Program"
                check = Table.item(i,2)!=None and Table.item(i,2).text()!=str('')
            else: # Constant or FCT or other type of combobox
                check = boolFilledRow(Table, i, beginCol) # check = true only if all cells are filled in

            if check : # if the line i is fully filled
                if Table.item(i, 6)!=None:
                    savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                    Item=Table.item(i, 6)
                    savfil.write(Item.text())
                    savfil.write("\n")
                if Table.cellWidget(i, 0).isChecked()==False:
                    savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                if Table.cellWidget(i, 1).currentIndex()==0: # Détection du type de donnée pour la ligne (constant)
                    savfil.write("CPHY_FL0D_T= ")
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 5).text())
                elif Table.cellWidget(i, 1).currentIndex()==1: # Détection du type de donnée pour la ligne (fonction)
                    savfil.write("CPHY_FL0D_T_FCT= ")
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 5).text())
                else:
                    savfil.write("CPHY_FL0D_T_PROG= ") # Détection du type de donnée pour la ligne (sous-programme)
                    savfil.write(Table.item(i, 2).text())
                savfil.write("\n")
            if (not boolFilledRow(Table, i, beginCol)) and (not boolEmptyRow(Table, i, beginCol)) and checkCombo != 2:
                # not filled but not empty AND the row is not of type PROGRAM
                syrthesIHMContext.notFullyFilledException = True

        savfil.write("/\n")

#------------------------------------------------------------------------------------------------------
class Volumetric_conditions_fluid0d_formImpl(QtWidgets.QDialog, Ui_Volumetric_conditions_fluid0d_form):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        Ui_Volumetric_conditions_fluid0d_form.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)

class Volumetric_conditions_fluid0d_formHandler(Volumetric_conditions_fluid0d_formImpl):
    def __init__(self, parent=None):
        Volumetric_conditions_fluid0d_formImpl.__init__(self, parent)

    def save(self, savfil):

        Table = self.Volumetric_conditions_fluid0d_table

        for i in range(Table.rowCount()):
        # refresh "check" for a new loop
        # Choice of value in combobox (if any) 0 : CONT ; 1 : FUNC ; 2 : PROG ; by default : 0
        # and choice of begin column where the first value is supposed to be

            checkCombo = Table.cellWidget(i,1).currentIndex()
            beginCol = 2
            if checkCombo == 2 : # combobox of type "Program"
                check = Table.item(i,2)!=None and Table.item(i,2).text()!=str('')
            else: # Constant or FCT or other type of combobox
                check = boolFilledRow(Table, i, beginCol) # check = true only if all cells are filled in

            if check : # if the line i is fully filled
                if Table.item(i, 5)!=None:
                    savfil.write("/+ ") # Ecriture d'une ligne de commentaire utilisateur
                    Item=Table.item(i, 5)
                    savfil.write(Item.text())
                    savfil.write("\n")
                if Table.cellWidget(i, 0).isChecked()==False:
                    savfil.write("/# ") # Ecriture d'une ligne de mot clé non prise en compte
                if Table.cellWidget(i, 1).currentIndex()==0: # Détection du type de donnée pour la ligne (constant)
                    savfil.write("CVOL_FL0D_T= ")
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                elif Table.cellWidget(i, 1).currentIndex()==1: # Détection du type de donnée pour la ligne (fonction)
                    savfil.write("CVOL_FL0D_T_FCT= ")
                    savfil.write(Table.item(i, 2).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 3).text())
                    savfil.write(" ")
                    savfil.write(Table.item(i, 4).text())
                else:
                    savfil.write("CVOL_FL0D_T_PROG= ") # Détection du type de donnée pour la ligne (sous-programme)
                    savfil.write(Table.item(i, 2).text())
                savfil.write("\n")
            if (not boolFilledRow(Table, i, beginCol)) and (not boolEmptyRow(Table, i, beginCol)) and checkCombo != 2:
                # not filled but not empty AND the row is not of type PROGRAM
                syrthesIHMContext.notFullyFilledException = True

        savfil.write("/\n")



#------------------------------------------------------------------------------------------------------

class SYRTHES_IHMCollector(object):
    def __init__(self, parent=None):
        self.Advanced_mode_form = Advanced_mode_formHandler()
        self.Boundary_conditions_cond_form = Boundary_conditions_cond_formHandler()
        self.Boundary_conditions_rad_form = Boundary_conditions_rad_formHandler()
        self.Boundary_conditions_TPv_form = Boundary_conditions_TPv_formHandler()
        self.Boundary_conditions_TPvPt_form = Boundary_conditions_TPvPt_formHandler()

        self.Conjugate_heat_transfer_form = Conjugate_heat_transfer_formHandler()
        self.Control_form = Control_formHandler()
        self.Fake_form = Fake_formHandler()
        self.Filename_form = Filename_formHandler()
        self.Home_form = Home_formHandler()
        self.Initial_conditions_cond_form = Initial_conditions_cond_formHandler()
        self.Initial_conditions_hum_TPv_form = Initial_conditions_hum_TPv_formHandler()
        self.Initial_conditions_hum_TPvPt_form = Initial_conditions_hum_TPvPt_formHandler()
        self.Material_humidity_properties_2D_form = Material_humidity_properties_2D_formHandler()
        self.Material_humidity_properties_3D_form = Material_humidity_properties_3D_formHandler()
        self.Material_radiation_properties_form = Material_radiation_properties_formHandler()
        self.Contact_resistance_humidity_TPv_form = Contact_resistance_humidity_TPv_formHandler()
        self.Contact_resistance_humidity_TPvPt_form = Contact_resistance_humidity_TPvPt_formHandler()
        # fluid1d
        self.Initial_conditions_fluid1d_form     =  Initial_conditions_fluid1d_formHandler()
        self.Boundary_conditions_fluid1d_3D_form =  Boundary_conditions_fluid1d_3D_formHandler()
        self.Physical_prop_fluid1d_form          =  Physical_prop_fluid1d_formHandler()
        self.Geometrie_fluid1d_form              =  Geometrie_fluid1d_formHandler()
        self.Volumetric_conditions_fluid1d_form  =  Volumetric_conditions_fluid1d_formHandler()
        self.Control_fluid1d_form                =  Control_fluid1d_formHandler()
        # fluid0d
        self.Geometrie_fluid0d_form               = Geometrie_fluid0d_formHandler()
        self.Boundary_conditions_fluid0d_form     = Boundary_conditions_fluid0d_formHandler()
        self.Physical_properties_fluid0d_form     = Physical_properties_fluid0d_formHandler()
        self.Volumetric_conditions_fluid0d_form   = Volumetric_conditions_fluid0d_formHandler()

        self.DialogNew = DialogNewHandler(parent)

        self.Output_2D_form = Output_2D_formHandler()
        self.Output_3D_form = Output_3D_formHandler()
        self.Periodicity_2D_form = Periodicity_2D_formHandler()
        self.Periodicity_3D_form = Periodicity_3D_formHandler()
        self.Physical_prop_2D_form = Physical_prop_2D_formHandler()
        self.Physical_prop_3D_form = Physical_prop_3D_formHandler()
        self.Running_options_form = Running_options_formHandler()
        self.Solar_aspect_form = Solar_aspect_formHandler()
        self.Spectral_parameters_form = Spectral_parameters_formHandler()
        self.User_C_function_form = User_C_function_formHandler()
        self.View_factor_2D_form = View_factor_2D_formHandler()
        self.View_factor_3D_form = View_factor_3D_formHandler()
        self.Volumetric_conditions_cond_form = Volumetric_conditions_cond_formHandler()
        self.Volumetric_conditions_hum_TPv_form = Volumetric_conditions_hum_TPv_formHandler()
        self.Volumetric_conditions_hum_TPvPt_form = Volumetric_conditions_hum_TPvPt_formHandler()
