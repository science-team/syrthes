# -*- coding: utf-8 -*-

from PyQt5 import QtWidgets

class clMyTabWidget(QtWidgets.QTabWidget):
    # vocabulaire (obsolète) :
    # onglets permanents : ceux créés avec Qtdesigner (au nombre de 4 pour l'instant)
    # onglets actifs : ceux ayant un titre tel que Graph1, Graph2, ...
    # onglets affichés : "Graph1", "Graph2", ... et éventuellement "+" (ie. = self.count())

    def __init__(self, parent):
        super(clMyTabWidget, self).__init__(parent)
        self.status = ""
        self.tabs = []
        self.nbTotal = -1
        self.nbActif = -1
        self.curveIndexLinked = []
        self.yRightEnabled = []

    def initView(self): # à appeler absolument lorsque la fenêtre Calculation progress est initialisée
        # sauvegarder les onglets actuels dans self.tabs

        self.nbTotal = self.count() # le nobmre des onglets y compris ceux cachés
        self.nbActif = 4
        for i in range(self.nbTotal):
            self.tabs.append(self.widget(i))
            self.curveIndexLinked.append((-1,-1))
            self.yRightEnabled.append(False)
        self.curveIndexLinked[0] = 0,0

    def boolAwaitingData(self):
        flag = True
        for i in range(self.count()):
            if self.curveIndexLinked[i][0] >= 0 and self.curveIndexLinked[i][1] >= 0 : flag = False
        return flag
