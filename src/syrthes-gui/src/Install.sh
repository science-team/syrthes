# pyuic5 takes a Qt5 user interface description file and compiles it to Python code.
#
# generation des fichiers python
#

echo "Installation : generation of python files ..."

rm -rf install
mkdir install

cp *.py install/

pyuic5 Advanced_mode_form.ui -o install/ui_Advanced_mode_form.py -x
pyuic5 Boundary_conditions_cond_form.ui -o install/ui_Boundary_conditions_cond_form.py -x
pyuic5 Boundary_conditions_rad_form.ui -o install/ui_Boundary_conditions_rad_form.py -x
pyuic5 Boundary_conditions_TPv_form.ui -o install/ui_Boundary_conditions_TPv_form.py -x
pyuic5 Boundary_conditions_TPvPt_form.ui -o install/ui_Boundary_conditions_TPvPt_form.py -x
pyuic5 Calculation_progress.ui -o install/ui_Calculation_progress.py -x
pyuic5 Conjugate_heat_transfer_form.ui -o install/ui_Conjugate_heat_transfer_form.py -x
pyuic5 Control_form.ui -o install/ui_Control_form.py -x
pyuic5 Fake_form.ui -o install/ui_Fake_form.py -x
pyuic5 Filename_form.ui -o install/ui_Filename_form.py -x
pyuic5 Home_form.ui -o install/ui_Home_form.py -x
pyuic5 Initial_conditions_cond_form.ui -o install/ui_Initial_conditions_cond_form.py -x
pyuic5 Initial_conditions_hum_TPv_form.ui -o install/ui_Initial_conditions_hum_TPv_form.py -x
pyuic5 Initial_conditions_hum_TPvPt_form.ui -o install/ui_Initial_conditions_hum_TPvPt_form.py -x
pyuic5 Material_humidity_properties_form.ui -o install/ui_Material_humidity_properties_form.py -x
pyuic5 Material_radiation_properties_form.ui -o install/ui_Material_radiation_properties_form.py -x
pyuic5 New_dialog.ui -o install/ui_New_dialog.py -x
pyuic5 Output_2D_form.ui -o install/ui_Output_2D_form.py -x
pyuic5 Output_3D_form.ui -o install/ui_Output_3D_form.py -x
pyuic5 Periodicity_2D_form.ui -o install/ui_Periodicity_2D_form.py -x
pyuic5 Periodicity_3D_form.ui -o install/ui_Periodicity_3D_form.py -x
pyuic5 Physical_prop_2D_form.ui -o install/ui_Physical_prop_2D_form.py -x
pyuic5 Physical_prop_3D_form.ui -o install/ui_Physical_prop_3D_form.py -x
pyuic5 Running_options_form.ui -o install/ui_Running_options_form.py -x
pyuic5 Solar_aspect_form.ui -o install/ui_Solar_aspect_form.py -x
pyuic5 Spectral_parameters_form.ui -o install/ui_Spectral_parameters_form.py -x
pyuic5 SyrthesMainwin80060023.ui -o install/ui_SyrthesMainwin80060023.py -x
pyuic5 User_C_function_form.ui -o install/ui_User_C_function_form.py -x
pyuic5 View_factor_2D_form.ui -o install/ui_View_factor_2D_form.py -x
pyuic5 View_factor_3D_form.ui -o install/ui_View_factor_3D_form.py -x
pyuic5 Volumetric_conditions_cond_form.ui -o install/ui_Volumetric_conditions_cond_form.py -x
pyuic5 Volumetric_conditions_hum_TPv_form.ui -o install/ui_Volumetric_conditions_hum_TPv_form.py -x
pyuic5 Volumetric_conditions_hum_TPvPt_form.ui -o install/ui_Volumetric_conditions_hum_TPvPt_form.py -x

pyuic5 Boundary_conditions_fluid1d_3D_form.ui  -o  install/ui_Boundary_conditions_fluid1d_3D_form.py -x
pyuic5 Control_fluid1d_form.ui   -o  install/ui_Control_fluid1d_form.py -x
pyuic5 Geometrie_fluid1d_form.ui  -o  install/ui_Geometrie_fluid1d_form.py -x
pyuic5 Initial_conditions_fluid1d_form.ui  -o  install/ui_Initial_conditions_fluid1d_form.py -x
pyuic5 Physical_prop_fluid1d_form.ui  -o  install/ui_Physical_prop_fluid1d_form.py -x
pyuic5 Volumetric_conditions_fluid1d_form.ui  -o  install/ui_Volumetric_conditions_fluid1d_form.py -x

pyuic5 Material_humidity_properties_2D_form.ui -o install/ui_Material_humidity_properties_2D_form.py -x
pyuic5 Material_humidity_properties_3D_form.ui -o install/ui_Material_humidity_properties_3D_form.py -x

pyuic5 Contact_resistance_humidity_TPv_form.ui  -o install/ui_Contact_resistance_humidity_TPv_form.py -x
pyuic5 Contact_resistance_humidity_TPvPt_form.ui -o install/ui_Contact_resistance_humidity_TPvPt_form.py -x

pyuic5 Output_Times_form.ui -o install/ui_Output_Times_form.py -x
pyuic5 Physical_properties_fluid0d_form.ui -o install/ui_Physical_properties_fluid0d_form.py -x
pyuic5 Boundary_conditions_fluid0d_form.ui -o install/ui_Boundary_conditions_fluid0d_form.py -x
pyuic5 Geometrie_fluid0d_form.ui  -o  install/ui_Geometrie_fluid0d_form.py -x
pyuic5 Volumetric_conditions_fluid0d_form.ui  -o  install/ui_Volumetric_conditions_fluid0d_form.py -x

pyrcc5 resource.qrc -o install/resource_rc.py

cp -rf 22x22 install/
cp -rf ficwhatsthis install/

# Prise en compte de la version dans le fichier version_syrthes
python version_syrthes/changer_version.py
