# -*- coding: utf-8 -*-

from PyQt5 import QtWidgets

class CustomMessageBox(QtWidgets.QMessageBox):
# suppression dernier argument **kwargs sinon probleme TypeError sous cygwin
#    def __init__(self,*args, **kwargs):
#        QtWidgets.QMessageBox.__init__(self, *args, **kwargs)
    def __init__(self, *args):
        QtWidgets.QMessageBox.__init__(self, *args)
        self.setSizeGripEnabled(True)
        pass

    def event(self, e):
        result = QtWidgets.QMessageBox.event(self, e)

        self.setMinimumHeight(0)
        self.setMaximumHeight(16777215)
        self.setMinimumWidth(0)
        self.setMaximumWidth(16777215)
        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)

        textEdit = self.findChild(QtWidgets.QTextEdit)
        if textEdit != None :
            textEdit.setMinimumHeight(0)
            textEdit.setMaximumHeight(16777215)
            textEdit.setMinimumWidth(0)
            textEdit.setMaximumWidth(16777215)
            textEdit.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
            pass
        return result
