# -*- coding: utf-8 -*-

from PyQt5 import QtGui, QtWidgets

class Check_table(object): # classe de vérification des données entrée dans les tableaux

    def Check_table(self, a, b): # self.table_check est un dictionnaire donnant pour chaque tableau le type de donnée à rentré par colonne
        self.table_check={self.syrthesIHMCollector.Control_form.By_Block_table : ['+int', '+float', ''], # Tableau du contrôle de pas de temps par block
                          self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table : ['', '', 'float>-273.15Combo', 'cond+=float', '+chkpintstr', ''], # Tableau de l'échange de chaleur
                          self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table : ['', '', 'cond+=float', '+compintstr', '+compintstr', ''], # Tableau des résistances de contact
                          self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table : ['', '', 'condfloat', '+chkpintstr', ''], # Tableau des conditions de flux
                          self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table : ['', '', 'float>-273.15Combo', '+chkpintstr', ''], # Tableau des conditions de Dirichlet
                          self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table : ['', '', 'float>-273.15Combo', 'cond0<=float<=1', '+chkpintstr', ''], # Tableau du rayonnement infinis
                          self.syrthesIHMCollector.Physical_prop_2D_form.Iso_cond_2D_table : ['', '', 'cond+float', 'cond+float', 'cond+=float','+chkintstr',''], # Tableau de la conductivité isotropique en 2D
                          self.syrthesIHMCollector.Physical_prop_3D_form.Iso_cond_3D_table : ['', '','cond+float', 'cond+float', 'cond+=float','+chkintstr',''], # Tableau de la conductivité isotropique en 3D
                          self.syrthesIHMCollector.Physical_prop_2D_form.Ort_cond_2D_table : ['', '','cond+float', 'cond+float', 'cond+=float', 'cond+=float', '+chkintstr',''], # Tableau de la conductivité orthotrope en 2D
                          self.syrthesIHMCollector.Physical_prop_3D_form.Ort_cond_3D_table : ['', '','cond+float', 'cond+float', 'cond+=float', 'cond+=float', 'cond+=float', '+chkintstr',''], # Tableau de la conductivité orthotrope en 3D
                          self.syrthesIHMCollector.Physical_prop_2D_form.Ani_cond_2D_table : ['', '','cond+float', 'cond+float', 'cond+=float', 'cond+=float', 'angleCombo', '+chkintstr',''], # Tableau de la conductivité anisotrope en 2D
                          self.syrthesIHMCollector.Physical_prop_3D_form.Ani_cond_3D_table : ['', '','cond+float', 'cond+float', 'cond+=float', 'cond+=float', 'cond+=float', 'float', 'float', 'float', 'float', 'float', 'float', 'float', 'float', 'float', '+chkintstr',''], # Tableau de la conductivité anisotrope en 3D
                          self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table : ['', '', 'float>-273.15Combo', '+chkintstr',''], # Tableau de la température initiale
                          self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table : ['', '', 'condfloat', 'condfloat', '+chkintstr',''], # Tableau des termes sources volumique
                          self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_rot_table : ['', 'float', 'float', 'angle', '+compintstr', '+compintstr', ''], # Tableau de la périodicité en rotation en 2D
                          self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_tra_table : ['', 'float', 'float', '+compintstr', '+compintstr', ''], # Tableau de la périodicité en translation en 2D
                          self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_rot_table : ['', 'float', 'float', 'float', 'float', 'float', 'float', 'angle', '+compintstr', '+compintstr', ''], # Tableau de la périodicité en rotation en 3D
                          self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_tra_table : ['', 'float', 'float', 'float', '+compintstr', '+compintstr', ''], # Tableau de la périodicité en translation en 3D
                          self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table : ['', '+chkintstr', 'spfloat', 'spfloat2', ''], # Tableau des définitions des bandes spectrales
                          self.syrthesIHMCollector.View_factor_2D_form.Vf_Ip_2D_table : ['', 'float', 'float', ''], # Tableau des coordonnées de points pour les facteurs de forme en 2D
                          self.syrthesIHMCollector.View_factor_2D_form.Vf_Sy_2D_table : ['', 'float', 'float', 'float', ''], # Tableau des symétries pour les facteurs de forme en 2D
                          self.syrthesIHMCollector.View_factor_2D_form.Vf_Pe_2D_table : ['', 'float', 'float', 'angle', ''], # Tableau des periodicités pour les facteurs de forme en 2D
                          self.syrthesIHMCollector.View_factor_3D_form.Vf_Ip_3D_table : ['', 'float', 'float', 'float', ''], # Tableau des coordonnées de points pour les facteurs de forme en 3D
                          self.syrthesIHMCollector.View_factor_3D_form.Vf_Sy_3D_table : ['', 'float', 'float', 'float', 'float', ''], # Tableau des symétries pour les facteurs de forme en 3D
                          self.syrthesIHMCollector.View_factor_3D_form.Vf_Pe_3D_table : ['', 'float', 'float', 'float', 'float', 'float', 'float', 'angle', ''], # Tableau des periodicités pour les facteurs de forme en 3D
                          self.syrthesIHMCollector.Material_radiation_properties_form.Mrp_table : ['', 'band+int', '0<=float<=1', '+chkintstr', ''], # Tableau des propriété des matériaux en rayonnement
                          self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irt_table : ['', 'float>-273.15', '+chkpintstr', ''], # Tableau des conditions aux limites de températures imposées en rayonnement
                          self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irf_table : ['', 'band+int', 'float', '+chkpintstr', ''], # Tableau des conditions aux limites des flux imposées en rayonnement
                          self.syrthesIHMCollector.Solar_aspect_form.Sa_Hm_table : ['', 'float>-273.15', '0<float<1', '+chkintstr', ''], # Tableau de la modélissation de l'horizon
                          self.syrthesIHMCollector.Solar_aspect_form.Sa_Sm_table :['', '0<float<1', '+chkintstr', ''], # Tableau de la modélisation de l'ombrage
                          self.syrthesIHMCollector.Solar_aspect_form.Sa_Sht_table : ['', '+chkintstr', ''], # Tableau de la répartion de la chaleur solaire
                          self.syrthesIHMCollector.Initial_conditions_hum_TPv_form.Vch_Ic_TPv_table : ['', '', '', 'cond2float', '+chkintstr', ''], # Tableau des conditions volumique en humidité modèle 2 équations
                          self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form.Vch_Ic_TPvPt_table : ['', '', '', 'cond2float', '+chkintstr', ''], # Tableau des conditions volumique en humidité modèle 3 équations
                          self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form.Vch_St_TPv_table : ['', '', '', 'cond2float', 'cond2float', '+chkintstr', ''], # Tableau des termes sources en humidité modèle 2 équations
                          self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form.Vch_St_TPvPt_table : ['', '', '', 'cond2float', 'cond2float', '+chkintstr', ''], # Tableau des termes sources en humidité modèle 3 équations
                          self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_iso_2D_table : ['', '', '+chkintstr', ''], # Tableau des propriété matériaux en humidité
                          self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_iso_3D_table : ['', '', '+chkintstr', ''], # Tableau des propriété matériaux en humidité
                          self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_aniso_2D_table : ['', '', 'float', '+chkintstr', ''], # Tableau des propriété matériaux en humidité
                          self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_aniso_3D_table : ['', '', 'float','float','float','float','float','float','float','float','float','+chkintstr', ''], # Tableau des propriété matériaux en humidité
                          self.syrthesIHMCollector.Boundary_conditions_TPv_form.Bc_TPv_table : ['', '', 'float>-273.15Combo', 'cond+=float', 'cond+=float', 'cond+=float', 'cond+=float', '+chkpintstr', ''], # Tableau des conditions aux limites en humidité modèle 2 équations
                          self.syrthesIHMCollector.Boundary_conditions_TPvPt_form.Bc_TPvPt_table : ['', '', 'float>-273.15Combo', 'cond+=float', 'cond+=float', 'cond+=float', 'cond+=float', 'cond+=float', 'cond+=float', '+chkpintstr', ''], # Tableau des conditions aux limites en humidités modèle 3 équations


                          self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form.Cont_res_hum_TPv_table : ['', '', 'float>-273.15Combo', 'cond+=float', '+compintstr', '+compintstr', ''], # Tableau des resistances de contact en humidité modèle 2 équations
                          self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form.Cont_res_hum_TPvPt_table : ['', '', 'float>-273.15Combo', 'cond+=float', 'cond+=float', '+compintstr', '+compintstr', ''], # Tableau des resistances de contact en humidités modèle 3 équations


                          self.syrthesIHMCollector.Output_2D_form.Op_Dc_2D_table : ['', 'float', 'float', ''], # Tableau des définitions par coordonnés en 2D dans les sorties
                          self.syrthesIHMCollector.Output_3D_form.Op_Dc_3D_table : ['', 'float', 'float', 'float', ''], # Tableau des définitions par coordonnés en 3D dans les sorties
                          self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Sc_table : ['', 'stringContinu', '+chkintstr', ''], # Tableau du couplage surfacique pour le transfert de chaleur conjugué
                          self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Vc_table : ['', 'stringContinu', '+chkintstr', ''], # Tableau du couplage volumique pour le transfert de chaleur conjugué
                          self.syrthesIHMCollector.Solar_aspect_form.Csm_Db_table : ['', '+int', 'float', ''], # Tableau de la distribution du flux par bande dans la modélisation solaire
                          self.syrthesIHMCollector.Output_3D_form.Op_Sb_3D_table : ['', '+intstr', ''], # Tableau des bilans surfaciques 3D
                          self.syrthesIHMCollector.Output_2D_form.Op_Sb_2D_table : ['', '+intstr', ''], # Tableau des bilans surfaciques 2D
                          self.syrthesIHMCollector.Output_3D_form.Op_Vb_3D_table : ['', '+intstr', ''], # Tableau des bilans volumiques 3D
                          self.syrthesIHMCollector.Output_2D_form.Op_Vb_2D_table : ['', '+intstr', ''], # Tableau des bilans volumiques 2D
                          # fluid1d
                          self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table : ['', '+float', '+float', '+=float', '+chkintstr',''], # Tableau de la geometrie fluide 1D
                          self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table : ['', '', 'float>-273.15Combo', 'condfloat', '+chkintstr',''], # Tableau de la conditions initiales fluide 1D
                          self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table : ['', '', 'float', 'float', 'float', 'condfloat', 'float>-273.15Combo', ''], # Tableau de la Inlet fluide 1D en 3D
                          self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table : ['', '', 'float>-273.15Combo', 'cond+float', '+chkintstr', ''], # Tableau de la Heat Exchange fluide 1D en 3D
                          self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table : ['', '', 'condfloat', '+chkintstr', ''], # Tableau de la Flux condition fluide 1D en 3D
                          self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table : ['', '', 'cond+float', 'cond+float', 'cond+float', 'cond+float', '+chkintstr', ''], #Tableau de la Physical Proprieties fluide 1D
                          self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table : ['', '', 'condfloat', 'condfloat', '+chkintstr', ''],# Tableau de la Volumetric source fluide 1D
                          self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table : ['', '', 'cond+float', '+chkintstr', ''], # Tableau de la Linear head losses fluide 1D
                          self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table : ['', '', 'cond+float', '+chkintstr', ''], # Tableau de la Singular head losses fluide 1D
                          self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl : ['', '', 'float', 'float', 'float', 'cond+float', 'cond+float', 'cond+float', 'condfloat', ''], # Tableau de la Closed Lopp fluide 1D en 3D
                          self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl : ['', '', 'float', 'float', 'float', ''],# Tableau de la Thermal Closed Loop fluide 1D en 3D
                          self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP : ['', '', 'float', 'float', 'float', 'condfloat', 'condfloat', 'float>-273.15Combo' , ''],# Tableau pour Delta Pressure fluide 1D en 3D
                          self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table : ['', '', '+float','+chkintstr' , ''],# Tableau pour Delta Pressure fluide 1D en 3D
                          # fluid0d
                          self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table : ['', '+chkintstr', '+float', '+chkintstr',''], # Tableau de la geometrie fluide 0D
                          self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table : ['','','cond+float','+chkintstr',''], # Tableau de conditions aux limites fluid0d
                          self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table : ['','', 'volume+int','cond+float','cond+float', 'float>-273.15Combo',''], # Tableau des propriétés physiques d'un volume fluid0d
                          self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table : ['','', 'volume+int', 'cond+float', 'cond+float',''] # Tableau des conditions couplage 1d
                          }

        Red=QtGui.QBrush(QtGui.QColor(255, 170, 127))
        self.Red = Red
        Redc=QtGui.QColor(255, 170, 127)
        White = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        self.White = White
        Black = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        self.Black = Black
        if self.open==True:
            pass
        else:
            widget=QtWidgets.QApplication.focusWidget()
            if widget == None :
                return
            if a==None: # les valeurs de a,b sont "aléatoires" mais tournent autours de quelques types...
                table=b.tableWidget()
                a=table.row(b)
                b=table.column(b)
            elif b==None:
                table=a.tableWidget()
                b=table.column(a)
                a=table.row(a)
            #elif str(a).isdigit() and str(b).isdigit() : # Check_table() called from checkWhiteTable
            else :
                if type(widget) != QtWidgets.QTableWidget :
                    table = widget.parentWidget().parentWidget()
                else :
                    table = widget

            if type(widget)==QtWidgets.QLineEdit:
                table=widget.parentWidget()
                table=table.parentWidget()
            elif type(widget)==QtWidgets.QCheckBox:
                pass
            elif type(widget)==QtWidgets.QComboBox:
                pass
            else:
                table=widget

            if table not in self.table_check :
                return
            chtype=self.table_check[table]

            if not (type(b)==type(1)) : # pour corriger une erreur qui se produit si on vient de corriger une cellule "rouge" puis quitter toute de suite l'application
                return
            chtype=chtype[b]
            if table.item(a,b) == None :
                Item = QtWidgets.QTableWidgetItem()
                table.setItem(a,b,Item)

            # prologue
            table.blockSignals(True)
            #check old state of table (red or white
            whOld, u, v, nbRed = self.checkWhiteTable(table)

            # main test
            if chtype=='float': # Vérification pour les types de données flottant
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black :
                            self.checkFloat(table, a, b)
            elif chtype=='+float': # Vérification pour les types de données flottant strictement supérieur à 0
                if type(table.cellWidget(a, 0)) == QtWidgets.QCheckBox :
                    if table.cellWidget(a, 0).isChecked() :
                        if table.item(a,b) != None :
                            if table.item(a,b).background() != self.Black :
                                self.checkPFloat(table, a, b)
                else :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black :
                            self.checkPFloat(table, a, b)
            elif chtype=='+=float': # Vérification pour les types de données flottant strictement supérieur à 0
                if type(table.cellWidget(a, 0)) == QtWidgets.QCheckBox :
                    if table.cellWidget(a, 0).isChecked() :
                        if table.item(a,b) != None :
                            if table.item(a,b).background() != self.Black :
                                self.checkPEFloat(table, a, b)
                else :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black :
                            self.checkPEFloat(table, a, b)
            elif chtype=='0<=float<=1' : # Vérification pour les types de données flottant compris entre 0<x<=1
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black :
                            self.checkCondE0E1float(table, a, b)
            elif chtype=='cond+float': # Vérification pour les types de données flottant supérieur à 0 comparé
                # cond+float est toujours accompagné d'un combobox
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black and table.cellWidget(a,1).currentIndex()!=1: # combobox not PROGRAM, not FUNCTION
                            self.checkPFloat(table, a, b)
                        elif table.cellWidget(a,1).currentIndex()==1: # FUNCTION : we check continuous string
                            if len(str(table.item(a,b).text()).split())>1:
                                table.item(a,b).setText(''.join(str(table.item(a,b).text()).split()))
            elif chtype=='cond+=float': # Vérification pour les types de données flottant supérieur ou égale à 0 comparé
                # 'cond+=float' est toujours accompagné d'un combobox
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black and table.cellWidget(a,1).currentIndex()!=1: # combobox not PROGRAM, not FUNCTION
                            self.checkPEFloat(table, a, b)
                        elif table.cellWidget(a,1).currentIndex()==1: # FUNCTION : we check continuous string
                            if len(str(table.item(a,b).text()).split())>1:
                                table.item(a,b).setText(''.join(str(table.item(a,b).text()).split()))
            elif chtype=='condfloat': # Vérification pour les types de données flottant comparé
                # condfloat est toujours accompagné d'un combobox
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black and table.cellWidget(a,1).currentIndex()!=1: # combobox not PROGRAM, not FUNCTION
                            self.checkFloat(table, a, b)
                        elif table.cellWidget(a,1).currentIndex()==1: # FUNCTION : we check continuous string
                            if len(str(table.item(a,b).text()).split())>1:
                                table.item(a,b).setText(''.join(str(table.item(a,b).text()).split()))
            elif chtype=='cond0<=float<=1' : # Vérification pour les types de données flottant compris entre 0 et 1 comparé
                # 'cond0<=float<=1' est toujours accompagné d'un combobox
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black and table.cellWidget(a,1).currentIndex()!=1: # combobox not PROGRAM, not FUNCTION
                            self.checkCondE0E1float(table, a, b)
                        elif table.cellWidget(a,1).currentIndex()==1: # FUNCTION : we check continuous string
                            if len(str(table.item(a,b).text()).split())>1:
                                table.item(a,b).setText(''.join(str(table.item(a,b).text()).split()))
            elif chtype=='+chkintstr': # Vérification pour les types de données chaîne d'entiers strictement supérieur à 0
                # define a list of cells concerned by the verification
                #print 'chkintstr'
                self.cellsList = []
                for i in range(table.rowCount()) :
                    if table.cellWidget(i, 0).isChecked() :
                        if table.item(i,b) != None :
                            if table.item(i,b).background() != self.Black :
                                if type(table.cellWidget(i,2)) == QtWidgets.QComboBox : # Combobox of variables T, Pv, Pt -> extra verification
                                    if table.cellWidget(i,2).currentIndex() == table.cellWidget(a,2).currentIndex() :
                                        self.cellsList.append([i,b])
                                else:
                                    self.cellsList.append([i,b])

                for cell in self.cellsList :
                    self.checkIntstr(table, cell[0], cell[1])
            elif chtype=='+chkpintstr':
                # define a list of cells concerned by the verification
                #print 'chkintstr'
                self.cellsList = []
                for i in range(table.rowCount()) :
                    if table.cellWidget(i, 0).isChecked() :
                        if table.item(i,b) != None :
                            if table.item(i,b).background() != self.Black :
                                if type(table.cellWidget(i,2)) == QtWidgets.QComboBox : # Combobox of variables T, Pv, Pt -> extra verification
                                    if table.cellWidget(i,2).currentIndex() == table.cellWidget(a,2).currentIndex() :
                                        self.cellsList.append([i,b])
                                else:
                                    self.cellsList.append([i,b])

                for cell in self.cellsList :
                    self.checkPIntstr(table, cell[0], cell[1])
            elif chtype=='+compintstr': # Vérification pour les types de données chaîne d'entiers strictement superieur à 0 comparé
                # define a list of cells concerned by the verification
                self.cellsList = []
                if b==table.columnCount()-2: # b = colonne réf 2
                    c=b-1 # c = colonne réf 1
                else: # b = colonne réf 1
                    c=b+1 # c = colonne réf 2
                for i in range(table.rowCount()) :
                    if table.cellWidget(i, 0).isChecked() :
                        # don't touch black cells
                        if table.item(i,b) != None :
                            if table.item(i,b).background() != self.Black : self.cellsList.append([i,b])
                        if table.item(i,c) != None :
                            if table.item(i,c).background() != self.Black : self.cellsList.append([i,c])
                for cell in self.cellsList :
                    self.checkCompintstr(table, cell[0], cell[1])
            elif chtype=='+int': # Vérification pour les types de données entier strictement superieur à 0
                if type(table.cellWidget(a, 0)) == QtWidgets.QCheckBox : # special treatment for By_Block_table which has not checkbox
                    if table.cellWidget(a, 0).isChecked() :
                        if table.item(a,b) != None :
                            if table.item(a,b).background() != self.Black :
                                self.checkPInt(table, a, b)
                else :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black :
                            self.checkPInt(table, a, b)
            elif chtype=='angle': # Vérification pour les types de données entier compris entre 0 et 360 comparé
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black :
                            self.checkAngle(table, a, b)
            elif chtype=='angleCombo': # Vérification pour les types de données entier compris entre 0 et 360 comparé
                # 'angleCombo' est toujours accompagné d'un combobox
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black and table.cellWidget(a,1).currentIndex()!=1: # combobox not PROGRAM, not FUNCTION
                            self.checkAngle(table, a, b)
                        elif table.cellWidget(a,1).currentIndex()==1: # FUNCTION : we check continuous string
                            if len(str(table.item(a,b).text()).split())>1:
                                table.item(a,b).setText(''.join(str(table.item(a,b).text()).split()))
            elif chtype=='spfloat': # Vérification pour les types de données flottant comparé à une donnée annexe
                # define a list of cells concerned by the verification
                self.cellsList = []
                for i in range(table.rowCount()) :
                    if table.cellWidget(i, 0).isChecked() :
                        if table.item(i,b) != None :
                            self.cellsList.append([i,b])
                for cell in self.cellsList :
                    self.checkSpfloat(table, cell[0], cell[1])
            elif chtype=='spfloat2': # Vérification pour les types de données flottant comparé à une donnée annexe
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        self.checkPFloat(table, a, b) # > 0
                        if table.item(a+1,b-1) != None : # check also the next lower-band
                            self.checkSpfloat(table, a+1, b-1)
            elif chtype=='band+int': # Vérification pour les types de données entier strictement positif comparé aux bandes spectrales
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        self.checkBandInt(table, a, b)
            elif chtype=='volume+int': # Vérification pour les types de données entier strictement positif comparé aux volumes 0D + '+chkpintstr'
                self.cellsList = []
                for i in range(table.rowCount()) :
                    if table.cellWidget(i, 0).isChecked() :
                        if table.item(i,b) != None :
                            if table.item(i,b).background() != self.Black :
                                self.cellsList.append([i,b])
                for cell in self.cellsList :
                    self.checkPIntstr(table, cell[0], cell[1])
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        self.checkVolumeInt(table, a, b)
            elif chtype=='float>-273.15Combo': # Vérification pour les types de données flottant strictement supérieur à -273.15
                # 'float>-273.15Combo' est toujours accompagné d'un combobox
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black and table.cellWidget(a,1).currentIndex()!=1: # combobox not PROGRAM, not FUNCTION
                            self.checkFloat273(table, a, b)
                        elif table.cellWidget(a,1).currentIndex()==1: # FUNCTION : we check continuous string
                            if len(str(table.item(a,b).text()).split())>1:
                                table.item(a,b).setText(''.join(str(table.item(a,b).text()).split()))
            elif chtype=='float>-273.15': # Vérification pour les types de données flottant strictement supérieur à -273.15
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black :
                            self.checkFloat273(table, a, b)
            elif chtype=='0<float<1' : # Vérification pour les types de données flottant strictement compris entre 0 et 1
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black :
                            self.checkCond01float(table, a, b)
            elif chtype=='cond2float': # Vérification pour les types de données sur un flottants comparé avec 2 autres données
                cmb2=table.cellWidget(a,2)
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black and table.cellWidget(a,1).currentIndex()!=1: # combobox not PROGRAM, not FUNCTION
                            if cmb2.currentIndex()==0: # temperature
                                self.checkFloat273(table, a, b)
                            else : # pressure
                                self.checkPEFloat(table, a, b)
                        elif table.cellWidget(a,1).currentIndex()==1: # FUNCTION : we check continuous string
                            if len(str(table.item(a,b).text()).split())>1:
                                table.item(a,b).setText(''.join(str(table.item(a,b).text()).split()))
            elif chtype=='+intstr': # Vérification pour les types de données chaîne d'entiers strictement supérieur à 0
                if table.cellWidget(a, 0).isChecked() :
                    if table.item(a,b) != None :
                        if table.item(a,b).background() != self.Black :
                            self.checkIntstrNotComp(table, a, b)
            elif chtype=='stringContinu': # Vérification pour les types de données chaîne de caractère sans espace
                if len(str(table.item(a,b).text()).split())>1:
                    table.item(a,b).setBackground(Red)
                    errmes="Please enter a string without space"
                    table.item(a,b).setToolTip(errmes)
                    self.errflag=True
                    self.Errorcallback(errmes,table)
                    #raise Exception(errmes)
                else:
                    table.item(a,b).setBackground(White)
                    table.item(a,b).setToolTip('')
                pass

            elif chtype=='': # Aucune verification de type de donnée
                #print "rien"
                pass

            #epilogue
            #make sure that all cells are white before restoring GUI's state
            wh, u, v, nbRed = self.checkWhiteTable(table)
            if wh and whOld==False: # if table was red and now is totally white -> restore GUI's state
                print("setOk")
                self.SetOk(table)
                self.errflag=False

            table.blockSignals(False)

# ébauche du nouveau contrôle pour les chaînes d'entier positifs avec vérification doublement croisé pour l'humidité
##            elif chtype=='+chkintstrhum': # Vérification pour les types de données chaîne d'entiers strictement supérieur à 0 pour l'humidité
##                i=0
##                line=-1
##                stritem=str(table.item(a,b).text()).split()
##                sameflag=True
##                flag=True
##                if table.item(a,b).text()=='':
##                    i=0
##                    while i<table.rowCount() and table.item(i,b)!=None:
##                        table.item(i,b).setBackground(White)
##                        table.item(i,b).setToolTip('')
##                        i=i+1
##                    self.SetOk(table)
##                    self.errflag=False
##                while i<len(stritem):
##                    if not(stritem[i].isdigit()):
##                        if stritem[i]=='-1' and len(stritem)==1:
##                            flag=True
##                            j=0
##                            while table.item(j, table.columnCount()-2)!=None:
##                                if table.cellWidget(j, 0).isChecked() and table.item(j, table.columnCount()-2).text()!='-1' and table.item(j, table.columnCount()-4).currentIndex()!=able.item(a,b).currentIndex():
##                                    table.item(j, table.columnCount()-2).setBackground(Red)
##                                    errmes="Please enter one or several int>0"
##                                    table.item(a,b).setToolTip(errmes)
##                                    self.errflag=True
##                                    self.Errorcallback(errmes,table)
##                                    raise(errmes)
##                                j=j+1
##
##                        else:
##                            flag=False
##                    elif int(stritem[i])<0:
##                        if int(stritem[i])==-1 and len(stritem)==1:
##                            flag=True
##                            self.allcheck(table)
##                        else:
##                            flag=False
##                    j=0
##                    while j<table.rowCount():
##                        if table.item(j,b)!=None and table.item(j,b)!=table.item(a,b) and table.item(j,b-2).currentIndex()!=table.item(a,b-2).currentIndex():
##                            if not(str(table.item(j,b).text()).find(stritem[i])==-1)==True:
##                                flag=False
##                                line=j
##
##                            else:
##                                flag=True
##                        j=j+1
##                    i=i+1
##                if flag==False:
##                    if line==-1:
##                        table.item(a,b).setBackground(Red)
##                        errmes="Please enter int>0 or a single int=-1"
##                        table.item(a,b).setToolTip(errmes)
##                        self.errflag=True
##                        self.Errorcallback(errmes,table)
##                        raise(errmes)
##
##                    else:
##                        table.item(a,b).setBackground(Red)
##                        errmes="Please don't enter same reference twice"
##                        table.item(a,b).setToolTip(errmes)
##                        table.item(line,b).setBackground(Red)
##                        table.item(line,b).setToolTip(errmes)
##                        errline=line
##                        self.errflag=True
##                        self.Errorcallback(errmes,table)
##                        raise(errmes)
##
##                else:
##                    if line==-1:
##                        i=0
##                        while i<table.rowCount() and table.item(i,b)!=None:
##                            table.item(i,b).setBackground(White)
##                            table.item(i,b).setToolTip('')
##                            i=i+1
##                        self.SetOk(table)
##                        self.errflag=False
##                    else:
##                        table.item(a,b).setBackground(Red)
##                        errmes="Please don't enter same reference twice"
##                        table.item(a,b).setToolTip(errmes)
##                        table.item(line,b).setBackground(Red)
##                        table.item(line,b).setToolTip(errmes)
##                        errline=line
##                        self.errflag=True
##                        self.Errorcallback(errmes,table)
##                        raise(errmes)

# assure that there is only one field for all tabs of current treeview
# count the number of "-1" in this table
#                            j=0
#                            count = 0
#                            coupable = -1
#                            while j < table.rowCount : # looking at reference field
#                                acell = table.item(j, table.columnCount()-2) # take a cell
#                                if table.cellWidget(j, 0).isChecked() and acell != None and acell.text() != '' : # if not an empty cell
#                                    count = count + 1 # count
#                                    if count > 1 : # if more than 1 row, raise error
#                                        acell.setBackground(Red)
#                                        errmes="-1 is only accepted when there is only one row in this set of tables"
#                                        table.item(a,b).setToolTip(errmes)
#                                        self.errflag=True
#                                        self.Errorcallback(errmes,table)
#                                        raise(errmes)
#                                j = j + 1


#                            while table.item(j, table.columnCount()-2)!=None: # looking at reference field
#                                if table.cellWidget(j, 0).isChecked() and table.item(j, table.columnCount()-2).text()!='-1' :
#                                    table.item(j, table.columnCount()-2).setBackground(Red)
#                                    errmes="-1 is only accepted when there is only one row in this set of tables" # Please enter one or several int>0"
#                                    table.item(a,b).setToolTip(errmes)
#                                    self.errflag=True
#                                    self.Errorcallback(errmes,table)
#                                    raise(errmes)
#                                j=j+1

    def checkCond01float(self, table, a, b): # Vérification pour les types de données flottant compris entre 0 et 1 comparé
        flag = True
        if table.item(a,b).text()!='' :
            if len(str(table.item(a,b).text()).split())>1 or not self.isFloat(str(table.item(a,b).text())) :
                flag = False
            elif float(table.item(a,b).text())<=0 or float(table.item(a,b).text())>=1 :
                flag = False

        if flag==False :
            table.blockSignals(True)
            table.item(a,b).setBackground(self.Red)
            errmes="Please enter a real number strictly between 0 and 1 (0<real<1)"
            table.item(a,b).setToolTip(errmes)
            self.errflag=True
            self.Errorcallback(errmes,table)
        else:
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkCondE0E1float(self, table, a, b): # Vérification pour les types de données flottant compris entre 0 et 1 comparé
        flag = True
        if table.item(a,b).text()!='' :
            if len(str(table.item(a,b).text()).split())>1:
                flag = False
            elif not self.isFloat(str(table.item(a,b).text())):
                if str(table.item(a,b).text())[0] != '$':
                    flag = False
            elif float(table.item(a,b).text())<0 or float(table.item(a,b).text())>1 :
                flag = False

        if flag==False :
            table.blockSignals(True)
            table.item(a,b).setBackground(self.Red)
            errmes="Please enter a real number between 0 and 1 (0<=real<=1)"
            table.item(a,b).setToolTip(errmes)
            self.errflag=True
            self.Errorcallback(errmes,table)
        else:
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkFloat273(self, table, a, b): # Vérification pour les types de données flottant strictement supérieur à -273.15
        flag = True
        if table.item(a,b).text()!='' :
            if len(str(table.item(a,b).text()).split())>1:
                flag = False
            elif not self.isFloat(str(table.item(a,b).text())):
                if str(table.item(a,b).text())[0] != '$':
                    flag = False
            elif float(table.item(a,b).text()) <= -273.15 :
                flag = False
        if flag==False :
            table.blockSignals(True)
            table.item(a,b).setBackground(self.Red)
            errmes="Please enter a real number greater than -273.15"
            table.item(a,b).setToolTip(errmes)
            self.errflag=True
            self.Errorcallback(errmes,table)
        else:
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkSpfloat(self, table, a, b): # Vérification pour les types de données flottant pour Spectral Parameter
        flag = True
        previtem=table.item(a,b) # by default, previous cell = the lower-band cell itself
        i=a-1
        while i>=0: # search for the last upper band
            if table.cellWidget(i,0).isChecked():
                previtem=table.item(i,b+1)
                i=0
            i=i-1
        if table.item(a,b).text()!='' :
            if len(str(table.item(a,b).text()).split())>1:
                flag = False
            elif not self.isFloat(str(table.item(a,b).text())):
                if str(table.item(a,b).text())[0] != '$':
                    flag = False
            elif table.item(a,b).text() != previtem.text() :
                flag = False

        if flag==False :
            table.blockSignals(True)
            table.item(a,b).setBackground(self.Red)
            errmes="Please enter a real number equal to the previous upper band"
            table.item(a,b).setToolTip(errmes)
            self.errflag=True
            self.Errorcallback(errmes,table)
        else:
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkPFloat(self, table, a, b): # Vérification pour les types de données flottant strictement supérieur à 0
        flag = True
        if table.item(a,b).text()!='' :
            if len(str(table.item(a,b).text()).split())>1:
                flag = False
            elif not self.isFloat(str(table.item(a,b).text())):
                if str(table.item(a,b).text())[0] != '$':
                    flag = False
            elif float(table.item(a,b).text()) <= 0 :
                flag = False

        if flag==False :
            table.blockSignals(True)
            table.item(a,b).setBackground(self.Red)
            errmes="Please enter a strictly positive real number (>0)"
            table.item(a,b).setToolTip(errmes)
            self.errflag=True
            self.Errorcallback(errmes,table)
        else:
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkPEFloat(self, table, a, b): # Vérification pour les types de données flottant strictement supérieur à 0
        flag = True
        if table.item(a,b).text()!='' :
            if len(str(table.item(a,b).text()).split())>1:
                flag = False
            elif not self.isFloat(str(table.item(a,b).text())):
                if str(table.item(a,b).text())[0] != '$':
                    flag = False
            elif float(table.item(a,b).text()) < 0 :
                flag = False

        if flag==False :
            table.blockSignals(True)
            table.item(a,b).setBackground(self.Red)
            errmes="Please enter a positive real number (>=0)"
            table.item(a,b).setToolTip(errmes)
            self.errflag=True
            self.Errorcallback(errmes,table)
        else:
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkFloat(self, table, a, b): # Vérification pour les types de données flottant
        flag = True
        if table.item(a,b).text()!='' :
            if len(str(table.item(a,b).text()).split())>1:
                flag = False
            elif not self.isFloat(str(table.item(a,b).text())):
                if str(table.item(a,b).text())[0] != '$':
                    flag = False

        if flag==False :
            table.blockSignals(True)
            table.item(a,b).setBackground(self.Red)
            errmes="Please enter a real number"
            table.item(a,b).setToolTip(errmes)
            self.errflag=True
            self.Errorcallback(errmes,table)
        else:
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkAngle(self, table, a, b): # Vérification pour les types de données entier compris entre 0 et 360 comparé
        flag = True
        if table.item(a,b).text()!='' :
            if len(str(table.item(a,b).text()).split())>1 or not str(table.item(a,b).text()).isdigit() :
                flag = False
            elif not str(table.item(a,b).text()).isdigit():
                if str(table.item(a,b).text())[0] != '$':
                    flag = False
            elif int(table.item(a,b).text())<0 or int(table.item(a,b).text())>360 :
                flag = False

        if flag==False :
            table.blockSignals(True)
            table.item(a,b).setBackground(self.Red)
            errmes="Please enter an integer between 0 and 360"
            table.item(a,b).setToolTip(errmes)
            self.errflag=True
            self.Errorcallback(errmes,table)
        else:
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkPInt(self, table, a, b): # Vérification pour les types de données entier strictement superieur à 0
        flag = True
        if table.item(a,b).text()!='' :
            if len(str(table.item(a,b).text()).split())>1 or not str(table.item(a,b).text()).isdigit() :
                flag = False
            elif int(table.item(a,b).text())<=0 :
                flag = False

        if flag==False :
            table.blockSignals(True)
            table.item(a,b).setBackground(self.Red)
            errmes="Please enter a strictly positive integer"
            table.item(a,b).setToolTip(errmes)
            self.errflag=True
            self.Errorcallback(errmes,table)
        else:
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkBandInt(self, table, a, b): # Vérification pour les types de données entier strictement positif comparé aux bandes spectrales
        flag=False
        if table.item(a,b).text()=='' :
            flag = True
        else :
            i=0
            while i<self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table.rowCount():
                if self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table.item(i,1)!=None and self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table.cellWidget(i, 0).isChecked()==True :
                    if table.item(a,b).text()==self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table.item(i,1).text():
                        flag=True
                        break
                i=i+1

        if flag==False :
            table.blockSignals(True)
            table.item(a,b).setBackground(self.Red)
            errmes="Please enter a band already defined in Spectral Band Definition table"
            table.item(a,b).setToolTip(errmes)
            self.errflag=True
            self.Errorcallback(errmes,table)
        else:
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkVolumeInt(self, table, a, b): # Vérification pour les types de données entier strictement positif comparé aux bandes spectrales
        flag=False
        if table.item(a,b).text()=='' :
            flag = True
        else :
            i=0
            while i<self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table.rowCount():
                if self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table.item(i,1)!=None and self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table.cellWidget(i, 0).isChecked()==True:
                    if table.item(a,b).text()==self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table.item(i,1).text():
                        flag=True
                        break
                i=i+1

        if flag==False :
            table.blockSignals(True)
            table.item(a,b).setBackground(self.Red)
            errmes="Please enter a volume already defined in Geometry table"
            table.item(a,b).setToolTip(errmes)
            self.errflag=True
            self.Errorcallback(errmes,table)
        else:
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkIntstrNotComp(self, table, a, b): # Vérification pour les types de données chaîne d'entiers strictement supérieur à 0 non comparé avec d'autres cellules
        line=-1
        stritem=str(table.item(a,b).text()).split()
        flag=True
        if table.item(a,b).text()=='': # si cellule vide --> la cellule (a,b) est automatiquement ok
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

        i=0
        while stritem != [] and i<len(stritem): # loops through toutes les réf de la cellule (a,b)
            if stritem.count(stritem[i]) > 1 : # ref is present twice in a cell
                flag = False
                break
            elif not(stritem[i].isdigit()): # if letter or negative number in the form of string
                if not(stritem[0]=='-1' and len(stritem)==1):
                    flag=False # there is not only "-1" in the cell, negative number other than -1 is not accepted
                    break
            elif int(stritem[0])<=0: # negative number - the handle will never go here ?
                if not(int(stritem[i])==-1 and len(stritem)==1) :
                    flag=False
                    break
            i=i+1
            # end check the item(a,b)

        if flag==False :
            if line==-1: # no same ref, error from letter or negative number
                table.blockSignals(True)
                table.item(a,b).setBackground(self.Red)
                errmes="Please enter distinctive positif integers (>0) or -1 only"
                table.item(a,b).setToolTip(errmes)
                self.errflag=True
                self.Errorcallback(errmes,table)
                #raise Exception(errmes)
            else:
                table.blockSignals(True)
                table.item(a,b).setBackground(self.Red)
                errmes="Please don't enter the same reference twice"
                table.item(a,b).setToolTip(errmes)
                table.item(line,b).setBackground(self.Red)
                table.item(line,b).setToolTip(errmes)
                errline=line
                self.errflag=True
                self.Errorcallback(errmes,table)
                #raise Exception(errmes)
        else: # flag = True
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkPIntstr(self, table, a, b): # Vérification pour les types de données chaîne d'entiers strictement supérieur à 0
        line=-1
        stritem=str(table.item(a,b).text()).split()
        flag=True
        if table.item(a,b).text()=='': # si cellule vide --> la cellule (a,b) est automatiquement ok
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

        i=0
        while stritem != [] and i<len(stritem): # loops through toutes les réf de la cellule (a,b)
            if stritem.count(stritem[i]) > 1 : # ref is present twice in a cell
                flag = False
                break
            elif not(stritem[i].isdigit()): # if letter or negative number in the form of string
                flag=False
                break
            elif int(stritem[0])<=0: # negative number - the handle will never go here ?
                flag=False
                break
            else:
                # on the same column, search for error of type "same ref"
                for cell in self.cellsList : # compare with other cells in the list pre-defined
                    if flag == False : break
                    if not(a == cell[0] and b == cell[1]) :
                        it = table.item(cell[0], cell[1])
                        stritemLoop = str(it.text()).split()
                        for stritemLoopitem in stritemLoop :
                            if stritemLoopitem == stritem[i] :
                                flag = False
                                line=cell[0] # mark this line to color it below
                                break
            i=i+1
            # end check the item(a,b)

        if flag==False :
            if line==-1: # no same ref, error from letter or negative number
                table.blockSignals(True)
                table.item(a,b).setBackground(self.Red)
                errmes="Please enter distinctive positif integers (>0)"
                table.item(a,b).setToolTip(errmes)
                self.errflag=True
                self.Errorcallback(errmes,table)
                #raise Exception(errmes)
            else:
                table.blockSignals(True)
                table.item(a,b).setBackground(self.Red)
                errmes="Please don't enter the same reference twice"
                table.item(a,b).setToolTip(errmes)
                table.item(line,b).setBackground(self.Red)
                table.item(line,b).setToolTip(errmes)
                errline=line
                self.errflag=True
                self.Errorcallback(errmes,table)
                #raise Exception(errmes)
        else: # flag = True
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkIntstr(self, table, a, b): # Vérification pour les types de données chaîne d'entiers strictement supérieur à 0
        line=-1
        stritem=str(table.item(a,b).text()).split()
        flag=True
        if table.item(a,b).text()=='': # si cellule vide --> la cellule (a,b) est automatiquement ok
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

        i=0
        while stritem != [] and i<len(stritem): # loops through toutes les réf de la cellule (a,b)
            if stritem.count(stritem[i]) > 1 : # ref is present twice in a cell
                flag = False
                break
            elif not(stritem[i].isdigit()): # if letter or negative number in the form of string
                if not(stritem[0]=='-1' and len(stritem)==1):
                    flag=False # there is not only "-1" in the cell, negative number other than -1 is not accepted
                    break
            elif int(stritem[0])<=0: # negative number - the handle will never go here ?
                if not(int(stritem[i])==-1 and len(stritem)==1) :
                    flag=False
                    break
            else:
                # on the same column, search for error of type "same ref"
                for cell in self.cellsList : # compare with other cells in the list pre-defined
                    if flag == False : break
                    if not(a == cell[0] and b == cell[1]) :
                        it = table.item(cell[0], cell[1])
                        stritemLoop = str(it.text()).split()
                        for stritemLoopitem in stritemLoop :
                            if stritemLoopitem == stritem[i] :
                                flag = False
                                line=cell[0] # mark this line to color it below
                                break
            i=i+1
            # end check the item(a,b)

        if flag==False :
            if line==-1: # no same ref, error from letter or negative number
                table.blockSignals(True)
                table.item(a,b).setBackground(self.Red)
                errmes="Please enter distinctive positif integers (>0) or -1 only"
                table.item(a,b).setToolTip(errmes)
                self.errflag=True
                self.Errorcallback(errmes,table)
                #raise Exception(errmes)
            else:
                table.blockSignals(True)
                table.item(a,b).setBackground(self.Red)
                errmes="Please don't enter the same reference twice"
                table.item(a,b).setToolTip(errmes)
                table.item(line,b).setBackground(self.Red)
                table.item(line,b).setToolTip(errmes)
                errline=line
                self.errflag=True
                self.Errorcallback(errmes,table)
                #raise Exception(errmes)
        else: # flag = True
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def checkCompintstr(self, table, a, b):
        stritem=str(table.item(a,b).text()).split()
        flag=True
        if table.item(a,b).text()=='': # si cellule vide --> la cellule (a,b) est automatiquement ok
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

        i=0
        while stritem != [] and i<len(stritem): # loops through toutes les réf de la cellule (a,b)
            if stritem.count(stritem[i]) > 1 : # ref is present twice in a cell
                flag = False
                break
            elif not(stritem[i].isdigit()) or int(stritem[i])<=0 :
                flag=False  # check positif
                break
            else :
                for cell in self.cellsList : # compare with other cells in the list pre-defined
                    if flag == False : break
                    if not(a == cell[0] and b == cell[1]) :
                        it = table.item(cell[0], cell[1])
                        stritemLoop = str(it.text()).split()
                        for stritemLoopitem in stritemLoop :
                            if stritemLoopitem == stritem[i] :
                                flag = False
                                break
            i=i+1

        if flag==False :
            table.blockSignals(True)
            table.item(a,b).setBackground(self.Red)
            errmes="Please enter distinctive strictly positif integers (>0)"
            table.item(a,b).setToolTip(errmes)
            self.errflag=True
            self.Errorcallback(errmes,table)
        else:
            table.blockSignals(True)
            table.item(a,b).setBackground(self.White)
            table.item(a,b).setToolTip('')

    def Errorcallback(self, errmes, table):
        print(self.errflag)

        if self.errflag==True and self.errwinflag==False:
            self.Errorwindow(errmes,table)
            self.Errwin.show()
            self.errwinflag=True

    def Errorwindow(self, errmes, table): # Fonction permettant la désactivation du reste de l'IHM et affichage fenêtre d'erreur
        self.Errwin=QtWidgets.QMessageBox()
        self.Errwin.setText(errmes)
        self.Errwin.addButton("Ok",QtWidgets.QMessageBox.YesRole)
        self.Errwin.setIcon(QtWidgets.QMessageBox.Critical)
        self.Errwin.setWindowTitle("Input error")
        self.treeWidget.setDisabled(True)
        self.toolBar.setDisabled(True)
        self.toolBar_2.setDisabled(True)
        self.menubar.setDisabled(True)
        Tab=table
        while type(Tab)!=QtWidgets.QTabWidget and Tab!=None :
            Tab=Tab.parentWidget()
            if type(Tab)==QtWidgets.QTabWidget:
                index=Tab.currentIndex()
                i=0
                while (i<=Tab.count()):
                    Tab.setCurrentIndex(i)
                    if Tab.currentIndex()!=index:
                        Tab.setTabEnabled(i,False)
                    i=i+1
                Tab.setCurrentIndex(index)
            else:
                pass

    def checkWhiteTable(self, table):
    # return True if all cells (in used) in the table are white and whitening cells not in used
        Red=QtGui.QBrush(QtGui.QColor(255, 170, 127))
        self.Red = Red
        Redc=QtGui.QColor(255, 170, 127)
        White = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        self.White = White
        Black = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        self.Black = Black

        flag = True
        i = 0
        j = 0
        u = -1 # row of the 1st red cell
        v = -1 # column of the 1st red cell
        nbRed = 0 # number of red cells
        for i in range(table.rowCount()) :
            for j in range(table.columnCount()) :
                if table.item(i,j)!=None :
                    if table.item(i,j).background() != self.Black : # don't touch black cells
                        if type(table.cellWidget(i, 0)) == QtWidgets.QCheckBox : # for table with QCheckBox
                            if table.cellWidget(i,0).isChecked() :
                                if table.item(i,j).background() == self.Red :
                                    flag = False
                                    nbRed += 1
                                    if nbRed == 1 : # enregistrer les coord. de la 1ere cellule rouge
                                        u = i
                                        v = j
                            else : # whitening cells not in used
                                table.item(i,j).setBackground(self.White)
                        else : # for table without QCheckBox
                            if table.item(i,j).background() == self.Red :
                                flag = False
                                nbRed += 1
                                if nbRed == 1 : # enregistrer les coord. de la 1ere cellule rouge
                                    u = i
                                    v = j

        return flag, u, v, nbRed

    def forcedCheck_table(self, state):
    # this function is called from checkbox/combobox signal
        widget=QtWidgets.QApplication.focusWidget()
        try:
            table = widget.parentWidget().parentWidget()
        except:
            return
        #table.blockSignals(True)
        if type(widget)==QtWidgets.QCheckBox : # the checkbox is checked
            if state == 2 :
                # determine the 1st and the last column to be darken
                j = 2 # choisi par défaut
                dic_1stcol = {1:2,5:2, # checkbox avec combobox
                              2:1, # only checkbox
                              3:3,4:3 # checkbox + 2 combobox
                              }
                j = dic_1stcol[self.dic_Table_type[table]]
                while j<=table.columnCount()-2:
                    if table.currentRow() == -1 : # aucune case est cliquée
                        self.Check_table(0, j)
                    else:
                        self.Check_table(table.currentRow(), j)
                    j+=1
            else :
                if table == self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table : # special case for lower band - upper band table
                    # because, unlike other tables, the action of uncheck a cell can CAUSE problem
                    i=table.rowCount()-1
                    while i>=0: # search for the last upper band
                        if table.cellWidget(i,0).isChecked():
                            self.Check_table(i, 2)
                            i=0
                        i=i-1
                    table.blockSignals(False)
                self.checkWhiteTable(table)
        elif type(widget)==QtWidgets.QComboBox : # combobox
            if widget.currentText() != "Function" : # verify the table if CONSTANT or PROGRAM or T, Pv, Pt
                # determine the 1st and the last column to be darken
                j = 2 # par hazard, par défaut
                dic_1stcol = {1:2,5:2, # checkbox avec combobox
                              3:3,4:3 # checkbox + 2 combobox
                              }
                j = dic_1stcol[self.dic_Table_type[table]]
                if j==3 : # checkbox + 2 combobox -> check all cells (not optimized but necessary)
                    while j<=table.columnCount()-2:
                        i=0
                        while i < table.rowCount() :
                            self.Check_table(i, j)
                            i+=1
                        j+=1

                while j<=table.columnCount()-2: # checkbox + 1 combobox -> check only the current row
                    if table.currentRow() == -1 : # aucune case est cliquée
                        self.Check_table(0, j)
                    else:
                        self.Check_table(table.currentRow(), j)
                    j+=1
#            elif widget.currentText() == "Function":
#                print 'Function'
#                print "1 ",self.table_check[table]
#                j=2
#                dic_1stcol = {1:2,5:2, # checkbox avec combobox
#                              3:3,4:3 # checkbox + 2 combobox
#                              }
#                j=dic_1stcol[self.dic_Table_type[table]]
#                if j==3:
#                    while j<=table.columnCount()-2:
#                        i=0
#                        while i < table.rowCount() :
#                            self.Check_table(i, j)
#                            print "2 ",self.table_check[table][j]
#                            i+=1
#                        j+=1
#
#                while j<=table.columnCount()-2: # checkbox + 1 combobox -> check only the current row
#                    if table.currentRow() == -1 : # aucune case est cliquée
#                        self.Check_table(0, j)
#                    else:
#                        self.Check_table(table.currentRow(), j)
#                        print "3 ",self.table_check[table][j]
#                    j+=1
#                pass
        wh, u, v, nbRed = self.checkWhiteTable(table)
        if wh : # if the table is totally white -> restore GUI's state
            self.SetOk(table)
            self.errflag=False

    def SetOk(self, table): # fonction de rappel antagoniste permettant la réactivation de l'IHM
        self.errwinflag=False
        self.treeWidget.setEnabled(True)
        self.toolBar.setEnabled(True)
        self.toolBar_2.setEnabled(True)
        self.menubar.setEnabled(True)
        Tab=table # Tab will be parent of itself until its type is QTabWidget
        while type(Tab)!=QtWidgets.QTabWidget and Tab!=None:
            Tab=Tab.parentWidget()
        if type(Tab)==QtWidgets.QTabWidget:
            index=Tab.currentIndex()
            i=0
            while (i<Tab.count()):
                Tab.setCurrentIndex(i)
                if Tab.currentIndex()!=index:
                    Tab.setTabEnabled(i,True)
                i=i+1
            Tab.setCurrentIndex(index)
        else:
            pass

    def isFloat(self, x):
        try :
            x = str(x)
            y = float(x)
            return True
        except ValueError:
            return False
