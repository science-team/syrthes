# -*- coding: utf-8 -*-

#-------------------------------------------------------------------------------
# importation des bibliothèques standard
#-------------------------------------------------------------------------------

import os, sys, string, subprocess, shutil, time, datetime, filecmp, threading, re
from threading import Thread

#-------------------------------------------------------------------------------
# importation des bibliothèques IHM
#-------------------------------------------------------------------------------
from PyQt5 import QtCore, QtGui, QtWidgets
# numpy est utile pour cx_freeze en particulier sur ubuntu
import numpy
import PyQt5.Qt as Qt
from PyQt5.QtCore import pyqtSignal

try:
    if os.name=='nt':
        import winreg
except:
    print("ATTENTION PROBLEME A L'IMPORTATION DE _WINREG")

#-------------------------------------------------------------------------------
# importation de la classe de la fenêtre principale
#-------------------------------------------------------------------------------
from ui_SyrthesMainwin80060023 import Ui_Syrthes_Mainwindow #classe de la fenêtre principale
from calcView import calcView
from syrthesIHMContext import syrthesIHMContext

################## importation fonctionnalité #############################
from OpenFile import OpenFile # classe d'ouverture de fichier
from savingtools import boolFilledTable, saveTable, boolEmptyRow, boolFilledRow

from StatusTip import StatusTip # fonction des messages de la barre de statut
from ToolTip import ToolTip # fonction des infobulles
from WhatsThis import WhatsThis, funcReadWhat, find_names # fonctions des what's this

#from internationalisation import internationalisation # classe de l'internationalisation
#from calculation_progress1_4 import Ui_calculation_progress, clMyTabWidget # classe de la fenêtre du progrès de calcul

from Check_table import Check_table # classe de vérification des types de données entrés dans les tableaux
from Table_callback import Table_callback # classe des fonctions de rappels des tableaux

from SYRTHES_IHMCollector import SYRTHES_IHMCollector, DialogNewHandler
from outputTimes import Output_Times_FormHandler
from customMessageBox import CustomMessageBox

class MainView(QtWidgets.QMainWindow, Ui_Syrthes_Mainwindow, OpenFile, Check_table):

    titleChanged = pyqtSignal()

    def __init__(self, parent=None, embedded=False, salomeCfdstudy=False): # initialisation de toute les classes de l'IHM (excepté le suivi de calcul)
    # l'option embedded=True ne marche pas en mode exécutable
        self.salomecfdstudy = salomeCfdstudy
        QtWidgets.QMainWindow.__init__(self)
        Ui_Syrthes_Mainwindow.__init__(self)
        self.setupUi(self)
        self.retranslateUi(self)
        self.pdfReader = ""

        syrthesIHMContext.setEmbedded(embedded)

        self.syrthesIHMCollector = SYRTHES_IHMCollector(parent)
        self.case = clCase()

        pathname = os.getenv('SYRTHES4_HOME')+ os.sep + "lib"+ os.sep + "syrthesGui"
        if embedded :
            import imp
            fp, pathname, description = imp.find_module("SyrthesMain") # marche pas en mode exécutable
            syrthesIHMContext.setExeFile(pathname)
            self.action_New_file.setEnabled(False)
            self.action_Open.setEnabled(False)
            self.action_Quit.setEnabled(False)


            # redefine DialogNew with "parent"
            #self.syrthesIHMCollector.DialogNew = DialogNewHandler(parent)

        # Renvoie la taille de l'écran. Cette ligne nous permettra par la suite de connaître la hauteur et la largeur de l'écran.
        size_ecran = QtWidgets.QDesktopWidget().screenGeometry()
        # Même chose que ci-dessus mais avec la fenêtre de l'application.
        size_fenetre = self.geometry()
        # La fonction move() permet de déplacer la fenêtre aux coordonnées passées en arguments.
        self.move((size_ecran.width()-size_fenetre.width())//2, (size_ecran.height()-size_fenetre.height())//2)




        #style=QStyle.QWindowsXPStyle
        #QApplication.setStyle("windowsxp")
        #print QtGui.QStyleFactory.keys()

        self.lastDir = "" # sauvegarde de la dernière opération dossier/fichier, sauf ceux dans case.dirPath

        #internationalisation.__init__(self)
        Check_table.__init__(self)

        setattr(MainView, "Table_callback", Table_callback)

        setattr(MainView, "StatusTip", StatusTip)
        setattr(MainView, "ToolTip", ToolTip)
        setattr(MainView, "WhatsThis", WhatsThis)
        setattr(MainView, "funcReadWhat", funcReadWhat)
        setattr(MainView, "find_names", find_names)

        # définir le style de QGroupBox
        #MPpath = syrthesIHMContext.getExeAbsDirPath()
        path = os.getenv('SYRTHES4_HOME')+ os.sep + "lib"+ os.sep + "syrthesGui"
        qssname = path + os.sep + "22x22" + os.sep + "stylesheet.qss"
        widgetsContenantGroupBox = [self.syrthesIHMCollector.Boundary_conditions_rad_form,
                                    self.syrthesIHMCollector.Control_form,
                                    self.syrthesIHMCollector.Filename_form,
                                    self.syrthesIHMCollector.Home_form,
                                    self.syrthesIHMCollector.Running_options_form,
                                    self.syrthesIHMCollector.User_C_function_form]

        flatGroupBox = [self.syrthesIHMCollector.Control_form.Pv_Pt_var_gb,self.syrthesIHMCollector.Control_form.T_var_gb]


        qss = open(qssname, "r")
        qstr = ""
        if os.access(qssname, os.F_OK) :
            for line in qss.readlines() :
                qstr += line
            for wcg in widgetsContenantGroupBox :
                wcg.setStyleSheet(qstr)
                pass
        qss.close()

        for gb in flatGroupBox:
            qstr = qstr.replace("border:1px","border:0px")
            gb.setStyleSheet(qstr);
            pass

        self.list_Volumic_Table = None
        self.list_2faces_Table = None

        # notable widget list
        self.widget=[self.action_Open,self.action_Save, self.action_New_file,
                    self.action_Quit, self.action_Screenshot, self.action_Calculation_Progress,
                    self.action_Run_Syrthes, self.action_Stop_Syrthes, self.treeWidget, self.syrthesIHMCollector.Home_form.lineEdit_7,
                    self.syrthesIHMCollector.Home_form.Ho_Ds_te, self.syrthesIHMCollector.Home_form.Ho_Ud_but, self.syrthesIHMCollector.Home_form.Dim_Comb, self.syrthesIHMCollector.Home_form.Ho_Tr_ch, self.syrthesIHMCollector.Home_form.Ho_Hm_ch,
                    self.syrthesIHMCollector.Home_form.Ho_Ch_ch, self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch ,self.syrthesIHMCollector.Home_form.Ho_fluid0d_ch ,self.syrthesIHMCollector.Control_form.Ch_res_cal, self.syrthesIHMCollector.Control_form.lineEdit_39, self.syrthesIHMCollector.Control_form.Le_Nts,
                    self.syrthesIHMCollector.Control_form.comb_time_st, self.syrthesIHMCollector.Control_form.Le_auto_It, self.syrthesIHMCollector.Control_form.Le_auto_Mt,
                    self.syrthesIHMCollector.Control_form.Le_auto_Mpv, self.syrthesIHMCollector.Control_form.Le_auto_Mpt,
                    self.syrthesIHMCollector.Control_form.Le_auto_Mts, self.syrthesIHMCollector.Control_form.Le_const_Ts, self.syrthesIHMCollector.Control_form.By_Block_table,
                    self.syrthesIHMCollector.Control_form.lineEdit_43, self.syrthesIHMCollector.Control_form.Le_Mni, self.syrthesIHMCollector.Control_form.Vap_Sp_le, self.syrthesIHMCollector.Control_form.Vap_Mn_le, self.syrthesIHMCollector.Control_form.Ap_Sp_le, self.syrthesIHMCollector.Control_form.Ap_Mn_le,
                    self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table,
                    self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table, self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table, self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table,
                    self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table, #self.Dens_2D_table, self.Heat_cap_2D_table,
                    self.syrthesIHMCollector.Physical_prop_2D_form.Iso_cond_2D_table, self.syrthesIHMCollector.Physical_prop_2D_form.Ort_cond_2D_table, self.syrthesIHMCollector.Physical_prop_2D_form.Ani_cond_2D_table,
                    self.syrthesIHMCollector.Physical_prop_3D_form.Iso_cond_3D_table,
                    self.syrthesIHMCollector.Physical_prop_3D_form.Ort_cond_3D_table, self.syrthesIHMCollector.Physical_prop_3D_form.Ani_cond_3D_table, self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table,
                    self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table, self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_rot_table,
                    self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_tra_table,
                    self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_rot_table, self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_tra_table,
                    self.syrthesIHMCollector.Filename_form.Fn_Cd_lne, self.syrthesIHMCollector.Filename_form.Fn_Cd_but, self.syrthesIHMCollector.Filename_form.Fn_Rs_lne, self.syrthesIHMCollector.Filename_form.Fn_Rs_but,
                    self.syrthesIHMCollector.Filename_form.Fn_Mt_lne, self.syrthesIHMCollector.Filename_form.Fn_Mt_but, self.syrthesIHMCollector.Filename_form.Fn_Rnp_lne, self.syrthesIHMCollector.Filename_form.Fn_Rnp_but,
                    self.syrthesIHMCollector.Filename_form.Fn_Rm_lne, self.syrthesIHMCollector.Filename_form.Fn_Rm_but, self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_cd, self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_ry,
                    self.syrthesIHMCollector.Running_options_form.Ro_Rs_Pb, self.syrthesIHMCollector.Running_options_form.Ro_Cr_cb, self.syrthesIHMCollector.Running_options_form.Ro_Pre_cb,self.syrthesIHMCollector.Running_options_form.Ro_Dp_cb, self.syrthesIHMCollector.Running_options_form.Ro_Ln_le,
                    self.syrthesIHMCollector.Running_options_form.Ro_Ln_pb,
                    self.syrthesIHMCollector.Running_options_form.Ro_Rs_Pb, self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table, self.syrthesIHMCollector.Material_radiation_properties_form.Mrp_table,
                    self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne, self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne, self.syrthesIHMCollector.Boundary_conditions_rad_form.pushButton, self.syrthesIHMCollector.Boundary_conditions_rad_form.pushButton_2,
                    self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_te, self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_te, self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irt_table, self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irf_table,
                    self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rpa_chb, self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rpa_le, #self.Cht_Sf_le, self.Cht_Sf_te,
                    # View factor
                    self.syrthesIHMCollector.View_factor_2D_form.Vf_Ip_2D_table, self.syrthesIHMCollector.View_factor_2D_form.Vf_Sy_2D_table, self.syrthesIHMCollector.View_factor_2D_form.Vf_Pe_2D_table,
                    self.syrthesIHMCollector.View_factor_3D_form.Vf_Ip_3D_table, self.syrthesIHMCollector.View_factor_3D_form.Vf_Sy_3D_table,
                    self.syrthesIHMCollector.View_factor_3D_form.Vf_Pe_3D_table, self.syrthesIHMCollector.View_factor_2D_form.Vfm_2D_cmb, self.syrthesIHMCollector.View_factor_3D_form.Vfm_3D_cmb,
                    # Solar
                    self.syrthesIHMCollector.Solar_aspect_form.Sa_chb, self.syrthesIHMCollector.Solar_aspect_form.Sa_chb, self.syrthesIHMCollector.Solar_aspect_form.Sa_cmb, self.syrthesIHMCollector.Solar_aspect_form.Csm_Db_table,
                    self.syrthesIHMCollector.Solar_aspect_form.spinBox, self.syrthesIHMCollector.Solar_aspect_form.spinBox_2, self.syrthesIHMCollector.Solar_aspect_form.spinBox_3, self.syrthesIHMCollector.Solar_aspect_form.spinBox_4, self.syrthesIHMCollector.Solar_aspect_form.spinBox_5,
                    self.syrthesIHMCollector.Solar_aspect_form.spinBox_6, self.syrthesIHMCollector.Solar_aspect_form.dateTimeEdit, #self.syrthesIHMCollector.Solar_aspect_form.Csm_ad_cb,
                    self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_5, self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_6,
                    self.syrthesIHMCollector.Solar_aspect_form.Sa_Hm_table, self.syrthesIHMCollector.Solar_aspect_form.Sa_Sm_table,
                    self.syrthesIHMCollector.Solar_aspect_form.Asm_Lat_cmb, self.syrthesIHMCollector.Solar_aspect_form.Asm_Lng_cmb, self.syrthesIHMCollector.Solar_aspect_form.Csm_cmb, self.syrthesIHMCollector.Solar_aspect_form.lineEdit,
                    self.syrthesIHMCollector.Solar_aspect_form.Sa_Sht_table, self.syrthesIHMCollector.Home_form.Hm_cmb, self.syrthesIHMCollector.Boundary_conditions_TPv_form.Bc_TPv_table,
                    self.syrthesIHMCollector.Boundary_conditions_TPvPt_form.Bc_TPvPt_table, self.syrthesIHMCollector.Initial_conditions_hum_TPv_form.Vch_Ic_TPv_table, self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form.Vch_St_TPv_table,
                    self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form.Vch_Ic_TPvPt_table, self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form.Vch_St_TPvPt_table,
                    self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_iso_2D_table,
                    self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_aniso_2D_table,
                    self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_iso_3D_table,
                    self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_aniso_3D_table,
                    self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Sc_table, self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Vc_table,
                     self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form.Cont_res_hum_TPvPt_table, self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form.Cont_res_hum_TPv_table,
                    # User C function
                    self.syrthesIHMCollector.User_C_function_form.Cfunc_lne, self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_lne, self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_lne, self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_lne, self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_lne, self.syrthesIHMCollector.User_C_function_form.Cfunc_other_lne ,
                    self.syrthesIHMCollector.User_C_function_form.Cfunc_but, self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_but, self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_but, self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_but, self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_but, self.syrthesIHMCollector.User_C_function_form.Cfunc_other_but,
                    self.syrthesIHMCollector.User_C_function_form.Cfunc_test_compile,
                    self.syrthesIHMCollector.Advanced_mode_form.Advanced_cmd_table,
                    # Output
                    self.syrthesIHMCollector.Output_2D_form.Tf_cb_2D_Op, self.syrthesIHMCollector.Output_2D_form.Cb_2D_Op, self.syrthesIHMCollector.Output_2D_form.Cb2_2D_Op, self.syrthesIHMCollector.Output_2D_form.Le_2D_Op, self.syrthesIHMCollector.Output_2D_form.Le2_2D_Op, self.syrthesIHMCollector.Output_2D_form.Op_Dc_2D_table, self.syrthesIHMCollector.Output_2D_form.Op_Sb_2D_table, self.syrthesIHMCollector.Output_2D_form.Op_Vb_2D_table,
                    self.syrthesIHMCollector.Output_3D_form.Tf_cb_3D_Op, self.syrthesIHMCollector.Output_3D_form.Cb_3D_Op, self.syrthesIHMCollector.Output_3D_form.Cb2_3D_Op, self.syrthesIHMCollector.Output_3D_form.Le_3D_Op, self.syrthesIHMCollector.Output_3D_form.Le2_3D_Op, self.syrthesIHMCollector.Output_3D_form.Op_Dc_3D_table, self.syrthesIHMCollector.Output_3D_form.Op_Sb_3D_table, self.syrthesIHMCollector.Output_3D_form.Op_Vb_3D_table,
                    self.syrthesIHMCollector.Output_3D_form.Hf_cb_3D_Op,
                    self.syrthesIHMCollector.Output_3D_form.Mt_cb_3D_Op,
                    self.syrthesIHMCollector.Output_3D_form.F_cb_3D_Op,
                    self.syrthesIHMCollector.Output_2D_form.Hf_cb_2D_Op,
                    self.syrthesIHMCollector.Output_2D_form.Mt_cb_2D_Op,
                    self.syrthesIHMCollector.Output_2D_form.F_cb_2D_Op,
                    # Calculation_progress
                    #self.verticalLayout, self.btnRemove_tab2, self.btnRemove_tab3, self.btnRemove_tab4,
                    #self.cbStyle_tab1, self.cbStyle_tab2, self.cbStyle_tab3, self.cbStyle_tab4,
                    #self.Cb_Top2, self.Cb_Top3, self.Cb_Top4
                    # fluid1d
                    self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table,
                    self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table,
                    self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table,
                    self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table,
                    self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl,
                    self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl,
                    self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP,
                    self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table,
                    self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1df_lne,
                    self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1dfUc_lne,
                    self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table,
                    self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table,
                    self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table,
                    self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table,
                    self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table,
                    self.syrthesIHMCollector.Control_fluid1d_form.Cb_solid_Ts_fluid1d,
                    self.syrthesIHMCollector.Control_fluid1d_form.Le_const_Ts_fluid1d,
                    self.syrthesIHMCollector.Filename_form.Fn_fluid1d_but,
                    self.syrthesIHMCollector.Filename_form.Fn_fluid1d_lne,
                    self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_x,
                    self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_y,
                    self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_z,
                    # fluid0d
                    self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table,
                    self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table,
                    self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table,
                    self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table
                    ]

        # Mise en place des messages de barre de statut, des infobulles
        self.StatusTip()
        self.ToolTip()
        self.WhatsThis()
        #self.setupframe()
        # Mise en place de l'arborescence
        self.setuptree()
        #self.setupstatustip()
        # Mise en place des composants graphiques dans les cellules des tableaux
        waitCursor = QtGui.QCursor(Qt.Qt.WaitCursor)
        QtWidgets.QApplication.setOverrideCursor(waitCursor)
        self.SetCellWidget()
        QtWidgets.QApplication.restoreOverrideCursor()
        # Mise en place des fonctions de rappels pour les tableaux
        self.Table_callback()
        #self.MousePressEvent(QMouseEvent)
        self.allflag=False # flag inutilisé
        self.open=False # flag d'ouverture de fichier
        self.Dimension="3D" # initialisation de la dimension du problème à : 3D
        self.setWindowTitle("SYRTHES V 4.3 - untitled.syd") # modification de la chaîne de caractère à l'entête de fenêtre

        self.titleChanged.emit()
        ## self.emit(SIGNAL("titleChanged")) # for SALOME
        self.errflag=False
        self.errwinflag=False


        # dictionnaire de correspondance entre les boutons de la vue des noms de fichiers et les champ d'éditions auxquels ils se rapportent

        self.dic_Open_file={self.syrthesIHMCollector.Filename_form.Fn_Cd_but : self.syrthesIHMCollector.Filename_form.Fn_Cd_lne,
                  self.syrthesIHMCollector.Filename_form.Fn_Rs_but : self.syrthesIHMCollector.Filename_form.Fn_Rs_lne,
                  self.syrthesIHMCollector.Filename_form.Fn_Mt_but : self.syrthesIHMCollector.Filename_form.Fn_Mt_lne,
                  self.syrthesIHMCollector.Filename_form.Fn_Rnp_but : self.syrthesIHMCollector.Filename_form.Fn_Rnp_lne,
                  self.syrthesIHMCollector.Filename_form.Fn_Rm_but : self.syrthesIHMCollector.Filename_form.Fn_Rm_lne,
                  self.syrthesIHMCollector.Filename_form.Fn_fluid1d_but : self.syrthesIHMCollector.Filename_form.Fn_fluid1d_lne,
                  self.syrthesIHMCollector.Running_options_form.Ro_Ln_pb : self.syrthesIHMCollector.Running_options_form.Ro_Ln_le}

        # dictionnaire décrivant le type des différentes tables

        self.dic_Table_type={self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table : 1, # checkbox + combobox
                            self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table : 1,
                            self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table : 1,
                            self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table : 1,
                            self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table : 1,
                            self.syrthesIHMCollector.Physical_prop_2D_form.Iso_cond_2D_table : 1,
                            self.syrthesIHMCollector.Physical_prop_2D_form.Ort_cond_2D_table : 1,
                            self.syrthesIHMCollector.Physical_prop_2D_form.Ani_cond_2D_table : 1,
                            self.syrthesIHMCollector.Physical_prop_3D_form.Iso_cond_3D_table : 1,
                            self.syrthesIHMCollector.Physical_prop_3D_form.Ort_cond_3D_table : 1,
                            self.syrthesIHMCollector.Physical_prop_3D_form.Ani_cond_3D_table : 1,
                            self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table : 1,
                            self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table : 1,
                            self.syrthesIHMCollector.Boundary_conditions_TPv_form.Bc_TPv_table : 1,
                            self.syrthesIHMCollector.Boundary_conditions_TPvPt_form.Bc_TPvPt_table : 1,
                            self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form.Cont_res_hum_TPv_table : 1,
                            self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form.Cont_res_hum_TPvPt_table : 1,
                            self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table : 1,
                            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table : 1,
                            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table : 1,
                            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table : 1,
                            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl : 1,
                            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl : 1,
                            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP : 1,
                            self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table : 1,
                            self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table : 1,
                            self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table : 2,
                            self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table : 1,
                            self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table : 1,
                            self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table : 1,
                            # fluid0d
                            self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table : 1,
                            self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table : 1,
                            self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table : 2,
                            self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table : 1,
                            #
                            self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_tra_table : 2, # checkbox only
                            self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_rot_table : 2,
                            self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_tra_table : 2,
                            self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_rot_table : 2,
                            self.syrthesIHMCollector.Output_2D_form.Op_Dc_2D_table : 2,
                            self.syrthesIHMCollector.Output_2D_form.Op_Sb_2D_table : 2,
                            self.syrthesIHMCollector.Output_2D_form.Op_Vb_2D_table : 2,
                            self.syrthesIHMCollector.Output_3D_form.Op_Dc_3D_table : 2,
                            self.syrthesIHMCollector.Output_3D_form.Op_Sb_3D_table : 2,
                            self.syrthesIHMCollector.Output_3D_form.Op_Vb_3D_table : 2,
                            self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table : 2,
                            self.syrthesIHMCollector.Material_radiation_properties_form.Mrp_table : 2,
                            self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irt_table : 2,
                            self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irf_table : 2,
                            self.syrthesIHMCollector.View_factor_2D_form.Vf_Ip_2D_table : 2,
                            self.syrthesIHMCollector.View_factor_2D_form.Vf_Sy_2D_table : 2,
                            self.syrthesIHMCollector.View_factor_2D_form.Vf_Pe_2D_table : 2,
                            self.syrthesIHMCollector.View_factor_3D_form.Vf_Ip_3D_table : 2,
                            self.syrthesIHMCollector.View_factor_3D_form.Vf_Sy_3D_table : 2,
                            self.syrthesIHMCollector.View_factor_3D_form.Vf_Pe_3D_table : 2,
                            self.syrthesIHMCollector.Solar_aspect_form.Sa_Sht_table : 2,
                            self.syrthesIHMCollector.Solar_aspect_form.Sa_Hm_table : 2,
                            self.syrthesIHMCollector.Solar_aspect_form.Sa_Sm_table : 2,
                            self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Sc_table : 2,
                            self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Vc_table : 2,
                            self.syrthesIHMCollector.Solar_aspect_form.Csm_Db_table : 2,
                            self.syrthesIHMCollector.Advanced_mode_form.Advanced_cmd_table : 2,
                            self.syrthesIHMCollector.Initial_conditions_hum_TPv_form.Vch_Ic_TPv_table : 3, # checkbox + 2 comboboxes
                            self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form.Vch_St_TPv_table : 3,
                            self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form.Vch_Ic_TPvPt_table : 4, # checkbox + 2 comboboxes
                            self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form.Vch_St_TPvPt_table : 4,
                            self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_iso_2D_table : 5, # checkbox + combobox Material
                            self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_aniso_2D_table : 5, # checkbox + combobox Material
                            self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_iso_3D_table : 5, # checkbox + combobox Material
                            self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_aniso_3D_table : 5, # checkbox + combobox Material
                            self.syrthesIHMCollector.Control_form.By_Block_table : 6 # only text
                            }

        #for table in self.dic_Table_type.keys() :





        self.initHheader() # allongement des dernières colonnes des tableaux

        # dictionnaire des dimensions
        self.Dim_comb_dic={0 : '3D',
                           1 : '2D_CART',
                           2 : '2D_AXI_OX',
                           3 : '2D_AXI_OY'}

        # tuple des items de l'arborescence
        self.treew = [self.Home, self.Control, self.Initial_conditions_cond,
                      self.Boundary_conditions_cond, self.Volumetric_conditions_cond,
                      self.Physical_Properties, self.Periodicity, self.Output,
                      self.Filename, self.Running_options,
                      self.Spectral_parameters, self.Material_radiation_properties,
                      self.Boundary_conditions_rad, self.View_factor,
                      self.Solar_aspect, self.Initial_conditions_hum,
                      self.Boundary_conditions_hum,
                      self.Contact_resistance_humidity, self.Volumetric_conditions_hum,
                      self.Material_Properties_hum, self.Conjugate_heat_transfer,
                      self.Humidity, self.Conduction,
                      self.Radiation, self.Advanced_mode, self.User_C_function,
                      # fluid1d
                      self.fluid1d, self.Initial_conditions_fluid1d, self.Boundary_conditions_fluid1d,
                      self.Physical_prop_fluid1d, self.Geometrie_fluid1d,
                      self.Volumetric_conditions_fluid1d, self.Control_fluid1d,
                      # fluid0d
                      self.fluid0d,
                      self.Geometrie_fluid0d,
                      self.Boundary_conditions_fluid0d,
                      self.Physical_properties_fluid0d,
                      self.Volumetric_conditions_fluid0d
                      ]

        # dictionnaire de correspondance entres les items de l'arborescence et les vues ( les fausses clé sont là quand la sélection de la vue implique la dimension du problème).

        self.dic_tree_frame={id(self.Conduction) : self.syrthesIHMCollector.Fake_form,#fausse clé
                             id(self.Home) : self.syrthesIHMCollector.Home_form,
                             id(self.Initial_conditions_cond) : self.syrthesIHMCollector.Initial_conditions_cond_form,
                             id(self.Boundary_conditions_cond) : self.syrthesIHMCollector.Boundary_conditions_cond_form,
                             id(self.Volumetric_conditions_cond) : self.syrthesIHMCollector.Volumetric_conditions_cond_form,
                             id(self.Physical_Properties) : self.syrthesIHMCollector.Physical_prop_3D_form,#fausse clé
                             id(self.Periodicity) : self.syrthesIHMCollector.Periodicity_3D_form,#fausse clé
                             id(self.Radiation) : self.syrthesIHMCollector.Fake_form,#fausse clé
                             id(self.Humidity) : self.syrthesIHMCollector.Fake_form,#fausse clé
                             id(self.Filename) : self.syrthesIHMCollector.Filename_form,
                             id(self.Running_options) : self.syrthesIHMCollector.Running_options_form,
                             id(self.Control) : self.syrthesIHMCollector.Control_form,
                             id(self.Output) : self.syrthesIHMCollector.Output_3D_form,#fausse clé
                             id(self.Spectral_parameters) : self.syrthesIHMCollector.Spectral_parameters_form,
                             id(self.Material_radiation_properties) : self.syrthesIHMCollector.Material_radiation_properties_form,
                             id(self.Conjugate_heat_transfer) : self.syrthesIHMCollector.Conjugate_heat_transfer_form,
                             id(self.Boundary_conditions_rad) : self.syrthesIHMCollector.Boundary_conditions_rad_form,
                             id(self.View_factor) : self.syrthesIHMCollector.Fake_form,#fausse clé
                             id(self.Solar_aspect) : self.syrthesIHMCollector.Fake_form,#fausse clé
                             id(self.Initial_conditions_hum) : self.syrthesIHMCollector.Fake_form,#fausse clé
                             id(self.Boundary_conditions_hum) : self.syrthesIHMCollector.Fake_form,#fausse clé
                             id(self.Contact_resistance_humidity) : self.syrthesIHMCollector.Fake_form,
                             id(self.Volumetric_conditions_hum) : self.syrthesIHMCollector.Fake_form,#fausse clé
                             id(self.Material_Properties_hum) : self.syrthesIHMCollector.Material_humidity_properties_3D_form,
                             # fluid1d
                             id(self.fluid1d) : self.syrthesIHMCollector.Fake_form,#fausse clé
                             id(self.Initial_conditions_fluid1d) : self.syrthesIHMCollector.Initial_conditions_fluid1d_form,
                             id(self.Boundary_conditions_fluid1d) : self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form,
                             id(self.Physical_prop_fluid1d) : self.syrthesIHMCollector.Physical_prop_fluid1d_form,
                             id(self.Geometrie_fluid1d) : self.syrthesIHMCollector.Geometrie_fluid1d_form,
                             id(self.Volumetric_conditions_fluid1d) : self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form,
                             id(self.Control_fluid1d) : self.syrthesIHMCollector.Control_fluid1d_form,
                             # fluid0d
                             id(self.fluid0d) : self.syrthesIHMCollector.Fake_form,#fausse clé
                             id(self.Boundary_conditions_fluid0d) : self.syrthesIHMCollector.Boundary_conditions_fluid0d_form,
                             id(self.Physical_properties_fluid0d) : self.syrthesIHMCollector.Physical_properties_fluid0d_form,
                             id(self.Geometrie_fluid0d) : self.syrthesIHMCollector.Geometrie_fluid0d_form,
                             id(self.Volumetric_conditions_fluid0d) : self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form,
                             #
                             id(self.Advanced_mode) : self.syrthesIHMCollector.Advanced_mode_form,
                             id(self.User_C_function) : self.syrthesIHMCollector.User_C_function_form
                             }

        #dictionnaire du type de donnée des champs d'éditions
        self.lne_dic={self.syrthesIHMCollector.Control_form.Le_Nts : '+=int',
                      self.syrthesIHMCollector.Control_form.lineEdit_39 : '+float',
                      self.syrthesIHMCollector.Control_form.Le_const_Ts : '+float',
                      self.syrthesIHMCollector.Control_form.Le_auto_It : '+float',
                      self.syrthesIHMCollector.Control_form.Le_auto_Mt : '+float',
                      self.syrthesIHMCollector.Control_form.Le_auto_Mpv : '+float',
                      self.syrthesIHMCollector.Control_form.Le_auto_Mpt : '+float',
                      self.syrthesIHMCollector.Control_form.Le_auto_Mts : '+float',
                      self.syrthesIHMCollector.Control_form.lineEdit_43 : '+float',
                      self.syrthesIHMCollector.Control_form.Le_Mni : '+int',
                      self.syrthesIHMCollector.Output_2D_form.Le_2D_Op : 'spec',
                      self.syrthesIHMCollector.Output_3D_form.Le_3D_Op : 'spec',
                      self.syrthesIHMCollector.Output_2D_form.Le2_2D_Op : 'spec2',
                      self.syrthesIHMCollector.Output_3D_form.Le2_3D_Op : 'spec2',
                      self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne : '+intstr',
                      self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne : '+intstr',
                      self.syrthesIHMCollector.Control_form.Vap_Sp_le : '+float',
                      self.syrthesIHMCollector.Control_form.Vap_Mn_le : '+int',
                      self.syrthesIHMCollector.Control_form.Ap_Sp_le : '+float',
                      self.syrthesIHMCollector.Control_form.Ap_Mn_le : '+int',
                      # fluid1d
                      self.syrthesIHMCollector.Control_fluid1d_form.Le_const_Ts_fluid1d : '+float',
                      self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1df_lne : '+intstr',
                      }

        # initialisation du menu contextuel des tableaux

        self.menu=QtWidgets.QMenu(self)
        self.action_New=self.menu.addAction("New")
        self.action_Copy=self.menu.addAction("Copy")
        self.action_Paste=self.menu.addAction("Paste")
        self.action_Delete=self.menu.addAction("Delete")
        if syrthesIHMContext.isEmbedded() :
            self.action_PasteSalome = self.menu.addAction("Paste SALOME references")

        # connections des fonctions de rappel
        self.action_Run_Syrthes.triggered.connect(self.Syrthes_running)
##         self.connect(self.action_Run_Syrthes, SIGNAL("activated(int)"), self.Syrthes_running)
        self.action_Stop_Syrthes.triggered.connect(self.Syrthes_stopping)
##         self.connect(self.action_Stop_Syrthes, SIGNAL("activated(int)"), self.Syrthes_stopping)
        self.action_Open.triggered.connect(self.openData)
##         self.connect(self.action_Open, SIGNAL("activated(int)"), self.openData)
        self.action_Save.triggered.connect(self.SavingFile)
##         self.connect(self.action_Save, SIGNAL("activated(int)"), self.SavingFile)
        self.actionSa_ve_as.triggered.connect(self.Saving_as)
##         self.connect(self.actionSa_ve_as, SIGNAL("activated(int)"), self.Saving_as)
        self.action_New_file.triggered.connect(self.New_File)
##         self.connect(self.action_New_file, SIGNAL("activated(int)"), self.New_File)
        self.action_Screenshot.triggered.connect(self.Screenshot)
##         self.connect(self.action_Screenshot, SIGNAL("activated(int)"), self.Screenshot)
        self.action_Quit.triggered.connect(self.Quitter)
##         self.connect(self.action_Quit, SIGNAL("activated(int)"), self.Quitter)
        self.treeWidget.itemClicked.connect(self.FrameSelect)
##         self.connect(self.treeWidget, SIGNAL("itemClicked(QTreeWidgetItem*,int)"), self.FrameSelect)
        self.treeWidget.itemExpanded.connect(self.resizetree)
##         self.connect(self.treeWidget, SIGNAL("itemExpanded(QTreeWidgetItem*)"), self.resizetree)
        self.treeWidget.itemSelectionChanged.connect(self.FrameSelectBis)
##         self.connect(self.treeWidget, SIGNAL("itemSelectionChanged()"), self.FrameSelectBis)
        self.syrthesIHMCollector.Control_form.comb_time_st.activated.connect(self.Time_step_select)
##         self.connect(self.syrthesIHMCollector.Control_form.comb_time_st, SIGNAL("activated(int)"), self.Time_step_select)
        self.syrthesIHMCollector.Solar_aspect_form.Sa_cmb.activated.connect(self.Modelling_select)
##         self.connect(self.syrthesIHMCollector.Solar_aspect_form.Sa_cmb, SIGNAL("activated(int)"), self.Modelling_select)
        self.syrthesIHMCollector.Solar_aspect_form.Csm_cmb.activated.connect(self.Constant_Modelling_select)
##         self.connect(self.syrthesIHMCollector.Solar_aspect_form.Csm_cmb, SIGNAL("activated(int)"), self.Constant_Modelling_select)
        self.syrthesIHMCollector.Home_form.Dim_Comb.activated.connect(self.Dimension_choice)
##         self.connect(self.syrthesIHMCollector.Home_form.Dim_Comb, SIGNAL("activated(int)"), self.Dimension_choice)
        self.syrthesIHMCollector.Filename_form.Fn_Cd_but.clicked.connect(self.meshFileselection)
##         self.connect(self.syrthesIHMCollector.Filename_form.Fn_Cd_but, SIGNAL("clicked()"), self.meshFileselection)
        self.syrthesIHMCollector.Filename_form.Fn_Rs_but.clicked.connect(self.Fileselection)
##         self.connect(self.syrthesIHMCollector.Filename_form.Fn_Rs_but, SIGNAL("clicked()"), self.Fileselection)
        self.syrthesIHMCollector.Filename_form.Fn_Mt_but.clicked.connect(self.meshFileselection)
##         self.connect(self.syrthesIHMCollector.Filename_form.Fn_Mt_but, SIGNAL("clicked()"), self.meshFileselection)
        self.syrthesIHMCollector.Filename_form.Fn_Rnp_but.clicked.connect(self.Fileselection)
##         self.connect(self.syrthesIHMCollector.Filename_form.Fn_Rnp_but, SIGNAL("clicked()"), self.Fileselection)
        self.syrthesIHMCollector.Filename_form.Fn_Rm_but.clicked.connect(self.meshFileselection)
##         self.connect(self.syrthesIHMCollector.Filename_form.Fn_Rm_but, SIGNAL("clicked()"), self.meshFileselection)
        self.syrthesIHMCollector.Filename_form.Fn_fluid1d_but.clicked.connect(self.meshFileselection)
##         self.connect(self.syrthesIHMCollector.Filename_form.Fn_fluid1d_but, SIGNAL("clicked()"), self.meshFileselection)

        self.syrthesIHMCollector.User_C_function_form.Cfunc_but.clicked.connect(self.User_C_function_Edit)
##         self.connect(self.syrthesIHMCollector.User_C_function_form.Cfunc_but, SIGNAL("clicked()"), self.User_C_function_Edit)
        self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_but.clicked.connect(self.User_C_function_Edit)
##         self.connect(self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_but, SIGNAL("clicked()"), self.User_C_function_Edit)
        self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_but.clicked.connect(self.User_C_function_Edit)
##         self.connect(self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_but, SIGNAL("clicked()"), self.User_C_function_Edit)
        self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_but.clicked.connect(self.User_C_function_Edit)
##         self.connect(self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_but, SIGNAL("clicked()"), self.User_C_function_Edit)
        self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_but.clicked.connect(self.User_C_function_Edit)
##         self.connect(self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_but, SIGNAL("clicked()"), self.User_C_function_Edit)
        self.syrthesIHMCollector.User_C_function_form.Cfunc_other_but.clicked.connect(self.User_C_function_Edit)
##         self.connect(self.syrthesIHMCollector.User_C_function_form.Cfunc_other_but, SIGNAL("clicked()"), self.User_C_function_Edit)
        self.syrthesIHMCollector.User_C_function_form.btnOpenOther.clicked.connect(self.User_C_function_Edit)
##         self.connect(self.syrthesIHMCollector.User_C_function_form.btnOpenOther, SIGNAL("clicked()"), self.User_C_function_Edit)
        self.syrthesIHMCollector.User_C_function_form.Cfunc_test_compile.clicked.connect(self.User_C_function_Compile)
##         self.connect(self.syrthesIHMCollector.User_C_function_form.Cfunc_test_compile, SIGNAL("clicked()"), self.User_C_function_Compile)

        self.action_New.triggered.connect(self.New_Line)
##        self.connect(self.action_New, SIGNAL("triggered()"), self.New_Line)
        self.action_Delete.triggered.connect(self.Del_Line)
##        self.connect(self.action_Delete, SIGNAL("triggered()"), self.Del_Line)
        self.action_Copy.triggered.connect(self.Copy)
##        self.connect(self.action_Copy, SIGNAL("triggered()"), self.Copy)
        self.action_Paste.triggered.connect(self.Paste)
##         self.connect(self.action_Paste, SIGNAL("triggered()"), self.Paste)
        if syrthesIHMContext.isEmbedded() :
            self.action_PasteSalome.triggered.connect(self.slotAddFromSalome)
##             self.connect(self.action_PasteSalome, SIGNAL("triggered()"), self.slotAddFromSalome)
        self.syrthesIHMCollector.Home_form.Ho_Tr_ch.toggled.connect(self.RadHid)
##         self.connect(self.syrthesIHMCollector.Home_form.Ho_Tr_ch, SIGNAL("toggled(bool)"), self.RadHid)
        self.syrthesIHMCollector.Home_form.Ho_Hm_ch.toggled.connect(self.HummodHid)
##         self.connect(self.syrthesIHMCollector.Home_form.Ho_Hm_ch, SIGNAL("toggled(bool)"), self.HummodHid)
        self.syrthesIHMCollector.Home_form.Ho_Ch_ch.toggled.connect(self.ChtHid)
##         self.connect(self.syrthesIHMCollector.Home_form.Ho_Ch_ch, SIGNAL("toggled(bool)"), self.ChtHid)
        # fluid1d
        self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.toggled.connect(self.fluid1dHid)
##        self.connect(self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch, SIGNAL("toggled(bool)"), self.fluid1dHid)
        self.syrthesIHMCollector.Control_fluid1d_form.Le_const_Ts_fluid1d.textChanged.connect(self.Check_type)
##        self.connect(self.syrthesIHMCollector.Control_fluid1d_form.Le_const_Ts_fluid1d, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Control_fluid1d_form.Cb_solid_Ts_fluid1d.toggled.connect(self.Check_solid_ts)
##         self.connect(self.syrthesIHMCollector.Control_fluid1d_form.Cb_solid_Ts_fluid1d, SIGNAL("toggled(bool)"), self.Check_solid_ts)
        self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1df_lne.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1df_lne, SIGNAL("textChanged(QString)"), self.Check_type)
        # fluid0d
        self.syrthesIHMCollector.Home_form.Ho_fluid0d_ch.toggled.connect(self.fluid0dHid)
##         self.connect(self.syrthesIHMCollector.Home_form.Ho_fluid0d_ch, SIGNAL("toggled(bool)"), self.fluid0dHid)
        #

        self.syrthesIHMCollector.Control_form.Le_Nts.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.Le_Nts, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Control_form.lineEdit_39.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.lineEdit_39, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Control_form.Le_const_Ts.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.Le_const_Ts, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Control_form.Le_auto_It.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.Le_auto_It, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Control_form.Le_auto_Mt.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.Le_auto_Mt, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Control_form.Le_auto_Mpv.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.Le_auto_Mpv, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Control_form.Le_auto_Mpt.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.Le_auto_Mpt, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Control_form.Le_auto_Mts.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.Le_auto_Mts, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Control_form.lineEdit_43.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.lineEdit_43, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Control_form.Le_Mni.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.Le_Mni, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Output_2D_form.Le_2D_Op.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Output_2D_form.Le_2D_Op, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Output_3D_form.Le_3D_Op.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Output_3D_form.Le_3D_Op, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Output_2D_form.Le2_2D_Op.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Output_2D_form.Le2_2D_Op, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Output_3D_form.Le2_3D_Op.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Output_3D_form.Le2_3D_Op, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Output_3D_form.Tf_cb_3D_Op.stateChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Output_3D_form.Tf_cb_3D_Op, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Output_2D_form.Tf_cb_2D_Op.stateChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Output_2D_form.Tf_cb_2D_Op, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Output_2D_form.Cb_2D_Op.currentIndexChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Output_2D_form.Cb_2D_Op, SIGNAL("currentIndexChanged(int)"), self.Check_type)
        self.syrthesIHMCollector.Output_3D_form.Cb_3D_Op.currentIndexChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Output_3D_form.Cb_3D_Op, SIGNAL("currentIndexChanged(int)"), self.Check_type)
        self.syrthesIHMCollector.Output_2D_form.Cb2_2D_Op.currentIndexChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Output_2D_form.Cb2_2D_Op, SIGNAL("currentIndexChanged(int)"), self.Check_type)
        self.syrthesIHMCollector.Output_3D_form.Cb2_3D_Op.currentIndexChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Output_3D_form.Cb2_3D_Op, SIGNAL("currentIndexChanged(int)"), self.Check_type)
        self.syrthesIHMCollector.Output_3D_form.F_cb_3D_Op.toggled.connect(self.Check_fields)
##         self.connect(self.syrthesIHMCollector.Output_3D_form.F_cb_3D_Op, SIGNAL("toggled(bool)"), self.Check_fields)
        self.syrthesIHMCollector.Output_2D_form.F_cb_2D_Op.toggled.connect(self.Check_fields)
##         self.connect(self.syrthesIHMCollector.Output_2D_form.F_cb_2D_Op, SIGNAL("toggled(bool)"), self.Check_fields)
        self.syrthesIHMCollector.Control_form.Vap_Sp_le.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.Vap_Sp_le, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Control_form.Vap_Mn_le.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.Vap_Mn_le, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Control_form.Ap_Sp_le.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.Ap_Sp_le, SIGNAL("textChanged(QString)"), self.Check_type)
        self.syrthesIHMCollector.Control_form.Ap_Mn_le.textChanged.connect(self.Check_type)
##         self.connect(self.syrthesIHMCollector.Control_form.Ap_Mn_le, SIGNAL("textChanged(QString)"), self.Check_type)
        self.action_Calculation_Progress.triggered.connect(self.calc)
##         self.connect(self.action_Calculation_Progress, SIGNAL("activated(int)"), self.calc)
        self.syrthesIHMCollector.Control_form.Ch_res_cal.toggled.connect(self.Enable)
##         self.connect(self.syrthesIHMCollector.Control_form.Ch_res_cal, SIGNAL("toggled(bool)"), self.Enable)
        self.syrthesIHMCollector.Home_form.Hm_cmb.currentIndexChanged.connect(self.EnableA)
##         self.connect(self.syrthesIHMCollector.Home_form.Hm_cmb, SIGNAL("currentIndexChanged(int)"), self.EnableA)
        self.syrthesIHMCollector.Running_options_form.Ro_Rs_Pb.clicked.connect(self.Syrthes_running)
##         self.connect(self.syrthesIHMCollector.Running_options_form.Ro_Rs_Pb, SIGNAL("clicked()"), self.Syrthes_running)
        self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_cd.valueChanged.connect(self.setmaxprocray)
##         self.connect(self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_cd, SIGNAL("valueChanged(int)"), self.setmaxprocray)
        self.syrthesIHMCollector.Running_options_form.Ro_Ln_pb.clicked.connect(self.Fileselection)
##         self.connect(self.syrthesIHMCollector.Running_options_form.Ro_Ln_pb, SIGNAL("clicked()"), self.Fileselection)
        self.action_Advanced_mode.toggled.connect(self.Advanced_mode_select)
##         self.connect(self.action_Advanced_mode, SIGNAL("toggled(bool)"), self.Advanced_mode_select)
        self.actionWhat_s_This.triggered.connect(self.showWhatsThis)
##         self.connect(self.actionWhat_s_This, SIGNAL("activated(int)"), self.showWhatsThis)
        self.action_About.triggered.connect(self.showAbout)
##         self.connect(self.action_About, SIGNAL("activated(int)"), self.showAbout)
        self.action_Licence.triggered.connect(self.showLicence)
##         self.connect(self.action_Licence, SIGNAL("activated(int)"), self.showLicence)
        self.actionNedit.triggered.connect(self.setNedit)
##         self.connect(self.actionNedit, SIGNAL("activated(int)"), self.setNedit)
        self.actionNotepad.triggered.connect(self.setNotepad)
##         self.connect(self.actionNotepad, SIGNAL("activated(int)"), self.setNotepad)
        self.actionCustomize.triggered.connect(self.showEditorChoice)
##         self.connect(self.actionCustomize, SIGNAL("activated(int)"), self.showEditorChoice)
        self.actionXpdf.triggered.connect(self.setXpdf)
##         self.connect(self.actionXpdf, SIGNAL("activated(int)"), self.setXpdf)
        self.actionDefaultPdf.triggered.connect(self.setDefaultPdf)
##         self.connect(self.actionDefaultPdf, SIGNAL("activated(int)"), self.setDefaultPdf)
        self.actionPDFCustomize.triggered.connect(self.showPDFChoice)
##         self.connect(self.actionPDFCustomize, SIGNAL("activated(int)"), self.showPDFChoice)
        self.actionUser_Guide.triggered.connect(self.showUserGuide)
##         self.connect(self.actionUser_Guide, SIGNAL("activated(int)"), self.showUserGuide)
        self.actionValidation_Guide.triggered.connect(self.showValidationGuide)
##         self.connect(self.actionValidation_Guide, SIGNAL("activated(int)"), self.showValidationGuide)
        self.actionTutoriel.triggered.connect(self.showTutorial)
##         self.connect(self.actionTutoriel, SIGNAL("activated(int)"), self.showTutorial)
        self.action_Open_shell.triggered.connect(self.showShell)
##         self.connect(self.action_Open_shell, SIGNAL("activated(int)"), self.showShell)
        self.action_Open_Desc.triggered.connect(self.showDesc)
##         self.connect(self.action_Open_Desc, SIGNAL("activated(int)"), self.showDesc)


        # dissimulation des vues au démarrage

        self.syrthesIHMCollector.Volumetric_conditions_cond_form.hide()
        self.syrthesIHMCollector.Initial_conditions_cond_form.hide()
        self.syrthesIHMCollector.Boundary_conditions_cond_form.hide()
        self.syrthesIHMCollector.Control_form.hide()
        self.syrthesIHMCollector.Control_form.Automatic_frame.hide()
        self.syrthesIHMCollector.Control_form.By_block_frame.hide()
        self.syrthesIHMCollector.Physical_prop_2D_form.hide()
        self.syrthesIHMCollector.Physical_prop_3D_form.hide()
        self.syrthesIHMCollector.Periodicity_2D_form.hide()
        self.syrthesIHMCollector.Periodicity_3D_form.hide()
        self.syrthesIHMCollector.Filename_form.hide()
        self.syrthesIHMCollector.Running_options_form.hide()
        self.syrthesIHMCollector.Output_2D_form.hide()
        self.syrthesIHMCollector.Output_3D_form.hide()
        self.syrthesIHMCollector.Spectral_parameters_form.hide()
        self.syrthesIHMCollector.Material_radiation_properties_form.hide()
        self.syrthesIHMCollector.Conjugate_heat_transfer_form.hide()
        self.syrthesIHMCollector.Boundary_conditions_rad_form.hide()
        self.syrthesIHMCollector.View_factor_2D_form.hide()
        self.syrthesIHMCollector.View_factor_3D_form.hide()
        self.syrthesIHMCollector.Solar_aspect_form.hide()
        self.syrthesIHMCollector.Solar_aspect_form.Asm_frame.hide()
        self.syrthesIHMCollector.Initial_conditions_hum_TPv_form.hide()
        self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form.hide()
        self.syrthesIHMCollector.Boundary_conditions_TPv_form.hide()
        self.syrthesIHMCollector.Boundary_conditions_TPvPt_form.hide()
        self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form.hide()
        self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form.hide()
        self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form.hide()
        self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form.hide()
        self.syrthesIHMCollector.Material_humidity_properties_2D_form.hide()
        self.syrthesIHMCollector.Material_humidity_properties_3D_form.hide()
        # fluid1d
        self.syrthesIHMCollector.Initial_conditions_fluid1d_form.hide()
        self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.hide()
        self.syrthesIHMCollector.Physical_prop_fluid1d_form.hide()
        self.syrthesIHMCollector.Geometrie_fluid1d_form.hide()
        self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.hide()
        self.syrthesIHMCollector.Control_fluid1d_form.hide()
        # fluid0d
        self.syrthesIHMCollector.Geometrie_fluid0d_form.hide()
        self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.hide()
        self.syrthesIHMCollector.Physical_properties_fluid0d_form.hide()
        self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.hide()
        #
        self.syrthesIHMCollector.Advanced_mode_form.hide()
        self.syrthesIHMCollector.User_C_function_form.hide()

        # tuple des différents tableaux
        self.Tables=[self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table, self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table,
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table, self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table,
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table,
                self.syrthesIHMCollector.Physical_prop_2D_form.Iso_cond_2D_table,
                self.syrthesIHMCollector.Physical_prop_2D_form.Ort_cond_2D_table, self.syrthesIHMCollector.Physical_prop_2D_form.Ani_cond_2D_table,
                self.syrthesIHMCollector.Physical_prop_3D_form.Iso_cond_3D_table, self.syrthesIHMCollector.Physical_prop_3D_form.Ort_cond_3D_table,
                self.syrthesIHMCollector.Physical_prop_3D_form.Ani_cond_3D_table, self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table,
                self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table, self.syrthesIHMCollector.Boundary_conditions_TPv_form.Bc_TPv_table,
                self.syrthesIHMCollector.Boundary_conditions_TPvPt_form.Bc_TPvPt_table,self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_rot_table,
                self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_tra_table, self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_rot_table,
                self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_tra_table, self.syrthesIHMCollector.Output_2D_form.Op_Dc_2D_table,
                self.syrthesIHMCollector.Output_2D_form.Op_Sb_2D_table, self.syrthesIHMCollector.Output_2D_form.Op_Vb_2D_table,
                self.syrthesIHMCollector.Output_3D_form.Op_Dc_3D_table, self.syrthesIHMCollector.Output_3D_form.Op_Sb_3D_table,
                self.syrthesIHMCollector.Output_3D_form.Op_Vb_3D_table, self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table,
                self.syrthesIHMCollector.Material_radiation_properties_form.Mrp_table, self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irt_table,
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irf_table, self.syrthesIHMCollector.View_factor_2D_form.Vf_Ip_2D_table,
                self.syrthesIHMCollector.View_factor_2D_form.Vf_Sy_2D_table, self.syrthesIHMCollector.View_factor_2D_form.Vf_Pe_2D_table,
                self.syrthesIHMCollector.View_factor_3D_form.Vf_Ip_3D_table, self.syrthesIHMCollector.View_factor_3D_form.Vf_Sy_3D_table,
                self.syrthesIHMCollector.View_factor_3D_form.Vf_Pe_3D_table, self.syrthesIHMCollector.Solar_aspect_form.Sa_Sht_table,
                self.syrthesIHMCollector.Solar_aspect_form.Sa_Hm_table, self.syrthesIHMCollector.Solar_aspect_form.Sa_Sm_table,
                self.syrthesIHMCollector.Control_form.By_Block_table, self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form.Vch_Ic_TPvPt_table,
                self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form.Vch_St_TPvPt_table, self.syrthesIHMCollector.Initial_conditions_hum_TPv_form.Vch_Ic_TPv_table,
                self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form.Vch_St_TPv_table,  self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Sc_table,
                self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_iso_2D_table,self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_aniso_2D_table,self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_iso_3D_table,self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_aniso_3D_table,
                self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Vc_table, self.syrthesIHMCollector.Solar_aspect_form.Csm_Db_table,
                self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form.Cont_res_hum_TPvPt_table, self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form.Cont_res_hum_TPv_table,
                # fluid1d
                self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table,
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table,
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table,
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table,
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl,
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl,
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP,
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table,
                self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table,
                self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table,
                self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table,
                self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table,
                self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table,
                # fluid0d
                self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table,
                self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table,
                self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table,
                self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table,
                #
                self.syrthesIHMCollector.Advanced_mode_form.Advanced_cmd_table]

        # define the contextual menu for tables
        for table in self.Tables :
            table.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            table.customContextMenuRequested.connect(self.contextMenuEvent2)
##             self.connect(table, SIGNAL("customContextMenuRequested(const QPoint &)"), self.contextMenuEvent2)

        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne.customContextMenuRequested.connect(self.contextMenuEvent2)
##         self.connect(self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne, SIGNAL("customContextMenuRequested(const QPoint &)"), self.contextMenuEvent2)
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne.customContextMenuRequested.connect(self.contextMenuEvent2)
##         self.connect(self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne, SIGNAL("customContextMenuRequested(const QPoint &)"), self.contextMenuEvent2)
        self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1df_lne.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1df_lne.customContextMenuRequested.connect(self.contextMenuEvent2)
##        self.connect(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1df_lne, SIGNAL("customContextMenuRequested(const QPoint &)"), self.contextMenuEvent2)


        # redimensionnement des tableaux
        for table in self.Tables:
            Hheader=table.horizontalHeader()
            Hheader.setStretchLastSection(False)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
            Hheader.setStretchLastSection(True)
            #Hheader.setResizeMode(3)
            pass
        # tuple des vues spécials (nécessitant une vérifcations de la dimension)
        self.Spec_frame=[self.syrthesIHMCollector.Physical_prop_3D_form,self.syrthesIHMCollector.Physical_prop_2D_form,
                    self.syrthesIHMCollector.Periodicity_2D_form, self.syrthesIHMCollector.Periodicity_3D_form,
                    self.syrthesIHMCollector.Output_2D_form, self.syrthesIHMCollector.Output_3D_form,
                    self.syrthesIHMCollector.View_factor_2D_form, self.syrthesIHMCollector.View_factor_3D_form,
                    self.syrthesIHMCollector.Initial_conditions_hum_TPv_form, self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form,
                    self.syrthesIHMCollector.Boundary_conditions_TPv_form, self.syrthesIHMCollector.Boundary_conditions_TPvPt_form,
                    self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form, self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form,
                    self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form, self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form,
                    self.syrthesIHMCollector.Material_humidity_properties_2D_form,self.syrthesIHMCollector.Material_humidity_properties_3D_form,
                    # fluid1d
                    self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form,
                    #
                    self.syrthesIHMCollector.Solar_aspect_form]

        # dissimulation des item de l'arborescence
        self.Radiation.setHidden(True)
        self.Humidity.setHidden(True)
        self.Conjugate_heat_transfer.setHidden(True)
        self.Advanced_mode.setHidden(True)
        # fluid1d
        self.fluid1d.setHidden(True)
        # fluid0d
        self.fluid0d.setHidden(True)
        #

        # dissimulation des champ d'édition de texte
        self.syrthesIHMCollector.Home_form.Ho_Ds_te.hide()
        self.syrthesIHMCollector.Solar_aspect_form.Csm_Us_frame.hide()
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_te.hide()
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_te.hide()

        # dissimulation de la groupbox
        self.syrthesIHMCollector.Control_form.Vt_Vap_gb.hide()
        self.syrthesIHMCollector.Control_form.Pv_Pt_var_gb.hide()
        # initialisation de la vue précédente
        self.previous=self.syrthesIHMCollector.Home_form

        # open data file from command shell
        self.OpeningFileShell()

        # text editor & pdf reader
        self.actionSave_Preferences.setVisible(False)
        self.readPref()

        self.runProcess = None

        # supprimer syrthes.run (il y en a, sinon Syrthes_running() ne démarre pas)
        try:
            os.remove(self.case.dirPath + os.sep + 'syrthes.run')
        except:
            pass

        self.refreshUserC()
        # Fin d'initialisation
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Advanced_mode_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Boundary_conditions_cond_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Boundary_conditions_rad_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Boundary_conditions_TPv_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Boundary_conditions_TPvPt_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Conjugate_heat_transfer_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Control_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Fake_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Filename_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Home_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Initial_conditions_cond_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Initial_conditions_hum_TPv_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Material_humidity_properties_2D_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Material_humidity_properties_3D_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Material_radiation_properties_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Output_2D_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Output_3D_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Periodicity_2D_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Periodicity_3D_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Physical_prop_2D_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Physical_prop_3D_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Spectral_parameters_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Running_options_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Solar_aspect_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.User_C_function_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.View_factor_2D_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.View_factor_3D_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Volumetric_conditions_cond_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form)
        # fluid1d
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Initial_conditions_fluid1d_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Physical_prop_fluid1d_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Geometrie_fluid1d_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Control_fluid1d_form)
        # fluid0d
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Boundary_conditions_fluid0d_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Physical_properties_fluid0d_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Geometrie_fluid0d_form)
        self.Home_horizontalLayout.addWidget(self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form)
        #
        self.syrthesIHMCollector.DialogNew.btnNewCase.clicked.connect(self.createNewCase)
##         self.connect(self.syrthesIHMCollector.DialogNew.btnNewCase, SIGNAL("clicked()"), self.createNewCase)
        self.syrthesIHMCollector.DialogNew.btnNewData.clicked.connect(self.createNewData)
##         self.connect(self.syrthesIHMCollector.DialogNew.btnNewData, SIGNAL("clicked()"), self.createNewData)
        self.syrthesIHMCollector.DialogNew.btnOpenData.clicked.connect(self.openData)
##         self.connect(self.syrthesIHMCollector.DialogNew.btnOpenData, SIGNAL("clicked()"), self.openData)
        self.syrthesIHMCollector.Output_2D_form.Ai_but_2D_Op.clicked.connect(self.addInstants)
##         self.connect(self.syrthesIHMCollector.Output_2D_form.Ai_but_2D_Op, SIGNAL("clicked()"), self.addInstants)
        self.syrthesIHMCollector.Output_3D_form.Ai_but_3D_Op.clicked.connect(self.addInstants)
##         self.connect(self.syrthesIHMCollector.Output_3D_form.Ai_but_3D_Op, SIGNAL("clicked()"), self.addInstants)

        if syrthesIHMContext.isEmbedded() :
            self.show()
            if syrthesIHMContext.getDataAbsFullPath() == '' :
                self.syrthesIHMCollector.DialogNew.show()
        else :
            if self.case.dirPath == '' : # self.case.dirPath n'est pas affecté par OpeningFileShell (option -d)
                if not self.salomecfdstudy :
                    self.syrthesIHMCollector.DialogNew.show()
            else :
                self.show()


    def resizetree(self, parent=None): # redimensionnement de l'arborescence
        self.treeWidget.resizeColumnToContents(0)
        pass
    def contextMenuEvent2(self, event): # fonction de rappel du menu contextuel
        #self.menu.exec_(event.globalPos())
        table=QtWidgets.QApplication.focusWidget()
        if type(table) == QtWidgets.QTableWidget :
            self.action_New.setEnabled(True)
            self.action_Delete.setEnabled(True)
            self.action_Copy.setEnabled(True)
            self.action_Paste.setEnabled(True)
            self.menu.exec_(table.mapToGlobal(event))

        # special case
        if table == self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne or table == self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne \
        or table == self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1df_lne :
            self.action_New.setEnabled(False)
            self.action_Delete.setEnabled(False)
            self.action_Copy.setEnabled(False)
            self.action_Paste.setEnabled(False)
            self.menu.exec_(table.mapToGlobal(event))


    def Syrthes_running(self, parent=None): # fonction de rappel du bouton de lancement de Syrthes
        # check if Syrthes is running

        if os.access(self.case.dirPath + os.sep + 'syrthes.run', os.F_OK) :
            QtWidgets.QMessageBox.information(self, 'Error', "Either SYRTHES is running or a problem has occurred since the last execution. Click on ""Stop SYRTHES"" button to enable SYRTHES running")
            return

        # check if mesh files are given : MessageBox an warning
        meshExist = True

        #and (os.access(self.syrthesIHMCollector.Filename_form.Fn_Cd_lne.text().replace("\\","/"), os.F_OK) or os.access((self.case.dirPath + os.sep + self.syrthesIHMCollector.Filename_form.Fn_Cd_lne.text()).replace("\\","/"), os.F_OK)) : # check if Conduction mesh file is given

        if self.syrthesIHMCollector.Filename_form.Fn_Cd_lne.text() != '':
            # if mesh already partitionned is selected then check if the parts exist
            if self.syrthesIHMCollector.Running_options_form.Ro_Dp_cb.currentIndex()==2:
                nbPart=self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_cd.value()
                #meshRootName=self.convertOSsep(self.syrthesIHMCollector.Filename_form.Fn_Cd_lne.text())
                meshRootName=self.convertOSsepRel(self.syrthesIHMCollector.Filename_form.Fn_Cd_lne.text())
                meshPartDir = meshRootName.replace(os.sep+meshRootName.split(os.sep)[-1],"")
                #meshPartDir = os.path.dirname(os.path.realpath(meshRootName))
                meshRootName = meshRootName.split(os.sep)[-1]
                extname = meshRootName.split('.')[-1]
                meshRootName = meshRootName.replace("."+extname,"")
                # Modification BERTIN 05/09/2014 : saved meshRootName to put it further in self.syrthesIHMCollector.Filename_form.Fn_Cd_lne
                saved_meshRootName = meshRootName
                if len(meshRootName.split('_')) == 1:
                    "The mesh is not a part, we guess a part"
                    #meshRootName = meshRootName
                    meshPartInfo = str(nbPart).zfill(5) + "part00000"
                else:
                    meshPartInfo = meshRootName.split('_')[-1]
                    meshRootName = meshRootName.replace('_'+meshRootName.split('_')[-1],"")
                if nbPart == 1:
                    if not (os.access(self.syrthesIHMCollector.Filename_form.Fn_Cd_lne.text().replace("\\","/"), os.F_OK) or os.access((self.case.dirPath + os.sep + self.syrthesIHMCollector.Filename_form.Fn_Cd_lne.text()).replace("\\","/"), os.F_OK)): # check if Conduction mesh file is given
                        meshExist = False
                        QtWidgets.QMessageBox.information(self, 'Message', "Conduction mesh file is missing or doesn't exist.")
                else:
                    for id in range(nbPart) :
                        meshPart=self.case.dirPath + os.sep + meshPartDir + os.sep + meshRootName + '_' + str(nbPart).zfill(5) + 'part' + str(id).zfill(5) + '.'+extname
                        if not os.access(meshPart, os.F_OK):
                            meshExist = False
                            QtWidgets.QMessageBox.information(self, 'Message', "The part number %d out of %d of the Conduction mesh file is missing or doesn't exist."%(id,nbPart))
                            return
#                    self.syrthesIHMCollector.Filename_form.Fn_Cd_lne.setText(meshPartDir+os.sep+meshRootName+'.'+extname)
                    self.syrthesIHMCollector.Filename_form.Fn_Cd_lne.setText(meshPartDir+os.sep+saved_meshRootName+'.'+extname)
            # if Thermal radiation is check, then check if Radiation mesh file is given
            if self.syrthesIHMCollector.Home_form.Ho_Tr_ch.isChecked() :
                if (self.syrthesIHMCollector.Filename_form.Fn_Rm_lne.text() != '') and (os.access(self.syrthesIHMCollector.Filename_form.Fn_Rm_lne.text().replace("\\","/"), os.F_OK) or os.access((self.case.dirPath + os.sep + self.syrthesIHMCollector.Filename_form.Fn_Rm_lne.text()).replace("\\","/"), os.F_OK)) :
                    meshExist = True
                else:
                    meshExist = False
                    QtWidgets.QMessageBox.information(self, 'Message', "Radiation mesh file is missing or doesn't exist.")
                    return
            else:
                pass # meshExist is still True (default value)
        else:
            meshExist = False
            QtWidgets.QMessageBox.information(self, 'Message', "Conduction mesh file is missing or doesn't exist.")
            return

        if self.syrthesIHMCollector.Filename_form.Fn_Rnp_lne.text() == "" :
            QtWidgets.QMessageBox.information(self, 'Message', "Results names prefix is missing.")
            return

        if self.syrthesIHMCollector.Running_options_form.Ro_Ln_le.text() == "" :
            QtWidgets.QMessageBox.information(self, 'Message', "Listing names is missing.")
            return

        # check frequency
        if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0 :
            LE2 = self.syrthesIHMCollector.Output_3D_form.Le2_3D_Op
            Table = self.syrthesIHMCollector.Output_3D_form.Op_Dc_3D_table
        else :
            LE2 = self.syrthesIHMCollector.Output_2D_form.Le2_2D_Op
            Table = self.syrthesIHMCollector.Output_2D_form.Op_Dc_2D_table

        if boolFilledTable(Table, 1) and LE2.text() == "" :
            LE2.setText("1") # Affichage de la valeur par défaut
            QtWidgets.QMessageBox.information(self, 'Message', "Frequency of output is missing. value set to 1 ")
            return

        # check si nombre de pas de temps : Control_form.Le_Nts : MessageBox an warning
        Nts = self.syrthesIHMCollector.Control_form.Le_Nts
        if Nts.text() == "" :
#            Nts.setText("1") # Activation automatique
            QtWidgets.QMessageBox.information(self, 'Message', "Global number of time steps is missing or doesn't exist. Syrthes will not run.")
            return

        # check si le pas de temps : Control_form.Le_const_Ts : MessageBox an warning
        const_Ts = self.syrthesIHMCollector.Control_form.Le_const_Ts
        auto_It = self.syrthesIHMCollector.Control_form.Le_auto_It
        auto_Mt = self.syrthesIHMCollector.Control_form.Le_auto_Mt
        auto_Mpv = self.syrthesIHMCollector.Control_form.Le_auto_Mpv
        auto_Mpt = self.syrthesIHMCollector.Control_form.Le_auto_Mpt
        auto_Mts = self.syrthesIHMCollector.Control_form.Le_auto_Mts
        TableBck = self.syrthesIHMCollector.Control_form.By_Block_table
        # boolFilledTable non utilisable pour By_Block_table... on regarde alors si la premiere colonne est vide
        boolTableBck = False
        for i in range(TableBck.rowCount()):
            if boolFilledRow(TableBck, i, 0):
                boolTableBck = True
                break
        type_pas_de_temps = self.syrthesIHMCollector.Control_form.comb_time_st.currentIndex()
        if ( const_Ts.text() == "" and type_pas_de_temps == 0 ) or ( (auto_It.text() == "" or auto_Mt.text() == "" or auto_Mts.text() == "") and type_pas_de_temps == 1 ) or ( not boolTableBck and type_pas_de_temps == 2 ) :
#            const_Ts.setText("10") # Activation automatique
            QtWidgets.QMessageBox.information(self, 'Message', "Time step is missing or doesn't exist. Syrthes will not run.")
            return

        # if Humidity is check, then check  Physical properties for humididy
        if self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked() :
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0 :
                 TableIso = self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_iso_3D_table
                 TableAni = self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_aniso_3D_table
            else:
                 TableIso = self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_iso_2D_table
                 TableAni = self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_aniso_2D_table

            if not boolFilledTable(TableIso, 2) and not boolFilledTable(TableAni, 2) :
                 QtWidgets.QMessageBox.information(self, 'Message',
                                                   "Material humidity properties is missing or doesn't exist. Syrthes will not run.")
                 return
        else:
        # check if Physical properties for conduction :  : MessageBox an warning
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0 :

                 TableIso = self.syrthesIHMCollector.Physical_prop_3D_form.Iso_cond_3D_table
                 TableOrt = self.syrthesIHMCollector.Physical_prop_3D_form.Ort_cond_3D_table
                 TableAni = self.syrthesIHMCollector.Physical_prop_3D_form.Ani_cond_3D_table
            else:

                 TableIso = self.syrthesIHMCollector.Physical_prop_2D_form.Iso_cond_2D_table
                 TableOrt = self.syrthesIHMCollector.Physical_prop_2D_form.Ort_cond_2D_table
                 TableAni = self.syrthesIHMCollector.Physical_prop_2D_form.Ani_cond_2D_table


            if not boolFilledTable(TableIso, 2) and not boolFilledTable(TableOrt, 2) and not boolFilledTable(TableAni, 2):
                 QtWidgets.QMessageBox.information(self, 'Message',
                                                   "Physical properties is missing or doesn't exist. Syrthes will not run.")
                 return


        # sauvegarder le fichier data
        self.SavingFile()

        # delete file syrthes.stop and create file syrthes.run to enable the Run
        try:
        #if 1:
            print("suppression syrthes.stop")
            if os.access(self.case.dirPath + os.sep + 'syrthes.stop', os.F_OK) :
                os.remove(self.case.dirPath + os.sep + 'syrthes.stop')
            f = open(self.case.dirPath + os.sep + 'syrthes.run', 'w')
            f.write("You have clicked on Run button")
            f.close()
        except:
            pass

        print("Syrthes run by GUI")
        dir_lancement_IHM = os.path.abspath(os.curdir)
        print("old dir :", dir_lancement_IHM)
        os.chdir(self.case.dirPath)

        nbp_cd=self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_cd.value()
        nbp_ry=self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_ry.value()

        # chercher la variable d'environnement HOME_CYGWIN
        if os.name=='nt':
            HOME_CYGWIN = os.popen('echo %HOME_CYGWIN%').read()
            print("HOME_CYGWIN=", HOME_CYGWIN)
            HOME_CYGWIN = (HOME_CYGWIN.split('\n'))[0] # éliminer le \n à la fin de HOME_CYGWIN
            if HOME_CYGWIN == "" :
                QtWidgets.QMessageBox.information(self, 'Message', "HOME_CYGWIN not found. Syrthes will not run.")
                return
            HOME_CYGWIN = HOME_CYGWIN + os.sep
        else:
            HOME_CYGWIN = ""

        # constituer le nom d'un (des fichiers) .his
        self.case.setHisnamePrefix(self.syrthesIHMCollector.Filename_form.Fn_Rnp_lne.text(), self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_cd.value())

        # backup le listing et le .his
        for filei in range(self.case.getNbProc()) :
            if os.access(self.case.getHisFullPath(filei), os.F_OK):
                rm = self.case.getHisFullPath(filei)
                rmbak = rm + '.bak'
                try:
                    os.remove(rmbak)
                except:
                    pass
                try:
                    os.rename(rm, rmbak)
                except:
                    pass
        listing = self.case.dirPath + os.sep + str(self.syrthesIHMCollector.Running_options_form.Ro_Ln_le.text())
        if os.access(listing, os.F_OK):
            rm = listing
            rmbak = rm + '.bak'
            try:
                os.remove(rmbak)
            except:
                pass
            try:
                os.rename(rm, rmbak)
            except:
                pass

        # construire la commande
        com=HOME_CYGWIN + "python3 syrthes.py " + "-n " + str(nbp_cd) + " "
        if nbp_cd>nbp_ry:
            com=com+"-r "+str(nbp_ry)+" "
        com = com + "-d " + str(self.case.name) + " "
        if nbp_cd>1 :
            if self.syrthesIHMCollector.Running_options_form.Ro_Dp_cb.currentIndex()==0:
                com=com+"-t scotch "
            if self.syrthesIHMCollector.Running_options_form.Ro_Dp_cb.currentIndex()==1:
                com=com+"-t metis "
            if self.syrthesIHMCollector.Running_options_form.Ro_Dp_cb.currentIndex()==2:
                com=com+"-p "
        elif self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.isChecked():
            if self.syrthesIHMCollector.Running_options_form.Ro_Pre_cb.currentIndex()==1:
                com=com+" -p"
            elif self.syrthesIHMCollector.Running_options_form.Ro_Pre_cb.currentIndex()==0:
                com=com+" "
            pass

        if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
            CBB=self.syrthesIHMCollector.Output_3D_form.F_cb_3D_Op
        else :
            CBB=self.syrthesIHMCollector.Output_2D_form.F_cb_2D_Op
        if not CBB.isChecked():
            if self.syrthesIHMCollector.Running_options_form.Ro_Cr_cb.currentIndex()==0:
                com=com+"-v ensight "
            elif self.syrthesIHMCollector.Running_options_form.Ro_Cr_cb.currentIndex()==1:
                com=com+"-v med "
            else:
                pass
            pass
        if self.syrthesIHMCollector.Running_options_form.Ro_Ln_le.text()!='':
            com=com+"-l "+self.syrthesIHMCollector.Running_options_form.Ro_Ln_le.text()

        print("#MP SyrthesMain :: command =", com)
        com=str(com)

        # exécuter la commande
        #self.runProcess = subprocess.Popen(com, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        try:
            os.remove("stdout.txt")
        except:
            pass
        try:
            os.remove("stderr.txt")
        except:
            pass

        fout=open("stdout.txt","wb")
        fout.close()
        ferr=open("stderr.txt","wb")
        ferr.close()
        fout=open("stdout.txt","r+")
        ferr=open("stderr.txt","r+")
        #QApplication.setOverrideCursor(QCursor(QtCore.Qt.WaitCursor))
        self.setCursor(QtCore.Qt.BusyCursor)
        self.runProcess = subprocess.Popen(com, stdout=fout, stderr=ferr, shell=True)
        fout.close()
        ferr.close()
        #self.runProcess = subprocess.Popen(com, shell=True)
        # revenir à l'endroit où l'interface a été lancée
        #print "revenir à :", dir_lancement_IHM

        os.chdir(dir_lancement_IHM)

        # afficher Calculation Progress
        #global Main2

        try:
            if 'Main2' in dir(self):
                self.Main2.close()
            self.Main2 = calcView(self.syrthesIHMCollector.Home_form, self.syrthesIHMCollector.Control_form, self.syrthesIHMCollector.Filename_form, self.syrthesIHMCollector.Output_2D_form, self.syrthesIHMCollector.Output_3D_form, self.syrthesIHMCollector.Running_options_form, self.case, self.lastDir, self.runProcess)
            #for i in range(len(self.Main2.curves)) : # supprimer toutes les courbes
            #    self.Main2.curves[i].razData()
            #    self.Main2.curves[i].detach()
            #self.Main2.plot.replot()
            #self.Main2.updateCalcView()

        except:
            self.Main2 = calcView(self.syrthesIHMCollector.Home_form, self.syrthesIHMCollector.Control_form, self.syrthesIHMCollector.Filename_form, self.syrthesIHMCollector.Output_2D_form, self.syrthesIHMCollector.Output_3D_form, self.syrthesIHMCollector.Running_options_form, self.case, self.lastDir, self.runProcess) # si self.Main2 n'était pas créé, on le crée ici
        finally:
            if 'Main2' in dir(self):
                self.Main2.Syrthes_stopping.connect(self.Syrthes_stopping)
##             self.connect(self.Main2, SIGNAL("Syrthes_stopping"), self.Syrthes_stopping)
                self.Main2.Syrthes_completed.connect(self.Syrthes_completed)
##            self.connect(self.Main2, SIGNAL("Syrthes_completed"), self.Syrthes_completed)
                self.Main2.show()
                if self.Main2.tickflag == False:
                    self.Main2.tickflag = True
                    ticker=self.Main2.timer.singleShot(1000, self.Main2.Time_Clb)

        # griser le(s) bouton(s) Run
        self.syrthesIHMCollector.Running_options_form.Ro_Rs_Pb.setEnabled(False)
        self.action_Run_Syrthes.setEnabled(False)

    def affiche(self, f, nom = ''): # obsolète
        while True:
            out = self.fout.read(1)
            if out == '' and self.runProcess.poll() != None:
                pass #break
            if out != '':
                sys.stdout.write(out)
                sys.stdout.flush()

    def Syrthes_stopping(self, parent=None):
        # create file syrthes.stop and delete file syrthes.run to terminate correctly Syrthes
        self.unsetCursor()
        print('creation syrthes.stop')
        try:
            f = open(self.case.dirPath + os.sep + 'syrthes.stop', 'w')
            f.write("This file will be removed by GUI to enable SYRTHES to run")
            f.close()
        except:
            pass

        # supprimer syrthes.run (il y en a, sinon Syrthes_running() ne démarre pas)
        try:
            os.remove(self.case.dirPath + os.sep + 'syrthes.run')
        except:
            pass
        self.syrthesIHMCollector.Running_options_form.Ro_Rs_Pb.setEnabled(True)
        self.action_Run_Syrthes.setEnabled(True)
        self.runProcess = None


    def Syrthes_completed(self, parent=None):
        self.unsetCursor()
        print('Syrthes_completed action')
        # supprimer syrthes.run et syrthes.stop (il y en a, sinon Syrthes_running() ne démarre pas)
        try:
            #print 'suppression syrthes.run'
            #os.remove(self.case.dirPath + os.sep + 'syrthes.run')
            print('suppression syrthes.stop')
            os.remove(self.case.dirPath + os.sep + 'syrthes.stop')
        except:
            pass
        self.syrthesIHMCollector.Running_options_form.Ro_Rs_Pb.setEnabled(True)
        self.action_Run_Syrthes.setEnabled(True)
        self.runProcess = None

    def setmaxprocray(self, parent=None): # fonction de rappel permettant le nom dépassement de la valeur max de processeur pour le rayonnement à la valeur max de processeur pour la conduction
        self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_ry.setMaximum(self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_cd.value())

    def New_File(self, parent=None): # fonction de rappel du bouton de nouveau fichier
        self.syrthesIHMCollector.DialogNew.setWindowTitle("New")

        if not syrthesIHMContext.isEmbedded() :
            # mask Open button
            self.syrthesIHMCollector.DialogNew.btnOpenData.setVisible(False)
            self.syrthesIHMCollector.DialogNew.btnNewData.setGeometry(QtCore.QRect(260,260,131,31))
        else :
            # restore default
            self.syrthesIHMCollector.DialogNew.btnOpenData.setVisible(True)
            self.syrthesIHMCollector.DialogNew.btnOpenData.setGeometry(QtCore.QRect(287, 260, 101, 31))
            self.syrthesIHMCollector.DialogNew.btnNewData.setGeometry(QtCore.QRect(180, 260, 101, 31))

        self.syrthesIHMCollector.DialogNew.show()

    def Quitter(self, parent=None):
        identik = self.SavingCompare()
        if identik == False :
            reply = QtWidgets.QMessageBox.question(self, 'Message', "Do you want to save the current data file ?", \
                                                   QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No | \
                                                   QtWidgets.QMessageBox.Cancel)
            if reply == QtWidgets.QMessageBox.Yes :
                self.SavingFile()
            elif reply == QtWidgets.QMessageBox.Cancel :
                return
        sys.exit()

    def Screenshot(self, parent=None): # fonction de rappel de la capture d'écran
##        self.originalPixmap=QtGui.QPixmap.grabWidget(self) #.Syrthes_Mainwindow)
        self.originalPixmap=self.grab() #.Syrthes_Mainwindow)
        format= "png"
##         format= QtCore.QString("png")
        #QtCore.QDir.currentPath() suivant sera le chemin à partir duquel on a lancé l'IHM, ou le chemin vers le cas-test si ce dernier est défini

        screenshotPath=self.lastDir + self.tr(os.sep+"untitled.")+format

        fileName=QtWidgets.QFileDialog.getSaveFileName(self, self.tr("Save As"),
                            screenshotPath,
                            self.tr("*."+format+";;All Files (*)"))[0]
        if fileName != "":
            self.originalPixmap.save(fileName, format)
            self.lastDir = fileName.rsplit(os.sep, 1)[0] #update lastDir
        #self.initSYRTHESFont()

    def Statecal(self, parent=None): # fonction de rappel dépréciée (à enlever)
        if self.syrthesIHMCollector.Control_form.Ch_res_cal.checkState()==0:
            self.Ch_Sr_cb.setDisabled(1)
            self.syrthesIHMCollector.Control_form.lineEdit_39.setDisabled(1)
        elif self.syrthesIHMCollector.Control_form.Ch_res_cal.checkState()==2:
            self.Ch_Sr_cb.setEnabled(1)

    def SetCellWidget_improvePerf1(self):
        # initialisation des composants graphiques pour les tableaux standard
        for table in self.Type_Standard_improvePerf1 :
            rc=table.rowCount()
            i=0
            while (i<=rc):
                j=2 # 3rd column
                while j <= table.columnCount()-1 : # other text cells
                    if table.item(i,j) == None : # if the cell i,j doesn't contain data
                        item0 = QtWidgets.QTableWidgetItem()
                        item0.setText("")
                        table.setItem(i, j, item0)
                    j+=1
                i+=1

    def SetCellWidget_improvePerf2(self):
        # initialisation des composants graphiques pour les tableaux standard
        for table in self.Type_Standard_improvePerf2 :
            rc=table.rowCount()
            i=0
            while (i<=rc):
                j=2 # 3rd column
                while j <= table.columnCount()-1 : # other text cells
                    if table.item(i,j) == None : # if the cell i,j doesn't contain data
                        item0 = QtWidgets.QTableWidgetItem()
                        item0.setText("")
                        table.setItem(i, j, item0)
                    j+=1
                i+=1

    def SetCellWidget_improvePerf3(self):
        # initialisation des composants graphiques pour les tableaux avec un seul checkbox
        for table in self.Type_ChB_only_improvePerf3 :
            rc=table.rowCount()
            i=0
            while (i<=rc):
                j=1 # 2nd column
                while j <= table.columnCount()-1 : # other text cells
                    if table.item(i,j) == None : # if the cell i,j doesn't contain data
                        item0 = QtWidgets.QTableWidgetItem()
                        item0.setText("")
                        table.setItem(i, j, item0)
                    j+=1
                i+=1

    def SetCellWidget_improvePerf4(self):
        # initialisation des composants graphiques pour les tableaux avec un seul checkbox
        for table in self.Type_ChB_only_improvePerf4 :
            rc=table.rowCount()
            i=0
            while (i<=rc):
                j=1 # 2nd column
                while j <= table.columnCount()-1 : # other text cells
                    if table.item(i,j) == None : # if the cell i,j doesn't contain data
                        item0 = QtWidgets.QTableWidgetItem()
                        item0.setText("")
                        table.setItem(i, j, item0)
                    j+=1
                i+=1

    def SetCellWidget(self, parent=None): # fonction de synthétisation des composants graphiques des tableaux
        print("Please wait")
        # tuple du type standard de tableaux: checkbox, combobox
        # == self.dic_Table_type[*] = 1
        self.Type_Standard=[self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table, self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table,
                       self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table, self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table,
                       self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table, #self.Dens_2D_table,
                       #self.Heat_cap_2D_table,
                       self.syrthesIHMCollector.Physical_prop_2D_form.Iso_cond_2D_table,
                       self.syrthesIHMCollector.Physical_prop_2D_form.Ort_cond_2D_table, self.syrthesIHMCollector.Physical_prop_2D_form.Ani_cond_2D_table,
                       #self.Dens_3D_table, self.Heat_cap_3D_table,
                       self.syrthesIHMCollector.Physical_prop_3D_form.Iso_cond_3D_table, self.syrthesIHMCollector.Physical_prop_3D_form.Ort_cond_3D_table,
                       self.syrthesIHMCollector.Physical_prop_3D_form.Ani_cond_3D_table, self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table,
                       self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table, self.syrthesIHMCollector.Boundary_conditions_TPv_form.Bc_TPv_table,
                       self.syrthesIHMCollector.Boundary_conditions_TPvPt_form.Bc_TPvPt_table,
                       self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form.Cont_res_hum_TPv_table,self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form.Cont_res_hum_TPvPt_table,
                       # fluid1d
                       self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table,
                       self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table,
                       self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table,
                       self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table,
                       self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl,
                       self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl,
                       self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP,
                       self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table,
                       self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table,
                       self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table,
                       self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table,
                       self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table,
                       # fluid0d
                       self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table,
                       self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table,
                       self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table
                       ]

        self.Type_Standard_improvePerf1 = [self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table,
                                             self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table,
                                             self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table,
                                             self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table,
                                             self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table]

        self.Type_Standard_improvePerf2 = [self.syrthesIHMCollector.Physical_prop_2D_form.Iso_cond_2D_table,
                                             self.syrthesIHMCollector.Physical_prop_2D_form.Ort_cond_2D_table,
                                             self.syrthesIHMCollector.Physical_prop_2D_form.Ani_cond_2D_table,
                                             self.syrthesIHMCollector.Physical_prop_3D_form.Iso_cond_3D_table,
                                             self.syrthesIHMCollector.Physical_prop_3D_form.Ort_cond_3D_table,
                                             self.syrthesIHMCollector.Physical_prop_3D_form.Ani_cond_3D_table]

        #tuple du type avec seulement la checkbox
        # == self.dic_Table_type[*] = 2
        self.Type_ChB_only=[self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_rot_table, self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_tra_table,
                       self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_rot_table, self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_tra_table,
                       self.syrthesIHMCollector.Output_2D_form.Op_Dc_2D_table, self.syrthesIHMCollector.Output_2D_form.Op_Sb_2D_table, self.syrthesIHMCollector.Output_2D_form.Op_Vb_2D_table,
                       self.syrthesIHMCollector.Output_3D_form.Op_Dc_3D_table, self.syrthesIHMCollector.Output_3D_form.Op_Sb_3D_table, self.syrthesIHMCollector.Output_3D_form.Op_Vb_3D_table,
                       self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table, self.syrthesIHMCollector.Material_radiation_properties_form.Mrp_table, self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irt_table,
                       self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irf_table, self.syrthesIHMCollector.View_factor_2D_form.Vf_Ip_2D_table,
                       self.syrthesIHMCollector.View_factor_2D_form.Vf_Sy_2D_table, self.syrthesIHMCollector.View_factor_2D_form.Vf_Pe_2D_table,
                       self.syrthesIHMCollector.View_factor_3D_form.Vf_Ip_3D_table, self.syrthesIHMCollector.View_factor_3D_form.Vf_Sy_3D_table,
                       self.syrthesIHMCollector.View_factor_3D_form.Vf_Pe_3D_table, self.syrthesIHMCollector.Solar_aspect_form.Sa_Sht_table,
                       self.syrthesIHMCollector.Solar_aspect_form.Sa_Hm_table, self.syrthesIHMCollector.Solar_aspect_form.Sa_Sm_table, self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Sc_table,
                       self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Vc_table, self.syrthesIHMCollector.Solar_aspect_form.Csm_Db_table,
                       #fluid_1d
                       self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table,
                       #fluid_0d
                       self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table,
                       self.syrthesIHMCollector.Advanced_mode_form.Advanced_cmd_table]

        self.Type_ChB_only_improvePerf3 = [self.syrthesIHMCollector.Output_2D_form.Op_Dc_2D_table,
                                           self.syrthesIHMCollector.Output_2D_form.Op_Sb_2D_table,
                                           self.syrthesIHMCollector.Output_2D_form.Op_Vb_2D_table,
                                           self.syrthesIHMCollector.Output_3D_form.Op_Dc_3D_table,
                                           self.syrthesIHMCollector.Output_3D_form.Op_Sb_3D_table,
                                           self.syrthesIHMCollector.Output_3D_form.Op_Vb_3D_table]

        self.Type_ChB_only_improvePerf4 = [self.syrthesIHMCollector.View_factor_2D_form.Vf_Ip_2D_table,
                                           self.syrthesIHMCollector.View_factor_2D_form.Vf_Sy_2D_table,
                                           self.syrthesIHMCollector.View_factor_2D_form.Vf_Pe_2D_table,
                                           self.syrthesIHMCollector.View_factor_3D_form.Vf_Ip_3D_table,
                                           self.syrthesIHMCollector.View_factor_3D_form.Vf_Sy_3D_table,
                                           self.syrthesIHMCollector.View_factor_3D_form.Vf_Pe_3D_table]

        # tuple du type avec 2 combobox modèle 3 équations
        # == self.dic_Table_type[*] = 4
        self.Type_2Cmb_TPvPt = [self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form.Vch_Ic_TPvPt_table, self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form.Vch_St_TPvPt_table]

        # tuple du type avec 2 combobox modèle 2 équations
        # == self.dic_Table_type[*] = 3
        self.Type_2Cmb_TPv = [self.syrthesIHMCollector.Initial_conditions_hum_TPv_form.Vch_Ic_TPv_table, self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form.Vch_St_TPv_table]

        # tuple du type avec combobox enrichis
        # == self.dic_Table_type[*] = 5
        self.Type_ECmb = [self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_iso_2D_table,self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_aniso_2D_table,self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_iso_3D_table,self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_aniso_3D_table]

        # tuple du type avec aucun composant graphique
        # == self.dic_Table_type[*] = 6
        self.Type_nothing=[self.syrthesIHMCollector.Control_form.By_Block_table]

##         self.Strlist=QtCore.QStringList()
        self.Strlist=[]

        # initialisation des composants graphiques pour les tableaux standard
        for table in self.Type_Standard :
            rc=table.rowCount()
            i=0
            while (i<=rc):
                self.checkbox=QtWidgets.QCheckBox()
                self.combobox=QtWidgets.QComboBox()
                self.combobox.addItem("")
                self.combobox.addItem("")
                self.combobox.addItem("")
                self.combobox.setItemText(0, QtCore.QCoreApplication.translate("MainWindow", "Constant"))
                self.combobox.setItemText(1, QtCore.QCoreApplication.translate("MainWindow", "Function"))
                self.combobox.setItemText(2, QtCore.QCoreApplication.translate("MainWindow", "Program"))
                # Modification pour tableau couplage 1D
                if table == self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table :
                    self.combobox.setItemText(1, QtCore.QCoreApplication.translate("MainWindow", "Colburn"))
                    self.combobox.setItemText(0, QtCore.QCoreApplication.translate("MainWindow", "Constant"))

                self.combobox.currentIndexChanged.connect(self.Table_prog)
##                 self.connect(self.combobox, SIGNAL("currentIndexChanged(int)"), self.Table_prog)
                self.checkbox.setGeometry(100,200,10,20)
                table.setCellWidget(i,0,self.checkbox)
                self.checkbox.setChecked(True)
                self.checkbox.stateChanged.connect(self.forcedCheck_table)
##                 self.connect(self.checkbox, SIGNAL("stateChanged(int)"), self.forcedCheck_table)
                table.setCellWidget(i,1,self.combobox)

                # simple text cells
                if (table not in self.Type_Standard_improvePerf1) and (table not in self.Type_Standard_improvePerf2) :
                    j=2 # 3rd column
                    while j <= table.columnCount()-1 : # other text cells
                        item0 = QtWidgets.QTableWidgetItem()
                        item0.setText("")
                        table.setItem(i, j, item0)
                        j+=1

                self.Strlist.append("")
                table.setVerticalHeaderLabels(self.Strlist)
                table.takeVerticalHeaderItem(i)
                i=i+1

        # initialisation des composants graphiques pour les tableaux à modèle 3 équations
        for table in self.Type_2Cmb_TPvPt :
            rc=table.rowCount()
            i=0
            while (i<=rc):
                self.checkbox=QtWidgets.QCheckBox()
                self.combobox=QtWidgets.QComboBox()
                self.combobox.addItem("")
                self.combobox.addItem("")
                self.combobox.addItem("")
                self.combobox.setItemText(0, QtCore.QCoreApplication.translate("MainWindow", "Constant"))
                self.combobox.setItemText(1, QtCore.QCoreApplication.translate("MainWindow", "Function"))
                self.combobox.setItemText(2, QtCore.QCoreApplication.translate("MainWindow", "Program"))
                self.combobox.currentIndexChanged.connect(self.Table_prog)
##                 self.connect(self.combobox, SIGNAL("currentIndexChanged(int)"), self.Table_prog)

                self.combobox2=QtWidgets.QComboBox()
                self.combobox2.addItem("")
                self.combobox2.addItem("")
                self.combobox2.addItem("")
                self.combobox2.setItemText(0, QtCore.QCoreApplication.translate("MainWindow", "T"))
                self.combobox2.setItemText(1, QtCore.QCoreApplication.translate("MainWindow", "Pv"))
                self.combobox2.setItemText(2, QtCore.QCoreApplication.translate("MainWindow", "Pt"))
                self.combobox2.currentIndexChanged.connect(self.forcedCheck_table)
##                 self.connect(self.combobox2, SIGNAL("currentIndexChanged(int)"), self.forcedCheck_table)

                self.checkbox.setGeometry(100,200,10,20)
                table.setCellWidget(i,0,self.checkbox)
                self.checkbox.setChecked(True)
                self.checkbox.stateChanged.connect(self.forcedCheck_table)
##                self.connect(self.checkbox, SIGNAL("stateChanged(int)"), self.forcedCheck_table)

                table.setCellWidget(i,1,self.combobox)
                table.setCellWidget(i,2,self.combobox2)

                # simple text cells
                j=3 # 4th column
                while j <= table.columnCount()-1 : # other text cells
                    item0 = QtWidgets.QTableWidgetItem()
                    item0.setText("")
                    table.setItem(i, j, item0)
                    j+=1

                self.Strlist.append("")
                table.setVerticalHeaderLabels(self.Strlist)
                table.takeVerticalHeaderItem(i)
                i=i+1

        # initialisation des composants graphiques pour les tableaux 2 équations
        for table in self.Type_2Cmb_TPv :
            rc=table.rowCount()
            i=0
            while (i<=rc):
                self.checkbox=QtWidgets.QCheckBox()
                self.combobox=QtWidgets.QComboBox()
                self.combobox.addItem("")
                self.combobox.addItem("")
                self.combobox.addItem("")
 ##                self.combobox.addItem("")
##                 self.combobox.addItem("")
##                 self.combobox.addItem("")
                self.combobox.setItemText(0, QtCore.QCoreApplication.translate("MainWindow", "Constant"))
                self.combobox.setItemText(1, QtCore.QCoreApplication.translate("MainWindow", "Function"))
                self.combobox.setItemText(2, QtCore.QCoreApplication.translate("MainWindow", "Program"))
                self.combobox.currentIndexChanged.connect(self.Table_prog)
##                 self.connect(self.combobox, SIGNAL("currentIndexChanged(int)"), self.Table_prog)

                self.combobox2=QtWidgets.QComboBox()
                self.combobox2.addItem("")
                self.combobox2.addItem("")
                self.combobox2.setItemText(0, QtCore.QCoreApplication.translate("MainWindow", "T"))
                self.combobox2.setItemText(1, QtCore.QCoreApplication.translate("MainWindow", "Pv"))
                self.combobox2.currentIndexChanged.connect(self.forcedCheck_table)
##                 self.connect(self.combobox2, SIGNAL("currentIndexChanged(int)"), self.forcedCheck_table)

                self.checkbox.setGeometry(100,200,10,20)
                table.setCellWidget(i,0,self.checkbox)
                self.checkbox.setChecked(True)
                self.checkbox.stateChanged.connect(self.forcedCheck_table)
##                 self.connect(self.checkbox, SIGNAL("stateChanged(int)"), self.forcedCheck_table)

                table.setCellWidget(i,1,self.combobox)
                table.setCellWidget(i,2,self.combobox2)

                # simple text cells
                j=3 # 4th column
                while j <= table.columnCount()-1 : # other text cells
                    item0 = QtWidgets.QTableWidgetItem()
                    item0.setText("")
                    table.setItem(i, j, item0)
                    j+=1

                self.Strlist.append("")
                table.setVerticalHeaderLabels(self.Strlist)
                table.takeVerticalHeaderItem(i)
                i=i+1


        # initialisation des composants graphiques pour les tableaux avec combobox enrichis
        for table in self.Type_ECmb :
            rc=table.rowCount()
            i=0
            while (i<=rc):
                self.checkbox=QtWidgets.QCheckBox()
                self.combobox=QtWidgets.QComboBox()
                j=0

                # Fichier syr_hmt_material.txt
                Matfile=open(self.Matfile_path(), "r")

                Matstr=Matfile.readline().rstrip()

                while Matstr:
                    Matstr=Matfile.readline().rstrip()
                    if Matstr!='':
                        self.combobox.addItem("")
                        self.combobox.setItemText(j, QtCore.QCoreApplication.translate("MainWindow", Matstr))
                        j=j+1

                self.checkbox.setGeometry(100,200,10,20)
                table.setCellWidget(i,0,self.checkbox)
                self.checkbox.setChecked(True)
                self.checkbox.stateChanged.connect(self.forcedCheck_table)
##                 self.connect(self.checkbox, SIGNAL("stateChanged(int)"), self.forcedCheck_table)
                table.setCellWidget(i,1,self.combobox)

                # simple text cells
                j=2 # 3rd column
                while j <= table.columnCount()-1 : # other text cells
                    item0 = QtWidgets.QTableWidgetItem()
                    item0.setText("")
                    table.setItem(i, j, item0)
                    j+=1

                self.Strlist.append("")
                table.setVerticalHeaderLabels(self.Strlist)
                table.takeVerticalHeaderItem(i)
                i=i+1

        # initialisation des composants graphiques pour les tableaux avec un seul checkbox
        for table in self.Type_ChB_only :
            rc=table.rowCount()
            i=0
            while (i<=rc):
                self.checkbox=QtWidgets.QCheckBox()
                table.setCellWidget(i,0,self.checkbox)
                self.checkbox.setChecked(True)
                self.checkbox.stateChanged.connect(self.forcedCheck_table)
##                 self.connect(self.checkbox, SIGNAL("stateChanged(int)"), self.forcedCheck_table)

                # simple text cells
                if (table not in self.Type_ChB_only_improvePerf3) and (table not in self.Type_ChB_only_improvePerf4) :
                    j=1 # 2nd column
                    while j <= table.columnCount()-1 : # other text cells
                        item0 = QtWidgets.QTableWidgetItem()
                        item0.setText("")
                        table.setItem(i, j, item0)
                        j+=1

                if (table==self.syrthesIHMCollector.Output_2D_form.Op_Dc_2D_table or table==self.syrthesIHMCollector.Output_3D_form.Op_Dc_3D_table or
                    table==self.syrthesIHMCollector.Output_2D_form.Op_Sb_2D_table or table==self.syrthesIHMCollector.Output_2D_form.Op_Vb_2D_table or
                    table==self.syrthesIHMCollector.Output_3D_form.Op_Sb_3D_table or table==self.syrthesIHMCollector.Output_3D_form.Op_Vb_3D_table or
                    table==self.syrthesIHMCollector.Solar_aspect_form.Sa_Sht_table):
                    pass
##                elif table==self.syrthesIHMCollector.Output_3D_form.Op_Dc_3D_table:
##                    pass
                else :
                    table.setVerticalHeaderLabels(self.Strlist)
                i=i+1
            if table == self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table : # default values
                table.item(0,1).setText("1")
                table.item(0,2).setText("1e-10")
                table.item(0,3).setText("10")


        # initialisation des tableaux avec aucun composant graphique
        for table in self.Type_nothing:
            rc=table.rowCount()
            i=0
            while (i<=rc):
                j=0 # 1st column
                while j <= table.columnCount()-1 : # other text cells
                    item0 = QtWidgets.QTableWidgetItem()
                    item0.setText("")
                    table.setItem(i, j, item0)
                    j+=1
                table.setVerticalHeaderLabels(self.Strlist)
                i=i+1

    def Matfile_path(self):

        # Retourne le chemin du fichier material syr_hmt_material.txt
        # soit dans un cas (priorite 1 s'il existe)
        # soit dans le syrthes4_home (priorite 2 s'il existe)
        # soit par defaut (priorite 3)

        # Recuperation de SYRTHES4_HOME
        if os.name=='nt':
            SYRTHES_home_path = os.popen('echo %SYRTHES4_HOME%').read()
        else :
            SYRTHES_home_path = os.popen('echo $SYRTHES4_HOME').read()
        SYRTHES_home_path = (SYRTHES_home_path.split('\n'))[0] # éliminer le \n à la fin de SYRTHES_home_path

        # Recuperation du chemin du cas passe en ligne de commande par -d
        case_d_path = os.path.dirname(syrthesIHMContext.getDataAbsFullPath())

        # fichier syr_hmt_material.txt par defaut
        #MPpath = syrthesIHMContext.getExeAbsDirPath()
        path = os.getenv('SYRTHES4_HOME')+ os.sep + "lib"+ os.sep + "syrthesGui"
        material_default = path + os.sep + "22x22" + os.sep + "syr_hmt_material.txt"

        # fichier syr_hmt_material.txt dans SYRTHES4_HOME/include
        # récupérer la variable d'environnement $SYRTHES4_HOME
        material_dev = ""
        if SYRTHES_home_path != "" and SYRTHES_home_path != "%SYRTHES4_HOME%" :
            material_dev = SYRTHES_home_path + os.sep + "include" + os.sep + "syr_hmt_material.txt"

        # fichier syr_hmt_material.txt du cas
        material_case = ""
        if case_d_path != "" and  self.case.dirPath == "":
            material_case = os.path.dirname(syrthesIHMContext.getDataAbsFullPath()) + os.sep + "mylibmat_include" + os.sep + "syr_hmt_material.txt"
        elif self.case.dirPath != "":
            material_case = self.case.dirPath + os.sep + "mylibmat_include" + os.sep + "syr_hmt_material.txt"

        if os.path.isfile(material_case):
            return material_case    # priorite 1
        elif os.path.isfile(material_dev):
            return material_dev     # priorite 2
        else:
            return material_default # priorite 3

    def FrameSelectBis(self):
        widget = self.treeWidget.selectedItems()[0]
        self.FrameSelect(widget)
        pass

    def FrameSelect(self, a): # fonction de séléction des vues
        waitCursor = QtGui.QCursor(Qt.Qt.WaitCursor)
        QtWidgets.QApplication.setOverrideCursor(waitCursor)
        for treew in self.treew:
            if a == treew :
                if treew== self.Physical_Properties :
                    self.SetCellWidget_improvePerf2() # init simple text cells
                    if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Physical_prop_3D_form)
                    else :
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Physical_prop_2D_form)


                elif treew== self.Material_Properties_hum :
                    if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Material_humidity_properties_3D_form)
                    else :
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Material_humidity_properties_2D_form)

                elif treew== self.Periodicity :
                    if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Periodicity_3D_form)

                    else :
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Periodicity_2D_form)

                elif treew== self.Output :
                    self.SetCellWidget_improvePerf3() # init simple text cells
                    if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Output_3D_form)
                    else :
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Output_2D_form)

                elif treew==self.View_factor :
                    self.SetCellWidget_improvePerf4() # init simple text cells
                    if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.View_factor_3D_form)
                    else :
                        self.Hide_Spec_frame(self.syrthesIHMCollector.View_factor_2D_form)

                elif treew==self.Solar_aspect :
                    if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Solar_aspect_form)
                    else:
                        self.previous.show()

                elif treew==self.Boundary_conditions_hum :
                    if self.syrthesIHMCollector.Home_form.Hm_cmb.currentIndex()==0:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Boundary_conditions_TPv_form)
                    else:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Boundary_conditions_TPvPt_form)

                elif treew==self.Volumetric_conditions_hum :
                    if self.syrthesIHMCollector.Home_form.Hm_cmb.currentIndex()==0:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form)
                    else:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form)

                elif treew==self.Initial_conditions_hum :
                    if self.syrthesIHMCollector.Home_form.Hm_cmb.currentIndex()==0:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Initial_conditions_hum_TPv_form)
                    else:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form)

                elif treew==self.Contact_resistance_humidity :
                    if self.syrthesIHMCollector.Home_form.Hm_cmb.currentIndex()==0:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form)
                    else:
                        self.Hide_Spec_frame(self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form)

                elif treew== self.Conduction :
                    self.previous.show()

                elif treew== self.Radiation :
                    self.previous.show()

                elif treew== self.Humidity :
                    self.previous.show()

                elif treew== self.Boundary_conditions_fluid1d and self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0 :
                    self.Hide_Spec_frame(self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form)

                else :
                    if treew== self.Boundary_conditions_cond :
                        self.SetCellWidget_improvePerf1() # init simple text cells
                    self.dic_tree_frame[id(treew)].show()
                    self.previous=self.dic_tree_frame[id(treew)]
                    for spec in self.Spec_frame:
                        spec.hide()

            else :
                self.dic_tree_frame[id(treew)].hide()

        QtWidgets.QApplication.restoreOverrideCursor()

    def Hide_Spec_frame(self, treew): # fonction de dissimulation des vues spécial
        for spec in self.Spec_frame:
            if spec==treew:
                spec.show()
                self.previous=spec
            else:
                spec.hide()

    def Time_step_select(self, a): # fonction d'affichage/dissimulation du type de définition des pas de temps

        if a==0:
            self.syrthesIHMCollector.Control_form.Constant_frame.show()
            self.syrthesIHMCollector.Control_form.Automatic_frame.hide()
            self.syrthesIHMCollector.Control_form.By_block_frame.hide()
        elif a==1:
            self.syrthesIHMCollector.Control_form.Automatic_frame.show()
            self.syrthesIHMCollector.Control_form.Constant_frame.hide()
            self.syrthesIHMCollector.Control_form.By_block_frame.hide()
        elif a==2:
            self.syrthesIHMCollector.Control_form.By_block_frame.show()
            self.syrthesIHMCollector.Control_form.Constant_frame.hide()
            self.syrthesIHMCollector.Control_form.Automatic_frame.hide()
        elif a==3:
            self.syrthesIHMCollector.Control_form.Constant_frame.hide()
            self.syrthesIHMCollector.Control_form.Automatic_frame.hide()
            self.syrthesIHMCollector.Control_form.By_block_frame.hide()

    def Modelling_select(self, a): # fonction d'affichage/dissimulation du type
        if a==0:
            self.syrthesIHMCollector.Solar_aspect_form.Csm_frame.show()
            self.syrthesIHMCollector.Solar_aspect_form.Asm_frame.hide()

        elif a==1:
            self.syrthesIHMCollector.Solar_aspect_form.Csm_frame.hide()
            self.syrthesIHMCollector.Solar_aspect_form.Asm_frame.show()

    def Constant_Modelling_select(self, a): # fonction de rappel d'affichage/dissimulation du type de modélisation solaire
        if a==0:
            self.syrthesIHMCollector.Solar_aspect_form.Csm_At_frame.show()
            self.syrthesIHMCollector.Solar_aspect_form.Csm_Us_frame.hide()

        elif a==1:
            self.syrthesIHMCollector.Solar_aspect_form.Csm_Us_frame.show()
            self.syrthesIHMCollector.Solar_aspect_form.Csm_At_frame.hide()

    def New_Line(self, parent=None): # fonction de rappel de l'ajout d'une ligne dans un tableau
        table=QtWidgets.QApplication.focusWidget()
        cr=table.currentRow()
        cr=cr+1

        table.insertRow(cr)
        if table==self.syrthesIHMCollector.Output_2D_form.Op_Dc_2D_table:
            pass
        elif table==self.syrthesIHMCollector.Output_3D_form.Op_Dc_3D_table:
            pass
        else :
            table.setVerticalHeaderLabels(self.Strlist)
        if self.dic_Table_type[table] == 1 :
            self.checkbox=QtWidgets.QCheckBox()
            table.setCellWidget(cr,0,self.checkbox)
            self.checkbox.setChecked(True)
            self.combobox=QtWidgets.QComboBox()
            self.combobox.addItem("")
            self.combobox.addItem("")
            self.combobox.addItem("")
            self.combobox.setItemText(0, QtCore.QCoreApplication.translate("MainWindow", "Constant"))
            self.combobox.setItemText(1, QtCore.QCoreApplication.translate("MainWindow", "Function"))
            self.combobox.setItemText(2, QtCore.QCoreApplication.translate("MainWindow", "Program"))
            # Modification pour tableau couplage 1D
            if table == self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table :
                self.combobox.setItemText(1, QtCore.QCoreApplication.translate("MainWindow", "Colburn"))
                self.combobox.setItemText(0, QtCore.QCoreApplication.translate("MainWindow", "Constant"))
            self.combobox.currentIndexChanged.connect(self.Table_prog)
##             self.connect(self.combobox, SIGNAL("currentIndexChanged(int)"), self.Table_prog)

            table.setCellWidget(cr,1,self.combobox)
            Hheader=table.horizontalHeader()
            Hheader.setStretchLastSection(False)
            table.resizeColumnsToContents()
            Hheader.setStretchLastSection(True)
        elif self.dic_Table_type[table] == 2:
            self.checkbox=QtWidgets.QCheckBox()
            table.setCellWidget(cr,0,self.checkbox)
            self.checkbox.setChecked(True)
            Hheader=table.horizontalHeader()
            Hheader.setStretchLastSection(False)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
            Hheader.setStretchLastSection(True)
        elif self.dic_Table_type[table] == 3:
            self.checkbox=QtWidgets.QCheckBox()
            table.setCellWidget(cr,0,self.checkbox)
            self.checkbox.setChecked(True)
            self.combobox=QtWidgets.QComboBox()
            self.combobox.addItem("")
            self.combobox.addItem("")
            self.combobox.addItem("")
            self.combobox.setItemText(0, QtCore.QCoreApplication.translate("MainWindow", "Constant"))
            self.combobox.setItemText(1, QtCore.QCoreApplication.translate("MainWindow", "Function"))
            self.combobox.setItemText(2, QtCore.QCoreApplication.translate("MainWindow", "Program"))
            self.combobox.currentIndexChanged.connect(self.Table_prog)
##             self.connect(self.combobox, SIGNAL("currentIndexChanged(int)"), self.Table_prog)

            self.combobox2=QtWidgets.QComboBox()
            self.combobox2.addItem("")
            self.combobox2.addItem("")
            self.combobox2.setItemText(0, QtCore.QCoreApplication.translate("MainWindow", "T"))
            self.combobox2.setItemText(1, QtCore.QCoreApplication.translate("MainWindow", "Pv"))

            table.setCellWidget(cr,1,self.combobox)
            table.setCellWidget(cr,2,self.combobox2)
            Hheader=table.horizontalHeader()
            Hheader.setStretchLastSection(False)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
            Hheader.setStretchLastSection(True)
        elif self.dic_Table_type[table] == 4:
            self.checkbox=QtWidgets.QCheckBox()
            table.setCellWidget(cr,0,self.checkbox)
            self.checkbox.setChecked(True)
            self.combobox=QtWidgets.QComboBox()
            self.combobox.addItem("")
            self.combobox.addItem("")
            self.combobox.addItem("")
            self.combobox.setItemText(0, QtCore.QCoreApplication.translate("MainWindow", "Constant"))
            self.combobox.setItemText(1, QtCore.QCoreApplication.translate("MainWindow", "Function"))
            self.combobox.setItemText(2, QtCore.QCoreApplication.translate("MainWindow", "Program"))
            self.combobox.currentIndexChanged.connect(self.Table_prog)
##             self.connect(self.combobox, SIGNAL("currentIndexChanged(int)"), self.Table_prog)

            self.combobox2=QtWidgets.QComboBox()
            self.combobox2.addItem("")
            self.combobox2.addItem("")
            self.combobox2.addItem("")
            self.combobox2.setItemText(0, QtCore.QCoreApplication.translate("MainWindow", "T"))
            self.combobox2.setItemText(1, QtCore.QCoreApplication.translate("MainWindow", "Pv"))
            self.combobox2.setItemText(2, QtCore.QCoreApplication.translate("MainWindow", "Pt"))
            table.setCellWidget(cr,1,self.combobox)
            table.setCellWidget(cr,2,self.combobox2)

            Hheader=table.horizontalHeader()
            Hheader.setStretchLastSection(False)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
            Hheader.setStretchLastSection(True)

        elif self.dic_Table_type[table] == 5:
            self.checkbox=QtWidgets.QCheckBox()
            table.setCellWidget(cr,0,self.checkbox)
            self.checkbox.setChecked(True)
            self.combobox=QtWidgets.QComboBox()
            j=0

            # Fichier syr_hmt_material.txt
            Matfile=open(self.Matfile_path(), "r")

            Matstr=Matfile.readline().rstrip()

            while Matstr:
                Matstr=Matfile.readline().rstrip()
                if Matstr!='':
                    self.combobox.addItem("")
                    self.combobox.setItemText(j, QtCore.QCoreApplication.translate("MainWindow", Matstr))
                    j=j+1
            table.setCellWidget(cr,1,self.combobox)
            Hheader=table.horizontalHeader()
            Hheader.setStretchLastSection(False)
            table.resizeColumnsToContents()
            table.resizeRowsToContents()
            Hheader.setStretchLastSection(True)

    def Del_Line(self, parent=None): # fonction de rappel de la suppression d'une ligne de tableau

        table=QtWidgets.QApplication.focusWidget()
        cr=table.currentRow()
        table.removeRow(cr)

    def Copy(self, parent=None): # Fonction de rappel de la copie dans un tableau par le menu contextuel
        syrthesIHMContext.reinitClipboard()
        table=QtWidgets.QApplication.focusWidget()
        ranges=table.selectedRanges()[0]
        i=ranges.topRow()
        j=ranges.leftColumn()
        while i<=ranges.bottomRow():
            while j<=ranges.rightColumn():
                if table.item(i,j) != None :
                    syrthesIHMContext.addToClipboard(table.item(i,j).text())
                else :
                    syrthesIHMContext.addToClipboard("")
                j=j+1
            j=ranges.leftColumn()
            i=i+1

    def Paste(self, parent=None): # Fonction de rappel du collage dans un tableau par le menu contextuel
        table=QtWidgets.QApplication.focusWidget()
        ranges=table.selectedRanges()[0]
        i=ranges.topRow()
        j=ranges.leftColumn()
        k=0
        while i<=ranges.bottomRow():
            while j<=ranges.rightColumn():
                DAT=QtWidgets.QTableWidgetItem()
                DAT.setText(syrthesIHMContext.getFromClipboard(k))
                table.setItem(i,j,DAT)
                k=k+1
                j=j+1
            j=ranges.leftColumn()
            i=i+1

    def slotAddFromSalome(self, parent=None): # Fonction de rappel du collage dans un tableau par le menu contextuel
        """
        When SYRTHES GUI is embedded in the Salome desktop.
        """
        from salome.syrthes.SalomeHandlerGeneral import BoundaryGroup
        #import smesh

        # define if necessary
        if self.list_Volumic_Table == None :
            self.list_Volumic_Table = [self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table,
                                      self.syrthesIHMCollector.Physical_prop_2D_form.Iso_cond_2D_table,
                                      self.syrthesIHMCollector.Physical_prop_2D_form.Ort_cond_2D_table,
                                      self.syrthesIHMCollector.Physical_prop_2D_form.Ani_cond_2D_table,
                                      self.syrthesIHMCollector.Physical_prop_3D_form.Iso_cond_3D_table,
                                      self.syrthesIHMCollector.Physical_prop_3D_form.Ort_cond_3D_table,
                                      self.syrthesIHMCollector.Physical_prop_3D_form.Ani_cond_3D_table,
                                      self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table,
                                      self.syrthesIHMCollector.Initial_conditions_hum_TPv_form.Vch_Ic_TPv_table,
                                      self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form.Vch_Ic_TPvPt_table,
                                      self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_iso_2D_table,
                                      self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_aniso_2D_table,
                                      self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_iso_3D_table,
                                      self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_aniso_3D_table,
                                      self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form.Vch_St_TPv_table,
                                      self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form.Vch_St_TPvPt_table,
                                      self.syrthesIHMCollector.Output_2D_form.Op_Vb_2D_table,
                                      self.syrthesIHMCollector.Output_3D_form.Op_Vb_3D_table,
                                      self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Vc_table,
                                      # fluid1d
                                      self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table,
                                      self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table,
                                      self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table,
                                      self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table,
                                      self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table,
                                      self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table,
                                      self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl,
                                      self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl,
                                      self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP,
                                      self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table,
                                      self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table,
                                      self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table,
                                      self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table

                                      ]

        if self.list_2faces_Table == None :
            self.list_2faces_Table = [self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table,
                                      self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form.Cont_res_hum_TPv_table,
                                      self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form.Cont_res_hum_TPvPt_table,
                                      self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_rot_table,
                                      self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_tra_table,
                                      self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_rot_table,
                                      self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_tra_table
                                      ]

        # definitions of uncompatible types
        list_not_compatible_VOLUME_3D = ["FACE", "EDGE", "NODE"]
        list_not_compatible_VOLUME_2D = ["VOLUME", "EDGE", "NODE"]
        list_not_compatible_SURF_3D = ["VOLUME", "EDGE", "NODE"]
        list_not_compatible_SURF_2D = ["VOLUME", "FACE", "NODE"]

        table=QtWidgets.QApplication.focusWidget()

        # list of uncompatible types
        if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex() == 0 :
            if table in self.list_Volumic_Table :
                list_not_compatible = list_not_compatible_VOLUME_3D
            else :
                list_not_compatible = list_not_compatible_SURF_3D
        else :
            if table in self.list_Volumic_Table :
                list_not_compatible = list_not_compatible_VOLUME_2D
            else :
                list_not_compatible = list_not_compatible_SURF_2D

        cmt,ref,typeGroup = BoundaryGroup()

        # control
        for typ in list_not_compatible :
            if typeGroup.count(typ) > 0 : # an uncompatible type "typ" exist in the string "typeGroup"
                print("Incompatible type(s) of reference(s)")
                return

        # special case : Boundary condition for radiation
        if table == self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne or table == self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne :
            # comparison with existing contents to find really new items
            itemRef = table
            if table == self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne :
                itemCmt = self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_te
            else :
                itemCmt = self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_te
            refFinal = ""
            cmtFinal = ""
            refsReallyNew, cmtsReallyNew = self.extractNewItems(ref, cmt, itemRef.text())
            if refsReallyNew != "" :
                refFinal += itemRef.text() + ' ' + refsReallyNew
                cmtFinal += itemCmt.toPlainText() + ' ' + cmtsReallyNew
            else :
                refFinal += itemRef.text()
                cmtFinal += itemCmt.toPlainText()
            # set new text for Ref lineEdit and for Comment textEdit
            itemRef.setText(str(refFinal).strip())
            itemCmt.setPlainText(str(cmtFinal).strip())
            return

        # special case : Boundary condition for 1D fluid 3D
        if table == self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1df_lne :
            # comparison with existing contents to find really new items
            itemRef = table
            itemCmt = self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1dfUc_lne
            refFinal = ""
            cmtFinal = ""
            refsReallyNew, cmtsReallyNew = self.extractNewItems(ref, cmt, itemRef.text())
            if refsReallyNew != "" :
                refFinal += itemRef.text() + ' ' + refsReallyNew
                cmtFinal += itemCmt.toPlainText() + ' ' + cmtsReallyNew
            else :
                refFinal += itemRef.text()
                cmtFinal += itemCmt.toPlainText()
            # set new text for Ref lineEdit and for Comment textEdit
            itemRef.setText(str(refFinal).strip())
            itemCmt.setPlainText(str(cmtFinal).strip())
            return



        listOfSelectedItems = table.selectedItems()

        # determine susceptible cells
        columnRef = table.columnCount()-2
        columnComment = columnRef + 1
        selectedRow = listOfSelectedItems[0].row()
        itemRef = table.item(selectedRow, columnRef)
        itemCmt = table.item(selectedRow, columnComment)

        # control
        if itemRef == None or itemCmt == None :
            print("table not initialized")
            return

        if table in self.list_2faces_Table : # tables of type 2 ref-columns
            if listOfSelectedItems[0].column() != columnRef :
                if listOfSelectedItems[0].column() == columnRef-1 :
                    itemRef = table.item(selectedRow, columnRef-1) # redefine the ref column
                else :
                    print("For this type of table, one of the two reference columns must be chosen to be pasted.")
                    QtWidgets.QMessageBox.information(self, 'Information', "For this type of table, one of the two reference columns must be chosen to be pasted from SALOME.")
                    return

        refFinal = ""
        cmtFinal = ""
        # comparison with existing contents to find really new items
        refsReallyNew, cmtsReallyNew = self.extractNewItems(ref, cmt, itemRef.text())
        if refsReallyNew != "" :
            refFinal += itemRef.text() + ' ' + refsReallyNew
            cmtFinal += itemCmt.text() + ' ' + cmtsReallyNew
        else :
            refFinal += itemRef.text()
            cmtFinal += itemCmt.text()

        # set new text for Ref cell and for Comment cell
        itemRef.setText(str(refFinal).strip())
        itemCmt.setText(str(cmtFinal).strip())

    def extractNewItems(self, ref, cmt, refsOld):
        refsReallyNew = ""
        cmtsReallyNew = ""
        ref_split = str(ref).split()
        cmt_split = str(cmt).split()
        refsOld_split = str(refsOld).split()
        for i in range(len(ref_split)) :
            # a ref X exists in refsOld already only if X == one of refsOld.split()
            if ref_split[i] not in refsOld_split :
                refsReallyNew += ref_split[i] + ' '
                cmtsReallyNew += cmt_split[i] + ' '
        return refsReallyNew.strip(), cmtsReallyNew.strip()


    def Dimension_choice (self, a): # fonction de rappel du choix de la dimension du problème
        if self.Dimension != self.Dim_comb_dic[a] :
            reply = QtWidgets.QMessageBox.question(self, "Warning", "Data may be lost when changing the dimension. Would you like to continue ?")
            if reply == QtWidgets.QMessageBox.Yes :
                self.Dimension=self.Dim_comb_dic[a]
                if a == 0 : # 3D
                    self.Solar_aspect.setDisabled(False)
                    # Rendre le 1D disponible seulement en 3D
                    self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.setDisabled(False)
                else :
                    self.Solar_aspect.setDisabled(True)
                    # Rendre le 1D disponible seulement en 3D
                    self.fluid1d.setHidden(True)
                    self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.setDisabled(True)
                    self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.setChecked(False)
                self.copyPropPhy()
            else: # remettre l'ancien index du combobox
                for i in self.Dim_comb_dic :
                    if self.Dim_comb_dic[i] == self.Dimension :
                        self.syrthesIHMCollector.Home_form.Dim_Comb.setCurrentIndex(i)
                        break

    def Table_prog (self, a): # fonction de rappel du coloriage en noir des cases lors du choix "Program"
        Black = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        White = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        cmb=QtWidgets.QApplication.focusWidget()
        if cmb == None :
            return
        widget=cmb.parentWidget()
        table=widget.parentWidget()
        i=0
        while i<table.rowCount():
            # determine the 1st and the last column to be darken
            j = 2 # choisi par défaut
            dic_1stcol = {1:2,5:2, # checkbox avec combobox
                          2:1, # only checkbox
                          3:3,4:3 # checkbox + 2 combobox
                          }
            j = dic_1stcol[self.dic_Table_type[table]]
            lastCol = table.columnCount()-3 # avant "la" colonne réf
            if table == self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table : lastCol = table.columnCount()-4 # checkbox + 2 combobox
            if table == self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form.Cont_res_hum_TPv_table : lastCol = table.columnCount()-4 # checkbox + 2 combobox
            if table == self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form.Cont_res_hum_TPvPt_table : lastCol = table.columnCount()-4 # checkbox + 2 combobox

            #Correction pour les cas fluid1d
            if table == self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table :
                lastCol = table.columnCount()-2
                j= 5
            if table == self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl :
                lastCol = table.columnCount()-2
                j = 5
            if table == self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP :
                lastCol = table.columnCount()-2
                j = 5
            if table==self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl :
                j=2
                lastCol = table.columnCount()-2

            #Correction pour les cas fluid0d
            if table == self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table :
                j=3
                lastCol = table.columnCount()-2
            if table == self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table :
                j=3
                lastCol = table.columnCount()-2

            if table.cellWidget(i,1).currentIndex()==2 and table!=self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl or (table.cellWidget(i,1).currentIndex()==1 and table==self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table): # de type "program" et pas thermal loop pour 1D ou H_COLBURN
                while j <= lastCol: # loop through the columns
                    if table.item(i,j) == None :
                        item=QtWidgets.QTableWidgetItem('')
                        table.setItem(i,j,item)
                    table.item(i,j).setBackground(Black)
                    j=j+1
                i=i+1
            else :
                while j <= lastCol:
                    if table.item(i,j) == None :
                        item=QtWidgets.QTableWidgetItem('')
                        table.setItem(i,j,item)
                    table.item(i,j).setBackground(White)
                    j=j+1
                i=i+1
        self.forcedCheck_table(cmb.currentIndex())

    def Table_prog2(self, table, cmb, i):
    # called by OpenFile.py to color cells in black when combobox=PROGRAM
    # this function is created because QApplication.focusWidget() doesn't
    # work in certain cases (use option -d in Shell for example)
    # table : table in use
    # cmb : combobox in use
    # i : table row in use

        # determine the 1st and the last column to be darken
        j = 2 # choisi par défaut
        dic_1stcol = {1:2,5:2, # checkbox avec combobox
                      2:1, # only checkbox
                      3:3,4:3 # checkbox + 2 combobox
                      }
        j = dic_1stcol[self.dic_Table_type[table]]
        lastCol = table.columnCount()-3 # avant "la" colonne réf
        if table == self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table : lastCol = table.columnCount()-4 # checkbox + 2 combobox
        if table == self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form.Cont_res_hum_TPv_table : lastCol = table.columnCount()-4 # checkbox + 2 combobox
        if table == self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form.Cont_res_hum_TPvPt_table : lastCol = table.columnCount()-4 # checkbox + 2 combobox

        #Correction pour les cas fluid1d
        if table == self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table :
            lastCol = table.columnCount()-2
            j=5
        if table == self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl :
            lastCol = table.columnCount()-2
            j=5
        if table == self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP :
            lastCol = table.columnCount()-2
            j=5

        #Correction pour les cas fluid0d
        if table == self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table :
            j=3
            lastCol = table.columnCount()-2
        if table == self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table :
            j=3
            lastCol = table.columnCount()-2

        while j<=lastCol: # loop through the columns
            if table.item(i,j) == None :
                item=QtWidgets.QTableWidgetItem('')
                table.setItem(i,j,item)
            table.item(i,j).setBackground(QtGui.QBrush(QtGui.QColor(0, 0, 0)))
            j=j+1


    def Check_fields(self, parent=None):
        if self.case.fullPath == "" :
            return
        if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
            CBB=self.syrthesIHMCollector.Output_3D_form.F_cb_3D_Op
            dim = "3D"
        else :
            CBB=self.syrthesIHMCollector.Output_2D_form.F_cb_2D_Op
            dim = "2D"
        if CBB.isChecked():
            reply = QtWidgets.QMessageBox.question(self, 'Message',
                                         "Do you want to disable "+ dim +" fields ?",
                                         QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No | QtWidgets.QMessageBox.Cancel)
            if reply == QtWidgets.QMessageBox.Yes :
                CBB.setChecked(True)
                pass
            elif reply == QtWidgets.QMessageBox.Cancel or reply == QtWidgets.QMessageBox.No:
                CBB.setChecked(False)
        pass

    def Check_solid_ts(self, parent=None):
        CF = self.syrthesIHMCollector.Control_form
        CF1D = self.syrthesIHMCollector.Control_fluid1d_form
        message = ""
        if CF1D.Cb_solid_Ts_fluid1d.isChecked():
            if CF.comb_time_st.currentIndex()==0:
                CF1D.Le_const_Ts_fluid1d.setText(CF.Le_const_Ts.text())
                CF1D.Le_const_Ts_fluid1d.setEnabled(False)
                pass
            else:
                CF1D.Cb_solid_Ts_fluid1d.setChecked(False)
                QtWidgets.QMessageBox.information(self, "Warning", "The solid time step is not constant.")
                pass
            pass
        else:
            CF1D.Le_const_Ts_fluid1d.setEnabled(True)
            pass
        pass

    def Check_type(self, chaine): #fonction de rappel permettant la vérification des données entrées dans les champs d'éditions
        lne=QtWidgets.QApplication.focusWidget()
        if lne==self.syrthesIHMCollector.Output_3D_form.Cb_3D_Op:
            lne=self.syrthesIHMCollector.Output_3D_form.Le_3D_Op
            chaine = self.syrthesIHMCollector.Output_3D_form.Le_3D_Op.text()
        elif lne==self.syrthesIHMCollector.Output_2D_form.Cb_2D_Op:
            lne=self.syrthesIHMCollector.Output_2D_form.Le_2D_Op
            chaine = self.syrthesIHMCollector.Output_2D_form.Le_2D_Op.text()
        if lne==self.syrthesIHMCollector.Output_3D_form.Cb2_3D_Op:
            lne=self.syrthesIHMCollector.Output_3D_form.Le2_3D_Op
            chaine = self.syrthesIHMCollector.Output_3D_form.Le2_3D_Op.text()
        elif lne==self.syrthesIHMCollector.Output_2D_form.Cb2_2D_Op:
            lne=self.syrthesIHMCollector.Output_2D_form.Le2_2D_Op
            chaine = self.syrthesIHMCollector.Output_2D_form.Le2_2D_Op.text()

        if lne == None:
            return
        if lne not in self.lne_dic:
#            print "lne_dic doesn't has this key :", lne
            return

        if self.lne_dic[lne]=='+=int':
            Valint=QtGui.QIntValidator(None)
            Valint.setBottom(0)
        elif self.lne_dic[lne]=='+int':
            Valint=QtGui.QIntValidator(None)
            Valint.setBottom(1)
        elif self.lne_dic[lne]=='+float':
            Valint=QtGui.QDoubleValidator(None)
            Valint.setBottom(1.e-99999)
        elif self.lne_dic[lne]=='+intstr':
            i=0
            strlne=str(chaine).split()
            flag=True
            while i<len(strlne):
                if not(strlne[i].isdigit()):
                    flag=False
                elif int(strlne[i])<=0:
                    if int(strlne[i])==-1 and len(strlne)==1:
                        flag=True
                    else:
                        flag=False
                #print i
                i=i+1

            if flag==False and lne.text()!='':
                palette = QtGui.QPalette()
                brush = QtGui.QBrush(QtGui.QColor(255, 170, 127))
                brush.setStyle(QtCore.Qt.SolidPattern)
                palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
                lne.setPalette(palette)
            else:
                palette = QtGui.QPalette()
                brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
                brush.setStyle(QtCore.Qt.SolidPattern)
                palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
                lne.setPalette(palette)
        elif self.lne_dic[lne]=='spec':
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                CBB=self.syrthesIHMCollector.Output_3D_form.Cb_3D_Op
                but = self.syrthesIHMCollector.Output_3D_form.Ai_but_3D_Op
                pass
            else :
                CBB=self.syrthesIHMCollector.Output_2D_form.Cb_2D_Op
                but = self.syrthesIHMCollector.Output_2D_form.Ai_but_2D_Op
                pass
            if CBB.currentIndex()==0:
                Valint=QtGui.QIntValidator(None)
                Valint.setBottom(1)
                but.setEnabled(False)
            elif CBB.currentIndex()==1:
                Valint=QtGui.QDoubleValidator(None)
                Valint.setBottom(1.e-99999)
                but.setEnabled(False)
            else:
                # TODO popup
                i=0
                strlne=str(chaine).split()
                flag=True
                but.setEnabled(True)
                while i<len(strlne):
                    if not self.isFloat(strlne[i]) :
                        flag=False
                        break
                    elif float(strlne[i])<0 :
                        flag=False
                        break
                    i=i+1

                if flag==False and lne.text()!='':
                    palette = QtGui.QPalette()
                    brush = QtGui.QBrush(QtGui.QColor(255, 170, 127))
                    brush.setStyle(QtCore.Qt.SolidPattern)
                    palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
                    lne.setPalette(palette)
                else:
                    palette = QtGui.QPalette()
                    brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
                    brush.setStyle(QtCore.Qt.SolidPattern)
                    palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
                    lne.setPalette(palette)
        elif self.lne_dic[lne]=='spec2':
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                CBB=self.syrthesIHMCollector.Output_3D_form.Cb2_3D_Op
            else :
                CBB=self.syrthesIHMCollector.Output_2D_form.Cb2_2D_Op
            if CBB.currentIndex()==0:
                Valint=QtGui.QIntValidator(None)
                Valint.setBottom(1)
            elif CBB.currentIndex()==1:
                Valint=QtGui.QDoubleValidator(None)
                Valint.setBottom(1.e-99999)
            else:
                # TODO popup
                i=0
                strlne=str(chaine).split()
                flag=True
                while i<len(strlne):
                    if not self.isFloat(strlne[i]) :
                        flag=False
                        break
                    elif float(strlne[i])<0 :
                        flag=False
                        break
                    i=i+1

                if flag==False and lne.text()!='':
                    palette = QtGui.QPalette()
                    brush = QtGui.QBrush(QtGui.QColor(255, 170, 127))
                    brush.setStyle(QtCore.Qt.SolidPattern)
                    palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
                    lne.setPalette(palette)
                else:
                    palette = QtGui.QPalette()
                    brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
                    brush.setStyle(QtCore.Qt.SolidPattern)
                    palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
                    lne.setPalette(palette)
        else:
            pass

        if self.lne_dic[lne]!='+intstr' and self.lne_dic[lne]!='spec' and self.lne_dic[lne]!='spec2':
            lne.setValidator(Valint)
            valid=lne.validator()
            res=valid.validate(lne.text(), 0)
            #print res[0]
            if res[0]!=2 and lne.text()!='':
                palette = QtGui.QPalette()
                brush = QtGui.QBrush(QtGui.QColor(255, 170, 127))
                brush.setStyle(QtCore.Qt.SolidPattern)
                palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
                lne.setPalette(palette)
                #lne.setStatusTip(QtGui.QApplication.translate("MainWindow", "Please enter an int>0", None, QtGui.QApplication.UnicodeUTF8))
            else:
                palette = QtGui.QPalette()
                brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
                brush.setStyle(QtCore.Qt.SolidPattern)
                palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
                lne.setPalette(palette)
                #lne.setStatusTip(QtGui.QApplication.translate("MainWindow", " ", None, QtGui.QApplication.UnicodeUTF8))
            lne.setValidator(None)

        elif (self.lne_dic[lne]=='spec' or self.lne_dic[lne]=='spec2') and CBB.currentIndex()!=2:
            lne.setValidator(Valint)
            valid=lne.validator()
            res=valid.validate(lne.text(), 0)
            #print res[0]
            if res[0]!=2 and lne.text()!='':
                palette = QtGui.QPalette()
                brush = QtGui.QBrush(QtGui.QColor(255, 170, 127))
                brush.setStyle(QtCore.Qt.SolidPattern)
                palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
                lne.setPalette(palette)
                #lne.setStatusTip(QtGui.QApplication.translate("MainWindow", "Please enter an int>0", None, QtGui.QApplication.UnicodeUTF8))
            else:
                palette = QtGui.QPalette()
                brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
                brush.setStyle(QtCore.Qt.SolidPattern)
                palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
                lne.setPalette(palette)
                #lne.setStatusTip(QtGui.QApplication.translate("MainWindow", " ", None, QtGui.QApplication.UnicodeUTF8))
            lne.setValidator(None)
            pass

        if lne == self.syrthesIHMCollector.Control_form.Le_const_Ts:
            CF = self.syrthesIHMCollector.Control_form
            CF1D = self.syrthesIHMCollector.Control_fluid1d_form
            if CF1D.Cb_solid_Ts_fluid1d.isChecked():
                if CF.comb_time_st.currentIndex()==0:
                    CF1D.Le_const_Ts_fluid1d.setText(CF.Le_const_Ts.text())
                    pass
                pass
            pass
        pass


    def RadHid (self, parent=None): #fonction de rappel permettant de rendre disponible les composants graphiques du maillage pour le rayonnement
        self.Radiation.setHidden(not self.syrthesIHMCollector.Home_form.Ho_Tr_ch.isChecked())
        self.syrthesIHMCollector.Filename_form.Fn_Rm_lb.setEnabled(self.syrthesIHMCollector.Home_form.Ho_Tr_ch.isChecked())
        self.syrthesIHMCollector.Filename_form.Fn_Rm_lne.setEnabled(self.syrthesIHMCollector.Home_form.Ho_Tr_ch.isChecked())
        self.syrthesIHMCollector.Filename_form.Fn_Rm_but.setEnabled(self.syrthesIHMCollector.Home_form.Ho_Tr_ch.isChecked())
        self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_ry.setEnabled(self.syrthesIHMCollector.Home_form.Ho_Tr_ch.isChecked())
        self.syrthesIHMCollector.Running_options_form.Ro_Np_lab_ry.setEnabled(self.syrthesIHMCollector.Home_form.Ho_Tr_ch.isChecked())
        if self.case.fullPath == "" :
            return
        else:
            self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_but.setEnabled(self.syrthesIHMCollector.Home_form.Ho_Tr_ch.isChecked())

    def HummodHid (self, parent=None): #fonction de rappel permettant de rendre disponible les composants graphiques du maillage pour l'humidité
        self.Humidity.setHidden(not self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked())
        self.Conduction.setHidden(self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked())
        self.syrthesIHMCollector.Control_form.Vt_Vap_gb.setVisible(self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked())
        self.syrthesIHMCollector.Control_form.Pv_Pt_var_gb.setVisible(self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked())
        if self.syrthesIHMCollector.Home_form.Hm_cmb.currentIndex()==0:
            self.syrthesIHMCollector.Control_form.Vap_lb4.setEnabled(False)
            self.syrthesIHMCollector.Control_form.Ap_Sp_le.setEnabled(False)
            self.syrthesIHMCollector.Control_form.Ap_Mn_le.setEnabled(False)
            self.syrthesIHMCollector.Control_form.ptLabel.setEnabled(False)
            self.syrthesIHMCollector.Control_form.Le_auto_Mpt.setEnabled(False)
        else :
            self.syrthesIHMCollector.Control_form.Vap_lb4.setEnabled(True)
            self.syrthesIHMCollector.Control_form.Ap_Sp_le.setEnabled(True)
            self.syrthesIHMCollector.Control_form.Ap_Mn_le.setEnabled(True)
            self.syrthesIHMCollector.Control_form.ptLabel.setEnabled(True)
            self.syrthesIHMCollector.Control_form.Le_auto_Mpt.setEnabled(True)
        if self.case.fullPath == "" :
            return
        else:
            self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_but.setEnabled(self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked())
            self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_but.setEnabled(not self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked()) #désactiver le bouton User C function conduction

    def ChtHid (self, parent=None): #fonction de rappel permettant d'afficher la branche Conjugate heat transfer dans l'arborescence
        self.Conjugate_heat_transfer.setHidden(not self.syrthesIHMCollector.Home_form.Ho_Ch_ch.isChecked())

    def fluid1dHid (self, parent=None): #fonction de rappel permettant d'afficher la branche fluid1d'
        self.fluid1d.setHidden(not self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.isChecked())
        self.syrthesIHMCollector.Filename_form.Fn_fluid1d_lb.setEnabled(self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.isChecked())
        self.syrthesIHMCollector.Filename_form.Fn_fluid1d_lne.setEnabled(self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.isChecked())
        self.syrthesIHMCollector.Filename_form.Fn_fluid1d_but.setEnabled(self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.isChecked())
        self.syrthesIHMCollector.Running_options_form.Ro_Pre_cb.setEnabled(self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.isChecked())
        self.syrthesIHMCollector.Running_options_form.Ro_Pre_lb.setEnabled(self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.isChecked())
        if self.case.fullPath == "" :
            return
        else:
            self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_but.setEnabled(self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.isChecked())

    def fluid0dHid (self, parent=None): #fonction de rappel permettant d'afficher la branche fluid0d'
        self.fluid0d.setHidden(not self.syrthesIHMCollector.Home_form.Ho_fluid0d_ch.isChecked())
        self.syrthesIHMCollector.Running_options_form.Ro_Pre_cb.setEnabled(self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.isChecked())
        self.syrthesIHMCollector.Running_options_form.Ro_Pre_lb.setEnabled(self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.isChecked())
        pass

    def Langselect(self, parent=None): #fonction de rappel permettant de changer la langue de l'interface
    # obsolète
        pass
#        if self.actionFarn_ais.isChecked():
#            self.Lang='fr_FR'
#        else:
#            self.Lang='en_US'
#        self.internationalisation()

    def SyrthesFileselection(self, parent=None): # fonction de rappel du dialogue d'ouverture de fichier Syrthes
        format= "*.syr"
        self.SyrthesFileselection = QtWidgets.QFileDialog.getSaveFileName(self, self.tr("Select File"),
                                                                          self.lastDir + self.tr(os.sep+"untitled.")+format,
                                                                          self.tr("*.syr;;All Files (*)"))[0]

    def addInstants(self, parent=None):
        if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
            lineEdit = self.syrthesIHMCollector.Output_3D_form.Le_3D_Op
        else:
            lineEdit = self.syrthesIHMCollector.Output_2D_form.Le_2D_Op

        self.Main3 =Output_Times_FormHandler(self,self.case.dirPath,lineEdit)
        self.Main3.show()
        pass

    def User_C_function_Compile(self, parent=None):
        if os.path.isdir(self.case.dirPath):
            os.chdir(self.case.dirPath)
            pass
        if os.path.isfile(self.case.dirPath + os.sep + "Makefile"):
            # changement temporaire du langage car probleme d'interpretation des caracteres speciaux apostrophes
            lang_var_value = os.getenv("LANG")
            os.unsetenv("LANG")
            cmd = "make -i"
            process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = process.communicate()
            out = out.decode()
            err = err.decode()
            os.putenv("LANG", str(lang_var_value))

            if err == "":
                QtWidgets.QMessageBox.information(self, "Message", "Compilation OK.")
                pass
            else:
                print(str(err))
                box = CustomMessageBox(QtWidgets.QMessageBox.Critical, "Error", "Compilation Error. Click Show Details to see the full message. ",QtWidgets.QMessageBox.Ok)
                box.setDetailedText(str(err))
                box.exec_()
                pass
            pass
        pass

    def User_C_function_Edit(self, parent=None):
        but = QtWidgets.QApplication.focusWidget()

        if but==self.syrthesIHMCollector.User_C_function_form.Cfunc_other_but : # other user C functions file
            formatlist=self.tr(' *.c ;; All Files (*)')
            userCfunc = QtWidgets.QFileDialog.getOpenFileName(self, self.tr("Select File"), self.case.dirPath+self.tr(os.sep),formatlist)[0]
            if str(userCfunc) == "" : # user clicks on "Cancel"
                return
            userCfunc = str(self.convertOSsep(userCfunc))
            self.lastDir = str(userCfunc).rsplit(os.sep, 1)[0] #update lastDir
            self.syrthesIHMCollector.User_C_function_form.Cfunc_other_lne.setText(str(userCfunc))
            try:
                subprocess.Popen([self.textEditor,str(userCfunc)])
            except Exception as e:
                print(e)
                return
            self.refreshUserC()
            return

        dic_User_C_origin = {self.syrthesIHMCollector.User_C_function_form.Cfunc_but : self.case.dirPath+os.sep+"usr_examples"+os.sep+"user.c",
                      self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_but : self.case.dirPath+os.sep+"usr_examples"+os.sep+"user_cond.c",
                      self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_but : self.case.dirPath+os.sep+"usr_examples"+os.sep+"user_ray.c",
                      self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_but : self.case.dirPath+os.sep+"usr_examples"+os.sep+"user_hmt.c",
                      self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_but : self.case.dirPath+os.sep+"usr_examples"+os.sep+"user_1Dfluid.c",
                      self.syrthesIHMCollector.User_C_function_form.btnOpenOther : self.syrthesIHMCollector.User_C_function_form.Cfunc_other_lne.text()}

        dic_User_C = {self.syrthesIHMCollector.User_C_function_form.Cfunc_but : self.case.dirPath+os.sep+"user.c",
                      self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_but : self.case.dirPath+os.sep+"user_cond.c",
                      self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_but : self.case.dirPath+os.sep+"user_ray.c",
                      self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_but : self.case.dirPath+os.sep+"user_hmt.c",
                      self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_but : self.case.dirPath+os.sep+"user_1Dfluid.c",
                      self.syrthesIHMCollector.User_C_function_form.btnOpenOther : self.syrthesIHMCollector.User_C_function_form.Cfunc_other_lne.text()}

        try:
            if not os.access(dic_User_C[but], os.F_OK):
                print("copied from usr_examples")
                shutil.copy2(dic_User_C_origin[but], dic_User_C[but])
            subprocess.Popen([self.textEditor,str(dic_User_C[but])])
        except OSError:
            QtWidgets.QMessageBox.information(self, "Error message", "Editor : \"" +self.textEditor + "\" not found")
        except IOError:
            QtWidgets.QMessageBox.information(self, "Error message", "File : \"" +dic_User_C_origin[but] + "\" not found")

        self.refreshUserC()
        pass

    def meshFileselection(self, parent=None): # fonction de rappel du dialogue d'ouverture du fichier de maillage
        but=QtWidgets.QApplication.focusWidget()
        self.lne=self.dic_Open_file[but]

        # demander le meshFile
        suggestedPath = self.lastDir + self.tr(os.sep+"untitled.syr")

        if syrthesIHMContext.isEmbedded():
            formatlist=self.tr("Compatible (*.syr *.des *.unv *.neu *.msh *.med);;*.syr;;All Files (*)")
        else:
            formatlist=self.tr("Compatible (*.syr *.des *.unv *.neu *.msh *.med);;*.syr;;All Files (*)")

        res = QtWidgets.QFileDialog.getOpenFileName(self, self.tr("Select File"), suggestedPath, formatlist)[0]
        if str(res) == "" : # user clicks on "Cancel"
            return

        self.lastDir = str(res).rsplit(os.sep, 1)[0] #update lastDir

        # récupérer le chemin relatif de meshFile avec "/" comme séparateur

        self.meshFile = str(self.convertOSsep(res))
        meshFileFull = self.meshFile
        # Attention : mise en garde les chemins de type /local00/home chez EDF
        if self.case.dirPath != '' :
            if os.path.abspath(self.meshFile).count(os.path.abspath(self.case.dirPath)+os.sep) :
                # mettre le chemin en relative si le fichier maillage se trouve
                # dans le dossier du cas.
                self.meshFile = self.meshFile.replace(os.path.abspath(self.case.dirPath), "")
                self.meshFile = self.meshFile.replace(self.case.dirPath, "")
                self.meshFile = self.meshFile.replace(os.sep, "", 1)
                pass
        self.meshFile = self.meshFile.replace(os.sep, "/") # necessaire même sous Windows
        meshFileFull = meshFileFull.replace(os.sep, "/") # necessaire même sous Windows

        # chercher l'extension du fichier maillage
        text=self.meshFile
        text = self.convertOSsep(text)
        text=text.split(os.sep)
        text=str(text[len(text)-1])
        text=text.rpartition('.')

        if text[2] != 'syr' : # convertir le maillage en format .syr
            # chercher la variable d'environnement SYRTHES4_HOME
            if os.name=='nt':
                SYRTHES4_HOME = os.popen('echo %SYRTHES4_HOME%').read()
            else:
                SYRTHES4_HOME = os.popen('echo $SYRTHES4_HOME').read()
            SYRTHES4_HOME = (SYRTHES4_HOME.split('\n'))[0] # éliminer le \n à la fin de SYRTHES4_HOME
            if SYRTHES4_HOME == "" :
                QtWidgets.QMessageBox.information(self, 'Message', "SYRTHES4_HOME not found. Syrthes will not run.")
                return

            # constituer la commande
            com = SYRTHES4_HOME + os.sep + "bin" + os.sep + "convert2syrthes4"
            com = com + ' -m ' + '\"' + meshFileFull + '\"'

            if text[2]=='des' or text[2]=='neu' or text[2]=='med':
                pass
            elif text[2]=='msh' or text[2]=='unv' :
                if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0:
                    dim='3'
                else:
                    dim='2'
                com = com + ' -dim ' + dim
            print("commande ---> "+com)
            com=str(com)

            # exécuter la commande
            proc = subprocess.Popen(com, shell=True)
            proc.wait()

            # vérifier le résultat
            self.meshFile = self.meshFile.replace("."+text[2], '.syr') # plus sûr avec le "." avant l'extension
            meshFileFull = meshFileFull.replace("."+text[2], '.syr') # plus sûr avec le "." avant l'extension
            print(self.meshFile)
            if os.access(meshFileFull, os.F_OK) :
                msg1 = "Format conversion from \"%s\" to \"syr\" finished. \n" % (text[2])
                if syrthesIHMContext.isEmbedded():
                    msg2 = "To display the selected mesh in the Object Browser, please save the current SYRTHES case."
                    QtWidgets.QMessageBox.information(self, 'Message', msg1+msg2)
                    pass
                else:
                    QtWidgets.QMessageBox.information(self, 'Message', msg1)
            else:
                QtWidgets.QMessageBox.information(self, 'Message', "Format conversion from \"%s\" to \"syr\" not successful" % (text[2]))
                self.meshFile = ''

        self.lne.setText(str(self.meshFile))

    def Fileselection(self, parent=None): # fonction de rappel du dialogue d'ouverture de fichier de maillage
        # TODO KAN : Ouverture d'un fichier maillage, vérification d'un fichier deja partitionne
        # Si le fichier est deja partititionne, il se trouvera dans le ${rep_cas}/PART
        # solid.syr -> solid_part0000i.syr
        # if self.syrthesIHMCollector.Running_options_form.Ro_Dp_cb.currentIndex()==2:

        but=QtWidgets.QApplication.focusWidget()
        self.lne=self.dic_Open_file[but]
        formatlist=self.tr(' All Files (*)')
        if but == self.syrthesIHMCollector.Filename_form.Fn_Rnp_but : # Results names prefix
            res = QtWidgets.QFileDialog.getSaveFileName(self, self.tr("Select File"),
                                                        self.lastDir + self.tr(os.sep+"untitled"),formatlist)[0]
                                #.arg(format))
        elif but == self.syrthesIHMCollector.Filename_form.Fn_Rs_but:
            formatlist=self.tr(' *.res ;; All Files (*)')
# isa            res = QtWidgets.QFileDialog.getSaveFileName(self, self.tr("Select File"), self.lastDir + self.tr(os.sep+"untitled.res"),formatlist)
            res = QtWidgets.QFileDialog.getOpenFileName(self, self.tr("Select File"),
                                                        self.lastDir + self.tr(os.sep+"untitled.res"),formatlist)[0]
        else:
            res = QtWidgets.QFileDialog.getOpenFileName(self, self.tr("Select File"),
                                                        self.lastDir + self.tr(os.sep+"untitled"),formatlist)[0]
                                #.arg(format))
        if str(res) == "" : # user clicks on "Cancel"
            return

        self.Fileselection = str(self.convertOSsep(res))
        FileselectionFull = self.Fileselection

        # Attention : mise en garde les chemins de type /local00/home chez EDF
        self.lastDir = str(self.Fileselection).rsplit(os.sep, 1)[0] #update lastDir
        if self.case.dirPath != '' :
            if os.path.abspath(self.Fileselection).count(os.path.abspath(self.case.dirPath)+os.sep) :
                # mettre le chemin en relative si le fichier maillage se trouve
                # dans le dossier du cas.
                self.Fileselection = self.Fileselection.replace(os.path.abspath(self.case.dirPath), "")
                self.Fileselection = self.Fileselection.replace(self.case.dirPath, "")
                self.Fileselection = self.Fileselection.replace(os.sep, "", 1)

        self.Fileselection = self.Fileselection.replace(os.sep, "/") # necessaire même sous Windows
        FileselectionFull = FileselectionFull.replace(os.sep, "/") # necessaire même sous Windows

        if self.lne==self.syrthesIHMCollector.Filename_form.Fn_Rnp_lne:# or self.lne==self.syrthesIHMCollector.Filename_form.Fn_Rs_lne:
            text=self.Fileselection
            text=text.rpartition('.')
            if text[0] == '' : # si self.Fileselection = "abcd" --> text = ['','','abcd']
                text = text[2]
            else : # si self.Fileselection = "abc.d" --> text = ['abc','.','d']
                text = text[0]

            self.lne.setText(text)
        else:
            self.lne.setText(self.Fileselection)

    def setPartitionning(self, index):
        QtWidgets.QMessageBox.information(self, "Message", "Please check the conduction file name and path.")
        pass

    def SavingFile(self, parent=None): # fonction de rappel de sauvegarde de fichier
        # check if mesh files are given : MessageBox an warning
        #reply = QMessageBox.Yes # initial value of reply (response of user to following questions :
        #if self.syrthesIHMCollector.Filename_form.Fn_Cd_lne.text()=='': # check if Conduction mesh file is not given
        #    reply = QMessageBox.question(self, 'Message', "Do you want to save data without Conduction mesh file ?", QMessageBox.Yes, QMessageBox.No)

        # check if user respond Yes to last question, then if Thermal radiation is check, then if Radiation mesh file is not given
        #if (reply == QMessageBox.Yes) and self.syrthesIHMCollector.Home_form.Ho_Tr_ch.isChecked() and (self.syrthesIHMCollector.Filename_form.Fn_Rm_lne.text()==''):
        #    reply = QMessageBox.question(self, 'Message', "Do you want to save data without Radiation mesh file ?", QMessageBox.Yes, QMessageBox.No)

        #if reply != QMessageBox.Yes: # on ne va rien sauvegarder
        #    print "data not saved"
        #    QMessageBox.information(self, 'Message', "Data have not been saved", QMessageBox.Ok)
        #    self.initSYRTHESFont()
        #    return

        if self.case.fullPath == "" or self.case.name == "untitled.syd":
            # déléguer la sauvegarde à Saving_as s'il s'agit de la 1ere sauvegarde
            self.Saving_as()
            return
        else :
            # compare actual data with previous saved data
            # mod GA pour depannage
            #identik = self.SavingCompare()
            identik = False
            # fin GA
            if identik == False : # if different
                self.savfil=open(self.case.fullPath, "w")
                self.SavingCommonPart()
                if syrthesIHMContext.isEmbedded() :
                    self.syrthesIHMCollector.Filename_form.PublishInSalome(self.case.fullPath, self.case.dirPath)
                print("data saved")
            else :
                print("same data")

        #self.initSYRTHESFont()

    def Saving_as(self, parent=None): # fonction de rappel de sauvegarde de fichier avec dialogue
        savePathSuggestion = ""
        if self.case.fullPath == "" :
            savePathSuggestion = QtCore.QDir.currentPath() + self.tr(os.sep+"untitled.syd")
        else :
            savePathSuggestion = self.case.dirPath + self.tr(os.sep+"untitled.syd")

        dataFileselection = QtWidgets.QFileDialog.getSaveFileName(self, self.tr("Select File"), savePathSuggestion, self.tr(" *.syd ;; All Files (*)"))[0]
        if dataFileselection == "" :
            #reset font for the application
            #self.initSYRTHESFont()
            return
        dataFileselection = str(self.convertOSsep(dataFileselection))
        if dataFileselection.count(".syd") == 0 : # if user forgets to add ".syd" extension
            dataFileselection += '.syd'

        self.case.fullPath = dataFileselection

        self.setWindowTitle("SYRTHES V 4.3 - " + self.case.dirPath.split(os.sep)[-1] + " / " + self.case.name)
        self.titleChanged.emit()
##         self.emit(SIGNAL("titleChanged")) # for SALOME

        self.savfil=open(self.case.fullPath, "w")
        self.SavingCommonPart()
        if syrthesIHMContext.isEmbedded() :
            self.syrthesIHMCollector.Filename_form.PublishInSalome(self.case.fullPath, self.case.dirPath)

        print("data saved")
        #self.initSYRTHESFont()

    def SavingCompare(self, parent=None):
        if self.case.fullPath == "" or not os.path.isfile(self.case.fullPath) :
            return False # different by default

        # save actual data to a temporary file
        tempSave = self.case.dirPath + self.tr(os.sep+"temp12321.syd")
        tempSave = str(self.convertOSsep(tempSave))

        self.savfil=open(tempSave, "w")
        self.SavingCommonPart()

        # compare actual data with previous saved data
        identik = self.compfichiers(tempSave, self.case.fullPath)
        os.remove(tempSave)
        return identik

    def SavingCommonPart(self, parent=None): # common part between SavingFile() and Saving_as()
        try :
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/                DONNEES POUR L'ENSEMBLE DE L'ETUDE                 *\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/\n")
            self.syrthesIHMCollector.Home_form.save(self.savfil)
            self.savfil.write("/\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/                NOMS DES FICHIERS \n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/********************************************************************\n")
            self.syrthesIHMCollector.Filename_form.save(self.savfil)
            self.savfil.write("/\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/                DONNEES POUR LE CONTROL DES PAS DE TEMPS           *\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/\n")
            self.syrthesIHMCollector.Control_form.save(self.savfil)
            if self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked()==False:
                self.savfil.write("/\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/                DONNEES POUR LA CONDUCTION\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/********************************************************************\n")
                self.syrthesIHMCollector.Initial_conditions_cond_form.save(self.savfil)
                self.syrthesIHMCollector.Boundary_conditions_cond_form.save(self.savfil)
                if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0 : # 3D
                    self.syrthesIHMCollector.Physical_prop_3D_form.save(self.savfil)
                    self.syrthesIHMCollector.Periodicity_3D_form.save(self.savfil)
                else :
                    self.syrthesIHMCollector.Physical_prop_2D_form.save(self.savfil)
                    self.syrthesIHMCollector.Periodicity_2D_form.save(self.savfil)
                self.syrthesIHMCollector.Volumetric_conditions_cond_form.save(self.savfil)
            if self.syrthesIHMCollector.Home_form.Ho_Tr_ch.isChecked():
                self.savfil.write("/\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/                DONNEES POUR LE RAYONNEMENT\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/********************************************************************\n")
                self.syrthesIHMCollector.Spectral_parameters_form.save(self.savfil)
                if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0 : # 3D
                    self.syrthesIHMCollector.View_factor_3D_form.save(self.savfil)
                else :
                    self.syrthesIHMCollector.View_factor_2D_form.save(self.savfil)
                self.syrthesIHMCollector.Material_radiation_properties_form.save(self.savfil)
                self.syrthesIHMCollector.Boundary_conditions_rad_form.save(self.savfil)
                self.syrthesIHMCollector.Solar_aspect_form.save(self.savfil)
            if self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked():
                if self.syrthesIHMCollector.Home_form.Hm_cmb.currentIndex()==0: # Détection du type de modèle (2, 3 équations) pour sélection du tableau correspondant
                    self.syrthesIHMCollector.Initial_conditions_hum_TPv_form.save(self.savfil)
                    self.syrthesIHMCollector.Boundary_conditions_TPv_form.save(self.savfil)
                    self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form.save(self.savfil)
                    self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form.save(self.savfil)
                else :
                    self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form.save(self.savfil)
                    self.syrthesIHMCollector.Boundary_conditions_TPvPt_form.save(self.savfil)
                    self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form.save(self.savfil)
                    self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form.save(self.savfil)
                if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0 : # 3D
                    self.syrthesIHMCollector.Material_humidity_properties_3D_form.save(self.savfil)
                else:
                    self.syrthesIHMCollector.Material_humidity_properties_2D_form.save(self.savfil)
            if self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.isChecked():
                self.savfil.write("/\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/                DONNEES POUR LE MODELE 1D FLUIDE\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/\n")
                self.syrthesIHMCollector.Control_fluid1d_form.save(self.savfil)
                self.syrthesIHMCollector.Geometrie_fluid1d_form.save(self.savfil)
                self.syrthesIHMCollector.Initial_conditions_fluid1d_form.save(self.savfil)
                self.syrthesIHMCollector.Physical_prop_fluid1d_form.save(self.savfil)
                self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.save(self.savfil)
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.save(self.savfil)


            if self.syrthesIHMCollector.Home_form.Ho_fluid0d_ch.isChecked():
                self.savfil.write("/\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/                DONNEES POUR LE MODELE 0D FLUIDE\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/\n")
                self.syrthesIHMCollector.Geometrie_fluid0d_form.save(self.savfil)
                self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.save(self.savfil)
                self.syrthesIHMCollector.Physical_properties_fluid0d_form.save(self.savfil)
                self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.save(self.savfil)
                pass
            if self.syrthesIHMCollector.Home_form.Ho_Ch_ch.isChecked():
                self.syrthesIHMCollector.Conjugate_heat_transfer_form.save(self.savfil)
            self.savfil.write("/\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/                 OUTPUTS\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/********************************************************************\n")
            if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()==0 : # 3D
                self.syrthesIHMCollector.Output_3D_form.save(self.savfil)
            else :
                self.syrthesIHMCollector.Output_2D_form.save(self.savfil)
            self.savfil.write("/\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/                  DONNEES POUR LE RUNNING OPTIONS                   \n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/********************************************************************\n")
            self.savfil.write("/\n")
            self.syrthesIHMCollector.Running_options_form.save(self.savfil)
            if self.action_Advanced_mode.isChecked():
                self.savfil.write("/\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/                 MOTS CLES UNIQUEMENT COMPREHENSIBLES PAR SYRTHES***\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/********************************************************************\n")
                self.savfil.write("/*******/ IHM_ADVANCED \n")
                self.syrthesIHMCollector.Advanced_mode_form.save(self.savfil) # MUST BE SAVE AT THE END OF FILE
                self.savfil.write("/\n")
                self.savfil.write("/\n")
            self.savfil.close()
        except Exception as ex :
            if ex.args[0] == "NotFullyFilledException" :
                QtWidgets.QMessageBox.information(self, "Warning", "Of all tables, there are non-empty lines that are not fully filled (except 'User comments' column).\nThose lines will not be saved.")

    def OpeningFile(self, dataFileselection="", parent=None): # fonction de rappel du dialogue de la selection du fichier à ouvrir
        if dataFileselection == "" :
            if self.case.fullPath == "" :
                dataFileselection = QtWidgets.QFileDialog.getOpenFileName(self, self.tr("Select File"), QtCore.QDir.currentPath(), self.tr(" *.syd ;; All Files (*)"))[0]
            else:
                dataFileselection = QtWidgets.QFileDialog.getOpenFileName(self, self.tr("Select File"), self.case.dirPath, self.tr(" *.syd ;; All Files (*)"))[0]

        if dataFileselection == "": # user clicks on "Cancel"
            return -1

        dataFileselection = str(self.convertOSsep(dataFileselection))
        print(dataFileselection)
        #propose to save the current case if it's not the first time
        identik = self.SavingCompare()
        if identik == False :
            if self.case.fullPath != "" :
                reply = QtWidgets.QMessageBox.question(self, 'Message',
                                             "Do you want to save the current data file ?",
                                             QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No | QtWidgets.QMessageBox.Cancel)
                if reply == QtWidgets.QMessageBox.Yes :
                    self.SavingFile()
                elif reply == QtWidgets.QMessageBox.Cancel :
                    return -1

        newCase = clCase()
        newCase.fullPath = dataFileselection

        # verify the existence of syrthes.py, Makefile, and usr_examples folder
        if os.path.isfile(newCase.dirPath + os.sep + "syrthes.py") and \
               os.path.isfile(newCase.dirPath + os.sep + "Makefile") and \
               os.path.isdir(newCase.dirPath + os.sep + "usr_examples") :
            pass
        else:
            QtWidgets.QMessageBox.information(self, 'Message',
                                              "This data file doesn't belong to any case : syrthes.py file, Makefile file and usr_examples folder are required")
            return -1

        self.case.fullPath = dataFileselection

        # vider toutes les entrées dans l'interface actuelle
        self.clearGUI()

        # Open file
        self.opfil=open(self.case.fullPath, "r")
        self.syrthesIHMCollector.Output_3D_form.F_cb_3D_Op.toggled.connect(self.Check_fields)
##         self.disconnect(self.syrthesIHMCollector.Output_3D_form.F_cb_3D_Op, SIGNAL("toggled(bool)"), self.Check_fields)
        self.syrthesIHMCollector.Output_2D_form.F_cb_2D_Op.toggled.connect(self.Check_fields)
##         self.disconnect(self.syrthesIHMCollector.Output_2D_form.F_cb_2D_Op, SIGNAL("toggled(bool)"), self.Check_fields)
        self.OpenFile()
        self.syrthesIHMCollector.Running_options_form.Ro_Dp_cb.currentIndexChanged.connect(self.setPartitionning)
##         self.connect(self.syrthesIHMCollector.Running_options_form.Ro_Dp_cb, SIGNAL("currentIndexChanged(int)"), self.setPartitionning)
        self.syrthesIHMCollector.Output_3D_form.F_cb_3D_Op.toggled.connect(self.Check_fields)
##         self.connect(self.syrthesIHMCollector.Output_3D_form.F_cb_3D_Op, SIGNAL("toggled(bool)"), self.Check_fields)
        self.syrthesIHMCollector.Output_2D_form.F_cb_2D_Op.toggled.connect(self.Check_fields)
##         self.connect(self.syrthesIHMCollector.Output_2D_form.F_cb_2D_Op, SIGNAL("toggled(bool)"), self.Check_fields)

        # special treatment for possible anormal inversion of os separator (/, \\) in Windows
        self.case.fullPath = self.convertOSsep(self.case.fullPath)
        self.lastDir = self.case.dirPath

        # refresh user C functions view
        self.refreshUserC()

        self.setWindowTitle("SYRTHES V.4.3 - " + self.case.dirPath.split(os.sep)[-1] + " / " + self.case.name)
        self.titleChanged.emit()
##         self.emit(SIGNAL("titleChanged")) # for SALOME

        if syrthesIHMContext.isEmbedded() :
            self.syrthesIHMCollector.Filename_form.PublishInSalome(self.case.fullPath, self.case.dirPath)

        self.show()
        return 0
        #reset font for the application
        #self.initSYRTHESFont()

    def OpeningFileShell(self, parent=None): # ouverture de fichier data dont le chemin est passé en tant d'argument de la ligne de commande shell
        argPath = syrthesIHMContext.getDataAbsFullPath()
        if argPath != "" :
            self.case.fullPath = argPath
            self.lastDir = self.case.dirPath
            self.opfil=open(self.case.fullPath, "r")
            self.OpenFile()
        else :
            return

        if syrthesIHMContext.advancedModeActivated == True :
            self.action_Advanced_mode.setChecked(True)
            self.Advanced_mode.setHidden(False)

        # refresh user C functions view
        self.refreshUserC()

        self.setWindowTitle("SYRTHES V.4.3 - " + self.case.dirPath.split(os.sep)[-1] + " / " + self.case.name)
        self.titleChanged.emit()
##         self.emit(SIGNAL("titleChanged")) # for SALOME

    def refreshUserC(self):
        # to be called after OpenFile or OpenFileShell or User_C_function_Edit
        # refresh user C functions view
        common = self.case.dirPath + os.sep + "user.c"
        cond = self.case.dirPath + os.sep + "user_cond.c"
        ray = self.case.dirPath + os.sep + "user_ray.c"
        hmt = self.case.dirPath + os.sep + "user_hmt.c"
        fluid1d = self.case.dirPath + os.sep + "user_1Dfluid.c"

        # common C functions
        self.syrthesIHMCollector.User_C_function_form.Cfunc_but.setEnabled(True)
        if os.access(common, os.F_OK) :
            self.syrthesIHMCollector.User_C_function_form.Cfunc_but.setText("Open user.c")
            self.syrthesIHMCollector.User_C_function_form.Cfunc_lne.setText(common)
        else :
            self.syrthesIHMCollector.User_C_function_form.Cfunc_but.setText("Edit user.c")
            self.syrthesIHMCollector.User_C_function_form.Cfunc_lne.setText("")

        # conduction C functions
        self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_but.setEnabled(not self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked())
        if os.access(cond, os.F_OK) :
            self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_but.setText("Open user_cond.c")
            self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_lne.setText(cond)
        else :
            self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_but.setText("Edit user_cond.c")
            self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_lne.setText("")

        # radiation C functions
        self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_but.setEnabled(self.syrthesIHMCollector.Home_form.Ho_Tr_ch.isChecked())
        if os.access(ray, os.F_OK) :
            self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_but.setText("Open user_ray.c")
            self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_lne.setText(ray)
        else :
            self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_but.setText("Edit user_ray.c")
            self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_lne.setText("")

        # humidity C functions
        self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_but.setEnabled(self.syrthesIHMCollector.Home_form.Ho_Hm_ch.isChecked())
        if os.access(hmt, os.F_OK) :
            self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_but.setText("Open user_hmt.c")
            self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_lne.setText(hmt)
        else :
            self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_but.setText("Edit user_hmt.c")
            self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_lne.setText("")

        # 1D fluid flow C functions
        self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_but.setEnabled(self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch.isChecked())
        if os.access(fluid1d, os.F_OK) :
            self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_but.setText("Open user_1Dfluid.c")
            self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_lne.setText(fluid1d)
        else :
            self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_but.setText("Edit user_1Dfluid.c")
            self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_lne.setText("")

        self.syrthesIHMCollector.User_C_function_form.Cfunc_other_but.setEnabled(True)
        if self.syrthesIHMCollector.User_C_function_form.Cfunc_other_lne.text() == "" :
            self.syrthesIHMCollector.User_C_function_form.btnOpenOther.setEnabled(False)
        else :
            self.syrthesIHMCollector.User_C_function_form.btnOpenOther.setEnabled(True)

        # end user C functions

    def telecharger(self):
        self.wait = QtWidgets.QProgressDialog("Patientez", "cancel", 0, 5)
        #self.wait.setWindowModality(QtCore.Qt.WindowModal)
        self.doSomething()

    def doSomething(self):
        for i in range(1, 6):
            time.sleep(1)
            self.wait.setValue(i)
            if (self.wait.wasCanceled()):
                break
        self.wait.setValue(5)

    def clearGUI(self) :
        #vider tous les entrées dans l'interface actuelle
        #vider Home form
        self.syrthesIHMCollector.Home_form.lineEdit_7.setText("")
        self.syrthesIHMCollector.Home_form.Ho_Ds_te.setText("")
        self.syrthesIHMCollector.Home_form.Hm_cmb.setCurrentIndex(1)
        #vider Time management
        self.syrthesIHMCollector.Control_form.Ch_res_cal.setChecked(False)
        self.syrthesIHMCollector.Control_form.lineEdit_39.setText("")
        self.syrthesIHMCollector.Control_form.Le_Nts.setText("")
        self.syrthesIHMCollector.Control_form.comb_time_st.setCurrentIndex(0)
        self.syrthesIHMCollector.Control_form.Le_const_Ts.setText("")
        #rétablir Solver information
        self.syrthesIHMCollector.Control_form.lineEdit_39.setText("1.e-6")
        self.syrthesIHMCollector.Control_form.Le_Mni.setText("100")
        #vider File Names
        self.syrthesIHMCollector.Filename_form.Fn_Cd_lne.setText("")
        self.syrthesIHMCollector.Filename_form.Fn_Rm_lne.setText("")
        self.syrthesIHMCollector.Filename_form.Fn_Rs_lne.setText("")
        self.syrthesIHMCollector.Filename_form.Fn_Mt_lne.setText("")
        self.syrthesIHMCollector.Filename_form.Fn_Wd_chb.setChecked(False)
        self.syrthesIHMCollector.Filename_form.Fn_Rnp_lne.setText("resu1")  # Results names prefix

        #vider toutes les tables
        for table in self.Tables:
            table.clearContents()

        waitCursor = QtGui.QCursor(Qt.Qt.WaitCursor)
        QtWidgets.QApplication.setOverrideCursor(waitCursor)
        self.SetCellWidget()
        QtWidgets.QApplication.restoreOverrideCursor()

        #rétablir View Factor Management (radiation)
        self.syrthesIHMCollector.View_factor_2D_form.Vfm_2D_cmb.setCurrentIndex(0)
        self.syrthesIHMCollector.View_factor_3D_form.Vfm_3D_cmb.setCurrentIndex(0)
        #vider Conduction/Radiation coupling (radiation)
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne.setText("")
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne.setText("")
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne.setText("")
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_te.setText("")
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne.setText("")
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_te.setText("")
        #vider et problem with aperture (radiation)
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rpa_chb.setChecked(False)
        self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rpa_le.setText("")

        #vider Sun position
        self.syrthesIHMCollector.Solar_aspect_form.Sa_chb.setChecked(False)
        self.syrthesIHMCollector.Solar_aspect_form.Sa_cmb.setCurrentIndex(0)
        self.syrthesIHMCollector.Solar_aspect_form.spinBox.setValue(0)
        self.syrthesIHMCollector.Solar_aspect_form.spinBox_2.setValue(0)
        self.syrthesIHMCollector.Solar_aspect_form.spinBox_3.setValue(0)
        self.syrthesIHMCollector.Solar_aspect_form.spinBox_4.setValue(0)
        self.syrthesIHMCollector.Solar_aspect_form.spinBox_5.setValue(0)
        self.syrthesIHMCollector.Solar_aspect_form.spinBox_6.setValue(0)
        self.syrthesIHMCollector.Solar_aspect_form.Csm_cmb.setCurrentIndex(0)
        self.syrthesIHMCollector.Solar_aspect_form.lineEdit.setText("")
        #rétablir Sky modelling
        self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_5.setValue(0.88)
        self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_6.setValue(0.26)
        #vider Output
        self.syrthesIHMCollector.Output_2D_form.Tf_cb_2D_Op.setChecked(False)
        self.syrthesIHMCollector.Output_2D_form.Cb_2D_Op.setCurrentIndex(0)
        self.syrthesIHMCollector.Output_2D_form.Le_2D_Op.setText("")
        self.syrthesIHMCollector.Output_2D_form.Le2_2D_Op.setText("")
        self.syrthesIHMCollector.Output_2D_form.Hf_cb_2D_Op.setChecked(False)
        self.syrthesIHMCollector.Output_2D_form.Mt_cb_2D_Op.setChecked(False)
        self.syrthesIHMCollector.Output_2D_form.F_cb_2D_Op.setChecked(False)
        self.syrthesIHMCollector.Output_2D_form.Ai_but_2D_Op.setEnabled(False)
        self.syrthesIHMCollector.Output_3D_form.Tf_cb_3D_Op.setChecked(False)
        self.syrthesIHMCollector.Output_3D_form.Cb_3D_Op.setCurrentIndex(0)
        self.syrthesIHMCollector.Output_3D_form.Le_3D_Op.setText("")
        self.syrthesIHMCollector.Output_3D_form.Le2_3D_Op.setText("")
        self.syrthesIHMCollector.Output_3D_form.Hf_cb_3D_Op.setChecked(False)
        self.syrthesIHMCollector.Output_3D_form.Mt_cb_3D_Op.setChecked(False)
        self.syrthesIHMCollector.Output_3D_form.F_cb_3D_Op.setChecked(False)
        self.syrthesIHMCollector.Output_3D_form.Ai_but_3D_Op.setEnabled(False)
        #rétablir Running options
        self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_cd.setValue(1)
        self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_ry.setValue(1)
        self.syrthesIHMCollector.Running_options_form.Ro_Ln_le.setText("listing")  # Listing name
        self.syrthesIHMCollector.Running_options_form.Ro_Dp_cb.setCurrentIndex(0)
        self.syrthesIHMCollector.Running_options_form.Ro_Cr_cb.setCurrentIndex(0)
        #vider User C functions
        self.syrthesIHMCollector.User_C_function_form.Cfunc_lne.setText("")
        self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_lne.setText("")
        self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_lne.setText("")
        self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_lne.setText("")
        self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_lne.setText("")
        self.syrthesIHMCollector.User_C_function_form.Cfunc_other_lne.setText("")
        #fluid1d
        self.syrthesIHMCollector.Filename_form.Fn_fluid1d_lne.setText("")
        self.syrthesIHMCollector.Control_fluid1d_form.Le_const_Ts_fluid1d.setText("")
        self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1df_lne.setText("")
        self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1dfUc_lne.setText("")
        self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_x.setText("")
        self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_y.setText("")
        self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_z.setText("")


    def initHheader(self, parent=None):
        Tables=list(self.dic_Table_type.keys())
        i=0
        while i<len(Tables):
            Hheader=Tables[i].horizontalHeader()
            Hheader.setStretchLastSection(False)
            Tables[i].resizeColumnsToContents()
            Hheader.setStretchLastSection(True)
            Hheader.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
            i=i+1

    def setupHheader(self, i, j, parent=None): # fonction de rappel permettant la remise en forme des tableau (extension des lignes jusqu'à la fin du tableau)
        widget=QtWidgets.QApplication.focusWidget()
        if widget == None:
            return
        table=None
        if type(widget)!=QtWidgets.QTableWidget:
            table = widget.parentWidget().parentWidget()
        else:
            table = widget

        if type(table) != QtWidgets.QTableWidget:
            return

        Hheader = table.horizontalHeader()
        Hheader.setStretchLastSection(True)
        size2 = Hheader.sectionSize(table.columnCount()-1)
        Hheader.setStretchLastSection(False)
        size1 = Hheader.sectionSize(table.columnCount()-1)
        if size1 < size2:
            Hheader.setStretchLastSection(True)
        else:
            Hheader.setStretchLastSection(False)

    def copyPropPhy(self, parent=None):
        # Fonction de rappel permettant de garder les propriétés physiques lors de passage entre 3D et 2D
        if self.syrthesIHMCollector.Home_form.Dim_Comb.currentIndex()!=0:
            tableFrom = self.syrthesIHMCollector.Physical_prop_3D_form.Iso_cond_3D_table
            tableTo = self.syrthesIHMCollector.Physical_prop_2D_form.Iso_cond_2D_table
        else:
            tableFrom = self.syrthesIHMCollector.Physical_prop_2D_form.Iso_cond_2D_table
            tableTo = self.syrthesIHMCollector.Physical_prop_3D_form.Iso_cond_3D_table

        # check if tableTo (cible) is already filled
        for i in range(tableTo.rowCount()) :
            if tableTo.item(i,5) != None :
                if tableTo.item(i,5).text() != '' :
                    return # if filled --> don't copy any more

        # copy
        for i in range(tableFrom.rowCount()) :
            for j in range(tableFrom.columnCount()) :
                if j == 1 :
                    currentItem = tableFrom.cellWidget(i, j)
                    if currentItem != None :
                        tableTo.cellWidget(i,j).setCurrentIndex(currentItem.currentIndex())
                if j >= 2 :
                    currentItem = tableFrom.item(i, j)
                    if currentItem != None :
                        cloneItem = QtWidgets.QTableWidgetItem()
                        cloneItem.setText(currentItem.text())
                        tableTo.setItem(i, j, cloneItem)
            if tableTo.cellWidget(i, 1) != None :
                if tableTo.cellWidget(i, 1).currentIndex() == 2 : # PROGRAM --> blacken cells
                    self.Table_prog2(tableTo, None, i)

    def calc(self, parent=None): # fonction de rappel de la QAction du lancement du suivis de calcul
        if self.case.fullPath == "" :
            QtWidgets.QMessageBox.information(self, 'Message', "No case found.")
            #self.initSYRTHESFont()
            return

        #print calcView
        #global Main2
        # constituer le nom d'un (des fichiers) .his
        self.case.setHisnamePrefix(self.syrthesIHMCollector.Filename_form.Fn_Rnp_lne.text(), self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_cd.value())

        self.Main2 = calcView(self.syrthesIHMCollector.Home_form, self.syrthesIHMCollector.Control_form, self.syrthesIHMCollector.Filename_form, self.syrthesIHMCollector.Output_2D_form, self.syrthesIHMCollector.Output_3D_form, self.syrthesIHMCollector.Running_options_form, self.case, self.lastDir, self.runProcess)
        self.Main2.Syrthes_stopping.connect(self.Syrthes_stopping)
##         self.connect(self.Main2, SIGNAL("Syrthes_stopping"), self.Syrthes_stopping)
        self.Main2.Syrthes_completed.connect(self.Syrthes_completed)
##         self.connect(self.Main2, SIGNAL("Syrthes_completed"), self.Syrthes_completed)
        self.Main2.show()

    def Enable(self, parent=None): # fonction de rappel permettant de rendre sensible les composant graphique concernant le fichier de reprise
        chk=self.syrthesIHMCollector.Control_form.Ch_res_cal.isChecked()
        self.syrthesIHMCollector.Filename_form.Fn_Rs_lb.setEnabled(chk)
        self.syrthesIHMCollector.Filename_form.Fn_Rs_lne.setEnabled(chk)
        self.syrthesIHMCollector.Filename_form.Fn_Rs_but.setEnabled(chk)
        if chk and self.syrthesIHMCollector.Filename_form.Fn_Rs_lne.text()=='':
            QtWidgets.QMessageBox.information(self, 'Message', "Please specify the restart file name in the \"File Names\" form.")

    def EnableA(self, a):
        if a==0:
            self.syrthesIHMCollector.Control_form.Vap_lb4.setEnabled(False)
            self.syrthesIHMCollector.Control_form.Ap_Sp_le.setEnabled(False)
            self.syrthesIHMCollector.Control_form.Ap_Mn_le.setEnabled(False)
            self.syrthesIHMCollector.Control_form.ptLabel.setEnabled(False)
            self.syrthesIHMCollector.Control_form.Le_auto_Mpt.setEnabled(False)
        else :
            self.syrthesIHMCollector.Control_form.Vap_lb4.setEnabled(True)
            self.syrthesIHMCollector.Control_form.Ap_Sp_le.setEnabled(True)
            self.syrthesIHMCollector.Control_form.Ap_Mn_le.setEnabled(True)
            self.syrthesIHMCollector.Control_form.ptLabel.setEnabled(True)
            self.syrthesIHMCollector.Control_form.Le_auto_Mpt.setEnabled(True)


    def setuptree(self, parent=None): # Définition du contenus de l'arborescence

        self.Home = QtWidgets.QTreeWidgetItem(self.treeWidget)
        self.Filename = QtWidgets.QTreeWidgetItem(self.treeWidget)
        self.Conduction = QtWidgets.QTreeWidgetItem(self.treeWidget)
        self.Initial_conditions_cond = QtWidgets.QTreeWidgetItem(self.Conduction)
        self.Boundary_conditions_cond = QtWidgets.QTreeWidgetItem(self.Conduction)
        self.Physical_Properties = QtWidgets.QTreeWidgetItem(self.Conduction)
        self.Volumetric_conditions_cond = QtWidgets.QTreeWidgetItem(self.Conduction)
        self.Periodicity = QtWidgets.QTreeWidgetItem(self.Conduction)
        self.Radiation = QtWidgets.QTreeWidgetItem(self.treeWidget)
        self.Spectral_parameters= QtWidgets.QTreeWidgetItem(self.Radiation)
        self.View_factor= QtWidgets.QTreeWidgetItem(self.Radiation)
        self.Material_radiation_properties = QtWidgets.QTreeWidgetItem(self.Radiation)
        self.Boundary_conditions_rad = QtWidgets.QTreeWidgetItem(self.Radiation)
        self.Solar_aspect = QtWidgets.QTreeWidgetItem(self.Radiation)
        self.Humidity = QtWidgets.QTreeWidgetItem(self.treeWidget)
        self.Initial_conditions_hum = QtWidgets.QTreeWidgetItem(self.Humidity)
        self.Material_Properties_hum = QtWidgets.QTreeWidgetItem(self.Humidity)
        self.Boundary_conditions_hum = QtWidgets.QTreeWidgetItem(self.Humidity)
        self.Contact_resistance_humidity = QtWidgets.QTreeWidgetItem(self.Humidity)
        self.Volumetric_conditions_hum = QtWidgets.QTreeWidgetItem(self.Humidity)
        self.Conjugate_heat_transfer = QtWidgets.QTreeWidgetItem(self.treeWidget)
        # fluid1d
        self.fluid1d                       = QtWidgets.QTreeWidgetItem(self.treeWidget)
        self.Geometrie_fluid1d             = QtWidgets.QTreeWidgetItem(self.fluid1d)
        self.Initial_conditions_fluid1d    = QtWidgets.QTreeWidgetItem(self.fluid1d)
        self.Boundary_conditions_fluid1d   = QtWidgets.QTreeWidgetItem(self.fluid1d)
        self.Physical_prop_fluid1d         = QtWidgets.QTreeWidgetItem(self.fluid1d)
        self.Volumetric_conditions_fluid1d = QtWidgets.QTreeWidgetItem(self.fluid1d)
        self.Control_fluid1d               = QtWidgets.QTreeWidgetItem(self.fluid1d)
        # fluid0d
        self.fluid0d                       = QtWidgets.QTreeWidgetItem(self.treeWidget)
        self.Geometrie_fluid0d             = QtWidgets.QTreeWidgetItem(self.fluid0d)
        self.Boundary_conditions_fluid0d   = QtWidgets.QTreeWidgetItem(self.fluid0d)
        self.Physical_properties_fluid0d   = QtWidgets.QTreeWidgetItem(self.fluid0d)
        self.Volumetric_conditions_fluid0d = QtWidgets.QTreeWidgetItem(self.fluid0d)
        #
        self.User_C_function = QtWidgets.QTreeWidgetItem(self.treeWidget)
        self.Advanced_mode = QtWidgets.QTreeWidgetItem(self.treeWidget)
        self.Control = QtWidgets.QTreeWidgetItem(self.treeWidget)
        self.Output = QtWidgets.QTreeWidgetItem(self.treeWidget)
        self.Running_options = QtWidgets.QTreeWidgetItem(self.treeWidget)
        self.treeWidget.topLevelItem(0).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Home"))
        self.treeWidget.topLevelItem(1).setText(0, QtCore.QCoreApplication.translate("MainWindow", "File Names"))
        self.treeWidget.topLevelItem(2).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Conduction"))
        self.treeWidget.topLevelItem(2).child(0).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Initial conditions"))
        self.treeWidget.topLevelItem(2).child(1).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Boundary conditions"))
        self.treeWidget.topLevelItem(2).child(2).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Physical properties"))
        self.treeWidget.topLevelItem(2).child(3).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Volumetric conditions"))
        self.treeWidget.topLevelItem(2).child(4).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Periodicity"))
        self.treeWidget.topLevelItem(3).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Radiation"))
        self.treeWidget.topLevelItem(3).child(0).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Spectral parameters"))
        self.treeWidget.topLevelItem(3).child(1).setText(0, QtCore.QCoreApplication.translate("MainWindow", "View Factor"))
        self.treeWidget.topLevelItem(3).child(2).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Material radiation properties"))
        self.treeWidget.topLevelItem(3).child(3).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Boundary conditions"))
        self.treeWidget.topLevelItem(3).child(4).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Solar modelling"))
        self.treeWidget.topLevelItem(4).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Humidity"))
        self.treeWidget.topLevelItem(4).child(0).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Initial conditions"))
        self.treeWidget.topLevelItem(4).child(1).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Material properties"))
        self.treeWidget.topLevelItem(4).child(2).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Coupled Boundary conditions"))
        self.treeWidget.topLevelItem(4).child(3).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Contact resistance"))
        self.treeWidget.topLevelItem(4).child(4).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Volumetric conditions"))
        self.treeWidget.topLevelItem(5).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Conjugate heat transfer"))
        # fluid1d
        self.treeWidget.topLevelItem(6).setText(0, QtCore.QCoreApplication.translate("MainWindow", "SYRTHES 1D fluid flow"))
        self.treeWidget.topLevelItem(6).child(0).setText(0, QtCore.QCoreApplication.translate("MainWindow", "1D fluid Geometry"))
        self.treeWidget.topLevelItem(6).child(1).setText(0, QtCore.QCoreApplication.translate("MainWindow", "1D fluid Initial conditions"))
        self.treeWidget.topLevelItem(6).child(2).setText(0, QtCore.QCoreApplication.translate("MainWindow", "1D fluid Boundary conditions"))
        self.treeWidget.topLevelItem(6).child(3).setText(0, QtCore.QCoreApplication.translate("MainWindow", "1D fluid Physical properties"))
        self.treeWidget.topLevelItem(6).child(4).setText(0, QtCore.QCoreApplication.translate("MainWindow", "1D fluid Volumetric conditions"))
        self.treeWidget.topLevelItem(6).child(5).setText(0, QtCore.QCoreApplication.translate("MainWindow", "1D fluid Time step"))
        # fluid0d
        self.treeWidget.topLevelItem(7).setText(0, QtCore.QCoreApplication.translate("MainWindow", "SYRTHES 0D fluid flow"))
        self.treeWidget.topLevelItem(7).child(0).setText(0, QtCore.QCoreApplication.translate("MainWindow", "0D fluid Geometry"))
        self.treeWidget.topLevelItem(7).child(1).setText(0, QtCore.QCoreApplication.translate("MainWindow", "0D fluid Boundary conditions"))
        self.treeWidget.topLevelItem(7).child(2).setText(0, QtCore.QCoreApplication.translate("MainWindow", "0D fluid Physical properties"))
        self.treeWidget.topLevelItem(7).child(3).setText(0, QtCore.QCoreApplication.translate("MainWindow", "0D fluid Volumetric conditions"))
        #
        self.treeWidget.topLevelItem(8).setText(0, QtCore.QCoreApplication.translate("MainWindow", "User C functions"))
        self.treeWidget.topLevelItem(9).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Advanced Mode"))
        self.treeWidget.topLevelItem(10).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Control"))
        self.treeWidget.topLevelItem(11).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Output"))
        self.treeWidget.topLevelItem(12).setText(0, QtCore.QCoreApplication.translate("MainWindow", "Running options"))

    def Advanced_mode_select(self):
        if self.action_Advanced_mode.isChecked():
            self.Advanced_mode.setHidden(False)
        else:
            self.Advanced_mode.setHidden(True)

    def showWhatsThis(self):
        #print "whats this"
        QtWidgets.QWhatsThis.enterWhatsThisMode ()

    def showAbout(self):
        #MPpath = syrthesIHMContext.getExeAbsDirPath()
        path = os.getenv('SYRTHES4_HOME')+ os.sep + "lib"+ os.sep + "syrthesGui"
        fichier = open(path + os.sep + '22x22' + os.sep + 'about', 'r')
        lignes = fichier.readlines()
        fichier.close()
        textAbout = ""
        for line in lignes:
            textAbout += line
        #QtWidgets.QMessageBox.about(self,"About","<html><table cellspacing=20><tr><td align=left><img src=%slogo_syrthes_png_sml.png ></td>"%('22x22' + os.sep)
        #                                     + "<td>" + textAbout
        #                                     + "</td></tr></table></html>")
        textAbout = textAbout.replace("!!!", path + os.sep + "22x22" + os.sep)
        QtWidgets.QMessageBox.about(self, "About", textAbout)

    def showLicence(self):
        #MPpath = syrthesIHMContext.getExeAbsDirPath()
        path = os.getenv('SYRTHES4_HOME')+ os.sep + "lib"+ os.sep + "syrthesGui"
        fichier = open(path + os.sep + '22x22' + os.sep + 'license', 'r')
        lignes = fichier.readlines()
        fichier.close()
        textLicense = ""
        for line in lignes:
            textLicense += line

        QtWidgets.QMessageBox.about(self,"Licence",textLicense)

    def setNedit(self):
        self.actionNedit.setChecked(True)
        self.textEditor = "nedit"
        self.actionNotepad.setChecked(False)
        self.actionCustomize.setChecked(False)
        self.writePref()

    def setNotepad(self):
        self.actionNedit.setChecked(False)
        self.actionNotepad.setChecked(True)
        self.textEditor = "C:" + os.sep + "WINDOWS" + os.sep + "notepad.exe"
        self.actionCustomize.setChecked(False)
        self.writePref()

    def initSYRTHESFont(self):
        # (re)set font for the application
        return
        if not syrthesIHMContext.isEmbedded() :
            myFont= app.font()
            myFont.setFamily('Sans')
            myFont.setStyleHint(QFont.SansSerif, QFont.PreferMatch)
            myFont.setPointSize(10)
            app.setFont(myFont)

    def showEditorChoice(self):
        custom = ''
        if os.name != 'nt':
            nomfichier = os.environ['HOME'] + os.sep + '.syrthes' + os.sep + 'config.data'
            if os.access(nomfichier, os.F_OK) :
                fichier = open(nomfichier, 'r')
                lignes = fichier.readlines()
                custom = lignes[1] # 2e ligne --> reader texte
                custom = custom.strip()
                fichier.close()
        else:
            # list values owned by this registry key
            try:
                reg = winreg.OpenKey(winreg.HKEY_CURRENT_USER,"Software\\SYRTHES")
                custom = winreg.QueryValue(reg, "textEditor")
            except WindowsError:
                pass

        if os.name != 'nt' :
            text, ok = QtWidgets.QInputDialog.getText(self, 'Input Dialog',
                "The actual editor is " + self.textEditor + ". \nChange the text editor here : ",
                QtWidgets.QLineEdit.Normal, custom)
        else:
            ok = True
            text = QtWidgets.QFileDialog.getOpenFileName(self, "Choose a text editor :", \
                custom, "Executable file (*.exe)")[0]

        if ok and str(text) != "" : # user clicks on ok AND typed the name of editor.
            self.textEditor = r"" + str(text)
            self.actionNedit.setChecked(False)
            self.actionNotepad.setChecked(False)
            self.actionCustomize.setChecked(True)
            self.actionCustomize.setText("Customize (" + self.textEditor + ")")

            self.writePref()
        else: # ne rien faire
            if self.actionNotepad.isChecked() or self.actionNedit.isChecked() :
                self.actionCustomize.setChecked(False)
            else :
                self.actionCustomize.setChecked(True)

    def setXpdf(self):
        self.actionXpdf.setChecked(True)
        self.pdfReader = "xpdf"
        self.actionDefaultPdf.setChecked(False)
        self.actionPDFCustomize.setChecked(False)
        self.writePref()

    def setDefaultPdf(self):
        self.actionDefaultPdf.setChecked(True)
        self.pdfReader = ""
        self.actionXpdf.setChecked(False)
        self.actionPDFCustomize.setChecked(False)
        self.writePref()

    def showPDFChoice(self):
        # custom = name of customized pdf reader, will be read from the registry (Windows)
        # or from $HOME/.syrthes/data.config (Linux)
        custom = ''
        if os.name != 'nt':
            nomfichier = os.environ['HOME'] + os.sep + '.syrthes' + os.sep + 'config.data'
            if os.access(nomfichier, os.F_OK) :
                fichier = open(nomfichier, 'r')
                lignes = fichier.readlines()
                if len(lignes) < 4 :
                    custom = ''
                else :
                    custom = lignes[3] # 4e ligne --> reader PDF
                fichier.close()
        else:
            try:
                reg = winreg.OpenKey(winreg.HKEY_CURRENT_USER,"Software\\SYRTHES")
                custom = winreg.QueryValue(reg, "pdfReader")
            except WindowsError:
                pass

        pr = self.pdfReader
        if self.pdfReader == "" :
            pr = "PDF reader by default"

        if os.name != 'nt' :
            text, ok = QtWidgets.QInputDialog.getText(self, 'Input Dialog',
                "The actual PDF reader is " + pr + ". \nChange the reader here : ",
                QtWidgets.QLineEdit.Normal, custom)
        else:
            ok = True
            text = QtWidgets.QFileDialog.getOpenFileName(self, "Choose a pdf reader :", \
                custom, "Executable file (*.exe)")[0]

        if ok and str(text) != "" : # user clicks on ok AND typed the name of editor.
            self.pdfReader = r"" + str(text)
            self.actionXpdf.setChecked(False)
            self.actionDefaultPdf.setChecked(False)
            self.actionPDFCustomize.setChecked(True)
            self.actionPDFCustomize.setText("Customize (" + self.pdfReader + ")")

            self.writePref()
        else: # ne rien faire
            if self.actionXpdf.isChecked() or self.actionDefaultPdf.isChecked():
                self.actionPDFCustomize.setChecked(False)
            else :
                self.actionPDFCustomize.setChecked(True)

    def readPref(self): # for description, cf. writePref()
        if os.name != 'nt':
            self.actionNotepad.setEnabled(False)
            self.actionNedit.setChecked(True)
            self.textEditor = "nedit"
            self.actionDefaultPdf.setEnabled(False)
            self.actionXpdf.setChecked(True)
            self.pdfReader = "xpdf"
            nomfichier = os.environ['HOME'] + os.sep + '.syrthes' + os.sep + 'config.data'
            if os.access(nomfichier, os.F_OK):
                fichier = open(nomfichier, 'r')
                lignes = fichier.readlines()
                try:
                    if str(lignes[0]) == "True\n" :
                        self.actionCustomize.setChecked(True)
                        self.actionNedit.setChecked(False)
                        self.textEditor = lignes[1]
                        self.textEditor = self.textEditor.strip()
                        self.actionCustomize.setText("Customize (" + self.textEditor + ")")
                    if str(lignes[2]) == "True\n" :
                        self.actionPDFCustomize.setChecked(True)
                        self.actionXpdf.setChecked(False)
                        self.pdfReader = lignes[3]
                        self.pdfReader = self.pdfReader.strip()
                        self.actionPDFCustomize.setText("Customize (" + self.pdfReader + ")")
                except:
                    pass
        else:
            self.actionNedit.setEnabled(False)
            self.actionNotepad.setChecked(True)
            self.textEditor = "C:" + os.sep + "WINDOWS" + os.sep + "notepad.exe"
            self.actionXpdf.setEnabled(False)
            self.actionDefaultPdf.setChecked(True)
            self.pdfReader = ""
            try:
                reg = winreg.OpenKey(winreg.HKEY_CURRENT_USER,"Software\\SYRTHES")
                if winreg.QueryValue(reg, "useTextEditor") == "True" :
                    self.actionCustomize.setChecked(True)
                    self.actionNotepad.setChecked(False)
                    self.textEditor = winreg.QueryValue(reg, "textEditor")
                    self.actionCustomize.setText("Customize (" + self.textEditor + ")")
                if winreg.QueryValue(reg, "usePdfReader") == "True" :
                    self.actionPDFCustomize.setChecked(True)
                    self.actionDefaultPdf.setChecked(False)
                    self.pdfReader = winreg.QueryValue(reg, "pdfReader")
                    self.actionPDFCustomize.setText("Customize (" + self.pdfReader + ")")
            except WindowsError:
                pass

    def writePref(self):
        if os.name != 'nt':
            # Linux : write in /$HOME/.syrthes/config.data
            # same order like in Windows (see below)
            nomfichier = os.environ['HOME'] + os.sep + '.syrthes' + os.sep + 'config.data'
            if not os.path.isdir(os.environ['HOME'] + os.sep + '.syrthes') :
                os.mkdir(os.environ['HOME'] + os.sep + '.syrthes')
            fichier = open(nomfichier, 'w')

            useTextEditor = str(self.actionCustomize.isChecked())
            fichier.write(useTextEditor+'\n')
            fichier.write(self.textEditor + '\n')

            usePdfReader = str(self.actionPDFCustomize.isChecked())
            fichier.write(usePdfReader+'\n')
            fichier.write(self.pdfReader + '\n')

            fichier.close()
        else:
            # Windows :
            # dans registry HKEY_CURRENT_USER\Software\SYRTHES :
            # line 1 : whether customized text editor is used
            # line 2 : name of customized text editor
            # line 3 : whether customized pdf reader is used
            # line 4 : name of customized pdf reader
            reg = winreg.CreateKey(winreg.HKEY_CURRENT_USER,"Software\\SYRTHES")

            utext = "%s" % str(self.actionPDFCustomize.isChecked())
            winreg.SetValue(reg, u"usePdfReader", winreg.REG_SZ, utext)
            if utext == "True" :
                utext = "%s" %(self.pdfReader)
                winreg.SetValue(reg, u"pdfReader", winreg.REG_SZ, utext)

            utext = "%s" % str(self.actionCustomize.isChecked())
            winreg.SetValue(reg, u"useTextEditor", winreg.REG_SZ, utext)
            if utext == "True" :
                utext = "%s" %(self.textEditor)
                winreg.SetValue(reg, u"textEditor", winreg.REG_SZ, utext)

    def showUserGuide(self):
        # récupérer la variable d'environnement $SYRTHES4_HOME
        if os.name=='nt':
            SYRTHES_home_path = os.popen('echo %SYRTHES4_HOME%').read()
        else :
            SYRTHES_home_path = os.popen('echo $SYRTHES4_HOME').read()
        SYRTHES_home_path = (SYRTHES_home_path.split('\n'))[0] # éliminer le \n à la fin de SYRTHES_home_path
        self.SYRTHES_home_path = SYRTHES_home_path

        if SYRTHES_home_path == "" or SYRTHES_home_path == "%SYRTHES4_HOME%" :
            QtWidgets.QMessageBox.information(self, "Message", "no $SYRTHES4_HOME found")
            return

        doc = SYRTHES_home_path + os.sep + "share" + os.sep + "syrthes" + os.sep + 'doc' + os.sep + "syrthes4_user_guide.pdf"
        if self.pdfReader == '' :
            os.system("start " + doc)
        else:
            try:
                subprocess.Popen([self.pdfReader,doc])
            except Exception as e:
                print(e)

    def showValidationGuide(self):
        # récupérer la variable d'environnement $SYRTHES4_HOME
        if os.name=='nt':
            SYRTHES_home_path = os.popen('echo %SYRTHES4_HOME%').read()
        else :
            SYRTHES_home_path = os.popen('echo $SYRTHES4_HOME').read()
        SYRTHES_home_path = (SYRTHES_home_path.split('\n'))[0] # éliminer le \n à la fin de SYRTHES_home_path
        self.SYRTHES_home_path = SYRTHES_home_path

        if SYRTHES_home_path == "" or SYRTHES_home_path == "%SYRTHES4_HOME%" :
            QtWidgets.QMessageBox.information(self, "Message", "no $SYRTHES4_HOME found")
            return

        doc = SYRTHES_home_path + os.sep + "share" + os.sep + "syrthes" + os.sep + 'doc' + os.sep + "syrthes4_validation.pdf"
        if self.pdfReader == '' :
            os.system("start " + doc)
        else:
            try:
                subprocess.Popen([self.pdfReader,doc])
            except Exception as e:
                print(e)

    def showTutorial(self):
        # récupérer la variable d'environnement $SYRTHES4_HOME
        if os.name=='nt':
            SYRTHES_home_path = os.popen('echo %SYRTHES4_HOME%').read()
        else :
            SYRTHES_home_path = os.popen('echo $SYRTHES4_HOME').read()
        SYRTHES_home_path = (SYRTHES_home_path.split('\n'))[0] # éliminer le \n à la fin de SYRTHES_home_path

        if SYRTHES_home_path == "" or SYRTHES_home_path == "%SYRTHES4_HOME%" :
            QtWidgets.QMessageBox.information(self, "Message", "no $SYRTHES4_HOME found")
            return

        doc = SYRTHES_home_path + os.sep + "share" + os.sep + "syrthes" + os.sep + 'doc' + os.sep + "syrthes4_tutorial.pdf"
        if self.pdfReader == '' :
            os.system("start " + doc)
        else:
            try:
                subprocess.Popen([self.pdfReader,doc])
            except Exception as e:
                print(e)

    def showShell(self):
        if os.name == 'nt' :
            subprocess.Popen(["start", "cd", self.case.dirPath], shell=True)
            return
        try : # pour KDE pour l'instant
            #subprocess.Popen("konsole --workdir " + self.case.dirPath, shell=True)
            subprocess.Popen("xterm -e bash -c \"cd " + self.case.dirPath + ";bash\"", shell=True)
        except :
            pass


    def showDesc(self):
        formatlist=self.tr(' *.syr_desc ;; All Files (*)')
        descFile = QtWidgets.QFileDialog.getOpenFileName(self, self.tr("Select File"), self.case.dirPath+self.tr(os.sep),formatlist)[0]
        if str(descFile) == "" : # user clicks on "Cancel"
            return
        descFile = str(self.convertOSsep(descFile))
        self.textEditor = self.textEditor.replace(" ", "\" \"")
        try:
            subprocess.Popen(self.textEditor + ' \"' +str(descFile) + '\"', shell=True)
        except Exception as e:
            print(e)
            return
        self.textEditor = self.textEditor.replace("\" \"", " ")
        pass

    def convertOSsepRel(self, strIn):
        # special treatment for possible anormal inversion of os separator (/, \\) in Windows
        sIn = str(strIn)
        if sIn.find(os.sep) == -1 :
            if os.sep == "\\" :
                sIn = sIn.replace("/", "\\")
            else :
                sIn = sIn.replace("\\", "/")
            pass
        return sIn

    def convertOSsep(self, strIn):
        # special treatment for possible anormal inversion of os separator (/, \\) in Windows
        sIn = str(strIn)
        if sIn.find(os.sep) == -1 :
            if os.sep == "\\" :
                sIn = sIn.replace("/", "\\")
            else :
                sIn = sIn.replace("\\", "/")
            pass
        return os.path.abspath(sIn)

    def ok_m(self):
        self.close()

    def closeEvent(self, event):
        if self.case.fullPath == "" :
            return

        identik = self.SavingCompare()
        if identik == True :
            return # nothing to do if nothing to save

        reply = QtWidgets.QMessageBox.question(self, 'Message',
                                     "Do you want to save the current data file ?",
                                     QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No | QtWidgets.QMessageBox.Cancel)

        if reply == QtWidgets.QMessageBox.Yes :
            self.SavingFile()
            event.accept()
        elif reply == QtWidgets.QMessageBox.No :
            event.accept()
        else:
            event.ignore()

    def compfichiers(self, nfc1, nfc2, lgbuf=32*1024):
        """Compare les 2 fichiers et renvoie True seulement s'ils ont un contenu identique"""
        return filecmp.cmp(nfc1, nfc2)
        #f1 = f2 = None
        #result = False
        #try:
            #if os.path.getsize(nfc1) == os.path.getsize(nfc2):
            #f1 = open(nfc1, "rb")
            #f2 = open(nfc2, "rb")
            #while True:
            #    buf1 = f1.read(lgbuf)
            #    if len(buf1) == 0:
            #        result = True
            #        break
            #    buf2 = f2.read(lgbuf)
            #    if buf1 != buf2:
            #        break
            #f1.close()
            #f2.close()
        #except:
            #if f1 != None: f1.close()
            #if f2 != None: f2.close()
            #return False
            #raise IOError
        #return result

    def createNewCase(self):
        print("SyrthesMain.createNewCase")

        # récupérer la variable d'environnement $SYRTHES4_HOME
        if os.name=='nt':
            SYRTHES_home_path = os.popen('echo %SYRTHES4_HOME%').read()
        else :
            SYRTHES_home_path = os.popen('echo $SYRTHES4_HOME').read()
        SYRTHES_home_path = (SYRTHES_home_path.split('\n'))[0] # éliminer le \n à la fin de SYRTHES_home_path
        self.SYRTHES_home_path = SYRTHES_home_path

        if SYRTHES_home_path == "" or SYRTHES_home_path == "%SYRTHES4_HOME%" :
            QtWidgets.QMessageBox.information(self, "Message", "no $SYRTHES4_HOME found")
            return
        #print '$SYRTHES4_HOME =', SYRTHES_home_path

        # suggérer la sauvegarde
        reply = QtWidgets.QMessageBox.Yes # initial value of reply (response of user to following questions) :
        if self.case.dirPath != '' : #windowTitle() == 'New': # windowTitle = New -> not the Welcome dialog box -> propose to save data
            reply = QtWidgets.QMessageBox.question(self, 'Message', "Do you want to save the current data file ?",
                                                   QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No | QtWidgets.QMessageBox.Cancel)
            if reply == QtWidgets.QMessageBox.Yes :
                self.SavingFile()
            elif reply == QtWidgets.QMessageBox.Cancel :
                #reset font for the application
                #self.initSYRTHESFont()
                return

        # demander le nom de dossier du nouveau cas
        folderSelection = QtWidgets.QFileDialog.getExistingDirectory(self, "Please choose an empty folder or create a new folder :")
        if folderSelection == "" :
            return

        folderSelection = str(self.convertOSsep(folderSelection))
        if os.listdir(str(folderSelection)) == [] :
            # supprimer le dossier créé par QFileDialog plus haut car syrthes4_create_case va le créer
            os.rmdir(folderSelection) # use python function instead of system
            #os.popen('rm -rf ' + "\"" + folderSelection + "\"")
            if os.path.isdir(folderSelection) : # au cas où la précédante suppression est prohibée
                QtWidgets.QMessageBox.information(self, "Message", "Access to " + folderSelection + " denied. Please try again or choose another empty folder or create a new folder")
                return
        else:
            QtWidgets.QMessageBox.information(self, "Message", "Please choose an empty folder or create a new folder")
            return

        # clear la présente IHM et créer une nouvelle
        self.clearGUI()
        self.case.dirPath = folderSelection
        self.case.name = "untitled.syd" # donner automatiquement le nom du fichier data

        self.setWindowTitle("SYRTHES V 4.3 - " + self.case.dirPath.split(os.sep)[-1] + " / " + self.case.name)
        self.titleChanged.emit()
##         self.emit(SIGNAL("titleChanged")) # for SALOME

        # Créer le nouveau cas
        pipe = os.popen(SYRTHES_home_path + os.sep + 'bin' + os.sep + 'syrthes4_create_case ' + "\"" + str(self.case.dirPath) + "\"").read()
        print(pipe)

        self.lastDir = self.case.dirPath

        #reset font for the application
        #self.initSYRTHESFont()
        self.show()
        self.syrthesIHMCollector.DialogNew.accept()

    def createNewData(self):
        #global Main
        #print self.windowTitle()

        folderSelection= self.case.dirPath

        if folderSelection == '':
            folderSelection = os.curdir

        if os.path.isfile(folderSelection + os.sep + "syrthes.py") and os.path.isfile(folderSelection + os.sep + "Makefile") and os.path.isdir(folderSelection + os.sep + "usr_examples") :
            reply = QtWidgets.QMessageBox.Yes # initial value of reply (response of user to following questions) :
            reply = QtWidgets.QMessageBox.question(self, 'Message', "Do you want to create a new data file in the current case ?", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No | QtWidgets.QMessageBox.Cancel)
            if reply == QtWidgets.QMessageBox.Yes :
                pass
            elif reply == QtWidgets.QMessageBox.No :
                folderSelection = QtWidgets.QFileDialog.getExistingDirectory(None, "Choose an existing case", "", QtWidgets.QFileDialog.ShowDirsOnly)
                if folderSelection == "" :
                    return
            elif reply == QtWidgets.QMessageBox.Cancel :
                return
            pass
        else:
            folderSelection = QtWidgets.QFileDialog.getExistingDirectory(None, "Choose an existing case", "", QtWidgets.QFileDialog.ShowDirsOnly)
            if folderSelection == "" :
                return

        folderSelection = str(self.convertOSsep(folderSelection))

        #verify the existence of syrthes.py, Makefile, and usr_examples folder
        if os.path.isfile(folderSelection + os.sep + "syrthes.py") and os.path.isfile(folderSelection + os.sep + "Makefile") and os.path.isdir(folderSelection + os.sep + "usr_examples") :
            pass
        else:
            QtWidgets.QMessageBox.information(self, 'Message',
                                              "This folder doesn't correspond to any SYRTHES case : syrthes.py file, Makefile file and usr_examples folder are required")
            return

        # windowTitle = New -> not the Welcome dialog box -> propose to save data
        if self.syrthesIHMCollector.DialogNew.windowTitle() == 'New':
            identik = self.SavingCompare()
            if identik == False :
                # suggérer la sauvegarde
                reply = QtWidgets.QMessageBox.Yes # initial value of reply (response of user to following questions) :
                reply = QtWidgets.QMessageBox.question(self, 'Message', "Do you want to save the current data file ?",
                                                       QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No | QtWidgets.QMessageBox.Cancel)
                if reply == QtWidgets.QMessageBox.Yes :
                    self.SavingFile()
                elif reply == QtWidgets.QMessageBox.Cancel :
                    #reset font for the application
                    #self.initSYRTHESFont()
                    return

        self.clearGUI()
        self.case.dirPath = folderSelection
        self.case.name = "untitled.syd" # string.split(lastName, ".")[0] + '_2.syd' # donner automatiquement le nom du fichier data (=name of folder + .syd)

        self.setWindowTitle("SYRTHES V 4.3 - " + self.case.dirPath.split(os.sep)[-1] + " / " + self.case.name)
        self.titleChanged.emit()
##         self.emit(SIGNAL("titleChanged")) # for SALOME

        self.lastDir = self.case.dirPath

        #reset font for the application
        #self.initSYRTHESFont()
        self.show()
        self.syrthesIHMCollector.DialogNew.accept()

    def openData(self):
        #global Main
        returncode = self.OpeningFile()
        if returncode != -1 :
            #if self.case.fullPath != "" :
                #self.show()
                #self.lastDir = self.case.dirPath
            self.syrthesIHMCollector.DialogNew.accept()

class clCase(object):
    def __init__(self, fullPath="", dirPath="", name=""):
        self._fullPath = fullPath
        self._dirPath = dirPath
        self._name = name
        self._fluname = ""
        self._fluFullPath = ""
        self._hisFullPath = []
        self._nbProc = 1

    def _getName(self):
        return self._name

    def _setName(self, aName):
        self._name = aName
        self._fullPath = self._dirPath + os.sep + self._name

    def _getDirPath(self):
        return self._dirPath

    def _setDirPath(self, aDirPath):
        self._dirPath = aDirPath
        self._fullPath = self._dirPath + os.sep + self._name

    def _getFullPath(self):
        return self._fullPath

    def _setFullPath(self, aFullPath):
        self._fullPath = aFullPath
        self._dirPath = str(self._fullPath).rsplit(os.sep, 1)[0]
        if len(str(self._fullPath).rsplit(os.sep,1)) == 2 :
            self._name = str(self._fullPath).rsplit(os.sep, 1)[1]
        else :
            self._name = "error"

    def _getFluname(self):
        return self._fluFullPath

    def _setFluname(self, aFluname):
        self._fluFullPath = aFluname

    def _getFluFullPath(self):
        return self._fluFullPath

    def _setFluFullPath(self, aFluFullPath):
        self._fluFullPath = aFluFullPath
        if len(str(aFluFullPath).rsplit(os.sep,1)) == 2 :
            self._fluname = str(aFluFullPath).rsplit(os.sep, 1)[1]
        else :
            self._fluname = "error"

    def setHisnamePrefix(self, aHisnamePrefix, aNbProc):
        self.setNbProc(aNbProc)

        # constituer le nom d'un (des fichiers) .his
        self._hisFullPath = []
        aHisnamePrefix_split = str(aHisnamePrefix).rsplit(os.sep, 1)
        nameonly = aHisnamePrefix_split[len(aHisnamePrefix_split) - 1]

        if self.getNbProc() > 1: # .../PART/prefix_part0000n.his
            for filei in range(self.getNbProc()) :
                self._hisFullPath.append(self._dirPath + os.sep + 'PART' + os.sep + nameonly + '_' +str(self.getNbProc()).zfill(5)+'part' + str(filei).zfill(5) + '.his')
        else:
            if str(aHisnamePrefix).count("/local") or str(aHisnamePrefix).count("/home") or str(aHisnamePrefix).count("C:") :
                # absolute path
                self._hisFullPath.append(str(aHisnamePrefix)+'.his')
            else :
                self._hisFullPath.append(self._dirPath + os.sep + str(aHisnamePrefix)+'.his')

        _hisFullPath0_split = (self._hisFullPath[0]).rsplit(os.sep, 1)
        self._setFluname(_hisFullPath0_split[0] + os.sep + nameonly + '.flu')
        print("flu", self._getFluname())
            #print "hisname"
            #print self._hisFullPath[0]

    def getHisFullPath(self, index):
        if index < self.getNbProc() :
            return self._hisFullPath[index]
        else :
            return ""

    def setNbProc(self, aNbProc):
        self._nbProc = int(aNbProc)

    def getNbProc(self):
        return self._nbProc

    name = property(_getName, _setName)
    dirPath = property(_getDirPath, _setDirPath)
    fullPath = property(_getFullPath, _setFullPath)
    fluname = property(_getFluname, _setFluname)
    fluFullPath = property(_getFluFullPath, _setFluFullPath)

import threading
class thrFillTable(Thread):
    def __init__(self, table, j):
        Thread.__init__(self)
        self.table = table
        self.j = j

    def run(self):
        rc = self.table.rowCount()
        i = 0
        while i<rc :
            while self.j <= self.table.columnCount()-1 : # other text cells
                item0 = QtWidgets.QTableWidgetItem()
                item0.setText("")
                self.table.setItem(i, self.j, item0)
                self.j+=1


#-------------------------------------------------------------------------------
# Local main program
#-------------------------------------------------------------------------------

if __name__ == "__main__": # instantiation de la classe principale de l'ihm et de celle de la fenêtre de suivi de calcul

    syrthesIHMContext.embedded = False
    syrthesIHMContext.setExeFile(sys.argv[0])

    for i in range(len(sys.argv)) : # recherche du fichier data
        if sys.argv[i] == "-d" and i != len(sys.argv)-1 :
            syrthesIHMContext.setDataFile(sys.argv[i+1])

        if sys.argv[i] == "-a" : # activer le mode avancé
            syrthesIHMContext.advancedModeActivated = True
        else :
            syrthesIHMContext.advancedModeActivated = False

        # recherche du fichier data
        if sys.argv[i] == "-h" or sys.argv[i] == "--help" :
            #MPpath = syrthesIHMContext.getExeAbsDirPath()
            path = os.getenv('SYRTHES4_HOME')+ os.sep + "lib"+ os.sep + "syrthesGui"
            fichelp=open(path + os.sep + "22x22" + os.sep + "help", "r")
            linehelp=fichelp.readline()
            resultat = ""
            while linehelp:
                resultat += linehelp
                linehelp=fichelp.readline()
            print(resultat)
            sys.exit()

    app = QtWidgets.QApplication(sys.argv)

    #set font for the application
    myFont= app.font()
    myFont.setFamily("Sans")
    app.setFont(myFont)

    # Create main window
    Main = MainView()

    sys.exit(app.exec_())
