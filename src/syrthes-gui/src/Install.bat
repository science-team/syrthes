@echo off
rem Modification 17/12/2014 : prise en compte de la version, commande pour generer avec cxfreeze
rem pyuic4 takes a Qt4 user interface description file and compiles it to Python code.
rem

rem
rem generation des fichiers python
rem

echo  .
echo ----------------------------------     
echo - generation des fichiers python
echo ----------------------------------
echo .  

set PYUIC=pyuic5.bat
set PYRCC=pyrcc5.bat

rd /S /Q install
mkdir install

xcopy *.py install\


call %PYUIC% Advanced_mode_form.ui -o install\ui_Advanced_mode_form.py -x
call %PYUIC% Boundary_conditions_cond_form.ui -o install\ui_Boundary_conditions_cond_form.py -x
call %PYUIC% Boundary_conditions_rad_form.ui -o install\ui_Boundary_conditions_rad_form.py -x
call %PYUIC% Boundary_conditions_TPv_form.ui -o install\ui_Boundary_conditions_TPv_form.py -x
call %PYUIC% Boundary_conditions_TPvPt_form.ui -o install\ui_Boundary_conditions_TPvPt_form.py -x
call %PYUIC% Calculation_progress.ui -o install\ui_Calculation_progress.py -x
call %PYUIC% Conjugate_heat_transfer_form.ui -o install\ui_Conjugate_heat_transfer_form.py -x
call %PYUIC% Control_form.ui -o install\ui_Control_form.py -x
call %PYUIC% Fake_form.ui -o install\ui_Fake_form.py -x
call %PYUIC% Filename_form.ui -o install\ui_Filename_form.py -x
call %PYUIC% Home_form.ui -o install\ui_Home_form.py -x
call %PYUIC% Initial_conditions_cond_form.ui -o install\ui_Initial_conditions_cond_form.py -x
call %PYUIC% Initial_conditions_hum_TPv_form.ui -o install\ui_Initial_conditions_hum_TPv_form.py -x
call %PYUIC% Initial_conditions_hum_TPvPt_form.ui -o install\ui_Initial_conditions_hum_TPvPt_form.py -x
call %PYUIC% Material_humidity_properties_form.ui -o install\ui_Material_humidity_properties_form.py -x
call %PYUIC% Material_radiation_properties_form.ui -o install\ui_Material_radiation_properties_form.py -x
call %PYUIC% New_dialog.ui -o install\ui_New_dialog.py -x
call %PYUIC% Output_2D_form.ui -o install\ui_Output_2D_form.py -x
call %PYUIC% Output_3D_form.ui -o install\ui_Output_3D_form.py -x
call %PYUIC% Periodicity_2D_form.ui -o install\ui_Periodicity_2D_form.py -x
call %PYUIC% Periodicity_3D_form.ui -o install\ui_Periodicity_3D_form.py -x
call %PYUIC% Physical_prop_2D_form.ui -o install\ui_Physical_prop_2D_form.py -x
call %PYUIC% Physical_prop_3D_form.ui -o install\ui_Physical_prop_3D_form.py -x
call %PYUIC% Running_options_form.ui -o install\ui_Running_options_form.py -x
call %PYUIC% Solar_aspect_form.ui -o install\ui_Solar_aspect_form.py -x
call %PYUIC% Spectral_parameters_form.ui -o install\ui_Spectral_parameters_form.py -x
call %PYUIC% SyrthesMainwin80060023.ui -o install\ui_SyrthesMainwin80060023.py -x
call %PYUIC% User_C_function_form.ui -o install\ui_User_C_function_form.py -x
call %PYUIC% View_factor_2D_form.ui -o install\ui_View_factor_2D_form.py -x
call %PYUIC% View_factor_3D_form.ui -o install\ui_View_factor_3D_form.py -x
call %PYUIC% Volumetric_conditions_cond_form.ui -o install\ui_Volumetric_conditions_cond_form.py -x
call %PYUIC% Volumetric_conditions_hum_TPv_form.ui -o install\ui_Volumetric_conditions_hum_TPv_form.py -x
call %PYUIC% Volumetric_conditions_hum_TPvPt_form.ui -o install\ui_Volumetric_conditions_hum_TPvPt_form.py -x

call %PYUIC% Boundary_conditions_fluid1d_3D_form.ui  -o  install\ui_Boundary_conditions_fluid1d_3D_form.py -x
call %PYUIC% Control_fluid1d_form.ui   -o  install\ui_Control_fluid1d_form.py -x
call %PYUIC% Geometrie_fluid1d_form.ui  -o  install\ui_Geometrie_fluid1d_form.py -x
call %PYUIC% Initial_conditions_fluid1d_form.ui  -o  install\ui_Initial_conditions_fluid1d_form.py -x
call %PYUIC% Physical_prop_fluid1d_form.ui  -o  install\ui_Physical_prop_fluid1d_form.py -x
call %PYUIC% Volumetric_conditions_fluid1d_form.ui  -o  install\ui_Volumetric_conditions_fluid1d_form.py -x

call %PYUIC% Material_humidity_properties_2D_form.ui -o install\ui_Material_humidity_properties_2D_form.py -x
call %PYUIC% Material_humidity_properties_3D_form.ui -o install\ui_Material_humidity_properties_3D_form.py -x

call %PYUIC% Contact_resistance_humidity_TPv_form.ui  -o install\ui_Contact_resistance_humidity_TPv_form.py -x
call %PYUIC% Contact_resistance_humidity_TPvPt_form.ui -o install\ui_Contact_resistance_humidity_TPvPt_form.py -x

call %PYUIC% Output_Times_form.ui -o install\ui_Output_Times_form.py -x
call %PYUIC% Physical_properties_fluid0d_form.ui -o install\ui_Physical_properties_fluid0d_form.py -x
call %PYUIC% Boundary_conditions_fluid0d_form.ui -o install\ui_Boundary_conditions_fluid0d_form.py -x
call %PYUIC% Geometrie_fluid0d_form.ui  -o  install\ui_Geometrie_fluid0d_form.py -x
call %PYUIC% Volumetric_conditions_fluid0d_form.ui  -o  install\ui_Volumetric_conditions_fluid0d_form.py -x

call %PYRCC% resource.qrc -o install\resource_rc.py

pause -1

mkdir install\22x22
xcopy /S 22x22 install\22x22
mkdir install\ficwhatsthis
xcopy /S ficwhatsthis install\ficwhatsthis

rem Prise en compte de la version dans le fichier version_syrthes
echo . 
echo ----------------------------------------------------------------  
echo - Prise en compte de la version dans le fichier version_syrthes
echo ----------------------------------------------------------------
echo .    

python version_syrthes\changer_version.py

pause -1

rem
rem cxfreeze
rem
echo . 
echo ----------------------------------------------------------------
echo - Etape de creation de l'exacutable de l IHM avec cxfreeze
echo ----------------------------------------------------------------
echo .     

cd install

python setup_cxfl.py build

echo . 
echo ------------------------------------------------------------------------ 
echo - FIN voir excutable dans le repertoire install \ build \ exe.win32-2.5
echo ------------------------------------------------------------------------
echo . 

pause -1
