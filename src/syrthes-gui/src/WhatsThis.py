# -*- coding: utf-8 -*-
import os, sys, string, subprocess, gc
from PyQt5 import QtCore, QtGui
from time import sleep
from syrthesIHMContext import syrthesIHMContext

def WhatsThis(self, parent=None):

#WhatsThis dictionnary
    dic_what={#Mainwindow
                self.action_New_file : "action_New_file", #action de la barre d'outil:action nouveau fichier
                self.action_Open : "action_Open", #action ouvrir
                self.action_Save : "action_Save", #action sauvegarder
                self.action_Quit : "action_Quit", #action quitter
                self.action_Screenshot : "action_Screenshot", #action capture d'écran
                self.action_Calculation_Progress : "action_Calculation_Progress", #action suivis de calcul
                self.action_Run_Syrthes : "action_Run_Syrthes", #action de lancement de Syrthes
                self.action_Stop_Syrthes : "action_Stop_Syrthes", # arrêt de Syrthes
                self.treeWidget : "treeWidget",#arborescence
                #Home_form
                self.syrthesIHMCollector.Home_form.lineEdit_7 : "lineEdit_7",#champ éditable case title
                self.syrthesIHMCollector.Home_form.Ho_Ud_but : "Ho_Ud_but",#bouton use description...
                self.syrthesIHMCollector.Home_form.Dim_Comb : "Dim_Comb",#Combobox de la dimension
                self.syrthesIHMCollector.Home_form.Ho_Tr_ch : "Ho_Tr_ch",#radio bouton pour le rayonnement
                self.syrthesIHMCollector.Home_form.Ho_Hm_ch : "Ho_Hm_ch",#radio bouton pour l'humidité
                self.syrthesIHMCollector.Home_form.Ho_Ch_ch : "Ho_Ch_ch",#radio bouton pour le transfert de chaleur
                self.syrthesIHMCollector.Home_form.Ho_fluid1d_ch : "Ho_fluid1d_ch",#radio bouton pour le module CFD1D
                self.syrthesIHMCollector.Home_form.Ho_fluid0d_ch : "Ho_fluid0d_ch",#radio bouton pour le module CFD0D
                #Control_form
                self.syrthesIHMCollector.Control_form.Ch_res_cal : "Ch_res_cal",#checkbox de la reprise de calcul
                self.syrthesIHMCollector.Control_form.lineEdit_39 : "lineEdit_39",#champ éditable du temp de reprise
                self.syrthesIHMCollector.Control_form.Le_Nts : "Le_Nts",#champ éditable du nombre de pas de temp global
                self.syrthesIHMCollector.Control_form.comb_time_st : "comb_time_st",#combobox du type de pas de temp
                self.syrthesIHMCollector.Control_form.Le_auto_It : "Le_auto_It",#champ éditable du temp initial
                self.syrthesIHMCollector.Control_form.Le_auto_Mt : "Le_auto_Mt",#champ éditable de la variation de température maximum
                self.syrthesIHMCollector.Control_form.Le_auto_Mts : "Le_auto_Mts",#champ éditable du pas de temp maximum
                self.syrthesIHMCollector.Control_form.Le_auto_Mpv : "Le_auto_Mpv",#champ éditable de la variation de pression de vapeur maximum,
                self.syrthesIHMCollector.Control_form.Le_auto_Mpt : "Le_auto_Mpt",#champ éditable de la variation de pression d'air maximum,
                self.syrthesIHMCollector.Control_form.Le_const_Ts : "Le_const_Ts",#champ éditable du pas de temp
                self.syrthesIHMCollector.Control_form.By_Block_table : "By_Block_table",#tableau des pas de temp par block
                self.syrthesIHMCollector.Control_form.lineEdit_43 : "lineEdit_43",#champ éditable de la précision du solver
                self.syrthesIHMCollector.Control_form.Le_Mni : "Le_Mni",#champ éditable du nombre maximum d'itération
                self.syrthesIHMCollector.Control_form.Vap_Sp_le : 'Vap_Sp_le', #champ éditable de la précision du solver
                self.syrthesIHMCollector.Control_form.Vap_Mn_le : 'Vap_Mn_le', #champ éditable du nombre maximum d'itération
                self.syrthesIHMCollector.Control_form.Ap_Sp_le : 'Ap_Sp_le', #champ éditable de la précision du solver
                self.syrthesIHMCollector.Control_form.Ap_Mn_le : 'Ap_Mn_le', #champ éditable du nombre maximum d'itération
                #Boundary_conditions_form
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Heat_ex_table : "Heat_ex_table",#tableau de l'échange de chaleur
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Cont_res_table : "Cont_res_table",#tableau de la réseistance de contact
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Flux_cond_table : "Flux_cond_table",#tableau de la condition du flux
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Diric_cond_table : "Diric_cond_table",#tableau de la condition Dirichlet
                self.syrthesIHMCollector.Boundary_conditions_cond_form.Inf_rad_table : "Inf_rad_table",#tableau de la radiation infinit
                #Physical_properties_2D_form
##                    self.Dens_2D_table : "31",#tableau de la densité
##                    self.Heat_cap_2D_table : "32",#tableau de la capacité calorifique
                self.syrthesIHMCollector.Physical_prop_2D_form.Iso_cond_2D_table : "Iso_cond_2D_table",#tableau de la conductivité isotropic
                self.syrthesIHMCollector.Physical_prop_2D_form.Ort_cond_2D_table : "Ort_cond_2D_table",#tableau de la conductivité orthotropic
                self.syrthesIHMCollector.Physical_prop_2D_form.Ani_cond_2D_table : "Ani_cond_2D_table",#tableau de la conductivité anisotropic
                #Physical_properties_3D_form
##                    self.Dens_3D_table : "36",#tableau de la densité
##                    self.Heat_cap_3D_table : "37",#tableau de la capacité calorifique
                self.syrthesIHMCollector.Physical_prop_3D_form.Iso_cond_3D_table : "Iso_cond_3D_table",#tableau de la conductivité isotropic
                self.syrthesIHMCollector.Physical_prop_3D_form.Ort_cond_3D_table : "Ort_cond_3D_table",#tableau de la conductivité orthotropic
                self.syrthesIHMCollector.Physical_prop_3D_form.Ani_cond_3D_table : "Ani_cond_3D_table",#tableau de la conductivité anisotropic
                #Volumetric_conditions
                self.syrthesIHMCollector.Initial_conditions_cond_form.Init_T_table : "Init_T_table",#tableau de la temperature initial
                self.syrthesIHMCollector.Volumetric_conditions_cond_form.Vol_so_table : "Vol_so_table",#tableau du volume source
                #Periodicity_2D_form
                #self.P2_pr_cb : "43",#checkbox de la periodicté de rotation
                self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_rot_table : "Per_2D_rot_table",#tableau de la periodicité de rotation
                #self.P2_pt_cb : "45",#checkbox de la periodicité de translation
                self.syrthesIHMCollector.Periodicity_2D_form.Per_2D_tra_table : "Per_2D_tra_table",#tableau de la periodicité de translation
                #Periodicity_3D_form
                #self.P3_Pr_cb : "47",#checkbox de la periodicité de rotation
                self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_rot_table : "Per_3D_rot_table",#tableau de la perriodicité de rotation
                #self.P3_Pt_cb : "49",#checkbox de la periodicité de translation
                self.syrthesIHMCollector.Periodicity_3D_form.Per_3D_tra_table : "Per_3D_tra_table",#tableau de la periodicité de translation
                #FileNames_form
                self.syrthesIHMCollector.Filename_form.Fn_Cd_lne : "Fn_Cd_lne",#champ éditable du fichier de conduction
                self.syrthesIHMCollector.Filename_form.Fn_Cd_but : "Fn_Cd_but",#bouton de saisie du fichier de conduction
                self.syrthesIHMCollector.Filename_form.Fn_Rs_lne : "Fn_Rs_lne",#champ éditable du fichier de reprise
                self.syrthesIHMCollector.Filename_form.Fn_Rs_but : "Fn_Rs_but",#bouton de saisie du fichier de reprise
                self.syrthesIHMCollector.Filename_form.Fn_Mt_lne : "Fn_Mt_lne",#champ éditable du fichier météo
                self.syrthesIHMCollector.Filename_form.Fn_Mt_but : "Fn_Mt_but",#bouton de saisie du fichier météo
                self.syrthesIHMCollector.Filename_form.Fn_Rnp_lne : "Fn_Rnp_lne",#champ éditable du préfix de fichier
                self.syrthesIHMCollector.Filename_form.Fn_Rnp_but : "Fn_Rnp_but",#bouton de saisie du préfix de fichier
                self.syrthesIHMCollector.Filename_form.Fn_Rm_lne : "Fn_Rm_lne",#champ éditable du fichier de maillage rayonnement format Syrthes
                self.syrthesIHMCollector.Filename_form.Fn_Rm_but : "Fn_Rm_but",#bouton de saisie du fichier de maillage rayonneemnt format Syrthes
                #Running_options_form
                self.syrthesIHMCollector.Running_options_form.Ro_Ln_le : "Ro_Ln_le",#champ d'édition du nomde listing
                self.syrthesIHMCollector.Running_options_form.Ro_Ln_pb : "Ro_Ln_pb",#bouton de choix de fichier listing
                self.syrthesIHMCollector.Running_options_form.Ro_Cr_cb : "Ro_Cr_cb",#combobox de la conversion de résultat
                self.syrthesIHMCollector.Running_options_form.Ro_Dp_cb : "Ro_Dp_cb",#combobox du positionnement de domaine
                self.syrthesIHMCollector.Running_options_form.Ro_Pre_cb : "Ro_Pre_cb",#combobox de l'utilisation du pre-processing
##                    self.syrthesIHMCollector.Solar_aspect_form.spinBox_4 : "79",#spinbox du nombre de processeur
##                    self.Ro_Pre_cb : "80",#checkbox de l'utilisation d' outils de pre-processing
##                    self.Ro_If_cb : "81",#checkbox de l'utilisation de fonctions interpretées
##                    self.Ro_Ff_cb : "82",#checkbox de l'utilisation de la conversion de format de fichier
##                    self.Ro_Li_cb : "83",#checkbox de l'utilisation du linking
##                    self.Ro_Ex_cb : "84",#checkbox de l'utilisation de l'écxecution
##                    self.Ro_Pos_cb : "85",#checkbox de l'utilisataion d'outil de post-processing
##                    self.Ro_Pos_cmb : "86",#Combobox due l'outil de post-processing à utiliser
                self.syrthesIHMCollector.Running_options_form.Ro_Rs_Pb : "Ro_Rs_Pb",#Bouton des lancement de Syrthes
                self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_cd : "Ro_Np_sb_cd",#Spinbox pour le nombre de processeur en conduction
                self.syrthesIHMCollector.Running_options_form.Ro_Np_sb_ry : "Ro_Np_sb_ry",#Spinbox pour le nombre de processeur en rayonnement
                #Radiation_parameters_form
                self.syrthesIHMCollector.Spectral_parameters_form.Rp_Sb_table : "Rp_Sb_table",#tableau des paramètres de rayonnement
                #Material_properties_form
                self.syrthesIHMCollector.Material_radiation_properties_form.Mrp_table : "Mrp_table",#tableau des paramètres des matériaux en rayonnement
                #Boundary_condition_rad_form
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_lne : "Bcr_Scf_lne",#champ d'édition de la conduction de faces solides
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_lne : "Bcr_Rf_lne",#champ d'édition du rayonnement des faces
                self.syrthesIHMCollector.Boundary_conditions_rad_form.pushButton : "pushButton",#bouton User comments du champ d'édition de la conduction de faces solides
                self.syrthesIHMCollector.Boundary_conditions_rad_form.pushButton_2 : "pushButton_2",#bouton User comments du champ d'édition du rayonnement des faces
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Scf_te : "Bcr_Scf_te",#champ d'édition User comments de la conduction de faces solides
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rf_te : "Bcr_Rf_te",#champ d'édition User comments du rayonnement des faces
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irt_table : "Bcr_Irt_table",#Tableau de la température imposé
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Irf_table : "Bcr_Irf_table",#Tableau du flux imposé
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rpa_chb : "Bcr_Rpa_chb",#checkbox du problème de rayonnement avec ouverture
                self.syrthesIHMCollector.Boundary_conditions_rad_form.Bcr_Rpa_le : "Bcr_Rpa_le",#champ d'édition du problème de rayonnement avec ouverture
                #View factor
##                    self.syrthesIHMCollector.View_factor_3D_form.Vfm_2D_sb : "67",#spinbox du nombre de face de rayonnement 2D
##                    self.syrthesIHMCollector.View_factor_2D_form.Vfm_2D_cmb : "68",#combobox du management des facteurs de forme 2D
                self.syrthesIHMCollector.View_factor_2D_form.Vf_Ip_2D_table : "Vf_Ip_2D_table",#tableau des points internes 2D
                self.syrthesIHMCollector.View_factor_2D_form.Vf_Sy_2D_table : "Vf_Sy_2D_table",#tableau des symétrie 2D
                self.syrthesIHMCollector.View_factor_2D_form.Vf_Pe_2D_table : "Vf_Pe_2D_table",#tableau des périodicités 2D
##                    self.syrthesIHMCollector.View_factor_3D_form.Vfm_3D_sb : "72",#spinbox du nombre de face de rayonnement 3D
##                    self.syrthesIHMCollector.View_factor_3D_form.Vfm_3D_cmb : "75",#combobox du management des facteurs de forme 3D
                self.syrthesIHMCollector.View_factor_3D_form.Vf_Ip_3D_table : "Vf_Ip_3D_table",#tableau des points internes 3D
                self.syrthesIHMCollector.View_factor_3D_form.Vf_Sy_3D_table : "Vf_Sy_3D_table",#tableau des symétrie 3D
                self.syrthesIHMCollector.View_factor_3D_form.Vf_Pe_3D_table : "Vf_Pe_3D_table",#tableau des périodicités 3D
                self.syrthesIHMCollector.View_factor_2D_form.Vfm_2D_cmb : "Vfm_2D_cmb",
                self.syrthesIHMCollector.View_factor_3D_form.Vfm_3D_cmb : "Vfm_3D_cmb",
                #Solar_aspect_form
                self.syrthesIHMCollector.Solar_aspect_form.Sa_cmb : "Sa_cmb",#combobox de la modélisation solaire
                self.syrthesIHMCollector.Solar_aspect_form.spinBox : "spinBox",#spinbox de l'azimuth
                self.syrthesIHMCollector.Solar_aspect_form.spinBox_2 : "spinBox_2",#spinbox de la hauteur
                self.syrthesIHMCollector.Solar_aspect_form.spinBox_3 : "spinBox_3",#spinbox des degrés en latitude
                self.syrthesIHMCollector.Solar_aspect_form.spinBox_4 : "spinBox_4",#spinbox des minutes en latitude
                self.syrthesIHMCollector.Solar_aspect_form.spinBox_5 : "spinBox_5",#spinbox des degrés en longitude
                self.syrthesIHMCollector.Solar_aspect_form.spinBox_6 : "spinBox_6",#spinbox des minutes en longitude
                self.syrthesIHMCollector.Solar_aspect_form.dateTimeEdit : "dateTimeEdit",#date
                #self.syrthesIHMCollector.Solar_aspect_form.Csm_ad_cb : "99",#Repartition automatique du flux solaire constant sur les bandes
                self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_5 : "doubleSpinBox_5",#doublespinbox du coefficient de clareté du ciel
                self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_6 : "doubleSpinBox_6",#doublespinbox du coefficient de clareté du ciel
##                    self.horizontalSlider : "72",#slider nord/sud
##                    self.horizontalSlider_2 : "73",#slider ouest/est
##                    self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_7 : "102",#doublespinbox des references des faces de l'horizon
##                    self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_8 : "103",#doublespinbox de la temperature d'émissivité de l'horizon
##                    self.lineEdit : "104",#champ d'édition de la temperature d'émissivité de l'horizon
##                    self.syrthesIHMCollector.Solar_aspect_form.doubleSpinBox_9 : "105",#doublespinbox du coefficient de la modélisation de l'ombrage
##                    self.lineEdit_2 : "106",#champ d'édition des références du coefficient de la modélisation de l'ombrage
                self.syrthesIHMCollector.Solar_aspect_form.Sa_Sht_table : "Sa_Sht_table",#tableau de l'équilibrage de la chaleur solaire
                self.syrthesIHMCollector.Solar_aspect_form.Sa_Hm_table : "Sa_Hm_table",#tableau de la modélisation de l'horizon
                self.syrthesIHMCollector.Solar_aspect_form.Sa_Sm_table :"Sa_Sm_table",#tableau de la modélisation de l'ombrage
                self.syrthesIHMCollector.Solar_aspect_form.Csm_cmb : "Csm_cmb",
                self.syrthesIHMCollector.Solar_aspect_form.lineEdit : "lineEdit",
                #Humidity_model_form
                self.syrthesIHMCollector.Home_form.Hm_cmb : "Hm_cmb",#combobox du modèle d'humidité
                #Boundary_conditions_TPv_form
                self.syrthesIHMCollector.Boundary_conditions_TPv_form.Bc_TPv_table : "Bc_TPv_table",#tableau des conditions au limite TPv
                #Boundary_conditions_TPvPt_form
                self.syrthesIHMCollector.Boundary_conditions_TPvPt_form.Bc_TPvPt_table : "Bc_TPvPt_table",#tableau des conditions au limite TPvPt
                #Contact_resistance_humidity_TPv_form
                self.syrthesIHMCollector.Contact_resistance_humidity_TPv_form.Cont_res_hum_TPv_table : "Cont_res_hum_TPv_table",#tableau des resistances de contact TPv
                #Contact_resistance_humidity_TPvPt_form
                self.syrthesIHMCollector.Contact_resistance_humidity_TPvPt_form.Cont_res_hum_TPvPt_table : "Cont_res_hum_TPvPt_table",#tableau des resistances de contact TPvPt
                #Volumetric_conditions_hum_TPv_form
                self.syrthesIHMCollector.Initial_conditions_hum_TPv_form.Vch_Ic_TPv_table : "Vch_Ic_TPv_table",#tableau des conditions initial TPv
                self.syrthesIHMCollector.Volumetric_conditions_hum_TPv_form.Vch_St_TPv_table : "Vch_St_TPv_table",#tableau des termes sources TPv
                #Volumetric_conditions_hum_TPvPt_form
                self.syrthesIHMCollector.Initial_conditions_hum_TPvPt_form.Vch_Ic_TPvPt_table : "Vch_Ic_TPvPt_table",#tableau des conditions initial TPvPt
                self.syrthesIHMCollector.Volumetric_conditions_hum_TPvPt_form.Vch_St_TPvPt_table : "Vch_St_TPvPt_table",#tableau des termes sources TPvPt
                #Material_humidity_properties_form
                self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_iso_2D_table : "Mhp_iso_2D_table",#tableau des propriétés des matériaux en humidité
                self.syrthesIHMCollector.Material_humidity_properties_2D_form.Mhp_aniso_2D_table : "Mhp_aniso_2D_table",#tableau des propriétés des matériaux en humidité
                self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_iso_3D_table : "Mhp_iso_3D_table",#tableau des propriétés des matériaux en humidité
                self.syrthesIHMCollector.Material_humidity_properties_3D_form.Mhp_aniso_3D_table : "Mhp_aniso_3D_table",#tableau des propriétés des matériaux en humidité
                #Conjugate heat transfer
                self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Sc_table : "Cht_Sc_table", # surface coupling
                self.syrthesIHMCollector.Conjugate_heat_transfer_form.Cht_Vc_table : "Cht_Vc_table", # volume coupling
##                    self.Cht_Sf_le : "72",#champ d'édition des faces solides
##                    self.Cht_Sf_tb : "75",#bouton user comments des faces solides
##                    self.Cht_Sf_te : "102",# champ dédition de texte des faces solides
##                    self.Cht_Se_le : "103",#champ d'édition des éléments solides
##                    self.Cht_Se_tb : "104",#bouton user comments des éléments solides
##                    self.Cht_Se_te : "105"# champ dédition de texte des éléments solides
                self.syrthesIHMCollector.User_C_function_form.Cfunc_but : "Cfunc_but", # bouton user C function
                self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_but : "Cfunc_cond_but",# bouton user C function pour la conduction
                self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_but : "Cfunc_ray_but",# bouton user C function pour le rayonnement
                self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_but : "Cfunc_hmt_but",# bouton user C function pour la humidité
                self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_but : "Cfunc_fluid1d_but",# bouton user C function pour cfd1d
                self.syrthesIHMCollector.User_C_function_form.Cfunc_other_but : "Cfunc_other_but", # bouton other user C functions
                self.syrthesIHMCollector.User_C_function_form.Cfunc_lne : "Cfunc_lne",
                self.syrthesIHMCollector.User_C_function_form.Cfunc_cond_lne : "Cfunc_cond_lne",
                self.syrthesIHMCollector.User_C_function_form.Cfunc_ray_lne : "Cfunc_ray_lne",
                self.syrthesIHMCollector.User_C_function_form.Cfunc_hmt_lne : "Cfunc_hmt_lne",
                self.syrthesIHMCollector.User_C_function_form.Cfunc_fluid1d_lne : "Cfunc_fluid1d_lne",
                self.syrthesIHMCollector.User_C_function_form.Cfunc_other_lne : "Cfunc_other_lne",
                self.syrthesIHMCollector.User_C_function_form.Cfunc_test_compile : "Cfunc_test_compile",

                self.syrthesIHMCollector.Advanced_mode_form.Advanced_cmd_table : "Advanced_cmd_table", # mode avancé
                # Output
                self.syrthesIHMCollector.Output_2D_form.Tf_cb_2D_Op : "Tf_cb_2D_Op",
                self.syrthesIHMCollector.Output_2D_form.Cb_2D_Op : "Cb_2D_Op",
                self.syrthesIHMCollector.Output_2D_form.Cb2_2D_Op : "Cb2_2D_Op",
                self.syrthesIHMCollector.Output_2D_form.Le_2D_Op : "Le_2D_Op",
                self.syrthesIHMCollector.Output_2D_form.Le2_2D_Op : "Le2_2D_Op",
                self.syrthesIHMCollector.Output_2D_form.Op_Dc_2D_table : "Op_Dc_2D_table",
                self.syrthesIHMCollector.Output_2D_form.Op_Sb_2D_table : "Op_Sb_2D_table",
                self.syrthesIHMCollector.Output_2D_form.Op_Vb_2D_table : "Op_Vb_2D_table",
                self.syrthesIHMCollector.Output_3D_form.Tf_cb_3D_Op : "Tf_cb_3D_Op",
                self.syrthesIHMCollector.Output_3D_form.Cb_3D_Op : "Cb_3D_Op",
                self.syrthesIHMCollector.Output_3D_form.Cb2_3D_Op : "Cb2_3D_Op",
                self.syrthesIHMCollector.Output_3D_form.Le_3D_Op : "Le_3D_Op",
                self.syrthesIHMCollector.Output_3D_form.Le2_3D_Op : "Le2_3D_Op",
                self.syrthesIHMCollector.Output_3D_form.Op_Dc_3D_table : "Op_Dc_3D_table",
                self.syrthesIHMCollector.Output_3D_form.Op_Sb_3D_table : "Op_Sb_3D_table",
                self.syrthesIHMCollector.Output_3D_form.Op_Vb_3D_table : "Op_Vb_3D_table",
                self.syrthesIHMCollector.Output_3D_form.Hf_cb_3D_Op : "Hf_cb_3D_Op",
                self.syrthesIHMCollector.Output_3D_form.Mt_cb_3D_Op : "Mt_cb_3D_Op",
                self.syrthesIHMCollector.Output_3D_form.F_cb_3D_Op : "F_cb_3D_Op",
                self.syrthesIHMCollector.Output_2D_form.Hf_cb_2D_Op : "Hf_cb_2D_Op",
                self.syrthesIHMCollector.Output_2D_form.Mt_cb_2D_Op : "Mt_cb_2D_Op",
                self.syrthesIHMCollector.Output_2D_form.F_cb_2D_Op : "F_cb_2D_Op",
                self.syrthesIHMCollector.Home_form.Ho_Ds_te : "Ho_Ds_te",
                self.syrthesIHMCollector.Solar_aspect_form.Sa_chb : "Sa_chb",
                self.syrthesIHMCollector.Solar_aspect_form.Asm_Lat_cmb : "Asm_Lat_cmb",
                self.syrthesIHMCollector.Solar_aspect_form.Asm_Lng_cmb : "Asm_Lng_cmb",
                self.syrthesIHMCollector.Solar_aspect_form.Csm_Db_table : "Csm_Db_table",

                #fluid1d
                self.syrthesIHMCollector.Initial_conditions_fluid1d_form.Init_TV_table : "Init_TV_table", #tableau des temperature et vitesse initiales
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Heat_ex_table : "Heat_ex_table", #tableau de l'échange de chaleur
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Flux_cond_table : "Flux_cond_table", #tableau de la condition du flux
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Inlet_3D_table : "Bcfluid1d_Inlet_3D_table", #tableau Inlet 3D
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1df_lne : "Bcfluid1d_1df_lne",#champ d'édition du fluide 1D des faces
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_1dfUc_lne : "Bcfluid1d_1dfUc_lne",#champ d'édition User comments du fluide 1D des faces
                self.syrthesIHMCollector.Physical_prop_fluid1d_form.Prop_fluid1d_table : "Prop_fluid1d_table", #tableau des proprietes du fluide
                self.syrthesIHMCollector.Geometrie_fluid1d_form.Geom_fluid1d_table : "Geom_fluid1d_table", # tableau pour la geometrie pour fluid1d
                self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Source_fluid1d_table : "Source_fluid1d_table", # tableau terme source fluid1d
                self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Linear_head_fluid1d_table : "Linear_head_fluid1d_table", # tableau perte de charge fluid1d reguliere
                self.syrthesIHMCollector.Volumetric_conditions_fluid1d_form.Singular_head_fluid1d_table : "Singular_head_fluid1d_table", # tableau perte de charge fluid1d singuliere
                self.syrthesIHMCollector.Control_fluid1d_form.Cb_solid_Ts_fluid1d : "Cb_solid_Ts_fluid1d", #checkbox pour indiquer que le pas de temps FLUIDE 1D est egal au pas de temps solide
                self.syrthesIHMCollector.Control_fluid1d_form.Le_const_Ts_fluid1d : "Le_const_Ts_fluid1d", #champ éditable du pas de temps FLUIDE 1D

                # DH (BT)
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_cl : "Bcfluid1d_Q_table_cl", #champ d'édition de la closed loop
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Q_table_th_cl : "Bcfluid1d_Q_table_th_cl", #champ d'édition de la thermal closed loop
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_table_DP : "Bcfluid1d_table_DP", #champ d'édition de la delta pressure
                self.syrthesIHMCollector.Boundary_conditions_fluid1d_3D_form.Bcfluid1d_Coupling_table : "Bcfluid1d_Coupling_table", #champ d'édition de la faces fluides couplees
                self.syrthesIHMCollector.Filename_form.Fn_fluid1d_but : "Fn_fluid1d_but",#bouton de saisie du fichier de fluid 1d
                self.syrthesIHMCollector.Filename_form.Fn_fluid1d_lne : "Fn_fluid1d_lne",#champ éditable du fichier de fluid 1d
                self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_x : "Le_Gravity_x", #Gravité X
                self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_y : "Le_Gravity_y", #Gravité Y
                self.syrthesIHMCollector.Physical_prop_fluid1d_form.Le_Gravity_z : "Le_Gravity_z", #Gravité Z
                self.syrthesIHMCollector.Boundary_conditions_fluid0d_form.Heat_ex_fluid0d_table : "Heat_ex_fluid0d_table",
                self.syrthesIHMCollector.Physical_properties_fluid0d_form.Prop_fluid0d_table : "Prop_fluid0d_table",
                self.syrthesIHMCollector.Geometrie_fluid0d_form.Geom_fluid0d_table : "Geom_fluid0d_table", # tableau pour la geometrie pour fluid0d
                self.syrthesIHMCollector.Volumetric_conditions_fluid0d_form.Volumetric_conditions_fluid0d_table : "Volumetric_conditions_fluid0d_table" # tableau pour la conditions volumetriques pour fluid0d
                }

    for wid in self.widget:
        wid.setWhatsThis(QtCore.QCoreApplication.translate("MainWindow", self.funcReadWhat(dic_what[wid])))
    pass
    #self.syrthesIHMCollector.User_C_function_form.Cfunc_but.setWhatsThis(QtGui.QApplication.translate("MainWindow", self.funcReadWhat(dic_what[self.syrthesIHMCollector.User_C_function_form.Cfunc_but]), None, QtGui.QApplication.UnicodeUTF8))

def funcReadWhat(self, nomfic):
    #MPpath = syrthesIHMContext.getExeAbsDirPath()
    path = os.getenv('SYRTHES4_HOME')+ os.sep + "lib"+ os.sep + "syrthesGui"
    name = path + os.sep + "ficwhatsthis" + os.sep + nomfic + ".html"
#MP Python3    self.ficwhat=open(name, "r", -1, None, "ignore")
    self.ficwhat=open(path + os.sep + "ficwhatsthis" + os.sep + nomfic + ".html", "r")
    self.linewhat=self.ficwhat.readline()
    resultat = ""
    while self.linewhat:
        resultat += self.linewhat
        self.linewhat=self.ficwhat.readline()

    resultat = resultat.replace("ficwhatsthis/images/", path + os.sep + "ficwhatsthis" + os.sep + "images" + os.sep)
    return resultat

def find_names(self, obj):
    frame = sys._getframe()
    for frame in iter(lambda: frame.f_back, None):
        frame.f_locals
    result = []
    for referrer in gc.get_referrers(obj):
        if isinstance(referrer, dict):
            for k, v in list(referrer.items()):
                if v is obj:
                    result.append(k)
    return result
