This directory contains both the sources and executable versions
of the SYRTHES GUI.

Sources:
=======
Located in directory:
/src

To compile/use these sources on Linux, the pre-requisites are
      Python-3.4.4.tgz,
      sip-4.18.1.tar.gz,
      qt-everywhere-opensource-src-5.7.0.tar.gz,
      PyQt5_gpl-5.7.tar.gz,
      PyQtChart_gpl-5.7.tar.gz,
      cx_Freeze-4.3.4.tar.gz
      future-0.16.0.tar.gz
      numpy-1.9.2.tar.gz

(packages on Windows are slightly different)

Install:
=======
Extract all prerequisites (sources) from tar file to src directory
The src directory must contain following subdirectories:
      Python-3.4.4,
      sip-4.18.1,
      qt-everywhere-opensource-src-5.7.0,
      PyQt5_gpl-5.7,
      PyQtChart_gpl-5.7,
      cx_Freeze-4.3.4
      future-0.16.0
      numpy-1.9.2


Launch build_Syrthes.sh script. It will compile and install all
pre-requisites and Syrthes application.


To test the syrthes-GUI:
=======================
Located in directory
/exe.linux-x86_64

For most users, having compatible systems,
a more practicable and ready to use executable
(compiled on Linux_x86_64-2.6   - calibre7 at EDF)
of the Syrthes-GUI (in which case there is no need to install all the packages)
is available.

To test/launch the syrthes GUI, enter the directory : exe.linux-x86_64
and

> ./SyrthesMain


------------------------------------------
When used interactively with the SYRTHES software (normal usage),
the activation of
the SYRTHES GUI (providing the syrthes.profile has been
sourced) is done through the command :

> syrthes.gui

------------------------------------------

Enjoy using SYRTHES


Contact : syrthes-support@edf.fr





