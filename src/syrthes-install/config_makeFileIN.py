# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------
#
#                         SYRTHES version 4.X.X.
#                         -------------------
#
#     This file is part of the SYRTHES Kernel, element of the
#     thermal code SYRTHES.
#
#     Copyright (C) 2009 EDF S.A., France
#
#     contact: syrthes-support@edf.fr
#
#
#     The SYRTHES Kernel is free software; you can redistribute it
#     and/or modify it under the terms of the GNU General Public License
#     as published by the Free Software Foundation; either version 2 of
#     the License, or (at your option) any later version.
#
#     The SYRTHES Kernel is distributed in the hope that it will be
#     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#
#     You should have received a copy of the GNU General Public License;
#     if not, write to the
#     Free Software Foundation, Inc.,
#     51 Franklin St, Fifth Floor,
#     Boston, MA  02110-1301  USA
#
#-----------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Library modules import
#
#-------------------------------------------------------------------------------

from __future__ import print_function  # for Python 2

import sys

import os, re, os.path, shutil
import time, getpass
#
import platform
#
if platform.system == 'Windows':
    sys.stderr.write("This script only works on Unix-like platforms\n")


#-------------------------------------------------------------------------------
# Global methods
#-------------------------------------------------------------------------------
def main():
    #
    #
    # install
    # -------
    setupmakefilein = Setupmakefilein()


#-------------------------------------------------------------------------------
# Class definition Setupmakefilein
#
#
#-------------------------------------------------------------------------------
class Setupmakefilein(object):

    #----------------------------------------------------------------------------
    #  Fonction configuration_make
    #  Adjust libraries location and environment variables
    #----------------------------------------------------------------------------
    def configuration_make(self, Setupmakefilein, my_arch = None):
        #
        # Makefile.in file update
        #
        makefileinFile = open('Makefile.in', mode='w')

        makefileinFile.write("""SHELL = /bin/sh
# ******************************************************
# ******************************************************
#      Initialisation
# ******************************************************
# ******************************************************
# pour appeler le makefile qui va bien
#(les options se cumulent quand c'est possible)
# make                --> version MPI, sans BLAS, sans CFD  avec DEBUG
# make MPI=no        --> version sequentielle sans MPI
# make BLAS=yes       --> version avec les BLAS
# make CFD=yes        --> version avec couplage CFD
# make DEBUG=no      --> version compilee en +O2
# make PROF=yes       --> version compilee avec -pg
# make MED=no        --> version compilee sans MED
# make PART_METIS=yes --> version compilee avec METIS
# make PART_SCOTCH=yes --> version compilee avec SCOTCH


#--------------
# Default setup
#--------------

BLAS=no
CFD=no
DEBUG=no
PROF=no
PART_METIS=no
PART_SCOTCH=no

""")
        if 'PATH' in self.dicolib['med']:
            makefileinFile.write("MED=yes\n")
        else :
            makefileinFile.write("MED=no\n")

        if 'PATH' in self.dicolib['mpi']:
            makefileinFile.write("MPI=yes\n")
        else :
            makefileinFile.write("MPI=no\n")

        if 'PATH' in self.dicolib['metis']:
            makefileinFile.write("PART_METIS=yes\n")
        else :
            makefileinFile.write("PART_METIS=no\n")

        if 'PATH' in self.dicolib['scotch']:
            makefileinFile.write("PART_SCOTCH=yes\n")
        else :
            makefileinFile.write("PART_SCOTCH=no\n")

        if 'PATH' in self.dicospeci['specific_inc']:
            makefileinFile.write("INC += "+self.dicospeci['specific_inc']['PATH']+"\n")
        else :
            makefileinFile.write("INC=\n")

        if 'PATH' in self.dicospeci['specific_lib']:
            makefileinFile.write("LIB += "+self.dicospeci['specific_lib']['PATH']+"\n")
        else :
            makefileinFile.write("LIB=\n")

        if 'PATH' in self.dicospeci['specific_debug']:
            makefileinFile.write("DEBUG=yes\n")
            makefileinFile.write("COPTIM= "+self.dicospeci['specific_debug']['PATH']+"\n")
        else:
            makefileinFile.write("COPTIM= -O3\n")

        makefileinFile.write("# Sources and build directories\n")
        makefileinFile.write("DIR_SYRTHES=%s"%self.syrthesDir+"\n")
        makefileinFile.write("BUILD_SYRTHES=%s"%self.syrthesDir+"/arch/"+self.arch+"\n")

        makefileinFile.write("""
#-----------------
# Building options
#-----------------

CFLAG = -D _FILE_OFFSET_BITS=64 $(CFLAGS)
LDFLAG= -D _FILE_OFFSET_BITS=64 $(LDFLAGS)

ifeq ($(MPI),yes)
        CFLAG  += -D_SYRTHES_MPI_
        LDFLAG += -D_SYRTHES_MPI_
endif

ifeq ($(BLAS),yes)
        CFLAG += -D_SYRTHES_BLAS_
endif

ifeq ($(CFD),yes)
        CFLAG += -D_SYRTHES_CFD_
endif

ifeq ($(PROF),yes)
        COPTIM = -pg
endif

""")
        if 'PATH' in self.dicospeci['specific_option']:
            makefileinFile.write("CFLAG += "+self.dicospeci['specific_option']['PATH']+"\n")

        makefileinFile.write("""

#-----------------------------------
# Names of libraries and executables
#-----------------------------------

NAME_PPSYR=syrthes-pp
NAME_POSTSYR=syrthes-post
NAME_PPFUNCSYR=syrthes-ppfunc

NAME_CONVERT=convert2syrthes4
NAME_SYRTHES2ENSIGHT=syrthes4ensight
NAME_SYRTHES2MED=syrthes4med30

NAMELIB = libsyrthes_mpi.a

ifeq ($(MPI),no)
        NAMELIB = libsyrthes_seq.a
endif

ifeq ($(CFD),yes)
        NAMELIB = libsyrthes_cfd.a
endif

ifeq ($(DEBUG),yes)
        NAMELIB = libsyrthes_mpi.a
        ifeq ($(MPI),no)
                NAMELIB = libsyrthes_seq.a
        endif
        ifeq ($(CFD),yes)
                NAMELIB = libsyrthes_cfd.a
        endif
endif

LIB_SYRTHES = $(NAMELIB)


# ******************************************************
# ******************************************************
#      SYSTEM DEPENDENT OPTIONS
# ******************************************************
# ******************************************************

""")



##  Compiler
        makefileinFile.write("# Compiler path for gcc\n")
        makefileinFile.write("FRONT_CC="+self.comp+"\n")

        p_mpicc = None
        if self.dicolib['mpi']['USE'].upper()=='YES':
            makefileinFile.write("# Compiler path for mpicc\n")
            for s in ('/', '/bin/', '/bin64/'):
                p1 = self.dicolib['mpi']['PATH'] + s + self.wrapperc
                if os.path.isfile(p1):
                    p_mpicc = p1
                    break

        if p_mpicc:
            makefileinFile.write("CC="+p_mpicc+"\n")
        else:
            makefileinFile.write("CC="+self.comp+"\n")

##  Compiler  CC whith out mpi
        makefileinFile.write("ifeq ($(MPI),no)\n")
        makefileinFile.write("        CC="+self.comp+"\n")
        makefileinFile.write("endif\n"+"\n")

##  Library
        if  'PATH' in self.dicolib['metis']:
             makefileinFile.write("DIR_METIS="+self.dicolib['metis']['PATH']+"\n")

        if  'PATH' in self.dicolib['scotch']:
             makefileinFile.write("DIR_SCOTCH="+self.dicolib['scotch']['PATH']+"\n")

        if  'PATH' in self.dicolib['med']:
             makefileinFile.write("DIR_MED="+self.dicolib['med']['PATH']+"\n")

        if  'PATH' in self.dicolib['hdf5']:
             makefileinFile.write("DIR_HDF5="+self.dicolib['hdf5']['PATH']+"\n")

        makefileinFile.write("\nifeq ($(CFD),yes)\n")
        if 'PATH' in self.dicolib['ple']:
             makefileinFile.write("       DIR_PLE="+self.dicolib['ple']['PATH']+"\n")
        makefileinFile.write("endif\n")

        makefileinFile.write("\nifeq ($(BLAS),yes)\n")
        if  'PATH' in self.dicolib['blas']:
             makefileinFile.write("       DIR_BLAS="+self.dicolib['blas']['PATH']+"\n")
        makefileinFile.write("endif\n"+"\n")


        makefileinFile.write("""
#
# ******************************************************
# ******************************************************
#      LOCAL DIRECTORIES
# ******************************************************
# ******************************************************
# kernel library directory, MAkefile.in path, material library
DIRLIB_KERNEL =$(BUILD_SYRTHES)/lib
MAKEINC = $(DIR_SYRTHES)
INC_SYRTHES_LIBMAT = -I$(DIR_SYRTHES)/src/syrthes-kernel/lib_material_syrthes

# for choosing your own SYRTHES libraries :
#DIRLIB_KERNEL =/home/foo/mylib
#MAKEINC = /home/foo/mymake
#INC_SYRTHES_LIBMAT = -I/home/foo/mymateriallib

#.....................................................................


# create build directory if needed
$(shell mkdir -p $(BUILDDIR))


INC_SYRTHES           = -I$(DIR_SYRTHES)/src/syrthes-kernel/include
SRC_SYRTHES           = $(DIR_SYRTHES)/src/syrthes-kernel/src

INC_PPSYR= -I$(DIR_SYRTHES)/src/syrthes-pp/include
INC_PPFUNCSYR= -I$(DIR_SYRTHES)/src/syrthes-ppfunc/include
INC_POSTSYR= -I$(DIR_SYRTHES)/src/syrthes-post/include

LIB_SYRTHES=   $(DIRLIB_KERNEL)/$(NAMELIB)
PPSYR=         $(BUILD_SYRTHES)/bin/$(NAME_PPSYR)
PPFUNCSYR=     $(BUILD_SYRTHES)/bin/$(NAME_PPFUNCSYR)
POSTSYR=       $(BUILD_SYRTHES)/bin/$(NAME_POSTSYR)

SYRENSIGHT=    $(BUILD_SYRTHES)/bin/$(NAME_SYRTHES2ENSIGHT)
CONVERTSYR=    $(BUILD_SYRTHES)/bin/$(NAME_CONVERT)
SYRREFINE=     $(BUILD_SYRTHES)/bin/$(NAME_SYRTHESREFINE)
CONVERTMED=    $(BUILD_SYRTHES)/bin/$(NAME_SYRTHES2MED)

# med
LIBMED=-L $(DIR_MED)/lib -L$(DIR_HDF5)/lib -lmedC -lhdf5 -L.
INC_MED= -I$(DIR_MED)/include -I$(DIR_HDF5)/include

# ensight
LIBENSIGHT=-lm -lz -lpthread

ifeq ($(CFD),yes)
        INC += -I$(DIR_PLE)/include
        INC += -I$(DIR_SYRTHES)/src/syrthes-kernel/include
        LIB_PLE = -L$(DIR_PLE)/lib -lple -Wl,-rpath -Wl,$(DIR_PLE)/lib
endif

ifeq ($(BLAS),yes)
        LDFLAG += -lcblas -latlas
        INC += -I$(DIR_BLAS)/??? isa
endif
""")

        # If metis or scotch are used
        # ---------------------------
        if  'PATH' in self.dicolib['metis']:

            makefileinFile.write("LIBMETIS=-L $(DIR_METIS)/lib -lmetis\n")
            makefileinFile.write("INC_METIS=-I$(DIR_METIS)/include\n")

        if  'PATH' in self.dicolib['scotch']:
            makefileinFile.write("LIBSCOTCH=-L $(DIR_SCOTCH)/lib -lscotch -L $(DIR_SCOTCH)/lib -lscotcherr\n")
            makefileinFile.write("INC_SCOTCH=-I$(DIR_SCOTCH)/include\n")

        makefileinFile.close()

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#           second fichier
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        makefileinFile = open('Makefile2.in', mode='w')

        makefileinFile.write("""SHELL = /bin/sh
# ******************************************************
# ******************************************************
#      Initialisation
# ******************************************************
# ******************************************************
# pour appeler le makefile qui va bien
#(les options se cumulent quand c'est possible)
# make                --> version MPI, sans BLAS, sans CFD  avec DEBUG
# make MPI=no        --> version sequentielle sans MPI
# make BLAS=yes       --> version avec les BLAS
# make CFD=yes        --> version avec couplage CFD
# make DEBUG=no      --> version compilee en +O2
# make PROF=yes       --> version compilee avec -pg
# make MED=no        --> versin compilee sans MED
# make PART_METIS=yes --> version compilee avec METIS
# make PART_SCOTCH=yes --> version compilee avec SCOTCH


#--------------
# Default setup
#--------------

BLAS=no
CFD=no
DEBUG=no
PROF=no
PART_METIS=no
PART_SCOTCH=no
""")
        if 'PATH' in self.dicolib['med']:
              makefileinFile.write("MED=yes\n")
        else :
              makefileinFile.write("MED=no\n")

        if 'PATH' in self.dicolib['mpi']:
              makefileinFile.write("MPI=yes\n")
        else :
              makefileinFile.write("MPI=no\n")

        if 'PATH' in self.dicolib['metis']:
            makefileinFile.write("PART_METIS=yes\n")
        else :
            makefileinFile.write("PART_METIS=no\n")

        if 'PATH' in self.dicolib['scotch']:
            makefileinFile.write("PART_SCOTCH=yes\n")
        else :
            makefileinFile.write("PART_SCOTCH=no\n")

        if 'PATH' in self.dicospeci['specific_inc']:
              makefileinFile.write("INC += -I"+self.dicospeci['specific_inc']['PATH']+"\n")
        else :
              makefileinFile.write("INC=\n")

        if 'PATH' in self.dicospeci['specific_lib']:
              makefileinFile.write("LIB += -L"+self.dicospeci['specific_lib']['PATH']+"\n")
        else :
              makefileinFile.write("LIB=\n")

        if 'PATH' in self.dicospeci['specific_debug']:
            makefileinFile.write("DEBUG=yes\n")
            makefileinFile.write("COPTIM= "+self.dicospeci['specific_debug']['PATH']+"\n")
        else:
            makefileinFile.write("COPTIM= -O3\n")

        makefileinFile.write("# Sources and build directories\n")
        makefileinFile.write("BUILD_SYRTHES=%s"%self.syrthesDir+"/arch/"+self.arch+"\n")

        makefileinFile.write("""
#-----------------
# Building options
#-----------------

CFLAG = -D _FILE_OFFSET_BITS=64 $(CFLAGS)
LDFLAG= -D _FILE_OFFSET_BITS=64 $(LDFLAGS)

ifeq ($(MPI),yes)
        CFLAG  += -D_SYRTHES_MPI_
        LDFLAG += -D_SYRTHES_MPI_
endif

ifeq ($(BLAS),yes)
        CFLAG += -D_SYRTHES_BLAS_
endif

ifeq ($(CFD),yes)
        CFLAG += -D_SYRTHES_CFD_
endif

ifeq ($(PROF),yes)
        COPTIM = -pg
endif


""")
        if 'PATH' in self.dicospeci['specific_option']:
            makefileinFile.write("CFLAG += "+self.dicospeci['specific_option']['PATH']+"\n")

        makefileinFile.write("""


#-----------------------------------
# Names of libraries and executables
#-----------------------------------

NAMELIB = libsyrthes_mpi.a

ifeq ($(MPI),no)
        NAMELIB = libsyrthes_seq.a
endif

ifeq ($(CFD),yes)
        NAMELIB = libsyrthes_cfd.a
endif

ifeq ($(DEBUG),yes)
        NAMELIB = libsyrthes_mpi.a
        ifeq ($(MPI),no)
                NAMELIB = libsyrthes_seq.a
        endif
        ifeq ($(CFD),yes)
                NAMELIB = libsyrthes_cfd.a
        endif
endif

LIB_SYRTHES = $(NAMELIB)


# ******************************************************
# ******************************************************
#      SYSTEM DEPENDENT OPTIONS
# ******************************************************
# ******************************************************

""")


##  Compiler
        makefileinFile.write("# Compiler path for gcc\n")
        makefileinFile.write("FRONT_CC="+self.comp+"\n")

        p_mpicc = None
        if self.dicolib['mpi']['USE'].upper()=='YES':
            makefileinFile.write("# Compiler path for mpicc\n")
            for s in ('/', '/bin/', '/bin64/'):
                p1 = self.dicolib['mpi']['PATH'] + s + self.wrapperc
                if os.path.isfile(p1):
                    p_mpicc = p1
                    break

        if p_mpicc:
            makefileinFile.write("# Compiler path for mpicc\n")
            makefileinFile.write("CC="+p_mpicc+"\n")
        else:
            makefileinFile.write("CC="+self.comp+"\n")

##  Compiler  CC whith out mpi
        makefileinFile.write("ifeq ($(MPI),no)\n")
        makefileinFile.write("        CC="+self.comp+"\n")
        makefileinFile.write("endif\n"+"\n")

##  Library
        makefileinFile.write("\nifeq ($(CFD),yes)\n")
        if  'PATH' in self.dicolib['ple']:
             makefileinFile.write("       DIR_PLE="+self.dicolib['ple']['PATH']+"\n")
        makefileinFile.write("endif\n")

        makefileinFile.write("\nifeq ($(BLAS),yes)\n")
        if  'PATH' in self.dicolib['blas']:
             makefileinFile.write("       DIR_BLAS="+self.dicolib['blas']['PATH']+"\n")
        makefileinFile.write("endif\n"+"\n")


        makefileinFile.write("""
# ******************************************************
# ******************************************************
#      LOCAL DIRECTORIES
# ******************************************************
# ******************************************************

# kernel library directory, MAkefile.in path, material library
DIRLIB_KERNEL =$(BUILD_SYRTHES)/lib
MAKEINC = $(DIR_SYRTHES)
INC_SYRTHES_LIBMAT    = -I$(BUILD_SYRTHES)/include

# for choosing your own SYRTHES libraries :
#DIRLIB_KERNEL =/home/foo/mylib
#MAKEINC = /home/foo/mymake
#INC_SYRTHES_LIBMAT = -I/home/foo/mymateriallib


# kernel library directory
DIRLIB_KERNEL =$(BUILD_SYRTHES)/lib
# for choosing your own library : DIRLIB_KERNEL = /home/myhome/w/mylib

# path for include
MAKEINC = $(DIR_SYRTHES)/include
# example :  MAKEINC = /home/myhome/w/myinclude

# Name of the material propreties directory
INC_SYRTHES_LIBMAT    = -I$(BUILD_SYRTHES)/lib_material_syrthes
# for choosing your own library : INC_SYRTHES_LIBMAT    = -I/home/myhome/w/mylib_material
#.....................................................................

INC_SYRTHES= -I$(BUILD_SYRTHES)/include
LIB_SYRTHES=$(DIRLIB_KERNEL)/$(NAMELIB)

ifeq ($(CFD),yes)
        INC += -I$(DIR_PLE)/include
        INC += -I$(BUILD_SYRTHES)/include
        LIB_PLE = -L$(DIR_PLE)/lib -lple -Wl,-rpath -Wl,$(DIR_PLE)/lib
endif

ifeq ($(BLAS),yes)
        LDFLAG += -lcblas -latlas
        INC += -I$(DIR_BLAS)/??? isa
endif
""")
        makefileinFile.close()


#-------------------------------------------------------------------------------
#
# Progam principal
#
#
#
#-------------------------------------------------------------------------------
if __name__ == '__main__':
        main()
