/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#ifndef _USERTYPE_H
#define _USERTYPE_H


/*  type of langage */
/*  0 = french      */
/*  1 = english     */

#define SYRTHES_LANG 1


/* ------------------------------------------- */

#define FR 0
#define EN 1

/* ------------------------------------------- */



#define SYRTHES_TYPEWIDTH 32

#if SYRTHES_TYPEWIDTH == 32
  typedef int rp_int;
/* typedef int32_t pp_int; */
#elif SYRTHES_TYPEWIDTH == 64
  typedef int64_t rp_int;
#else
  #error "Incorrect user-supplied value for SYRTHES_TYPEWIDTH"
#endif


#endif

