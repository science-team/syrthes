/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#ifndef _BD_H
#define _BD_H



#include "syr_const.h"

/* adresses des variables pour les cond limites */
#define ADR_T 0
#define ADR_PV 1
#define ADR_PT 2

struct Variable
{
  int nbvar;   /* nombre de variables physiques a calculer (1 en conduction ou 3 en hmt) */
  char ** nomvar;

  int nbadr;   /* nombre d'adresses necessaires pour strocker tous les champs            */
  int adr_t,  adr_t_m1,  adr_t_m2;  
  int adr_pv, adr_pv_m1, adr_pv_m2;  
  int adr_pt, adr_pt_m1, adr_pt_m2;

  int adr_tmin, adr_tmax;
  int adr_pvmin, adr_pvmax;
  int adr_ptmin, adr_ptmax;


  double **var;
};


struct Clim
{
  int nbval;
  int npoin,nelem,ndmat,nbarete;  /* cond[nbface,ndmass] */
  int *numf,*arete;
  int *nump;
  double **val1;
  double **val2;
  double *xdms; /*uniquement pour les dirichlets */
};

struct Cvol
{
  int existglob;
  int nbval;
  int nelem;  /* cond[nbele,nb_val] */
  int *nume;
  double *val1;
  double *val2;
};

struct Cperio
{
  int existglob;
  int nelem,ndmat,npoin; 
  int *nump,*numpc;
  double *trav;
};


struct Contact
{
  int existglob;
  int nelem,npoin,ndmat;
  int *numf,**node;   /* numf = numero de la face de bord dans nodebord */
  int *nump,*numpc;   /* num glob du noeud RC, num glob du corresp       */
  double ***g;        /* g[numvar][ndmat][nelem] */
  double *gassc,*gnonassc; /* resistance de contact avec et sans assemblage complementaire */
  double *trav;       /* tableau de travil pour le grad.conj. */
};

struct Couple
{
  int nelem,ndmat;
  int *numf,*numcor;
  double *dist;               /* distance du correspondant (utile en parallele) */
  double **bary,**h,**t,*tf;
};

struct Climcfd
{
  int exist,existglob;
  int ndmat;
  int *nelem;  /* nbre de faces solides couplees par code fluide */
  int **nume;  /* liste des faces solides couplees (dans nodeus) pour chaque code fluide */
  double **tfluid,**hfluid;
};

struct Mst
{
  int nelem,nbfbr,nbfbs,nbfbord,nface;
  int listref[MAX_REF];
  int *nume,**face,**nodeb,**nfabor,*refbord;
  int nbray,*elttri;
  double *eps,*lf,*lbn,*lbnm1,*divflux,*Jnm1,*Jn,*tc,*tb;
  double ***xnora,*surfb;
  double *wm,*mu,*ksi,*eta;
  double omega,beta; /* beta=k+sigma=extinction, omega=sigma/beta=albedo */
};

/*======== Proprietes physiques ========*/
/*======================================*/
struct Iso
{
  int nelem;
  int *ele;
  double *k;
};

struct Ortho 
{
  int nelem;
  int *ele;
  double *k11,*k22,*k33;
};

struct Aniso 
{
  int nelem;
  int *ele;
  double *k11,*k22,*k33;
  double *k12,*k13,*k23;
};

struct Prophy
{                        
  int ndmat,nelem;
  int isotro;     /* isotro=1 s'il y a des elts isotro sur une des partitions */
  int orthotro;   /* orthotro=1 s'il y a des elts isotro sur une des partitions */
  int anisotro;   /* anisotro=1 s'il y a des elts isotro sur une des partitions */
  int indicrhocpcst;     /* rhocpcst=1 si constant partout, 0 sinon */
  int indicrhocpvar;     /* rhocpvar=1 si variable partout, 0 sinon */
  int kcst;         /* kcst=1 si constant partout, 0 sinon */                
  int kvar;         /* kvar=1 si variable partout, 0 sinon */                
  double *rho,*cp;
  struct Iso kiso;
  struct Ortho kortho;
  struct Aniso kaniso;
};


/*======================================*/
/*===     MAILLAGE    ==================*/
/*======================================*/



struct Maillage
{
  int ndim,ndiele,nelem,npoin,ndmat,nbface;
  int ncoema,iaxisy;
  double **coord,*volume,**xnf;
  int **node;
  int *nref,*nrefe,*type;
  int **nvoisin; /* seulement pour les mst */
  int nbarete,**arete, **eltarete;   /* seulement pour maillnodes */
};


struct MaillageBord
{
  int ndim,ndiele,nelem,ndmat;
  double *volume;
  int **node;
  int *nrefe,**type;  /* type[nelem][MAXPOS] */
};


struct MaillageCL
{
  int ndim,ndiele,nelem,ndmat,nbface,iaxisy;
  double *volume;
  int **node;
  int *nrefe,**type;
};

struct BoutArete
{
  int num,numloc;
  struct BoutArete *suivant;
};

struct Matrice
{
  double *b,*dmat,**wct,**xmat,*xdms,*ddiff;
};


struct Histo
{
  int actif,npoin,npoincoo,npoinref;
  double freq;
  int freq_nt,freq_list_nb;
  double *freq_list_s; 
  int *nump;                   /* nump[npoin] liste des noeuds definis par leur numer */
  int *numpr;                  /* numpr[npoinref] liste des noeuds recherches par reference */
  int *numev;                  /* numev[npoincoo] liste des elts dans lequel se trouvent les histo par coord */
  double **coord;              /* coord[ndim][npoincoo] coordonnees des historiques */
  double **bary;               /* bary[ndim+1][npoincoo]  coordonnees barycentriques des historiques */
};

/*======== gestion du pas de temps ========*/
/*=========================================*/
struct DtAuto
{
  int actif;
  double deltaT,deltaPv,deltaPt,dtlimit;
};

struct DtMult
{
  int actif,nb;
  int nbiter[100];
  double dt[100];
};
struct PasDeTemps
{
  int ntsyr,nbpaso,nbparay;
  int ntsmax,ldern,lstops;
  int premier,suiteprem,suite;
  double rdtts,rdttsprec,tempss;
  double new_t_init;
  struct DtAuto dtauto;
  struct DtMult dtmult;
};
/*=========================================*/

struct SymPer
{
  int nbsym,nbper,nbsection;
  double sym[10][7];
  double per[10][7];
};

/*============================================================*/
/*========            RAYONNEMENT                     ========*/
/*============================================================*/

struct Compconnexe
{
  int nb,nbcomp;
  double x[100],y[100],z[100];
};

struct FacForme
{
  double fdf;      /* valeur du fdf */
  int numenface;   /* numero de la facette en vis-a-vis */
  struct FacForme *suivant;
};


/*======== Proprietes physiques rayonnement ========*/
struct Bande
{
  int nb;
  double borneinf[100];
  double bornesup[100];
};

struct ProphyRay
{           
  int nelem;
  double **emissi,**transm,**reflec,**absorb;
  struct Bande bandespec;

};


struct PropInfini
{
  int actif,nb;
  double temp;
  double emi[100],*fdfnp1;
};

struct MatriceRay
{
  double *x,*b,*xm1,*gd,*res,*z,*di,*resm1;
};

/*======== Solaire ==============================*/

struct Lieu
{
  int latituded,latitudem;
  int longituded,longitudem;
  int fuseau,decalage,vertical,hauteur;
  int mois,jour,heure,minute,seconde,day;
  int mois_i,jour_i,heure_i,minute_i,day_i;
  double gama;
  double xc,yc,zc;
};

struct Soleil
{
  int actif,anglect_actif;
  double h,a,x,y,z;
  double Rterre,sphere;
  double A,B,C,I0;
  double *fluxct,anglect,azict,**direct,**diffus;
  double **emissi,**reflec,**absorb,**transm;
  int autorepartct;
};


struct Mask
{
  int nelem,ndmat;
  int **node,*ref;
  double *opacite;
};


struct Horizon
{
  int actif,nelem,ndmat;
  int **node,*ref;
  double *fdf,temp,emissi;
};

struct Vitre
{
  int actif,nelem,prop_glob;
  double **refrac;
  int *voisin,*voisvol,*voisloc;
};

struct Meteo
{
  int actif,nbvar,nelem;
  double **fdirect,**fdiffus;
  double **var;
};

struct Myfile
{
  int actif,nbvar,nelem;
  double **var;
};

struct Bilan
{
  int nb;
  int nbref[MAX_BIL];
  int *ref[MAX_BIL];
};



struct Liste_entier
{
  int num;
  struct Liste_entier *suivant;
};


struct Travail
{
  int nbtab;
  int *nlong;
  double **tab;
  int nbitab;
  int *ilong;
  int **itab;
};

struct Performances
{
  double mem_max;
  double mem_cond,mem_cond_max;
  double mem_ray,mem_ray_max;
  double cpu_deb,cpu_lect,cpu_corr_ray;
  double cpu_init_cond,cpu_init_ray;
  double cpu_init_soleil,cpu_init_tot;
  double cpu_iter_soleil,cpu_iter_ray,cpu_iter_cond;
  double cpu_1iter_soleil,cpu_1iter_ray,cpu_1iter_cond;
  double cpu_tot;
};


struct Affichages
{
  int cond_mat,cond_creemaill,cond_prophy,cond_clim;
  int cond_perio,cond_rescon;
  int ray_mem_devel,ray_mem_user,ray_resray;
  int ray_fi2teq,ray_orient,ray_fdf,ray_soleil,ray_extrbord;
  int correspFS,correspSR,passageSR,passageSF;
  int mst,maill_parall,cfd;
};

struct GestionFichiers
{
  int freq_chrono;
  int freq_chrono_nt,freq_chrono_list_nb;
  double freq_chrono_s;
  double *freq_chrono_list_s;
  int champmax,champflux,suppchampfinal;
  int resu_r;
  int stock_corr_sr,lec_corr_sr;
  int stock_fdf,lec_fdf;
};





#endif
