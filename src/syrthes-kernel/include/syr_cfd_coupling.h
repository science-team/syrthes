/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#ifndef _SYR_CFD_COUPLING_H_
#define _SYR_CFD_COUPLING_H_

/*============================================================================
 * Main API functions for coupling between Syrthes and Code_Saturne
 *
 * Library: Code_Saturne                                    Copyright EDF 2008
 *============================================================================*/


/*----------------------------------------------------------------------------
 * System and BFT headers
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Local headers
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Structure definitions
 *----------------------------------------------------------------------------*/

typedef enum {

  SYR_NO_COUPLING,
  SYR_SURF_COUPLING,
  SYR_VOL_COUPLING,
  SYR_SURFVOL_COUPLING

} syr_cfd_coupling_type_t;

typedef struct _syr_cfd_coupling_t syr_cfd_coupling_t;

/*============================================================================
 * Public function prototypes
 *============================================================================*/
#if defined(_SYRTHES_CFD_)


#if defined(_SYRTHES_MPI_)

/*----------------------------------------------------------------------------
 * Initialize CFD coupling.
 *
 * Discover other applications in the same MPI root communicator.
 *
 * parameters:
 *   syrthes_app_name <-- optional name of this SYRTHES instance, or NULL.
 *   syrthes_comm     <-- communicator for this SYRTHES application
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_mpi_init(const char  *syrthes_app_name,
                          MPI_Comm     syrthes_comm);

#endif /* defined(_SYRTHES_MPI_) */

/*----------------------------------------------------------------------------
 * Finalize MPI coupling helper structures.
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_finalize(void);

/*----------------------------------------------------------------------------
 * Create syr_cfd_coupling_t structure
 *
 * Couplings are possible either with completely separate process groups
 * from the main group (Syrthes communicator), or with this same group.
 *
 * arguments:
 *   dim        <-- Spatial dimension
 *   app_name   <-- Optional application name, or NULL
 *   coupl_dim  <-- coupl_dim[0] indicates surface coupling,
 *                  coupl_dim[1] indicates volume coupling
 *
 * returns:
 *   pointer to new coupling structure
 *----------------------------------------------------------------------------*/

syr_cfd_coupling_t *
syr_cfd_coupling_create(int                       dim,
                        const char               *app_name,
			syr_cfd_coupling_type_t   type);

/*---------------------------------------------------------------------------
 * Finalize syr_cfd_coupling_t structure
 *
 * arguments:
 *   coupling <-- Pointer to coupling structure pointer
 *---------------------------------------------------------------------------*/

void
syr_cfd_coupling_destroy(syr_cfd_coupling_t **coupling);

/*----------------------------------------------------------------------------
 * Initialize CFD coupling communication using MPI.
 *
 * This function may be called once all couplings have been defined,
 * and it will match defined couplings with available applications.
 *
 * arguments:
 *   n_coupling <-- number of CFD couplings
 *   couplings  <-- array of CFD couplings
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_init_comm(int                   n_couplings,
                           syr_cfd_coupling_t  **couplings);

/*----------------------------------------------------------------------------
 * Get CFD coupling type.
 *
 * arguments:
 *   coupl  <-- CFD coupling
 *
 * returns:
 *   type related to a syr_cfd_coupling_t structure
 *----------------------------------------------------------------------------*/

syr_cfd_coupling_type_t
syr_cfd_coupling_get_type(syr_cfd_coupling_t  *coupl);

/*----------------------------------------------------------------------------
 * Assign lists of cells associated with a coupling.
 *
 * Local boundary cells are used as a location basis for values of distant
 * "coupled" cells, and their vertices are located relative to the
 * corresponding distant supports.
 *
 * arguments:
 *   coupling        <-- pointer to coupling structure pointer
 *   n_coupled_cells <-- number of coupled cells
 *   coupled_cells   <-- list of coupled cells (indirection, 0 to n-1)
 *   cell_vertices   <-- cell vertex ids (0 to n-1)
 *   vertex_coords   <-- vertex coordinates ([dim][n_points], 0 to n-1)
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_set_cells(syr_cfd_coupling_t  *coupl,
                           int                  n_coupled_cells,
                           const int            coupled_cells[],
                           const int          **cell_vertices,
                           const double       **vertex_coords);

/*----------------------------------------------------------------------------
 * Assign lists of faces associated with a coupling.
 *
 * Local boundary faces are used as a location basis for values of distant
 * "coupled" faces, and their vertices are located relative to the
 * corresponding distant supports.
 *
 * arguments:
 *   coupling        <-- pointer to coupling structure pointer
 *   n_coupled_faces <-- number of coupled faces
 *   coupled_faces   <-- list of coupled faces (indirection, 0 to n-1)
 *   face_vertices   <-- face vertex ids (0 to n-1)
 *   vertex_coords   <-- vertex coordinates ([dim][n_points], 0 to n-1)
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_set_faces(syr_cfd_coupling_t  *coupl,
                           int                  n_coupled_faces,
                           const int            coupled_faces[],
                           const int          **face_vertices,
                           const double       **vertex_coords);

/*----------------------------------------------------------------------------
 * Synchronize with applications in the same PLE coupling group.
 *
 * This function should be called before starting a new time step. The
 * current time step id is that of the last finished time step, or 0 at
 * initialization.
 *
 * Default synchronization flags indicating a new iteration or end of
 * calculation are set automatically, but the user may set additional flags
 * to this function if necessary.
 *
 * parameters:
 *   flags         <-- optional additional synchronization flags
 *   current_ts_id <-- current time step id
 *   max_ts_id     <-> maximum time step id
 *   ts            <-> suggested time step value
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_sync_apps(int      flags,
                           int      current_ts_id,
                           int     *max_ts_id,
                           double  *ts);

/*----------------------------------------------------------------------------
 * Exchange of synchronization (supervision) messages
 *
 * parameters:
 *  coupling <-- Associated coupling object
 *  is_end   --> Calculation stop indicator
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_supervise(syr_cfd_coupling_t  *coupling,
                           int                 *is_end);

/*----------------------------------------------------------------------------
 * Data exchange for coupled cell values prior to iteration
 *
 * Send wall temperature
 * Receive fluid temperature and pseudo-exchange coefficient
 *
 * parameters:
 *   coupling <-- Associated coupling object
 *   t_node   <-- Temperature at node, or NULL
 *   t_cell   <-> Wall temperature on coupled cells in (if t_node is NULL),
 *                Fluid temperature on coupled cells out
 *   h_cell   --> Wall pseudo-exchange coefficient on coupled cells
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_cell_vars(syr_cfd_coupling_t  *coupling,
                           const double        *t_node,
                           double              *t_cell,
                           double              *h_cell);

/*----------------------------------------------------------------------------
 * Data exchange for coupled face values prior to iteration
 *
 * Send wall temperature
 * Receive fluid temperature and pseudo-exchange coefficient
 *
 * parameters:
 *   coupling <-- Associated coupling object
 *   t_node   <-- Temperature at node, or NULL
 *   t_face   <-> Wall temperature on coupled faces in (if t_node is NULL),
 *                Fluid temperature on coupled faces out
 *   h_face   --> Wall pseudo-exchange coefficient on coupled faces
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_face_vars(syr_cfd_coupling_t  *coupling,
                           const double        *t_node,
                           double              *t_face,
                           double              *h_face);

/*----------------------------------------------------------------------------
 * Ensure conservativity thanks to a corrector coefficient computed by SYRTHES
 * SYRTHES computes a global flux for a given tfluid and hfluid field.
 * CFD code sent before its computed global flux for this time step.
 * h_face is modified to ensure conservativity.
 *
 * parameters:
 *   coupling   <-- coupling structure
 *   sur_flux   <-- thermal flux computed by SYRTHES to compare with CFD
 *   h_surf     <-> exchange coefficient defined by face
 *----------------------------------------------------------------------------*/

void
syr_cfd_coupling_ensure_conservativity(syr_cfd_coupling_t   *coupling,
                                       double                syr_flux,
                                       double                h_surf[]);

/*----------------------------------------------------------------------------*/

#endif /* defined(_SYRTHES_CFD_) */

#endif /* _SYR_CFD_COUPLING_H_ */
