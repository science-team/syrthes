/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#ifndef _HMT_PROTO_H
#define _HMT_PROTO_H

#include "syr_bd.h"
#include "syr_hmt_bd.h"
#include "syr_parall.h"
#

double fphyhmt_fxl(struct ConstPhyhmt,double);
double fphyhmt_fdl(struct ConstPhyhmt,double);
double fphyhmt_fpsat(double);
double fphyhmt_fxmg(double);
double fphyhmt_fxml(double);
double fphyhmt_feps(struct ConstPhyhmt,struct ConstMateriaux,double); 
double fphyhmt_fxkt(struct ConstPhyhmt,struct ConstMateriaux,double,double,double,double);
double fphyhmt_fxkl(struct ConstPhyhmt,struct ConstMateriaux,double,double); 

void hmt_alloue_val_clim(struct HmtClimhhh*);
double hmt_calbilsurf(int,struct HmtClimhhh,struct Maillage,struct MaillageCL,
		      struct Bilan,struct Variable, 
		      struct ConstPhyhmt,
		      double*,double*,double*);
void hmt_calbilvol(int n,struct Bilan,struct Maillage,double**,struct Humid,
		   struct ConstPhyhmt,struct ConstMateriaux*,double*,double*,double*);
void hmt_coeffequa(int,int,struct Maillage,struct Humid,double**,
		   struct ConstPhyhmt,struct ConstMateriaux *,double *);		      

void hmt_cree_liste_clim(struct MaillageCL,struct HmtClimhhh*);
void hmt_decode_clim(struct MaillageBord*);
void hmt_diffus_t(struct PasDeTemps,struct Maillage,struct MaillageCL,struct MaillageBord,
		  struct Variable,double**,struct Contact,
		  struct Couple,struct Clim,struct Climcfd,struct Climcfd,
		  struct HmtClimhhh,
		  struct Cvol*,struct Cperio,struct Humid,struct ConstPhyhmt,struct ConstMateriaux*,
		  struct Mst,struct Matrice,struct Travail,struct Travail,struct SDparall);

void hmt_diffus_pv(struct PasDeTemps,struct Maillage,struct MaillageCL,struct MaillageBord,
		   struct Variable,double**,struct Contact,
		   struct Couple,struct Clim,struct Climcfd,struct Climcfd,
		   struct HmtClimhhh,
		   struct Cvol*,struct Cperio,struct Humid,struct ConstPhyhmt,struct ConstMateriaux*,
		   struct Mst,struct Matrice,struct Travail,struct Travail,struct SDparall);
void hmt_diffus_pt(struct PasDeTemps,struct Maillage,struct MaillageCL,struct MaillageBord,
		   struct Variable,double**,struct Contact,
		   struct Couple,struct Clim,struct Climcfd,struct Climcfd,
		   struct HmtClimhhh,
		   struct Cvol*,struct Cperio,struct Humid,struct ConstPhyhmt,struct ConstMateriaux*,
		   struct Mst,struct Matrice,struct Travail,struct Travail,struct SDparall);

void hmt_init(struct Maillage,struct ConstPhyhmt*,struct ConstMateriaux**,
	      struct Humid*,double***);

void hmt_iniconstphyhmt(struct ConstPhyhmt*);
void hmt_iniconstmat(struct ConstMateriaux*,struct ConstPhyhmt);
void hmt_lire_hhh(struct HmtClimhhh*,struct MaillageCL,char*,int);
void hmt_lire_limite(struct HmtClimhhh*,
		     struct Maillage,struct MaillageBord,struct MaillageCL);
void hmt_lire_materiaux(struct Maillage,struct Humid);
void hmt_lire_rescon(struct Contact*,struct MaillageCL,char*,int);
void hmt_madif(struct Maillage,double*,double*,double*);

void hmt_mafcli_t(struct HmtClimhhh,struct Couple,struct Clim,struct Climcfd,
		  struct Maillage,struct MaillageCL,
		  double*,double*,double*);

void hmt_mafcli_pv(struct HmtClimhhh,struct Maillage,struct MaillageCL,
		   struct Variable, struct ConstPhyhmt,double*,double*);

void hmt_mafcli_pt(struct HmtClimhhh, struct Maillage,struct MaillageCL,
		   struct Variable,struct ConstPhyhmt,double*,double*);

void mafclc_t(struct Contact,struct Maillage,struct MaillageCL,
	    double*,double*,double*);
void mafclc_pv(struct Contact,struct Maillage,struct MaillageCL,
	    double*,double*,double*);
void mafclc_pt(struct Contact,struct Maillage,struct MaillageCL,
	    double*,double*,double*);


void hmt_resol(struct PasDeTemps*,struct Maillage,struct MaillageCL,  
	       struct MaillageCL,struct MaillageBord,  
	       struct Cperio,struct Climcfd,struct Climcfd,
	       struct Contact,struct Couple,struct Clim,
	       struct HmtClimhhh,
	       struct Cvol*,struct Mst,struct Variable,double**,
	       struct Humid,struct ConstPhyhmt,struct ConstMateriaux*,struct Meteo,struct Myfile myfile,
	       struct Matrice,struct Travail,struct Travail,struct SDparall);

void hmt_smexp_t(struct HmtClimhhh,struct Maillage,struct MaillageCL,
		 struct Variable,struct ConstPhyhmt,double*);
void hmt_smexp_pv(struct HmtClimhhh,struct Maillage,struct MaillageCL,
		 struct Variable,struct ConstPhyhmt,double*);
void hmt_smexp_pt(struct HmtClimhhh,struct Maillage,struct MaillageCL,
		 struct Variable,struct ConstPhyhmt,double*);
void hmt_verif_affectmat(struct Maillage,struct Humid);



void user_hmt_affectmat(struct Maillage,struct Humid);
void user_hmt_cini(struct Maillage,double*,double*,double*,
		   struct Humid,struct PasDeTemps*,struct Meteo,struct Myfile);
void user_hmt_limfso(struct Maillage,struct MaillageCL,double*,double*,double*,
		     struct HmtClimhhh,struct Clim,struct PasDeTemps*,struct Meteo,struct Myfile);
void user_hmt_limfso_fct(struct Maillage,struct MaillageCL,double*,double*,double*,
		     struct HmtClimhhh,struct Clim,double);
void user_hmt_cfluvs(struct Maillage maillnodes,
		     double*,struct Cvol,double*,struct Cvol,double*,struct Cvol,struct PasDeTemps*,
		     struct Humid,struct Meteo,struct Myfile);
void user_hmt_cfluvs_fct(struct Maillage maillnodes,
		     double*,struct Cvol,double*,struct Cvol,double*,struct Cvol,double);

void user_hmt_rescon(struct Maillage,struct MaillageCL,
		      double*,double*,double*,double*,double*,double*,
		      struct Contact,struct PasDeTemps*,struct SDparall);
void user_hmt_rescon_fct(struct Maillage,struct MaillageCL,
		      double*,double*,double*,double*,double*,double*,
		      struct Contact,double,struct SDparall);

#endif
