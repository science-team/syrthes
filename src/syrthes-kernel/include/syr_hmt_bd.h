/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#ifndef _HMT_BD_H
#define _HMT_BD_H


/*======================================================================*/
/*======== Parametres generaux =========================================*/
/*======================================================================*/
struct Humid
{
  int actif;
  int model;
  int *mat;
};


/*======================================================================*/
/*======== Conditions aux limites ======================================*/
/*======================================================================*/
struct HmtClimhhh
{
  int nbval;
  int npoin,nelem,ndmat;  /* cond[nbface,ndmass] */
  int *numf;
  double **t_ext,**t_h;
  double **pv_ext,**pv_h;
  double **pt_ext,**pt_h;
};

/*======================================================================*/
/*======== Proprietes physiques constante du fluide et de l'air ========*/
/*======================================================================*/
struct ConstPhyhmt
{
  double rhol;      /* Masse volumique du liquide                  */
  double rhot;      /* Masse volumique de l'air                    */
  double R;         /* Constante des gaz parfaits                  */
  double xmv;       /* Masse molaire de la vapeur                  */
  double xmas;      /* Masse molaire de l'air sec                  */
  double Rv;        /* Constante massique de la vapeur             */
  double Ras;       /* Constante massique de l'air sec             */
  double Cpv;       /* Capacite calorifique massique de la vapeur  */
  double Cpas;      /* Capacite calorifique massique de l'air sec  */
  double Cpl;       /*  Capacite calorifique massique de l'eau     */
};

/*=====================================================*/
/*======== Proprietes constantes des materiaux ========*/
/*=====================================================*/
struct ConstMateriaux
{
  double rhos;      /* Masse volumique du materiau sec            */
  double cs;        /* Chaleur massique du materiqu sec           */
  double eps0;      /* Porosite du materiau sec                   */
  double xk;        /* Permeabilite intrinseque                   */
  double xknv;      /* Permeabilite de Knudsen                    */
  double taumax;    /* Taux d'humidite volumique maximum          */
};



#endif
