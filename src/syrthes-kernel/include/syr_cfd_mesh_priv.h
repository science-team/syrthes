#ifndef __SYR_CFD_MESH_PRIV_H__
#define __SYR_CFD_MESH_PRIV_H__

/*============================================================================
 * Main structure for a mesh representation
 *============================================================================*/

/*
  This file is part of the "Parallel Location and Exchange" library,
  intended to provide mesh or particle-based code coupling services.

  Copyright (C) 2005-2010  EDF

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*----------------------------------------------------------------------------
 *  Local headers
 *----------------------------------------------------------------------------*/

#include "ple_defs.h"

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#if 0
} /* Fake brace to force back Emacs auto-indentation back to column 0 */
#endif
#endif /* __cplusplus */

/*=============================================================================
 * Macro definitions
 *============================================================================*/

/*============================================================================
 * Type definitions
 *============================================================================*/

/*----------------------------------------------------------------------------
 * Structure defining a mesh
 *----------------------------------------------------------------------------*/

struct _syr_cfd_mesh_t {

  /* Global indicators */
  /*-------------------*/

  int                 dim;           /* Spatial dimension */
  syr_cfd_element_t   element_type;  /* Element type */

  /* Local dimensions */
  /*------------------*/

  int                 n_vertices;    /* Number of vertices */
  int                 n_elements;    /* Number of elements */

  /* Vertex definitions; */
  /*---------------------*/

  double             *vertex_coords;    /* pointer to  vertex coordinates
                                           (always interlaced:
                                           x1, y1, z1, x2, y2, z2, ...) */

  /* Mesh connectivity */
  /*-------------------*/

  int                *vertex_num;       /* element vertex numbers (1 to n);
                                           size: n_elements*(element_dim+1) */
};

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SYR_CFD_MESH_PRIV_H__ */
