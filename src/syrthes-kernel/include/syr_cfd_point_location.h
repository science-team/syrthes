#ifndef __SYR_CFD_POINT_LOCATION_H__
#define __SYR_CFD_POINT_LOCATION_H__

/*============================================================================
 * Locate local points in a mesh
 *============================================================================*/

/*
  This file is part of the "Parallel Location and Exchange" library,
  intended to provide mesh or particle-based code coupling services.

  Copyright (C) 2005-2010  EDF

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*----------------------------------------------------------------------------*/

#include "ple_config.h"

/*----------------------------------------------------------------------------
 *  Local headers
 *----------------------------------------------------------------------------*/

#include "ple_defs.h"
#include "syr_cfd_mesh.h"

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#if 0
} /* Fake brace to force back Emacs auto-indentation back to column 0 */
#endif
#endif /* __cplusplus */

/*=============================================================================
 * Macro definitions
 *============================================================================*/

/*============================================================================
 * Type definitions
 *============================================================================*/

/*=============================================================================
 * Static global variables
 *============================================================================*/

/*=============================================================================
 * Public function prototypes
 *============================================================================*/

/*----------------------------------------------------------------------------
 * Compute extents of a mesh representation
 *
 * parameters:
 *   mesh          <-- pointer to mesh representation structure
 *   n_max_extents <-- maximum number of sub-extents (such as element extents)
 *                     to compute, or -1 to query
 *   tolerance     <-- addition to local extents of each element:
 *                     extent = base_extent * (1 + tolerance)
 *   extents       <-> extents associated with mesh:
 *                     x_min, y_min, ..., x_max, y_max, ... (size: 2*dim)
 *----------------------------------------------------------------------------*/

ple_lnum_t
syr_cfd_point_location_extents(const void  *mesh,
                               ple_lnum_t   n_max_extents,
                               double       tolerance,
                               double       extents[]);

/*----------------------------------------------------------------------------
 * Find elements in a given mesh containing points: updates the
 * location[] and distance[] arrays associated with a set of points
 * for points that are in an element of this mesh, or closer to one
 * than to previously encountered elements.
 *
 * parameters:
 *   mesh               <-- pointer to mesh representation structure
 *   tolerance_base     <-- associated base tolerance (used for bounding
 *                          box check only, not for location test)
 *   tolerance_fraction <-- associated fraction of element bounding boxes
 *                          added to tolerance
 *   n_points           <-- number of points to locate
 *   point_coords       <-- point coordinates
 *   point_tag          <-- optional point tag (unused here)
 *   location           <-> number of element containing or closest to each
 *                          point (size: n_points)
 *   distance           <-> distance from point to element indicated by
 *                          location[]: < 0 if unlocated, 0 - 1 if inside,
 *                          and > 1 if outside a volume element, or absolute
 *                          distance to a surface element (size: n_points)
 *----------------------------------------------------------------------------*/

void
syr_cfd_point_location_contain(const void         *mesh,
                               float               tolerance_base,
                               float               tolerance_fraction,
                               ple_lnum_t          n_points,
                               const ple_coord_t   point_coords[],
                               const ple_lnum_t    point_tag[],
                               ple_lnum_t          location[],
                               float               distance[]);

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SYR_CFD_POINT_LOCATION_H__ */
