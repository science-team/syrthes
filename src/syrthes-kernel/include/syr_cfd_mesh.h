#ifndef __SYR_CFD_MESH_H__
#define __SYR_CFD_MESH_H__

/*============================================================================
 * Main structure for a mesh representation
 *============================================================================*/

/*
  This file is part of the "Parallel Location and Exchange" library,
  intended to provide mesh or particle-based code coupling services.

  Copyright (C) 2005-2010  EDF

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*----------------------------------------------------------------------------
 *  Local headers
 *----------------------------------------------------------------------------*/

#include "ple_defs.h"

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#if 0
} /* Fake brace to force back Emacs auto-indentation back to column 0 */
#endif
#endif /* __cplusplus */

/*=============================================================================
 * Macro definitions
 *============================================================================*/

/*============================================================================
 * Type definitions
 *============================================================================*/

/*----------------------------------------------------------------------------
 * Element types
 *----------------------------------------------------------------------------*/

typedef enum {

  SYR_CFD_EDGE,            /* Edge */
  SYR_CFD_TRIA,            /* Triangle */
  SYR_CFD_TETRA,           /* Tetrahedron */
  SYR_CFD_N_ELEMENT_TYPES  /* Number of element types */

} syr_cfd_element_t;

/*----------------------------------------------------------------------------
 * Structure defining a mesh in mesh definition
 *----------------------------------------------------------------------------*/

typedef struct _syr_cfd_mesh_t syr_cfd_mesh_t;

/*=============================================================================
 * Static global variables
 *============================================================================*/

/* Number of vertices associated with each "mesh" element type */

extern const int  syr_cfd_mesh_n_vertices_element[];

/*=============================================================================
 * Public function prototypes
 *============================================================================*/

/*----------------------------------------------------------------------------
 * Creation of a mesh representation structure.
 *
 * Ownership of the given coordinates and connectivity arrays is transferred
 * to the mesh representation structure.
 *
 * parameters:
 *   dim           <-- spatial dimension
 *   n_vertices    <-- number of vertices
 *   n_elements    <-- number of elements
 *   type          <-- type of elements
 *   vertex_coords <-- coordinates of parent vertices (interlaced)
 *   vertex_num    <-- element -> vertex connectivity
 *
 * returns:
 *  pointer to created mesh representation structure
 *----------------------------------------------------------------------------*/

syr_cfd_mesh_t *
syr_cfd_mesh_create(int                 dim,
                    int                 n_vertices,
                    int                 n_elements,
                    syr_cfd_element_t   type,
                    double              vertex_coords[],
                    int                 vertex_num[]);

/*----------------------------------------------------------------------------
 * Destruction of a mesh representation structure.
 *
 * parameters:
 *   this_mesh  <-> pointer to pointer to structure that should be destroyed
 *----------------------------------------------------------------------------*/

void
syr_cfd_mesh_destroy(syr_cfd_mesh_t  **this_mesh);

/*----------------------------------------------------------------------------
 * Copy element centers to an array.
 *
 * parameters:
 *   this_mesh    <-- pointer to mesh structure
 *   cell_centers --> cell centers coordinates (pre-allocated)
 *----------------------------------------------------------------------------*/

void
syr_cfd_mesh_get_element_centers(const syr_cfd_mesh_t  *mesh,
                                 ple_coord_t           *cell_centers);

/*----------------------------------------------------------------------------
 * Dump printout of a mesh representation structure.
 *
 * parameters:
 *   this_mesh <-- pointer to structure that should be dumped
 *----------------------------------------------------------------------------*/

void
syr_cfd_mesh_dump(const syr_cfd_mesh_t  *this_mesh);

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SYR_CFD_SYR_MESH_H__ */
