/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_bd.h"
#include "syr_option.h"
#include "syr_abs.h"
#include "syr_tree.h"
#include "syr_const.h"
#include "syr_parall.h"

#include "syr_proto.h"

#ifdef _SYRTHES_MPI_
#include "mpi.h"
MPI_Status status;
#endif


#ifdef _SYRTHES_BLAS_
#include "cblas.h"
#endif

extern struct Performances perfo;
extern struct Affichages affich;
extern int optim,lmst;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |       Produit Matrice.Vecteur                                        |
  |       X = M Y                                                        |
  |======================================================================| */

void omv(struct Maillage maillnodes,double *x,double *y,struct Matrice matr,
	 struct Cperio perio, double *trav)
{
  int i;
  int nbs,**nar;
  double xseg,*xdms;
  int *nar0,*nar1;

  nbs=maillnodes.nbarete;
  nar=maillnodes.arete;
  xdms=matr.xdms;

  for (i=0,nar0=*(nar),nar1=*(nar+1);i<nbs;i++,nar0++,nar1++)
    {
      xseg=xdms[i];
      x[*nar0]+=xseg*y[*nar1];
      x[*nar1]+=xseg*y[*nar0];
    }
  
  /* prise en compte de la periodicite */
  if (perio.npoin) period(maillnodes,x,trav,perio);


  /* ajout de la multiplication par la diagonale */
  for (i=0;i<maillnodes.npoin;i++) x[i]+=matr.dmat[i]*y[i];



}





/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |       Produit Matrice.Vecteur en parallele                           |
  |       X = M Y                                                        |
  |======================================================================| */

void omv_parall(struct Maillage maillnodes,
		double *x,double *y,struct Matrice matr,
		struct Cperio perio,double *trav0,
		struct SDparall sdparall )
{
#ifdef _SYRTHES_MPI_
  int i;
  double *w0,*w1,*w2,*w3;
  int nbs,**nar;
  double xseg,*xdms;
  int *nar0,*nar1;

  nbs=maillnodes.nbarete;
  nar=maillnodes.arete;
  xdms=matr.xdms;

  for (i=0,nar0=*(nar),nar1=*(nar+1);i<nbs;i++,nar0++,nar1++)
    {
      xseg=xdms[i];
      x[*nar0]+=xseg*y[*nar1];
      x[*nar1]+=xseg*y[*nar0];
    }

  if (sdparall.nparts>1) assemb_complt(x,maillnodes.npoin,trav0,sdparall);


  /* ajout des contributions periodiques */
  if (perio.existglob)
    if (sdparall.nparts>1)
      period_parall(maillnodes,x,trav0,perio,sdparall);
    else
      period(maillnodes,x,trav0,perio);



  /* ajout de la multiplication par la diagonale */
  for (i=0;i<maillnodes.npoin;i++) x[i]+=matr.dmat[i]*y[i];

#endif
}






/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Produit Matrice.Vecteur  reduit pour traitement des Dirichlet        |
  |       X = M Y                                                        |
  |======================================================================| */

void omvdir(struct Maillage maillnodes,struct Clim diric,
	    double *x,double *y,struct Matrice matr)
{
  int i,j,n=0,ne,np;
  double y1,y2,y3,y4;
  int ne0,ne1,ne2,ne3,ne4;

  int nbsd;
  int nar0,nar1,nard;
  int **nar;
  double *xdms,xseg;

  nbsd=diric.nbarete;
  nar=maillnodes.arete;
  xdms=matr.xdms;

  for (i=0;i<nbsd;i++)
    {
      nard=diric.arete[i];
      nar0=nar[0][nard];nar1=nar[1][nard];
/*       xseg=xdms[nard]; */
      xseg=diric.xdms[i];
      x[nar0]+=xseg*y[nar1];
      x[nar1]+=xseg*y[nar0];
    }

  /* prise en compte de la periodicite - pas sur  qu'il faille le prendre en compte*/
  /* voir ulterieurement avec isa */
  /*   if (perio.npoin) period(maillnodes,x,trav,perio); */
 

  /* ajout de la multiplication par la diagonale */
  for (i=0;i<diric.npoin;i++)
    {
      np=diric.nump[i];
      x[np]+=matr.dmat[np]*y[np];
    }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | en parallele, completion de l'assemblage des matrices elementaires   |
  |        RES = A Y                                                     |
  |======================================================================|*/
void assemb_complt(double *res,int npoins,double *trav0,
		   struct SDparall sdparall )
{
  int i,j,n,nbrequest;
  double *adr;
  double *trav1,*trav2;

#ifdef _SYRTHES_MPI_
  MPI_Status tabstatus[100];
  trav1=sdparall.trav1;
  trav2=sdparall.trav2;

  /* on met dans trav1 la totalite de la frontiere du domaine courant */
  /* avec tous les autres domaines, suivant le rangement de adrcommun */

  goto coucou;

  for (n=0;n<sdparall.nparts;n++)
    for (i=sdparall.adrcommun[n]; i<sdparall.adrcommun[n]+sdparall.nbcommun[n]; i++) 
      trav1[i]=res[sdparall.tcommun[i]];


  for (nbrequest=n=0;n<sdparall.nparts;n++)
    {
      if (n!=sdparall.rang && sdparall.nbcommun[n]>0)
	{
	  MPI_Irecv(trav2+sdparall.adrcommun[n],sdparall.nbcommun[n],MPI_DOUBLE,n,sdparall.rang,
		    sdparall.syrthes_comm_world,sdparall.request+nbrequest);
	  nbrequest++;
	}
    }

  MPI_Barrier(sdparall.syrthes_comm_world);


  for (n=0;n<sdparall.nparts;n++)
    {
      if (n!=sdparall.rang && sdparall.nbcommun[n]>0)
	{
	  MPI_Isend(trav1+sdparall.adrcommun[n],sdparall.nbcommun[n],MPI_DOUBLE,n,n,
		    sdparall.syrthes_comm_world,sdparall.request+nbrequest);
	  nbrequest++;
	}
    }

  MPI_Waitall(nbrequest,sdparall.request,tabstatus);



  for (n=0;n<sdparall.nparts;n++)
    if (n!=sdparall.rang && sdparall.nbcommun[n]>0)
      {
	for (i=sdparall.adrcommun[n] ;i<sdparall.adrcommun[n]+sdparall.nbcommun[n]; i++)
	  res[sdparall.tcommun[i]]+=trav2[i];
      }


return;




 coucou:
  /* version initiale */
  azero1D(npoins,trav0);

  for (n=0;n<sdparall.nparts;n++)
    {
      if (n!=sdparall.rang && sdparall.nbcommun[n]>0)
	{
	  for (i=0;i<sdparall.nbcommun[n];i++) 
	    trav1[i]=res[sdparall.tcommun[sdparall.adrcommun[n]+i]];

	  MPI_Sendrecv_replace(trav1,sdparall.nbcommun[n],MPI_DOUBLE,n,0,n,0,
			       sdparall.syrthes_comm_world,&status);

/* 	  MPI_Sendrecv(trav1,sdparall.nbcommun[n],MPI_DOUBLE,n,0, */
/* 		       trav2,sdparall.nbcommun[n],MPI_DOUBLE,n,0, */
/* 		       sdparall.syrthes_comm_world,&status); */

	  for (i=0;i<sdparall.nbcommun[n];i++)
	    trav0[sdparall.tcommun[sdparall.adrcommun[n]+i]]+=trav1[i];
	}
    }

/*   on rajoute les contributions recues */
  for (i=0;i<npoins;i++) res[i]+=trav0[i];


#endif
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | operation vectorielle y = x                                          |
  |                                                                      |
  |======================================================================|*/
void syr_copy(int nb,double *x,double *y)
{
  int i;
  double res;

#ifdef _SYRTHES_BLAS_
  cblas_dcopy(nb,x,1,y,1);
#else
  for (i=0;i<nb;i++) y[i]=x[i];
#endif
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | operation vectorielle y = ax + y                                     |
  |                                                                      |
  |======================================================================|*/
void syr_axpy(int nb,double a, double *x,double *y)
{
  int i;
  double res;

#ifdef _SYRTHES_BLAS_
  cblas_daxpy(nb,a,x,1,y,1);
#else
  for (i=0;i<nb;i++) y[i]+=a*x[i];
#endif

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Produit scalaire en sequentiel                                       |
  |                                                                      |
  |======================================================================|*/
double syr_prosca(int nb,double *x, double *y)
{
  int i;
  double res;

#ifdef _SYRTHES_BLAS_
  res = cblas_ddot(nb,x,1,y,1);
#else
  for (res=0,i=0;i<nb;i++) res+=x[i]*y[i];
#endif

  return res;

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Produit scalaire en parallele                                        |
  |                                                                      |
  |======================================================================|*/
void syr_prosca_parall(double *resu,double *x, double *y,
		       struct SDparall sdparall)
{
#ifdef _SYRTHES_MPI_
  int i,ideb;
  double resuloc;

  /* produit scalaire sur les noeuds internes */
  for (resuloc=0.,i=0;i<sdparall.nbno_interne;i++) resuloc+=x[i]*y[i];

  /* produit scalaire sur les noeuds frontieres                              */
  /* --> on divise par le nombre de fois ou ils sont communs aux autres procs */
  for (ideb=sdparall.nbno_interne,i=0;i<sdparall.nbno_front;i++)
    resuloc+=(x[ideb+i]*y[ideb+i])/sdparall.nfoisfront[i];

  MPI_Allreduce(&resuloc,resu,1,MPI_DOUBLE,MPI_SUM,sdparall.syrthes_comm_world);

  return;

#endif
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Produit scalaire en parallele                                        |
  |                                                                      |
  |======================================================================|*/
void syr_2prosca_parall(double *tabres,
			double *x1, double *y1,
			double *x2, double *y2,
			struct SDparall sdparall)
{
#ifdef _SYRTHES_MPI_
  int i,ideb;
  double resuloc[2];

  /* produit scalaire sur les noeuds internes */
  for (resuloc[0]=resuloc[1]=0.,i=0;i<sdparall.nbno_interne;i++) {
    resuloc[0]+=x1[i]*y1[i];
    resuloc[1]+=x2[i]*y2[i];
  }

  /* produit scalaire sur les noeuds frontieres                              */
  /* --> on divise par le nombre de fois ou ils sont communs aux autres procs */
  for (ideb=sdparall.nbno_interne,i=0;i<sdparall.nbno_front;i++){
    resuloc[0] += (x1[ideb+i]*y1[ideb+i])/sdparall.nfoisfront[i];
    resuloc[1] += (x2[ideb+i]*y2[ideb+i])/sdparall.nfoisfront[i];
  }

  MPI_Allreduce(resuloc,tabres,2,MPI_DOUBLE,MPI_SUM,sdparall.syrthes_comm_world);

  return;

#endif
}

