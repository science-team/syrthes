/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "syr_usertype.h"
#include "syr_abs.h"
#include "syr_bd.h"
#include "syr_tree.h"
#include "syr_option.h"
#include "syr_proto.h"

/* #ifdef _SYRTHES_MPI_ */
/* #include "mpi.h" */
/* #endif */

#define N64 128

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | boite2 : calcul de la boite englobante                                |
  |                                                                      |
  |======================================================================| */
void boite2(int ndim,int npoin1,double **coord1,int npoin2,double **coord2,
	    double dim_boite[])
{
  int i;
  double xmin,xmax,ymin,ymax,zmin,zmax,dx,dy,dz;

  xmin=ymin=zmin= 1.e10;
  xmax=ymax=zmax=-1.e10;
    
  for (i=0;i<npoin1;i++)
    {
      xmin=min(coord1[0][i],xmin);ymin=min(coord1[1][i],ymin);
      xmax=max(coord1[0][i],xmax);ymax=max(coord1[1][i],ymax);
    }
  if (ndim==3)
    for (i=0;i<npoin1;i++)
      {
	zmin=min(coord1[2][i],zmin);
	zmax=max(coord1[2][i],zmax);
      }


  for (i=0;i<npoin2;i++)
    {
      xmin=min(coord2[0][i],xmin);ymin=min(coord2[1][i],ymin);
      xmax=max(coord2[0][i],xmax);ymax=max(coord2[1][i],ymax);
    }
  if (ndim==3)
    for (i=0;i<npoin2;i++)
      {
	zmin=min(coord2[2][i],zmin);
	zmax=max(coord2[2][i],zmax);
      }
  
  dx=xmax-xmin; dy=ymax-ymin; dz=zmax-zmin;
  xmin -= (dx*0.01); ymin -= (dy*0.01); zmin -= (dz*0.01);
  xmax += (dx*0.01); ymax += (dy*0.01); zmax += (dz*0.01); 
  
  
  dx=xmax-xmin; dy=ymax-ymin; dz=zmax-zmin;
  if (dx<1.e-10) xmax+=1.e-4;
  if (dy<1.e-10) ymax+=1.e-4;
  if (dz<1.e-10) zmax+=1.e-4;
  
  xmin -= (dx*0.01); ymin -= (dy*0.01); zmin -= (dz*0.01);
  xmax += (dx*0.01); ymax += (dy*0.01); zmax += (dz*0.01); 
  
  dim_boite[0]=xmin; dim_boite[1]=xmax; 
  dim_boite[2]=ymin; dim_boite[3]=ymax; 
  dim_boite[4]=zmin; dim_boite[5]=zmax; 
  
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | bary3d                                                               |
  | Calcul des coefficients barycentriques dans un triangle en 3D        |
  |======================================================================| */

void bary3d(double xx,double yy,double zz,
	    double xa,double ya,double za,double xb,double yb,double zb,
	    double xc,double yc,double zc,
	    double *xl1,double *xl2,double *xl3)
{
  
  double deno,deno1,deno2,deno3,x1,y1,z1,x2,y2,z2,x3,y3,z3,un;
  int n;

  un=1.;
  deno = deno1 = determ (xa,ya,un,xb,yb,un,xc,yc,un);
  deno2 = determ (ya,za,un,yb,zb,un,yc,zc,un);
  deno3 = determ (xa,za,un,xb,zb,un,xc,zc,un);
  n=1; if (fabs(deno2)>fabs(deno)) {deno=deno2;n=2;}
  if (fabs(deno3)>fabs(deno)) {deno=deno3;n=3;}

  switch(n)
    {
    case 1:
      *xl1  = determ (xx,yy,un,xb,yb,un,xc,yc,un);
      *xl2  = determ (xa,ya,un,xx,yy,un,xc,yc,un);
      break;
    case 2:
      *xl1  = determ (yy,zz,un,yb,zb,un,yc,zc,un);
      *xl2  = determ (ya,za,un,yy,zz,un,yc,zc,un);
      break;
    case 3:
      *xl1  = determ (xx,zz,un,xb,zb,un,xc,zc,un);
      *xl2  = determ (xa,za,un,xx,zz,un,xc,zc,un);
      break;
    }

  *xl1 = *xl1 / deno;
  *xl2 = *xl2 / deno;
  *xl3 = 1. - *xl1 - *xl2;

}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | determ                                                               |
  |         Calcul d'un determinant                                      |
  |======================================================================| */
double determ(double x1,double y1,double z1,
	      double x2,double y2,double z2,
	      double x3,double y3,double z3)
  {
    return(x1*y2*z3 + x2*y3*z1 + y1*z2*x3-z1*y2*x3 - y1*x2*z3 - z2*y3*x1);
  }
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | cal_dmin                                                             |
  |         Calcul du point qui realise la distance minimale             |
  |                                                                      |
  |======================================================================| */
void cal_dmin(double xp1,double yp1,double zp1,
	      double *xa,double *ya,double *za,double *xb,double *yb,double *zb,
	      double *xc,double *yc,double *zc,
	      int nlonv,double *dmin,double *xmin,double *ymin,
	      double *zmin,int *nelmin,
	      int *lco,int *icode)
{

  int i;
  double  xx[N64],yy[N64],dist[N64],xdmi[N64],ydmi[N64],zdmi[N64];
  double  xab[N64],yab[N64],zab[N64],xac[N64],yac[N64],zac[N64];
  double  abn[N64],acn[N64],xabac[N64],yabac[N64],zabac[N64];
  double  xe1[N64],ye1[N64],ze1[N64],xe2[N64],ye2[N64],ze2[N64];
  double  ace1[N64],e2n[N64],xh[N64];
  double  xanew[N64],yanew[N64],xbnew[N64],ybnew[N64],xcnew[N64],ycnew[N64];
  double  xn[N64],yn[N64],zn[N64],xp[N64],yp[N64];
  double  a1[N64],b1[N64],c1[N64],a2[N64],b2[N64],c2[N64];
  double  a3[N64],b3[N64],c3[N64],d1[N64],d2[N64],d3[N64];
  double  cond12[N64],cond21[N64],cond13[N64];
  double  cond31[N64],cond23[N64],cond32[N64],deno[N64];
  double  xnn[N64];


  *dmin=1.e10;
      
  for (i=0;i<nlonv;i++)
    {
      xab[i] = xb[i]-xa[i]; yab[i]=yb[i]-ya[i]; zab[i]=zb[i]-za[i];
      xac[i] = xc[i]-xa[i]; yac[i]=yc[i]-ya[i]; zac[i]=zc[i]-za[i];

      abn[i] = sqrt ( xab[i]*xab[i] + yab[i]*yab[i] + zab[i]*zab[i] );
      acn[i] = sqrt ( xac[i]*xac[i] + yac[i]*yac[i] + zac[i]*zac[i] );

      xabac[i] =   yab[i]*zac[i] - zab[i]*yac[i];
      yabac[i] = - xab[i]*zac[i] + zab[i]*xac[i];
      zabac[i] =   xab[i]*yac[i] - yab[i]*xac[i];

      xnn[i] = sqrt( xabac[i]*xabac[i] + yabac[i]*yabac[i] + zabac[i]*zabac[i]);
      xn[i]  = xabac[i]/xnn[i]; yn[i] = yabac[i]/xnn[i]; zn[i] = zabac[i]/xnn[i];

      xe1[i] = xab[i]/abn[i]; ye1[i] = yab[i]/abn[i];  ze1[i] = zab[i]/abn[i];

      ace1[i]= xac[i]*xe1[i] + yac[i]*ye1[i] + zac[i]*ze1[i];

      xe2[i] = xac[i] - ace1[i]*xe1[i];
      ye2[i] = yac[i] - ace1[i]*ye1[i];
      ze2[i] = zac[i] - ace1[i]*ze1[i];
   
      e2n[i] = sqrt ( xe2[i]*xe2[i] + ye2[i]*ye2[i] + ze2[i]*ze2[i] );
      xe2[i] = xe2[i]/e2n[i]; ye2[i] = ye2[i]/e2n[i]; ze2[i] = ze2[i]/e2n[i];

      xp[i] = (xp1-xa[i])*xe1[i] + (yp1-ya[i])*ye1[i]  + (zp1-za[i])*ze1[i];
      yp[i] = (xp1-xa[i])*xe2[i] + (yp1-ya[i])*ye2[i]  + (zp1-za[i])*ze2[i];

      xh[i] = (xp1-xa[i])*xn[i] + (yp1-ya[i])*yn[i]  + (zp1-za[i])*zn[i];

      xanew[i] = 0.; yanew[i] = 0.;
      xbnew[i] = abn[i]; ybnew[i] = 0.;
      xcnew[i] = xac[i]*xe1[i] + yac[i]*ye1[i] + zac[i]*ze1[i];
      ycnew[i] = xac[i]*xe2[i] + yac[i]*ye2[i] + zac[i]*ze2[i];
    }


  for (i=0;i<nlonv;i++)
    {
      if (lco[i]) 
	{

	  if (ycnew[i]<0) {a1[i]=0; b1[i]=1.; c1[i]=0.;}
	  else {a1[i] =0.; b1[i]=-1.; c1[i]=0.;}

	  a2[i]=-ycnew[i]; b2[i] = xcnew[i]-xbnew[i]; c2[i]=ycnew[i]*xbnew[i];

	  if ((a2[i]*xanew[i] + b2[i]*yanew[i] + c2[i])>0.) 
	    {a2[i]=-a2[i];b2[i]=-b2[i]; c2[i]=-c2[i];}

	  xx[i] = sqrt (a2[i]*a2[i] + b2[i]*b2[i]);
	  a2[i] /= xx[i]; b2[i] /= xx[i]; c2[i] /= xx[i];

	  a3[i] = - ycnew[i]; b3[i]=xcnew[i]; c3[i]=0.;

	  if ((a3[i]*xbnew[i] + b3[i]*ybnew[i] + c3[i])>0.)
	    {a3[i]=-a3[i]; b3[i]=-b3[i];}
	      
	  xx[i] = sqrt ( a3[i]*a3[i] + b3[i]*b3[i] );
	  a3[i] /= xx[i]; b3[i] /= xx[i];

	  d1[i] = a1[i]*xp[i] + b1[i]*yp[i] + c1[i];                        
	  d2[i] = a2[i]*xp[i] + b2[i]*yp[i] + c2[i];                      
	  d3[i] = a3[i]*xp[i] + b3[i]*yp[i] + c3[i];

	  cond12[i] = d1[i] - d2[i]*(a2[i]*a1[i] + b2[i]*b1[i]);
	  cond21[i] = d2[i] - d1[i]*(a1[i]*a2[i] + b1[i]*b2[i]);
	  cond13[i] = d1[i] - d3[i]*(a3[i]*a1[i] + b3[i]*b1[i]);
	  cond31[i] = d3[i] - d1[i]*(a1[i]*a3[i] + b1[i]*b3[i]);
	  cond23[i] = d2[i] - d3[i]*(a3[i]*a2[i] + b3[i]*b2[i]);
	  cond32[i] = d3[i] - d2[i]*(a2[i]*a3[i] + b2[i]*b3[i]);
	}
    }


  for (i=0;i<nlonv;i++)
    {
      if (lco[i]) 
	{
	  if (d1[i]<=0. && d2[i]<=0. && d3[i]<=0.) 
	    {xx[i]=xp[i]; yy[i]=yp[i]; dist[i]=fabs(xh[i]);}

	  else if (d1[i]>0. && cond21[i]<=0. && cond31[i]<=0.) 
	    {xx[i]=xp[i]-d1[i]*a1[i];yy[i]=yp[i]-d1[i]*b1[i];
	     dist[i] = sqrt (xh[i]*xh[i] + d1[i]*d1[i]);}

	  else if (d2[i]>0. && cond12[i]<=0. && cond32[i]<=0.)
	    {xx[i]=xp[i]-d2[i]*a2[i]; yy[i]=yp[i]-d2[i]*b2[i];
	     dist[i] = sqrt (xh[i]*xh[i] + d2[i]*d2[i]);}
 
	  else if (d3[i]>0. && cond13[i]<=0. && cond23[i]<=0.) 
	    {xx[i]=xp[i]-d3[i]*a3[i]; yy[i]=yp[i]-d3[i]*b3[i];
  	     dist[i] = sqrt (xh[i]*xh[i] + d3[i]*d3[i]);}

          else if ( cond23[i]>0. && cond32[i]>0. &&
		    (d1[i]-(cond23[i]*(a1[i]*a2[i]+b1[i]*b2[i])
			    +cond32[i]*(a1[i]*a3[i]+b1[i]*b3[i]))/
		     (1.-(a2[i]*a3[i]+b2[i]*b3[i])*(a2[i]*a3[i]+b2[i]*b3[i])))<=0. )
	    {deno[i]= 1.-(a2[i]*a3[i]+b2[i]*b3[i])*(a2[i]*a3[i]+b2[i]*b3[i]);
	     xx[i]=xp[i]-(cond23[i]*a2[i]+cond32[i]*a3[i]) / deno[i];
	     yy[i]=yp[i]-(cond23[i]*b2[i]+cond32[i]*b3[i]) / deno[i];
	     dist[i]= sqrt ( xh[i]*xh[i] + 
                            (xp[i]-xx[i])*(xp[i]-xx[i]) 
			     + (yp[i]-yy[i])*(yp[i]-yy[i]) );}

	  else if (cond13[i]>0. && cond31[i]>0. &&
                   (d2[i]-(cond13[i]*(a2[i]*a1[i]+b2[i]*b1[i])
			   +cond31[i]*(a2[i]*a3[i]+b2[i]*b3[i]))/
		    (1.-(a1[i]*a3[i]+b1[i]*b3[i])*(a1[i]*a3[i]+b1[i]*b3[i])))<=0. )
	    {deno[i]=1.-(a1[i]*a3[i]+b1[i]*b3[i])*(a1[i]*a3[i]+b1[i]*b3[i]);
	     xx[i]=xp[i]- (cond13[i]*a1[i]+cond31[i]*a3[i]) / deno[i];
	     yy[i]=yp[i]- (cond13[i]*b1[i]+cond31[i]*b3[i]) / deno[i];
	     dist[i]= sqrt ( xh[i]*xh[i] +  (xp[i]-xx[i])*(xp[i]-xx[i]) 
			     + (yp[i]-yy[i])*(yp[i]-yy[i]) );}

	  else if (cond21[i]>0. && cond12[i]>0. &&
		   (d3[i]-(cond12[i]*(a3[i]*a1[i]+b3[i]*b1[i])
			   +cond21[i]*(a3[i]*a2[i]+b3[i]*b2[i]))
		    /(1-(a1[i]*a2[i]+b1[i]*b2[i])*(a1[i]*a2[i]+b1[i]*b2[i])))<=0.)
	    {deno[i]= 1.-(a1[i]*a2[i]+b1[i]*b2[i])*(a1[i]*a2[i]+b1[i]*b2[i]);
	     xx[i]=xp[i]- (cond12[i]*a1[i]+cond21[i]*a2[i]) / deno[i];
	     yy[i]=yp[i]- (cond12[i]*b1[i]+cond21[i]*b2[i]) / deno[i];
	     dist[i]=sqrt ( xh[i]*xh[i] + 
			      (xp[i]-xx[i])*(xp[i]-xx[i]) 
			      + (yp[i]-yy[i])*(yp[i]-yy[i]) );}
	  else
	    icode[i] = 1;


	  xdmi[i] = xa[i] + xx[i]*xe1[i] + yy[i]*xe2[i];
	  ydmi[i] = ya[i] + xx[i]*ye1[i] + yy[i]*ye2[i];
	  zdmi[i] = za[i] + xx[i]*ze1[i] + yy[i]*ze2[i];

	}
    }


  for (i=0;i<nlonv;i++)
    if (lco[i] && dist[i]<*dmin) 
      {
	*dmin= dist[i];*nelmin = i;
	*xmin=xdmi[i]; *ymin=ydmi[i]; *zmin=zdmi[i];
      }

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | histog                                                               |
  |         calculs d'histogrammes                                       |
  |======================================================================| */

void histog (int nbre,double *tab,double vmin, double vmax,
             int *thisto,int nbhisto)
{
  int i,n;
  double x,interv;

  for (i=0;i<nbhisto;i++) thisto[i]=0;
  interv=vmax-vmin;
  if (interv<1.e-16)
    {thisto[0]=nbre;}
  else
    for (i=0;i<nbre;i++)
      {
	x=(tab[i]-vmin)*nbhisto/interv;
	n=(int)x; if (n==nbhisto) n--;
	thisto[n]+=1;
      }
}
      
      
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | dptseg                                                               |
  |         distance d'un point a un segment                             |
  |======================================================================| */
void dptseg(double xp1,double yp1,
	    double xa,double ya,double xb,double yb,
	    double *dist,double *xmin,double *ymin,int *icode)
{
  double xab,yab,abn,xe1,ye1,xe2,ye2;
  double xh,xanew,yanew,xbnew,ybnew,xp;

  xab=xb-xa; yab=yb-ya;
  abn=sqrt ( xab*xab + yab*yab );

  xe1=xab/abn; ye1=yab/abn;
  xe2=-ye1;    ye2= xe1;

  xp=(xp1-xa)*xe1 + (yp1-ya)*ye1;
  xh=(xp1-xa)*xe2 + (yp1-ya)*ye2;

  xanew=yanew=0.;
  xbnew=abn;  ybnew=0.;

  if (xp>=xanew && xp<=xbnew) 
    {*dist=fabs(xh);*xmin=xa+xp*xe1; *ymin=ya+xp*ye1;} 
  else if (xp>xbnew)
    {*dist=sqrt(xh*xh+(xp-xbnew)*(xp-xbnew));
     *xmin=xa+xbnew*xe1; *ymin=ya+xbnew*ye1;}
  else if (xp<xanew)
    {*dist=sqrt(xh*xh+(xp-xanew)*(xp-xanew));
     *xmin=xa+xanew*xe1; *ymin=ya+xanew*ye1;} 
  else 
    *icode=1;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | bary2d                                                               |
  |         Calcul des coefficients barycentriques                       |
  |======================================================================| */
void bary2d(double xx,double yy,double xa,double ya,double xb,double yb,
	    double *xl1,double *xl2)
{
  
  double deno;
  int n;

  n=1; deno=xa-xb;
  if (fabs(ya-yb)>fabs(deno)) {deno=ya-yb; n=2;}

  if (n==1){*xl1=(xx-xb)/deno; *xl2= 1.-*xl1;}
  else {*xl1=(yy-yb)/deno; *xl2= 1.-*xl1;}
}
