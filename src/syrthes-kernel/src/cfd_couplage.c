/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <assert.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_abs.h"
#include "syr_bd.h"
#include "syr_tree.h"
#include "syr_option.h"
#include "syr_const.h"
#include "syr_proto.h"

#ifdef _SYRTHES_MPI_
#include "mpi.h"
MPI_Status status;
#endif

#ifdef _SYRTHES_CFD_
#include "ple_coupling.h"
#include "syr_cfd_coupling.h"
#include "syr_cfd.h"
#endif


extern struct Performances perfo;
extern struct Affichages affich;
extern FILE *fdata;
extern char nomcplcfdg[CHLONG];

static char ch[CHLONG],motcle[CHLONG],repc[CHLONG],formule[CHLONG];
static double dlist[100];
static int ilist[MAX_REF];


#ifdef _SYRTHES_CFD_

int nbCFD=0;
extern struct Cfd cfd;

/*|======================================================================|
  | SYRTHES 4.3                                  COPYRIGHT EDF 2009-2010 |
  |======================================================================|
  | AUTEURS  : J. BONELLE                                                |
  |======================================================================|
  |        Ordonne un tableau de mot                                     |
  |======================================================================| */

static void
_sort_names(int  n, char **names, int *a)
{
  int i, j, h;

  /* Compute stride */
  for (h = 1; h <= n/9; h = 3*h+1) ;

  /* Sort array */
  for (; h > 0; h /= 3) {

    for (i = h; i < n; i++) {

      char *ref = names[a[i]];
      int  ref_id = a[i];

      j = i;
      while ((j >= h) && (strcmp(ref, names[a[j-h]]) < 0)) {
        a[j] = a[j-h];
        j -= h;
      }
      a[j] = ref_id;

    } /* Loop on array elements */

  } /* End of loop on stride */

}

/*|======================================================================|
  | SYRTHES 4.3                                  COPYRIGHT EDF 2009-2010 |
  |======================================================================|
  | AUTEURS  : J. BONELLE                                                |
  |======================================================================|
  |        Comptage du nombre de couplages avec des codes de CFD         |
  |        Allocation des noms associes a chaque code                    |
  |======================================================================| */

static void _add_name(const char *motcle, int n_couplings,
                      int *n_couplings_max, char  ***names)
{
  int  len;
  int  _max = *n_couplings_max;
  char **_names = *names;

  if (n_couplings + 1 > _max) {
    _max *= 2;
    _names = realloc(_names, _max*sizeof(char *));
  }

  len = strlen(motcle);
  _names[n_couplings] = (char *)malloc((len+1)*sizeof(char));
  strncpy(_names[n_couplings], motcle, strlen(motcle));
  _names[n_couplings][len] = '\0';

  *n_couplings_max = _max;
  *names = _names;
}

/*|======================================================================|
  | SYRTHES 4.3                                  COPYRIGHT EDF 2009-2010 |
  |======================================================================|
  | AUTEURS  : J. BONELLE                                                |
  |======================================================================|
  |        Comptage du nombre de couplages avec des codes de CFD         |
  |        Allocation des noms associes a chaque code                    |
  |======================================================================| */

static void _add_coupling_names(struct Cfd  *cfd)
{
  int  i1,i2,i3,i4,i5,i6,id,one_more,len;

  int  n_couplings = 0, n_couplings_max = 3;
  syr_cfd_coupling_type_t  *ctype = NULL;
  int  *array = NULL;
  char  **names = NULL;

  /* Pre-allocation */

  names = (char **)malloc(n_couplings_max*sizeof(char *));

  fseek(fdata,0,SEEK_SET);
  while (fgets(ch,400,fdata)) {
    if (ch[0]!='/' && strlen(ch)>1){
      extr_motcle_(motcle,ch,&i1,&i2);
      if (!strcmp(motcle,"CLIM")) {
	extr_motcle(motcle,ch+i2+1,&i3,&i4);

	if (!strcmp(motcle,"COUPLAGE_SURF_FLUIDE")) {

          /* Identify related cfd coupling name */
          id=i2+1+i4+1;
          extr_motcle(motcle,ch+id,&i5,&i6);

          if (i6-i5 < 1) {
            if (SYRTHES_LANG == FR){
              printf("\n -->  ERREUR cfd_init_couplage :"
                     " erreur dans le fichier de donnees \n");
              printf("        Ne retrouve pas le bon nom de couplage\n");
              printf("        mot-cles COUPLAGE_SURF_FLUIDE NOM_COUPLAGE LISTE_SELECTION\n");
            }
            else if (SYRTHES_LANG == EN){
              printf("\n --> ERROR cfd_init_couplage:"
                     " error in data file when defining the boundary conditions\n"); 
              printf("       Cannot find the related cfd coupling name\n");
              printf("       keyword COUPLAGE_SURF_FLUIDE COUPLING_NAME SELECTION_LIST\n");
            }
            syrthes_exit(1);
          }

          _add_name(motcle, n_couplings, &n_couplings_max, &names);
          n_couplings++;

        }
      } /* CLIM */
    }
  } /* End of while */

  fseek(fdata,0,SEEK_SET);
  while (fgets(ch,400,fdata)) {
    if (ch[0]!='/' && strlen(ch)>1){
      extr_motcle_(motcle,ch,&i1,&i2);

      if (!strcmp(motcle,"CLIM")) {
	extr_motcle(motcle,ch+i2+1,&i3,&i4);

	if (!strcmp(motcle,"COUPLAGE_VOL_FLUIDE")) {

          id=i2+1+i4+1;
          extr_motcle(motcle,ch+id,&i5,&i6);

          if (i6-i5 < 1) {
            if (SYRTHES_LANG == FR){
              printf("\n -->  ERREUR cfd_init_couplage :"
                     " erreur dans le fichier de donnees \n");
              printf("        Ne retrouve pas le bon nom de couplage\n");
              printf("        mot-cles COUPLAGE_VOL_FLUIDE NOM_COUPLAGE LISTE_SELECTION\n");
            }
            else if (SYRTHES_LANG == EN) {
              printf("\n --> ERROR cfd_init_couplage:"
                     " error in data file when defining the boundary conditions\n"); 
              printf("       Cannot find the related cfd coupling name\n");
              printf("       keyword COUPLAGE_VOL_FLUIDE COUPLING_NAME SELECTION_LIST\n");
            }
            syrthes_exit(1);
          }

          _add_name(motcle, n_couplings, &n_couplings_max, &names);
          n_couplings++;

        }
      }
    }
  } /* End of while */

  cfd->n_couplings = 0;

  if (n_couplings > 0) {

    char *ref_name;

    /* Sort names */

    array = (int *)malloc(sizeof(int)*n_couplings);
    for (i1 = 0; i1 < n_couplings; i1++)
      array[i1] = i1;

    _sort_names(n_couplings, names, array);

    /* Delete redundancy */

    cfd->n_couplings = 1;
    ref_name = names[array[0]];

    for (i1 = 0; i1 < n_couplings - 1; i1++) {
      if (strcmp(ref_name, names[array[i1+1]]) != 0) {
        cfd->n_couplings += 1;
        ref_name = names[array[i1+1]];
      }
    }

    cfd->c_names = (char **)malloc(cfd->n_couplings*sizeof(char *));

    ref_name = names[array[0]];
    len = strlen(ref_name);
    cfd->c_names[0] = (char *)malloc(sizeof(char)*(len+1));
    strncpy(cfd->c_names[0], ref_name, len);
    cfd->c_names[0][len] = '\0';

    cfd->n_couplings = 1;

    for (i1 = 0; i1 < n_couplings - 1; i1++) {
      if (strcmp(ref_name, names[array[i1+1]]) != 0) {
        ref_name = names[array[i1+1]];
        len = strlen(ref_name);
        cfd->c_names[cfd->n_couplings] = (char *)malloc(sizeof(char)*(len+1));
        strncpy(cfd->c_names[cfd->n_couplings], ref_name, len);
        cfd->c_names[cfd->n_couplings][len] = '\0';
        cfd->n_couplings += 1;
      }
    }

    /* Free memory */

    for (i1 = 0; i1 < cfd->n_couplings; i1++)
      free(names[i1]);
    free(array);

  } /* n_couplings > 0 */

  free(names);

}

/*|======================================================================|
  | SYRTHES 4.3                                  COPYRIGHT EDF 2009-2010 |
  |======================================================================|
  | AUTEURS  : J. BONELLE                                                |
  |======================================================================|
  |        Initialisation des couplages CFD                              |
  |        (niveau structure de couplage)                                |
  |======================================================================| */
void cfd_init_couplage(struct SDparall sdparall, int ndim, struct Cfd *cfd)
{
  int i1,i2,i3,i4,i5,i6,id,one_more;
  int coupling_id;
  syr_cfd_coupling_type_t  *ctype = NULL;

  /* --------------------- */
  /* Verifications de base */
  /* --------------------- */

  if (cfd->do_coupling==0 &&(sdparall.nparts==1 || sdparall.rang==0)) {
    if (SYRTHES_LANG == FR) {
      printf("\n -->  ERREUR cfd_init : SYRTHES doit etre couple a au moins 1 Saturne \n");
      printf("                          voir l'option --name de la ligne de commandes\n");
    }
    else if (SYRTHES_LANG == EN) {
      printf("\n --> ERROR cfd_init : SYRTHE must be coupled to at least 1 Saturne\n"); 
      printf("                        see option --name of the command line\n");
    }
    syrthes_exit(1);
  }

  /* Read syrthes.data to find how many couplings are defined and their related names */

  _add_coupling_names(cfd);

  /* By default, initialize with no coupling */

  ctype = (syr_cfd_coupling_type_t *)malloc(cfd->n_couplings*sizeof(syr_cfd_coupling_type_t));

  for (i1 = 0; i1 < cfd->n_couplings; i1++)
    ctype[i1] = SYR_NO_COUPLING;

  /* Y-a-t-il couplage surfacique ? */
  /* ------------------------------ */

  fseek(fdata,0,SEEK_SET);
  while (fgets(ch,CHLONG,fdata)) {
    if (ch[0]!='/' && strlen(ch)>1){
      extr_motcle_(motcle,ch,&i1,&i2);
      if (!strcmp(motcle,"CLIM")) {
	extr_motcle(motcle,ch+i2+1,&i3,&i4);

	if (!strcmp(motcle,"COUPLAGE_SURF_FLUIDE")) {

          /* Identify related cfd coupling name */
          id=i2+1+i4+1;
          extr_motcle(motcle,ch+id,&i5,&i6);

          if (i6-i5 < 1) {
            if (SYRTHES_LANG == FR){
              printf("\n -->  ERREUR cfd_init_couplage :"
                     " erreur dans le fichier de donnees \n");
              printf("        Ne retrouve pas le bon nom de couplage\n");
              printf("        mot-cles COUPLAGE_SURF_FLUIDE NOM_COUPLAGE LISTE_SELECTION\n");
            }
            else if (SYRTHES_LANG == EN){
              printf("\n --> ERROR cfd_init_couplage :"
                     " error in data file when defining the boundary conditions\n"); 
              printf("       Cannot find the related cfd coupling name\n");
              printf("       keyword COUPLAGE_SURF_FLUIDE COUPLING_NAME SELECTION_LIST\n");
            }
            syrthes_exit(1);
          }

          /* Find related cfd coupling */
          coupling_id = 0;
          one_more = 1;
          while (coupling_id < cfd->n_couplings && one_more == 1) {
            if (!strncmp(motcle, cfd->c_names[coupling_id], i6-i5))
              one_more = 0; /* Stop search */
            else
              coupling_id++;
          }

          if (one_more == 1) { /* Coupling name not found */
            if (SYRTHES_LANG == FR){
              printf("\n -->  ERREUR cfd_init_couplage :"
                     " erreur dans le fichier de donnees \n");
              printf("        Ne retrouve pas le bon nom de couplage\n");
              printf("        mot-cles COUPLAGE_SURF_FLUIDE NOM_COUPLAGE LISTE_SELECTION\n");
            }
            else if (SYRTHES_LANG == EN){
              printf("\n --> ERROR cfd_init_couplage :"
                     " error in data file when defining the boundary conditions\n"); 
              printf("       Cannot find the related cfd coupling name\n");
              printf("       keyword COUPLAGE_SURF_FLUIDE COUPLING_NAME SELECTION_LIST\n");
            }
            syrthes_exit(1);
          }

          ctype[coupling_id] = SYR_SURF_COUPLING;
        
        } /* Couplage surfacique */
      } /* Mot-clé = CLIM */

    }
  } /* End of while */

  /* Y-a-t-il couplage volumique ? */
  /* ----------------------------- */

  fseek(fdata,0,SEEK_SET);
  while (fgets(ch,CHLONG,fdata)) {
    if (ch[0]!='/' && strlen(ch)>1){
      extr_motcle_(motcle,ch,&i1,&i2);

      if (!strcmp(motcle,"CLIM")) {
	extr_motcle(motcle,ch+i2+1,&i3,&i4);

	if (!strcmp(motcle,"COUPLAGE_VOL_FLUIDE")) {

          id=i2+1+i4+1;
          extr_motcle(motcle,ch+id,&i5,&i6);

          if (i6-i5 < 1) {
            if (SYRTHES_LANG == FR){
              printf("\n -->  ERREUR cfd_init_couplage :"
                     " erreur dans le fichier de donnees \n");
              printf("        Ne retrouve pas le bon nom de couplage\n");
              printf("        mot-cles COUPLAGE_VOL_FLUIDE NOM_COUPLAGE LISTE_SELECTION\n");
            }
            else if (SYRTHES_LANG == EN){
              printf("\n --> ERROR cfd_init_couplage :"
                     " error in data file when defining the boundary conditions\n"); 
              printf("       Cannot find the related cfd coupling name\n");
              printf("       keyword COUPLAGE_VOL_FLUIDE COUPLING_NAME SELECTION_LIST\n");
            }
            syrthes_exit(1);
          }

          /* Find related cfd coupling */
          coupling_id = 0;
          one_more = 1;
          while (coupling_id < cfd->n_couplings && one_more == 1) {
            if (!strncmp(motcle, cfd->c_names[coupling_id], i6-i5))
              one_more = 0; /* Stop search */
            else
              coupling_id++;
          }

          if (one_more == 1) {
            if (SYRTHES_LANG == FR){
              printf("\n -->  ERREUR cfd_init_couplage :"
                     " erreur dans le fichier de donnees \n");
              printf(" Ne retrouve pas le bon nom de couplage\n");
              printf(" mot-cles COUPLAGE_VOL_FLUIDE NOM_COUPLAGE LISTE_SELECTION\n");
            }
            else if (SYRTHES_LANG == EN){
              printf("\n --> ERROR cfd_init_couplage:"
                     " error in data file when defining the boundary conditions\n"); 
              printf("       Cannot find the related cfd coupling name\n");
              printf("       keyword COUPLAGE_VOL_FLUIDE COUPLING_NAME SELECTION_LIST\n");
            }
            syrthes_exit(1);
          }

          if (ctype[coupling_id] == SYR_NO_COUPLING)
            ctype[coupling_id] = SYR_VOL_COUPLING;
          else
            ctype[coupling_id] = SYR_SURFVOL_COUPLING;

        } /* Couplage volumique */
      } /* Mot-clé = CLIM */

    }
  } /* End of while */

  /* Create syr_cfd_coupling_t structures */
  /* ------------------------------------ */

  cfd->couplings = (syr_cfd_coupling_t **)malloc(cfd->n_couplings*sizeof(syr_cfd_coupling_t *));

  /* Check memory allocation */
  if (cfd->couplings == NULL){
    if (SYRTHES_LANG == FR)
      printf("\n\n ERREUR D'ALLOCATION dans la fonction cfd_init_couplage\n\n");
    else if (SYRTHES_LANG == EN)
      printf("\n\n ALLOCATION ERROR in function cfd_init_couplage\n\n");
    syrthes_exit(1);
  }

  cfd->coupl_surf=0; /* By default, no surface coupling */
  cfd->coupl_vol=0;  /* By default, no volume coupling */

  for (i1 = 0; i1 < cfd->n_couplings; i1++) {

    if (ctype[i1] == SYR_SURF_COUPLING)
      cfd->coupl_surf += 1;
    else if (ctype[i1] == SYR_VOL_COUPLING)
      cfd->coupl_vol += 1;
    else if (ctype[i1] == SYR_SURFVOL_COUPLING)
      cfd->coupl_surf += 1, cfd->coupl_vol += 1;
    else {

      assert(ctype[i1] == SYR_NO_COUPLING);

      if (SYRTHES_LANG == FR){
	printf("\n -->  ERREUR cfd_init_couplage :"
               " erreur dans le fichier de donnees \n");
	printf("        Aucun couplage avec la CFD n'est defini\n"); 
	printf("        mot-cles COUPLAGE_SURF_FLUIDE NOM SELECTION\n");
	printf("        ou COUPLAGE_VOL_FLUIDE NOM SELECTION\n");
      }
      else if (SYRTHES_LANG == EN){
	printf("\n --> ERROR cfd_init_couplage:"
               " error in data file when defining the boundary conditions\n"); 
	printf("       keyword COUPLAGE_SURF_FLUIDE  NAME SELECTION_LIST\n");
	printf("       or COUPLAGE_VOL_FLUIDE  NAME SELECTION_LIST\n");
      }
      syrthes_exit(1);

    }

    cfd->couplings[i1] = syr_cfd_coupling_create(ndim,
                                                 cfd->c_names[i1],
                                                 ctype[i1]);

  }

  nbCFD = cfd->n_couplings;

  /* Initialize MPI communicators */
  syr_cfd_coupling_init_comm(cfd->n_couplings, cfd->couplings);

#if 1 /* Debugging information */
  printf("\nCfd struct\n");
  printf("  app_num: %8d\n", cfd->app_num);
  if (cfd->name != NULL)
    printf("  name: %s\n", cfd->name);
  printf("  do_coupling:   %3d\n", cfd->do_coupling);
  printf("  coupl_surf :   %3d\n", cfd->coupl_surf);
  printf("  coupl_vol  :   %3d\n", cfd->coupl_vol);
  printf("  n_couplings:   %3d\n", cfd->n_couplings);
  for (i1 = 0; i1 < cfd->n_couplings; i1++)
    printf("      %s\n", cfd->c_names[i1]);
  printf("\n");
#endif

  if (affich.cfd) {
    printf(">>>> nombre de codes CFD couples surfaciquement avec SYRTHES =%d\n",
           cfd->coupl_surf);
    printf(">>>> nombre de codes CFD couples volumiquement avec SYRTHES =%d\n",
           cfd->coupl_vol);
  }

  /* Free locally allocated memory */
  free(ctype);

}

/*|======================================================================|
  | SYRTHES 4.3                                  COPYRIGHT EDF 2009-2010 |
  |======================================================================|
  | AUTEURS  : J. BONELLE, I. RUPP                                       |
  |======================================================================|
  |        Définition des structures géométriques                        |
  |        liées à un couplage surfacique                                |
  |======================================================================| */
void cfd_surf_init(struct Maillage   maillnodes,
		   struct MaillageCL maillnodeus,
		   struct Climcfd   *scoupf,
		   struct SDparall   sdparall,
		   struct Cfd       *cfd)
{
  int  i,j,n,nf,nfflui,nbtotall,cid,one_more;
  int  i1,i2,i3,i4,i5,i6,id,numcfd,nb,nbtot;
  int  *icoupl = NULL, *nbstat = NULL, *nbfcall = NULL;
  syr_cfd_coupling_t  *coupl = NULL;
  int ok=0;
  
  /* Tableau temporaire de stockage des references pour le couplage surfacique */
  icoupl=(int*)malloc(maillnodeus.nelem*sizeof(int));
  verif_alloue_int1d("cfd_surf_init",icoupl);
  for (n=0;n<maillnodeus.nelem;n++)  icoupl[n]=-1;
  perfo.mem_cond+=maillnodeus.nelem*sizeof(int);

  /* Premier passage : couplage avec combien de codes CFD */
  /* ---------------------------------------------------- */

  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata)) {

    if (ch[0]!='/' && strlen(ch)>1) {

      extr_motcle_(motcle,ch,&i1,&i2);

      if (!strcmp(motcle,"CLIM")) {

        extr_motcle(motcle,ch+i2+1,&i3,&i4);
        id=i2+1+i4+1;
	      
        if (!strcmp(motcle,"COUPLAGE_SURF_FLUIDE")) {

          extr_motcle(motcle,ch+id,&i5,&i6);

          /* Find related cfd coupling */
          cid = 0;
          one_more = 1;
          while (cid < cfd->n_couplings && one_more == 1) {
            if (!strncmp(motcle, cfd->c_names[cid], i6-i5))
              one_more = 0; /* Stop search */
            else
              cid++;
          }

          assert(cid < cfd->n_couplings);
          coupl = cfd->couplings[cid];
          assert(   syr_cfd_coupling_get_type(coupl) == SYR_SURF_COUPLING
                 || syr_cfd_coupling_get_type(coupl) == SYR_SURFVOL_COUPLING);

          id = i2+1+i4+1+i6+1;
          rep_listint(ilist,&nb,ch+id);

          /* Tag nodes implied in the coupling process */
          for (n=0; n<nb; n++)
            for (i=0; i<maillnodeus.nelem; i++)
              if (maillnodeus.nrefe[i]==ilist[n]) {
                assert(icoupl[i] == -1); /* Be sure that the current node is not
                                            already tagged */
                icoupl[i]=cid;
              }

        } /* COUPLAGE_SURF_FLUIDE */

      } /* CLIM */

    } /* Not a comment */

  } /* End of while */

  /* Allocation pour le tableau de structures de couplage */
  /* ---------------------------------------------------- */
  scoupf->ndmat=maillnodeus.ndmat;

  /* Combien d'elements couples par code */
  scoupf->nelem=(int*)malloc(cfd->n_couplings*sizeof(int));
  for (i=0; i<cfd->n_couplings; i++) scoupf->nelem[i]=0; 

  for (n=0; n<maillnodeus.nelem; n++) 
    if (icoupl[n]>-1) scoupf->nelem[icoupl[n]]++;

  scoupf->nume=(int**)malloc(cfd->n_couplings*sizeof(int*));
  for (i=0; i<cfd->n_couplings; i++) {
    scoupf->nume[i]=NULL;
    if(scoupf->nelem[i]>0) {
      scoupf->nume[i]=(int*)malloc(scoupf->nelem[i]*sizeof(int*));
      verif_alloue_int1d("cfd_surf_init",scoupf->nume[i]);
      perfo.mem_cond+=scoupf->nelem[i]*sizeof(int);
    }
  }

  /* Liste des faces solides couplees avec le fluide */
  /* ----------------------------------------------- */

  for (nbtot=i=0; i<cfd->n_couplings; i++) nbtot+=scoupf->nelem[i];

  /* Verification de la presence de face couplees */
  nbfcall=(int*)malloc(cfd->n_couplings*sizeof(int));
  for (i=0; i<cfd->n_couplings; i++) nbfcall[i]=0; 

  nbtotall = somme_int_parall(nbtot);
  for (i=0; i<cfd->n_couplings; i++)
    nbfcall[i] = somme_int_parall(scoupf->nelem[i]);

  if (!nbtotall && (sdparall.nparts==1 || sdparall.rang==0)) {
    if (SYRTHES_LANG == FR)
      printf("\n -->  ERREUR cfd_surf_init :"
             " Il n'y a aucune face solide couplee au fluide\n\n"); 
    else if (SYRTHES_LANG == EN)
      printf("\n --> ERROR cfd_surf_init:"
             " There is no solid face coupled with CFD code\n\n"); 
    syrthes_exit(1);
  }

  /* Creation des listes de faces couplees par code fluide */
  nbstat=(int*)malloc(cfd->n_couplings*sizeof(int));
  for (i=0; i<cfd->n_couplings; i++) nbstat[i]=0;
 
  for (n=0; n<maillnodeus.nelem; n++) 
    if (icoupl[n]>-1) {scoupf->nume[icoupl[n]][nbstat[icoupl[n]]]=n; nbstat[icoupl[n]]++;}

  free(nbstat);

  /* Affichage de controle */
  if (affich.cfd)
    for (n=0; n<cfd->n_couplings; n++) {
      printf(" Liste des faces couplees (dans nodeus)\n");
      for (i=0; i<scoupf->nelem[n]; i++)
        if (maillnodeus.ndim==2)
          printf("CFD code <%d> scoupf->nume[%d]=%d  noeuds %d %d\n",
                 n,i,scoupf->nume[n][i],
                 maillnodeus.node[0][scoupf->nume[n][i]],
                 maillnodeus.node[1][scoupf->nume[n][i]]);
        else
          printf("CFD code <%d> scoupf->nume[%d]=%d  noeuds %d %d %d\n",
                 n,i,scoupf->nume[n][i],
                 maillnodeus.node[0][scoupf->nume[n][i]],
                 maillnodeus.node[1][scoupf->nume[n][i]],
                 maillnodeus.node[2][scoupf->nume[n][i]]);
    }

  /* Allocation pour les valeurs de la CL */
  /* ------------------------------------ */
  /* (dimensionne au max des nbre d'elts a echanger) */

  scoupf->tfluid=(double**)malloc(cfd->n_couplings*sizeof(double));
  scoupf->hfluid=(double**)malloc(cfd->n_couplings*sizeof(double));

  for (n=0; n<cfd->n_couplings; n++) {
    scoupf->tfluid[n]=NULL;
    scoupf->hfluid[n]=NULL;
    if (scoupf->nelem[n]>0) {
      scoupf->tfluid[n]=(double*)malloc(scoupf->nelem[n]*sizeof(double));
      scoupf->hfluid[n]=(double*)malloc(scoupf->nelem[n]*sizeof(double));
      verif_alloue_double1d("cfd_surf_init",scoupf->tfluid[n]);
      verif_alloue_double1d("cfd_surf_init",scoupf->hfluid[n]);
      perfo.mem_cond+=(scoupf->nelem[n]*sizeof(double));
    }
  }

  /* Initialisation des couplages avec les codes de CFD */
  /* -------------------------------------------------- */

  for (n=0; n<cfd->n_couplings; n++) {

    coupl = cfd->couplings[n];

    if (   syr_cfd_coupling_get_type(coupl) == SYR_SURF_COUPLING
        || syr_cfd_coupling_get_type(coupl) == SYR_SURFVOL_COUPLING) {

      /* Correspondances geometriques des mailles fluides et solides */
      syr_cfd_coupling_set_faces(coupl,
                                 scoupf->nelem[n], scoupf->nume[n],
                                 (const int **)maillnodeus.node,
                                 (const double **)maillnodes.coord);
      cfd->couplings[n] = coupl;
      ok=1;
    }

  } /* End of loop on cfd couplings */


  /* ecriture du maillage solide couple a la cfd */
  ecrire_geom_cplcfd(nomcplcfdg,maillnodes,maillnodeus,*scoupf,*cfd);


  /* Impressions */
  /* ----------- */

  if ((sdparall.nparts==1 || sdparall.rang==0) && ok==1) {
    if (SYRTHES_LANG == FR)
      printf("\n *** NOMBRE DE FACES COUPLEES EN SURFACIQUE AVEC UN CODE DE CFD"
             " (cfd_surf_init)\n");
    else if (SYRTHES_LANG == EN)
      printf("\n *** NUMBER OF SURFACE COUPLED FACES WITH CFD CODE (cfd_surf_init)\n");
      
    printf("                         |---------------|\n");
    printf("                         |   Nb elements |\n");
    printf("      |------------------|---------------|\n");
    for (i=0; i<cfd->n_couplings; i++)
      {
	coupl = cfd->couplings[i];

	if (   syr_cfd_coupling_get_type(coupl) == SYR_SURF_COUPLING
	    || syr_cfd_coupling_get_type(coupl) == SYR_SURFVOL_COUPLING) 
	  printf("      | CFD code %2d      |    %10d |\n",i+1,nbfcall[i]);
      }
    printf("      |----------------------------------|\n\n"); 
      
  }

}

/*|======================================================================|
  | SYRTHES 4.3                                  COPYRIGHT EDF 2009-2010 |
  |======================================================================|
  | AUTEURS  : J. BONELLE, I. RUPP                                       |
  |======================================================================|
  |        Echange des données entre syrthes et les codes de CFD         |
  |======================================================================| */
void cfd_trans_surf(struct Maillage maillnodes,struct MaillageCL maillnodeus,
                    struct Climcfd    scoupf,
                    double           *tmps,
                    struct Cfd        cfd)
{
  int i,j,n,nf;
  double  syr_flux = 0.0;
  const double tiers=1./3.;

  /* Preparation de la temperature solide pour envoi au fluide */
  for (n=0; n<cfd.n_couplings; n++) { /* Pour chaque code fluide */

    syr_cfd_coupling_t  *coupl = cfd.couplings[n];

    if (   syr_cfd_coupling_get_type(coupl) == SYR_SURF_COUPLING
        || syr_cfd_coupling_get_type(coupl) == SYR_SURFVOL_COUPLING) {

      if (maillnodeus.ndim==2) {
        for (i=0; i<scoupf.nelem[n]; i++) {
          nf=scoupf.nume[n][i];
          scoupf.tfluid[n][i]=0.5*(  tmps[maillnodeus.node[0][nf]]
                                   + tmps[maillnodeus.node[1][nf]]);
        }
      }
      else {
        for (i=0; i<scoupf.nelem[n]; i++) {
          nf=scoupf.nume[n][i];
          scoupf.tfluid[n][i]= tiers*(  tmps[maillnodeus.node[0][nf]] 
                                      + tmps[maillnodeus.node[1][nf]] 
                                      + tmps[maillnodeus.node[2][nf]]);
        }
      }
      
      /* Echange fluide/solide */
      syr_cfd_coupling_face_vars(coupl, NULL, scoupf.tfluid[n], scoupf.hfluid[n]);

      /* Compute SYRTHES global thermal flux */
      syr_flux = cfd_bilsurf_conserv(scoupf.nelem[n],scoupf.nume[n],
				     scoupf.tfluid[n],scoupf.hfluid[n],
				     maillnodes,maillnodeus,tmps);

      /* Modify hfluid to ensure conservativity. Only if the CFD has
         activated this option (Should be by default). */
 
      syr_cfd_coupling_ensure_conservativity(coupl, syr_flux, scoupf.hfluid[n]);

      /* Rq : en sortie, scoupf.tfluid et scoupf.hfluid ont ete mis a jour
         par le fluide */

      if (affich.cfd) {
        if (SYRTHES_LANG == FR) {
          printf(" -->  transCFD : valeurs de t et h en provenance du fluide");
          for (i=0; i<scoupf.nelem[n]; i++)
            printf(" code %d face couplee=%d  tfluid=%f  hfluid=%f\n",
                   n+1,i,scoupf.tfluid[n][i],scoupf.hfluid[n][i]);
        }
        else if (SYRTHES_LANG == EN){
          printf(" -->  transCFD : t and h values from the fluid");
          for (i=0; i<scoupf.nelem[n]; i++)
            printf(" code %d coupled face=%d  tfluid=%f  hfluid=%f\n",
                   n+1,i,scoupf.tfluid[n][i],scoupf.hfluid[n][i]);
        }
        printf("\n");
      }

    } /* End if coupl->type == SYR_SURF_COUPL or SYR_SURFVOL_COUPL */

  } /* fin de la boucle sur les codes */

}

/*|======================================================================|
  | SYRTHES 4.3                                  COPYRIGHT EDF 2009-2010 |
  |======================================================================|
  | AUTEURS  : J. BONELLE, I. RUPP                                       |
  |======================================================================|
  |        Définition des structures géométriques                        |
  |        liées à un couplage volumique                                 |
  |======================================================================| */
void cfd_vol_init(struct Maillage maillnodes,
                  struct Climcfd *scouvf,
                  struct SDparall sdparall,
                  struct Cfd     *cfd)
{
  int i,j,n,nf,nfflui,nbtotall,one_more;
  int i1,i2,i3,i4,i5,i6,id,cid,numcfd,nb,nbtot;
  int *icoupl = NULL, *nbstat = NULL, *nbfcall = NULL;
  syr_cfd_coupling_t  *coupl = NULL;
  int ok=0;

  /* Tableau temporaire de stockage des references pour le couplage volumique */
  icoupl=(int*)malloc(maillnodes.nelem*sizeof(int));
  verif_alloue_int1d("cfd_vol_init",icoupl);
  for (n=0; n<maillnodes.nelem; n++)  icoupl[n]=-1;
  perfo.mem_cond+=maillnodes.nelem*sizeof(int);

  /* Premier passage : couplage avec combien de codes CFD */
  /* ---------------------------------------------------- */

  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata)) {

    if (ch[0]!='/' && strlen(ch)>1) {

      extr_motcle_(motcle,ch,&i1,&i2);

      if (!strcmp(motcle,"CLIM")) {

        extr_motcle(motcle,ch+i2+1,&i3,&i4);
        id=i2+1+i4+1;
	      
        if (!strcmp(motcle,"COUPLAGE_VOL_FLUIDE")) {

          extr_motcle(motcle,ch+id,&i5,&i6);

          /* Find related cfd coupling */
          cid = 0;
          one_more = 1;
          while (cid < cfd->n_couplings && one_more == 1) {
            if (!strncmp(motcle, cfd->c_names[cid], i6-i5))
              one_more = 0; /* Stop search */
            else
              cid++;
          }

          assert(cid < cfd->n_couplings);
          coupl = cfd->couplings[cid];
          assert(   syr_cfd_coupling_get_type(coupl) == SYR_VOL_COUPLING
                 || syr_cfd_coupling_get_type(coupl) == SYR_SURFVOL_COUPLING);

          id = i2+1+i4+1+i6+1;
          rep_listint(ilist,&nb,ch+id);

          /* Tag nodes implied in the coupling process */
          for (n=0; n<nb; n++)
            for (i=0; i<maillnodes.nelem; i++)
              if (maillnodes.nrefe[i]==ilist[n]) {
                assert(icoupl[i] == -1); /* Be sure that the current node is not
                                            already tagged */
                icoupl[i]=cid;
              }

        } /* COUPLAGE_VOL_FLUIDE */

      } /* CLIM */

    } /* Not a comment */

  } /* End of while */

  /* Allocation pour le tableau de structures de couplage */
  /* ---------------------------------------------------- */
  scouvf->ndmat=maillnodes.ndmat;

  /* Combien d'elements couples par code */
  scouvf->nelem=(int*)malloc(cfd->n_couplings*sizeof(int));
  for (i=0; i<cfd->n_couplings; i++) scouvf->nelem[i]=0; 

  for (n=0; n<maillnodes.nelem; n++) 
    if (icoupl[n]>-1) scouvf->nelem[icoupl[n]]++;

  scouvf->nume=(int**)malloc(cfd->n_couplings*sizeof(int*));
  for (i=0; i<cfd->n_couplings; i++) {
    scouvf->nume[i]=NULL;
    if(scouvf->nelem[i]>0) {
      scouvf->nume[i]=(int*)malloc(scouvf->nelem[i]*sizeof(int*));
      verif_alloue_int1d("cfd_vol_init",scouvf->nume[i]);
      perfo.mem_cond+=scouvf->nelem[i]*sizeof(int);
    }
  }
  
  /* Liste des elements solides couples avec le fluide */
  /* ------------------------------------------------- */

  for (nbtot=i=0; i<cfd->n_couplings; i++) nbtot+=scouvf->nelem[i];

  /* Verification de la presence d'elements couples */
  nbfcall=(int*)malloc(cfd->n_couplings*sizeof(int));
  for (i=0;i<cfd->n_couplings;i++) nbfcall[i]=0; 

  nbtotall=somme_int_parall(nbtot);
  for (i=0; i<cfd->n_couplings; i++)
    nbfcall[i]=somme_int_parall(scouvf->nelem[i]);

  if (!nbtotall && (sdparall.nparts==1 || sdparall.rang==0)) {
    if (SYRTHES_LANG == FR)
      printf("\n -->  ERREUR cfd_vol_init :"
             " Il n'y a aucun element solide couple au fluide\n\n"); 
    else if (SYRTHES_LANG == EN)
      printf("\n --> ERROR cfd_vol_init:"
             " There is no solid eleemnt coupled with CFD code\n\n"); 
    syrthes_exit(1);
  }

  /* Creation des listes d'elements couplees par code fluide */
  nbstat=(int*)malloc(cfd->n_couplings*sizeof(int));
  for (i=0; i<cfd->n_couplings; i++) nbstat[i]=0;
 
  for (n=0; n<maillnodes.nelem; n++) 
    if (icoupl[n]>-1) {scouvf->nume[icoupl[n]][nbstat[icoupl[n]]]=n; nbstat[icoupl[n]]++;}

  free(nbstat);

  /* Affichage de controle */
  if (affich.cfd)
    for (n=0; n<cfd->n_couplings; n++) {
      printf(" Liste des elements couples (dans nodes)\n");
      for (i=0; i<scouvf->nelem[n]; i++)
        if (maillnodes.ndim==2)
          printf("CFD code <%d> scouvf->nume[%d]=%d  noeuds %d %d\n",
                 n,i,scouvf->nume[n][i],
                 maillnodes.node[0][scouvf->nume[n][i]],
                 maillnodes.node[1][scouvf->nume[n][i]]);
	  else
	    printf("CFD code <%d> scouvf->nume[%d]=%d  noeuds %d %d %d\n",
		   n,i,scouvf->nume[n][i],
		   maillnodes.node[0][scouvf->nume[n][i]],
		   maillnodes.node[1][scouvf->nume[n][i]],
		   maillnodes.node[2][scouvf->nume[n][i]]);
      }

  /* Allocation pour les valeurs de la CL */
  /* ------------------------------------ */
  /* (dimensionne au max des nbre d'elts a echanger) */

  scouvf->tfluid=(double**)malloc(cfd->n_couplings*sizeof(double));
  scouvf->hfluid=(double**)malloc(cfd->n_couplings*sizeof(double));

  for (n=0; n<cfd->n_couplings; n++) {
    scouvf->tfluid[n]=NULL;
    scouvf->hfluid[n]=NULL;
    if (scouvf->nelem[n]) {
      scouvf->tfluid[n]=(double*)malloc(scouvf->nelem[n]*sizeof(double));
      scouvf->hfluid[n]=(double*)malloc(scouvf->nelem[n]*sizeof(double));
      verif_alloue_double1d("cfd_vol_init",scouvf->tfluid[n]);
      verif_alloue_double1d("cfd_vol_init",scouvf->hfluid[n]);
      perfo.mem_cond+=(scouvf->nelem[n]*sizeof(double));
    }
  }

  /* Initialisation des couplages avec les codes de CFD */
  /* -------------------------------------------------- */

  for (n=0; n<cfd->n_couplings; n++) {

    coupl = cfd->couplings[n];

    if (   syr_cfd_coupling_get_type(coupl) == SYR_VOL_COUPLING
        || syr_cfd_coupling_get_type(coupl) == SYR_SURFVOL_COUPLING) {

      /* Correspondances geometriques des mailles fluides et solides */
      syr_cfd_coupling_set_cells(coupl,
                                 scouvf->nelem[n], scouvf->nume[n],
                                 (const int **)maillnodes.node,
                                 (const double **)maillnodes.coord);
      cfd->couplings[n] = coupl;
      ok=1;

    }

  } /* End of loop on cfd couplings */

  /* Impressions */
  /* ----------- */
  if ((sdparall.nparts==1 || sdparall.rang==0) && ok==1) {
    if (SYRTHES_LANG == FR)
      printf("\n *** NOMBRE D'ELEMENTS COUPLES EN VOLUMIQUE AVEC"
             " UN CODE DE CFD (cfd_vol_init)\n");
    else if (SYRTHES_LANG == EN)
      printf("\n *** NUMBER OF VOLUME COUPLED ELEMENTS WITH"
             " CFD CODE (cfd_vol_init)\n");
      
    printf("                         |---------------|\n");
    printf("                         |   Nb elements |\n");
    printf("      |------------------|---------------|\n");
    for (i=0;i<cfd->n_couplings;i++)
      {
	coupl = cfd->couplings[i];

	if (   syr_cfd_coupling_get_type(coupl) == SYR_VOL_COUPLING
            || syr_cfd_coupling_get_type(coupl) == SYR_SURFVOL_COUPLING) 
	  printf("      | CFD code  %2d  |    %10d |\n",i+1,nbfcall[i]);
      }
    printf("      |----------------------------------|\n\n"); 
      
  }

}

/*|======================================================================|
  | SYRTHES 4.3                                  COPYRIGHT EDF 2009-2010 |
  |======================================================================|
  | AUTEURS  : J. BONELLE, I. RUPP                                       |
  |======================================================================|
  |        Echange des données entre syrthes et les codes de CFD         |
  |======================================================================| */
void cfd_trans_vol(struct Maillage maillnodes,
                   struct Climcfd scouvf,
                   double *tmps,
                   struct Cfd cfd)
{
  int i,j,n,ne;

  const double tiers=1./3.;
  const double quart=0.25;

  /* Preparation de la temperature solide pour envoi au fluide */
  for (n=0;n<cfd.n_couplings;n++) { /* Pour chaque code fluide */

    syr_cfd_coupling_t  *coupl = cfd.couplings[n];

    if (   syr_cfd_coupling_get_type(coupl) == SYR_VOL_COUPLING
        || syr_cfd_coupling_get_type(coupl) == SYR_SURFVOL_COUPLING) {

      if (maillnodes.ndim==2) {
        for (i=0; i<scouvf.nelem[n]; i++) {
          ne=scouvf.nume[n][i];
          scouvf.tfluid[n][i]=tiers*(  tmps[maillnodes.node[0][ne]] 
                                     + tmps[maillnodes.node[1][ne]]
                                     + tmps[maillnodes.node[2][ne]]);
          /*          printf("%d %g %d %d %d\n",
                      i, scouvf.tfluid[n][i], ne,
                      maillnodes.node[0][ne], maillnodes.node[1][ne]); */
        }
      }
      else {
        for (i=0; i<scouvf.nelem[n]; i++) {
          ne=scouvf.nume[n][i];
          scouvf.tfluid[n][i]= quart*(  tmps[maillnodes.node[0][ne]] 
                                      + tmps[maillnodes.node[1][ne]] 
                                      + tmps[maillnodes.node[2][ne]]
                                      + tmps[maillnodes.node[3][ne]]);
        }
      }

      /* Echange fluide/solide */
      syr_cfd_coupling_cell_vars(coupl, NULL, scouvf.tfluid[n], scouvf.hfluid[n]);

      /* Rq : en sortie, scouvf.tfluid et scouvf.hfluid ont ete ;is a jour par le fluide */
  
      if (affich.cfd) {
        if (SYRTHES_LANG == FR) {
          printf(" -->  transCFDvol : valeurs de t et h en provenance du fluide");
          for (i=0; i<scouvf.nelem[n]; i++)
            printf(" code %d elt couple=%d  tfluid=%f  hfluid=%f\n",
                   n+1,i,scouvf.tfluid[n][i],scouvf.hfluid[n][i]);
        }
        else if (SYRTHES_LANG == EN) {
          printf(" -->  transCFDvol : t and h values from the fluid");
          for (i=0; i<scouvf.nelem[n]; i++)
            printf(" code %d coupled elt=%d  tfluid=%f  hfluid=%f\n",
                   n+1,i,scouvf.tfluid[n][i],scouvf.hfluid[n][i]);
        }
        printf("\n");
      }

    } /* End if coupl->type == SYR_VOL_COUPL or SYR_SURFVOL_COUPL */

  } /* fin de la boucle sur les codes */

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Fin du couplage SYRTHES/CFD - Liberation des structures de couplage  |
  |======================================================================| */
void cfd_fin(struct Cfd *cfd)
{
  int n;

  if (cfd->n_couplings > 0) {
    for (n = 0; n < cfd->n_couplings; n++)
      syr_cfd_coupling_destroy(&(cfd->couplings[n]));

    free(cfd->c_names);
    free(cfd->couplings);

    syr_cfd_coupling_finalize();
  }

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : J. BONELLE        P                                       |
  |======================================================================|
  | Supervision des couplages - Est-ce la fin du couplage ?              |
  |======================================================================| */
void cfd_synchro(struct PasDeTemps *pasdetemps, 
		 struct Cfd cfd)
{
  int n;

  int  is_end = 0;
  int  flag = 0;

  if (pasdetemps->lstops) {
    is_end=1;
    flag = PLE_COUPLING_STOP;
  }
      
  /* Synchronisation des état des codes couples */

  syr_cfd_coupling_sync_apps(flag,                  /* flag */
			     pasdetemps->ntsyr,     /* current time step id */
			     &(pasdetemps->ntsmax), /* max time step id */
			     &(pasdetemps->rdtts)); /* time step */

  /* Recuperation de l'etat du code CFD */
  if (is_end || pasdetemps->ntsyr >= pasdetemps->ntsmax)
    pasdetemps->lstops=1;

}

#endif /* _SYRTHES_CFD_ */
