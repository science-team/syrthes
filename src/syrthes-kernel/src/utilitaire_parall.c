/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "syr_usertype.h"
#include "syr_bd.h"
#include "syr_tree.h"
#include "syr_parall.h"
#include "syr_proto.h"

#ifdef _SYRTHES_MPI_
#include "mpi.h"
MPI_Status status;
#endif

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | somme_parall                                                         |
  |  somme total d'une grandeur entiere presente sur l'ensemble des proc |
  |======================================================================| */
int somme_int_parall(int nb)
{
  int i,nbi,nbtot;

  nbtot=nb;

#ifdef _SYRTHES_MPI_
  if (syrglob_nparts>1)
    MPI_Allreduce(&nb,&nbtot,1,MPI_INT,MPI_SUM,syrglob_comm_world);
#endif

  return nbtot;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | somme_parall                                                         |
  |   somme total d'une grandeur double presente sur l'ensemble des proc |
  |======================================================================| */
double somme_double_parall(double x)
{
  int i;
  double xi,xtot;

  xtot=x;

#ifdef _SYRTHES_MPI_
  if (syrglob_nparts>1)
    MPI_Allreduce(&x,&xtot,1,MPI_DOUBLE,MPI_SUM,syrglob_comm_world);
#endif

  return xtot;
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | max_parall                                                           |
  |     max total d'une grandeur int presente sur l'ensemble des proc    |
  |======================================================================| */
int max_int_parall(int n)
{
  int max_glob;

  max_glob=n;
  
#ifdef _SYRTHES_MPI_
  if (syrglob_nparts>1)
    MPI_Allreduce(&n,&max_glob,1,MPI_INT,MPI_MAX,syrglob_comm_world);
#endif

  return max_glob;
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | min_parall                                                           |
  |     min total d'une grandeur int presente sur l'ensemble des proc    |
  |======================================================================| */
int min_int_parall(int n)
{
  int min_glob;

  min_glob=n;
  
#ifdef _SYRTHES_MPI_
  if (syrglob_nparts>1)
    MPI_Allreduce(&n,&min_glob,1,MPI_INT,MPI_MIN,syrglob_comm_world);
#endif

  return min_glob;
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | max_parall                                                           |
  |     max total d'une grandeur double presente sur l'ensemble des proc |
  |======================================================================| */
double max_double_parall(double x)
{
  double max_glob;

  max_glob=x;
  
#ifdef _SYRTHES_MPI_
  if (syrglob_nparts>1)
    MPI_Allreduce(&x,&max_glob,1,MPI_DOUBLE,MPI_MAX,syrglob_comm_world);
#endif

  return max_glob;
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | min_parall                                                           |
  |     min total d'une grandeur double presente sur l'ensemble des proc |
  |======================================================================| */
double min_double_parall(double x)
{
  double min_glob;

  min_glob=x;
  
#ifdef _SYRTHES_MPI_
  if (syrglob_nparts>1)
    MPI_Allreduce(&x,&min_glob,1,MPI_DOUBLE,MPI_MIN,syrglob_comm_world);
#endif

  return min_glob;
}
