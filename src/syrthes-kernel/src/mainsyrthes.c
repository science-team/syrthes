/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <math.h>
# include <assert.h>

#include "syr_usertype.h"
# include "syr_bd.h"
# include "syr_parall.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_proto.h"

#ifdef _SYRTHES_MPI_
# include "mpi.h"
#endif

/* Set to 0 to switch off this functionnality */
#define _REDIRECT_STDERR 1

# include "syr_cfd.h"
struct Cfd cfd;

char nomdata[200];
int syrglob_rang=0;
int syrglob_nparts=1;
int syrglob_npartsray=1;

#ifdef _SYRTHES_MPI_
  MPI_Comm syrglob_comm_world=MPI_COMM_NULL; 
#endif

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |  Programme principal                                                 |
  |======================================================================| */
main(argc,argv)
     int     argc;
     char    *argv[];
{
  int len,rang_deb,numarg,rang,size,nbproc_ray;
  double timedeb,timefin;
  char *log_name = NULL; /* name of log file (default: log to stdout) */

  const  char *s;

  rang=0; size=1;

#ifdef _SYRTHES_CFD_ /* Default initialization */
  cfd.do_coupling = 0;
  cfd.app_num = -1;
  cfd.name = NULL;
  cfd.n_couplings  = 0;
  cfd.c_names = NULL;
  cfd.coupl_surf = 0;
  cfd.coupl_vol = 0;
  cfd.couplings = NULL;
#endif

#ifdef _SYRTHES_MPI_
  rang_deb=0;
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&rang);
  MPI_Comm_size(MPI_COMM_WORLD,&size);
  nbproc_ray = -1;
  syrglob_rang=rang;
  syrglob_nparts=size;
  syrglob_comm_world=MPI_COMM_WORLD;
#endif

  /*============================ Lecture des arguments =======================*/
  /*==========================================================================*/

  /* Change working directory if necessary */

  numarg=0;
  while (++numarg < argc) {
    s = argv[numarg];
    if (strcmp(s, "-w") == 0 || strcmp(s, "-wdir") == 0) {
      if (++numarg < argc) {
        s = argv[numarg];
        if (chdir(s) != 0) {
	  if (SYRTHES_LANG == FR)
	    printf("\n\n SYRTHES : Erreur lors du changement de repertoire de travail : %s\n",s);
	  else if (SYRTHES_LANG == EN)
	    printf("\n\n SYRTHES : Error switching to directory : %s\n",s);
	  syrthes_exit(1);
	}
      }
    }
  }

  /* Redirect output if necessary */

  numarg=0;
  while (++numarg < argc) {
    s = argv[numarg];
    if (strcmp(s, "--log") == 0 || strcmp(s, "-l") == 0) {
      if (++numarg < argc) {
        s = argv[numarg];
        len = strlen(s);
        log_name = (char *)malloc(sizeof(char)*(len+1));
        strncpy(log_name, s, len);
        log_name[len] = '\0';
      }
    }
  }

#if _REDIRECT_STDERR
  if (log_name != NULL) {
    FILE *log_ptr = freopen(log_name, "a", stdout);
    if (log_ptr != NULL)
      dup2(fileno(log_ptr), fileno(stderr));
  }
#endif

  /* Analyse command-line */

  numarg=0;
  while (++numarg < argc) {
    
    s = argv[numarg];

    if (strcmp (s, "-h") == 0) 
      {
	 if (SYRTHES_LANG == FR)
	   {
	     printf("\n syrthes : Code de resolution de la Thermique et du Rayonnement\n");
	     printf("\n           usage   : syrthes -n nb_proc -d fichier_de_donnees  [-r nb_proc_ray]  [-w repertoire_execution]\n");
	     printf("\n           exemple : syrthes -n 1 -d syrthes_data.syd\n");
	   }
	 else if (SYRTHES_LANG == EN)
	   {
	     printf("\n syrthes : Software to solve conduction and radiation\n");
	     printf("\n           usage   : syrthes -n nb_proc -d data_file  [-r nb_proc_rad]  [-w working_directory] \n");
	     printf("\n           example : syrthes -n 1 -d syrthes_data.syd\n");
	   }
	syrthes_exit(1);
      }
    else if (strcmp (s, "-d") == 0) 
      {
        s = argv[++numarg];
	strncpy(nomdata,s,200) ;
	if (!fopen(nomdata,"r"))
	  {
	    if (SYRTHES_LANG == FR)
	      printf("\n\n SYRTHES : Erreur d'ouverture du fichier de donnees %s\n",nomdata);
	    else if (SYRTHES_LANG == EN)
	      printf("\n\n SYRTHES : Error opening data file %s\n",nomdata);
	    syrthes_exit(1);
	  }
	else if (size==1 || rang==0) 
	  if (SYRTHES_LANG == FR)
	    printf (" Fichier de donnees de SYRTHES : %s\n",nomdata) ;
	  else if (SYRTHES_LANG == EN)
	    printf (" Data file of SYRTHES : %s\n",nomdata) ;
      }

    else if (strcmp (s, "-r") == 0)
      {
        s = argv[++numarg];
        nbproc_ray=atoi(s);
	syrglob_npartsray=nbproc_ray;
#ifdef _SYRTHES_MPI_
	if (nbproc_ray>size)
	  if (SYRTHES_LANG == FR){
	    printf ("\n\n Le nombre de processeur pour le rayonnement doit etre inferieur ou egal\n");
	    printf (" au nombre total de processeurs demandes pour le calcul\n");
	    syrthes_exit(1);
	  }
	  else if (SYRTHES_LANG == EN){
	    printf ("\n\n Number of processors for radiation must less or equal \n");
	    printf (" than the total number of processors\n");
	    syrthes_exit(1);
	  }
#endif
      }

#ifdef _SYRTHES_CFD_
    else if (strcmp (s, "--name") == 0)
      {
        if (numarg+1 < argc) {
          s = argv[++numarg];
          len = strlen(s);
          cfd.do_coupling = 1;
          cfd.name = (char *)malloc(sizeof(char)*(len+1));
          strncpy(cfd.name, s, len);
          cfd.name[len] = '\0';
        }
        else {
	  if (SYRTHES_LANG == FR){
	    printf ("\n\n Le nom du cas SYRTHES doit etre renseigner si\n");
	    printf (" l'option --name est utilisee\n");
	    syrthes_exit(1);
	  }
	  else if (SYRTHES_LANG == EN){
	    printf ("\n\n SYRTHES case name has to be done if --name option \n");
	    printf (" is used. Check your command line arguments.\n");
	    syrthes_exit(1);
	  }
        }
      }
#endif /* End if _SYRTHES_CFD_ */

  } /* End of while */

#ifdef _SYRTHES_MPI_
  timedeb=MPI_Wtime();
  rang_deb=0;

  syrthes(rang_deb,nbproc_ray);

  timefin=MPI_Wtime();
  if (SYRTHES_LANG == FR)
    printf(" >>>>  rang %d cpu time=%f\n",rang,timefin-timedeb);
  else if (SYRTHES_LANG == EN)
    printf(" >>>>  rank %d cpu time=%f\n",rang,timefin-timedeb);

  MPI_Finalize();

#else
  rang=rang_deb=0; size=1; nbproc_ray=1;
  timedeb=cpusyrt();

  syrthes(rang_deb,nbproc_ray);

  timefin=cpusyrt();
  printf(" >>>>  cpu time=%f\n",timefin-timedeb);
#endif

  /* Free memory */

  if (log_name != NULL)
    free(log_name);

  exit(0);  
}

