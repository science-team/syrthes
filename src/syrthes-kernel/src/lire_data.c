/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "syr_usertype.h" 
#include "syr_bd.h" 
#include "syr_hmt_libmat.h" 
#include "syr_tree.h" 
#include "syr_option.h"
#include "syr_parall.h"
#include "syr_proto.h"
#include "syr_const.h"

#ifdef _SYRTHES_MPI_
#include <mpi.h>
#endif

extern struct Performances perfo;
extern struct Affichages affich;

extern int lmst;

int nbVar;

extern char nomdata[CHLONG];
FILE *fdata,*fadd;

int nitmx_t, nitmx_pv, nitmx_pt;
double epsgcs_t, epsgcs_pv, epsgcs_pt;

char nomgeom[CHLONG]="",nomgeomr[CHLONG]="";
char nomsuit[CHLONG]="",nomfdf[CHLONG]="mesh.fdf\0",nomcorrr[CHLONG]="mesh.cor\0";
char nomgeoms[CHLONG]="",nomresu[CHLONG]="",nomresuc[CHLONG]="",nomhisto[CHLONG]="";
char nomff1[CHLONG]="",nomff2[CHLONG]="",nomff2c[CHLONG]="",nomresur[CHLONG]="";
char nomresucr[CHLONG]="",nommeteo[CHLONG]="";
char nomprefixe[CHLONG]="";
char nommnx[CHLONG]="",nomadd[CHLONG]="",nomflu[CHLONG]="";
char nomcplcfdg[CHLONG]="",nomcplcfdr[CHLONG]="",nomcplcfdc[CHLONG]="";  /* maillage couple cfd */

char titre[CHLONG]="";

static char ch[CHLONG],motcle[CHLONG],repc[CHLONG],formule[CHLONG];
static double dlist[100];
static int ilist[MAX_REF];

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture des donnees                                                  |
  |======================================================================| */

void lire_firstdata(struct Variable *variable,
		    struct Maillage *maillnodes,
		    struct GestionFichiers *gestionfichiers,
		    struct PasDeTemps *pasdetemps,
		    struct Humid *humid,struct Meteo *meteo)
{
  int n;

  if ((fdata=fopen(nomdata,"r")) == NULL)
    {
      if (SYRTHES_LANG == FR)
	printf("Impossible d'ouvrir le fichier de donnees %s\n",nomdata);
      else if (SYRTHES_LANG == EN)
	printf("Impossible to open the data file %s\n",nomdata);
      syrthes_exit(1) ;
    }

  fseek(fdata,0,SEEK_SET);

  verif_data();
  verif_COND_data();
  lire_main_options(humid,&(pasdetemps->suite),&lray);
  lire_env(pasdetemps->suite,&(meteo->actif)); 
  lire_data_mc(gestionfichiers,maillnodes,pasdetemps);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture des options principale du calcul                             |
  |======================================================================| */
void lire_main_options(struct Humid *humid, int *suite,int *rayonn)
{
  int i1,i2,ok=1,ii;
  int n,nb,i;
  int ret1,ret2;

  fseek(fdata,0,SEEK_SET);

  /* initialisations */
  nbVar=1;
  *rayonn=0;
  lhumid=0;
  humid->actif=0;  humid->model=0; 
  *suite=0;
  
  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  strcpy(repc,"");
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"PRISE EN COMPTE DU RAYONNEMENT CONFINE")) 
	    {
	      ret1=rep_ch(repc,ch+i2+1);
	      if (ret1){
		if (!strcmp(repc,"OUI")) *rayonn=1;
		else if (!strcmp(repc,"NON")) *rayonn=0;
		else ok=0;
	      }
	    }
	  else if (!strcmp(motcle,"MODELISATION DES TRANSFERTS D HUMIDITE")) 
	    {
	      humid->model=rep_int(ch+i2+1);
	      if (humid->model==0) {humid->actif=0; nbVar=1;}
	      else if (humid->model==2) {nbVar=2; humid->actif=lhumid=1;}
	      else if (humid->model==3) {nbVar=3; humid->actif=lhumid=1;}
	      else ok=0;
	    }

	  else if (!strcmp(motcle,"SUITE DE CALCUL")) 
	    {
	      ret2=rep_ch(repc,ch+i2+1);
	      if (ret2){
		if (!strcmp(repc,"OUI"))      *suite=1;
		else if (!strcmp(repc,"NON")) *suite=0; 
		else ok=0;
	      }
	    }
	}
    }

  /* verifications */  
  /* ------------- */
  if (!ok && (syrglob_nparts==1 ||syrglob_rang==0))
    {
      if (!ret1){
	if (SYRTHES_LANG == FR)
	  printf("\n\n ERREUR a la lecture du mot-cle PRISE EN COMPTE DU RAYONNEMENT CONFINE=\n");
	else if (SYRTHES_LANG == EN){
	  printf("\n\n ERROR while reading key-word PRISE EN COMPTE DU RAYONNEMENT CONFINE=\n");
	}
      }

      if (humid->model!=0 && humid->model!=2 && humid->model!=3) 
	{
	  if (SYRTHES_LANG == FR){
	    printf("\n\n ERREUR a la lecture du mot-cle MODELISATION DES TRANSFERTS D HUMIDITE=\n");
	    printf(" la valeur du model de transferts couples ne peut etre que 0,2 ou 3\n");
	  }
	  else if (SYRTHES_LANG == EN){
	    printf("\n\n ERROR while reading key-word MODELISATION DES TRANSFERTS D HUMIDITE=\n");
	    printf(" the model of heat and mass transfert can be only 0, 2 or 3\n");
	  }
	}
      
      if (!ret2){
	if (SYRTHES_LANG == FR)
	  printf("\n\n ERREUR a la lecture du mot-cle SUITE DE CALCUL=\n");
	else if (SYRTHES_LANG == EN){
	  printf("\n\n ERROR while reading key-word SUITE DE CALCUL=\n");
	}
      }

      syrthes_exit(1);
    }


  /* Impressions */
  /* ----------- */
  if (syrglob_nparts==1 ||syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n *** SYRTHES : RECAPITULATIF DES OPTIONS PRINCIPALES DU CALCUL\n");
	  if (humid->actif)   printf("      - calcul des transferts couples de temperature et d'humidite\n");
	  else  printf("      - pas de calcul des transferts couples de temperature et d'humidite\n");
	  if (*rayonn)  printf("      - calcul du rayonnement thermique\n");
	  else printf("      - pas de calcul du rayonnement thermique\n");
	    
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n *** SYRTHES : SUMMARY OF MAIN OPTIONS\n");
	  if (humid->actif)   printf("      - heat and moisture transfert model is used\n");
	  else  printf("      - no heat and moisture transfert model used\n");
	  if (*rayonn)  printf("      - thermal radiation calculation\n");
	  else printf("      - no thermal radiation calculation\n");
	}
    }
   


}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture des maillages                                                |
  |======================================================================| */
void lire_maill(struct Maillage *maillnodes,struct MaillageBord *maillnodebord,
		struct SDparall *sdparall )
{
  int i,j,l;


  verif_type_maillage(nomgeom);
  lire_syrthes(maillnodes,maillnodebord,sdparall);
  verif_maill(maillnodes);
  extrarete(maillnodes);

  /* initialisations complementaires  */
  maillnodes->ncoema=3; if (maillnodes->ndim==3) maillnodes->ncoema=6;

/*  imprime_maillage_ref(*maillnodes); */

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2012 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Verification de l'orientation et de la taille des elements           |
  |======================================================================| */
void verif_maill(struct Maillage *maillnodes)
{
  int i,nbo=0; 
  int nbd=0;
  int *ne0,*ne1,*ne2,*ne3,n1,n2;
  double s6 = 1. / 6.;
  double epsvol;
  double x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4;
  double x12,y12,z12,x13,y13,z13,x14,y14,z14;
  double *c0,*c1,*c2;
  double vol;
  struct Liste_entier *listv,*pv;

  listv=(struct Liste_entier*)malloc(sizeof(struct Liste_entier));  listv->suivant=NULL;
  pv=listv;


  if (maillnodes->ndim==2)
    {
      epsvol=1.e-12; 
      c0=maillnodes->coord[0]; c1=maillnodes->coord[1];
      for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],ne2=maillnodes->node[2];
	   i<maillnodes->nelem;
	   i++,ne0++,ne1++,ne2++)
	{
	  x1=*(c0+*ne0);  y1=*(c1+*ne0);
	  x2=*(c0+*ne1);  y2=*(c1+*ne1);
	  x3=*(c0+*ne2);  y3=*(c1+*ne2);
	  x12=x2-x1;  y12=y2-y1;
	  x13=x3-x1;  y13=y3-y1;
	  vol=0.5* fabs( x12*y13 - y12*x13 );
	  /* si l'elt est degenere, on le note */
	  if (vol<epsvol) 
	    {
	      pv->num=i;
	      pv->suivant=(struct Liste_entier*)malloc(sizeof(struct Liste_entier));
	      pv=pv->suivant; pv->suivant=NULL;
	      nbd++;
	    }
	}
    }

  else
    {

      int n1,n2;
      epsvol=1.e-18;
      c0=maillnodes->coord[0]; c1=maillnodes->coord[1]; c2=maillnodes->coord[2];
      for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],ne2=maillnodes->node[2],ne3=maillnodes->node[3];
	   i<maillnodes->nelem;
	   i++,ne0++,ne1++,ne2++,ne3++)
	{
	  x1=*(c0+*ne0);  y1=*(c1+*ne0); z1=*(c2+*ne0);
	  x2=*(c0+*ne1);  y2=*(c1+*ne1); z2=*(c2+*ne1);
	  x3=*(c0+*ne2);  y3=*(c1+*ne2); z3=*(c2+*ne2);
	  x4=*(c0+*ne3);  y4=*(c1+*ne3); z4=*(c2+*ne3);
	  
	  x12=x2-x1;  y12=y2-y1;   z12=z2-z1;  
	  x13=x3-x1;  y13=y3-y1;   z13=z3-z1;
	  x14=x4-x1;  y14=y4-y1;   z14=z4-z1;
	  vol = s6 * (   x12*(y13*z14-z13*y14) - x13*(y12*z14-z12*y14)
			 + x14*(y12*z13-z12*y13) );

	  /* si le volume est negatif, on reoriente l'element */
	  if (vol<0.){
	    n1=*ne1;	    
	    n2=*ne2;
	    maillnodes->node[1][i] = n2;
	    maillnodes->node[2][i] = n1;
	    nbo++;
	  }
	  
	  /* si l'elt est degenere, on le note */
	  if (fabs(vol)<epsvol)
	    {
	      printf(">>>(fabs(vol)=%f\n",fabs(vol));
	      pv->num=i;
	      pv->suivant=(struct Liste_entier*)malloc(sizeof(struct Liste_entier));
	      pv=pv->suivant; pv->suivant=NULL;
	      nbd++;
	    }
	}
    }
 

  printf("\n *** verif_maill : number of elements reoriented : %d\n",nbo);


  if (nbd>0)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n  %%%% ERROR verif_maill : \n");
	  printf("           Il existe des elements degeneres dans le maillage (volume<1.e-18) : %d elements\n",nbd);
	  printf("           Liste des element :\n           ");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n  %%%% ERROR verif_maill : \n");
	  printf("           Some elements of the mesh are degenerated (volume<1.e-18) : %d elements\n",nbd);
	  printf("           Element list :\n           ");
	}
      pv=listv;
      for (i=0;i<nbd;i++)
	{printf("%d ",pv->num+1); pv=pv->suivant;}
      printf("\n");

      exit(1);
    }

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture des donnees                                                  |
  |======================================================================| */
void lire_donnees(struct Maillage maillnodes,
		  struct Variable *variable,
		  struct Prophy *physol,struct Histo *histos,
		  struct Bilan *bilansurf,struct Bilan *bilanvol,
		  struct Humid humid)
{

  if (!humid.actif)
    {
      /* physol n'existe pas en hmt */
      decode_prophy(maillnodes,physol);
      lire_prophy(maillnodes,physol);
    }

  lire_cini(maillnodes,variable);
  lire_histo(maillnodes,histos);
  lire_bilan(maillnodes,bilansurf,bilanvol);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Verification des mots-cles du fichier de donnees                     |
  |======================================================================| */
void verif_data()
{
  int i1,i2,i3,i4,ok=1;

  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"MAILLAGE CONDUCTION")) {} 
	  else if (!strcmp(motcle,"MAILLAGE RAYONNEMENT")) {}
	  else if (!strcmp(motcle,"FICHIER METEO")) {}

	  /* temporairement, presence des 2 mc pour assurer la compatibilite */
	  else if (!strcmp(motcle,"PREFIXE DU RESULTAT PRECEDENT POUR SUITE DE CALCUL")) {}
	  else if (!strcmp(motcle,"RESULTAT PRECEDENT POUR SUITE DE CALCUL")) {}

	  else if (!strcmp(motcle,"PREFIXE DES FICHIERS RESULTATS")) {}

	  else if (!strcmp(motcle,"TITRE ETUDE")) {}
	  else if (!strcmp(motcle,"DIMENSION DU PROBLEME")) {}
	  else if (!strcmp(motcle,"PRISE EN COMPTE DU RAYONNEMENT CONFINE")) {}
	  else if (!strcmp(motcle,"PAS DE TEMPS SOLIDE")) {}
	  else if (!strcmp(motcle,"PAS DE TEMPS AUTOMATIQUE")) {}
	  else if (!strcmp(motcle,"PAS DE TEMPS MULTIPLES")) {}
	  else if (!strcmp(motcle,"NOMBRE DE PAS DE TEMPS SOLIDES")) {}
	  else if (!strcmp(motcle,"PAS DES SORTIES CHRONO SOLIDE ITERATIONS")) {}
	  else if (!strcmp(motcle,"PAS DES SORTIES CHRONO SOLIDE SECONDES")) {}
	  else if (!strcmp(motcle,"INSTANTS SORTIES CHRONO SOLIDE SECONDES")) {}
	  else if (!strcmp(motcle,"CHAMP DE TEMPERATURES MAXIMALES")) {}
	  else if (!strcmp(motcle,"CHAMP DE FLUX THERMIQUE")) {}
	  else if (!strcmp(motcle,"SUPPRESSION DU CHAMP RESULTAT FINAL")) {}
	  else if (!strcmp(motcle,"NOMBRE ITERATIONS SOLVEUR TEMPERATURE")) {}
	  else if (!strcmp(motcle,"NOMBRE ITERATIONS SOLVEUR PRESSION VAPEUR")) {}
	  else if (!strcmp(motcle,"NOMBRE ITERATIONS SOLVEUR PRESSION TOTALE")) {}
	  else if (!strcmp(motcle,"PRECISION POUR LE SOLVEUR TEMPERATURE")) {}
	  else if (!strcmp(motcle,"PRECISION POUR LE SOLVEUR PRESSION VAPEUR")) {}
	  else if (!strcmp(motcle,"PRECISION POUR LE SOLVEUR PRESSION TOTALE")) {}
	  else if (!strcmp(motcle,"SUITE DE CALCUL")) {}
	  else if (!strcmp(motcle,"SUITE : NOUVEAU TEMPS INITIAL")) {}
	  else if (!strcmp(motcle,"MODELISATION DES TRANSFERTS D HUMIDITE")) {}

	  else if (!strcmp(motcle,"REF ELEMENTS SEMI-TRANSPARENTS")) {}

	  else if (!strcmp(motcle,"BILAN FLUX SURFACIQUES")) {}
	  else if (!strcmp(motcle,"BILAN FLUX VOLUMIQUES")) {}

	  else if (!strcmp(motcle,"CINI_T")) {}
	  else if (!strcmp(motcle,"CINI_T_FCT")) {}
	  else if (!strcmp(motcle,"CINI_T_PROG")) {}
	  else if (!strcmp(motcle,"CINI_PV")) {}
	  else if (!strcmp(motcle,"CINI_PV_FCT")) {}
	  else if (!strcmp(motcle,"CINI_PV_PROG")) {}
	  else if (!strcmp(motcle,"CINI_PT")) {}
	  else if (!strcmp(motcle,"CINI_PT_FCT")) {}
	  else if (!strcmp(motcle,"CINI_PT_PROG")) {}

	  else if (!strcmp(motcle,"CLIM")) {
	    extr_motcle(motcle,ch+i2+1,&i3,&i4);
	    if (!strcmp(motcle,"RES_CONTACT")){}
	    else if (!strcmp(motcle,"RAY_INFINI")){}
	    else if (!strcmp(motcle,"PERIODICITE_2D")){}
	    else if (!strcmp(motcle,"PERIODICITE_2D")){}
	  }

	  else if (!strcmp(motcle,"CLIM")) {
	    extr_motcle(motcle,ch+i2+1,&i3,&i4);
	    if (!strncmp(motcle,"COUPLAGE_SURF_FLUIDE",20)){}
	    else if (!strncmp(motcle,"COUPLAGE_VOL_FLUIDE",19)){}
	  }
	  else if (!strcmp(motcle,"CLIM_FCT")) {
	    extr_motcle(motcle,ch+i2+1,&i3,&i4);
	    if (!strcmp(motcle,"RES_CONTACT")){}
	  }

	  else if (!strcmp(motcle,"CLIM_PROG")) {
	    extr_motcle(motcle,ch+i2+1,&i3,&i4);
	    if (!strcmp(motcle,"RES_CONTACT")){}
	  }

	  else if (!strcmp(motcle,"CLIM_T")) {
	    extr_motcle(motcle,ch+i2+1,&i3,&i4);
	    if (!strcmp(motcle,"COEF_ECH")){}
	    else if (!strcmp(motcle,"DIRICHLET")){}
	    else if (!strcmp(motcle,"FLUX")){}
	  }
	  else if (!strcmp(motcle,"CLIM_T_FCT")) {
	    extr_motcle(motcle,ch+i2+1,&i3,&i4);
	    if (!strcmp(motcle,"COEF_ECH")){}
	    else if (!strcmp(motcle,"DIRICHLET")){}
	    else if (!strcmp(motcle,"FLUX")){}
	  }
	  else if (!strcmp(motcle,"CLIM_T_PROG")) {
	    extr_motcle(motcle,ch+i2+1,&i3,&i4);
	    if (!strcmp(motcle,"COEF_ECH")){}
	    else if (!strcmp(motcle,"DIRICHLET")){}
	    else if (!strcmp(motcle,"FLUX")){}
	  }

	  else if (!strcmp(motcle,"CLIM_HMT")) {
	    extr_motcle(motcle,ch+i2+1,&i3,&i4);
	    if (!strcmp(motcle,"RES_CONTACT")){}
	    else if (!strcmp(motcle,"HHH")){}
	  }

	  else if (!strcmp(motcle,"CLIM_HMT")) {}
	  else if (!strcmp(motcle,"CLIM_HMT_FCT")) {}
	  else if (!strcmp(motcle,"CLIM_HMT_PROG")) {}

	  else if (!strcmp(motcle,"CPHY_MAT_ISO")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ORTHO_2D")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ORTHO_3D")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ANISO_2D")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ANISO_3D")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ISO_FCT")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ORTHO_2D_FCT")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ORTHO_3D_FCT")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ANISO_2D_FCT")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ANISO_3D_FCT")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ISO_PROG")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ORTHO_2D_PROG")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ORTHO_3D_PROG")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ANISO_2D_PROG")) {}
	  else if (!strcmp(motcle,"CPHY_MAT_ANISO_3D_PROG")) {}

	  else if (!strcmp(motcle,"HMT_MAT")) {}

	  else if (!strcmp(motcle,"HIST")) {}

	  else if (!strcmp(motcle,"CVOL_T")) {}
	  else if (!strcmp(motcle,"CVOL_PV")) {}
	  else if (!strcmp(motcle,"CVOL_PT")) {}
	  else if (!strcmp(motcle,"CVOL_T_FCT")) {}
	  else if (!strcmp(motcle,"CVOL_PV_FCT")) {}
	  else if (!strcmp(motcle,"CVOL_PT_FCT")) {}
	  else if (!strcmp(motcle,"CVOL_T_PROG")) {}
	  else if (!strcmp(motcle,"CVOL_PV_PROG")) {}
	  else if (!strcmp(motcle,"CVOL_PT_PROG")) {}

	  else if (!strcmp(motcle,"CONDUCTIVITE CONSTANTE")) {}

	  else if (!strcmp(motcle,"NOMBRE DE BANDES SPECTRALES POUR LE RAYONNEMENT")) {}
	  else if (!strcmp(motcle,"NOMBRE DE REDECOUPAGES POUR CALCUL DES FACTEURS DE FORME")) {}
	  else if (!strcmp(motcle,"NOMBRE DE BANDES SPECTRALES POUR LE RAYONNEMENT")) {}
	  else if (!strcmp(motcle,"DOMAINE DE RAYONNEMENT CONFINE OUVERT SUR L EXTERIEUR")) {}
	  else if (!strcmp(motcle,"LECTURE DES FACTEURS DE FORME SUR FICHIER")) {}
/* 	  else if (!strcmp(motcle,"LECTURE DES CORRESPONDANTS POUR RAYONNEMENT")) {} */
	  else if (!strcmp(motcle,"ECRITURES OPTIONNELLES RAYONNEMENT")) {}
	  else if (!strcmp(motcle,"PRISE EN COMPTE DU RAYONNEMENT SOLAIRE")) {}


	  else if (!strcmp(motcle,"PROPRIETES PHYSIQUES GLOBALES POUR LES VITRAGES")) {}
	  else if (!strcmp(motcle,"RAYT")) {}
	  else if (!strcmp(motcle,"CLIM_RAYT")) {}
	  else if (!strcmp(motcle,"SOLAIRE")) {}
	  else if (!strcmp(motcle,"HIST_RAYT")) {}

	  else if (!strcmp(motcle,"MODELE 1D FLUIDE")) {}
	  else if (!strcmp(motcle,"MODELE 0D FLUIDE")) {}

	  else 
	    {
	      ok=0;
	      if (SYRTHES_LANG == FR)
		{
		  printf("\n\n ERREUR A LA LECTURE DES MOTS CLES\n");
		  printf("        Mot-cle non reconnu : %s\n",ch);
		}
	      else if (SYRTHES_LANG == EN)
		{
		  printf("\n\n ERROR DURING THE READING OF THE KEY WORD\n");
		  printf("        Key-word not recognized : %s\n",ch);
		}
	    }
	}
    }



  if (!ok) syrthes_exit(1);
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Verification des mots-cles RAYT en 2eme nbiveau                      |
  |======================================================================| */
void verif_COND_data()
{
  int id,i1,i2,i3,i4,ok=1;

  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"CLIM_T") || !strcmp(motcle,"CLIM_T_FCT") || !strcmp(motcle,"CLIM_T_PROG")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      
	      if (!strcmp(motcle,"COEF_ECH")) {}
	      else if (!strcmp(motcle,"DIRICHLET")) {}
	      else if (!strcmp(motcle,"FLUX")) {}
	      else if (!strcmp(motcle,"RES_CONTACT")) {}
	      else if (!strcmp(motcle,"RAY_INFINI")) {}
	      else 
		{
		  ok=0;
		  if (SYRTHES_LANG == FR){
		    printf("\n\n ERREUR A LA LECTURE DES MOTS CLES\n");
		    printf("        Mot-cle non reconnu : %s\n",ch);
		  }
		  else if (SYRTHES_LANG == EN){
		    printf("\n\n ERROR DURING THE READING OF THE KEY WORD\n");
		    printf("        Key-word not recognized : %s\n",ch);
		  }
		}
	    }


	  else if (!strcmp(motcle,"CLIM")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      
	      if (!strcmp(motcle,"PERIODICITE_2D")) {}
	      else if (!strcmp(motcle,"PERIODICITE_3D")) {}
	      else if (!strcmp(motcle,"COUPLAGE_RAYONNEMENT")) {}
	      else if (!strncmp(motcle,"COUPLAGE_SURF_FLUIDE",20)) {}
	      else if (!strncmp(motcle,"COUPLAGE_VOL_FLUIDE",19)) {}
	      else 
		{
		  ok=0;
		  if (SYRTHES_LANG == FR){
		    printf("\n\n ERREUR A LA LECTURE DES MOTS CLES\n");
		    printf("        Mot-cle non reconnu : %s\n",ch);
		  }
		  else if (SYRTHES_LANG == EN){
		    printf("\n\n ERROR DURING THE READING OF THE KEY WORD\n");
		    printf("        Key-word not recognized : %s\n",ch);
		  }
		}
	    }

	  else if (!strcmp(motcle,"HIST")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      
	      if (!strcmp(motcle,"FREQ_SECONDS")) {}
	      else if (!strcmp(motcle,"FREQ_ITER")) {}
	      else if (!strcmp(motcle,"FREQ_LIST_TIMES")) {}
	      else if (!strcmp(motcle,"NOEUDS")) {}
	      else if (!strcmp(motcle,"COORD")) {}
	      else 
		{
		  ok=0;
		  if (SYRTHES_LANG == FR){
		    printf("\n\n ERREUR A LA LECTURE DES MOTS CLES\n");
		    printf("        Mot-cle non reconnu : %s\n",ch);
		  }
		  else if (SYRTHES_LANG == EN){
		    printf("\n\n ERROR DURING THE READING OF THE KEY WORD\n");
		    printf("        Key-word not recognized : %s\n",ch);
		  }
		}
	    }

	}
    }



  if (!ok) syrthes_exit(1);
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Verification des mots-cles RAYT en 2eme nbiveau                      |
  |======================================================================| */
void verif_RAYT_data()
{
  int id,i1,i2,i3,i4,ok=1;

  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"RAYT")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      
	      if (!strcmp(motcle,"VOLUME_CONNEXE")) {}
	      else if (!strcmp(motcle,"SYMETRIE_2D")) {}
	      else if (!strcmp(motcle,"SYMETRIE_3D")) {}
	      else if (!strcmp(motcle,"PERIODICITE_2D")) {}
	      else if (!strcmp(motcle,"PERIODICITE_3D")) {}
	      else if (!strcmp(motcle,"BANDES_SPECTRALES")) {}
	      else if (!strcmp(motcle,"TEMPERATURE_INFINI")) {}
	      else if (!strcmp(motcle,"ETR")) {}
	      else 
		{
		  ok=0;
		  if (SYRTHES_LANG == FR){
		    printf("\n\n ERREUR A LA LECTURE DES MOTS CLES\n");
		    printf("        Mot-cle non reconnu : %s\n",ch);
		  }
		  else if (SYRTHES_LANG == EN){
		    printf("\n\n ERROR DURING THE READING OF THE KEY WORD\n");
		    printf("        Key-word not recognized : %s\n",ch);
		  }
		}
	    }


	  else if (!strcmp(motcle,"CLIM_RAYT")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      
	      if (!strcmp(motcle,"COUPLAGE_CONDUCTION")) {}
	      else if (!strcmp(motcle,"OMBRAGE")) {}
	      else if (!strcmp(motcle,"HORIZON")) {}
	      else if (!strcmp(motcle,"TEMPERATURE_IMPOSEE")) {}
	      else if (!strcmp(motcle,"FLUX_IMPOSE_PAR_BANDE")) {}
	      else if (!strcmp(motcle,"SEMI_TRANSPARENT")) {}
	      else 
		{
		  ok=0;
		  if (SYRTHES_LANG == FR){
		    printf("\n\n ERREUR A LA LECTURE DES MOTS CLES\n");
		    printf("        Mot-cle non reconnu : %s\n",ch);
		  }
		  else if (SYRTHES_LANG == EN){
		    printf("\n\n ERROR DURING THE READING OF THE KEY WORD\n");
		    printf("        Key-word not recognized : %s\n",ch);
		  }
		}
	    }

	  else if (!strcmp(motcle,"SOLAIRE")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      
	      if (!strcmp(motcle,"LATITUDE_DU_LIEU")) {}
	      else if (!strcmp(motcle,"LONGITUDE_DU_LIEU")) {}
	      else if (!strcmp(motcle,"MOIS_JOUR_HEURE_MINUTE")) {}
	      else if (!strcmp(motcle,"COEFFICIENTS_DE_CLARTE_DU_CIEL")) {}
	      else if (!strcmp(motcle,"2D_VERTICAL_:_ANGLE_PAR_RAPPORT_AU_NORD")) {}
	      else if (!strcmp(motcle,"CENTRE_DU_DOMAINE_DE_CALCUL")) {}
	      else if (!strcmp(motcle,"SOURCE_AZIMUT_H")) {}
	      else if (!strcmp(motcle,"SOURCE_FLUX_CONSTANT_PAR_BANDE")) {}
	      else if (!strcmp(motcle,"SOURCE_REPART_AUTO_SUR_LES_BANDES")) {}
	      else if (!strcmp(motcle,"TEMPERATURE_ET_EMISSIVITE_DE_LA_TERRE_A_L_HORIZON")) {}
	      else if (!strcmp(motcle,"BILAN")) {}
	      else 
		{
		  ok=0;
		  if (SYRTHES_LANG == FR){
		    printf("\n\n ERREUR A LA LECTURE DES MOTS CLES\n");
		    printf("        Mot-cle non reconnu : %s\n",ch);
		  }
		  else if (SYRTHES_LANG == EN){
		    printf("\n\n ERROR DURING THE READING OF THE KEY WORD\n");
		    printf("        Key-word not recognized : %s\n",ch);
		  }
		}
	    }

	}
    }



  if (!ok) syrthes_exit(1);
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture du fichier d'environnement syrthes.env                       |
  |======================================================================| */
void lire_env(int suite,int *lmeteo)
{
  int i1,i2,ok=1,l,lext;
  char chnum[20],extens[6];
  int ret1=1,ret2=1,ret3=1,ret4=1,ret5=0;

  *lmeteo=0;  

  if (syrglob_nparts==1 ||syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf("\n *** SYRTHES : LECTURE DES NOMS DE FICHIERS\n");
    else if (SYRTHES_LANG == EN)
      printf("\n *** SYRTHES : FILE NAMES READING\n");

  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' &&  strlen(ch)>1)
	{
	  strcpy(repc,"");
	  extr_motcle_(motcle,ch,&i1,&i2);
	  
	  if      (!strcmp(motcle,"MAILLAGE CONDUCTION")) 
	    {
	      ret1=rep_chw(nomgeom,ch+i2+1);
	      if (syrglob_nparts==1 ||syrglob_rang==0)
		if (SYRTHES_LANG == FR)	   printf("      - Maillage conduction : %s\n",nomgeom);
		else if (SYRTHES_LANG == EN) printf("      - Conduction mesh : %s\n",nomgeom);

	      /* en // suppression du numero du fichier s'il est present */
	      if (syrglob_nparts>1){
		l=strlen(nomgeom);
		if      (!strncmp(nomgeom+l-4,".syr",4))  {strcpy(extens,".syr\0");lext=4;}
		else if (!strncmp(nomgeom+l-5,".syrb",5)) {strcpy(extens,".syrb\0");lext=5;}

		if (!strncmp(nomgeom+l-lext-9,"part",4) && !strncmp(nomgeom+l-lext-15,"_",1)){
		  strcpy(nomgeom+l-lext-15,extens);
		}
	      } 
	    }

	  else if (!strcmp(motcle,"MAILLAGE RAYONNEMENT")) {
	    if (lray) ret2=rep_chw(nomgeomr,ch+i2+1);
	    if (syrglob_nparts==1 ||syrglob_rang==0)
	      if (SYRTHES_LANG == FR)	   printf("      - Maillage rayonnement : %s\n",nomgeomr);
	      else if (SYRTHES_LANG == EN) printf("      - Radiation mesh : %s\n",nomgeomr);
	  }

	  else if (!strcmp(motcle,"FICHIER METEO")) {
	    ret5=rep_chw(nommeteo,ch+i2+1);
	    if (ret5>0) *lmeteo=1;
	  }

	  /* temporairement, presence des 2 mc pour assurer la compatibilite */
	  else if ( !strcmp(motcle,"PREFIXE DU RESULTAT PRECEDENT POUR SUITE DE CALCUL") ||
	            !strcmp(motcle,"RESULTAT PRECEDENT POUR SUITE DE CALCUL") ) {
	    if (suite) {
	      ret3=rep_chw(nomsuit,ch+i2+1);
	      if (syrglob_nparts==1 ||syrglob_rang==0)
		if (SYRTHES_LANG == FR)	   printf("      - Suite de calcul : %s\n",nomsuit);
		else if (SYRTHES_LANG == EN) printf("      - Restart file : %s\n",nomsuit);

	      /* verification de la presence de l'extension .res */
		l=strlen(nomsuit);
		if (!strncmp(nomsuit+l-4,".res",4))  {
		  strcpy(extens,".res\0");lext=4;}
		else {
		  if (SYRTHES_LANG == FR)
		    printf("\n\n ERREUR : le format du fichier suite n'est pas correct. On attend une fichier .res\n");
		  else if (SYRTHES_LANG == EN)
		    printf("\n\n ERROR : invalid restart file format - .res file is required\n");
		  syrthes_exit(1) ;
		}

	      /* suppression de l'extension en sequentiel */
	      if  (syrglob_nparts==1){
		strcpy(nomsuit+l-4,"\0");
	      }
	      /* en // suppression du numero du fichier s'il est present */
	      else if (syrglob_nparts>1){
		if (!strncmp(nomsuit+l-lext-9,"part",4) && !strncmp(nomsuit+l-lext-15,"_",1)){
		  strcpy(nomsuit+l-lext-15,"\0");
		}
		else {
		  if (SYRTHES_LANG == FR)
		    printf("\n\n ERREUR : calcul parallele, fichiers suite non trouves (ex: PART/myfile_00003part00000.res)\n");
		  else if (SYRTHES_LANG == EN)
		    printf("\n\n ERROR : parallel computation, restart files not found (ex: PART/myfile_00003part00000.res)\n");
		  syrthes_exit(1) ;
		}
	      } 
	    }
	  }

	  else if (!strcmp(motcle,"PREFIXE DES FICHIERS RESULTATS")) {
	    ret4=rep_chw(nomprefixe,ch+i2+1);
	    if (syrglob_nparts==1 ||syrglob_rang==0)
	      if (SYRTHES_LANG == FR)	   printf("      - Fichiers resusltats : %s\n",nomprefixe);
	      else if (SYRTHES_LANG == EN) printf("      - Results files : %s\n",nomprefixe);

	  }
	}
    }

  /* verifications */
  if (syrglob_nparts==1 ||syrglob_rang==0)
    {

      if (!ret1)
	{
	  ok=0;
	  if (SYRTHES_LANG == FR){
	    printf("\n   --> ERREUR lire_env : Le nom du maillage conduction n'a pas ete donne\n");
	    printf("                         voir le mote-cle 'MAILLAGE CONDUCTION'\n");
	  }
	  else if (SYRTHES_LANG == EN){
	    printf("\n   --> ERROR lire_env : The file name of the mesh for conduction has not been given\n");
	    printf("                        see keyword 'MAILLAGE CONDUCTION'\n");
	  }
	}

      if (!ret2)
	{
	  ok=0;
	  if (SYRTHES_LANG == FR){
	    printf("\n   --> ERREUR lire_env : Le nom du maillage rayonnement n'a pas ete donne\n");
	    printf("                         voir le mote-cle 'MAILLAGE RAYONNEMENT'\n");
	  }
	  else if (SYRTHES_LANG == EN){
	    printf("\n   --> ERROR lire_env : The file name of the mesh for radiation has not been given\n");
	    printf("                        see keyword 'MAILLAGE RAYONNEMENT'\n");
	  }
	}

      if (!ret3)
	{
	  ok=0;
	  if (SYRTHES_LANG == FR){
	    printf("\n   --> ERREUR lire_env : Le prefixe pour les precedents fichiers resultats n'a pas ete donne\n");
	    printf("                         voir le mote-cle 'PREFIXE DU RESULTAT PRECEDENT POUR SUITE DE CALCUL'\n");
	  }
	  else if (SYRTHES_LANG == EN){
	    printf("\n   --> ERROR lire_env : The prefix name for the previous result files has not been given\n");
	    printf("                        see keyword 'PREFIXE DU RESULTAT PRECEDENT POUR SUITE DE CALCUL'\n");
	  }
	}

      if (!ret4)
	{
	  ok=0;
	  if (syrglob_nparts==1 ||syrglob_rang==0)
	    if (SYRTHES_LANG == FR){
	      printf("\n   --> ERREUR lire_env : Le prefixe pour les fichiers resultats n'a pas ete donne\n");
	      printf("                         voir le mote-cle 'PREFIXE DES FICHIERS RESULTATS'\n");
	    }
	    else if (SYRTHES_LANG == EN){
	      printf("\n   --> ERROR lire_env : The prefix name for the result files has not been given\n");
	      printf("                        see keyword 'PREFIXE DES FICHIERS RESULTATS'\n");
	    }
	}

      if (!ok) syrthes_exit(1);
    }


  /* construction automatique de l'ensemble des fichiers resultats */
  strcpy(nomgeoms,nomprefixe);    
  strcpy(nomresu,nomprefixe);     
  strcpy(nomresuc,nomprefixe);    
  strcpy(nomhisto,nomprefixe);    
  strcpy(nommnx,nomprefixe);    
  strcpy(nomadd,nomprefixe); 
  strcpy(nomflu,nomprefixe); 
     
  if (lray){
    strcpy(nomresur,nomprefixe);    
    strcpy(nomresucr,nomprefixe);   

    /* construction automatique fichiers internes des corresp et fdf */
    /* --> rien a faire, c'est initialise a la declaration */
    /*     strncpy(nomcorrr,nomgeomr,strlen(nomgeomr)-4);    strcat(nomcorrr,".cor\0"); */
    /*     strncpy(nomfdf,nomgeomr,strlen(nomgeomr)-4);      strcat(nomfdf,".fdf\0"); */
  }

 #ifdef _SYRTHES_CFD_ 
    strcpy(nomcplcfdg,nomprefixe); 
    strcpy(nomcplcfdr,nomprefixe); 
    strcpy(nomcplcfdc,nomprefixe); 
 #endif

  /* mise en place des extensions */
  if (syrglob_nparts==1)
    {
      strcat(nomgeoms,".syr\0");  
      strcat(nomresu,".res\0");  
      strcat(nomsuit,".res\0");  
      strcat(nomresuc,".rdt\0");   
      strcat(nomhisto,".his\0");   
      strcat(nommnx,".mnx\0");   
      strcat(nomadd,".add\0");   
      
      if (lray){
	     strcat(nomresur,"_rad.res\0");  
	     strcat(nomresucr,"_rad.rdt\0");  
         }

#ifdef _SYRTHES_CFD_ 
      strcat(nomcplcfdg,"_cplcfd.syr\0"); 
      strcat(nomcplcfdr,"_cplcfd.res\0"); 
      strcat(nomcplcfdc,"_cplcfd.rdt\0"); 
#endif
    }
  else
    /* modification des noms des fichiers de resultats en cas de parallelisme            */
    {
      sprintf(chnum,"_%05dpart%05d\0",syrglob_nparts,syrglob_rang);

      strcat(nomgeoms,chnum);     strcat(nomgeoms,".syr\0");  
      strcat(nomresu,chnum);      strcat(nomresu,".res\0");  
      strcat(nomresuc,chnum);     strcat(nomresuc,".rdt\0"); 
      strcat(nomhisto,chnum);     strcat(nomhisto,".his\0"); 

      if (lray){
	strcat(nomresur,chnum);     strcat(nomresur,"_rad.res\0");  
	strcat(nomresucr,chnum);    strcat(nomresucr,"_rad.rdt\0");  
      }

#ifdef _SYRTHES_CFD_ 
	strcat(nomcplcfdg,chnum);    strcat(nomcplcfdg,"_cplcfd.syr\0");
	strcat(nomcplcfdr,chnum);    strcat(nomcplcfdr,"_cplcfd.res\0");
	strcat(nomcplcfdc,chnum);    strcat(nomcplcfdc,"_cplcfd.rdt\0");
#endif

      strcat(nommnx,".mnx\0"); /* le fichier des min_max est global */   

      strcat(nomadd,chnum);       strcat(nomadd,".add\0");   

      strcat(nomsuit,chnum);      strcat(nomsuit,".res\0");   
    }

  /* le fichier de flux n'est rempli que sur le proc 0 */
  strcat(nomflu,".flu\0");   
      
  /* on ouvre le fichier additionnel des maintenant pour que l'utilisateur
     puisse y ecrire a tout moment */
  if ((fadd=fopen(nomadd,"w")) == NULL) 
    {
      if (SYRTHES_LANG == FR)
        printf("Impossible d'ouvrir le fichier maillage additionnel :%s\n",nomadd);
      else if (SYRTHES_LANG == EN)
        printf("Cannot open the additionnal file %s\n",nomadd);
      syrthes_exit(1) ;
    }

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture du fichier de mots-cles syrthes.data : mots-cles generaux    |
  |======================================================================| */
void lire_data_mc(struct GestionFichiers *gestionfichiers,
		  struct Maillage *maillnodes,
		  struct PasDeTemps *pasdetemps)
{
  int i1,i2,ok=1,ii;
  int n,nb,i;
  int ret1=1,ret2=1;

  fseek(fdata,0,SEEK_SET);

  /* initialisations */

  nitmx_t=50; nitmx_pv=200; nitmx_pt=200; 
  epsgcs_t=1.e-5;  epsgcs_pv=1.e-6;    epsgcs_pv=1.e-14;                  

  pasdetemps->ntsmax=0;   
  maillnodes->iaxisy=0;  
  maillnodes->ndim=3;  
  
  pasdetemps->nbpaso=pasdetemps->nbparay=0;
  pasdetemps->ntsyr=0;  
  pasdetemps->rdttsprec=pasdetemps->tempss=0;
  pasdetemps->rdtts=-1.;
  pasdetemps->dtauto.actif=0;    
  pasdetemps->dtmult.actif=0;         
  pasdetemps->dtmult.nb=0;
  pasdetemps->new_t_init=-1;
  gestionfichiers->champmax=0; 
  gestionfichiers->freq_chrono_s=0;
  gestionfichiers->freq_chrono_nt=0;
  gestionfichiers->freq_chrono_list_nb=0;
  gestionfichiers->freq_chrono_list_s=NULL;
  gestionfichiers->champflux=0;
  gestionfichiers->suppchampfinal=0;

  /*  pasdetemps->suite est deja initialise dans la lecture "lire_main_options" */
  pasdetemps->premier=1; 
  if (pasdetemps->suite==1) pasdetemps->premier=0;

  pasdetemps->suiteprem=0; 
  if (pasdetemps->suite==1) pasdetemps->suiteprem=1;


  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  strcpy(repc,"");
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"TITRE ETUDE")) 
	    {
	      ret1=rep_ch(titre,ch+i2+1);
	    }

	  else if (!strcmp(motcle,"PAS DE TEMPS SOLIDE")) 
	    {
	      pasdetemps->rdtts=rep_dbl(ch+i2+1);
	    }
	  else if (!strcmp(motcle,"PAS DE TEMPS AUTOMATIQUE")) 
	    {
	      pasdetemps->dtauto.actif=1;
	      if (nbVar==1)
		{
		  rep_ndbl(2,dlist,&ii,ch+i2+1);
		  pasdetemps->dtauto.deltaT=dlist[0];
		  pasdetemps->dtauto.dtlimit=dlist[1];
		}
	      else if (nbVar==2)
		{
		  rep_ndbl(3,dlist,&ii,ch+i2+1);
		  pasdetemps->dtauto.deltaT=dlist[0];
		  pasdetemps->dtauto.deltaPv=dlist[1];
		  pasdetemps->dtauto.dtlimit=dlist[2];
		}
	      else if (nbVar==3)
		{
		  rep_ndbl(4,dlist,&ii,ch+i2+1);
		  pasdetemps->dtauto.deltaT=dlist[0];
		  pasdetemps->dtauto.deltaPv=dlist[1];
		  pasdetemps->dtauto.deltaPt=dlist[2];
		  pasdetemps->dtauto.dtlimit=dlist[3];
		}
	    }
	  else if (!strcmp(motcle,"PAS DE TEMPS MULTIPLES")) 
	    {
	      pasdetemps->dtmult.actif=1; 
	      rep_ndbl(2,dlist,&ii,ch+i2+1);
	      pasdetemps->dtmult.nbiter[pasdetemps->dtmult.nb]=(int)dlist[0];
	      pasdetemps->dtmult.dt[pasdetemps->dtmult.nb]=dlist[1];
	      pasdetemps->dtmult.nb++;
	    }
	  else if (!strcmp(motcle,"NOMBRE DE PAS DE TEMPS SOLIDES")) 
	    {
	      pasdetemps->ntsmax=rep_int(ch+i2+1);
	    }
	  else if (!strcmp(motcle,"SUITE : NOUVEAU TEMPS INITIAL")) 
	    {
	      pasdetemps->new_t_init=rep_dbl(ch+i2+1);
	    }
	  else if (!strcmp(motcle,"PAS DES SORTIES CHRONO SOLIDE ITERATIONS")) 
	    {
	      gestionfichiers->freq_chrono_nt=rep_int(ch+i2+1);
	      gestionfichiers->freq_chrono_s=0;
	      gestionfichiers->freq_chrono_list_nb=0;
	    }
	  else if (!strcmp(motcle,"PAS DES SORTIES CHRONO SOLIDE SECONDES")) 
	    {
	      gestionfichiers->freq_chrono_s=rep_dbl(ch+i2+1);
	      gestionfichiers->freq_chrono_nt=0;
	      gestionfichiers->freq_chrono_list_nb=0;
	    }
	  else if (!strcmp(motcle,"INSTANTS SORTIES CHRONO SOLIDE SECONDES")) 
	    {
	      rep_listdble(dlist,&nb,ch+i2+1);
	      if (nb>0 && dlist[0]<=0)
		{
		  gestionfichiers->freq_chrono_list_nb=0;
		}
	      else
		{
		  if (gestionfichiers->freq_chrono_list_nb<=0)
		    {
		      gestionfichiers->freq_chrono_list_nb=nb;
		      gestionfichiers->freq_chrono_list_s=(double*)malloc(nb*sizeof(double));
		      verif_alloue_double1d("lire_data_mc",gestionfichiers->freq_chrono_list_s);
		      for (i=0;i<nb;i++) gestionfichiers->freq_chrono_list_s[i]=dlist[i];
		      gestionfichiers->freq_chrono_nt=0;
		      gestionfichiers->freq_chrono_s=0;
		    }
		  else
		    {
		      gestionfichiers->freq_chrono_list_s=realloc(gestionfichiers->freq_chrono_list_s,
								  (gestionfichiers->freq_chrono_list_nb+nb)*sizeof(double)); 
		      verif_alloue_double1d("lire_data_mc",gestionfichiers->freq_chrono_list_s);
		      for (i=0;i<nb;i++) gestionfichiers->freq_chrono_list_s[gestionfichiers->freq_chrono_list_nb+i]=dlist[i];
		      gestionfichiers->freq_chrono_list_nb+=nb;
		    }
		}
	    }
	  else if (!strcmp(motcle,"CHAMP DE TEMPERATURES MAXIMALES")) 
	    {
	      ret1=rep_ch(repc,ch+i2+1);
	      if (ret1){
		if (!strcmp(repc,"OUI"))  gestionfichiers->champmax=1;
		else if (!strcmp(repc,"NON")) gestionfichiers->champmax=0;
		else ret1=0;
	      }
	    }
	  else if (!strcmp(motcle,"CHAMP DE FLUX THERMIQUE")) 
	    {
	      ret1=rep_ch(repc,ch+i2+1);
	      if (ret1){
		if (!strcmp(repc,"OUI"))  gestionfichiers->champflux=1;
		else if (!strcmp(repc,"NON")) gestionfichiers->champflux=0;
		else ret1=0;
	      }
	    }
	  else if (!strcmp(motcle,"SUPPRESSION DU CHAMP RESULTAT FINAL")) 
	    {
	      ret1=rep_ch(repc,ch+i2+1);
	      if (ret1){
		if (!strcmp(repc,"OUI"))  gestionfichiers->suppchampfinal=1;
		else if (!strcmp(repc,"NON")) gestionfichiers->suppchampfinal=0;
		else ret1=0;
	      }
	    }

	  else if (!strcmp(motcle,"NOMBRE ITERATIONS SOLVEUR TEMPERATURE")) {
	    nitmx_t=rep_int(ch+i2+1);
	  }
	  else if (!strcmp(motcle,"NOMBRE ITERATIONS SOLVEUR PRESSION VAPEUR")) {
	    nitmx_pv=rep_int(ch+i2+1);
	  }
	  else if (!strcmp(motcle,"NOMBRE ITERATIONS SOLVEUR PRESSION TOTALE")) {
	    nitmx_pt=rep_int(ch+i2+1);
	  }
	  else if (!strcmp(motcle,"PRECISION POUR LE SOLVEUR TEMPERATURE")) {
	    epsgcs_t=rep_dbl(ch+i2+1);	
	  }
	  else if (!strcmp(motcle,"PRECISION POUR LE SOLVEUR PRESSION VAPEUR")) {
	    epsgcs_pv=rep_dbl(ch+i2+1);	
	  }
	  else if (!strcmp(motcle,"PRECISION POUR LE SOLVEUR PRESSION TOTALE")) {
	    epsgcs_pt=rep_dbl(ch+i2+1);	
	  }
	  else if (!strcmp(motcle,"DIMENSION DU PROBLEME")) 
	    {
	      ret2=rep_ch(repc,ch+i2+1);
	      if (!strcmp(repc,"3D"))  	          {maillnodes->ndim=3; maillnodes->iaxisy=0;}
	      else if (!strcmp(repc,"2D_CART"))   {maillnodes->ndim=2; maillnodes->iaxisy=0;}
	      else if (!strcmp(repc,"2D_AXI_OX")) {maillnodes->ndim=2; maillnodes->iaxisy=1;}
	      else if (!strcmp(repc,"2D_AXI_OY")) {maillnodes->ndim=2; maillnodes->iaxisy=2;}
	      else ret2=0;
	    }
	}
    }

  /* indicateur de sorties chrono */
  gestionfichiers->freq_chrono=0;
  if (gestionfichiers->freq_chrono_s>0 || gestionfichiers->freq_chrono_nt>0 || 
      gestionfichiers->freq_chrono_list_nb>0)
    gestionfichiers->freq_chrono=1;


  /* Verifications */
  /* ------------- */

  if (syrglob_nparts==1 ||syrglob_rang==0){

    if (!ret1){
      if (SYRTHES_LANG == FR)  printf("\n ERREUR mot-cle CHAMP DE TEMPERATURES MAXIMALES=\n");
      else if (SYRTHES_LANG == EN)  printf("\n ERROR keyword CHAMP DE TEMPERATURES MAXIMALES=\n");
      ok=0;
    }


    if (!ret2){
      if (SYRTHES_LANG == FR)  printf(" ERREUR mot-cle DIMENSION DU PROBLEME=\n");
      else if (SYRTHES_LANG == EN)  printf("\n ERROR keyword DIMENSION DU PROBLEME=\n");
      ok=0;
    }


    if (pasdetemps->rdtts<0 && pasdetemps->dtmult.actif==0) {
      if (SYRTHES_LANG == FR) printf("\n ERREUR mot-cle PAS DE TEMPS SOLIDE=\n");
      else if (SYRTHES_LANG == EN)  printf("\n ERROR keyword PAS DE TEMPS SOLIDE=\n");
      ok=0;
    }
  
    if (nitmx_t<0) {
      if (SYRTHES_LANG == FR) printf("\n ERREUR mot-cle NOMBRE ITERATIONS SOLVEUR TEMPERATURE=\n");
      else if (SYRTHES_LANG == EN)  printf("\n ERROR keyword NOMBRE ITERATIONS SOLVEUR TEMPERATURE=\n");
      ok=0;
    }
    if (nitmx_pv<0) {
      if (SYRTHES_LANG == FR) printf("\n ERREUR mot-cle NOMBRE ITERATIONS SOLVEUR PRESSION VAPEUR=\n");
      else if (SYRTHES_LANG == EN)  printf("\n ERROR keyword NOMBRE ITERATIONS SOLVEUR PRESSION VAPEUR=\n");
      ok=0;
    }
    if (nitmx_pt<0) {
      if (SYRTHES_LANG == FR) printf("\n ERREUR mot-cle NOMBRE ITERATIONS SOLVEUR PRESSION TOTALE=\n");
      else if (SYRTHES_LANG == EN)  printf("\n ERROR keyword NOMBRE ITERATIONS SOLVEUR PRESSION TOTALE=\n");
      ok=0;
    }


    if (!ok) syrthes_exit(1);

  }


  /* Impressions */
  /* ----------- */
  if (syrglob_nparts==1 ||syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n *** CONDUCTION : RECAPITULATIF DES OPTIONS DU CALCUL\n");
	  if (maillnodes->ndim==3) printf("      - calcul 3D cartesien\n");
	  else if (maillnodes->ndim==2 && maillnodes->iaxisy==0) printf("      - calcul 2D cartesien\n");
	  else
	    switch(maillnodes->iaxisy)
	      { 
	      case 1: printf("      - calcul 2D axisymetrique. Axe d'axisymetrie = X\n"); break;
	      case 2: printf("      - calcul 2D axisymetrique. Axe d'axisymetrie = Y\n"); break;
	      }
	  if (pasdetemps->suite) printf("      - Il s'agit d'une suite de calcul\n");
	  if (!pasdetemps->dtmult.actif) printf("      - pas de temps sur le solide : %f\n",pasdetemps->rdtts);
	  if (pasdetemps->dtauto.actif)  printf("        le pas de temps evoluera de facon automatique\n"); 
	  if (pasdetemps->dtmult.actif) printf("      - des pas de temps multiples ont ete definis\n");
	  printf("      - nombre de pas de temps demandes : %d\n",pasdetemps->ntsmax); 
	  if (gestionfichiers->freq_chrono_nt>0)
	    printf("      - pas des sorties chronologiques (iterations) : %d\n",gestionfichiers->freq_chrono_nt);
	  else if (gestionfichiers->freq_chrono_s>0)
	    printf("      - pas des sorties chronologiques (secondes) : %f\n",gestionfichiers->freq_chrono_s);
	  else if(gestionfichiers->freq_chrono_list_nb>0)
	    {
	      printf("      - instants des sorties chronologiques (secondes) :\n      ");
	      for (i=0;i<gestionfichiers->freq_chrono_list_nb;i++) printf(" %f",gestionfichiers->freq_chrono_list_s[i]);
	      printf("\n");
	    }

	  if (gestionfichiers->champmax) printf("      - calcul des champs de temperature min et max\n");
	  else printf("      - pas de calcul des champs de temperature min et max\n");
	  if (gestionfichiers->champflux) printf("      - calcul des champs de flux de temperature\n");
	  else printf("      - pas de calcul des champs de flux de temperature\n");
	  printf("      - nombre max du solveur temperature : %d\n",nitmx_t);
	  printf("      - precision demandee pour le solveur temperature : %e\n",epsgcs_t);
	  if (nbVar>=2) {
	    printf("      - nombre max du solveur pression de vapeur : %d\n",nitmx_pv);
	    printf("      - precision demandee pour le solveur pression de vapeur : %e\n",epsgcs_pv);
	  }
	  if (nbVar==3) {
	    printf("      - nombre max du solveur pression totale : %d\n",nitmx_pt);
	    printf("      - precision demandee pour le solveur pression totale : %e\n",epsgcs_pt);
	  }
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n *** CONDUCTION : SUMMARY OF THE CALCULATION OPTIONS\n");
	  if (maillnodes->ndim==3) printf("      - 3D cartesian calculation\n");
	  else if (maillnodes->ndim==2 && maillnodes->iaxisy==0) printf("      - 3D cartesian calculation\n");
	  else
	    switch(maillnodes->iaxisy)
	      { 
	      case 1: printf("      - axisymetrical calculation. Axisymetrical axis = X\n"); break;
	      case 2: printf("      - axisymetrical calculation. Axisymetrical axis = Y\n"); break;
	      }
	  if (pasdetemps->suite) printf("      - This is a restart calculation\n");
	  if (!pasdetemps->dtmult.actif) printf("      - time step on the solid : %f\n",pasdetemps->rdtts);
	  if (pasdetemps->dtauto.actif)  printf("      - the time step will change automatically\n"); 
	  if (pasdetemps->dtmult.actif) printf("      - multiple time steps have been defined\n");
	  printf("      - number of time step asked : %d\n",pasdetemps->ntsmax); 
	  if (gestionfichiers->freq_chrono_nt>0)
	    printf("      - chronological output asked (steps) : %d\n",gestionfichiers->freq_chrono_nt);
	  else if (gestionfichiers->freq_chrono_s>0)
	    printf("      - chronological output asked (secondes) : %f\n",gestionfichiers->freq_chrono_s);
	  else if(gestionfichiers->freq_chrono_list_nb>0)
	    {
	      printf("      - times for chronological output (secondes) :\n      ");
	      for (i=0;i<gestionfichiers->freq_chrono_list_nb;i++) printf(" %f",gestionfichiers->freq_chrono_list_s[i]);
	      printf("\n");
	    }
	  if (gestionfichiers->champmax) printf("      - min and max temperature field calculation\n");
	  else printf("      - no calculation of the min and max temperature field\n");
	  if (gestionfichiers->champflux) printf("      - temperature flux field calculation\n");
	  else printf("      - no calculation of temperature flux field\n");
	  printf("      - maximum number of iterations in the temperature solver : %d\n",nitmx_t);
	  printf("      - precision required for the temperature solver : %e\n",epsgcs_t);
	  if (nbVar>=2) {
	    printf("      - maximum number of iterations in the  vapor pressure solver : %d\n",nitmx_pv);
	    printf("      - precision required for the vapor pressure solver : %e\n",epsgcs_pv);
	  }
	  if (nbVar==3) {
	    printf("      - maximum number of iterations in total pressure solver : %d\n",nitmx_pt);
	    printf("      - precision required for the total pressure solver : %e\n",epsgcs_pt);
	  }
	}
    }
   

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture du fichier de mots-cles syrthes.data : Temperature initiale  |
  |======================================================================| */
void lire_cini(struct Maillage maillnodes,struct Variable *variable)
{
  int i,j,indicT=0,indicPV=0,indicPT=0;
  int i1,i2,ok=1,ii,nb,nr,n,np;
  double val;

  if (syrglob_nparts==1 ||syrglob_rang==0){
    if (SYRTHES_LANG == FR)      printf("\n *** LECTURE DES CONDITIONS INITIALES\n");
    else if (SYRTHES_LANG == EN) printf("\n *** READING INITIAL CONDITIONS\n");
    fflush(stdout);
  }

  /* initialisation de la temperature */
  if (variable->nbvar==1)
    for (i=0;i<maillnodes.npoin;i++) variable->var[variable->adr_t][i] = 20.;

  else if (variable->nbvar==2)
    for (i=0;i<maillnodes.npoin;i++)
      {
	variable->var[variable->adr_t][i]  =     20.;
	variable->var[variable->adr_pv][i] =   2316.;
      }
  else if (variable->nbvar==3)
    for (i=0;i<maillnodes.npoin;i++)
      {
	variable->var[variable->adr_t][i]  =     20.;
	variable->var[variable->adr_pv][i] =   2316.;
	variable->var[variable->adr_pt][i] = 101300.;
      }


  if (syrglob_nparts==1 ||syrglob_rang==0){
    if (SYRTHES_LANG == FR)
      printf("\n *** LECTURE DES CONDITIONS INITIALES DANS LE FICHIER DE DONNEES\n");
    else if (SYRTHES_LANG == EN)
      printf("\n *** READING THE INITIAL CONDITIONS IN THE DATA FILE\n");
  }
  

  /* lecture du fichier de donnees */
  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"CINI_T")) 
	    {lire_ini(variable->var[variable->adr_t],"Temperature\0",maillnodes,ch,i2+1); indicT=1;}
	  else if (!strcmp(motcle,"CINI_PV") && variable->nbvar>1 ) 
	    {lire_ini(variable->var[variable->adr_pv],"PV\0",maillnodes,ch,i2+1); indicPV=1;}
	  else if (!strcmp(motcle,"CINI_PT") && variable->nbvar>1) 
	    {lire_ini(variable->var[variable->adr_pt],"PT\0",maillnodes,ch,i2+1); indicPT=1;}
	}
    }

  if (!indicT && (syrglob_nparts==1 ||syrglob_rang==0))
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("        --> Aucune condition initiale sur la temperature n'est presente dans le fichier de donnees\n");
	  printf("            La temperature est initialisee a 20 degres C par defaut\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("        --> No initial condition for temperature is present in the data file\n");
	  printf("            The initial temperature is set everywhere at 20 degres C (default value)\n");
	}
    }
  if (variable->nbvar>1 && !indicPV && (syrglob_nparts==1 ||syrglob_rang==0))
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("        --> Aucune condition initiale sur PV n'est presente dans le fichier de donnees\n");
	  printf("            PV est initialisee a 2316 par defaut\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("        --> No initial condition for temperature is present in the data file\n");
	  printf("            PV is initially set everywhere at 2316 (default value)\n");
	}
    }
  if (variable->nbvar>1 && !indicPT && (syrglob_nparts==1 ||syrglob_rang==0))
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("        --> Aucune condition initiale sur PT n'est presente dans le fichier de donnees\n");
	  printf("            PT est initialisee a 101300 par defaut\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("        --> No initial condition for PT is present in the data file\n");
	  printf("            PT is initially set everywhere at 101300 (default value)\n");
	}
    }
} 
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture du fichier de mots-cles syrthes.data : Temperature initiale  |
  |======================================================================| */
void lire_ini(double *t,char *nomvar,struct Maillage maillnodes,
	      char *ch,int id)

{
  int i,j,indic=0;
  int ii,nb,nr,n,np;
  double val;

  indic=1;
  rep_ndbl(1,&val,&ii,ch+id);
  rep_listint(ilist,&nb,ch+id+ii);
  for (n=0;n<nb;n++)
    {
      nr=ilist[n];
      if (nr==-1)
	{
	  for (i=0;i<maillnodes.npoin;i++) t[i]=val;
	  if (syrglob_nparts==1 ||syrglob_rang==0){
	    if (SYRTHES_LANG == FR)
	      printf("        --> CONDITION INITIALE : %s = %f  sur tout le domaine,\n ",nomvar,val);
	    else if (SYRTHES_LANG == EN)
	      printf("        --> INITIAL CONDITION : %s = %f imposed on the whole domain,\n ",nomvar,val);
	  }
	}
      else
	{
	  for (i=0;i<maillnodes.nelem;i++)
	    if (maillnodes.nrefe[i]==nr)
	      {
		for (j=0;j<maillnodes.ndim+1;j++) 
		  {
		    np=maillnodes.node[j][i];
		    t[np]=val;
		  }
	      }
	  if (syrglob_nparts==1 ||syrglob_rang==0){
	    if (SYRTHES_LANG == FR)
	      printf("        --> CONDITION INITIALE : %s = %f  sur les elements de reference %d\n",nomvar,val,nr);
	    else if (SYRTHES_LANG == EN)
	      printf("        --> INITIAL CONDITION : %s = %f imposed on the elements with reference %d\n",nomvar,val,nr);
	  }
	  
	}
    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |   Decodage des proprietes physiques                                  |
  |======================================================================| */
void decode_prophy(struct Maillage maillnodes,struct  Prophy *physol)
{
  int i,j;
  int i1,i2,i3,i4,id,ii,ii2,nb,nr,n,itype,manque,neletot,ityp;
  int nbc=3;
  int **ref;
  int *refnotdef;
  int kisonelemtot,korthonelemtot,kanisonelemtot;
  double val;

  ref=(int**)malloc(nbc*sizeof(int*));
  for (i=0;i<nbc;i++) {
    ref[i]=(int*)malloc(MAX_REF*sizeof(int));
    for (j=0;j<MAX_REF;j++) ref[i][j]=0;
  }

  refnotdef=(int*)malloc(MAX_REF*sizeof(int));
  for (j=0;j<MAX_REF;j++) refnotdef[j]=0;

  /* 1ere passe : decodage des references */
  /* ==================================== */
  
  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);

	  ityp=-1;
	  if (!strncmp(motcle,"CPHY_MAT",8)) {
	    ityp=0;
	    if      (!strncmp(motcle+strlen(motcle)-4,"_FCT",4)) ityp=1;
	    else if (!strncmp(motcle+strlen(motcle)-5,"_PROG",5)) ityp=2;
	  }

	  if (ityp>=0)
	    {
	      id=i2+1;

	      if (!strncmp(motcle,"CPHY_MAT_ISO",12)) 
		{
		  switch(ityp){
		  case 0: rep_ndbl(3,dlist,&ii,ch+id);id+=ii;  break;
		  case 1: for (i=0;i<3;i++) {ii=rep_ch(formule,ch+id); id+=ii;} break;  
		  }
		  rep_listint(ilist,&nb,ch+id);
		  if (ilist[0]==-1)
		    for (j=0;j<MAX_REF;j++) ref[0][j]=1;
		  else
		    for (n=0;n<nb;n++) ref[0][ilist[n]]=1;
		}

	      else if (!strncmp(motcle,"CPHY_MAT_ORTHO_2D",17)) 
		{
		  if (maillnodes.ndim==3)
		    {
		      if (SYRTHES_LANG == FR) {
			printf("\n\n ERREUR lire_prophy : on demande l'affectation");
			printf(" d'une conductivite orthotrope 2D\n");
			printf("                      alors qu'on est en dimension 3\n\n");
		      }
		      else if (SYRTHES_LANG == EN) {
			printf("\n\n ERROR lire_prophy : One requires");
			printf(" a 2D orthotropic conductivity\n");
			printf(" although the dimension is 3\n\n");
		      }
		      syrthes_exit(1);
		    }
		  else
		    {
		      switch(ityp){
		      case 0: rep_ndbl(4,dlist,&ii,ch+id); id+=ii; break;
		      case 1: for (i=0;i<4;i++) {ii=rep_ch(formule,ch+id); id+=ii;} break;
		      }
		      rep_listint(ilist,&nb,ch+id);
		      if (ilist[0]==-1)
			for (j=0;j<MAX_REF;j++) ref[1][j]=1;
		      else
			for (n=0;n<nb;n++) ref[1][ilist[n]]=1;
		    }
		}

	      else if (!strncmp(motcle,"CPHY_MAT_ORTHO_3D",17))
		{ 
		  if (maillnodes.ndim==2) 
		    {
		      if (SYRTHES_LANG == FR){
			printf("\n\n ERREUR lire_prophy : on demande l'affectation");
			printf(" d'une conductivite orthotrope 3D\n");
			printf("                      alors qu'on est en dimension 2\n\n");
		      }
		      else if (SYRTHES_LANG == EN){
			printf("\n\n ERROR lire_prophy : one requires");
			printf(" a 3D orthotropic conductivity\n");
			printf(" although the dimension is 2\n\n");
		      }
		      syrthes_exit(1);
		    }
		  else
		    {
		      switch(ityp){
		      case 0: rep_ndbl(5,dlist,&ii,ch+id); id+=ii; break;
		      case 1: for (i=0;i<5;i++) {ii=rep_ch(formule,ch+id); id+=ii;} break;
		      }
		      rep_listint(ilist,&nb,ch+id);
		      if (ilist[0]==-1)
			for (j=0;j<MAX_REF;j++) ref[1][j]=1;
		      else
			for (n=0;n<nb;n++) ref[1][ilist[n]]=1;
		    }
		}


	      else if (!strncmp(motcle,"CPHY_MAT_ANISO_2D",17))
		{ 
		  if (maillnodes.ndim==3) 
		    {
		      if (SYRTHES_LANG == FR){
			printf("\n\n ERREUR lire_prophy : on demande l'affectation");
			printf(" d'une conductivite anisotrope 2D\n");
			printf("                      alors qu'on est en dimension 3\n\n");
		      }
		      else if (SYRTHES_LANG == EN){
			printf("\n\n ERROR lire_prophy : on requires");
			printf(" a 2D anisotropic conductivity\n");
			printf(" although the dimension is 3\n\n");
		      }
		      syrthes_exit(1);
		    }
		  else
		    {
		      switch(ityp){
		      case 0: rep_ndbl(5,dlist,&ii,ch+id); id+=ii; break;
		      case 1: for (i=0;i<5;i++) {ii=rep_ch(formule,ch+id); id+=ii;} break;
		      }
		      rep_listint(ilist,&nb,ch+id);
		      if (ilist[0]==-1)
			for (j=0;j<MAX_REF;j++) ref[2][j]=1;
		      else
			for (n=0;n<nb;n++) ref[2][ilist[n]]=1;
		    }
		}
	      else if (!strncmp(motcle,"CPHY_MAT_ANISO_3D",17))
		{
		  if (maillnodes.ndim==2) 
		    {
		      if (SYRTHES_LANG == FR){
			printf("\n\n ERREUR lire_prophy : on demande l'affectation");
			printf(" d'une conductivite anisotrope 3D\n");
			printf("                      alors qu'on est en dimension 2\n\n");
		      }
		      else if (SYRTHES_LANG == EN){
			printf("\n\n ERROR lire_prophy : one requires");
			printf(" a 3D anisotropic conductivity\n");
			printf(" although the dimension is 2\n\n");
		      }
		      syrthes_exit(1);
		    }
		  else
		    {
		      switch(ityp){
		      case 0: rep_ndbl(14,dlist,&ii,ch+id); id+=ii; break;
		      case 1: for (i=0;i<14;i++) {ii=rep_ch(formule,ch+id); id+=ii;} break;
		      }
		      rep_listint(ilist,&nb,ch+id);
		      if (ilist[0]==-1)
			for (j=0;j<MAX_REF;j++) ref[2][j]=1;
		      else
			for (n=0;n<nb;n++) ref[2][ilist[n]]=1;
		    }
		}

	    }
	}
    }

  /* verification que des references ne sont pas de plusieurs type en meme temps */
  /* --------------------------------------------------------------------------- */
  for (j=0;j<MAX_REF;j++) 
    {
      if (ref[0][j]+ref[1][j]+ref[2][j] > 1)
	{
	  if (SYRTHES_LANG == FR) {
	    printf(" !!! ERREUR lors de la lecture des proprietes physiques\n");
	    printf("            La meme reference definit des elements isotrope/orthotrope/anisotrope\n");
	    printf("            --> revoir les mots-cle CPHY_MAT_...\n");
	  }
	  else if (SYRTHES_LANG == EN) {
	    printf(" !!! ERROR  During the reading of physical properties\n");
	    printf("            The same reference defined elements isotropic/orthotropic/anisotropic\n");
	    printf("            --> verify the key-words CPHY_MAT_...\n");
	  }
	  syrthes_exit(1);
	} 
    }

  /* compte du nombre d'elements de chaque type */
  /* ------------------------------------------ */
  physol->kiso.nelem=physol->kortho.nelem=physol->kaniso.nelem=0;

  for (i=0;i<maillnodes.nelem;i++)
    {
      nr=maillnodes.nrefe[i];
      if (ref[0][nr]) physol->kiso.nelem++;
      else if (ref[1][nr])physol->kortho.nelem++;
      else if (ref[2][nr])physol->kaniso.nelem++;
    }


  /* allocations et initialisations Rho, Cp, k */
  /* --------------------------------------------- */
  physol->ndmat=maillnodes.ndmat;
  physol->nelem=maillnodes.nelem;
  physol->kvar=1;

  physol->rho=(double*)malloc(physol->nelem*sizeof(double));
  verif_alloue_double1d("lire_prophy",physol->rho);
  avaleur1D(physol->nelem,physol->rho,-1);

  physol->cp=(double*)malloc(physol->nelem*sizeof(double));
  verif_alloue_double1d("lire_prophy",physol->cp);
  avaleur1D(physol->nelem,physol->cp,-1);

  perfo.mem_cond+=physol->nelem*2*sizeof(double);



  if (physol->kiso.nelem>0) 
    {
      physol->kiso.ele=(int*)malloc(physol->kiso.nelem*sizeof(int));
      verif_alloue_int1d("lire_prophy",physol->kiso.ele);

      physol->kiso.k=(double*)malloc(physol->kiso.nelem*sizeof(double));
      verif_alloue_double1d("lire_prophy",physol->kiso.k);
      avaleur1D(physol->kiso.nelem,physol->kiso.k,-1);
      perfo.mem_cond+=physol->kiso.nelem*sizeof(int);
      perfo.mem_cond+=physol->kiso.nelem*sizeof(double);
    }

  if (physol->kortho.nelem>0)
    {

      physol->kortho.ele=(int*)malloc(physol->kortho.nelem*sizeof(int));
      verif_alloue_int1d("lire_prophy",physol->kortho.ele);
      perfo.mem_cond+=physol->kortho.nelem*sizeof(int);

      physol->kortho.k11=(double*)malloc(physol->kortho.nelem*sizeof(double));
      physol->kortho.k22=(double*)malloc(physol->kortho.nelem*sizeof(double));
      verif_alloue_double1d("lire_prophy",physol->kortho.k11);
      verif_alloue_double1d("lire_prophy",physol->kortho.k22);
      avaleur1D(physol->kortho.nelem,physol->kortho.k11,-1);
      avaleur1D(physol->kortho.nelem,physol->kortho.k22,-1);
      perfo.mem_cond+=physol->kortho.nelem*2*sizeof(double);
      if (maillnodes.ndim==3)
	{
	  physol->kortho.k33=(double*)malloc(physol->kortho.nelem*sizeof(double));
	  verif_alloue_double1d("lire_prophy",physol->kortho.k33);
	  avaleur1D(physol->kortho.nelem,physol->kortho.k33,-1);
	  perfo.mem_cond+=physol->kortho.nelem*sizeof(double);
	}
    }
  

   if (physol->kaniso.nelem>0) 
    {
      physol->kaniso.ele=(int*)malloc(physol->kaniso.nelem*sizeof(int));
      verif_alloue_int1d("lire_prophy",physol->kaniso.ele);
      perfo.mem_cond+=physol->kaniso.nelem*sizeof(int);

      physol->kaniso.k11=(double*)malloc(physol->kaniso.nelem*sizeof(double));
      physol->kaniso.k22=(double*)malloc(physol->kaniso.nelem*sizeof(double));
      physol->kaniso.k12=(double*)malloc(physol->kaniso.nelem*sizeof(double));
      verif_alloue_double1d("lire_prophy",physol->kaniso.k11);
      verif_alloue_double1d("lire_prophy",physol->kaniso.k22);
      verif_alloue_double1d("lire_prophy",physol->kaniso.k12);
      avaleur1D(physol->kaniso.nelem,physol->kaniso.k11,-1);
      avaleur1D(physol->kaniso.nelem,physol->kaniso.k22,-1);
      avaleur1D(physol->kaniso.nelem,physol->kaniso.k12,-1);
      perfo.mem_cond+=physol->kaniso.nelem*3*sizeof(double);
      if (maillnodes.ndim==3)
	{
	  physol->kaniso.k33=(double*)malloc(physol->kaniso.nelem*sizeof(double));
	  physol->kaniso.k13=(double*)malloc(physol->kaniso.nelem*sizeof(double));
	  physol->kaniso.k23=(double*)malloc(physol->kaniso.nelem*sizeof(double));
	  verif_alloue_double1d("lire_prophy",physol->kaniso.k33);
	  verif_alloue_double1d("lire_prophy",physol->kaniso.k13);
	  verif_alloue_double1d("lire_prophy",physol->kaniso.k23);
	  avaleur1D(physol->kaniso.nelem,physol->kaniso.k33,-1);
	  avaleur1D(physol->kaniso.nelem,physol->kaniso.k13,-1);
	  avaleur1D(physol->kaniso.nelem,physol->kaniso.k23,-1);
	  perfo.mem_cond+=physol->kaniso.nelem*3*sizeof(double);
	}
    }

 

  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;


  /* Liste des elements iso, ortho et aniso */
  /* -------------------------------------------- */
  for (i1=i2=i3=i=0;i<maillnodes.nelem;i++)
    {
      nr=maillnodes.nrefe[i];
      if (ref[0][nr]) {physol->kiso.ele[i1]=i; i1++;}
      else if (ref[1][nr]){physol->kortho.ele[i2]=i; i2++;}
      else if (ref[2][nr]){physol->kaniso.ele[i3]=i; i3++;}
      else refnotdef[nr]=1;
    }
  
  
  
  /* liberation memoire */
  /* ------------------ */
  for (i=0;i<nbc;i++) free(ref[i]);
  free(ref);
  


  /* affichages */
  /* ---------- */

  kisonelemtot=somme_int_parall(physol->kiso.nelem);
  korthonelemtot=somme_int_parall(physol->kortho.nelem);
  kanisonelemtot=somme_int_parall(physol->kaniso.nelem);
  neletot=somme_int_parall(maillnodes.nelem);

  physol->isotro=0;   if (kisonelemtot>0) physol->isotro=1;
  physol->orthotro=0; if (korthonelemtot>0) physol->orthotro=1;
  physol->anisotro=0; if (kanisonelemtot>0) physol->anisotro=1;


  if (syrglob_nparts==1 ||syrglob_rang==0)
    {

      /* on verifie que tous les elts ont une conductivite */
      /* ------------------------------------------------- */
      if (kisonelemtot+korthonelemtot+kanisonelemtot != neletot)
	{
	  manque=neletot - kisonelemtot - korthonelemtot - kanisonelemtot;
	  if (SYRTHES_LANG == FR) {
	    printf(" !!! ERREUR lors de l'initialisation des proprietes physiques des materiaux\n");
	    printf("            Aucune propriete materiau n'est definie pour %d elements\n",manque);
	    printf("            --> revoir les mots-cle CPHY_MAT_...\n");
	    printf("            Liste des references concernees :\n");
	  }
	  else if (SYRTHES_LANG == EN) {
	    printf(" !!! ERROR  During the reading of physical properties\n");
	    printf("            No material properties for %d elements\n",manque);
	    printf("            --> verify the key-words CPHY_MAT_...\n");
	    printf("            List of concerned references :\n");
	  }
	  for (i=0;i<MAX_REF;i++) 
	    if (refnotdef[i]!=0) printf("               %d\n",i);

	  syrthes_exit(1);
	}


      if (SYRTHES_LANG == FR)
	{
	  printf("\n *** TRI DES ELEMENTS EN FONCTION DES PROPRIETES PHYSIQUES (cree_liste_prophy)\n");
	  printf("                             |---------------|\n");
	  printf("                             |   Nb elements |\n");
	  printf("      |----------------------|---------------|\n");
	  printf("      | Materiau Isotrope    |    %10d |\n",kisonelemtot);
	  printf("      | Materiau Orthotrope  |    %10d |\n",korthonelemtot);
	  printf("      | Materiau Anisotrope  |    %10d |\n",kanisonelemtot);
	  printf("      |--------------------------------------|\n"); 
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n *** Sorting elements according their physical properties (cree_liste_prophy)\n");
	  printf("                             |---------------|\n");
	  printf("                             |   Nb elements |\n");
	  printf("      |----------------------|---------------|\n");
	  printf("      | Isotropic material   |    %10d |\n",kisonelemtot);
	  printf("      | Orthotropic material |    %10d |\n",korthonelemtot);
	  printf("      | Anisotropic material |    %10d |\n",kanisonelemtot);
	  printf("      |--------------------------------------|\n"); 
	}
    }

  free(refnotdef);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture de syrthes.env                                        |
  |======================================================================| */
void lire_prophy(struct Maillage maillnodes,struct  Prophy *physol)
{
  int i,j;
  int i1,i2,i3,i4,id,ii,nb,nr,n;
  double val;
  
  if (syrglob_nparts==1 ||syrglob_rang==0){
    if (SYRTHES_LANG == FR)  
      printf("\n *** LECTURE DES PROPRIETES PHYSIQUES DES MATERIAUX DANS LE FICHIER DE DONNEES\n");
    else if (SYRTHES_LANG == EN)
      printf("\n *** READING OF MATERIAL PROPRETIES IN THE DATA FILE\n");
  }

  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  id=i2+1;

	  if (!strcmp(motcle,"CONDUCTIVITE CONSTANTE")) 
	    {
	      rep_ch(repc,ch+i2+1);
	      if (!strcmp(repc,"OUI")) physol->kvar=0;
	    }

	  else if (!strcmp(motcle,"CPHY_MAT_ISO")) 
	    {
	      rep_ndbl(3,dlist,&ii,ch+id);
	      rep_listint(ilist,&nb,ch+id+ii);
		  
	      if (syrglob_nparts==1 ||syrglob_rang==0){
		if (SYRTHES_LANG == FR)       printf("        --> Materiau isotrope impose sur les references");
		else if (SYRTHES_LANG == EN)  printf("        --> Isotropic material imposed on the references");
		for (n=0;n<nb;n++) printf(" %d",ilist[n]); printf("\n");
		printf("            rho=%f   Cp=%f   k=%f\n",dlist[0],dlist[1],dlist[2]);
	      }
	      
	      if (ilist[0]==-1){
		for (i=0;i<maillnodes.nelem;i++) {physol->rho[i]=dlist[0]; physol->cp[i]=dlist[1];}
		for (i=0;i<physol->kiso.nelem;i++) physol->kiso.k[i]=dlist[2];
	      }
	      else
		for (n=0;n<nb;n++)
		  {
		    nr=ilist[n];
		    for (i=0;i<maillnodes.nelem;i++) 
		      if (maillnodes.nrefe[i]==nr) {physol->rho[i]=dlist[0];physol->cp[i]=dlist[1];}
		    for (i=0;i<physol->kiso.nelem;i++) 
		      if (maillnodes.nrefe[physol->kiso.ele[i]]==nr) physol->kiso.k[i]=dlist[2];
		  }
	    }
	  
	  
	  else if (!strcmp(motcle,"CPHY_MAT_ORTHO_2D") || !strcmp(motcle,"CPHY_MAT_ORTHO_3D")) 
	    {
	      if (maillnodes.ndim==2) rep_ndbl(4,dlist,&ii,ch+id);
	      else rep_ndbl(5,dlist,&ii,ch+id);
	      rep_listint(ilist,&nb,ch+id+ii);
	      
	      if (syrglob_nparts==1 ||syrglob_rang==0){
		if (SYRTHES_LANG == FR)       printf("        --> Materiau orthotrope impose sur les references");
		else if (SYRTHES_LANG == EN ) printf("        --> Orthotropic material imposed on the references");
		for (n=0;n<nb;n++) printf(" %d",ilist[n]); printf("\n");
		if (maillnodes.ndim==2)
		  printf("            rho=%f   Cp=%f   kx=%f   ky=%f\n",dlist[0],dlist[1],dlist[2],dlist[3]);
		else
		  printf("            rho=%f   Cp=%f   kx=%f    ky=%f   kz=%f\n",dlist[0],dlist[1],dlist[2],dlist[3],dlist[4]);
		
	      }
	      
	      if (ilist[0]==-1){
		for (i=0;i<maillnodes.nelem;i++) {physol->rho[i]=dlist[0];physol->cp[i]=dlist[1];}
		for (i=0;i<physol->kortho.nelem;i++) 
		  {
		    physol->kortho.k11[i]=*(dlist+2);
		    physol->kortho.k22[i]=*(dlist+3);
		    if (maillnodes.ndim==3) physol->kortho.k33[i]=*(dlist+4);
		  }
	      }
	      else
		for (n=0;n<nb;n++)
		  {
		    nr=ilist[n];
		    for (i=0;i<maillnodes.nelem;i++) 
		      if (maillnodes.nrefe[i]==nr) {physol->rho[i]=dlist[0];physol->cp[i]=dlist[1];}
		    
		    for (i=0;i<physol->kortho.nelem;i++) 
		      if (maillnodes.nrefe[physol->kortho.ele[i]]==nr) 
			{
			  physol->kortho.k11[i]=*(dlist+2);
			  physol->kortho.k22[i]=*(dlist+3);
			  if (maillnodes.ndim==3) physol->kortho.k33[i]=*(dlist+4);
			}
		  }
	    }
	  
	  else if (!strcmp(motcle,"CPHY_MAT_ANISO_2D"))
	    {
	      rep_ndbl(5,dlist,&ii,ch+id);
	      rep_listint(ilist,&nb,ch+id+ii);
	      
	      if (syrglob_nparts==1 ||syrglob_rang==0){
		if (SYRTHES_LANG == FR)       printf("        --> Materiau anisotrope impose sur les references");
		else if (SYRTHES_LANG == EN ) printf("        --> Anisotropic material imposed on the references");
		for (n=0;n<nb;n++) printf(" %d",ilist[n]); printf("\n");
		printf("            rho=%f   Cp=%f   kx=%f   ky=%f   alpha=%f\n",dlist[0],dlist[1],dlist[2],dlist[3],dlist[4]);
	      }
	      
	      rotation_k(2,dlist+2);
	      
	      if (ilist[0]==-1){
		for (i=0;i<maillnodes.nelem;i++) {physol->rho[i]=dlist[0];physol->cp[i]=dlist[1];}
		for (i=0;i<physol->kaniso.nelem;i++) 
		  {
		    physol->kaniso.k11[i]=dlist[2];
		    physol->kaniso.k22[i]=dlist[3];
		    physol->kaniso.k12[i]=dlist[4];
		  }
	      }
	      else
		for (n=0;n<nb;n++)
		  {
		    nr=ilist[n];
		    for (i=0;i<maillnodes.nelem;i++) 
		      if (maillnodes.nrefe[i]==nr) {physol->rho[i]=dlist[0];physol->cp[i]=dlist[1];}
		    
		    for (i=0;i<physol->kaniso.nelem;i++) 
		      if (maillnodes.nrefe[physol->kaniso.ele[i]]==nr) 
			{
			  physol->kaniso.k11[i]=dlist[2];
			  physol->kaniso.k22[i]=dlist[3];
			  physol->kaniso.k12[i]=dlist[4];
			}
		  }
	    }
	  else if (!strcmp(motcle,"CPHY_MAT_ANISO_3D")) 
	    {
	      rep_ndbl(14,dlist,&ii,ch+id);
	      rep_listint(ilist,&nb,ch+id+ii);
	      
	      if (syrglob_nparts==1 ||syrglob_rang==0){
		if (SYRTHES_LANG == FR)       printf("        --> Materiau anisotrope impose sur les references");
		else if (SYRTHES_LANG == EN ) printf("        --> Anisotropic material imposed on the references");
		for (n=0;n<nb;n++) printf(" %d",ilist[n]); printf("\n");
		printf("            rho=%f   Cp=%f   kx=%f    ky=%f   kz=%f\n",dlist[0],dlist[1],dlist[2],dlist[3],dlist[4]);
		printf("                             axis (%f, %f, %f)   \n",dlist[5],dlist[6],dlist[7]);
		printf("                             axis (%f, %f, %f)   \n",dlist[8],dlist[9],dlist[10]);
		printf("                             axis (%f, %f, %f)   \n",dlist[11],dlist[12],dlist[13]);
	      }
	      
	      rotation_k(3,dlist+2);
	      
	      if (ilist[0]==-1){
		for (i=0;i<maillnodes.nelem;i++) {physol->rho[i]=dlist[0];physol->cp[i]=dlist[1];}
		for (i=0;i<physol->kaniso.nelem;i++) 
		  {
		    physol->kaniso.k11[i]=dlist[2];
		    physol->kaniso.k22[i]=dlist[3];
		    physol->kaniso.k33[i]=dlist[4];
		    physol->kaniso.k12[i]=dlist[5];
		    physol->kaniso.k13[i]=dlist[6];
		    physol->kaniso.k23[i]=dlist[7];
		  }
	      }
	      else
		for (n=0;n<nb;n++)
		  {
		    nr=ilist[n];
		    
		    for (i=0;i<maillnodes.nelem;i++) 
		      if (maillnodes.nrefe[i]==nr) {physol->rho[i]=dlist[0];physol->cp[i]=dlist[1];}
		    
		    for (i=0;i<physol->kaniso.nelem;i++) 
		      if (maillnodes.nrefe[physol->kaniso.ele[i]]==nr) 
			{
			  physol->kaniso.k11[i]=dlist[2];
			  physol->kaniso.k22[i]=dlist[3];
			  physol->kaniso.k33[i]=dlist[4];
			  physol->kaniso.k12[i]=dlist[5];
			  physol->kaniso.k13[i]=dlist[6];
			  physol->kaniso.k23[i]=dlist[7];
			}
		  } 
	    }
	  
	}
    }
  
  
  if (affich.cond_prophy)
    {
      if (SYRTHES_LANG == FR)
	printf("\n     Impression des proprietes physiques\n");
      else if (SYRTHES_LANG == EN)
	printf("\n     Printing of the physical properties\n");

      printf("\n     --> rho, cp\n");
      for (i=0;i<maillnodes.nelem;i++)
	printf(" rhocp i=numglob=%d rho=%f cp=%f\n",i,physol->rho[i],physol->cp[i]);

      if (physol->kiso.nelem)
	{
	  if (SYRTHES_LANG == FR)
	    printf("\n     -->k isotrope variable\n");
	  else if (SYRTHES_LANG == EN)
	    printf("\n     -->k isotropic and variable\n");
	  for (i=0;i<physol->kiso.nelem;i++)
	    printf(" kiso i=%d numglob=%d k=%f\n",i,physol->kiso.ele[i],physol->kiso.k[i]);
	}

      if (physol->kortho.nelem)
	{
	  if (SYRTHES_LANG == FR)
	    printf("\n     --> k orthotrope variable\n");
	  else if (SYRTHES_LANG == EN)
	    printf("\n     --> k orthotropic and variable\n");
	  for (i=0;i<physol->kortho.nelem;i++)
	    {
	      printf(" kortho i=%d numglob=%d k11=%f k22=%f",i,physol->kortho.ele[i],physol->kortho.k11[i],physol->kortho.k22[i]);
	      if (maillnodes.ndim==3) printf(" k33=%f",physol->kortho.k33[i]);
	      printf("\n");
	    }
	}

      if (physol->kaniso.nelem)
	{
	  if (SYRTHES_LANG == FR)
	    printf("\n     --> k anisotrope variable\n");
	  else if (SYRTHES_LANG == EN)
	    printf("\n     --> k anisotropic and variable\n");

	  for (i=0;i<physol->kaniso.nelem;i++)
	    {
	      printf(" kaniso i=%d numglob=%d k11=%f k22=%f k12=%f",i,physol->kaniso.ele[i],physol->kaniso.k11[i],physol->kaniso.k22[i],physol->kaniso.k12[i]);
	      if (maillnodes.ndim==3) printf(" k33=%f k13=%f k23=%f",physol->kaniso.k33[i],physol->kaniso.k13[i],physol->kaniso.k23[i]);
	      printf("\n");
	    }
	}
    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void rotation_k(int ndim,double *param)
{
  int i,j;
  double teta,det;
  double kx,ky,kz;
  double a11,a12,a13,a21,a22,a23,a31,a32,a33;
  double ca11,ca12,ca13,ca21,ca22,ca23,ca31,ca32,ca33;
  double va11,va12,va13,va21,va22,va23,va31,va32,va33;
  double f11,f12,f13,f22,f23,f33;
  double eps=1.e-8;

  if (ndim==2)
    {
      kx=param[0]; ky=param[1];  /* kx,ky */
      teta=param[2]*Pi/180.;     /* angle */

      a11 = cos(teta);        a12 = -sin(teta);
      a21 = sin(teta);        a22 =  cos(teta);
   

      va11 =  cos(teta);      va12 = sin(teta);
      va21 = -sin(teta);      va22 = cos(teta);

      param[0]=va11*a11*kx + va12*a21*ky;         param[2]=va11*a12*kx + va12*a22*ky;
                                                  param[1]=va21*a12*kx + va22*a22*ky; 
    }
  
  else
    {
      kx=param[0]; ky=param[1]; kz=param[2];
      a11=param[3];  a12=param[6];    a13=param[9];
      a21=param[4];  a22=param[7];    a23=param[10];
      a31=param[5];  a32=param[8];    a33=param[11];

      det=a11*a22*a33 + a12*a23*a31 + a13*a21*a32 - a31*a22*a13 - a21*a12*a33 - a11*a32*a13;
      
      if (fabs(det)<eps){
	printf(" %%%% Erreur de donnees pour la definition du nouveau repere pour l'anisotropie\n");
	exit(1);
      }

      /* matrice des cofacteurs */
      ca11=  a22*a33-a32*a23;   ca12=-(a21*a33-a31*a23);    ca13=  a21*a32-a31*a22;
      ca21=-(a12*a33-a32*a13);  ca22=  a11*a33-a31*a13;     ca23=-(a11*a32-a31*a12);
      ca31=  a12*a23-a22*a13;   ca32=-(a11*a23-a21*a13);    ca33=  a11*a22-a21*a12;

      /* matrice inverse */
      va11=ca11/det;  va12=ca21/det;    va13=ca31/det;
      va21=ca12/det;  va22=ca22/det;    va23=ca32/det;
      va31=ca13/det;  va32=ca23/det;    va33=ca33/det;


      f11=kx*va11*a11+ky*va12*a21+kz*va13*a31;
      f12=kx*va11*a12+ky*va12*a22+kz*va13*a32;
      f13=kx*va11*a13+ky*va12*a23+kz*va13*a33;
      f22=kx*va21*a12+ky*va22*a22+kz*va23*a32;
      f23=kx*va21*a13+ky*va22*a23+kz*va23*a33;
      f33=kx*va31*a13+ky*va32*a23+kz*va33*a33;

      param[0]=f11;
      param[1]=f22;
      param[2]=f33;
      param[3]=f12;
      param[4]=f13;
      param[5]=f23;
    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_histo (struct Maillage maillnodes, struct Histo *histos)
{
  int i,j,k;
  int i1,i2,i3,i4,id,ok=1,ii,nb,nr,n,err=0,err2=0;
  int numel,num, num1,pas,np,jmin,histocoo=0;
  int nhloc,nhtot;
  int *p;
  int n0,n1,n2,n3;
  double val,x,y,z,xl1,xl2,xl3,xl4;
  struct node *arbre,*noeud;
  double size_min,dim_boite[6];
  int nbeltmax_tree3d=100000;
  double toler=1.e-6;

  if (syrglob_nparts==1 ||syrglob_rang==0){
    if (SYRTHES_LANG == FR)      printf("\n *** LECTURE DES SONDES\n");
    else if (SYRTHES_LANG == EN) printf("\n *** READING PROBES\n");
    fflush(stdout);
  }
 
  /* initialisations */
  histos->actif=0; 
  histos->freq=-1; 
  histos->freq_nt=-1; 
  histos->freq_list_nb=0;
  histos->npoin=histos->npoincoo=histos->npoinref=0;


  /* premiere lecture pour savoir s'il y a des histo definis a partir des coord */
  fseek(fdata,0,SEEK_SET);
  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"HIST")) 
	    {
	      histos->actif=1;
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      if (!strcmp(motcle,"COORD")) histocoo=1;
	    }
	}
    }

  /* s'il n'y a pas d'histo a lire : rien a faire */
  if  (histos->actif==0) return;

  /* preparation de la recherche des positions des historiques */
  if (histocoo)
    {
      histos->coord=(double**)malloc(maillnodes.ndim*sizeof(double*));
      histos->bary=(double**)malloc((maillnodes.ndim+1)*sizeof(double*));
      arbre= (struct node *)malloc(sizeof(struct node));
     
      if (maillnodes.ndim==2)
	build_quadtree_2d(arbre,maillnodes.npoin,maillnodes.nelem,maillnodes.node,maillnodes.coord,
			&size_min,dim_boite,nbeltmax_tree3d);
      else
	build_octree_3d(arbre,maillnodes.npoin,maillnodes.nelem,maillnodes.node,maillnodes.coord,
			&size_min,dim_boite,nbeltmax_tree3d);
    }

  /* lecture effective des historiques */
  
  fseek(fdata,0,SEEK_SET);
      
      
  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"HIST")) 
	    {
	      histos->actif=1;
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      
	      if (!strcmp(motcle,"FREQ_SECONDS"))
		/*-----*/ 
		{
		  rep_ndbl(1,&val,&ii,ch+id);
		  histos->freq=val;
		}
	      
	      else if (!strcmp(motcle,"FREQ_ITER"))
		/*-----*/ 
		{
		  histos->freq_nt=rep_int(ch+id);
		}
	      else if (!strcmp(motcle,"FREQ_LIST_TIMES"))
		/*-----*/ 
		{
		  rep_listdble(dlist,&nb,ch+id);
		  if (nb>0 && dlist[0]<=0)
		    {
		      histos->freq_list_nb=0;
		    }
		  else
		    {
		      if (histos->freq_list_nb<=0)
			{
			  histos->freq_list_nb=nb;
			  histos->freq_list_s=(double*)malloc(nb*sizeof(double));
			  verif_alloue_double1d("lire_data_mc",histos->freq_list_s);
			  for (i=0;i<nb;i++) histos->freq_list_s[i]=dlist[i];
			}
		      else
			{
			  histos->freq_list_s=realloc(histos->freq_list_s,
						      (histos->freq_list_nb+nb)*sizeof(double)); 
			  verif_alloue_double1d("lire_data_mc",histos->freq_list_s);
			  for (i=0;i<nb;i++) histos->freq_list_s[histos->freq_list_nb+i]=dlist[i];
			  histos->freq_list_nb+=nb;
			}
		    }
		}
	      else if (!strcmp(motcle,"NOEUDS")) 
		/*------*/ 
		{
		  if (syrglob_nparts>1)
		    {
		      if (SYRTHES_LANG == FR)
			{
			  printf(" Attention : en fonctionnement parallele, il n'est pas possible de realiser\n");
			  printf("             des historiques a partir des numeros des noeuds\n");
			  printf("       -->   utilisez le mot-cle COORD\n");
			}
		      else if (SYRTHES_LANG == EN)
			{
			  printf(" Warning : in parallel, it is not possible to activate\n");
			  printf("             thermal probes using the nodes number\n");
			  printf("       -->   use instead the keyword COORD\n");
			}
		    }
		  else
		    {
		      rep_listint(ilist,&nb,ch+id);
		      if (histos->npoin>0) 
			histos->nump=(int*)realloc(histos->nump,(histos->npoin+nb)*sizeof(int));
		      else                 
			histos->nump=(int*)malloc(nb*sizeof(int));
		      verif_alloue_int1d("lire_histo",histos->nump);
		      perfo.mem_cond+=nb*sizeof(int);
		      for (i=0;i<nb;i++) 
			{
			  if (*(ilist+i)>maillnodes.npoin)
			    {
			      err=1;
			      if (SYRTHES_LANG == FR)
				{
				  printf("\n %%%% ERREUR lire_histo : le numero de noeud demande pour les ");
				  printf("historiques conduction\n    est superieur au nombre total de noeuds : %d > %d \n",
					 *(ilist+i),maillnodes.npoin);
				}  
			      else if (SYRTHES_LANG == EN)
				{
				  printf("\n %%%% ERROR lire_histo : the node number given for ");
				  printf("the conduction probe historic\n   is above the total number of nodes : %d > %d \n",
					 *(ilist+i),maillnodes.npoin);
				}  
			    }
			  histos->nump[i+histos->npoin]=*(ilist+i)-1;
			}
		      histos->npoin+=nb;
		    }
		}
	      else if (!strcmp(motcle,"COORD")) 
		/*-----*/ 
		{
		  rep_listdble(dlist,&nb,ch+id);
		  if (nb%maillnodes.ndim!=0)
		    {
		      if (SYRTHES_LANG == FR)
			{
			  printf("\n %%%% ERREUR lire_histo : le nombre de coordonnees des noeuds pour les ");
			  printf("historiques conduction\n    n'est pas coherent : %d coordonnees sont definies \n",
				 nb);
			}
		      else if (SYRTHES_LANG == EN)
			{
			  printf("\n %%%% ERROR lire_histo : the number of coordinates given for the  ");
			  printf("conduction probe historic\n    is not coherent : %d coordinates are defined \n",
				 nb);
			}
		      err=1;
		    }
		  np=nb/maillnodes.ndim;
		  if (maillnodes.ndim==2)
		    {
		      for (i=0;i<np;i++) 
			{
			  x=*(dlist+i*maillnodes.ndim); y=*(dlist+i*maillnodes.ndim+1);

			  if (in_rectan(x,y,dim_boite[0],dim_boite[1],dim_boite[2],dim_boite[3]))
			    {
			      noeud = arbre; err2=0;
			      findel(noeud,maillnodes.ndim,maillnodes.coord,maillnodes.node, 
				     x,y,0,&numel,toler,&err2);
			      if (! err2)
				{
				  n0=maillnodes.node[0][numel]; n1=maillnodes.node[1][numel]; 
				  n2=maillnodes.node[2][numel]; 
				  bary_tria(x,y,
					    maillnodes.coord[0][n0],maillnodes.coord[1][n0],
					    maillnodes.coord[0][n1],maillnodes.coord[1][n1],
					    maillnodes.coord[0][n2],maillnodes.coord[1][n2],
					    &xl1,&xl2,&xl3);
				  
				  if (histos->npoincoo>0)
				    { 
				      histos->numev=(int*)realloc(histos->numev,(histos->npoincoo+1)*sizeof(int));
				      for (j=0;j<maillnodes.ndim;j++)
					histos->coord[j]=(double*)realloc(histos->coord[j],(histos->npoincoo+1)*sizeof(double));
				      for (j=0;j<maillnodes.ndim+1;j++)
					histos->bary[j]=(double*)realloc(histos->bary[j],(histos->npoincoo+1)*sizeof(double));
				    }
				  else
				    {     
				      histos->numev=(int*)malloc(sizeof(int));
				      for (j=0;j<maillnodes.ndim;j++)	histos->coord[j]=(double*)malloc(sizeof(double));
				      for (j=0;j<maillnodes.ndim+1;j++)	histos->bary[j]=(double*)malloc(sizeof(double));
				    }
				  verif_alloue_int1d("lire_histo",histos->numev);
				  perfo.mem_cond+= sizeof(int)+(2*maillnodes.ndim+1)*sizeof(double);

				  histos->numev[histos->npoincoo]=numel;
				  histos->coord[0][histos->npoincoo]=x;   histos->coord[1][histos->npoincoo]=y; 
				  histos->bary[0][histos->npoincoo]=xl1;  histos->bary[1][histos->npoincoo]=xl2;  
				  histos->bary[2][histos->npoincoo]=xl3; 

				  histos->npoincoo++;

				}
			    }

			} /* fin du for */

		    }
		  else  /* dimension 3 */
		    {
		      for (i=0;i<np;i++) 
			{
			  x=*(dlist+i*maillnodes.ndim); y=*(dlist+i*maillnodes.ndim+1); z=*(dlist+i*maillnodes.ndim+2);

			  if (in_boite(x,y,z,dim_boite[0],dim_boite[1],
				       dim_boite[2],dim_boite[3], dim_boite[4],dim_boite[5]))
			    {
			      noeud = arbre; err2=0;
			      findel(noeud,maillnodes.ndim,maillnodes.coord,maillnodes.node, 
				     x,y,z,&numel,toler,&err2);

			      if (! err2)
				{
				  n0=maillnodes.node[0][numel]; n1=maillnodes.node[1][numel]; 
				  n2=maillnodes.node[2][numel]; n3=maillnodes.node[3][numel]; 
				  bary_tetra(x,y,z,
					     maillnodes.coord[0][n0],maillnodes.coord[1][n0],maillnodes.coord[2][n0],
					     maillnodes.coord[0][n1],maillnodes.coord[1][n1],maillnodes.coord[2][n1],
					     maillnodes.coord[0][n2],maillnodes.coord[1][n2],maillnodes.coord[2][n2],
					     maillnodes.coord[0][n3],maillnodes.coord[1][n3],maillnodes.coord[2][n3],
					     &xl1,&xl2,&xl3,&xl4);
				  
				  if (histos->npoincoo>0)
				    { 
				      histos->numev=(int*)realloc(histos->numev,(histos->npoincoo+1)*sizeof(int));
				      for (j=0;j<maillnodes.ndim;j++)
					histos->coord[j]=(double*)realloc(histos->coord[j],(histos->npoincoo+1)*sizeof(double));
				      for (j=0;j<maillnodes.ndim+1;j++)
					histos->bary[j]=(double*)realloc(histos->bary[j],(histos->npoincoo+1)*sizeof(double));
				    }
				  else
				    {     
				      histos->numev=(int*)malloc(sizeof(int));
				      for (j=0;j<maillnodes.ndim;j++)	histos->coord[j]=(double*)malloc(sizeof(double));
				      for (j=0;j<maillnodes.ndim+1;j++)	histos->bary[j]=(double*)malloc(sizeof(double));
				    }
				  verif_alloue_int1d("lire_histo",histos->numev);
				  perfo.mem_cond+= sizeof(int)+(2*maillnodes.ndim+1)*sizeof(double);

				  histos->numev[histos->npoincoo]=numel;
				  histos->coord[0][histos->npoincoo]=x;   histos->coord[1][histos->npoincoo]=y;  
				  histos->coord[2][histos->npoincoo]=z;
				  histos->bary[0][histos->npoincoo]=xl1;  histos->bary[1][histos->npoincoo]=xl2;  
				  histos->bary[2][histos->npoincoo]=xl3;  histos->bary[3][histos->npoincoo]=xl4;

				  histos->npoincoo++;

				}
			    }

			} /* fin du for */
		    } /* fin de la dimension 3 */
		  
		} /* fin du traitement par COORD */
		  

		
		  else if (!strcmp(motcle,"REFERENCES")) 
		                          /*---------*/ 
		    {
		      rep_listint(ilist,&nb,ch+id);
		      /* compte des noeuds des ref demandees */
		      for (np=i=0;i<maillnodes.npoin;i++)
			if (maillnodes.nref[i]>0)
			  for (n=0;n<nb;n++)
			    if (maillnodes.nref[i]==ilist[n]) {np++;break;}
		      
		      if (histos->npoinref>0) 
			histos->numpr=(int*)realloc(histos->numpr,(histos->npoinref+np)*sizeof(int));
		      else     
			histos->numpr=(int*)malloc(np*sizeof(int));
		      verif_alloue_int1d("lire_histo",histos->numpr);
		      perfo.mem_cond+=nb*sizeof(int);
		      
		      for (np=i=0;i<maillnodes.npoin;i++)
			if (maillnodes.nref[i]>0)
			  for (n=0;n<nb;n++)
			    if (maillnodes.nref[i]==ilist[n]) {histos->numpr[np+histos->npoinref]=i; np++; break;}
		      histos->npoinref+=np;
		      
		    } /* fin du traitement par REFERENCE */
		
		  
	    }/* fin du mot-cle HIST */
	      
	}
      
    }/* fin du while*/
      
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

  if (histocoo) tuer_tree(arbre); 

  /* verification pour les historiques */
  /* --------------------------------- */

  if ( (histos->freq    >=0 && histos->freq_list_nb>0) ||
       (histos->freq_nt >=0 && histos->freq_list_nb>0) ||
       (histos->freq    >=0 && histos->freq_nt     >=0) )
    {
      if (SYRTHES_LANG == FR)
	printf("\n %%%% ERREUR lire_histo : Choisir une frequence OU une liste d'instants\n");
      else if (SYRTHES_LANG == EN)
	printf("\n %%%% ERROR lire_histo : You have to choose the frequency OR a list of times\n");
      printf("                       HIST= FREQ_SECONDS / HIST= FREQ_ITER /  HIST= FREQ_LIST_TIMES\n");
      err=1;
    }

  if (histos->npoin+histos->npoincoo+histos->npoinref>0 && histos->freq<=0 && histos->freq_list_nb<=0 && histos->freq_nt<=0)
    {
      if (SYRTHES_LANG == FR)
	printf("\n %%%% ERREUR lire_histo : Pour les historiques, il faut definir une frequence ou une liste d'instants\n");
      else if (SYRTHES_LANG == EN)
	printf("\n %%%% ERROR lire_histo : For probes, you have to define an output frequency or a list of times\n");
      printf("                       HIST= FREQ_SECONDS / HIST= FREQ_ITER /  HIST= FREQ_LIST_TIMES\n");
      err=1;
    }
  
  
  if (err) syrthes_exit(1);
  
  nhloc=histos->npoin+histos->npoincoo+histos->npoinref;

#ifdef _SYRTHES_MPI_
  nhtot=somme_int_parall(nhloc);
#else
  nhtot=nhloc;
#endif


  if (nhtot && (syrglob_rang==0 || syrglob_nparts==1))
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n *** HISTORIQUES TEMPORELS SUR LE SOLIDE :\n");
	  if (histos->freq>0)
	    printf("      - Frequence (s) = %f\n",histos->freq);
	  else if (histos->freq_nt>0)
	    printf("      - Frequence (iter) = %d\n",histos->freq_nt);
	  printf("      - Nombre de sondes = %d\n",nhtot);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n *** HISTORIC SOLID THERMAL PROBES :\n");
	  if (histos->freq>0)
	    printf("      - Frequency (s) = %f\n",histos->freq);
	  else if (histos->freq_nt>0)
	    printf("      - Frequency (iter) = %d\n",histos->freq_nt);
	  printf("      - Number of probes = %d\n",nhtot);
	}
      if (histos->npoin && syrglob_nparts==1)
	{
	  if (SYRTHES_LANG == FR)
	    printf("      - liste des noeuds specifies=\n          ");
	  else if (SYRTHES_LANG == EN)
	    printf("      - specified nodes list=\n          ");
	  for (j=0;j<histos->npoin/10;j++) 
	    {      
	      for (i=0;i<10;i++) printf("%d ",histos->nump[j*10+i]+1);
	      printf("\n          ");
	    }
	  for (i=(histos->npoin/10)*10;i<histos->npoin;i++) printf("%d ",histos->nump[i]+1);
	  printf("\n");
	}
      if (histos->npoincoo && syrglob_nparts)
	{
	  if (SYRTHES_LANG == FR)
	    printf("      - liste des noeuds specifies a partir des coordonnees=\n");
	  else if (SYRTHES_LANG == EN)
	    printf("      - specified nodes list (from coordinates)=\n");
	  for (j=0;j<histos->npoincoo;j++) 
	    {      
	      printf("           ( ");
	      for (i=0;i<maillnodes.ndim;i++) printf("%16.9e  ",histos->coord[i][j]);
	      printf(")\n");
	    }
	  printf("\n");
	}
      if (histos->npoinref && syrglob_nparts)
	{
	  if (SYRTHES_LANG == FR)
	    printf("      - liste des noeuds specifies a partir des references=\n          ");
	  else if (SYRTHES_LANG == EN)
	    printf("      - specified nodes list (from references)=\n          ");
	  for (j=0;j<histos->npoinref/10;j++) 
	    {      
	      for (i=0;i<10;i++) printf("%d ",histos->numpr[j*10+i]+1);
	      printf("\n          ");
	    }
	  for (i=(histos->npoinref/10)*10;i<histos->npoinref;i++) printf("%d ",histos->numpr[i]+1);
	  printf("\n");
	}
    }

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_bilan(struct Maillage maillnodes,
		struct Bilan *bilansurf,struct Bilan *bilanvol)
{
  int i,j,k;
  int i1,i2,i3,i4,id,ok=1,ii,nb,nr,n,err=0;
  int num, num1,pas,np,jmin;
  int *p;
  double val,x,y,z,dist,distmin;

  if (syrglob_nparts==1 ||syrglob_rang==0){
    if (SYRTHES_LANG == FR)       printf("\n *** LECTURE DES BILANS SURFACIQUES OU VOLUMIQUES\n");
    else if (SYRTHES_LANG == EN ) printf("\n *** READING SURFACIC OR VOLUMIC BALANCES\n");
    fflush(stdout);
  }

  fseek(fdata,0,SEEK_SET);

  bilansurf->nb=0;
  bilanvol->nb=0;


  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);

	  if (!strcmp(motcle,"BILAN FLUX SURFACIQUES")) 
	    {
	      rep_listint(ilist,&nb,ch+i2+1);
	      bilansurf->nbref[bilansurf->nb]=nb;
	      bilansurf->ref[bilansurf->nb]=(int*)malloc(nb*sizeof(int));
	      verif_alloue_int1d("lire_bilan",bilansurf->ref[bilansurf->nb]);
	      for (i=0;i<nb;i++) bilansurf->ref[bilansurf->nb][i]=ilist[i];
	      bilansurf->nb++;	  
	    }
	  else if (!strcmp(motcle,"BILAN FLUX VOLUMIQUES")) 
	    {
	      rep_listint(ilist,&nb,ch+i2+1);
	      bilanvol->nbref[bilanvol->nb]=nb;
	      bilanvol->ref[bilanvol->nb]=(int*)malloc(nb*sizeof(int));
	      verif_alloue_int1d("lire_bilan",bilanvol->ref[bilanvol->nb]);
	      for (i=0;i<nb;i++) bilanvol->ref[bilanvol->nb][i]=ilist[i];
	      bilanvol->nb++;	  
	    }


	}
    }

  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

  if (syrglob_nparts==1 ||syrglob_rang==0){
    if (SYRTHES_LANG == FR)       {printf("\n     --> Nombre de bilans surfaciques demandes : %d\n",bilansurf->nb);
                                   printf("\n     --> Nombre de bilans volumiques demandes  : %d\n",bilanvol->nb);}
    else if (SYRTHES_LANG == EN ) {printf("\n     --> Number of surfacic balances : %d\n",bilansurf->nb);
                                   printf("\n     --> Nombre of volumic balances  : %d\n",bilanvol->nb);}
    fflush(stdout);
  }

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |   Verification des proprietes physiques                              |
  |======================================================================| */
void verify_initial_cphy(struct Maillage maillnodes,struct  Prophy physol)
{
  int i,nb,ok=1,nbtrop,nbpasassez;
  int *ele;
  int refpb[MAX_REF],refpbpassez[MAX_REF],refpbtrop[MAX_REF],refpbneg[MAX_REF];


  /* controle de la masse volumique */
  /* ------------------------------ */
  for (i=0;i<MAX_REF;i++) refpb[i]=0; 

  for (nb=i=0;i<physol.nelem;i++) 
    if (physol.rho[i]<=0) {nb++; refpb[maillnodes.nrefe[i]]++;}

  if (nb){
    ok=0;
    if (SYRTHES_LANG == FR) printf(" ERREUR : la masse volumique n'a pas ete definie sur %d elements\n",nb);
    else if (SYRTHES_LANG == EN) printf(" ERROR : density not defined on %d elements\n",nb);
    for (i=0;i<MAX_REF;i++)
      if (refpb[i]>0) printf("          - reference %d  : %d elements\n",i,refpb[i]);
  }


  /* controle de la chaleur specifique */
  /* --------------------------------- */
  for (i=0;i<MAX_REF;i++) refpb[i]=0; 
 
  for (nb=i=0;i<physol.nelem;i++) 
    if (physol.cp[i]<=0)  {nb++; refpb[maillnodes.nrefe[i]]++;}

  if (nb){
    ok=0;
    if (SYRTHES_LANG == FR) printf(" ERREUR : la chaleur specifique n'a pas ete definie (ou a une valeur negative) sur %d elements\n",nb);
    else if (SYRTHES_LANG == EN) printf(" ERROR : heat capacity not defined (or has a negative value) on %d elements\n",nb);
    for (i=0;i<MAX_REF;i++)
      if (refpb[i]>0) printf("          - reference %d  : %d elements\n",i,refpb[i]);
  }


  /* controle de la conductivite */
  /* --------------------------- */
  for (i=0;i<MAX_REF;i++) {refpbtrop[i]=0; refpbpassez[i]=0; refpbneg[i]=0;}
  nb=0;

  ele=(int*)malloc(maillnodes.nelem*sizeof(int));
  verif_alloue_int1d("verify_initial_cphy",ele);
  for (i=0;i<maillnodes.nelem;i++) ele[i]=0; /* nombre de fois que la cond d'un element est definie */ 
 
  for (i=0;i<physol.kiso.nelem;i++) {
    ele[physol.kiso.ele[i]]++;   
    if (physol.kiso.k[i]<0) {nb++; refpbneg[maillnodes.nrefe[physol.kiso.ele[i]]]++;}
  }

  for (i=0;i<physol.kortho.nelem;i++) {
    ele[physol.kortho.ele[i]]++;
    if (maillnodes.ndim==2){
      if (physol.kortho.k11[i]<0 || physol.kortho.k22[i]<0) {nb++; refpbneg[maillnodes.nrefe[physol.kortho.ele[i]]]++;}
    }
    else
      if (physol.kortho.k11[i]<0 || physol.kortho.k22[i]<0 || physol.kortho.k33[i]<0) {nb++; refpbneg[maillnodes.nrefe[physol.kortho.ele[i]]]++;}
  }

  for (i=0;i<physol.kaniso.nelem;i++) {
    ele[physol.kaniso.ele[i]]++;
    if (maillnodes.ndim==2){
      if (physol.kaniso.k11[i]<0 || physol.kaniso.k22[i]<0) 
	{nb++; refpbneg[maillnodes.nrefe[physol.kaniso.ele[i]]]++;}
    }
    else
      if (physol.kaniso.k11[i]<0 || physol.kaniso.k22[i]<0 || physol.kaniso.k33[i]<0) 
	{nb++; refpbneg[maillnodes.nrefe[physol.kaniso.ele[i]]]++;}
  }


  /* y'a plus qu'a voir si tout est OK ! */
  for (nbtrop=nbpasassez=i=0;i<maillnodes.nelem;i++) 
    if (ele[i]>1) {nbtrop++; refpbtrop[maillnodes.nrefe[i]]++;}
    else if (ele[i]==0) {nbpasassez++; refpbpassez[maillnodes.nrefe[i]]++;}



  if (nbpasassez){
    ok=0;
    if (SYRTHES_LANG == FR) printf(" ERREUR : la conductivite thermique n'a pas ete definie (ou a une valeur negative) sur %d elements\n",nbpasassez);
    else if (SYRTHES_LANG == EN) printf(" ERROR : thermal conductivity not defined (or has a negative value) on %d elements\n",nbpasassez);
    for (i=0;i<MAX_REF;i++)
      if (refpbpassez[i]>0) printf("          - reference %d  : %d elements\n",i,refpbpassez[i]);
  }

  if (nbtrop){
    ok=0;
    if (SYRTHES_LANG == FR) printf(" ERREUR : la conductivite thermique est definie plusieurs fois sur %d elements\n",nbtrop);
    else if (SYRTHES_LANG == EN) printf(" ERROR : multiply definition of thermal conductivity on %d elements\n",nbtrop);
    for (i=0;i<MAX_REF;i++)
      if (refpbtrop[i]>0) printf("          - reference %d  : %d elements\n",i,refpbtrop[i]);
  }

  if (nb){
    ok=0;
    if (SYRTHES_LANG == FR) printf(" ERREUR : la conductivite thermique a une valeur negative sur %d elements\n",nb);
    else if (SYRTHES_LANG == EN) printf(" ERROR : thermal conductivity has a negative value on %d elements\n",nb);
    for (i=0;i<MAX_REF;i++)
      if (refpbneg[i]>0) printf("          - reference %d  : %d elements\n",i,refpbneg[i]);
  }

  free(ele);

  if (!ok) syrthes_exit(1);

}
