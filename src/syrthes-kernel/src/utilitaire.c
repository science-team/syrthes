/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "syr_usertype.h"
#include "syr_bd.h"
#include "syr_parall.h"
#include "syr_const.h"
#include "syr_tree.h"
#include "syr_proto.h"

extern struct Performances perfo;
extern struct Affichages affich;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | dupliq_maill                                                         |
  |          Dupliquer le maillage pour le traitement des symtries       |
  |          n2 : en prevision, on alloue des tableaux de la longueur    |
  |               de n2 fois le maillage initial                         |
  |======================================================================| */
void dupliq_maill(struct Maillage maill1,struct Maillage *maill2,int n2)
{
  int i,j;

  maill2->ndim=maill1.ndim;
  maill2->nelem=n2*maill1.nelem;
  maill2->npoin=n2*maill1.npoin;
  maill2->ndmat=maill1.ndmat;
  maill2->nbface=maill1.nbface;
  maill2->ndiele=maill1.ndiele;

  maill2->nref=NULL;
  maill2->nrefe=NULL;

  maill2->node=(int**)malloc(maill2->ndmat*sizeof(int*));
  for (i=0;i<maill2->ndmat;i++) 
    maill2->node[i]=(int*)malloc(maill2->nelem*sizeof(int));
  perfo.mem_ray+=maill2->ndmat*maill2->nelem*sizeof(int);
  verif_alloue_int2d(maill2->ndmat,"dupliq2d_sym",maill2->node);

  maill2->coord=(double**)malloc(maill2->ndim*sizeof(double*)); 
  for (i=0;i<maill2->ndim;i++) maill2->coord[i]=(double*)malloc(maill2->npoin*sizeof(double));
  perfo.mem_ray+=maill2->ndim*maill2->npoin*sizeof(int);
  verif_alloue_double2d(maill2->ndim,"dupliq2d_sym",maill2->coord);


  for (j=0;j<maill1.ndim;j++)
    for (i=0;i<maill1.npoin;i++)
      maill2->coord[j][i]=maill1.coord[j][i];


  for (j=0;j<maill1.ndmat;j++)
    for (i=0;i<maill1.nelem;i++)
      maill2->node[j][i]=maill1.node[j][i];

  if (maill1.xnf)
    {
      maill2->xnf=(double**)malloc(maill2->ndim*sizeof(double*));
      for (i=0;i<maill2->ndim;i++) maill2->xnf[i]=(double*)malloc(maill2->nelem*sizeof(double));
      verif_alloue_double2d(maill2->ndim,"dupliq2d_sym",maill2->xnf);
      perfo.mem_ray+=maill2->ndim*maill2->nelem*sizeof(double);

      for (j=0;j<maill1.ndim;j++)
	for (i=0;i<maill1.nelem;i++)
	  maill2->xnf[j][i]=maill1.xnf[j][i];
    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | impression d'un maillage avec references                             |
  |======================================================================| */
void imprime_maillage_ref(struct Maillage maillnodes)
{
  int i,j;

  printf("\n lire_donnees : Table des elements\n");
  for (i=0;i<maillnodes.nelem;i++) 
    {
      printf("\n elt=%d ref=%d type=%d noeuds: ",i,maillnodes.nrefe[i],maillnodes.type[i]);
      for (j=0;j<maillnodes.ndmat;j++) printf(" %d",maillnodes.node[j][i]);
    }
  printf("\n\n");
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | Impression d'un maillage sans references                             |
  |======================================================================| */
void imprime_maillage(struct Maillage maillnodes)
{
  int i,j,bidon=0;

  printf("\n lire_donnees : Table des noeuds\n");
  for (i=0;i<maillnodes.npoin;i++) 
    {
      printf("\n %d %d ",i+1,bidon);
      for (j=0;j<maillnodes.ndim;j++) printf(" %f",maillnodes.coord[j][i]);
    }
  printf("\n lire_donnees : Table des elements\n");
  for (i=0;i<maillnodes.nelem;i++) 
    {
      printf("\n %d %d  ",i+1,bidon);
      for (j=0;j<maillnodes.ndmat;j++) printf(" %d",maillnodes.node[j][i]+1);
    }
  printf("\n\n");
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | Impression de la connectivite                                        |
  |======================================================================| */
void imprime_connectivite(struct Maillage maillnodes)
{
  int i,j;

  printf("\n Table des elements\n");
  for (i=0;i<maillnodes.nelem;i++) 
    {
      printf("\n elt %d ref %d noeuds: ",i+1,maillnodes.nrefe[i]);
      for (j=0;j<maillnodes.ndmat;j++) printf(" %d",maillnodes.node[j][i]+1);
    }
  printf("\n\n");

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  |      impression d'une CL sur les faces                               |
  |======================================================================| */
void imprime_Clim(struct Clim cond)
{
  int i,j;


  for (i=0;i<cond.nelem;i++) 
    {
      printf("Face(loc)=%d val1=",cond.numf[i]);
      for (j=0;j<cond.ndmat;j++) printf(" %f",cond.val1[j][i]);
      if (cond.nbval==2)
	{
	  printf(" val2=");
	  for (j=0;j<cond.ndmat;j++) printf(" %f",cond.val2[j][i]);
	}
      printf("\n");
    }
  printf("\n\n");
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  |       impression d'une CL sur les noeuds                             |
  |======================================================================| */
void imprime_Climp(struct Clim cond)
{
  int i,j;

  for (i=0;i<cond.npoin;i++) 
    {
      printf(" %d noeud(glob)=%d  val1=%f \n",i,cond.nump[i],cond.val1[0][i]);
    }
  printf("\n\n");
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | Controle de l'allocation d'un tableau 1D d'entiers                   |
  |======================================================================| */
void verif_alloue_int1d(char *chaine,int *pointeur)
{
  if (!pointeur)
    {
      if (SYRTHES_LANG == FR)
	printf("\n\n ERREUR D'ALLOCATION dans la fonction %s\n\n",chaine);
      else if (SYRTHES_LANG == EN)
	printf("\n\n ALLOCATION ERROR in function %s\n\n",chaine);
      syrthes_exit(1);
    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | Controle de l'allocation d'un tableau 2D d'entiers                   |
  |======================================================================| */
void verif_alloue_int2d(int idim,char *chaine,int **pointeur)
{
  int i,err=0;
  
  if (!pointeur)
    err=1;
  else 
    for (i=0;i<idim;i++)
      if (!pointeur[i]) err=1;

  if (err)
    {
      if (SYRTHES_LANG == FR)
	printf("\n\n ERREUR D'ALLOCATION dans la fonction %s\n\n",chaine);
      else if (SYRTHES_LANG == EN)
	printf("\n\n ALLOCATION ERROR in function %s\n\n",chaine);
      syrthes_exit(1);
    }
}
    
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | Controle de l'allocation d'un tableau 1D de doubles                  |
  |======================================================================| */
void verif_alloue_double1d(char *chaine,double *pointeur)
{
  if (!pointeur)
    {
      if (SYRTHES_LANG == FR)
	printf("\n\n ERREUR D'ALLOCATION dans la fonction %s\n\n",chaine);
      else if (SYRTHES_LANG == EN)
	printf("\n\n ALLOCATION ERROR in function %s\n\n",chaine);
      syrthes_exit(1);
    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | Controle de l'allocation d'un tableau 2D de doubles                  |
  |======================================================================| */
void verif_alloue_double2d(int idim,char *chaine,double **pointeur)
{
  int i,err=0;
  
  if (!pointeur)
    err=1;
  else 
    for (i=0;i<idim;i++)
      if (!pointeur[i]) err=1;

  if (err)
    {
      if (SYRTHES_LANG == FR)
	printf("\n\n ERREUR D'ALLOCATION dans la fonction %s\n\n",chaine);
      else if (SYRTHES_LANG == EN)
	printf("\n\n ALLOCATION ERROR in function %s\n\n",chaine);
      syrthes_exit(1);
    }
}
    
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | Controle de l'allocation d'unechaine                                 |
  |======================================================================| */
void verif_alloue_char(char *chaine,char *pointeur)
{
  if (!pointeur)
    {
      if (SYRTHES_LANG == FR)
	printf("\n\n ERREUR D'ALLOCATION dans la fonction %s\n\n",chaine);
      else if (SYRTHES_LANG == EN)
	printf("\n\n ALLOCATION ERROR in function %s\n\n",chaine);
      syrthes_exit(1);
    }
}



/*----------------------------------------------------------------------------*/
/* Permutation des octets pour passage de "little endian" a "big endian"      */
/* (Y. Fournier)                                                              */
/*----------------------------------------------------------------------------*/
void fic_bin_f__endswap
(
 void  *buf,   /* Tampon contenant les elements                               */
 size_t size,  /* Taille d'un element                                         */
 size_t nitems /* Nombre d'elements                                           */
)
{
  char  tmpswap;
  char *ptr = (char *)buf;

  size_t i, j, shift;
  for (j = 0; j < nitems; j++) {
    shift = j * size;
    for (i = 0; i < (size / 2); i++) {
      tmpswap = *(ptr + shift + i);
      *(ptr + shift + i ) = *(ptr + shift + (size - 1) - i);
      *(ptr + shift + (size - 1) - i) = tmpswap;
    }
  }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | Controle du type de maillage                                         |
  |======================================================================| */
int verif_type_maillage(char *nomfich)
{
  int l;

  l=strlen(nomfich);
  if ( strncmp(nomfich+l-4,".syr",4) && strncmp(nomfich+l-5,".syrb",5))
    {
      printf("\n\nLe format du fichier <<%s>> n'est pas reconnu\n",nomfich);
      printf("Les types reconnus sont :\n");
      printf("    - .syr  = maillage SYRTHES ASCII\n\n");
      printf("    - .syrb = maillage SYRTHES binaire\n\n");
      syrthes_exit(1);
    }
  else if ( !strncmp(nomfich+l-4,".syr",4)) return 3;
  else if ( !strncmp(nomfich+l-5,".syrb",5)) return 3;

  else
    return -1;

  return -1;
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  |        Mise a 0 d'un tableau 2D                                      |
  |======================================================================| */
void azero1D(int ndim1,double *tab)
{
  int i;

  for (i=0;i<ndim1;i++) 
    tab[i]=0.;
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  |        Mise a 0 d'un tableau 2D                                      |
  |======================================================================| */
void azero2D(int ndim1, int ndim2, double **tab)
{
  int i,j;

  for (i=0;i<ndim1;i++) 
    for (j=0;j<ndim2;j++) 
      tab[i][j]=0.;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  |        Mise a une valeur d'un tableau 1D                             |
  |======================================================================| */
void avaleur1D(int ndim1,double *tab,double val)
{
  int i;

  for (i=0;i<ndim1;i++) 
    tab[i]=val;
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  |        Mise a une valeur d'un tableau 2D                             |
  |======================================================================| */
void avaleur2D(int ndim1,int ndim2,double **tab,double val)
{
  int i,j;

  for (i=0;i<ndim1;i++) 
    for (j=0;j<ndim2;j++) 
      tab[i][j]=val;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | verif_ray                                                            |
  |          verification des donnees physiques rayonnement              |
  |======================================================================| */
void verif_ray(struct Maillage maillnodray,struct ProphyRay phyray)
{
  int i,n,err_emissi,err_somm;
  double somme,eps=1.e-5;

  err_emissi=0;
  err_somm=0;

  for (i=0;i<maillnodray.nelem;i++)
    {
      for (n=0;n<phyray.bandespec.nb;n++)
	{
	  if (phyray.emissi[n][i]>1.+eps || phyray.emissi[n][i]<eps) err_emissi=1;
	  somme=phyray.absorb[n][i]+phyray.reflec[n][i]+phyray.transm[n][i];
	  if (somme<1-eps || somme>1.+eps) {
	    err_somm=1;}
	}
    }
  

  if (err_emissi) 
    printf(" %% Erreur de donnees sur l'emissivite des facettes\n");
  if (err_somm) 
    {
      printf(" %% Erreur de donnees sur les proprietes radiatives des facettes\n");
      printf("    --> verifier que l'on a partout absorptivite+reflectivite+transmittivite=1\n");
    }

  if (err_emissi || err_somm) syrthes_exit(1);
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | verif_liste_ref                                                      |
  |    verification des listes de ref donnees par l'utilisateur          |
  |======================================================================| */
void verif_liste_ref_0N(char *motcle,int nb,int *liste)
{
  int i,ok=1;
  int rmax;

  rmax=MAX_REF-1;

  for (i=0;i<nb;i++)
    if (liste[i]<0 || liste[i]>MAX_REF) ok=0;

  if (!ok)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n\n %%%% ERREUR decode_clim : les references doivent etre comprises entre 0 et %d\n",rmax);
	  printf("                         mot-cle  %s  ",motcle);
	  for (i=0;i<nb;i++) printf(" %d",liste[i]);
	  printf("\n                         --> verifier le fichier fichier de donnees\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n\n %%%% ERROR decode_clim : references have to be between 0 and %d\n",rmax);
	  printf("                         keyword  %s  ",motcle);
	  for (i=0;i<nb;i++) printf(" %d",liste[i]);
	  printf("\n                         --> Please, check the data file\n");
	}
      syrthes_exit(1);
    }
}
  
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  | Affichage des facteurs de forme                                      |
  |======================================================================| */
void affiche_fdf(int nelray,double *surface,
		 struct FacForme ***listefdf,double *diagfdf,
		 struct PropInfini propinf,struct Horizon horiz,
		 int npartsray)
{
  int i,j;
  double s;
  struct FacForme *pff;

  printf("\n *** affiche_fdf : Facteurs de forme\n");


  for (i=0;i<nelray;i++)
    {
      s=0;
      for (j=0;j<npartsray;j++)
	{
	  pff=listefdf[j][i];
	  while(pff) {s+=pff->fdf; 
	    /* pour debug */
	    /* printf("affiche_fdf : i=%d pff->fdf=%25.18e \n",i,pff->fdf);*/
	    pff=pff->suivant;}
	}
      s+=diagfdf[i];
/*       printf(" affiche_fdf : surface[%d]=%25.15e diagfdf[%d]=%25.28e \n",i,surface[i],i,diagfdf[i]); */
	  
      if (horiz.actif)
	printf("     Ligne %5d   Surface=%12.7e  FDF=%12.7e  FDFH=%12.7e  FDFC=%12.7e\n",
	       i,surface[i],s/surface[i],horiz.fdf[i]/surface[i],
	       propinf.fdfnp1[i]/surface[i]);
      else if (propinf.actif)
	printf("     Ligne %5d   Surface=%12.7e  FDF=%12.7e  FDFC=%12.7e\n",
	       i,surface[i],s/surface[i],propinf.fdfnp1[i]/surface[i]);
      else
	printf("     Ligne %5d   Surface=%12.7e  FDF=%12.7e\n",
	       i,surface[i],s/surface[i]);
    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  |   caracteristiques moyennes sur un element                           |
  |======================================================================| */
void data_element_moy(int ne,struct Maillage maillnodes,double *var,
		      int *nrefe,double *x,double *y,double *z,double *v)
{
  int n,i,nump[4];


  /* numero de materiau de l'element */ 
  *nrefe=maillnodes.nrefe[ne];    
  
  /* noeuds de l'elements */
  for (n=0; n<maillnodes.ndmat; n++) nump[n]=maillnodes.node[n][ne];
  
  /* coord barycentrique de l'element */
  for (*x=0,n=0; n<maillnodes.ndmat; n++) (*x)+=maillnodes.coord[0][nump[n]];
  for (*y=0,n=0; n<maillnodes.ndmat; n++) (*y)+=maillnodes.coord[1][nump[n]];
  *x/=maillnodes.ndmat;
  *y/=maillnodes.ndmat;
  if (maillnodes.ndim==3)
    {
      for (*z=0,n=0; n<maillnodes.ndmat; n++) (*z)+=maillnodes.coord[2][nump[n]];
      *z/=maillnodes.ndmat; 
    }
  else
    *z=0.;
  
  /* variable :  moyenne de l'element */
  for (*v=0,n=0; n<maillnodes.ndmat; n++) *v+=var[nump[n]];
  *v/=maillnodes.ndmat; 
  
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  |   caracteristiques moyennes sur un element                           |
  |======================================================================| */
double var_element_moy(int ne,struct Maillage maillnodes,double *var)
{
  int n,i;
  double vmoy;
  
  for (vmoy=0,n=0; n<maillnodes.ndmat; n++) vmoy+=var[maillnodes.node[n][ne]];
  vmoy/=maillnodes.ndmat; 
  return vmoy;
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  |   calcul le centre de gravite d'un element                           |
  |======================================================================| */
void element_center(int ne,struct Maillage maillnodes,
			 double *x,double *y,double *z)
{
  int n,nump[4];

  
  /* noeuds de l'elements */
  for (n=0; n<maillnodes.ndmat; n++) nump[n]=maillnodes.node[n][ne];
  
  /* coord barycentrique de l'element */
  for (*x=0,n=0; n<maillnodes.ndmat; n++) (*x)+=maillnodes.coord[0][nump[n]];
  for (*y=0,n=0; n<maillnodes.ndmat; n++) (*y)+=maillnodes.coord[1][nump[n]];
  *x/=maillnodes.ndmat;
  *y/=maillnodes.ndmat;
  if (maillnodes.ndim==3)
    {
      for (*z=0,n=0; n<maillnodes.ndmat; n++) (*z)+=maillnodes.coord[2][nump[n]];
      *z/=maillnodes.ndmat; 
    }
  else
    *z=0.;
  
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  |   Interpolation d'une valeur dans un tableau 1D                      |
  |                    TABLE [tabX] = tabFX                              |
  |                                                                      |
  |    x    : entree pour laquelle, il faut interpoler une valeur        |
  |           dans la table                                              |
  |   nb    : longueur de la table                                       |
  |   tabX  : liste des entrees de la table qui doivent etre ordonnees   |
  |           tabX[i] < tabX[i+1]                                        |
  |   tabFX : valeurs de la table                                        |
  |                                                                      |
  |======================================================================| */
double interpol_table1D(double x,int nb,double *tabX,double *tabFX)
{
  int ic;
  double al;

  ic=0;
  if (x<tabX[0])
    return tabFX[0];
  
  else if (x>tabX[nb-1])
    return tabFX[nb-1];
  
  else
    {
      ic=1;
      while (x>tabX[ic]) ic++;
      
      al=(x-tabX[ic-1]) / (tabX[ic]-tabX[ic-1]);
      return (1-al)*tabFX[ic-1] + al*tabFX[ic];
    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2012 |
  |======================================================================|
  | AUTEURS  :  I. RUPP, C. PENIGUEL                                     |
  |======================================================================|
  |   Interpolation d'une valeur dans un tableau 2D                      |
  |                    tabXY [nbx][nby]                                  |
  |                                                                      |
  | (  ---------> Y )                                                    |
  | ( |             )  tabXY[ ][0] valeurs des entrees de X              |
  | ( |             )  tabXY[0][ ] valeurs des entrees de Y              |
  | ( |             )                                                    |
  | ( V             )  Rq : tabXY[0][0] ne contient rien                 |
  | ( X             )       tabXY[i][j] = valeur de la fonction          |
  |                              pour x=tabXY[i][0] et y=tabXY[0][j]     |
  |                         (bien sur i>0 et j>0)                        |
  |                                                                      |
  |   On suppose que les entrees sont ordonnees :                        |
  |         tabXY[i][0]<tabXY[i+1][0] et tabXY[0][j]<tabXY[][j+1]        |
  |                                                                      |
  |   x,y     : entrees pour laquelle, il faut interpoler une valeur     |
  |             dans la table                                            |
  |   nbx,nby : longueur de la table en X (lignes) et en Y (colonnes)    |
  |   tabXY   : valeurs de la tables                                     |
  |                                                                      |
  |======================================================================| */
double interpol_table2D(double x,double y,int nbx,int nby, double **tabXY)
{
  int i,j,ibx,iby;
  double z1,z2;
  
  i=j=ibx=iby=-1;
  
  if (x <= tabXY[1][0]) {i=1;ibx=0;}     
  else if (x >= tabXY[nbx-1][0])	{i=nbx-1;ibx=0;}
  else {i=2; while (x>tabXY[i][0]) i++;}
  
  if (y <= tabXY[0][1]) {j=1;iby=0;}
  else if (y >= tabXY[0][nby-1]){j=nby-1; iby=0;}
  else {j=2; while (y>tabXY[0][j]) j++;} 
  
  if (ibx==0 && iby==0) 
    return tabXY[i][j];
  else if (ibx==0)
    {
      return (tabXY[0][j]-y)/(tabXY[0][j]-tabXY[0][j-1])*tabXY[i][j-1] + \
	(y-tabXY[0][j-1])/(tabXY[0][j]-tabXY[0][j-1])*tabXY[i][j];
    }
  else if (iby==0)
    {
      return (tabXY[i][0]-x)/(tabXY[i][0]-tabXY[i-1][0])*tabXY[i-1][j] + \
	(x-tabXY[i-1][0])/(tabXY[i][0]-tabXY[i-1][0])*tabXY[i][j];
    }
  else
    {
      z1=(tabXY[i][0]-x)/(tabXY[i][0]-tabXY[i-1][0])*tabXY[i-1][j-1] +	\
	(x-tabXY[i-1][0])/(tabXY[i][0]-tabXY[i-1][0])*tabXY[i][j-1];
      z2=(tabXY[i][0]-x)/(tabXY[i][0]-tabXY[i-1][0])*tabXY[i-1][j] +	\
	(x-tabXY[i-1][0])/(tabXY[i][0]-tabXY[i-1][0])*tabXY[i][j];
      return (tabXY[0][j]-y)/(tabXY[0][j]-tabXY[0][j-1])*z1 +	\
	(y-tabXY[0][j-1])/(tabXY[0][j]-tabXY[0][j-1])*z2;
    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | bary3d                                                               |
  | Calcul des coefficients barycentriques dans un tetraedre en 3D       |
  |======================================================================| */
void bary_tetra(double xx,double yy,double zz,
		double xa,double ya,double za,
		double xb,double yb,double zb,
		double xc,double yc,double zc,
		double xd,double yd,double zd,
		double *xl1,double *xl2,double *xl3,double *xl4)
{
  double det,xap,yap,zap,xab,yab,zab,xac,yac,zac,xad,yad,zad;
  
  xap=xx-xa; yap=yy-ya;  zap=zz-za;
  xab=xb-xa; yab=yb-ya;  zab=zb-za;
  xac=xc-xa; yac=yc-ya;  zac=zc-za;
  xad=xd-xa; yad=yd-ya;  zad=zd-za;
  
  det=determ(xab,yab,zab,xac,yac,zac,xad,yad,zad);
  
  *xl2 = determ(xap,yap,zap,xac,yac,zac,xad,yad,zad)/det;
  *xl3 = determ(xab,yab,zab,xap,yap,zap,xad,yad,zad)/det;
  *xl4 = determ(xab,yab,zab,xac,yac,zac,xap,yap,zap)/det;
  *xl1=1.-*xl2-*xl3-*xl4;
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | bary3d                                                               |
  | Calcul des coefficients barycentriques dans un triangle en 2D        |
  |======================================================================| */
void bary_tria(double xx,double yy,
	       double xa,double ya,
	       double xb,double yb,
	       double xc,double yc,
	       double *xl1,double *xl2,double *xl3)
{
  double det;
  double xap,yap,xab,yab,xac,yac;

  xap=xx-xa; yap=yy-ya;
  xab=xb-xa; yab=yb-ya;
  xac=xc-xa; yac=yc-ya;
  
  det=xab*yac-yab*xac;
  
  *xl2 = ( xap*yac - yap*xac)/det;
  *xl3 = ( xab*yap - yab*xap)/det;
  *xl1=1.-*xl2-*xl3;
}
