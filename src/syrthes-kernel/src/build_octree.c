/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <math.h>
# include <stdlib.h>
# include <string.h>

# include "syr_usertype.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_bd.h"
# include "syr_proto.h"

/* # include "mpi.h" */


extern struct Performances perfo;
extern struct Affichages affich;
extern int nelvoip;
extern int nsp;

double conso3=0;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | build_octree                                                         |
  |         Construction de l'octree surfacique                          |
  |======================================================================| */
void build_octree (struct node *arbre,int npoinr,int nelray,
		   int **nodray,double **cooray, double *size_min,double dim_boite[],
		   int nbeltmax_tree)

{
  struct child *p1;
  struct element *f1,*f2;  
  int i,nbface;
  double dx,dy,dz,dd,ddp;
  double xmin,xmax,ymin,ymax,zmin,zmax;
    
  xmin=dim_boite[0]; xmax=dim_boite[1]; 
  ymin=dim_boite[2]; ymax=dim_boite[3]; 
  zmin=dim_boite[4]; zmax=dim_boite[5]; 
  dx = xmax-xmin; dy=ymax-ymin; dz=zmax-zmin;

  strncpy(arbre->name,"A\0",2);
  arbre->xc = (xmin+xmax)*0.5;
  arbre->yc = (ymin+ymax)*0.5;
  arbre->zc = (zmin+zmax)*0.5;
  arbre->sizx = dx*0.5;    
  arbre->sizy = dy*0.5;    
  arbre->sizz = dz*0.5;    
  arbre->lelement = NULL;
  arbre->lfils = NULL;
  *size_min = min(dx,dy); *size_min = min(*size_min,dz);
  
  f1 = (struct element *)malloc(sizeof(struct element));  conso3+=sizeof(struct element);
  if (f1==NULL) 
    {
      if (SYRTHES_LANG == FR)
	printf(" ERREUR build_octree : probleme d'allocation memoire\n");
      else if (SYRTHES_LANG == EN)
	printf(" ERROR build_octree : Memory allocation problem\n");
      syrthes_exit(1);
    }
  f1->num = 0;
  f1->suivant=NULL;
  arbre->lelement=f1;
  
  for (i=1;i<nelray;i++)
    {
      f2 = (struct element *)malloc(sizeof(struct element)); conso3+=sizeof(struct element);
      if (f2==NULL) 
	{
	  if (SYRTHES_LANG == FR)
	    printf(" ERREUR build_octree : probleme d'allocation memoire\n");
	  else if (SYRTHES_LANG == EN)
	    printf(" ERROR build_octree : Memory allocation problem\n");
	  syrthes_exit(1);
	}
      f2->num = i;
      f2->suivant=NULL;
      f1->suivant = f2;
      f1 = f2;
    }
  nbface = nelray;
  
  decoupe(arbre,nodray,cooray,nelray,npoinr,nbface,size_min,nbeltmax_tree);

  if (affich.ray_mem_devel) 
    if (SYRTHES_LANG == FR)
      printf("   ---> build_octree : memoire pour la construction de l'arbre %.3f Mo\n",conso3/1.e6);
    else if (SYRTHES_LANG == EN)
      printf("   ---> build_octree : Memory used for building the octree  %.3f Mo\n",conso3/1.e6);

  perfo.mem_ray+=conso3;
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  
  elague_tree(arbre);   

/*    printf("\n\n Arbre apres elaguage\n");
    affiche_tree(arbre,8);   */
      
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | decoupe                                                              |
  |         Construction de l'octree                                     |
  |======================================================================| */
void decoupe(struct node *noeud,int **nodray,double **cooray,
            int nelray,int npoinr,int nbface,double *size_min,int nbeltmax_tree)
{
  double xmin[8],xmax[8],ymin[8],ymax[8],zmin[8],zmax[8];
  double x,y,z,dx,dy,dz ;
  int i,nbfac,nbelt_max;
  struct node *n1,*n2,*noeudi;
  struct child *f1,*f2;
  struct element *face1;
  char *c[]={"A","B","C","D","E","F","G","H"};

/*   nbelt_max=max(nelvoip*nsp+10,30); */

  if (nbface>nbeltmax_tree)
    {
      x = noeud->xc; y = noeud->yc; z = noeud->zc; 
      dx = noeud->sizx; dy = noeud->sizy; dz = noeud->sizz;
      
      xmax[0]=xmax[3]=xmax[4]=xmax[7]= x;
      xmin[1]=xmin[2]=xmin[5]=xmin[6]= x;
      xmin[0]=xmin[3]=xmin[4]=xmin[7]= x - dx;
      xmax[1]=xmax[2]=xmax[5]=xmax[6]= x + dx;

      ymax[0]=ymax[1]=ymax[2]=ymax[3]= y;
      ymin[4]=ymin[5]=ymin[6]=ymin[7]= y;
      ymin[0]=ymin[1]=ymin[2]=ymin[3]= y - dy;
      ymax[4]=ymax[5]=ymax[6]=ymax[7]= y + dy;

      zmax[2]=zmax[3]=zmax[6]=zmax[7]= z;
      zmin[0]=zmin[1]=zmin[4]=zmin[5]= z;
      zmin[2]=zmin[3]=zmin[6]=zmin[7]= z - dz;
      zmax[0]=zmax[1]=zmax[4]=zmax[5]= z + dz;


      f1= (struct child *)malloc(sizeof(struct child)); conso3+=sizeof(struct child);
      n1= (struct node *) malloc(sizeof(struct node )); conso3+=sizeof(struct node);
      if (f1==NULL || n1==NULL) 
	{
	  if (SYRTHES_LANG == FR)
	    printf(" ERREUR decoupe : probleme d'allocation memoire\n");
	  else if (SYRTHES_LANG == EN)
	    printf(" ERROR decoupe : Memory allocation problem\n");
	  syrthes_exit(1);
	}

      noeud->lfils = f1;
      strcpy(f1->name,noeud->name); strcat(f1->name,"A"); 
      f1->fils = n1;
      f1->suivant = NULL;

      for (i=1;i<8;i++)
	{
	  f2= (struct child *)malloc(sizeof(struct child)); conso3+=sizeof(struct child);
	  n2= (struct node *) malloc(sizeof(struct node )); conso3+=sizeof(struct node);
	  if (f2==NULL || n2==NULL) 
	    {
	      if (SYRTHES_LANG == FR)
		printf(" ERREUR decoupe : probleme d'allocation memoire\n");
	      else if (SYRTHES_LANG == EN)
		printf(" ERROR decoupe : Memory allocation problem\n");
	      syrthes_exit(1);
	    }
	  f1->suivant = f2;
	  strcpy(f2->name,noeud->name); strcat(f2->name,c[i]); 
	  f2->fils = n2;
	  f2->suivant = NULL;
	  f1 = f2;
	}

      f1 = noeud->lfils;
      
      for (i=0;i<8;i++)
	{
	  noeudi = f1->fils;
	  strcpy(noeudi->name,noeud->name); strcat(noeudi->name,c[i]); 
	  noeudi->xc = (xmin[i]+xmax[i])*0.5;
	  noeudi->yc = (ymin[i]+ymax[i])*0.5;
	  noeudi->zc = (zmin[i]+zmax[i])*0.5;
	  noeudi->sizx = (xmax[i]-xmin[i])*0.5;
	  noeudi->sizy = (ymax[i]-ymin[i])*0.5;
	  noeudi->sizz = (zmax[i]-zmin[i])*0.5;
	  *size_min = min(*size_min,noeudi->sizx);
	  *size_min = min(*size_min,noeudi->sizy);
	  *size_min = min(*size_min,noeudi->sizz);
	  noeudi->lfils = NULL;
	  face1= (struct element *)malloc(sizeof(struct element)); conso3+=sizeof(struct element);
	  if (face1==NULL) 
	    {
	      if (SYRTHES_LANG == FR)
		printf(" ERREUR decoupe : probleme d'allocation memoire\n");
	      else if (SYRTHES_LANG == EN)
		printf(" ERROR decoupe : Memory allocation problem\n");
	      syrthes_exit(1);
	    }

	  noeudi->lelement = face1;
	  
	  triface(noeud->lelement,noeudi->lelement,
		  &nbfac,nelray,npoinr,nodray,cooray,
		  noeudi->xc,noeudi->yc,noeudi->zc,
		  noeudi->sizx,noeudi->sizy,noeudi->sizz);
	  
	  if (nbfac != 0){
	    decoupe(noeudi,nodray,cooray,nelray,npoinr,nbfac,size_min,nbeltmax_tree);
	  }
	  else
	    {
	      noeudi->lelement = NULL;
	      free(face1);
	    }
	  
	  f1 = f1->suivant;
	}
      
    }

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | triface                                                              |
  |         Tri des faces pour les placer dans l'octree                  |
  |======================================================================| */
void triface( struct element *face_pere, struct element *face_fils, 
              int *nbfac,int nelray,int npoinr,int **nodray,double **cooray,
              double xcc,double ycc,double zcc,double dx,double dy,double dz)
{
  int i,n,prem,nret ;
  double xa,ya,za,xb,yb,zb,xc,yc,zc;
  struct element *fp1,*ff1,*ff2;

  prem = 1;
  fp1 = face_pere;
  ff1 = face_fils;
  *nbfac = 0;

  do
    {
      n=nodray[0][fp1->num];  xa=cooray[0][n];  ya=cooray[1][n]; za=cooray[2][n];
      n=nodray[1][fp1->num];  xb=cooray[0][n];  yb=cooray[1][n]; zb=cooray[2][n];
      n=nodray[2][fp1->num];  xc=cooray[0][n];  yc=cooray[1][n]; zc=cooray[2][n];

      /* test isa */
      nret=0;
      nret=tria_in_cube(xa,ya,za,xb,yb,zb,xc,yc,zc,xcc,ycc,zcc,dx,dy,dz);
      if (nret)
/*       if (tria_in_cube(xa,ya,za,xb,yb,zb,xc,yc,zc,xcc,ycc,zcc,dx,dy,dz)) */
	{
	  if (prem)
	    {
	      prem = 0;
	      ff1->num = fp1->num;
	      ff1->suivant = NULL;
	    }
	  else
	    {
	      ff2= (struct element *)malloc(sizeof(struct element)); conso3+=sizeof(struct element);
	      if (ff2==NULL) 
		{
		  if (SYRTHES_LANG == FR)
		    printf(" ERREUR triface : probleme d'allocation memoire\n");
		  else if (SYRTHES_LANG == EN)
		    printf(" ERROR triface : Memory allocation problem\n");
		  syrthes_exit(1);
		}
	      ff2->num = fp1->num;
	      ff2->suivant = NULL;
	      ff1->suivant = ff2;
	      ff1 = ff2;
	    }
	  *nbfac += 1;
	  
	}
      
      fp1 = fp1->suivant;
      
    }while (fp1 != NULL);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | tria_in_cube                                                         |
  |         Tri des faces pour les placer dans l'octree                  |
  |======================================================================| */
int tria_in_cube(double xa,double ya,double za,double xb,double yb,double zb,
		 double xc,double yc,double zc,
		 double xcc,double ycc,double zcc,double dx,double dy,double dz)
{
  double xmin,xmax,ymin,ymax,zmin,zmax;
  double ta,tb,tc,td,xab,yab,zab,xac,yac,zac,xp,yp,zp;
  double epsi,dxy,dxz,dyz,d3,t;

  epsi=1.e-5;
  dxy= dx+dy; dxz=dx+dz; dyz=dy+dz;
  d3=dxy+dz;
  
  xmin = xcc-1.05*dx; xmax = xcc+1.05*dx;
  ymin = ycc-1.05*dy; ymax = ycc+1.05*dy;
  zmin = zcc-1.05*dz; zmax = zcc+1.05*dz;
  
 
  if (in_boite (xa,ya,za,xmin,xmax,ymin,ymax,zmin,zmax))
    return(1);
  else if (in_boite (xb,yb,zb,xmin,xmax,ymin,ymax,zmin,zmax))
    return(1);
  else if (in_boite (xc,yc,zc,xmin,xmax,ymin,ymax,zmin,zmax))
    return(1);
  
  
  /* tous les points sont du meme cote d'un plan ==> pas intersection */
  else if (xa>xmax && xb>xmax && xc>xmax)
    return(0);
  else if (xa<xmin && xb<xmin && xc<xmin)
    return(0);
  else if (ya>ymax && yb>ymax && yc>ymax)
    return(0);
  else if (ya<ymin && yb<ymin && yc<ymin)
    return(0);
  else if (za>zmax && zb>zmax && zc>zmax)
    return(0);
  else if (za<zmin && zb<zmin && zc<zmin)
    return(0);
  
  else
    {
      xa -= xcc; ya -= ycc; za -= zcc;
      xb -= xcc; yb -= ycc; zb -= zcc;
      xc -= xcc; yc -= ycc; zc -= zcc;
      
      if (xa-za<-dxz && xb-zb<-dxz && xc-zc<-dxz)
	return(0);
      else if (xa-za>dxz  && xb-zb>dxz  && xc-zc>dxz)
	return(0);
      else if (xa+za<-dxz && xb+zb<-dxz && xc+zc<-dxz)
	return(0);
      else if (xa+za>dxz  && xb+zb>dxz  && xc+zc>dxz)
	return(0);
      else if (ya-za<-dyz && yb-zb<-dyz && yc-zc<-dyz)
	return(0);
      else if (ya-za>dyz  && yb-zb>dyz  && yc-zc>dyz)
	return(0);
      else if (ya+za<-dyz && yb+zb<-dyz && yc+zc<-dyz)
	return(0);
      else if (ya+za>dyz  && yb+zb>dyz  && yc+zc>dyz)
	return(0);
      else if (xa-ya<-dxy && xb-yb<-dxy && xc-yc<-dxy)
	return(0);
      else if (xa-ya>dxy  && xb-yb>dxy  && xc-yc>dxy)
	return(0);
      else if (xa+ya<-dxy && xb+yb<-dxy && xc+yc<-dxy)
	return(0);
      else if (xa+ya>dxy  && xb+yb>dxy  && xc+yc>dxy)
	return(0);
      
      
      else if ( xa+ya-za>d3 &&  xb+yb-zb>d3 &&  xc+yc-zc>d3)
	return(0);
      else if (-xa+ya-za>d3 && -xb+yb-zb>d3 && -xc+yc-zc>d3)
	return(0);
      else if (-xa+ya+za>d3 && -xb+yb+zb>d3 && -xc+yc+zc>d3)
	return(0);
      else if ( xa+ya+za>d3 &&  xb+yb+zb>d3 &&  xc+yc+zc>d3)
	return(0);
      else if ( xa-ya-za>d3 &&  xb-yb-zb>d3 &&  xc-yc-zc>d3)
	return(0);
      else if (-xa-ya-za>d3 && -xb-yb-zb>d3 && -xc-yc-zc>d3)
	return(0);
      else if (-xa-ya+za>d3 && -xb-yb+zb>d3 && -xc-yc+zc>d3)
	return(0);
      else if ( xa-ya+za>d3 &&  xb-yb+zb>d3 &&  xc-yc+zc>d3)
	return(0);
      
      
      else if (seg_cubex( dx,dy,dz,xa,xb,ya,yb,za,zb))
	return(1);
      else if (seg_cubex(-dx,dy,dz,xa,xb,ya,yb,za,zb))
	return(1);
      else if (seg_cubey(dx, dy,dz,xa,xb,ya,yb,za,zb))
	return(1);
      else if (seg_cubey(dx,-dy,dz,xa,xb,ya,yb,za,zb))
	return(1);
      else if (seg_cubez(dx,dy, dz,xa,xb,ya,yb,za,zb))
	return(1);
      else if (seg_cubez(dx,dy,-dz,xa,xb,ya,yb,za,zb))
	return(1);
      
      else if (seg_cubex( dx,dy,dz,xb,xc,yb,yc,zb,zc))
	return(1);
      else if (seg_cubex(-dx,dy,dz,xb,xc,yb,yc,zb,zc))
	return(1);
      else if (seg_cubey(dx, dy,dz,xb,xc,yb,yc,zb,zc))
	return(1);
      else if (seg_cubey(dx,-dy,dz,xb,xc,yb,yc,zb,zc))
	return(1);
      else if (seg_cubez(dx,dy, dz,xb,xc,yb,yc,zb,zc))
	return(1);
      else if (seg_cubez(dx,dy,-dz,xb,xc,yb,yc,zb,zc))
	return(1);
      
      else if (seg_cubex( dx,dy,dz,xa,xc,ya,yc,za,zc))
	return(1);
      else if (seg_cubex(-dx,dy,dz,xa,xc,ya,yc,za,zc))
	return(1);
      else if (seg_cubey(dx, dy,dz,xa,xc,ya,yc,za,zc))
	return(1);
      else if (seg_cubey(dx,-dy,dz,xa,xc,ya,yc,za,zc))
	return(1);
      else if (seg_cubez(dx,dy, dz,xa,xc,ya,yc,za,zc))
	return(1);
      else if (seg_cubez(dx,dy,-dz,xa,xc,ya,yc,za,zc))
	return(1);
      
      else
	{
	  xab=xb-xa; yab=yb-ya; zab=zb-za;
	  xac=xc-xa; yac=yc-ya; zac=zc-za;
	  ta = yab*zac-zab*yac;
	  tb = zab*xac-xab*zac;
	  tc = xab*yac-yab*xac;
	  td = -(ta*xa+tb*ya+tc*za);
	  
	  if (diag_tria(ta,tb,tc,td,dx,dy,dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xab,yab,zab,xac,yac,zac,xp,yp,zp))
		return(1);
	    }
	  else if (diag_tria(ta,tb,tc,td,dx,dy,-dz,&t))
	    {
	      xp=t*dx; yp=t*dy; zp=-t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xab,yab,zab,xac,yac,zac,xp,yp,zp))
		return(1);
	    }
	  else if (diag_tria(ta,tb,tc,td,-dx,dy,dz,&t))
	    {
	      xp=-t*dx; yp=t*dy; zp=t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xab,yab,zab,xac,yac,zac,xp,yp,zp))
		return(1);
	    }
	  else if (diag_tria(ta,tb,tc,td,-dx,dy,-dz,&t))
	    {
	      xp=-t*dx; yp=t*dy; zp=-t*dz;
	      if (in_triangle(ta,tb,tc,td,xa,ya,za,xab,yab,zab,xac,yac,zac,xp,yp,zp))
		return(1);
	    }
	  else
	    {
/* 	      if (SYRTHES_LANG == FR) */
/* 		printf(" tria_in_cube : on n'est dans aucun cas !\n"); */
/* 	      else if (SYRTHES_LANG == EN) */
/* 		printf(" tria_in_cube : not in any anticipated case !\n"); */
/* 	      printf("       triangle  A=(%f,%f,%f) B=(%f,%f,%f)  C=(%f,%f,%f)\n", */
/* 		     xa, ya, za, xb, yb, zb,xc, yc, zc); */
/* 	      printf("       cube      CC=(%f,%f,%f) DXYZ=(%f,%f,%f)\n",xcc,ycc,zcc,dx,dy,dz); */
	      return(2);
	    }
	}
    }
  return(2);
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | boite : calcul de la boite englobante                                |
  |                                                                      |
  |======================================================================| */
void boite(int npoin,double **coord,double dim_boite[])
{
  int i;
  double xmin,xmax,ymin,ymax,zmin,zmax,dx,dy,dz;

  xmin=ymin=zmin= 1.e10;
  xmax=ymax=zmax=-1.e10;
    
  for (i=0;i<npoin;i++)
    {
      xmin=min(coord[0][i],xmin);
      ymin=min(coord[1][i],ymin);
      zmin=min(coord[2][i],zmin);
      xmax=max(coord[0][i],xmax);
      ymax=max(coord[1][i],ymax);
      zmax=max(coord[2][i],zmax);
    }
  
  dx=xmax-xmin; dy=ymax-ymin; dz=zmax-zmin;
  xmin -= (dx*0.01); ymin -= (dy*0.01); zmin -= (dz*0.01);
  xmax += (dx*0.01); ymax += (dy*0.01); zmax += (dz*0.01); 
  
  
  dx=xmax-xmin; dy=ymax-ymin; dz=zmax-zmin;
  if (dx<1.e-10) xmax+=1.e-4;
  if (dy<1.e-10) ymax+=1.e-4;
  if (dz<1.e-10) zmax+=1.e-4;
  
  xmin -= (dx*0.01); ymin -= (dy*0.01); zmin -= (dz*0.01);
  xmax += (dx*0.01); ymax += (dy*0.01); zmax += (dz*0.01); 
  
  dim_boite[0]=xmin; dim_boite[1]=xmax; 
  dim_boite[2]=ymin; dim_boite[3]=ymax; 
  dim_boite[4]=zmin; dim_boite[5]=zmax; 
  
}
