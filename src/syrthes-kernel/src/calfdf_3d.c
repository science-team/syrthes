/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "syr_const.h"
# include "syr_usertype.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_option.h"
# include "syr_bd.h"
# include "syr_parall.h"
# include "syr_proto.h"

/* # include "mpi.h" */

extern struct Performances perfo;
extern struct Affichages affich;
                    


int ss_tria[4][3] = { {0,3,5},
	              {1,4,3},
	              {2,5,4},
	              {3,4,5} };
int nsp;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | facecache_3d                                                         |
  |          Algorithme de detection rapide de presence de face cachees  |
  |======================================================================| */
void facecache_3d(int nelem,int **nod,double **coord,
		  double **xnf,double *pland,int *grcon,int *faces_cachees,
		  int ligne_deb,int ligne_fin)
{

  int i,j,k,n,ns;
  int noeud[6];
  int nbfcoplanaire,code_decoupe,nbfca;
  double xi[6],yi[6],zi[6],dsign[6];
  double xn1,yn1,zn1,x,y,z,fforme;


  nbfca=0 ;
  nbfcoplanaire=0 ;

  for (i=ligne_deb ; i<ligne_fin ; i++ ) 
    {
      xn1=xnf[0][i];          yn1=xnf[1][i];        zn1=xnf[2][i];
      noeud[0]=nod[0][i];     noeud[1]=nod[1][i];   noeud[2]=nod[2][i];
      for  (j=0; j<nelem ; j++ )   
	{
	  if (i!=j  && grcon[i]==grcon[j])  /* si les facettes sont dans la meme comp connexe */
	    {
	      fforme=coplanaire_3d(xn1,yn1,zn1,xnf[0][j],xnf[1][j],xnf[2][j]);
	      if (fforme<-1.)  /* les faces ne sont pas coplanaires */
		{
		  noeud[3]=nod[0][j];   noeud[4]=nod[1][j]; noeud[5]=nod[2][j];
		  for (k=0;k<6;k++)
		    { 
		      xi[k]=coord[0][noeud[k]];
		      yi[k]=coord[1][noeud[k]];
		      zi[k]=coord[2][noeud[k]]; 
		    }
		  derriere_3d(i,j,xnf,pland,xi,yi,zi,dsign,&code_decoupe) ; 
		  if ( code_decoupe != 0 ) nbfca += 1 ;
		}
	    }
	}
    }
  

  if (nbfca != 0 ) 
    {
      *faces_cachees=1 ;
      if (syrglob_nparts==1 || syrglob_rang==0)
	if (SYRTHES_LANG == FR)
	  printf("\n *** facecache_3d : Le maillage comporte des faces cachees (nbre=%d) \n",nbfca ) ;
	else if (SYRTHES_LANG == EN)
	  printf("\n *** facecache_3d : The mesh contains hidden faces (nbre=%d) \n",nbfca ) ;
    }
  else
    {
      *faces_cachees=0 ;
      if (syrglob_nparts==1 || syrglob_rang==0)
	if (SYRTHES_LANG == FR)
	  printf("\n *** facecache_3d : Le maillage ne comporte a priori pas de faces cachees \n" ) ;
	else if (SYRTHES_LANG == EN)
	  printf("\n *** facecache_3d : A priori, the mesh does not contain hidden faces \n" ) ;
    }

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | facforme_3d                                                          |
  |           calcul des facteurs de forme en dimension 3                |
  | Rq : ici listefdf est le tableau des liste des fdf pour la           |
  |      composante en cours                                             |
  |======================================================================| */
void facforme_3d(int nelem0,int npoin,int nelem,int **nod,double **coord,
		 double *pland,double **xnf,
		 struct FacForme ***listefdf,double *diagfdf,int *grcon,
		 int faces_cachees,struct Mask mask,
		 struct Horizon *horiz,int *typf,
		 struct SDparall_ray sdparall_ray)
{
  int i,j,k,l,npoin2,ik,il,id,ideb,nt,nbtour,nelemloc,np,idecal;
  int noeud[6],prem,ndecoup,iloc;
  int *voir,intersect,arrivee;
  int codem10,code0,code1,codem1,code2,codem2,code3,code4,code5,codem5,codem6;
  int nbfcoplanaire,code_decoupe;
  double xi[6],yi[6],zi[6],fforme,xp[6],yp[6],zp[6],xq[6],yq[6],zq[6];
  double xii[6],yii[6],zii[6],xt[3],yt[3],zt[3];
  double xn1,yn1,zn1,x,y,z,size_min,dim_boite[6],dsign[6],xn2,yn2,zn2;
  double tiers;
  struct node *arbre;
  double xg1,yg1,zg1,xg2,yg2,zg2;
  double titi, titi1, titi2;
  int ncomplique,pasok;
  double total_fac,valmask;
  struct FacForme *pff,*ppff,*qff;
  int nbffcree=0;
  double *lignefdf,*fdfH;
  int *existefdf;
  int ligne_deb,ligne_fin;
  int nbeltmax_tree=30;
  int centre=1;
  float pourcent;
  int okaff; 
  int ncompt_rej=0;
  int ncompt_rsto=0;
  double eps_rsto=10e-10;
  char pct='%';

  /* Initialisations 
     --------------- */

  fdfH=horiz->fdf;
  
  tiers=1./3.;
  nbfcoplanaire=ncomplique=0 ;
  codem10=code0=code1=codem1=code2=codem2=code3=code4=code5=codem5=codem6=0;

  if (sdparall_ray.npartsray>1)    {
    ligne_deb=sdparall_ray.ieledeb[sdparall_ray.rang];
    ligne_fin=sdparall_ray.ieledeb[sdparall_ray.rang+1];
    nelemloc=sdparall_ray.nelrayloc[sdparall_ray.rang];
  }
  else {
    ligne_deb=0;
    ligne_fin=nelem0;
    nelemloc=nelem0;
  }

  lignefdf=(double*)malloc(nelem0*sizeof(double));
  existefdf=(int*)malloc(nelem0*sizeof(int));


  if (faces_cachees)
    { 
      if (!centre)
	{
	  voir = (int *)malloc((npoin+1)*npoin/2*(sizeof(int))); 
	  if (voir==NULL) 
	    {
	      if (SYRTHES_LANG == FR)
		printf(" ERREUR facforme_3d : probleme d'allocation memoire\n");
	      else if (SYRTHES_LANG == EN)
		printf(" ERROR facforme_3d : Memory allocation problem\n");
	      
	      syrthes_exit(1);
	    }
	  for (i=0 ; i<npoin*(npoin+1)/2 ; i++ ) *(voir+i) = -2 ; 
	  for (i=0 ; i<npoin ; i++) voir[i*npoin+i*(1-i)/2]=-1; 
	}
      boite(npoin,coord,dim_boite);
      size_min=1.E8;
      arbre= (struct node *)malloc(sizeof(struct node));
      if (arbre==NULL) 
	{
	  if (SYRTHES_LANG == FR)
	    printf(" ERREUR facforme_3d : probleme d'allocation memoire\n");
	  else if (SYRTHES_LANG == EN)
	    printf(" ERROR facforme_3d : Memory allocation problem\n");
	  syrthes_exit(1);
	}
      build_octree(arbre,npoin,nelem,nod,coord,&size_min,dim_boite,nbeltmax_tree);
    }
  
  titi1 =0;
  titi2 =0;

  nbtour=nelem/nelem0;

  if (sdparall_ray.rangray==0)
    if (SYRTHES_LANG == FR)
      printf(" *** facforme_3d :   calcul des fateurs de forme \n");
    else if (SYRTHES_LANG == EN)
      printf(" *** facforme_3d :   view factors calculation \n");

  okaff=0;

  /* ici c'etait de 0 a nelem0 */

  for (i=ligne_deb;i<ligne_fin;i++) 
 /*-------------------------------*/
    {
      iloc=i-ligne_deb; /* numero local de la ligne */

      if(affich.ray_fdf) 
	if (SYRTHES_LANG == FR)
	  printf(" *** facforme_3d :   facette i=%d \n",i);
	else if (SYRTHES_LANG == EN)
	  printf(" *** facforme_3d :   face i=%d \n",i);

      pourcent=(float)i/(float)(ligne_fin-ligne_deb+1);
      if (sdparall_ray.rangray==0)
	if (okaff==2 && pourcent > 0.75)
	  {printf("           75 %c OK...\n",pct); okaff=3;}
	else if (okaff==1 && pourcent > 0.50)
	  {printf("           50 %c OK...\n",pct);okaff=2;}
	else if (okaff==0 && pourcent > 0.25)
	  {printf("           25 %c OK...\n",pct);okaff=1;}
      
      for (k=0;k<nelem0;k++) {lignefdf[k]=0; existefdf[k]=0;}  /* on met les fdf de la ligne a 0 */ 

      xn1=xnf[0][i]; yn1=xnf[1][i]; zn1=xnf[2][i];
      noeud[0]=nod[0][i]; noeud[1]=nod[1][i]; noeud[2]=nod[2][i];
      
      for  (j=0;j<nelem;j++)
     /*------------------*/
	{
	  if (grcon[i]!=grcon[j]) {continue;}

	  xn2=xnf[0][j]; yn2=xnf[1][j]; zn2=xnf[2][j];
	  fforme=coplanaire_3d(xn1,yn1,zn1,xn2,yn2,zn2);
	  if (fforme>-0.1) {nbfcoplanaire++; pasok=1;}
	  if (fforme<-1.)  
	    {
	      noeud[3]=nod[0][j]; noeud[4]=nod[1][j]; noeud[5]=nod[2][j];
	      for (k=0;k<6;k++)
		{ 
		  xi[k]=coord[0][noeud[k]];
		  yi[k]=coord[1][noeud[k]];
		  zi[k]=coord[2][noeud[k]]; 
		}
	      fforme=0.; pasok=0;
	      valmask=1;
	      if (!faces_cachees && mask.nelem) 
		{
		  valmask=cal_mask_3d(xi,yi,coord,mask);
		  if (SYRTHES_LANG == FR)
		    printf(">>> facforme3d : facette %d et %d on applique un masque de %f\n",i,j,valmask);
		  else if (SYRTHES_LANG == EN)
		    printf(">>> facforme3d : face %d and %d on applies a mask of %f\n",i,j,valmask);
		}
	      
	      if (!faces_cachees)	 
		  fforme=contou3d(xi,yi,zi);

	      else
		{
		  derriere_3d(i,j,xnf,pland,xi,yi,zi,dsign,&code_decoupe);  
		  if (code_decoupe!=-10 && mask.nelem) 
		    {
		      valmask=cal_mask_3d(xi,yi,coord,mask);
		      if (SYRTHES_LANG == FR)
			printf(">>> facforme3d : facette %d et %d on applique un masque de %f\n",i,j,valmask);
		      else if (SYRTHES_LANG == EN)
			printf(">>> facforme3d : face %d and %d on applies a mask of %f\n",i,j,valmask);
		    }

		  prem=1; fforme=0; ndecoup=0;
		  if (code_decoupe==0)  prem=1;
		  else prem=0;
		  
		  if (code_decoupe==-10)    /* les faces sont derrieres */  
		    {codem10++; fforme=0; pasok=1;} 
		  else if (code_decoupe==0)       /* les faces se voient potentiellement */  
		    { 
		      code0 += 1;
		      if (bary_cache_3d(xi,yi,zi,xn1,yn1,zn1))
			{ fforme=0; pasok=1;}
		      else
			{
			  pasok=rejectfdf_3d (xn1,yn1,zn1,xn2,yn2,zn2,xi,yi,zi);
			  if (pasok)
			    {
			      fforme=0.;
			      ncompt_rej+=1;
			    }
			  else
			    {
			      xg1=(xi[0]+xi[1]+xi[2])*tiers; yg1=(yi[0]+yi[1]+yi[2])*tiers; zg1=(zi[0]+zi[1]+zi[2])*tiers;
			      xg2=(xi[3]+xi[4]+xi[5])*tiers; yg2=(yi[3]+yi[4]+yi[5])*tiers; zg2=(zi[3]+zi[4]+zi[5])*tiers;
			      xii[0]=xi[0]+(xg1-xi[0])*0.001; yii[0]=yi[0]+(yg1-yi[0])*0.001; zii[0]=zi[0]+(zg1-zi[0])*0.001;
			      xii[1]=xi[1]+(xg1-xi[1])*0.001; yii[1]=yi[1]+(yg1-yi[1])*0.001; zii[1]=zi[1]+(zg1-zi[1])*0.001;
			      xii[2]=xi[2]+(xg1-xi[2])*0.001; yii[2]=yi[2]+(yg1-yi[2])*0.001; zii[2]=zi[2]+(zg1-zi[2])*0.001;
			      xii[3]=xi[3]+(xg2-xi[3])*0.001; yii[3]=yi[3]+(yg2-yi[3])*0.001; zii[3]=zi[3]+(zg2-zi[3])*0.001;
			      xii[4]=xi[4]+(xg2-xi[4])*0.001; yii[4]=yi[4]+(yg2-yi[4])*0.001; zii[4]=zi[4]+(zg2-zi[4])*0.001;
			      xii[5]=xi[5]+(xg2-xi[5])*0.001; yii[5]=yi[5]+(yg2-yi[5])*0.001; zii[5]=zi[5]+(zg2-zi[5])*0.001;
			      if (centre)
				triafdfC(arbre,size_min,dim_boite,&prem,xi,yi,zi,
					 noeud,npoin,nod,coord,&ndecoup,&fforme); 
			      else
				triafdf(arbre,size_min,dim_boite,&prem,xi,yi,zi,xii,yii,zii,
					noeud,voir,npoin,nod,coord,&ndecoup,&fforme,&ncomplique); 
			      if (fforme < eps_rsto)
				{fforme=0.; pasok=1;  ncompt_rsto+=1;}
			    }
			}
		    } 
		  else if (fabs(code_decoupe)==1)  /* intersection To-Td ou Td-To */
		    { 
		      if (code_decoupe==1) code1++;
		      else codem1++;
		      decoupe_totd(i,j,xnf,pland,xi,yi,zi,dsign,code_decoupe);
		      
		      if (code_decoupe==1)
			pasok=bary_cache_3d(xi,yi,zi,xn1,yn1,zn1);
		      else
			pasok=bary_cache_3d(xi,yi,zi,xn2,yn2,zn2);
		      
		      if(pasok)
			fforme=0;
		      else
			{
			  pasok=rejectfdf_3d (xn1,yn1,zn1,xn2,yn2,zn2,xi,yi,zi);
			  if (pasok)
			    {
			      fforme=0.;
			      ncompt_rej+=1;
			    }
			  else
			    {
			      xg1=(xi[0]+xi[1]+xi[2])*tiers; yg1=(yi[0]+yi[1]+yi[2])*tiers; zg1=(zi[0]+zi[1]+zi[2])*tiers;
			      xg2=(xi[3]+xi[4]+xi[5])*tiers; yg2=(yi[3]+yi[4]+yi[5])*tiers; zg2=(zi[3]+zi[4]+zi[5])*tiers;
			      xii[0]=xi[0]+(xg1-xi[0])*0.001; yii[0]=yi[0]+(yg1-yi[0])*0.001; zii[0]=zi[0]+(zg1-zi[0])*0.001;
			      xii[1]=xi[1]+(xg1-xi[1])*0.001; yii[1]=yi[1]+(yg1-yi[1])*0.001; zii[1]=zi[1]+(zg1-zi[1])*0.001;
			      xii[2]=xi[2]+(xg1-xi[2])*0.001; yii[2]=yi[2]+(yg1-yi[2])*0.001; zii[2]=zi[2]+(zg1-zi[2])*0.001;
			      xii[3]=xi[3]+(xg2-xi[3])*0.001; yii[3]=yi[3]+(yg2-yi[3])*0.001; zii[3]=zi[3]+(zg2-zi[3])*0.001;
			      xii[4]=xi[4]+(xg2-xi[4])*0.001; yii[4]=yi[4]+(yg2-yi[4])*0.001; zii[4]=zi[4]+(zg2-zi[4])*0.001;
			      xii[5]=xi[5]+(xg2-xi[5])*0.001; yii[5]=yi[5]+(yg2-yi[5])*0.001; zii[5]=zi[5]+(zg2-zi[5])*0.001;
			      if (centre)
				triafdfC(arbre,size_min,dim_boite,&prem,xi,yi,zi,
					 noeud,npoin,nod,coord,&ndecoup,&fforme); 
			      else
				triafdf(arbre,size_min,dim_boite,&prem,xi,yi,zi,xii,yii,zii,
					noeud,voir,npoin,nod,coord,&ndecoup,&fforme,&ncomplique); 
			      if (fforme < eps_rsto)
				{fforme=0.; pasok=1;  ncompt_rsto+=1;}
			    }
			}
		    } 
		  
		  else if (fabs(code_decoupe)==2 ) /* intersection To-Qd ou Qd-To */ 
		    { 
		      if (code_decoupe==2) code2   += 1;
		      else codem2  += 1;
		      decoupe_toqd(i,j,xnf,pland,xi,yi,zi,xp,yp,zp,dsign,code_decoupe);
		      xi[3]=xp[0]; yi[3]=yp[0]; zi[3]=zp[0];
		      xi[4]=xp[1]; yi[4]=yp[1]; zi[4]=zp[1];
		      xi[5]=xp[2]; yi[5]=yp[2]; zi[5]=zp[2];
		      
		      if (code_decoupe==2)
			pasok=bary_cache_3d(xi,yi,zi,xn1,yn1,zn1);
		      else
			pasok=bary_cache_3d(xi,yi,zi,xn2,yn2,zn2);
		      
		      if (pasok)
			fforme=0;
		      else
			{
			  pasok=rejectfdf_3d (xn1,yn1,zn1,xn2,yn2,zn2,xi,yi,zi);
			  if (pasok)
			    {
			      fforme=0.;
			      ncompt_rej+=1;
			    }
			  else
			    {
			      xg1=(xi[0]+xi[1]+xi[2])*tiers; yg1=(yi[0]+yi[1]+yi[2])*tiers; zg1=(zi[0]+zi[1]+zi[2])*tiers;
			      xg2=(xi[3]+xi[4]+xi[5])*tiers; yg2=(yi[3]+yi[4]+yi[5])*tiers; zg2=(zi[3]+zi[4]+zi[5])*tiers;
			      xii[0]=xi[0]+(xg1-xi[0])*0.001; yii[0]=yi[0]+(yg1-yi[0])*0.001; zii[0]=zi[0]+(zg1-zi[0])*0.001;
			      xii[1]=xi[1]+(xg1-xi[1])*0.001; yii[1]=yi[1]+(yg1-yi[1])*0.001; zii[1]=zi[1]+(zg1-zi[1])*0.001;
			      xii[2]=xi[2]+(xg1-xi[2])*0.001; yii[2]=yi[2]+(yg1-yi[2])*0.001; zii[2]=zi[2]+(zg1-zi[2])*0.001;
			      xii[3]=xi[3]+(xg2-xi[3])*0.001; yii[3]=yi[3]+(yg2-yi[3])*0.001; zii[3]=zi[3]+(zg2-zi[3])*0.001;
			      xii[4]=xi[4]+(xg2-xi[4])*0.001; yii[4]=yi[4]+(yg2-yi[4])*0.001; zii[4]=zi[4]+(zg2-zi[4])*0.001;
			      xii[5]=xi[5]+(xg2-xi[5])*0.001; yii[5]=yi[5]+(yg2-yi[5])*0.001; zii[5]=zi[5]+(zg2-zi[5])*0.001;
			      if (centre)
				triafdfC(arbre,size_min,dim_boite,&prem,xi,yi,zi,
					 noeud,npoin,nod,coord,&ndecoup,&fforme); 
			      else
				triafdf(arbre,size_min,dim_boite,&prem,xi,yi,zi,xii,yii,zii,
					noeud,voir,npoin,nod,coord,&ndecoup,&fforme,&ncomplique); 
			      if (fforme < eps_rsto)
				{fforme=0.; pasok=1;  ncompt_rsto+=1;}
			    }
			}
		      
		      if (code_decoupe==2)  id=0;
		      else id=3;
		      for (k=0;k<3;k++)  /* on recharche les xi originaux */
			{ 
			  xi[k]=coord[0][noeud[k+id]];
			  yi[k]=coord[1][noeud[k+id]];
			  zi[k]=coord[2][noeud[k+id]]; 
			}
		      xi[3]=xp[0]; yi[3]=yp[0]; zi[3]=zp[0];
		      xi[4]=xp[2]; yi[4]=yp[2]; zi[4]=zp[2];
		      xi[5]=xp[3]; yi[5]=yp[3]; zi[5]=zp[3];
		      
		      if (code_decoupe==2)
			pasok=bary_cache_3d(xi,yi,zi,xn1,yn1,zn1);
		      else
			pasok=bary_cache_3d(xi,yi,zi,xn2,yn2,zn2);
		      
		      if (pasok)
			fforme=0;
		      else
			{
			  pasok=rejectfdf_3d (xn1,yn1,zn1,xn2,yn2,zn2,xi,yi,zi);
			  if (pasok)
			    {
			      fforme=0.;
			      ncompt_rej+=1;
			    }
			  else
			    {
			      xg1=(xi[0]+xi[1]+xi[2])*tiers; yg1=(yi[0]+yi[1]+yi[2])*tiers; zg1=(zi[0]+zi[1]+zi[2])*tiers;
			      xg2=(xi[3]+xi[4]+xi[5])*tiers; yg2=(yi[3]+yi[4]+yi[5])*tiers; zg2=(zi[3]+zi[4]+zi[5])*tiers;
			      xii[0]=xi[0]+(xg1-xi[0])*0.001; yii[0]=yi[0]+(yg1-yi[0])*0.001; zii[0]=zi[0]+(zg1-zi[0])*0.001;
			      xii[1]=xi[1]+(xg1-xi[1])*0.001; yii[1]=yi[1]+(yg1-yi[1])*0.001; zii[1]=zi[1]+(zg1-zi[1])*0.001;
			      xii[2]=xi[2]+(xg1-xi[2])*0.001; yii[2]=yi[2]+(yg1-yi[2])*0.001; zii[2]=zi[2]+(zg1-zi[2])*0.001;
			      xii[3]=xi[3]+(xg2-xi[3])*0.001; yii[3]=yi[3]+(yg2-yi[3])*0.001; zii[3]=zi[3]+(zg2-zi[3])*0.001;
			      xii[4]=xi[4]+(xg2-xi[4])*0.001; yii[4]=yi[4]+(yg2-yi[4])*0.001; zii[4]=zi[4]+(zg2-zi[4])*0.001;
			      xii[5]=xi[5]+(xg2-xi[5])*0.001; yii[5]=yi[5]+(yg2-yi[5])*0.001; zii[5]=zi[5]+(zg2-zi[5])*0.001;
			      if (centre)
				triafdfC(arbre,size_min,dim_boite,&prem,xi,yi,zi,
					 noeud,npoin,nod,coord,&ndecoup,&fforme); 
			      else
				triafdf(arbre,size_min,dim_boite,&prem,xi,yi,zi,xii,yii,zii,
					noeud,voir,npoin,nod,coord,&ndecoup,&fforme,&ncomplique); 
			      if (fforme < eps_rsto)
				{fforme=0.; pasok=1;  ncompt_rsto+=1;}
			    }
			}
		    } 
		  
		  else if (code_decoupe==3)    /* intersection Td-Td */ 
		    { 
		      code3++;
		      decoupe_tdtd(i,j,xnf,pland,xi,yi,zi,dsign,code_decoupe);
		      if(bary_cache_3d(xi,yi,zi,xn1,yn1,zn1))
			{ fforme=0; pasok=1;}
		      else
			{
			  pasok=rejectfdf_3d (xn1,yn1,zn1,xn2,yn2,zn2,xi,yi,zi);
			    if (pasok)
			      {
				fforme=0.;
				ncompt_rej+=1;
			      }
			    else
			      {
				xg1=(xi[0]+xi[1]+xi[2])*tiers; yg1=(yi[0]+yi[1]+yi[2])*tiers; zg1=(zi[0]+zi[1]+zi[2])*tiers;
				xg2=(xi[3]+xi[4]+xi[5])*tiers; yg2=(yi[3]+yi[4]+yi[5])*tiers; zg2=(zi[3]+zi[4]+zi[5])*tiers;
				xii[0]=xi[0]+(xg1-xi[0])*0.001; yii[0]=yi[0]+(yg1-yi[0])*0.001; zii[0]=zi[0]+(zg1-zi[0])*0.001;
				xii[1]=xi[1]+(xg1-xi[1])*0.001; yii[1]=yi[1]+(yg1-yi[1])*0.001; zii[1]=zi[1]+(zg1-zi[1])*0.001;
				xii[2]=xi[2]+(xg1-xi[2])*0.001; yii[2]=yi[2]+(yg1-yi[2])*0.001; zii[2]=zi[2]+(zg1-zi[2])*0.001;
				xii[3]=xi[3]+(xg2-xi[3])*0.001; yii[3]=yi[3]+(yg2-yi[3])*0.001; zii[3]=zi[3]+(zg2-zi[3])*0.001;
				xii[4]=xi[4]+(xg2-xi[4])*0.001; yii[4]=yi[4]+(yg2-yi[4])*0.001; zii[4]=zi[4]+(zg2-zi[4])*0.001;
				xii[5]=xi[5]+(xg2-xi[5])*0.001; yii[5]=yi[5]+(yg2-yi[5])*0.001; zii[5]=zi[5]+(zg2-zi[5])*0.001;
				if (centre)
				  triafdfC(arbre,size_min,dim_boite,&prem,xi,yi,zi,
					   noeud,npoin,nod,coord,&ndecoup,&fforme); 
				else
				  triafdf(arbre,size_min,dim_boite,&prem,xi,yi,zi,xii,yii,zii,
					  noeud,voir,npoin,nod,coord,&ndecoup,&fforme,&ncomplique);
			      if (fforme < eps_rsto)
				{fforme=0.; pasok=1;  ncompt_rsto+=1;}
			      }
			}
		    } 
		  else if (code_decoupe==4)   /* intersection Qd-Qd */ 
		    { 
		      code4++;
		      decoupe_qdqd(i,j,xnf,pland,xi,yi,zi,xp,yp,zp,xq,yq,zq,dsign,code_decoupe);
		      for (k=0;k<2;k++)
			{ 
			  xi[0]=xp[0];    yi[0]=yp[0];    zi[0]=zp[0];
			  xi[1]=xp[1+k];  yi[1]=yp[1+k];  zi[1]=zp[1+k];
			  xi[2]=xp[2+k];  yi[2]=yp[2+k];  zi[2]=zp[2+k];
			  for (l=0;l<2;l++)
			    {
			      xi[3]=xq[0];    yi[3]=yq[0];    zi[3]=zq[0];
			      xi[4]=xq[1+l];  yi[4]=yq[1+l];  zi[4]=zq[1+l];
			      xi[5]=xq[2+l];  yi[5]=yq[2+l];  zi[5]=zq[2+l];
			      if(bary_cache_3d(xi,yi,zi,xn1,yn1,zn1))
				{fforme=0; pasok=1;}
			      else
				{
				  pasok=rejectfdf_3d (xn1,yn1,zn1,xn2,yn2,zn2,xi,yi,zi);
				  if (pasok)
				    {
				      fforme=0.;
				      ncompt_rej+=1;
				    }
				  else
				    {
				      pasok=rejectfdf_3d (xn1,yn1,zn1,xn2,yn2,zn2,xi,yi,zi);
					if (pasok)
					  {
					    fforme=0.;
					    ncompt_rej+=1;
					  }
					else
					  {
					    xg1=(xi[0]+xi[1]+xi[2])*tiers; yg1=(yi[0]+yi[1]+yi[2])*tiers; zg1=(zi[0]+zi[1]+zi[2])*tiers;
					    xg2=(xi[3]+xi[4]+xi[5])*tiers; yg2=(yi[3]+yi[4]+yi[5])*tiers; zg2=(zi[3]+zi[4]+zi[5])*tiers;
					    xii[0]=xi[0]+(xg1-xi[0])*0.001; yii[0]=yi[0]+(yg1-yi[0])*0.001; zii[0]=zi[0]+(zg1-zi[0])*0.001;
					    xii[1]=xi[1]+(xg1-xi[1])*0.001; yii[1]=yi[1]+(yg1-yi[1])*0.001; zii[1]=zi[1]+(zg1-zi[1])*0.001;
					    xii[2]=xi[2]+(xg1-xi[2])*0.001; yii[2]=yi[2]+(yg1-yi[2])*0.001; zii[2]=zi[2]+(zg1-zi[2])*0.001;
					    xii[3]=xi[3]+(xg2-xi[3])*0.001; yii[3]=yi[3]+(yg2-yi[3])*0.001; zii[3]=zi[3]+(zg2-zi[3])*0.001;
					    xii[4]=xi[4]+(xg2-xi[4])*0.001; yii[4]=yi[4]+(yg2-yi[4])*0.001; zii[4]=zi[4]+(zg2-zi[4])*0.001;
					    xii[5]=xi[5]+(xg2-xi[5])*0.001; yii[5]=yi[5]+(yg2-yi[5])*0.001; zii[5]=zi[5]+(zg2-zi[5])*0.001;
					    if (centre)
					      triafdfC(arbre,size_min,dim_boite,&prem,xi,yi,zi,
						       noeud,npoin,nod,coord,&ndecoup,&fforme);
					    else 
					      triafdf(arbre,size_min,dim_boite,&prem,xi,yi,zi,xii,yii,zii,
						      noeud,voir,npoin,nod,coord,&ndecoup,&fforme,&ncomplique); 
					    if (fforme < eps_rsto)
					      {fforme=0.; pasok=1;  ncompt_rsto+=1;}
					  }
				    }
				}
			    }
			}
		    }
		  
		  else if (fabs(code_decoupe)==5 )   /* intersection Td-Qd */ 
		    { 
		      if (code_decoupe==5) code5 += 1;
		      else codem5++;
		      decoupe_tdqd(i,j,xnf,pland,xi,yi,zi,xt,yt,zt,xp,yp,zp,dsign,code_decoupe);
		      xi[0]=xt[0]; yi[0]=yt[0]; zi[0]=zt[0];
		      xi[1]=xt[1]; yi[1]=yt[1]; zi[1]=zt[1];
		      xi[2]=xt[2]; yi[2]=yt[2]; zi[2]=zt[2];
		      xi[3]=xp[0]; yi[3]=yp[0]; zi[3]=zp[0];
		      xi[4]=xp[1]; yi[4]=yp[1]; zi[4]=zp[1];
		      xi[5]=xp[2]; yi[5]=yp[2]; zi[5]=zp[2];
		      
		      if  (code_decoupe==5)
			pasok=bary_cache_3d(xi,yi,zi,xn1,yn1,zn1);
		      else
			pasok=bary_cache_3d(xi,yi,zi,xn2,yn2,zn2);
		      
		      if (pasok)
			fforme=0;
		      else
			{
			  pasok=rejectfdf_3d (xn1,yn1,zn1,xn2,yn2,zn2,xi,yi,zi);
			  if (pasok)
			    {
			      fforme=0.;
			      ncompt_rej+=1;
			    }
			  else
			    {
			      xg1=(xi[0]+xi[1]+xi[2])*tiers; yg1=(yi[0]+yi[1]+yi[2])*tiers; zg1=(zi[0]+zi[1]+zi[2])*tiers;
			      xg2=(xi[3]+xi[4]+xi[5])*tiers; yg2=(yi[3]+yi[4]+yi[5])*tiers; zg2=(zi[3]+zi[4]+zi[5])*tiers;
			      xii[0]=xi[0]+(xg1-xi[0])*0.001; yii[0]=yi[0]+(yg1-yi[0])*0.001; zii[0]=zi[0]+(zg1-zi[0])*0.001;
			      xii[1]=xi[1]+(xg1-xi[1])*0.001; yii[1]=yi[1]+(yg1-yi[1])*0.001; zii[1]=zi[1]+(zg1-zi[1])*0.001;
			      xii[2]=xi[2]+(xg1-xi[2])*0.001; yii[2]=yi[2]+(yg1-yi[2])*0.001; zii[2]=zi[2]+(zg1-zi[2])*0.001;
			      xii[3]=xi[3]+(xg2-xi[3])*0.001; yii[3]=yi[3]+(yg2-yi[3])*0.001; zii[3]=zi[3]+(zg2-zi[3])*0.001;
			      xii[4]=xi[4]+(xg2-xi[4])*0.001; yii[4]=yi[4]+(yg2-yi[4])*0.001; zii[4]=zi[4]+(zg2-zi[4])*0.001;
			      xii[5]=xi[5]+(xg2-xi[5])*0.001; yii[5]=yi[5]+(yg2-yi[5])*0.001; zii[5]=zi[5]+(zg2-zi[5])*0.001;
			      if (centre)
				triafdfC(arbre,size_min,dim_boite,&prem,xi,yi,zi,
					 noeud,npoin,nod,coord,&ndecoup,&fforme);
			      else 
				triafdf(arbre,size_min,dim_boite,&prem,xi,yi,zi,xii,yii,zii,
					noeud,voir,npoin,nod,coord,&ndecoup,&fforme,&ncomplique);
			      if (fforme < eps_rsto)
				{fforme=0.; pasok=1;  ncompt_rsto+=1;}
			    }
			}
		      
		      xi[0]=xt[0]; yi[0]=yt[0]; zi[0]=zt[0];
		      xi[1]=xt[1]; yi[1]=yt[1]; zi[1]=zt[1];
		      xi[2]=xt[2]; yi[2]=yt[2]; zi[2]=zt[2];
		      xi[3]=xp[0]; yi[3]=yp[0]; zi[3]=zp[0];
		      xi[4]=xp[2]; yi[4]=yp[2]; zi[4]=zp[2];
		      xi[5]=xp[3]; yi[5]=yp[3]; zi[5]=zp[3];
		      
		      if  (code_decoupe==5)
			pasok=bary_cache_3d(xi,yi,zi,xn1,yn1,zn1);
		      else
			pasok=bary_cache_3d(xi,yi,zi,xn2,yn2,zn2);
		      
		      if (pasok)
			fforme=0;
		      else
			{
			  pasok=rejectfdf_3d (xn1,yn1,zn1,xn2,yn2,zn2,xi,yi,zi);
			  if (pasok)
			    {
			      fforme=0.;
			      ncompt_rej+=1;
			    }
			  else
			    {
			      xg1=(xi[0]+xi[1]+xi[2])*tiers; yg1=(yi[0]+yi[1]+yi[2])*tiers; zg1=(zi[0]+zi[1]+zi[2])*tiers;
			      xg2=(xi[3]+xi[4]+xi[5])*tiers; yg2=(yi[3]+yi[4]+yi[5])*tiers; zg2=(zi[3]+zi[4]+zi[5])*tiers;
			      xii[0]=xi[0]+(xg1-xi[0])*0.001; yii[0]=yi[0]+(yg1-yi[0])*0.001; zii[0]=zi[0]+(zg1-zi[0])*0.001;
			      xii[1]=xi[1]+(xg1-xi[1])*0.001; yii[1]=yi[1]+(yg1-yi[1])*0.001; zii[1]=zi[1]+(zg1-zi[1])*0.001;
			      xii[2]=xi[2]+(xg1-xi[2])*0.001; yii[2]=yi[2]+(yg1-yi[2])*0.001; zii[2]=zi[2]+(zg1-zi[2])*0.001;
			      xii[3]=xi[3]+(xg2-xi[3])*0.001; yii[3]=yi[3]+(yg2-yi[3])*0.001; zii[3]=zi[3]+(zg2-zi[3])*0.001;
			      xii[4]=xi[4]+(xg2-xi[4])*0.001; yii[4]=yi[4]+(yg2-yi[4])*0.001; zii[4]=zi[4]+(zg2-zi[4])*0.001;
			      xii[5]=xi[5]+(xg2-xi[5])*0.001; yii[5]=yi[5]+(yg2-yi[5])*0.001; zii[5]=zi[5]+(zg2-zi[5])*0.001;
			      if (centre)
				triafdfC(arbre,size_min,dim_boite,&prem,xi,yi,zi,
					 noeud,npoin,nod,coord,&ndecoup,&fforme); 
			      else
				triafdf(arbre,size_min,dim_boite,&prem,xi,yi,zi,xii,yii,zii,
					noeud,voir,npoin,nod,coord,&ndecoup,&fforme,&ncomplique); 
			      if (fforme < eps_rsto)
				{fforme=0.; pasok=1;  ncompt_rsto+=1;}
			    }
			}
		    }
		  
		  
		  else if ( code_decoupe==-6 ) 
		    { 
		      codem6++; fforme=0; pasok=1;
		    }
		  
		} /* else faces_cachees */
	      
	    } /* if (fforme<-1.)  les faces ne sont pas coplanaires  */
	  
/* 	  if (i==92) */
/* 	    {printf("j=%d fdf=%f\n",j,fforme*1e6); syrthes_exit;} */

	  if(fforme < 0.) 
	    {
	      if (affich.ray_fdf) 
		if (SYRTHES_LANG == FR)
		  printf(" facforme3d : facteur de forme negatif i %d j %d  fdf= %f\n",i+1,j+1,1e6*fforme);
		else if (SYRTHES_LANG == EN)
		  printf(" facforme3d : negative view factor  i %d j %d  fdf= %f\n",i+1,j+1,1e6*fforme);
	    }
	  else  if (!horiz->nelem && pasok==0)  
	    {lignefdf[j%nelem0] += fforme*valmask;  existefdf[j%nelem0]+=1;}
	  else if (horiz->nelem)
	    if (typf[i]!=20 && typf[j]!=20) 
	      {if (pasok==0) {lignefdf[j%nelem0] += fforme*valmask; existefdf[j%nelem0]+=1;}}
	    else if (typf[i]==20 && typf[j]!=20) 
	      {fdfH[j]+= fforme*valmask;printf("iJ %d %d  fdfH=%f\n",i,j,fforme*1e6);} 
	    else if (typf[i]!=20 && typf[j]==20) 
	      {fdfH[i]+= fforme*valmask;printf("Ij %d %d  fdfH=%f\n",i,j,fforme*1e6);} 
       
	}  /* fin boucle sur j */

/*       printf(" face_i=%d  ncompt_rsto=%d \n",i,ncompt_rsto); */
/*       printf(" face_i=%d  ncompt_rej=%d \n",i,ncompt_rej); */
      

      /* reste a compacter la ligne calculee dans la listefdf et creer la diagonale */
      for (j=0;j<nelem0;j++)
	{
	  if (existefdf[j])  /* s'il y a un fdf entre les facettes i et j a stocker */
	    {
	      if (j==i)  /* on est sur la diagonale */
		{
		  diagfdf[j-ligne_deb]=lignefdf[j];
		}
	      else
		{
		  /* sur quel proc est j ? */
		  idecal=np=0;
		  if (sdparall_ray.npartsray>1)
		    {
		      while (j>=sdparall_ray.ieledeb[np+1]) np++;
		      idecal=sdparall_ray.ieledeb[np]; 
		    }
		  qff=(struct FacForme*)malloc(sizeof(struct FacForme));
		  qff->numenface=j-idecal; 
		  qff->suivant=NULL;
		  qff->fdf=lignefdf[j];
		  nbffcree++;
		  
		  if (!listefdf[np][iloc])
		    listefdf[np][iloc]=qff;
		  else
		    {
		      pff=listefdf[np][iloc];
		      while(pff) {ppff=pff; pff=pff->suivant;}
		      ppff->suivant=qff;
		    }
		}
	    }
	}

/*       if (SYRTHES_LANG == FR) */
/* 	printf("facforme3d : ligne %d : on a cree %d fdf\n",i,nbffcree); */
/*       else if (SYRTHES_LANG == EN) */
/* 	printf("facforme3d : line %d : one has stored %d fdf\n",i,nbffcree); */


    } /* for i */
  
  if (faces_cachees || mask.nelem)  tuer_tree(arbre);
  if (faces_cachees && centre==0) free(voir);

  free(lignefdf); free(existefdf);

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | dupliq3d_sym                                                         |
  |          Dupliquer le maillage pour le traitement des symtries       |
  |======================================================================| */
void dupliq3d_sym(struct SymPer defsymper,int n2,
		  struct Maillage maillnodray,struct Maillage *maillnod2)
{
  int i,j,n,i1,i2,ii1,ii2,idebnel,idebnp,numsym,inverse;
  double a,b,c,d,e,t[4][4],t1[4][4],t2[4][4];
  
  /* On duplique noeuds, coordonnees et normales */
  dupliq_maill(maillnodray,maillnod2,n2);


  /* Creation des symetries */
  /* ---------------------- */

  numsym=0;

  for (i=0;i<defsymper.nbsym;i++)
    {
      a=defsymper.sym[i][0];
      b=defsymper.sym[i][1];
      c=defsymper.sym[i][2];
      d=defsymper.sym[i][3];
      e=-2./(a*a+b*b+c*c);
      t[0][0]=1.+ a*a*e;
      t[1][1]=1.+ b*b*e;
      t[2][2]=1.+ c*c*e;
      t[0][1]=t[1][0]=a*b*e;
      t[0][2]=t[2][0]=a*c*e;
      t[1][2]=t[2][1]=b*c*e;
      t[0][3]=a*d*e; t[1][3]=b*d*e; t[2][3]=c*d*e;
      t[3][0]=t[3][1]=t[3][2]=0; t[3][3]=1;
      
      for (j=i;j<defsymper.nbsym;j++)
	{
	  inverse=-1;
	  if (i!=j)
	    {
	      inverse=1;
	      a=defsymper.sym[j][0];
	      b=defsymper.sym[j][1];
	      c=defsymper.sym[j][2];
	      d=defsymper.sym[j][3];
	      e=-2./(a*a+b*b+c*c);
	      t1[0][0]=1.+ a*a*e;
	      t1[1][1]=1.+ b*b*e;
	      t1[2][2]=1.+ c*c*e;
	      t1[0][1]=t1[1][0]=a*b*e;
	      t1[0][2]=t1[2][0]=a*c*e;
	      t1[1][2]=t1[2][1]=b*c*e;
	      t1[0][3]=a*d*e; t1[1][3]=b*d*e; t1[2][3]=c*d*e;
	      t1[3][0]=t1[3][1]=t1[3][2]=0; t1[3][3]=1;
	      
	      t2[0][0]= t[0][0]*t1[0][0]+t[0][1]*t1[1][0]+t[0][2]*t1[2][0]+t[0][3]*t1[3][0];
	      t2[0][1]= t[0][0]*t1[0][1]+t[0][1]*t1[1][1]+t[0][2]*t1[2][1]+t[0][3]*t1[3][1];
	      t2[0][2]= t[0][0]*t1[0][2]+t[0][1]*t1[1][2]+t[0][2]*t1[2][2]+t[0][3]*t1[3][2];
	      t2[0][3]= t[0][0]*t1[0][3]+t[0][1]*t1[1][3]+t[0][2]*t1[2][3]+t[0][3]*t1[3][3];
	      
	      t2[1][1]= t[1][0]*t1[0][1]+t[1][1]*t1[1][1]+t[1][2]*t1[2][1]+t[1][3]*t1[3][1];
	      t2[1][2]= t[1][0]*t1[0][2]+t[1][1]*t1[1][2]+t[1][2]*t1[2][2]+t[1][3]*t1[3][2];
	      t2[1][3]= t[1][0]*t1[0][3]+t[1][1]*t1[1][3]+t[1][2]*t1[2][3]+t[1][3]*t1[3][3];
	      
	      t2[2][2]= t[2][0]*t1[0][2]+t[2][1]*t1[1][2]+t[2][2]*t1[2][2]+t[2][3]*t1[3][2];
	      t2[2][3]= t[2][0]*t1[0][3]+t[2][1]*t1[1][3]+t[2][2]*t1[2][3]+t[2][3]*t1[3][3];
	      
	      t2[1][0]=t2[0][1];
	      t2[2][0]=t2[0][2]; t2[2][1]=t2[1][2];
	    }
	  else
	    for (i1=0;i1<4;i1++)
	      for (i2=0;i2<4;i2++)
		t2[i1][i2]=t[i1][i2];
	  
	  numsym +=1;
	  idebnel=numsym*maillnodray.nelem;	
	  idebnp =numsym*maillnodray.npoin;  
	  sym3d(t2,numsym,inverse,maillnodray,*maillnod2,idebnel,idebnp);
	}
    }
  

  if (defsymper.nbsym==3)
    {
      inverse=-1;
      i=0;
      a=defsymper.sym[i][0];
      b=defsymper.sym[i][1];
      c=defsymper.sym[i][2];
      d=defsymper.sym[i][3];
      e=-2./(a*a+b*b+c*c);
      t[0][0]=1.+ a*a*e;
      t[1][1]=1.+ b*b*e;
      t[2][2]=1.+ c*c*e;
      t[0][1]=t[1][0]=a*b*e;
      t[0][2]=t[2][0]=a*c*e;
      t[1][2]=t[2][1]=b*c*e;
      t[0][3]=a*d*e; t[1][3]=b*d*e; t[2][3]=c*d*e;
      t[3][0]=t[3][1]=t[3][2]=0; t[3][3]=1;
      
      j=1;
      a=defsymper.sym[j][0];
      b=defsymper.sym[j][1];
      c=defsymper.sym[j][2];
      d=defsymper.sym[j][3];
      e=-2./(a*a+b*b+c*c);
      t1[0][0]=1.+ a*a*e;
      t1[1][1]=1.+ b*b*e;
      t1[2][2]=1.+ c*c*e;
      t1[0][1]=t1[1][0]=a*b*e;
      t1[0][2]=t1[2][0]=a*c*e;
      t1[1][2]=t1[2][1]=b*c*e;
      t1[0][3]=a*d*e; t1[1][3]=b*d*e; t1[2][3]=c*d*e;
      t1[3][0]=t1[3][1]=t1[3][2]=0; t1[3][3]=1;
      
      t2[0][0]= t[0][0]*t1[0][0]+t[0][1]*t1[1][0]+t[0][2]*t1[2][0]+t[0][3]*t1[3][0];
      t2[0][1]= t[0][0]*t1[0][1]+t[0][1]*t1[1][1]+t[0][2]*t1[2][1]+t[0][3]*t1[3][1];
      t2[0][2]= t[0][0]*t1[0][2]+t[0][1]*t1[1][2]+t[0][2]*t1[2][2]+t[0][3]*t1[3][2];
      t2[0][3]= t[0][0]*t1[0][3]+t[0][1]*t1[1][3]+t[0][2]*t1[2][3]+t[0][3]*t1[3][3];
      
      t2[1][1]= t[1][0]*t1[0][1]+t[1][1]*t1[1][1]+t[1][2]*t1[2][1]+t[1][3]*t1[3][1];
      t2[1][2]= t[1][0]*t1[0][2]+t[1][1]*t1[1][2]+t[1][2]*t1[2][2]+t[1][3]*t1[3][2];
      t2[1][3]= t[1][0]*t1[0][3]+t[1][1]*t1[1][3]+t[1][2]*t1[2][3]+t[1][3]*t1[3][3];
      
      t2[2][2]= t[2][0]*t1[0][2]+t[2][1]*t1[1][2]+t[2][2]*t1[2][2]+t[2][3]*t1[3][2];
      t2[2][3]= t[2][0]*t1[0][3]+t[2][1]*t1[1][3]+t[2][2]*t1[2][3]+t[2][3]*t1[3][3];
      
      t2[1][0]=t2[0][1];
      t2[2][0]=t2[0][2]; t2[2][1]=t2[1][2];
      
      j=2;
      a=defsymper.sym[j][0];
      b=defsymper.sym[j][1];
      c=defsymper.sym[j][2];
      d=defsymper.sym[j][3];
      e=-2./(a*a+b*b+c*c);
      t1[0][0]=1.+ a*a*e;
      t1[1][1]=1.+ b*b*e;
      t1[2][2]=1.+ c*c*e;
      t1[0][1]=t1[1][0]=a*b*e;
      t1[0][2]=t1[2][0]=a*c*e;
      t1[1][2]=t1[2][1]=b*c*e;
      t1[0][3]=a*d*e; t1[1][3]=b*d*e; t1[2][3]=c*d*e;
      t1[3][0]=t1[3][1]=t1[3][2]=0; t1[3][3]=1;
      
      t[0][0]= t2[0][0]*t1[0][0]+t2[0][1]*t1[1][0]+t2[0][2]*t1[2][0]+t2[0][3]*t1[3][0];
      t[0][1]= t2[0][0]*t1[0][1]+t2[0][1]*t1[1][1]+t2[0][2]*t1[2][1]+t2[0][3]*t1[3][1];
      t[0][2]= t2[0][0]*t1[0][2]+t2[0][1]*t1[1][2]+t2[0][2]*t1[2][2]+t2[0][3]*t1[3][2];
      t[0][3]= t2[0][0]*t1[0][3]+t2[0][1]*t1[1][3]+t2[0][2]*t1[2][3]+t2[0][3]*t1[3][3];
      
      t[1][1]= t2[1][0]*t1[0][1]+t2[1][1]*t1[1][1]+t2[1][2]*t1[2][1]+t2[1][3]*t1[3][1];
      t[1][2]= t2[1][0]*t1[0][2]+t2[1][1]*t1[1][2]+t2[1][2]*t1[2][2]+t2[1][3]*t1[3][2];
      t[1][3]= t2[1][0]*t1[0][3]+t2[1][1]*t1[1][3]+t2[1][2]*t1[2][3]+t2[1][3]*t1[3][3];
      
      t[2][2]= t2[2][0]*t1[0][2]+t2[2][1]*t1[1][2]+t2[2][2]*t1[2][2]+t2[2][3]*t1[3][2];
      t[2][3]= t2[2][0]*t1[0][3]+t2[2][1]*t1[1][3]+t2[2][2]*t1[2][3]+t2[2][3]*t1[3][3];
      
      t[1][0]=t[0][1];
      t[2][0]=t[0][2]; t[2][1]=t[1][2];
      
      numsym +=1;
      idebnel=numsym*maillnodray.nelem;	
      idebnp =numsym*maillnodray.npoin;  
      sym3d(t2,numsym,inverse,maillnodray,*maillnod2,idebnel,idebnp);
    }
    
  /* impressions de controle */
  /* imprime_maillage(*maillnod2); */
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | sym3d                                                                |
  |          Calculer le symetrique d'un maillage                        |
  |======================================================================| */
void sym3d(double t[4][4],int numsym,int inverse,
	   struct Maillage maillnodray,struct Maillage maillnod2,
	   int idebnel,int idebnp)
{
  int n,nv;
  double x,y,z;
      
  for (n=0;n<maillnodray.nelem;n++)
    {
      x=maillnodray.xnf[0][n];  y=maillnodray.xnf[1][n]; z=maillnodray.xnf[2][n];
      maillnod2.xnf[0][idebnel+n]= t[0][0]*x+t[0][1]*y+t[0][2]*z;
      maillnod2.xnf[1][idebnel+n]= t[1][0]*x+t[1][1]*y+t[1][2]*z;
      maillnod2.xnf[2][idebnel+n]= t[2][0]*x+t[2][1]*y+t[2][2]*z;
    }

  for (n=0;n<maillnodray.npoin;n++)
    {	
      x=maillnodray.coord[0][n]; y=maillnodray.coord[1][n]; z=maillnodray.coord[2][n];
      maillnod2.coord[0][idebnp+n]= t[0][0]*x+t[0][1]*y+t[0][2]*z + t[0][3]; 
      maillnod2.coord[1][idebnp+n]= t[1][0]*x+t[1][1]*y+t[1][2]*z + t[1][3]; 
      maillnod2.coord[2][idebnp+n]= t[2][0]*x+t[2][1]*y+t[2][2]*z + t[2][3];
    } 

  if (inverse==-1)
    for (n=0;n<maillnodray.nelem;n++)
      {
        maillnod2.node[0][idebnel+n] = maillnodray.node[0][n] + idebnp;
        maillnod2.node[1][idebnel+n] = maillnodray.node[2][n] + idebnp;
        maillnod2.node[2][idebnel+n] = maillnodray.node[1][n] + idebnp;
      }
  else
    for (n=0;n<maillnodray.nelem;n++)
      {
        maillnod2.node[0][idebnel+n] = maillnodray.node[0][n] + idebnp;
        maillnod2.node[1][idebnel+n] = maillnodray.node[1][n] + idebnp;
        maillnod2.node[2][idebnel+n] = maillnodray.node[2][n] + idebnp;
      }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | indvoir                                                              |
  |           calcul d'adresse dans le tableau voir (facforme_3d)        |
  |======================================================================| */
int indvoir(int i,int j, int n)
{
     return(i*n-i*(i+1)/2+j); 
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | surface_tria                                                         |
  |           calcul de la surface des triangles                         |
  |======================================================================| */
void surface_tria(struct Maillage maill)
{
  int i,*n0,*n1,*n2,**nod;
  double x01,y01,z01,x02,y02,z02,**coo;
  
  nod=maill.node;
  coo=maill.coord;

  for (i=0,n0=*nod,n1=*(nod+1),n2=*(nod+2);i<maill.nelem;i++,n0++,n1++,n2++)   
    {
      x01=coo[0][*n1]-coo[0][*n0];  x02=coo[0][*n2]-coo[0][*n0];  
      y01=coo[1][*n1]-coo[1][*n0];  y02=coo[1][*n2]-coo[1][*n0];
      z01=coo[2][*n1]-coo[2][*n0];  z02=coo[2][*n2]-coo[2][*n0];
      maill.volume[i]=0.5 * sqrt ((x01*y02-y01*x02)*(x01*y02-y01*x02)
			      + (y01*z02-z01*y02)*(y01*z02-z01*y02)
			      + (x01*z02-z01*x02)*(x01*z02-z01*x02) );
    }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | coplanaire_3d                                                        |
  |           retourne 0 si les faces sont coplanaires, -1000 sinon      |
  |======================================================================| */
double coplanaire_3d (double xn1,double yn1,double zn1,
		      double xn2,double yn2,double zn2)
{
  double dnx,dny,dnz,xn,epscop;
  
  epscop=5.e-3;
  dnx=xn1 - xn2;
  dny=yn1 - yn2;
  dnz=zn1 - zn2;
  
  xn=sqrt(dnx*dnx + dny*dny + dnz*dnz);
  
  if (xn<epscop) 
    return(0);
  else if ( fabs(dnx)<epscop && fabs(dny)<epscop && fabs(dnz)<epscop)
    return(0);
  else
    return(-1000);
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | triafdf                                                              |
  |        organisation du calcul du facteur de forme entre 2 triangles  |
  |======================================================================| */
void triafdf (struct node *arbre,double size_min,double dim_boite[],int *prem,
	      double xi[],double yi[],double zi[],double xii[],double yii[],double zii[],
	      int noeud[],int voir[],int npoin,int **nod, double **coord,
	      int *ndecoup,double *fforme,int *ncomplique)
 
{
  int i,ok,k,l,ik,il,ii,intersect,arrivee,nd;
  double xp[6],yp[6],zp[6],xq[6],yq[6],zq[6],ro[3],rd[3],pt_arr[3],fdf,epsff;
  double xg1,yg1,zg1,xg2,yg2,zg2,tiers;
  int vu[3][3],dans_prem;
  struct node *noeud_dep,*noeud_arr;



  epsff=1.E-10;
  tiers=1./3.;
  ok=0; 
  i=-1;
  dans_prem=0;
  
  for (k=0;k<3;k++)
    for (l=0;l<3;l++)
      vu[k][l]=-1;


  if (*prem)
    {
      *prem=0;
      dans_prem=1;
      for (k=0;k<3;k++)
	for (l=3;l<6;l++)
	  {
	    ik=noeud[k];  il=noeud[l];
	    if (il<ik) {ii=ik; ik=il; il=ii;}
	    
	    if (voir[indvoir(ik,il,npoin)]<-1)
	      {
		intersect=-1;  
		ro[0]=xi[k]; ro[1]=yi[k]; ro[2]=zi[k];
		pt_arr[0]=xi[l];pt_arr[1]=yi[l]; pt_arr[2]=zi[l];
		rd[0]=pt_arr[0]-ro[0];
		rd[1]=pt_arr[1]-ro[1];
		rd[2]=pt_arr[2]-ro[2];
		noeud_dep=arbre; noeud_arr=arbre;
		find_node_3d(&noeud_dep,ro[0],ro[1],ro[2]);
		find_node_3d(&noeud_arr,pt_arr[0],pt_arr[1],pt_arr[2]);
		arrivee=0;
		ivoitj_3d(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
			  &intersect,size_min,nod,coord,&arrivee,dim_boite);
		voir[indvoir(ik,il,npoin)]=intersect;
		vu[k][l-3]= intersect;
	      }
	    else
	      {
		intersect=voir[indvoir(ik,il,npoin)];
	      }
	    
	    if (intersect==-1) ok += 1;
	    /*	      printf(">> facforme_3d : k l intersect %d %d %d\n",ik+1,il+1,intersect);  */
	  }
    }
  
    
  else
    
    for (k=0;k<3;k++)
      for (l=3;l<6;l++) 
	{
	  intersect=-1;  
	  ro[0]=xii[k]; ro[1]=yii[k]; ro[2]=zii[k];
	  pt_arr[0]=xii[l];pt_arr[1]=yii[l]; pt_arr[2]=zii[l];
	  rd[0]=pt_arr[0]-ro[0];
	  rd[1]=pt_arr[1]-ro[1];
	  rd[2]=pt_arr[2]-ro[2];
	  noeud_dep=arbre; noeud_arr=arbre;
	  find_node_3d (&noeud_dep,ro[0],ro[1],ro[2]);
	  find_node_3d (&noeud_arr,pt_arr[0],pt_arr[1],pt_arr[2]);
	  arrivee=0;
	  
	  /*	    printf(" \n noeud depart %d noeud arrivee %d\n",noeud_dep->name,noeud_arr->name); */
	  ivoitj_3d(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
		    &intersect,size_min,nod,coord,&arrivee,dim_boite);
	  /*	    printf(">> facforme_3d (2): k l intersect %d %d %d\n",ik+1,il+1,intersect);*/
	    if (intersect==-1) ok += 1;
	}
	  




  if ( 0 < ok && ok < 9 && dans_prem) 
    {
      *ncomplique += 1;
      ok=0;
      for (k=0;k<3;k++)
	for (l=3;l<6;l++) 
	  if (vu[k][l-3]==-1)
	    {
	      ro[0]=xii[k]; ro[1]=yii[k]; ro[2]=zii[k];
	      pt_arr[0]=xii[l];pt_arr[1]=yii[l]; pt_arr[2]=zii[l];
	      rd[0]=pt_arr[0]-ro[0];
	      rd[1]=pt_arr[1]-ro[1];
	      rd[2]=pt_arr[2]-ro[2];
	      noeud_dep=arbre; noeud_arr=arbre;
	      find_node_3d (&noeud_dep,ro[0],ro[1],ro[2]);
	      find_node_3d (&noeud_arr,pt_arr[0],pt_arr[1],pt_arr[2]);
	      arrivee=0;
	      
	      /*	    printf(" \n noeud depart %d noeud arrivee %d\n",noeud_dep->name,noeud_arr->name); */
	      ivoitj_3d(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
			&intersect,size_min,nod,coord,&arrivee,dim_boite);
	      /*	    printf(">> facforme_3d (2): k l intersect %d %d %d\n",ik+1,il+1,intersect);*/
	      if (intersect==-1) ok += 1;
	    }
    }
  
  
  /*   printf(">> facforme_3d : ok final =%d\n",ok);  */
  if (ok!=0)
    {
      fdf=contou3d(xi,yi,zi); 
      /* printf(">> facforme_3d : estimation fdf =%f, ok=%d\n",fdf*1.E6,ok); */
      
      
      if (ok==9)
	{*fforme += fdf;  }
      
      /*	else if (*ndecoup>=ndecoup_max || fdf<epsff) */
      else if (*ndecoup>=ndecoup_max)
	{  *fforme=*fforme + (fdf*ok/9.); 
	   /* if (ok>4) *fforme=*fforme + fdf; */
	 }
      
      else
	{
	  nd=*ndecoup+1;
	  xp[0]=xi[0]; yp[0]=yi[0]; zp[0]=zi[0];
	  xp[1]=xi[1]; yp[1]=yi[1]; zp[1]=zi[1];
	  xp[2]=xi[2]; yp[2]=yi[2]; zp[2]=zi[2];
	  xp[3]=(xi[0]+xi[1])/2.; yp[3]=(yi[0]+yi[1])/2.;zp[3]=(zi[0]+zi[1])/2.; 
	  xp[4]=(xi[2]+xi[1])/2.; yp[4]=(yi[2]+yi[1])/2.;zp[4]=(zi[2]+zi[1])/2.; 
	  xp[5]=(xi[0]+xi[2])/2.; yp[5]=(yi[0]+yi[2])/2.;zp[5]=(zi[0]+zi[2])/2.; 
	  
	  xq[0]=xi[3]; yq[0]=yi[3]; zq[0]=zi[3];
	  xq[1]=xi[4]; yq[1]=yi[4]; zq[1]=zi[4];
	  xq[2]=xi[5]; yq[2]=yi[5]; zq[2]=zi[5];
	  xq[3]=(xi[3]+xi[4])/2.; yq[3]=(yi[3]+yi[4])/2.;zq[3]=(zi[3]+zi[4])/2.; 
	  xq[4]=(xi[5]+xi[4])/2.; yq[4]=(yi[5]+yi[4])/2.;zq[4]=(zi[5]+zi[4])/2.; 
	  xq[5]=(xi[3]+xi[5])/2.; yq[5]=(yi[3]+yi[5])/2.;zq[5]=(zi[3]+zi[5])/2.; 
	  
	  for (k=0;k<4;k++)
	    for (l=0;l<4;l++)
	      {
		xi[0]=xp[ss_tria[k][0]]; yi[0]=yp[ss_tria[k][0]]; zi[0]=zp[ss_tria[k][0]];
		xi[1]=xp[ss_tria[k][1]]; yi[1]=yp[ss_tria[k][1]]; zi[1]=zp[ss_tria[k][1]];
		xi[2]=xp[ss_tria[k][2]]; yi[2]=yp[ss_tria[k][2]]; zi[2]=zp[ss_tria[k][2]];
		xi[3]=xq[ss_tria[l][0]]; yi[3]=yq[ss_tria[l][0]]; zi[3]=zq[ss_tria[l][0]];
		xi[4]=xq[ss_tria[l][1]]; yi[4]=yq[ss_tria[l][1]]; zi[4]=zq[ss_tria[l][1]];
		xi[5]=xq[ss_tria[l][2]]; yi[5]=yq[ss_tria[l][2]]; zi[5]=zq[ss_tria[l][2]];
		
		xg1=(xi[0]+xi[1]+xi[2])*tiers; yg1=(yi[0]+yi[1]+yi[2])*tiers; zg1=(zi[0]+zi[1]+zi[2])*tiers;
		xg2=(xi[3]+xi[4]+xi[5])*tiers; yg2=(yi[3]+yi[4]+yi[5])*tiers; zg2=(zi[3]+zi[4]+zi[5])*tiers;
		xii[0]=xi[0]+(xg1-xi[0])*0.001; yii[0]=yi[0]+(yg1-yi[0])*0.001; zii[0]=zi[0]+(zg1-zi[0])*0.001;
		xii[1]=xi[1]+(xg1-xi[1])*0.001; yii[1]=yi[1]+(yg1-yi[1])*0.001; zii[1]=zi[1]+(zg1-zi[1])*0.001;
		xii[2]=xi[2]+(xg1-xi[2])*0.001; yii[2]=yi[2]+(yg1-yi[2])*0.001; zii[2]=zi[2]+(zg1-zi[2])*0.001;
		xii[3]=xi[3]+(xg2-xi[3])*0.001; yii[3]=yi[3]+(yg2-yi[3])*0.001; zii[3]=zi[3]+(zg2-zi[3])*0.001;
		xii[4]=xi[4]+(xg2-xi[4])*0.001; yii[4]=yi[4]+(yg2-yi[4])*0.001; zii[4]=zi[4]+(zg2-zi[4])*0.001;
		xii[5]=xi[5]+(xg2-xi[5])*0.001; yii[5]=yi[5]+(yg2-yi[5])*0.001; zii[5]=zi[5]+(zg2-zi[5])*0.001;
		
		triafdf(arbre,size_min,dim_boite,prem,xi,yi,zi,xii,yii,zii,
			noeud,voir,npoin,nod,coord,&nd,fforme,ncomplique); 
	      }
	  *ndecoup++;
	}
    }        
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | triafdf                                                              |
  |        organisation du calcul du facteur de forme entre 2 triangles  |
  |======================================================================| */
void triafdfC (struct node *arbre,double size_min,double dim_boite[],int *prem,
	      double xi[],double yi[],double zi[],
	      int noeud[],int npoin,int **nod, double **coord,
	      int *ndecoup,double *fforme)
 
{
  int i,ok,k,l,ik,il,ii,intersect,arrivee,nd;
  double xp[6],yp[6],zp[6],xq[6],yq[6],zq[6],ro[3],rd[3],pt_arr[3],fdf,epsff;
  double xg1,yg1,zg1,xg2,yg2,zg2,tiers;
  int vu[3][3],dans_prem;
  struct node *noeud_dep,*noeud_arr;


  tiers=1./3.;

  if (*ndecoup==ndecoup_max)
    {
      ro[0]=tiers*(xi[0]+xi[1]+xi[2]);     ro[1]=tiers*(yi[0]+yi[1]+yi[2]);     ro[2]=tiers*(zi[0]+zi[1]+zi[2]);
      pt_arr[0]=tiers*(xi[3]+xi[4]+xi[5]); pt_arr[1]=tiers*(yi[3]+yi[4]+yi[5]); pt_arr[2]=tiers*(zi[3]+zi[4]+zi[5]);
      rd[0]=pt_arr[0]-ro[0];rd[1]=pt_arr[1]-ro[1];rd[2]=pt_arr[2]-ro[2];
      noeud_dep=arbre; noeud_arr=arbre;
      find_node_3d(&noeud_dep,ro[0],ro[1],ro[2]);
      find_node_3d(&noeud_arr,pt_arr[0],pt_arr[1],pt_arr[2]);
      arrivee=0; intersect=-1;
      ivoitj_3d(arbre,noeud_dep,noeud_arr,ro,rd,pt_arr,
		&intersect,size_min,nod,coord,&arrivee,dim_boite);

      if (intersect==-1) *fforme+=contou3d(xi,yi,zi); 
    }
      
  else
    {

      xp[0]=xi[0]; yp[0]=yi[0]; zp[0]=zi[0];
      xp[1]=xi[1]; yp[1]=yi[1]; zp[1]=zi[1];
      xp[2]=xi[2]; yp[2]=yi[2]; zp[2]=zi[2];
      xp[3]=(xi[0]+xi[1])/2.; yp[3]=(yi[0]+yi[1])/2.;zp[3]=(zi[0]+zi[1])/2.; 
      xp[4]=(xi[2]+xi[1])/2.; yp[4]=(yi[2]+yi[1])/2.;zp[4]=(zi[2]+zi[1])/2.; 
      xp[5]=(xi[0]+xi[2])/2.; yp[5]=(yi[0]+yi[2])/2.;zp[5]=(zi[0]+zi[2])/2.; 
      
      xq[0]=xi[3]; yq[0]=yi[3]; zq[0]=zi[3];
      xq[1]=xi[4]; yq[1]=yi[4]; zq[1]=zi[4];
      xq[2]=xi[5]; yq[2]=yi[5]; zq[2]=zi[5];
      xq[3]=(xi[3]+xi[4])/2.; yq[3]=(yi[3]+yi[4])/2.;zq[3]=(zi[3]+zi[4])/2.; 
      xq[4]=(xi[5]+xi[4])/2.; yq[4]=(yi[5]+yi[4])/2.;zq[4]=(zi[5]+zi[4])/2.; 
      xq[5]=(xi[3]+xi[5])/2.; yq[5]=(yi[3]+yi[5])/2.;zq[5]=(zi[3]+zi[5])/2.; 

      for (k=0;k<4;k++)
	for (l=0;l<4;l++)
	  {
	    xi[0]=xp[ss_tria[k][0]]; yi[0]=yp[ss_tria[k][0]]; zi[0]=zp[ss_tria[k][0]];
	    xi[1]=xp[ss_tria[k][1]]; yi[1]=yp[ss_tria[k][1]]; zi[1]=zp[ss_tria[k][1]];
	    xi[2]=xp[ss_tria[k][2]]; yi[2]=yp[ss_tria[k][2]]; zi[2]=zp[ss_tria[k][2]];
	    xi[3]=xq[ss_tria[l][0]]; yi[3]=yq[ss_tria[l][0]]; zi[3]=zq[ss_tria[l][0]];
	    xi[4]=xq[ss_tria[l][1]]; yi[4]=yq[ss_tria[l][1]]; zi[4]=zq[ss_tria[l][1]];
	    xi[5]=xq[ss_tria[l][2]]; yi[5]=yq[ss_tria[l][2]]; zi[5]=zq[ss_tria[l][2]];
		
	    triafdfC(arbre,size_min,dim_boite,prem,xi,yi,zi,
		     noeud,npoin,nod,coord,&nd,fforme); 
	  }
      *ndecoup++;
    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | dupliq3d_per                                                         |
  |          Dupliquer le maillage pour le traitement de la periodicite  |
  |          avec eventuellement un plan de symetrie orthogonal a l'axe  |
  |          definissant la periodicite de rotation                      |
  |======================================================================| */
void dupliq3d_per(struct SymPer defsymper,int n2,
		  struct Maillage maillnodray,struct Maillage *maillnod2)
{
  int i,j,n,i1,i2,ii1,ii2,idebnel,idebnp,numper,npersym,inverse;
  double sa,sb,sc,sd,se,t[4][4],t1[4][4],t2[4][4];
  double phi,theta,c,s,c2,s2,an,aa,bb,cc,dd,ee,ff,gg,hh,ii;
  double px,py,pz,ax,ay,az,alfa,angle,eps=1.e-6;
    

  /* On duplique noeuds, coordonnees et normales */
  dupliq_maill(maillnodray,maillnod2,n2);



  /* Creation des symetries ou periodicites */
  /* -------------------------------------- */

  /* si besoin, on commence par la symetrie du domaine initial */
  if (defsymper.nbsym==1)
    {
      inverse=-1;
      sa=defsymper.sym[0][0];
      sb=defsymper.sym[0][1];
      sc=defsymper.sym[0][2];
      sd=defsymper.sym[0][3];
      se=-2./(sa*sa+sb*sb+sc*sc);
      t1[0][0]=1.+ sa*sa*se;
      t1[1][1]=1.+ sb*sb*se;
      t1[2][2]=1.+ sc*sc*se;
      t1[0][1]=t1[1][0]=sa*sb*se;
      t1[0][2]=t1[2][0]=sa*sc*se;
      t1[1][2]=t1[2][1]=sb*sc*se;
      t1[0][3]=sa*sd*se; t1[1][3]=sb*sd*se; t1[2][3]=sc*sd*se;
      t1[3][0]=t1[3][1]=t1[3][2]=0; t1[3][3]=1;
      
      npersym=defsymper.nbper ;
      idebnel=npersym*maillnodray.nelem;	
      idebnp =npersym*maillnodray.npoin;
      persym3d(t1,npersym,inverse,maillnodray,*maillnod2,idebnel,idebnp);
    }


    numper=0;

    px=defsymper.per[0][0];
    py=defsymper.per[0][1];
    pz=defsymper.per[0][2];
    ax=defsymper.per[0][3];
    ay=defsymper.per[0][4];
    az=defsymper.per[0][5];
    alfa=defsymper.per[0][6]*2*Pi/360.;

    /* ensuite, on cree chaque autre portion d'angle */
    /* et si besoin, son domaine symetrique          */

    for (i=0;i<defsymper.nbper-1;i++)
      {
        angle=alfa*(i+1);
	if (fabs(ax) >  eps)
	  {
	    an=sqrt(ax*ax+ay*ay);
	    phi=atan2(ay,ax); theta=atan2(az,an) ;
	    c=cos(phi) ; s  =sin(phi) ; c2=cos(theta) ; s2=sin(theta);
	  }
	else if (fabs(ay) >  eps)
	  {
	    an=sqrt(ax*ax+ay*ay);
	    theta=atan2(az,an) ; c=0. ; s=1. ; c2=cos(theta) ; s2=sin(theta) ;
	  }
	else
	  {
	    c =1 ; s=0 ; c2=0 ; s2=1 ;
	  }

	aa= c2*c ;
	bb=-c2*s ;
	cc= s2 ;
	dd= cos(angle)*s+sin(angle)*s2*c ;
	ee= cos(angle)*c-sin(angle)*s*s2 ;
	ff= -sin(angle)*c2 ;
	gg= sin(angle)*s-cos(angle)*s2*c ;
	hh= sin(angle)*c+cos(angle)*s*s2 ;
	ii= cos(angle)*c2 ;

	t[0][0]=aa*aa+s*dd-c*s2*gg;
	t[1][1]=-s*c2*bb+c*ee+s*s2*hh;
	t[2][2]=s2*cc+c2*ii;
	t[1][0]=-s*c2*aa+c*dd+s*s2*gg;
	t[0][1]=aa*bb+s*ee-c*s2*hh;
	t[2][0]=s2*aa+c2*gg;
	t[0][2]=aa*cc+s*ff-c*s2*ii;
	t[2][1]=s2*bb+c2*hh;
	t[1][2]=-s*c2*cc+c*ff+s*s2*ii;
        t[3][0]=t[3][1]=t[3][2]=0.;
	t[0][3]= px ; t[1][3]= py ; t[2][3]= pz ; t[3][3]=1;
	
	numper +=1;
	inverse=1 ;
	idebnel=numper*maillnodray.nelem;	
	idebnp =numper*maillnodray.npoin;  
	persym3d(t,numper,inverse,maillnodray,*maillnod2,idebnel,idebnp);

	if (defsymper.nbsym==1)
	  {
	    inverse=-1;
	    sa=defsymper.sym[0][0];
	    sb=defsymper.sym[0][1];
	    sc=defsymper.sym[0][2];
	    sd=defsymper.sym[0][3];
	    se=-2./(sa*sa+sb*sb+sc*sc);
	    t1[0][0]=1.+ sa*sa*se;
	    t1[1][1]=1.+ sb*sb*se;
	    t1[2][2]=1.+ sc*sc*se;
	    t1[0][1]=t1[1][0]=sa*sb*se;
	    t1[0][2]=t1[2][0]=sa*sc*se;
	    t1[1][2]=t1[2][1]=sb*sc*se;
	    t1[0][3]=sa*sd*se; t1[1][3]=sb*sd*se; t1[2][3]=sc*sd*se;
	    t1[3][0]=t1[3][1]=t1[3][2]=0; t1[3][3]=1;
		
	    t2[0][0]= t[0][0]*t1[0][0]+t[0][1]*t1[1][0]+t[0][2]*t1[2][0]+t[0][3]*t1[3][0];
	    t2[0][1]= t[0][0]*t1[0][1]+t[0][1]*t1[1][1]+t[0][2]*t1[2][1]+t[0][3]*t1[3][1];
	    t2[0][2]= t[0][0]*t1[0][2]+t[0][1]*t1[1][2]+t[0][2]*t1[2][2]+t[0][3]*t1[3][2];
	    t2[0][3]= t[0][0]*t1[0][3]+t[0][1]*t1[1][3]+t[0][2]*t1[2][3]+t[0][3]*t1[3][3];

	    t2[1][0]= t[1][0]*t1[0][0]+t[1][1]*t1[1][0]+t[1][2]*t1[2][0]+t[1][3]*t1[3][0];
	    t2[1][1]= t[1][0]*t1[0][1]+t[1][1]*t1[1][1]+t[1][2]*t1[2][1]+t[1][3]*t1[3][1];
	    t2[1][2]= t[1][0]*t1[0][2]+t[1][1]*t1[1][2]+t[1][2]*t1[2][2]+t[1][3]*t1[3][2];
	    t2[1][3]= t[1][0]*t1[0][3]+t[1][1]*t1[1][3]+t[1][2]*t1[2][3]+t[1][3]*t1[3][3];

	    t2[2][0]= t[2][0]*t1[0][0]+t[2][1]*t1[1][0]+t[2][2]*t1[2][0]+t[2][3]*t1[3][0];
	    t2[2][1]= t[2][0]*t1[0][1]+t[2][1]*t1[1][1]+t[2][2]*t1[2][1]+t[2][3]*t1[3][1];
	    t2[2][2]= t[2][0]*t1[0][2]+t[2][1]*t1[1][2]+t[2][2]*t1[2][2]+t[2][3]*t1[3][2];
	    t2[2][3]= t[2][0]*t1[0][3]+t[2][1]*t1[1][3]+t[2][2]*t1[2][3]+t[2][3]*t1[3][3];

	    t2[3][0]= t[3][0]*t1[0][0]+t[3][1]*t1[1][0]+t[3][2]*t1[2][0]+t[3][3]*t1[3][0];
	    t2[3][1]= t[3][0]*t1[0][1]+t[3][1]*t1[1][1]+t[3][2]*t1[2][1]+t[3][3]*t1[3][1];
	    t2[3][2]= t[3][0]*t1[0][2]+t[3][1]*t1[1][2]+t[3][2]*t1[2][2]+t[3][3]*t1[3][2];
	    t2[3][3]= t[3][0]*t1[0][3]+t[3][1]*t1[1][3]+t[3][2]*t1[2][3]+t[3][3]*t1[3][3];

	    npersym=numper+defsymper.nbper;
	    idebnel=npersym*maillnodray.nelem;	
	    idebnp =npersym*maillnodray.npoin;  
	    persym3d(t2,npersym,inverse,maillnodray,*maillnod2,idebnel,idebnp);

	  }

      }                     


   
  /* impressions de controle */
  /* imprime_maillage(*maillnod2); */
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | persym3d                                                             |
  |          Calculer le periodique d'un maillage                        |
  |          et eventuellement son periodique et symetrique              |
  |======================================================================| */
void persym3d(double t[4][4],int numsym,int inverse,
	      struct Maillage maillnodray,struct Maillage maillnod2,
	      int idebnel,int idebnp)
{
  int n,nv;
  double x,y,z;
      
  for (n=0;n<maillnodray.nelem;n++)
    {
      x=maillnodray.xnf[0][n];  y=maillnodray.xnf[1][n]; z=maillnodray.xnf[2][n];
      maillnod2.xnf[0][idebnel+n]= t[0][0]*x+t[0][1]*y+t[0][2]*z;
      maillnod2.xnf[1][idebnel+n]= t[1][0]*x+t[1][1]*y+t[1][2]*z;
      maillnod2.xnf[2][idebnel+n]= t[2][0]*x+t[2][1]*y+t[2][2]*z;
    }  

  for (n=0;n<maillnodray.npoin;n++)
    {	
      x=maillnodray.coord[0][n]; y=maillnodray.coord[1][n]; z=maillnodray.coord[2][n];
      maillnod2.coord[0][idebnp+n]= t[0][0]*x+t[0][1]*y+t[0][2]*z + t[0][3]; 
      maillnod2.coord[1][idebnp+n]= t[1][0]*x+t[1][1]*y+t[1][2]*z + t[1][3]; 
      maillnod2.coord[2][idebnp+n]= t[2][0]*x+t[2][1]*y+t[2][2]*z + t[2][3];
    } 

  if (inverse==-1)
    for (n=0;n<maillnodray.nelem;n++)
      {
        maillnod2.node[0][idebnel+n] = maillnodray.node[0][n] + idebnp;
        maillnod2.node[1][idebnel+n] = maillnodray.node[2][n] + idebnp;
        maillnod2.node[2][idebnel+n] = maillnodray.node[1][n] + idebnp;
      }
  else
    for (n=0;n<maillnodray.nelem;n++)
      {
        maillnod2.node[0][idebnel+n] = maillnodray.node[0][n] + idebnp;
        maillnod2.node[1][idebnel+n] = maillnodray.node[1][n] + idebnp;
        maillnod2.node[2][idebnel+n] = maillnodray.node[2][n] + idebnp;
      }

}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | dimension_3d                                                         |
  |           dimensions caracteristiques du probleme                    |
  |======================================================================| */
void dimension_3d(struct Maillage maillnodray,
		  double *taille_boite,double *taille_seg)
{
  int i,*na,*nb,*nc;
  double xmin,xmax,ymin,ymax,zmin,zmax;
  double xa,ya,za,xb,yb,zb,xc,yc,zc,ds;
  double *px,*py,*pz;
  
    xmin=ymin=zmin=  1.e10; 
    xmax=ymax=zmax= -1.e10;

    for (i=0,px=maillnodray.coord[0],py=maillnodray.coord[1],pz=maillnodray.coord[2];
	 i<maillnodray.npoin;i++,px++,py++,pz++)
    {
       xmin=min(*px,xmin); xmax=max(*px,xmax);
       ymin=min(*py,ymin); ymax=max(*py,ymax);
       zmin=min(*pz,zmin); zmax=max(*pz,zmax);
    }
  
  *taille_boite=0.;
  *taille_boite=max(*taille_boite,(xmax-xmin));
  *taille_boite=max(*taille_boite,(ymax-ymin));
  *taille_boite=max(*taille_boite,(zmax-zmin));


  *taille_seg=1.e8;

  for (i=0,na=maillnodray.node[0],nb=maillnodray.node[1],nc=maillnodray.node[2];
       i<maillnodray.nelem;i++,na++,nb++,nc++)
    {
      xa=maillnodray.coord[0][*na]; ya=maillnodray.coord[1][*na]; za=maillnodray.coord[2][*na];
      xb=maillnodray.coord[0][*nb]; yb=maillnodray.coord[1][*nb]; zb=maillnodray.coord[2][*nb];
      xc=maillnodray.coord[0][*nc]; yc=maillnodray.coord[1][*nc]; zc=maillnodray.coord[2][*nc];
      ds=sqrt((xb-xa)*(xb-xa)+(yb-ya)*(yb-ya)+(zb-za)*(zb-za));
      *taille_seg=min(*taille_seg,ds);
      ds=sqrt((xc-xa)*(xc-xa)+(yc-ya)*(yc-ya)+(zc-za)*(zc-za));
      *taille_seg=min(*taille_seg,ds);
      ds=sqrt((xb-xc)*(xb-xc)+(yb-yc)*(yb-yc)+(zb-zc)*(zb-zc));
      *taille_seg=min(*taille_seg,ds);
    }


  if (syrglob_nparts==1 || syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      {
	printf("\n *** dimension_3d : dimensions caracteristiques :\n"); 
	printf("                    encombrement   =%f \n",*taille_boite); 
	printf("                    plus petit segment=%f \n",*taille_seg); 
      }
    else if (SYRTHES_LANG == EN)
      {
	printf("\n *** dimension_3d : dimensions  :\n"); 
	printf("                    overall box dimension  =%f \n",*taille_boite); 
	printf("                    smallest segment=%f \n",*taille_seg); 
      }

}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | cnorm3                                                               |
  |         Calcul de normales en dimension 3                            |
  |======================================================================| */
void cnor_3d(struct Maillage maill)
{
  int n,*n1,*n2,*n3;
  double *xn1,*xn2,*xn3;
  double xn,yn,zn,an,xab,yab,zab,xac,yac,zac;

  for (n=0,xn1=maill.xnf[0],xn2=maill.xnf[1],xn3=maill.xnf[2];n<maill.nelem;n++,xn1++,xn2++,xn3++) 
    {*xn1=*xn2=*xn3=0.;}

  for (n=0,n1=maill.node[0],n2=maill.node[1],n3=maill.node[2],
	 xn1=maill.xnf[0],xn2=maill.xnf[1],xn3=maill.xnf[2];
       n<maill.nelem;
       n++,n1++,n2++,n3++,xn1++,xn2++,xn3++) 
    {
      xab=maill.coord[0][*n2]-maill.coord[0][*n1]; xac=maill.coord[0][*n3]-maill.coord[0][*n1];
      yab=maill.coord[1][*n2]-maill.coord[1][*n1]; yac=maill.coord[1][*n3]-maill.coord[1][*n1];
      zab=maill.coord[2][*n2]-maill.coord[2][*n1]; zac=maill.coord[2][*n3]-maill.coord[2][*n1];

      xn= yab*zac - zab*yac;
      yn=-xab*zac + zab*xac;
      zn= xab*yac - yab*xac;
      an=sqrt(xn*xn + yn*yn + zn*zn);
      *xn1=xn/an; *xn2=yn/an; *xn3=zn/an;
    }
}
      
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | box_3d                                                               |
  |         Optimisation de la boite englobante en 3d                    |
  |======================================================================| */
void box_3d(int npoin,double **cooray)
{
  int n,ok,vpd;
  double xm,ym,zm,xm2,ym2,zm2,xmym,xmzm,ymzm;
  double c11,c22,c33,c12,c13,c21,c23,c31,c32,a,b,c;
  double a11,a22,a33,a12,a13,a23;
  double b11,b22,b33,b12,b13,b21,b23,b31,b32;
  double x1,x2,x3,x,y,z,a1,a2,a3,b1,b2,b3,c1,c2,c3;
  double ux,uy,uz,vx,vy,vz,wx,wy,wz,tx,ty,tz,an,det;
  double epsi,epsir,delta ;
  double p,q,racp,aux,phi,pi ;

  epsi=1e-6 ; epsir=1e-20 ;
  pi=3.141592653589793;


  xm=ym=zm=0.;
  for (n=0;n<npoin;n++)
    {
      xm += cooray[0][n];
      ym += cooray[1][n];
      zm += cooray[2][n];
    }
   xm /= npoin; ym /= npoin;   zm /= npoin;


  xm2=ym2=zm2=xmym=xmzm=ymzm=0.;
  for (n=0;n<npoin;n++)
    {
      xm2  += cooray[0][n]*cooray[0][n];
      ym2  += cooray[1][n]*cooray[1][n];
      zm2  += cooray[2][n]*cooray[2][n];
      xmym += cooray[0][n]*cooray[1][n];
      xmzm += cooray[0][n]*cooray[2][n];
      ymzm += cooray[1][n]*cooray[2][n];
    }
  
  c11=xm2/npoin -xm*xm;
  c22=ym2/npoin -ym*ym;
  c33=zm2/npoin -zm*zm;
  c12=c21=xmym/npoin - xm*ym;
  c13=c31=xmzm/npoin - xm*zm;
  c23=c32=ymzm/npoin - ym*zm;
      
      
  a=-(c11+c22+c33);
  b=-(-c11*c22 - c11*c33 - c22*c33 + c13*c31 + c12*c21 + c32*c23);
  c=-(c11*c22*c33 + c21*c32*c13 + c12*c23*c31 - c13*c31*c22 - c21*c12*c33 - c32*c23*c11);
    
  p=(3*b-a*a)/9.;
  q=a*a*a/27. -a*b/6. +c/2 ;

  if ( p < -epsir && p*p*p-q*q < -epsir )
    {
      racp=sqrt(-p) ;
      aux=- q/(p*racp) ;
      if (aux>1) aux=1;
      if (aux<-1) aux=-1;
      phi=acos(aux);

      x1=-2*racp*cos(phi/3)       -a/3 ;
      x2= 2*racp*cos((pi-phi)/3.) -a/3 ;
      x3= 2*racp*cos((pi+phi)/3.) -a/3.;
/*       if (SYRTHES_LANG == FR) */
/* 	printf(" $$ box_3d :Valeur propres de la transformation l1=%f l2=%f l3=%f \n",x1,x2,x3); */
/*       else if (SYRTHES_LANG == EN) */
/* 	printf(" $$ box_3d :Transformation eigen values l1=%f l2=%f l3=%f \n",x1,x2,x3); */
    }
  else
    {
/*       if (syrglob_nparts==1 ||syrglob_rang==0) */
/* 	if (SYRTHES_LANG == FR) */
/* 	  printf(" $$ box_3d : pas de valeurs propres reelles 1\n"); */
/* 	else if (SYRTHES_LANG == EN) */
/* 	  printf(" $$ box_3d : No real eigen values 1\n"); */
      return;
    }

  /* recherche des valeurs propres multiples */
  /* elle est triple => rien a faire */
  if (fabs(x1-x2)<epsi && fabs(x1-x3)<epsi && fabs(x3-x2)<epsi) return;
  /* vp double */
  if (fabs(x1-x2)<epsi) vpd=12;
  else if (fabs(x1-x3)<epsi) vpd=13;
  else if (fabs(x2-x3)<epsi) vpd=23;
  else vpd=0;


  delta=x1*0.00001;
  a11=c11-x1-delta; a12=c12          ; a13=c13   ;
                    a22=c22-x1-delta ; a23=c23   ;
                                       a33=c33-x1-delta;

/*   if (SYRTHES_LANG == FR) */
/*     printf("$$ box_3d : systeme 1\n"); */
/*   else if (SYRTHES_LANG == EN) */
/*     printf("$$ box_3d : system 1\n"); */

  det=a11*a22*a33 + 2*a12*a23*a13  -a13*a13*a22 -a11*a23*a23 -a33*a12*a12;
  b11=(a22*a33-a23*a23)/det ; b12=-(a12*a33-a13*a23)/det ; b13= (a12*a23-a13*a22)/det ;
  b21=b12                   ; b22= (a11*a33-a13*a13)/det ; b23=-(a11*a23-a12*a13)/det ;
  b31=b13                   ; b32=b23                    ; b33= (a11*a22-a12*a12)/det ;
  
  resoud3(b11,b12,b13,b21,b22,b23,b31,b32,b33,&ux,&uy,&uz);

/*   if (SYRTHES_LANG == FR) */
/*     printf("$$ box_3d : vecteur 1 : %f %f %f\n",ux,uy,uz); */
/*   else if (SYRTHES_LANG == EN) */
/*     printf("$$ box_3d : vector 1 : %f %f %f\n",ux,uy,uz); */

  if (vpd!=12)
    {
/*       if (SYRTHES_LANG == FR) */
/* 	printf("$$ box_3d : systeme 2\n"); */
/*       else if (SYRTHES_LANG == EN) */
/* 	printf("$$ box_3d : system 2\n"); */

      delta=x2*0.00001;
      a11=c11-x2-delta;  a22=c22-x2-delta ;  a33=c33-x2-delta;

      det=a11*a22*a33 + 2*a12*a23*a13  -a13*a13*a22 -a11*a23*a23 -a33*a12*a12;
      b11=(a22*a33-a23*a23)/det ; b12=-(a12*a33-a13*a23)/det ; b13= (a12*a23-a13*a22)/det ;
      b21=b12                   ; b22= (a11*a33-a13*a13)/det ; b23=-(a11*a23-a12*a13)/det ;
      b31=b13                   ; b32=b23                    ; b33= (a11*a22-a12*a12)/det ;

      resoud3(b11,b12,b13,b21,b22,b23,b31,b32,b33,&vx,&vy,&vz);
/*       printf("vecteur 2 : %f %f %f\n",vx,vy,vz); */
    }


   if (vpd!=13 && vpd!=23)    
     {
/*        if (SYRTHES_LANG == FR) */
/* 	 printf("$$ box_3d :systeme 3\n"); */
/*        else if (SYRTHES_LANG == EN) */
/* 	 printf("$$ box_3d :system 3\n"); */

      delta=x3*0.00001;
      a11=c11-x3-delta;  a22=c22-x3-delta ;  a33=c33-x3-delta;

      det=a11*a22*a33 + 2*a12*a23*a13  -a13*a13*a22 -a11*a23*a23 -a33*a12*a12;
      b11=(a22*a33-a23*a23)/det ; b12=-(a12*a33-a13*a23)/det ; b13= (a12*a23-a13*a22)/det ;
      b21=b12                   ; b22= (a11*a33-a13*a13)/det ; b23=-(a11*a23-a12*a13)/det ;
      b31=b13                   ; b32=b23                    ; b33= (a11*a22-a12*a12)/det ;

      resoud3(b11,b12,b13,b21,b22,b23,b31,b32,b33,&wx,&wy,&wz);
/*       if (SYRTHES_LANG == FR) */
/* 	printf("$$ box_3d :vecteur 3 : %f %f %f\n",wx,wy,wz); */
/*       else if (SYRTHES_LANG == EN) */
/* 	printf("$$ box_3d :vector 3 : %f %f %f\n",wx,wy,wz); */
     }


  if (vpd!=0) 
    switch (vpd)
      {
      case 12 : vx=wy*uz-wz*uy;
	        vy =-wx*uz+wz*ux;
	        vz=wx*uy-wy*ux;
	        break;

      case 13 : wx=uy*vz-uz*vy;
	        wy =-ux*vz+uz*vx;
	        wz=ux*vy-uy*vx;
	        break;

	
      case 23 : wx=uy*vz-uz*vy;
	        wy =-ux*vz+uz*vx;
	        wz=ux*vy-uy*vx;
	        break;

      }

/*   if (SYRTHES_LANG == FR) */
/*     printf("vecteurs propres :\n"); */
/*   else if (SYRTHES_LANG == EN) */
/*     printf("eigen vectors :\n"); */
/*   printf("   u : %f %f %f :\n",ux,uy,uz); */
/*   printf("   v : %f %f %f :\n",vx,vy,vz); */
/*   printf("   w : %f %f %f :\n",wx,wy,wz); */


  tx=uy*vz-uz*vy;  ty=uz*vx-ux*vz;  tz=ux*vy-uy*vx;
  if (fabs(tx-wx)>epsi || fabs(ty-wy)>epsi || fabs(tz-wz)>epsi)
    {wx =-wx; wy=-wy; wz=-wz;}

  for (n=0;n<npoin;n++)
    {
      x=ux*cooray[0][n] + vx*cooray[1][n] + wx*cooray[2][n];
      y=uy*cooray[0][n] + vy*cooray[1][n] + wy*cooray[2][n];
      z=uz*cooray[0][n] + vz*cooray[1][n] + wz*cooray[2][n];
      cooray[0][n]=x;
      cooray[1][n]=y;  
      cooray[2][n]=z;
    }
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | resoud3                                                              |
  |                                                                      |
  |======================================================================| */
void resoud3(double b11,double b12,double b13,double b21,double b22,
	     double b23,double b31,double b32,double b33,
	     double *ux,double *uy,double *uz)
{  
  double xx,yy,zz,xn,x,y,z;
  double epsi;
  int n;

  epsi=1.E-8;
  x=y=z=1.;
  xx=0; yy=0; zz=0;
  n=0;

  while (fabs(fabs(xx)-fabs(x))>epsi || 
         fabs(fabs(zz)-fabs(z))>epsi || fabs(fabs(yy)-fabs(y))>epsi)
    {
      n+=1;
      xx=x; yy=y; zz=z;
      x=b11*xx+b12*yy+b13*zz;
      y=b21*xx+b22*yy+b23*zz;
      z=b31*xx+b32*yy+b33*zz;
      xn=sqrt(x*x+y*y+z*z); x/=xn; y/=xn; z/= xn;
    }

/*   if (SYRTHES_LANG == FR) */
/*     printf(" >>> resoud3 : vecteur propre obtenu en %d iterations\n",n); */
/*   else if (SYRTHES_LANG == EN) */
/*     printf(" >>> resoud3 : eigen vectors obtained in %d iterations\n",n); */

  *ux=x; *uy=y; *uz=z;
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I.RUPP                                       |
  |======================================================================|
  | contou2d                                                             |
  |                                                                      |
  |======================================================================| */
double contou3d(double xi[],double yi[],double zi[])
{      
  int n,m,n2,m1,m2,ityp;
  double xab,yab,zab,xcd,ycd,zcd,xac,yac,zac,ab,cd;
  double xad,yad,zad,xbc,ybc,zbc,xbd,ybd,zbd,ac,ad,bc,bd;
  double a,b,c,pabac;
  double cost,valint,pi;
  double eps,fforme;


  fforme=0.;
  pi=3.141592653589793;
  eps=1.e-6;

  for (n=0;n<3;n++)
    for (m=0;m<3;m++)
      {
	n2=(n+1)%3;
	m2=3+(m+1)%3;
	m1=m+3;

	xab=xi[n2]-xi[n];  yab=yi[n2]-yi[n];  zab=zi[n2]-zi[n];
	xcd=xi[m2]-xi[m1]; ycd=yi[m2]-yi[m1]; zcd=zi[m2]-zi[m1];

	ab=sqrt(xab*xab + yab*yab + zab*zab);
	cd=sqrt(xcd*xcd + ycd*ycd + zcd*zcd);

	cost=(xab*xcd + yab*ycd + zab*zcd) / (ab*cd);

	/* 1.3 orthogonalite des vecteurs
	   ------------------------------*/
	if (fabs(cost)<=eps)
	  valint=0.;
	else
	  {
            xad=xi[m2]-xi[n];  yad=yi[m2]-yi[n];  zad=zi[m2]-zi[n];
            xbc=xi[m1]-xi[n2]; ybc=yi[m1]-yi[n2]; zbc=zi[m1]-zi[n2];
            xac=xi[m1]-xi[n];  yac=yi[m1]-yi[n];  zac=zi[m1]-zi[n];
            xbd=xi[m2]-xi[n2]; ybd=yi[m2]-yi[n2]; zbd=zi[m2]-zi[n2];

            ad=sqrt(xad*xad + yad*yad + zad*zad);
            bc=sqrt(xbc*xbc + ybc*ybc + zbc*zbc);
            ac=sqrt(xac*xac + yac*yac + zac*zac);
            bd=sqrt(xbd*xbd + ybd*ybd + zbd*zbd);

           /* 1.4 cas general
           ---------------*/
            if (ad>eps && bc>eps && ac>eps && bd>eps)
	      {
		pabac=(xab*xac + yab*yac + zab*zac)/(ab*ac);
		if (fabs(cost)<=1.-eps || fabs(pabac)<=1.-eps)
		  valint=intseg(xab,yab,zab,xac,yac,zac,xcd,ycd,zcd);
		else
		  {
		    a=ab; b=cd; c=ac;
		    if (cost > eps && pabac > eps )
		      valint=-3. + ( -(-a+b+c)*(-a+b+c) * log(fabs(-a+b+c)) +(c-a)*(c-a)    * log(c-a)
				    -c*c       * log(c)+(c+b)*(c+b) *log(c+b) )/ (a*b);
		  
		    else if ( cost > eps && pabac <= -eps )
		      valint=-3. + (-(c-b+a)*(c-b+a) * log(fabs(c-b+a))+(c-b)*(c-b)   * log(fabs(c-b))
				    +(c+a)*(c+a)   * log(a+c) -c*c       * log(c)    )/ (a*b);
		    else if ( cost <= -eps && pabac >= eps ) 
		      valint=-3. +( (-a-b+c)*(-a-b+c) * log(fabs(c-b-a))-(c-b)*(c-b)    * log(fabs(c-b))
				    -(c-a)*(c-a)    * log(fabs(c-a))+ c*c       * log(c)    )/(a*b);
		    else if ( cost <= -eps && pabac <= -eps ) 
		      valint=-3. +( (a+b+c)*(a+b+c) * log(a+b+c)-(b+c)*(b+c)   * log(b+c)
				   -(c+a)*(c+a)   * log(c+a)+ c*c      * log(c)      )/(a*b);
		    else
		      if (SYRTHES_LANG == FR)
			printf("\n contou3d : Ce cas ne devrait jamais intervenir !\n voir IR/CP concepteurs de syrthes\n");
		      else if (SYRTHES_LANG == EN)
			printf("\n contou3d : This case should never arise !\n see IR/CP syrthes conceptors\n");
		  }
	      }

           /* 1.5 cas singuliers 
           ------------------ */
            else if (ad <= eps)
	      if (fabs(cost) >= 1.-eps*0.001) 
		if ( abs (ab-cd) < eps )  
		  if (cost< 0.) 
		    valint=2.*log(ab)-3.;
		  else
		    valint=2.*log(ab) -3. +4*log(2*ab);
		else
		  if (cost < 0.) 
		    valint=- 3. +( ab*ab*log(ab) + cd*cd*log(cd) -
				  (ab-cd)*(ab-cd)*log(fabs(cd-ab)) )/(ab*cd);
		  else
		    valint=- 3. +(-ab*ab*log(ab)-cd*cd*log(cd) +
				  (ab+cd)*(ab+cd)*log(ab+cd) )/(ab*cd);
	      else
		{
		  ityp=1;a=ab*ab; b=2.*(xab*xac + yab*yac + zab*zac); c=ac*ac;
		  valint=int2eg(a,b,c,ityp);
		}

            else if (bc <= eps) 
	      if (fabs(cost) >= 1.-eps*0.001) 
		if (fabs(ab-cd) <= eps) 
		  if (cost< 0.)
		    { 
		      if (SYRTHES_LANG == FR)
			printf(" contou3d : ce cas aurait deja du etre traite\n");
		      else if (SYRTHES_LANG == EN)
			printf(" contou3d : This case should already have been handled \n");

                      valint=2.*log(ab)-3.;
		    }
		  else
		    valint=-2.*log(ab) -3. +4*log(2*ab);
		else
		  if (cost<0.) 
		    valint=- 3. +( cd*cd*log(cd) + ab*ab*log(ab) -
                              (ab-cd)*(ab-cd)*log(fabs(ab-cd)) )/(ab*cd);
		  else
		    valint=- 3. +(-cd*cd*log(cd)-ab*ab*log(ab) +
				  (ab+cd)*(ab+cd)*log(ab+cd) )/(ab*cd);
	      else
		{
		  a=cd*cd;b=-2.*(xab*xcd + yab*ycd + zab*zcd); c=ab*ab; ityp=1;
		  valint=int2eg(a,b,c,ityp);
		}

            else if (ac <= eps) 
	      if (fabs(cost) >= 1.-eps*0.001) 
		if (fabs(ab-cd) <= eps) 
		  if (cost< 0.) 
		    valint=-2.*log(ab) -3. + 4*log(2*ab);
		  else
		    valint=2.*log(ab)-3.;
		else
		  if (cost>0.) 
		    valint=-3. +( cd*cd*log(cd) + ab*ab*log(ab) -
				 (ab-cd)*(ab-cd)*log(fabs(cd-ab)) )/ (ab*cd);
		  else
		    valint=-3. +(-ab*ab*log(ab) -cd*cd*log(cd) +
				 (ab+cd)*(ab+cd)*log(ab+cd) )/ (ab*cd);
	      else
		{
		  a=ab*ab;b=-2.*(xab*xad + yab*yad + zab*zad);c=ad*ad;ityp=2;
		  valint=int2eg(a,b,c,ityp);
		}

            else if (bd <= eps) 
	      if (fabs(cost) >= 1.-eps*.001) 
		if (fabs(ab-cd) <= eps) 
		  if (cost< 0.) 
		    valint=-2.*log(ab) -3. + 4*log(2*ab);
		  else
		    {
		      if (SYRTHES_LANG == FR)
			printf(" contou3d : ce cas est a priori deja traite\n");
		      else if (SYRTHES_LANG == EN)
			printf(" contou3d : a priori, this case has already been handled\n");

                      valint=2.*log(ab)-3.;
		    }
                 else
                   if (cost>0.) 
                      valint=- 3. +( ab*ab*log(ab) + cd*cd*log(cd) -
                              (ab-cd)*(ab-cd)*log(fabs(ab-cd))  )/ (ab*cd);              
                   else
                      valint=- 3. +( -ab*ab*log(ab)-cd*cd*log(cd) +
                              (ab+cd)*(ab+cd)*log(ab+cd)  )/ (ab*cd);              

               else
		 {
		   ityp=3; a=ab*ab; b=2.*(xab*xbc + yab*ybc + zab*zbc); c=bc*bc; 
		   valint=int2eg(a,b,c,ityp); 
		 }

            /*1.6- erreur
	      -----------  */
            else
	      {
		if (SYRTHES_LANG == FR)
		  printf("\n ERREUR contou3d : on n'est dans aucun cas prevu !\n");
		else if (SYRTHES_LANG == EN)
		  printf("\n ERROR contou3d : out of imagined case !\n");
		syrthes_exit(1);
	      }
	  }
           /* 1.7- mise  a jour de la valeur de l'integrale
            --------------------------------------------- */
            fforme+= 0.5 * cost * ab * cd * valint;

/* 	    printf("m=%d n=%d cost=%f ab=%f cd=%f valint=%f \n",m,n,cost,ab,cd,valint); */
      }

  /* 3- valeur du facteur de forme * surface */
  fforme/=(2.*pi);
/*   printf("fforme_f92-f260=%f \n",fforme); */
  return fforme;

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | intseg                                                               |
  |                                                                      |
  |======================================================================| */
double intseg(double xab,double yab,double zab,double xac,double yac,
	      double zac,double xcd,double ycd,double zcd)
{      
  double term1,term2,al,b2,u,v,u2,v2;
  double valint,t12,t22,t1,t2,t1t2,t0;
  double a,b,c,d,e,eps;
  double res,resg,del,al1,al2,c2,d2,rac,rac2;
  double ag,bg,cg,dg,delg;
  double toto1,toto2;
  int i;

  double x[8]={0.408282678752175 , 0.237233795041835, 
	       0.101666761293187 , 0.019855071751232 ,
               0.591717321247825 , 0.762766204958165 ,
               0.898333238706814 , 0.980144928248768 };

  double w[8]={0.362683783378362 , 0.313706645877887,
               0.222381034453374 , 0.101228536290376,
               0.362683783378362 , 0.313706645877887,
               0.222381034453374 , 0.101228536290376  };


  eps = 1.e-10;

  t12 = xab*xab + yab*yab + zab*zab;
  t22 = xcd*xcd + ycd*ycd + zcd*zcd;
  t1  = -2 * (xac*xab + yac*yab + zac*zab);
  t2  =  2 * (xac*xcd + yac*ycd + zac*zcd);
  t1t2= -2 * (xab*xcd + yab*ycd + zab*zcd);
  t0  = xac*xac + yac*yac + zac*zac;

  for (i=0,resg=0;i<8;i++)
    {
      ag = t12;
      bg = t1t2*x[i]+t1;
      cg = t22*x[i]*x[i] + t2*x[i] + t0;
      dg = -bg*bg + 4.*t12*cg;
      delg = sqrt(fabs(dg));
      resg = resg + w[i]*(delg/ag) *(atan((2.*ag+bg)/delg)-atan(bg/delg));
    }

  res=resg*0.5;

  a = 0.5*t1t2/t12;
  b = 0.5*t1/t12 + 1.;
  c = t22;
  d = t1t2+t2;
  e = t12+t1+t0;
  c2 = c*c;
  d2 = d*d;
  del = sqrt(fabs(4.*c*e-d2));
  if (del<eps)
    {
      rac = -0.5*d/c;
      rac2 = rac*rac;
      term1 = (b+0.5*a)*log(c)
	       +(2.*b*(1.-rac)+a*(1-rac2))*log(fabs(1.-rac))
	       +(a*rac2+2.*b*rac) * log(fabs(-rac))
	       -2.*b - a*(0.5+rac) ;
    }
  else
    {
      al1 = atan(d/del);
      al2 = atan((2.*c+d)/del);
      term1 = (al1-al2)*(a*(4.*d*e*c-d2*d)+b*(2.*c*d2-8.*e*c2))/(2.*c2*del)
              -(-c2*(4.*b+2.*a)-2.*c*(a*e+b*d)+a*d2)/(4.*c2)*log(c+d+e)
	      -(a+4*b)*0.5 + a*d*0.5/c
	      +(-2.*c*(b*d+a*e)+a*d2)*0.25/c2*log(e);

    }

  a = -0.5*t1t2/t12;
  b = -0.5*t1/t12;
  c = t22;
  d = t2;
  e = t0;
  c2 = c*c;
  d2 = d*d;
  del = sqrt(fabs(4.*c*e-d2));
  if (del<eps)
    {
      rac = -0.5*d/c;
      rac2 = rac*rac;
      term2 = (b+0.5*a)*log(c)
	       +(2.*b*(1.-rac)+a*(1-rac2))*log(fabs(1.-rac))
	       +(a*rac2+2.*b*rac) * log(fabs(-rac))
	       -2.*b - a*(0.5+rac) ;
    }
  else
    {
      al1 = atan(d/del);
      al2 = atan((2.*c+d)/del);
      b =  - 0.5*t1/t12;
      term2 = (al1-al2)*(a*(4.*d*e*c-d2*d)+b*(2.*c*d2-8.*e*c2))/(2.*c2*del)
              -(-c2*(4.*b+2.*a)-2.*c*(a*e+b*d)+a*d2)/(4.*c2)*log(c+d+e)
	      -(a+4*b)*0.5 + a*d*0.5/c
	      +(-2.*c*(b*d+a*e)+a*d2)*0.25/c2*log(e);
    }

/*   printf("res=%f term1=%f term2=%f \n",res,term1,term2); */
  return (res + term1 + term2 -2.);
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | int2eg                                                             |
  |                                                                      |
  |======================================================================| */
double int2eg(double a,double b,double c,int ityp)
{      
  double term1,term2,al,b2,u,v,u2,v2;
  double del,al1,al2,c2,res;

  switch (ityp)
    {
    case 1: /* Dans tout ce qui suit, on a soit A=D soit B=C */
      al=sqrt(fabs(4.*a*c-b*b));
      u=2.*a/al;v=b/al;u2=u*u;v2=v*v;
      res=0.5*u2/((v2+1.)*(v2+1.)) *
                  ( (v2-1)*(atan(v)-atan((u*v-1.-v2)/u)) -
                      v*(log(u2)-log(v2-2.*u*v+1.+u2)) )
            + 0.5*( atan(u-v) + u/(v2+1.) );
      res *=(al/a);
      res=res + 0.5*al/a*atan(b/al);

      u=0.5*b/a; c2=c*c; b2=b*b;
      del=sqrt(fabs(4.*c*a-b2)); al1=atan((2.*c-b)/del);
      al2=atan(b/del);
      term1=0.5/c2 *  (
		       (al1+al2) * (u*(b*b*b-4.*a*b*c)+2.*c*(4.*a*c-b2))/del
		       + 0.5 *(b*(2.*c-b*u)+2.*u*a*c) * log(a)
		       + (c2*(-u+2.)-c*(b+u*a)+0.5*u*b2) * log(a-b+c)
		       + u*b*c+c2*(u-4.)  );
      
      term2=0.25*b/a *(log(c)-1.);
      return (res + term1 + term2 -2.);
      break;

    case 2: /*dans tout ce qui suit, on a a=c */
      al=sqrt(fabs(4.*a*c-b*b));
      u=2.*a/al; v=b/al; u2=u*u; v2=v*v;
      res=0.5*u2/((v2+1.)*(v2+1.)) *
	   ( (v2-1)*(-atan(v)+atan((u*v+1.+v2)/u)) 
	    +  v*(log(u2)-log(v2+2.*u*v+1.+u2)) )
	     + 0.5*( atan(u+v) + u/(v2+1.) );
      res*=(al/a);
      res-= 0.5*al/a*atan(b/al);

      u=0.5*b/a; c2=c*c; b2=b*b;
      del=sqrt(fabs(4.*c*a-b2));
      al1=atan((2.*c+b)/del); al2=atan(b/del);
      term1=0.5/c2 *  (
         (al1-al2) * (u*(b*b*b-4.*a*b*c)+2.*c*(4.*a*c-b2))/del
       - 0.5 *(b*(2.*c-b*u)+2.*u*a*c) * log(a)
       + (c2*(u+2.)+c*(b+u*a)-0.5*u*b2) * log(a+b+c)
      + u*b*c-c2*(u+4.)  );

      term2=0.25*b/a *(-log(c)+1.);
      return (res + term1 + term2 -2.);
      break;

    case 3:
      al=sqrt(4.*a*c-b*b);
      u=2.*a/al; v=-b/al; u2=u*u; v2=v*v;
      res=0.5*u2/((v2+1.)*(v2+1.)) *
                   ( (v2-1)*(atan(v)-atan((u*v-1.-v2)/u)) -
                       v*(log(u2)-log(v2-2.*u*v+1.+u2)) )
            + 0.5*( atan(u-v) + u/(v2+1.) );
      res*=(al/a);
      res-= 0.5*al/a*atan(b/al);

      u=0.5*b/a; c2=c*c;  b2=b*b;
      del=sqrt(fabs(4.*c*a-b2));
      al1=atan((2.*c+b)/del); al2=atan(b/del);
      term1=0.5/c2 *  (
         (al1-al2) * (u*(b*b*b-4.*a*b*c)+2.*c*(4.*a*c-b2))/del
       - 0.5 *(b*(2.*c-b*u)+2.*u*a*c) * log(a)
       + (c2*(u+2.)+c*(b+u*a)-0.5*u*b2) * log(a+b+c)
       + u*b*c-c2*(u+4.)  );

      term2=0.25*b/a *(1.-log(c));
      return (res + term1 + term2 -2.);
      break;
    }
  return 0.;
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | cal_mask_3d                                                            |
  |         Detecter si deux point se voient                             |
  |======================================================================| */
double cal_mask_3d(double xi[],double yi[],double **cooray,struct Mask mask)
{
  int i;
  int na,nb,nc;
  double epsi,xn,xab,yab,zab,xac,yac,zac;
  double xa,ya,za,xb,yb,zb,xc,yc,zc,a,b,c,d,den,den1,valmask,t,xp,yp,zp;
  double ro[3],rd[3],pt_arr[3];

  epsi=1.E-6;

  valmask=1;
  ro[0]=0.5*(xi[0]+xi[1]+xi[2]);     ro[1]=0.5*(yi[0]+yi[1]+yi[2]);
  pt_arr[0]=0.5*(xi[3]+xi[4]+xi[5]); pt_arr[1]=0.5*(yi[3]+yi[4]+yi[5]);
  rd[0]=pt_arr[0]-ro[0];  rd[1]=pt_arr[1]-ro[1];


  for (i=0,nb=0;i<mask.nelem;i++)
    {
      na=mask.node[0][i]; nb=mask.node[1][i]; nc=mask.node[2][i];
      xa=cooray[0][na]; ya=cooray[1][na]; za=cooray[2][na];
      xb=cooray[0][nb]; yb=cooray[1][nb]; zb=cooray[2][nb];
      xc=cooray[0][nc]; yc=cooray[1][nc]; zc=cooray[2][nc];
      xab = xb-xa; yab = yb-ya; zab = zb-za;
      xac = xc-xa; yac = yc-ya; zac = zc-za;
      a = yab*zac-zab*yac;
      b = zab*xac-xab*zac;
      c = xab*yac-yab*xac;
      xn=sqrt(a*a+b*b+c*c);
      a /=xn; b /=xn; c /=xn;
      d = -(a*xa+b*ya+c*za);
      
      den = a*rd[0]+b*rd[1]+c*rd[2];
      den1=den/(sqrt(rd[0]*rd[0]+rd[1]*rd[1]+rd[2]*rd[2]));

      if (fabs(den1)<epsi)
	continue;
      else
	{
	  t = - (d  + a*ro[0]+b*ro[1]+c*ro[2] ) / den;   
	  if (t<epsi)     /* ==> le plan du triangle est derriere => pas d'intersection */
	    continue;
	  else if (t>(1+epsi)) /* ==> le plan du triangle est trop loin  => pas d'intersection */
	    continue;
	  else
	    {
	      xp = ro[0]+t*rd[0];  yp = ro[1]+t*rd[1]; zp = ro[2]+t*rd[2]; /* intersection */
	      if (in_triangle(a,b,c,d,xa,ya,za,xab,yab,zab,xac,yac,zac,xp,yp,zp))
		valmask*=mask.opacite[i];
	    }
	}
    }
  
  return valmask;
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | rejectfdf_3d                                                         |
  |         Detection rapide de faces ne valant pas le coup              |
  |         Demandera peut-etre un REX                                   |
  |======================================================================| */
int rejectfdf_3d (double xn1,double yn1,double zn1,
		      double xn2,double yn2,double zn2,
		      double xi[],double yi[],double zi[])
{
  double epsrej=1e-8;
  double xg1,yg1,zg1,xg2,yg2,zg2;
  double vec_x,vec_y,vec_z;
  double dist,dist2,invdist,Fij,Si,Sj,Ri,Rj;
  double x01,y01,z01,x02,y02,z02,x12,y12,z12;
  double a,b,c,p;
  double tiers=1./3.;
  
  
  xg1=(xi[0]+xi[1]+xi[2])*tiers; yg1=(yi[0]+yi[1]+yi[2])*tiers; zg1=(zi[0]+zi[1]+zi[2])*tiers;
  xg2=(xi[3]+xi[4]+xi[5])*tiers; yg2=(yi[3]+yi[4]+yi[5])*tiers; zg2=(zi[3]+zi[4]+zi[5])*tiers;
  
  x01=xi[1]-xi[0]; x02=xi[2]-xi[0]; x12=xi[2]-xi[1];
  y01=yi[1]-yi[0]; y02=yi[2]-yi[0]; y12=yi[2]-yi[1];
  z01=zi[1]-zi[0]; z02=zi[2]-zi[0]; z12=zi[2]-zi[1];
  Si=0.5 * sqrt ((x01*y02-y01*x02)*(x01*y02-y01*x02)
		 + (y01*z02-z01*y02)*(y01*z02-z01*y02)
		 + (x01*z02-z01*x02)*(x01*z02-z01*x02) );
  a=sqrt(x01*x01+y01*y01+z01*z01);
  b=sqrt(x02*x02+y02*y02+z02*z02);
  c=sqrt(x12*x12+y12*y12+z12*z12);
  p=0.5*(a+b+c);
  Ri=0.25*a*b*c/sqrt(p*(p-a)*(p-b)*(p-c));


  x01=xi[4]-xi[3]; x02=xi[5]-xi[3]; x12=xi[2]-xi[1];
  y01=yi[4]-yi[3]; y02=yi[5]-yi[3]; y12=yi[2]-yi[1];
  z01=zi[4]-zi[3]; z02=zi[5]-zi[3]; z12=zi[2]-zi[1];
  Sj=0.5 * sqrt ((x01*y02-y01*x02)*(x01*y02-y01*x02)
		 + (y01*z02-z01*y02)*(y01*z02-z01*y02)
		 + (x01*z02-z01*x02)*(x01*z02-z01*x02) );
  a=sqrt(x01*x01+y01*y01+z01*z01);
  b=sqrt(x02*x02+y02*y02+z02*z02);
  c=sqrt(x12*x12+y12*y12+z12*z12);
  p=0.5*(a+b+c);
  Rj=0.25*a*b*c/sqrt(p*(p-a)*(p-b)*(p-c));
  
  
  vec_x=xg2-xg1;  vec_y=yg2-yg1;  vec_z=zg2-zg1;
  dist2=vec_x*vec_x+vec_y*vec_y*vec_z*vec_z;
  dist=sqrt(dist2);
  invdist=1./sqrt(vec_x*vec_x+vec_y*vec_y*vec_z*vec_z);
  vec_x=vec_x*invdist;  vec_y=vec_y*invdist;  vec_z=vec_z*invdist;
  
  Fij=(vec_x*xn1+vec_y*yn1*vec_z*zn1)*
    (vec_x*xn2+vec_y*yn2*vec_z*zn2)/dist2*Sj;
  

  if (fabs(Fij)<=epsrej && dist>5.*Ri && dist>5.*Rj)
    /* optimisation trop agressive (on ne retourne jamais 1) */
    return(0);
  else
    return(0);  
  
}

