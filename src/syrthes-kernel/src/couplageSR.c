/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_abs.h"
#include "syr_bd.h"
#include "syr_const.h"
#include "syr_tree.h"
#include "syr_option.h"
#include "syr_proto.h"
#include "syr_parall.h"

#ifdef _SYRTHES_MPI_
#include "mpi.h"
MPI_Status status;
#endif

#define N64 128

extern struct Affichages affich;
struct Performances perfo;
extern char nomcorrr[CHLONG];
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Initialisation des tableaux de couplage conduction/rayt            |
  |======================================================================| */
void init_scoupr(struct MaillageCL maillnodeus,struct Couple *scoupr,
		 struct SDparall_ray sdparall_ray)
{
  int i,j,nr,sr,srtot=0;


  for (i=0,sr=0;i<maillnodeus.nelem;i++)
    if (maillnodeus.type[i][POSCOUPR]==1) sr++;

  srtot=sr;
  if (syrglob_nparts>1) srtot=somme_int_parall(sr);

  if (srtot==0) {
    if (syrglob_nparts==1 || syrglob_rang==0)
      if (SYRTHES_LANG == FR)
	{
	  printf("\n\n %%%% ERREUR init_scoupr : Il n'y a pas de facettes ");
	  printf("de conduction couplee avec le rayonnement ");
	  printf("                             Voir le mot-cle 'CLIM=  COUPLAGE_RAYONNEMENT'");
	  printf(" dans le fichier de donnees (syrthes.data)\n\n"); 
	}
      else
	{
	  printf("\n\n %%%% ERREUR init_scoupr : There is no solid face coupled ");
	  printf("with the radiation mesh ");
	  printf("                             Check the keyword 'CLIM=  COUPLAGE_RAYONNEMENT'");
	  printf(" in the data file (syrthes.data)\n\n"); 
	}
    syrthes_exit(1);
  }
  
  if (sdparall_ray.npartsray==1 || sdparall_ray.rang==0)
    {
      if (SYRTHES_LANG == FR)
	printf("\n *** init_scoupr : nbre de faces solides couplees au rayonnement = %d\n",srtot);
      else if (SYRTHES_LANG == EN)
	printf("\n *** init_scoupr : number of solid faces coupled to radiation = %d\n",srtot);
    }

  scoupr->nelem=sr; scoupr->ndmat=maillnodeus.ndmat;

  if (sr>0)
    {
      scoupr->numf=(int*)malloc(sr*sizeof(int));verif_alloue_int1d("init_scoupr",scoupr->numf);
      scoupr->numcor=(int*)malloc(sr*sizeof(int));verif_alloue_int1d("init_scoupr",scoupr->numcor);
      scoupr->dist=(double*)malloc(sr*sizeof(double));verif_alloue_double1d("init_scoupr",scoupr->dist);
      scoupr->t=(double**)malloc(maillnodeus.ndmat*sizeof(double*));
      scoupr->h=(double**)malloc(maillnodeus.ndmat*sizeof(double*));
      for (i=0;i<maillnodeus.ndmat;i++) scoupr->t[i]=(double*)malloc(sr*sizeof(double));
      for (i=0;i<maillnodeus.ndmat;i++) scoupr->h[i]=(double*)malloc(sr*sizeof(double));
      verif_alloue_double2d(maillnodeus.ndmat,"init_scoupr",scoupr->t);
      verif_alloue_double2d(maillnodeus.ndmat,"init_scoupr",scoupr->h);

      for (i=0,sr=0;i<maillnodeus.nelem;i++)
	if (maillnodeus.type[i][POSCOUPR]==1) {scoupr->numf[sr]=i; sr++;}
    }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Initialisation des tableaux de couplage conduction/rayt            |
  |======================================================================| */
void init_rcoups(struct Maillage maillnodray,struct Couple *rcoups)
{
  int i,j,nr,rs;

  for (i=0,rs=0;i<maillnodray.nelem;i++)
    if (maillnodray.type[i]==TYPRCOUPS) rs++;

  rcoups->nelem=rs; rcoups->ndmat=maillnodray.ndmat;

  rcoups->numf=(int*)malloc(rs*sizeof(int));verif_alloue_int1d("init_rcoups",rcoups->numf);
  rcoups->numcor=(int*)malloc(rs*sizeof(int));verif_alloue_int1d("init_rcoups",rcoups->numcor);
  rcoups->dist=(double*)malloc(rs*sizeof(double));verif_alloue_double1d("init_rcoups",rcoups->dist);
  rcoups->bary=(double**)malloc(maillnodray.ndim*sizeof(double*));
  for (i=0;i<maillnodray.ndim;i++) rcoups->bary[i]=(double*)malloc(rcoups->nelem*sizeof(double));
  verif_alloue_double2d(maillnodray.ndim,"init_rcoups",rcoups->bary);
  for (i=0;i<rs;i++) 
    for (j=0;j<maillnodray.ndim;j++) rcoups->bary[j][i]=0;

  for (i=0,rs=0;i<maillnodray.nelem;i++)
    if (maillnodray.type[i]==TYPRCOUPS) {rcoups->numf[rs]=i; rs++;}

  for (i=0;i<rs;i++) rcoups->dist[i]=1.e6;
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Calcul des correspondances conduction/rayt                         |
  |======================================================================| */
void corresSR(struct GestionFichiers gestionfichiers,
	      struct Maillage maillnodes,
	      struct MaillageCL maillnodeus,struct Maillage maillnodray,
	      struct Couple scoupr,struct SDparall_ray sdparall_ray)
{
  if (syrglob_nparts==1 ||syrglob_rang==0){
    if (SYRTHES_LANG == FR) {
      printf("\n *** Calcul des correspondants solide/rayonnement \n"); 
    }
    else if (SYRTHES_LANG == EN){
      printf("\n *** Solid/radiation corresponding points calculation\n"); 
    }
  }

  if (scoupr.nelem)
    {

      /*       if (gestionfichiers.lec_corr_sr) */
      /* 	lire_corrSR(maillnodes.ndim,scoupr); */
      /*       else */
      
      /* recherche des correspondants */
      if (maillnodes.ndim==2)
	corresSR2d(maillnodes,maillnodeus,maillnodray,scoupr);
      else
	corresSR3d(maillnodes,maillnodeus,maillnodray,scoupr);
      
  
  
      /*       if (gestionfichiers.stock_corr_sr) */
      /* 	ecrire_corrSR(maillnodes.ndim,scoupr); */
    }

  /* histogramme des distances de correspondance */
  histog_corresp(scoupr.nelem,scoupr.dist,sdparall_ray);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Calcul des correspondances rayt/conduction                         |
  |======================================================================| */
void corresRS(struct GestionFichiers gestionfichiers,
	      struct MaillageBord maillnodebord,struct Maillage maillnodes,
	      struct MaillageCL maillnodeus,struct Maillage maillnodray,
	      struct Couple scoupr,struct Couple rcoups,struct SDparall_ray sdparall_ray)
{
  int i,j,ne,nb,np;
  int *itrav;
  struct Maillage maillnodecoupl;


  if (syrglob_nparts==1 ||syrglob_rang==0){
    if (SYRTHES_LANG == FR) {
      printf("\n *** Calcul des correspondants rayonnement/solide\n"); 
    }
    else if (SYRTHES_LANG == EN){
      printf("\n *** Radiation/solid corresponding points calculation\n"); 
    }
  }

  /*   if (gestionfichiers.lec_corr_sr) */
  /*     lire_corrRS(maillnodes.ndim,rcoups); */
  /*   else */

  /* creation d'un maillage de bord solide couple au rayt */
  maillnodecoupl.nelem=scoupr.nelem;
  maillnodecoupl.ndmat=scoupr.ndmat;
  maillnodecoupl.ndim=maillnodes.ndim;
  
  maillnodecoupl.node=(int**)malloc(maillnodecoupl.ndmat*sizeof(int*));
  for (i=0;i<maillnodecoupl.ndmat;i++) 
    maillnodecoupl.node[i]=(int*)malloc(maillnodecoupl.nelem*sizeof(int));
  verif_alloue_int2d(maillnodecoupl.ndmat,"corresRS",maillnodecoupl.node);


  /* attention, on a detourne nrefe pour y stocker le numero d'un elt nodecoupl dans nodeus */
  maillnodecoupl.nrefe=(int*)malloc(maillnodecoupl.nelem*sizeof(int));
  verif_alloue_int1d("corresRS",maillnodecoupl.nrefe);
  
  perfo.mem_ray+=(maillnodecoupl.ndmat+1)*maillnodecoupl.nelem*sizeof(int);
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  
  
  for (i=0;i<maillnodecoupl.nelem;i++)
    {
      for (j=0;j<maillnodecoupl.ndmat;j++)
	{
	  ne=scoupr.numf[i];
	  maillnodecoupl.node[j][i]=maillnodeus.node[j][ne];
	}
      maillnodecoupl.nrefe[i]=ne;
    }
  
  /* il faut passer les numero de noeud en num locale pour creer la table de coordonnees */
  itrav=(int*)malloc(maillnodes.npoin*sizeof(int));
  for (i=0;i<maillnodes.npoin;i++) itrav[i]=-1;
  
  for (nb=i=0;i<maillnodecoupl.nelem;i++)
    for (j=0;j<maillnodecoupl.ndmat;j++)
      {
	np=maillnodecoupl.node[j][i];
	if (itrav[np]==-1) {itrav[np]=nb; nb++;}
      }
  
  maillnodecoupl.npoin=nb;
  maillnodecoupl.nref=NULL;
  
  maillnodecoupl.coord=(double**)malloc(maillnodecoupl.ndim*sizeof(double*));
  for (i=0;i<maillnodecoupl.ndim;i++) 
    maillnodecoupl.coord[i]=(double*)malloc(maillnodecoupl.npoin*sizeof(double));
  verif_alloue_double2d(maillnodecoupl.ndim,"corresRS",maillnodecoupl.coord);
  perfo.mem_ray+=(maillnodecoupl.ndim+0.5)*maillnodecoupl.npoin*sizeof(double);
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  
  /* remplissage des coord */
  for (i=0;i<maillnodes.npoin;i++)
    if (itrav[i]>-1)
      for (j=0;j<maillnodecoupl.ndim;j++) maillnodecoupl.coord[j][itrav[i]]=maillnodes.coord[j][i];
  
  /* passage effectif a la num loc */
  for (nb=i=0;i<maillnodecoupl.nelem;i++)
    for (j=0;j<maillnodecoupl.ndmat;j++)
      maillnodecoupl.node[j][i] = itrav[maillnodecoupl.node[j][i]];
  
  
  /* recherche des correspondants */
  if (maillnodes.ndim==2)
    corresRS2d(maillnodecoupl,maillnodray,rcoups);
  else
    corresRS3d(maillnodecoupl,maillnodray,rcoups);
  
  /* destruction du maillage de bord solide couple au rayt */
  for (i=0;i<maillnodecoupl.ndmat;i++) free(maillnodecoupl.node[i]);
  for (i=0;i<maillnodecoupl.ndim;i++)  free(maillnodecoupl.coord[i]);
  free (maillnodecoupl.node);
  free (maillnodecoupl.coord);
  free (maillnodecoupl.nrefe);
  free (itrav);
  perfo.mem_ray-=(maillnodecoupl.ndmat+1)*maillnodecoupl.nelem*sizeof(int);
  perfo.mem_ray-=(maillnodecoupl.ndim+0.5)*maillnodecoupl.npoin*sizeof(double);
  
  /* histogramme des distances de correspondance */
  histog_corresp(rcoups.nelem,rcoups.dist,sdparall_ray);
  
  
  /*   if (gestionfichiers.stock_corr_sr) */
  /*     ecrire_corrRS(maillnodes.ndim,rcoups); */

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Calcul du correspondant de chaque facette de conduction            |
  |======================================================================| */
void corresSR3d(struct Maillage maillnodes,
		struct MaillageCL maillnodeus,struct Maillage maillnodray,
		struct Couple scoupr)
{
  int i,j,n,ne,arbre_entier,bis;
  int nelmin,numtri,nlonv,num,nm;
  int n0,n1,n2,n3,n4,n5,n6;
  int ntabel[N64],na[N64],nb[N64],nc[N64],icode[N64],lco[N64];
  double dim_boite[6],tiers;
  double xa[N64],xb[N64],xc[N64],ya[N64],yb[N64],yc[N64];
  double za[N64],zb[N64],zc[N64];
  double dm,dmin,xmin,ymin,zmin,xm,ym,zm;
  double b1,b2,b3,size_min;
  double xp1,yp1,zp1,x1,y1,z1,x2,y2,z2,x3,y3,z3;
  double vmin,interv;
  struct element *fa,*ff;
  struct node *arbre,*noeud;
  double **coords,**coordray;
  int nbeltmax_tree=200;


  coords=maillnodes.coord;
  coordray=maillnodray.coord;

    
  tiers=1./3.;

  boite2(3,maillnodes.npoin,maillnodes.coord,maillnodray.npoin,maillnodray.coord,dim_boite);

  size_min = 1.E8;
  arbre= (struct node *)malloc(sizeof(struct node));
  if (arbre==NULL) 
    {
      if (SYRTHES_LANG == FR)
	printf(" ERREUR corresSR3d : probleme d'allocation memoire\n");
      else if (SYRTHES_LANG == EN)
	printf(" ERROR corresSR3d : Memory allocation problem\n");
      syrthes_exit(1);
    }
  build_octree(arbre,maillnodray.npoin,maillnodray.nelem,maillnodray.node,maillnodray.coord,
	       &size_min,dim_boite,nbeltmax_tree);

  /* recreer la tete de l'arbre */
  for (n=0; n<maillnodray.nelem; n++)
    { 
      fa = (struct element *)malloc(sizeof(struct element));
      if (fa==NULL) 
	{
	  if (SYRTHES_LANG == FR)
	    printf(" ERREUR corresSR3d : probleme d'allocation memoire\n"); 
	  else if (SYRTHES_LANG == EN)
	    printf(" ERROR  corresSR3d : Memory allocation problem\n"); 
	  syrthes_exit(1);
	}
      fa->num = n;
      fa->suivant=NULL;
      if (n==0)
	{
	  arbre->lelement=fa;
	  ff=fa;
	}
      else
	{
	  ff->suivant=fa;
	  ff=fa;
	}
    }

/* corresp de chaque face solide */
  for (n=0; n<scoupr.nelem; n++) 
    /*--------------------------------*/
    {
      dmin=1.e6; xmin=ymin=zmin=0;
      nelmin=1; 
      nm=1; xm=ym=zm=0; dm=1.e6;

      ne=scoupr.numf[n];
      n0=maillnodeus.node[0][ne]; n1=maillnodeus.node[1][ne];  n2=maillnodeus.node[2][ne]; 
      xp1=tiers*(coords[0][n0]+coords[0][n1]+coords[0][n2]);
      yp1=tiers*(coords[1][n0]+coords[1][n1]+coords[1][n2]);
      zp1=tiers*(coords[2][n0]+coords[2][n1]+coords[2][n2]);

      noeud=arbre;
      find_node_3d (&noeud,xp1,yp1,zp1);

      fa=noeud->lelement;
      arbre_entier=0;
      if (fa==NULL) {noeud=arbre;fa=noeud->lelement; arbre_entier=1;}
   recommence2 :   while (fa != NULL)
	{
	  nlonv=0;
	  while (fa!=NULL && nlonv<N64) 
	    {if (maillnodray.type[fa->num]==TYPRCOUPS || maillnodray.type[fa->num]==TYPRVITRE){ntabel[nlonv]=fa->num;nlonv++;}
	     fa=fa->suivant;}
	  for (i=0;i<nlonv;i++)
	    {
              icode[i]=0; lco[i]=1; num=ntabel[i];
              na[i]=maillnodray.node[0][num];nb[i]=maillnodray.node[1][num];nc[i]=maillnodray.node[2][num];
	      xa[i]=coordray[0][na[i]]; ya[i]=coordray[1][na[i]]; za[i]=coordray[2][na[i]];
	      xb[i]=coordray[0][nb[i]]; yb[i]=coordray[1][nb[i]]; zb[i]=coordray[2][nb[i]];
	      xc[i]=coordray[0][nc[i]]; yc[i]=coordray[1][nc[i]]; zc[i]=coordray[2][nc[i]];
            }

	  cal_dmin(xp1,yp1,zp1,xa,ya,za,xb,yb,zb,xc,yc,zc,nlonv,&dm,&xm,&ym,&zm,&nm,lco,icode);

	  for (i=0;i<nlonv;i++)
	    if (icode[i]!=0) 
	      {
		if (SYRTHES_LANG == FR)
		  {
		    printf("\n !!! ERREUR corresSR3d : LA RECHERCHE DU CORRESPONDANT A ECHOUE\n");
		    printf("                        Face solide : %d\n",ne);
		    printf("                                      %13.6e %13.6e %13.6e\n",xp1,yp1,zp1);
		    printf("        dans la facette rayonnement : %d\n",ntabel[i]);
		    printf("                                      %d %d %d\n",na[i],nb[i],nc[i]);
		  }
		else if (SYRTHES_LANG == EN)
		  {
		    printf("\n !!! ERROR corresSR3d : THE CORRESPONDING POINT SEARCH HAS ABORTED\n");
		    printf("                        Solid face : %d\n",ne);
		    printf("                                      %13.6e %13.6e %13.6e\n",xp1,yp1,zp1);
		    printf("             in the radiation face : %d\n",ntabel[i]);
		    printf("                                      %d %d %d\n",na[i],nb[i],nc[i]);
		  }
		syrthes_exit(1);
	      }
	  
	  if (dm<dmin) {dmin=dm; xmin=xm; ymin=ym;zmin=zm; nelmin=ntabel[nm];}
	}

   /* on n'a pas trouve de correspondant, on fait une seconde tentative sur tout le domaine */
   if (dmin>99999.) 
     {
       if (arbre_entier)
	 {
	   if (SYRTHES_LANG == FR)
	     printf("       Pas de correspondant, segment rayonnement %d coord %f %f\n",ne,xp1,yp1);
	   else if (SYRTHES_LANG == EN)
	     printf("       No corresponding point, radiation segment %d coord %f %f\n",ne,xp1,yp1);
	   syrthes_exit(1);
	 }
       if (!arbre_entier)
	 {
	   if (SYRTHES_LANG == FR)
	     {
	       printf("       Pas de correspondant, segment rayonnement %d coord %f %f\n",ne,xp1,yp1);
	       printf("       On recommence la recherche sur l'ensemble du domaine...\n");
	     }
	   else if (SYRTHES_LANG == EN)
	     {
	       printf("       No corresponding nodes, radiation segment %d coord %f %f\n",ne,xp1,yp1);
	       printf("       One starts the search again on the overall domain...\n");
	     }
	   noeud=arbre;fa=noeud->lelement; 
	   arbre_entier=1;
	   goto recommence2;
	 }
     }
   
   /*    commenter pour les gros cas : */
   /*    if (bis) printf("       --> OK\n"); */

   n1=maillnodray.node[0][nelmin]; n2=maillnodray.node[1][nelmin]; n3=maillnodray.node[2][nelmin];
   
   scoupr.numcor[n]=nelmin; 
   scoupr.dist[n]=dmin; 
   
   if (affich.correspSR)
     {
       if (SYRTHES_LANG == FR)
	 {
	   printf("\n       Face solide : %6d     coord : %13.6e %13.6e %13.6e\n",n,xp1,yp1,zp1);
	   printf("   CORRESPONDANT RAYONNEMENT :\n");
	   printf("            numero de la facette ray : %6d\n",nelmin);
	   printf("                              noeuds : %6d  %6d  %6d\n",n1,n2,n3);
	   printf("              coord du correspondant : %13.6e %13.6e %13.6e\n",xmin,ymin,zmin);
	   printf("                   distance minimale :%22.16e\n",dmin);
	 }
       else if (SYRTHES_LANG == EN)
	 {
	   printf("\n       Solid face : %6d     coord : %13.6e %13.6e %13.6e\n",n,xp1,yp1,zp1);
	   printf("   RADIATION CORRESPONDING POINT :\n");
	   printf("               radiation face number : %6d\n",nelmin);
	   printf("                               nodes : %6d  %6d  %6d\n",n1,n2,n3);
	   printf("     corresponding point coordinates : %13.6e %13.6e %13.6e\n",xmin,ymin,zmin);
	   printf("                      minum distance :%22.16e\n",dmin);
	 }
     }
   
    }
  
  tuer_tree(arbre); free(arbre);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Calcul du correspondant de chaque facette de rayonnement           |
  |======================================================================| */
void corresRS3d(struct Maillage maillnodecoupl,struct Maillage maillnodray,
		struct Couple rcoups)
{
  int i,j,n,ne,arbre_entier,bis;
  int nelmin,numtri,nlonv,num,nm;
  int n0,n1,n2,n3,n4,n5,n6;
  int ntabel[N64],na[N64],nb[N64],nc[N64],icode[N64],lco[N64];
  double dim_boite[6],tiers;
  double xa[N64],xb[N64],xc[N64],ya[N64],yb[N64],yc[N64];
  double za[N64],zb[N64],zc[N64];
  double dm,xmin,ymin,zmin,xm,ym,zm,dmin=2.e6;
  double b1,b2,b3,size_min;
  double xp1,yp1,zp1,x1,y1,z1,x2,y2,z2,x3,y3,z3;
  double vmin,interv;
  struct element *fa,*ff;
  struct node *arbre,*noeud;
  double **coords,**coordray;
  int nbeltmax_tree=200;

  for (n=0; n<rcoups.nelem; n++) rcoups.dist[n]=dmin;
  if (maillnodecoupl.nelem==0) return;

  coords=maillnodecoupl.coord;
  coordray=maillnodray.coord;

    
  tiers=1./3.;

  boite2(3,maillnodecoupl.npoin,maillnodecoupl.coord,maillnodray.npoin,maillnodray.coord,dim_boite);


  arbre= (struct node *)malloc(sizeof(struct node));
  if (arbre==NULL) 
    {
      if (SYRTHES_LANG == FR)
	printf(" ERREUR corresRS3d : probleme d'allocation memoire\n");
      else if (SYRTHES_LANG == EN)
	printf(" ERROR corresRS3d : Memory allocation problem\n");
      syrthes_exit(1);
    }
  build_octree(arbre,maillnodecoupl.npoin,maillnodecoupl.nelem,maillnodecoupl.node,maillnodecoupl.coord,
	       &size_min,dim_boite,nbeltmax_tree);

  /* recreer la tete de l'arbre */
  for (n=0; n<maillnodecoupl.nelem; n++)
    { 
      fa = (struct element *)malloc(sizeof(struct element));
      if (fa==NULL) 
	{
	  if (SYRTHES_LANG == FR)
	    printf(" ERREUR corresRS3d : probleme d'allocation memoire\n");
	  else if (SYRTHES_LANG == EN)
	    printf(" ERROR  corresRS3d : Memory allocation problem\n");
	  syrthes_exit(1);
	}
      fa->num = n;
      fa->suivant=NULL;
      if (n==0)
	{
	  arbre->lelement=fa;
	  ff=fa;
	}
      else
	{
	  ff->suivant=fa;
	  ff=fa;
	}
    }

/* corresp de chaque face de ray */
  for (n=0; n<rcoups.nelem; n++) 
    /*--------------------------------*/
    {
      dmin=1.e6; xmin=ymin=zmin=0;
      nelmin=1; 
      nm=1; xm=ym=zm=0; dm=1.e6;

      ne=rcoups.numf[n];
      n0=maillnodray.node[0][ne]; n1=maillnodray.node[1][ne];  n2=maillnodray.node[2][ne]; 
      xp1=tiers*(coordray[0][n0]+coordray[0][n1]+coordray[0][n2]);
      yp1=tiers*(coordray[1][n0]+coordray[1][n1]+coordray[1][n2]);
      zp1=tiers*(coordray[2][n0]+coordray[2][n1]+coordray[2][n2]);

      noeud=arbre;
      find_node_3d (&noeud,xp1,yp1,zp1);

      fa=noeud->lelement;
      arbre_entier=0; bis=0;
      if (fa==NULL) {noeud=arbre;fa=noeud->lelement; arbre_entier=1;}
  recommence1 :  while (fa != NULL)
	{
	  nlonv=0;
	  while (fa!=NULL && nlonv<N64) 
	    {
	      ntabel[nlonv]=fa->num;
	      nlonv++;
	      fa=fa->suivant;
	    }
	  for (i=0;i<nlonv;i++)
	    {
              icode[i]=0; lco[i]=1; num=ntabel[i];
              na[i]=maillnodecoupl.node[0][num];nb[i]=maillnodecoupl.node[1][num];nc[i]=maillnodecoupl.node[2][num];
	      xa[i]=coords[0][na[i]]; ya[i]=coords[1][na[i]]; za[i]=coords[2][na[i]];
	      xb[i]=coords[0][nb[i]]; yb[i]=coords[1][nb[i]]; zb[i]=coords[2][nb[i]];
	      xc[i]=coords[0][nc[i]]; yc[i]=coords[1][nc[i]]; zc[i]=coords[2][nc[i]];
            }

	  cal_dmin(xp1,yp1,zp1,xa,ya,za,xb,yb,zb,xc,yc,zc,nlonv,&dm,&xm,&ym,&zm,&nm,lco,icode);

	  for (i=0;i<nlonv;i++)
	    if (icode[i]!=0) 
	      {
		if (SYRTHES_LANG == FR)
		  {
		    printf("\n !!! ERREUR corresSR3d  : LA RECHERCHE DU CORRESPONDANT A ECHOUE\n");
		    printf("                    FACE RAYONNEMENT: %d\n",ne);
		    printf("                             centre : %13.6e %13.6e %13.6e\n",xp1,yp1,zp1);
		    printf("            DANS LE TRIANGLE SOLIDE : %d\n",ntabel[i]);
		    printf("                             noeuds : %d %d %d\n",na[i],nb[i],nc[i]);
		  }
		else if (SYRTHES_LANG == EN)
		  {
		    printf("\n !!! ERROR corresSR3d  : THE CORRESPONDING POINT SEARCH HAS FAILED\n");
		    printf("                   radiation face : %d\n",ne);
		    printf("                                      %13.6e %13.6e %13.6e\n",xp1,yp1,zp1);
		    printf("            in the solid triangle : %d\n",ntabel[i]);
		    printf("                                      %d %d %d\n",na[i],nb[i],nc[i]);
		  }
		syrthes_exit(1);
	      }
	  
	  if (dm<dmin) {dmin=dm; xmin=xm; ymin=ym;zmin=zm; nelmin=ntabel[nm];}
	}

      /* on n'a pas trouve de correspondant, on fait une seconde tentative sur tout le domaine */
      if (dmin>99999.) 
	{
	  if (arbre_entier)
	    {
	      if (SYRTHES_LANG == FR)
		printf("       Pas de correspondant, segment rayonnement %d coord %f %f\n",ne,xp1,yp1);
	      else if (SYRTHES_LANG == EN)
		printf("       No corresponding point, radiation segment %d coord %f %f\n",ne,xp1,yp1);
	      syrthes_exit(1);
	    }
	  if (!arbre_entier)
	    {
	      if (SYRTHES_LANG == FR)
		{
		  printf("       Pas de correspondant, segment rayonnement %d coord %f %f\n",ne,xp1,yp1);
		  printf("       On recommence la recherche sur l'ensemble du domaine...\n");
		}
	      else if (SYRTHES_LANG == EN)
		{
		  printf("       No corresponding point, radiation segment %d coord %f %f\n",ne,xp1,yp1);
		  printf("       The search is done again on the overall domain...\n");
		}
	      noeud=arbre;fa=noeud->lelement; 
	      arbre_entier=1; bis=1;
	      goto recommence1;
	    }
	}

/*       if (bis) printf("       --> OK\n"); */

      rcoups.dist[n]=dmin;
      n1=maillnodecoupl.node[0][nelmin]; n2=maillnodecoupl.node[1][nelmin]; n3=maillnodecoupl.node[2][nelmin];

      x1=coords[0][n1];  y1=coords[1][n1]; z1=coords[2][n1]; 
      x2=coords[0][n2];  y2=coords[1][n2]; z2=coords[2][n2]; 
      x3=coords[0][n3];  y3=coords[1][n3]; z3=coords[2][n3]; 

      bary3d (xmin,ymin,zmin,x1,y1,z1, x2,y2,z2, x3,y3,z3,&b1,&b2,&b3);
      
      rcoups.bary[0][n]=b1; rcoups.bary[1][n]=b2; rcoups.bary[2][n]=b3; 
      /* rcoups.numcor[n]=nelmin; Attention  c'est donc un numero de corresp dans maillnodecoupl ! */
      /* attention, on a detourne nrefe pour y stocke le numero d'un elt nodecoupl dans nodeus */
      rcoups.numcor[n]=maillnodecoupl.nrefe[nelmin];  /* c'est donc un numero de corresp dans maillnodeus */

      if (affich.correspSR)
	{
	  if (SYRTHES_LANG == FR)
	    {
	      printf("\n   FACE RAYONNEMENT : %6d    coord : %13.6e %13.6e %13.6e\n",n,xp1,yp1,zp1);
	      printf("   CORRESPONDANT SOLIDE :\n");
	      printf("           numero du triangle solide : %6d\n",nelmin);
	      printf("                              noeuds : %6d  %6d  %6d\n",n1,n2,n3);
	      printf("              coord du correspondant : %13.6e %13.6e %13.6e\n",xmin,ymin,zmin);
	      printf("                   distance minimale :%22.16e\n",dmin);
	      printf("                coord barycentriques : %13.6e %13.6e %13.6e\n",b1,b2,b3);
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      printf("\n   RADIATION FACE : %6d    coord : %13.6e %13.6e %13.6e\n",n,xp1,yp1,zp1);
	      printf("   SOLID CORRESPONDING POINT :\n");
	      printf("                   solid face number : %6d\n",nelmin);
	      printf("                               nodes : %6d  %6d  %6d\n",n1,n2,n3);
	      printf("     corresponding point corrdinates : %13.6e %13.6e %13.6e\n",xmin,ymin,zmin);
	      printf("                    minimum distance : %22.16e\n",dmin);
	      printf("                  barycentric weight : %13.6e %13.6e %13.6e\n",b1,b2,b3);
	    }
	  
	}

    }

  tuer_tree(arbre); free(arbre);

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Calcul du correspondant de chaque facette de conduction            |
  |======================================================================| */
void corresSR2d(struct Maillage maillnodes,struct MaillageCL maillnodeus,
		struct Maillage maillnodray,
		struct Couple scoupr)
{
  int i,j,n,arbre_entier,bis;
  int nelmin,numseg,nlonv,num,nm,ne;
  int n0,n1,n2;
  int ntabel,na,nb,nc,icode,lco;
  double dim_boite[6];
  double xa,xb,xc,ya,yb,yc;
  double dm,dmin,xmin,ymin,xm,ym;
  double b1,b2,b3,size_min;
  double xp1,yp1,x1,y1,x2,y2;
  double vmin,interv;
  struct element *fa,*ff;
  struct node *arbre,*noeud;
  double **coordray,**coords;
  int nbeltmax_tree=200;


  coordray=maillnodray.coord;
  coords=maillnodes.coord;


  boite2(2,maillnodes.npoin,maillnodes.coord,maillnodray.npoin,maillnodray.coord,dim_boite);

  size_min = 1.E8;
  arbre= (struct node *)malloc(sizeof(struct node));
  if (arbre==NULL) 
    {
      if (SYRTHES_LANG == FR)
	printf(" ERREUR corresSR2d : probleme d'allocation memoire\n");
      else if (SYRTHES_LANG == EN)
	printf(" ERREUR corresSR2d : Memory allocation problem\n");
      syrthes_exit(1);
    }
  build_quadtree_1d(arbre,maillnodray.npoin,maillnodray.nelem,maillnodray.node,maillnodray.coord,
		    &size_min,dim_boite,nbeltmax_tree);

  /* recreer la tete de l'arbre */
  for (n=0; n<maillnodray.nelem; n++)
    { 
      fa = (struct element *)malloc(sizeof(struct element));
      if (fa==NULL) 
	{
	  if (SYRTHES_LANG == FR)
	    printf(" ERREUR corresSR2d : probleme d'allocation memoire\n");
	  else if (SYRTHES_LANG == EN)
	    printf(" ERREUR corresSR2d : Memory allocation problem\n");
	  syrthes_exit(1);
	}
      fa->num = n;
      fa->suivant=NULL;
      if (n==0)
	{
	  arbre->lelement=fa;
	  ff=fa;
	}
      else
	{
	  ff->suivant=fa;
	  ff=fa;
	}
    }

/* pour chaque face solide...     */
/*--------------------------------*/
  for (n=0; n<scoupr.nelem; n++) 
/*--------------------------------*/
    {
      dmin=1.e6; xmin=ymin=0;  nelmin=1;  nm=1; xm=ym=0; dm=1.e6;

      ne=scoupr.numf[n];
      n0=maillnodeus.node[0][ne]; n1=maillnodeus.node[1][ne];
      xp1=0.5*(coords[0][n0]+coords[0][n1]);
      yp1=0.5*(coords[1][n0]+coords[1][n1]);

      noeud=arbre;
      find_node_2d(&noeud,xp1,yp1);

      fa=noeud->lelement;
      arbre_entier=0;
      if (fa==NULL) {noeud=arbre;fa=noeud->lelement; arbre_entier=1;}
  recommence2 :    while (fa != NULL)
	{
	  if (maillnodray.type[fa->num]==TYPRCOUPS|| maillnodray.type[fa->num]==TYPRVITRE)
	    {
	      num=fa->num;
              icode=0; lco=1;
              na=maillnodray.node[0][num];nb=maillnodray.node[1][num];
	      xa=coordray[0][na]; ya=coordray[1][na];
	      xb=coordray[0][nb]; yb=coordray[1][nb];
	      dptseg(xp1,yp1,xa,ya,xb,yb,&dm,&xm,&ym,&icode);
	      if (icode!=0) 
		{
		  if (SYRTHES_LANG == FR)
		    {
		      printf("\n !!! ERREUR corresSR2d  : LA RECHERCHE DU CORRESPONDANT A ECHOUE\n");
		      printf("                       Face solide : %d\n",ne);
		      printf("                            centre : %13.6e %13.6e\n",xp1,yp1);
		      printf("               dans la facette ray : %d\n",num);
		      printf("                            noeuds : %d %d\n",na,nb);
		    }
		  else if (SYRTHES_LANG == EN)
		    {
		      printf("\n !!! ERROR corresSR2d  : THE CORRESPONDING SEARCH HAS FAILED\n");
		      printf("                     Solid face : %d\n",ne);
		      printf("                                      %13.6e %13.6e\n",xp1,yp1);
		      printf("          in the radiation face : %d\n",num);
		      printf("                                      %d %d\n",na,nb);
		    }
		  syrthes_exit(1);
		}	      
	      if (dm<dmin) {dmin=dm; xmin=xm; ymin=ym; nelmin=num;}
	    }
	  fa=fa->suivant;
	}
      

      /* on n'a pas trouve de correspondant, on fait une seconde tentative sur tout le domaine */
      if (dmin>99999.) 
	{
	  if (arbre_entier)
	    {
	      if (SYRTHES_LANG == FR)
		printf("       Pas de correspondant, segment solide %d coord %f %f\n",ne,xp1,yp1);
	      else if (SYRTHES_LANG == EN)
		printf("       No corresponding points, solid segment %d coord %f %f\n",ne,xp1,yp1);
	      syrthes_exit(1);
	    }
	  if (!arbre_entier)
	    {
	      if (SYRTHES_LANG == FR)
		{
		  printf("       Pas de correspondant, segment solide %d glob=%d coord %f %f\n",n,ne,xp1,yp1);
		  printf("       On recommence la recherche sur l'ensemble du domaine...\n");
		}
	      else if (SYRTHES_LANG == EN)
		{
		  printf("       No corresponding points, solid segment %d coord %f %f\n",ne,xp1,yp1);
		  printf("       The search is started again on the overall domain...\n");
		}
	      noeud=arbre;fa=noeud->lelement; 
	      arbre_entier=1;
	      goto recommence2;
	    }
	}

/*       if (bis) printf("       --> OK\n"); */

      n1=maillnodray.node[0][nelmin]; n2=maillnodray.node[1][nelmin]; 

      scoupr.numcor[n]=nelmin; 
      scoupr.dist[n]=dmin; 

      if (affich.correspSR)
	{
	  if (SYRTHES_LANG == FR)
	    {
	      printf("\n   FACE SOLIDE : %6d   coordonnees : %13.6e %13.6e\n",n,xp1,yp1);
	      printf("   CORRESPONDANT RAYONNEMENT :\n");
	      printf("               numero de la face ray : %6d\n",nelmin);
	      printf("                              noeuds : %6d  %6d\n",n1,n2);
	      printf("              coord du correspondant : %13.6e %13.6e\n",xmin,ymin);
	      printf("                   distance minimale :%22.16e\n",dmin);
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      printf("\n   SOLID FACE : %6d   coordinates : %13.6e %13.6e\n",n,xp1,yp1);
	      printf("   RADIATION CORRESPONDING POINT :\n");
	      printf("               radiation face number : %6d\n",nelmin);
	      printf("                               nodes : %6d  %6d\n",n1,n2);
	      printf("     corresponding point coordinates : %13.6e %13.6e\n",xmin,ymin);
	      printf("                    minimum distance :%22.16e\n",dmin);
	    }
	}

    }

  tuer_tree(arbre); free(arbre);

  if (affich.correspSR)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf(">>> corresSR2d : recapitulatif des tableaux de transfert\n");
	  printf(">>> pour chaque face solide, numero du corresp de rayonnement\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf(">>> corresSR2d : Transfer tables summary\n");
	  printf(">>> for each solid face, number of the radiation corresponding point\n");
	}
      for (n=0; n<scoupr.nelem; n++) 
	printf("Face=%d numcor=%d \n",n,scoupr.numcor[n]);
    }

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Calcul du correspondant de chaque facette de rayonnement           |
  |======================================================================| */
void corresRS2d(struct Maillage maillnodecoupl,struct Maillage maillnodray,
		struct Couple rcoups)
{
  int i,j,n,arbre_entier,bis;
  int nelmin,numseg,nlonv,num,nm,ne;
  int n0,n1,n2;
  int ntabel,na,nb,nc,icode,lco;
  double dim_boite[6];
  double xa,xb,xc,ya,yb,yc;
  double dm,xmin,ymin,xm,ym,dmin=2.e6;;
  double b1,b2,b3,size_min;
  double xp1,yp1,x1,y1,x2,y2;
  double vmin,interv;
  struct element *fa,*ff;
  struct node *arbre,*noeud;
  double **coordray,**coords;
  int nbeltmax_tree=200;



  for (n=0; n<rcoups.nelem; n++) rcoups.dist[n]=dmin;
  if (maillnodecoupl.nelem==0) return;


  coordray=maillnodray.coord;
  coords=maillnodecoupl.coord;


  boite2(2,maillnodecoupl.npoin,maillnodecoupl.coord,maillnodray.npoin,maillnodray.coord,dim_boite);


  arbre= (struct node *)malloc(sizeof(struct node));
  if (arbre==NULL) 
    {
      if (SYRTHES_LANG == FR)
	printf(" ERREUR corresRS2d : probleme d'allocation memoire\n");
      else if (SYRTHES_LANG == EN)
	printf(" ERREUR corresRS2d : Memory allocation problem\n");
      syrthes_exit(1);
    }
  build_quadtree_1d(arbre,maillnodecoupl.npoin,maillnodecoupl.nelem,maillnodecoupl.node,
		    maillnodecoupl.coord,
		    &size_min,dim_boite,nbeltmax_tree);

  /* recreer la tete de l'arbre */
  for (n=0; n<maillnodecoupl.nelem; n++)
    { 
      fa = (struct element *)malloc(sizeof(struct element));
      if (fa==NULL) 
	{
	  if (SYRTHES_LANG == FR)
	    printf(" ERREUR corresRS2d : probleme d'allocation memoire\n");
	  else if (SYRTHES_LANG == EN)
	    printf(" ERREUR corresRS2d : Memory allocation problem\n");
	  syrthes_exit(1);
	}
      fa->num = n;
      fa->suivant=NULL;
      if (n==0)
	{
	  arbre->lelement=fa;
	  ff=fa;
	}
      else
	{
	  ff->suivant=fa;
	  ff=fa;
	}
    }

/* pour chaque face de rayonnement... */
/*------------------------------------*/
  for (n=0; n<rcoups.nelem; n++) 
/*------------------------------------*/
    {
      dmin=1.e6; xmin=ymin=0;
      nelmin=0; 
      nm=1; xm=ym=0; dm=1.e6;

      ne=rcoups.numf[n];
      n0=maillnodray.node[0][ne]; n1=maillnodray.node[1][ne]; 
      xp1=0.5*(coordray[0][n0]+coordray[0][n1]);
      yp1=0.5*(coordray[1][n0]+coordray[1][n1]);
      
      noeud=arbre;
      find_node_2d(&noeud,xp1,yp1);
      
      fa=noeud->lelement;
      arbre_entier=0; bis=0;
      if (fa==NULL) {noeud=arbre;fa=noeud->lelement; arbre_entier=1;}
      recommence1 :  while (fa != NULL)
	{

	  /* affichage chris */
	  na=maillnodecoupl.node[0][fa->num];nb=maillnodecoupl.node[1][fa->num];
	  if (affich.correspSR) 
	    if (SYRTHES_LANG == FR)
	      printf("fa->num %d  : noeuds na=%d    nb=%d \n",fa->num,na,nb);
	    else if (SYRTHES_LANG == EN)
	      printf("fa->num %d  : nodes na=%d    nb=%d \n",fa->num,na,nb);

	  num=fa->num;
	  icode=0; lco=1; 
	  na=maillnodecoupl.node[0][num];nb=maillnodecoupl.node[1][num];
	  xa=coords[0][na]; ya=coords[1][na];
	  xb=coords[0][nb]; yb=coords[1][nb];
	  dptseg(xp1,yp1,xa,ya,xb,yb,&dm,&xm,&ym,&icode);
	  if (icode!=0) 
	    {
	      if (SYRTHES_LANG == FR)
		{
	      printf("\n !!! ERREUR corresRS2d : LA RECHERCHE DU CORRESPONDANT A ECHOUE\n");
	      printf("               Segment rayonnement : %d\n",ne);
	      printf("                                      %13.6e %13.6e\n",xp1,yp1);
	      printf("            dans le segment solide : %d\n",num);
	      printf("                                     %d %d\n",na,nb);
		}
	      else if (SYRTHES_LANG == EN)
		{
	      printf("\n !!! ERROR corresRS2d : THE CORRESPONDING SEARCH HAS FAILED\n");
	      printf("                 Radiation segment : %d\n",ne);
	      printf("                                      %13.6e %13.6e\n",xp1,yp1);
	      printf("              in the solid segment : %d\n",num);
	      printf("                                     %d %d\n",na,nb);
		}
	      syrthes_exit(1);
	    }	      
	  if (dm<dmin) {dmin=dm; xmin=xm; ymin=ym; nelmin=num;}
	  fa=fa->suivant;
	}

      /* on n'a pas trouve de correspondant, on fait une seconde tentative sur tout le domaine */
      if (dmin>99999.) 
	{
	  if (arbre_entier)
	    {
	      if (SYRTHES_LANG == FR)
		printf("       Pas de correspondant, segment rayonnement %d coord %f %f\n",ne,xp1,yp1);
	      else if (SYRTHES_LANG == EN)
		printf("       No corresponding point, radiative segment %d coord %f %f\n",ne,xp1,yp1);
	      syrthes_exit(1);
	    }
	  if (!arbre_entier)
	    {
	       if (SYRTHES_LANG == FR)
		 {
		   printf("       Pas de correspondant, segment rayonnement %d coord %f %f\n",ne,xp1,yp1);
		   printf("       On recommence la recherche sur l'ensemble du domaine...\n");
		 }
	       else if (SYRTHES_LANG == EN)
		 {
		   printf("       No corresponding point, radiative segment %d coord %f %f\n",ne,xp1,yp1);
		   printf("       The search is started again on the overall domain...\n");
		 }
	       noeud=arbre;fa=noeud->lelement; 
	       arbre_entier=1; bis=1;
	       goto recommence1;
	    }
	}

/*       if (bis) printf("       --> OK\n"); */

      rcoups.dist[n]=dmin;

      n1=maillnodecoupl.node[0][nelmin]; n2=maillnodecoupl.node[1][nelmin];
      x1=coords[0][n1];  y1=coords[1][n1]; 
      x2=coords[0][n2];  y2=coords[1][n2];

      bary2d (xmin,ymin,x1,y1,x2,y2,&b1,&b2);

      rcoups.bary[0][n]=b1; rcoups.bary[1][n]=b2; 
      /* rcoups.numcor[n]=nelmin; Attention  c'est donc un numero de corresp dans maillnodecoupl ! */
      /* attention, on a detourne nrefe pour y stocke le numero d'un elt nodecoupl dans nodeus */
      rcoups.numcor[n]=maillnodecoupl.nrefe[nelmin];  /* c'est donc un numero de corresp dans maillnodeus */

      if (affich.correspSR)
	{
	  if (SYRTHES_LANG == FR)
	    {
	      printf("\n   FACE RAYONNEMENT : %6d    coord : %13.6e %13.6e\n",n,xp1,yp1);
	      printf("   CORRESPONDANT SOLIDE :\n");
	      printf("            numero du segment solide : %6d\n",nelmin);
	      printf("                              noeuds : %6d  %6d\n",n1,n2);
	      printf("              coord du correspondant : %13.6e %13.6e\n",xmin,ymin);
	      printf("                   distance minimale :%22.16e\n",dmin);
	      printf("                coord barycentriques : %13.6e %13.6e\n",b1,b2);
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      printf("\n   RADIATION FACE : %6d    coord : %13.6e %13.6e\n",n,xp1,yp1);
	      printf("   CORRESPONDING SOLID POINT :\n");
	      printf("                   solid face number : %6d\n",nelmin);
	      printf("                               nodes : %6d  %6d\n",n1,n2);
	      printf("     corresponding point coordinates : %13.6e %13.6e\n",xmin,ymin);
	      printf("                    minimum distance : %22.16e\n",dmin);
	      printf("                  barycentric weight : %13.6e %13.6e\n",b1,b2);
	    }
	}
      
    }
  
  tuer_tree(arbre); free(arbre);

  if (affich.correspSR)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf(">>> corresRS2d : recapitulatif des tableaux de transfert\n");
	  printf(">>> pour chaque face de rayonnement numero du corresp solide et coeff bary\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf(">>> corresRS2d : transfer table summary\n");
	  printf(">>> for each radiative face, number of the corresponding point and barycentric weight\n");
	}
      for (n=0; n<rcoups.nelem; n++) 
	printf("Face=%d numcor=%d bary=%f %f\n",n,rcoups.numcor[n],rcoups.bary[0][n], rcoups.bary[1][n]);
    }

}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Transfert des informations de la conduction vers le rayt           |
  |======================================================================| */
void transSR(int ndim,int **nodeus,struct Couple rcoups,
	     double *tmps,double *tmpray)
{
  int i,n1,n2,n3,nc;

  if (ndim==2)
    for (i=0;i<rcoups.nelem;i++)
      {
	nc=rcoups.numcor[i]; 
	n1=nodeus[0][nc]; n2=nodeus[1][nc]; 
	tmpray[rcoups.numf[i]]=rcoups.bary[0][i]*tmps[n1] + rcoups.bary[1][i]*tmps[n2];
      }

  else
    for (i=0;i<rcoups.nelem;i++)
      {
	nc=rcoups.numcor[i]; 
	n1=nodeus[0][nc]; n2=nodeus[1][nc]; n3=nodeus[2][nc];
	tmpray[rcoups.numf[i]]= rcoups.bary[0][i]*tmps[n1] + rcoups.bary[1][i]*tmps[n2]
	                      + rcoups.bary[2][i]*tmps[n3];
      }
  
  if (affich.passageSR)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf(" -->  transSR : valeur de tmpray apres transfert de la temperature");
	  printf(" issue du solide\n");
	  for (i=0;i<rcoups.nelem;i++)
	    printf(" face couplee=%d  num_ds_temray=%d temray=%25.18e\n",
		   i,rcoups.numf[i],tmpray[rcoups.numf[i]]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf(" -->  transSR : tmpray value after temperature transfer");
	  printf(" from the solid\n");
	  for (i=0;i<rcoups.nelem;i++)
	    printf(" coupled face=%d  num_ds_temray=%d temray=%25.18e\n",
		   i,rcoups.numf[i],tmpray[rcoups.numf[i]]);
	}
      printf("\n");
    }
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Transfert des donnees entre rayonnement et conduction              |
  |======================================================================| */
void transRS(int ndmat,struct Couple scoupr,double *trayeq,double *erayeq)
{
  int i,j,nc;

  for (i=0;i<scoupr.nelem;i++)
    {
      nc=scoupr.numcor[i]; 
      for (j=0;j<ndmat;j++)
	{
	  scoupr.t[j][i]=trayeq[nc];
	  scoupr.h[j][i]=erayeq[nc];
	}
    }
  
  if (affich.passageSR)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf(" -->  transRS : valeur de scoupr apres transfert de la temperature");
	  printf(" issue du rayonnement (1er noeud de la face uniquement)\n");
	  for (i=0;i<scoupr.nelem;i++)
	    printf(" face couplee=%d  t=%25.18e h=%25.18e\n",
		   i,scoupr.t[0][i],scoupr.h[0][i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf(" -->  transRS : scoupr value after temperature transfer");
	  printf(" from radiation (face 1st node only)\n");
	  for (i=0;i<scoupr.nelem;i++)
	    printf(" coupled face=%d  t=%25.18e h=%25.18e\n",
		   i,scoupr.t[0][i],scoupr.h[0][i]);
	}
    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_corrSR(int ndim,struct Couple scoupr)
{
  FILE  *fcor;
  int i;
  int sr_nelem_new,sr_ndmat_new;
  int rs_nelem_new,rs_ndmat_new;

  if ((fcor=fopen(nomcorrr,"r")) == NULL)
    {
      if (SYRTHES_LANG == FR)
	printf("Impossible d'ouvrir le fichier de stockage des correspondants solide/rayonnement : %s\n",
	       nomcorrr);	
      else if (SYRTHES_LANG == EN)
	printf("Impossible to open the file where solid/radiation corresponding information are stored : %s\n",
	       nomcorrr);
      syrthes_exit(1) ;
    }


  fread(&sr_nelem_new,sizeof(int),1,fcor);
  fread(&sr_ndmat_new,sizeof(int),1,fcor);
  if (sr_nelem_new!=scoupr.nelem || sr_ndmat_new!=scoupr.ndmat)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n Le nombre d'elements solides couples au rayonnement du fichier\n");
	  printf("des correspondants solide/rayonnement n'est pas egal a celui\n");
	  printf("du present calcul\n");
	  printf("--> verifier le fichier des correspondants solide/rayonnement\n");
	  printf("--> recalculer les correpondants si les maillages ont change\n\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n The number of solid element coupled to radiation stored in the file\n");
	  printf("of the solid/fluid corresponding information is not equal\n");
	  printf("to the present calculation\n");
	  printf("--> check the solid/radiation corresponding information file\n");
	  printf("--> Please, recalculate the corresponding information if mesh have changed \n\n");
	}
      syrthes_exit(1);
    }

  fread(scoupr.numf,sizeof(int),scoupr.nelem,fcor);
  fread(scoupr.numcor,sizeof(int),scoupr.nelem,fcor);

  fclose(fcor);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |        Lecture                                                       |
  |======================================================================| */
void lire_corrRS(int ndim,struct Couple rcoups)
{
  FILE  *fcor;
  int rs_nelem_new,rs_ndmat_new;
  int i,ibid1,ibid2;

  if ((fcor=fopen(nomcorrr,"r")) == NULL)
    {
      if (SYRTHES_LANG == FR)
	printf("Impossible d'ouvrir le fichier de stockage des correspondants solide/rayonnement : %s\n",
	       nomcorrr);
      else if (SYRTHES_LANG == EN)
	printf("Impossible to open the file where solid/radiation corresponding information are stored : %s\n",
	       nomcorrr);
      syrthes_exit(1) ;
    }

  /* on passe le stockage de scoupr */
  fread(&ibid1,sizeof(int),1,fcor);
  fread(&ibid2,sizeof(int),1,fcor);
  for (i=0;i<ibid1;i++) fread(&ibid2,sizeof(int),1,fcor);
  for (i=0;i<ibid1;i++) fread(&ibid2,sizeof(int),1,fcor);


  fread(&rs_nelem_new,sizeof(int),1,fcor);
  fread(&rs_ndmat_new,sizeof(int),1,fcor);
  if (rs_nelem_new!=rcoups.nelem || rs_ndmat_new!=rcoups.ndmat)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n Le nombre d'elements rayonnement couples au solide du fichier\n");
	  printf("des correpondants solide/rayonnement n'est pas egal a celui\n");
	  printf("du present calcul\n");
	  printf("--> verifier le fichier des correspondants solide/rayonnement\n");
	  printf("--> recalculer les correpondants si les maillages ont change\n\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n The number of radiation element coupled to the solid in the file\n");
	  printf("of solid/radiation correspondant is not equal to \n");
	  printf("the present calculation\n");
	  printf("--> check the solid/radiation correspondant points information\n");
	  printf("--> Calculate again the corresponding points information is mesh have changed\n\n");
	}
      syrthes_exit(1);
    }

  fread(rcoups.numf,sizeof(int),rcoups.nelem,fcor);
  fread(rcoups.numcor,sizeof(int),rcoups.nelem,fcor);
  for (i=0;i<ndim;i++)
    fread(rcoups.bary[i],sizeof(double),rcoups.nelem,fcor);

   
  fclose(fcor);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |    ecrire_corrSR                                                     |
  |======================================================================| */
void ecrire_corrSR(int ndim,struct Couple scoupr)
{
  FILE  *fcor;
  int i;

  if ((fcor=fopen(nomcorrr,"w")) == NULL)
    {
      if (SYRTHES_LANG == FR)
	printf("Impossible d'ouvrir le fichier de stockage des correspondants solide/rayonnement : %s\n",
	       nomcorrr);	
      else if (SYRTHES_LANG == EN)
	printf("Impossible to open the solid/radiation corresponding points information : %s\n",
	       nomcorrr);
      syrthes_exit(1) ;
    }


  fwrite(&(scoupr.nelem),sizeof(int),1,fcor);
  fwrite(&(scoupr.ndmat),sizeof(int),1,fcor);
  fwrite(scoupr.numf,sizeof(int),scoupr.nelem,fcor);
  fwrite(scoupr.numcor,sizeof(int),scoupr.nelem,fcor);

  fclose(fcor);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |  Ecriture                                                            |
  |======================================================================| */
void ecrire_corrRS(int ndim,struct Couple rcoups)
{
  FILE  *fcor;
  int i;

  if ((fcor=fopen(nomcorrr,"a")) == NULL)
    {
      if (SYRTHES_LANG == FR)
	printf("Impossible d'ouvrir le fichier de stockage des correspondants solide/rayonnement : %s\n",
	       nomcorrr);	
      else if (SYRTHES_LANG == EN)
     	printf("Impossible to open the solid/radiation corresponding information file : %s\n",
	       nomcorrr);
      syrthes_exit(1) ;
    }

  fwrite(&(rcoups.nelem),sizeof(int),1,fcor);
  fwrite(&(rcoups.ndmat),sizeof(int),1,fcor);
  fwrite(rcoups.numf,sizeof(int),rcoups.nelem,fcor);
  fwrite(rcoups.numcor,sizeof(int),rcoups.nelem,fcor);
  for (i=0;i<ndim;i++)
    fwrite(rcoups.bary[i],sizeof(double),rcoups.nelem,fcor);
    
  fclose(fcor);
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |  Histogramme distance des correspondants                             |
  |======================================================================| */
void histog_corresp(int nb, double *distance,struct SDparall_ray sdparall_ray)
{
  int nbhisto=10;
  int i,n,nbtot,nbi;
  double dist_moy,dist_var,dist_max,dist_min,interv;
  int *thisto;
  double *disti;

  if (sdparall_ray.nparts==1)
    {
      for (i=0,dist_moy=dist_var=dist_max=0; i<nb; i++)
	{
	  if (distance[i]>dist_max) dist_max=distance[i];
	  dist_moy+=distance[i];
	  dist_var+=distance[i]*distance[i];
	}
      nbtot=nb;
    }
#ifdef _SYRTHES_MPI_
  else
    {
      dist_moy=dist_var=dist_max=0; nbtot=0;
      if (sdparall_ray.rang==0)
	{
	  for (i=0; i<nb; i++)
	    {
	      if (distance[i]>dist_max) dist_max=distance[i];
	      dist_moy+=distance[i];
	      dist_var+=distance[i]*distance[i];
	      nbtot++;
	    }

	  for (n=1;n<sdparall_ray.nparts;n++)
	    {
	      MPI_Recv(&nbi,1,MPI_INT,n,0,sdparall_ray.syrthes_comm_world,&status);
	      if (nbi>0){
		disti=(double*)malloc(nbi*sizeof(double));
		MPI_Recv(disti,nbi,MPI_DOUBLE,n,0,sdparall_ray.syrthes_comm_world,&status);
		for (i=0; i<nbi; i++)
		  {
		    if (disti[i]>dist_max) dist_max=disti[i];
		    dist_moy+=disti[i];
		    dist_var+=disti[i]*disti[i];
		    nbtot++;
		  }
		free(disti);
	      }
	    }
	}
      else
	{
	  MPI_Send(&nb,1,MPI_INT,0,0,sdparall_ray.syrthes_comm_world);
	  if (nb>0) MPI_Send(distance,nb,MPI_DOUBLE,0,0,sdparall_ray.syrthes_comm_world);
	}
    }
#endif
  
  dist_moy /= nbtot; 
  dist_var = dist_var/nbtot-(dist_moy*dist_moy);

  if (sdparall_ray.nparts==1 ||sdparall_ray.rang==0 )
    if (SYRTHES_LANG == FR)
      {
	printf("\n              Distance de correspondance\n"); 
	printf("                               - maximale : %12.5e\n",dist_max); 
	printf("                               - moyenne  : %12.5e\n",dist_moy); 
	printf("                               - variance : %12.5e\n",dist_var); 
      }
    else if (SYRTHES_LANG == EN)
      {
	printf("\n              Corresponding points distance\n"); 
	printf("                               - maximum : %12.5e\n",dist_max); 
	printf("                               - average : %12.5e\n",dist_moy); 
	printf("                               - rms     : %12.5e\n",dist_var); 
      }

  /* tout ca, ce n'est pas fait en // Pour le moment actif que en sequentiel */
  if (sdparall_ray.nparts==1)
    {
  dist_min=0;  interv=dist_max/(nbhisto);
  thisto=(int *)malloc(nbhisto*sizeof(int));  verif_alloue_int1d("histog_corresp",thisto);

  histog (nb,distance,dist_min,dist_max,thisto,nbhisto);

  if (sdparall_ray.nparts==1 ||sdparall_ray.rang==0 )
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("              Histogramme de la distance (nombre de points=%5d)\n",nb);
	  printf("                       Intervalle                  nb_points    Pourcentage\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("              corresponding distance histogram (points number=%5d)\n",nb);
	  printf("                       Range             nb_points    Percentage\n");
	}
      
      for (i=0;i<nbhisto;i++) 
	{printf("                 %12.5e - %12.5e        %5d         %5.2f\n",
		i*interv,(i+1)*interv,thisto[i],(double)thisto[i]/nb);
	}
    }
  free(thisto);
    }

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Transfert des donnees entre rayonnement et conduction en //        |
  |======================================================================| */
void transRS_parall(int ndmat,struct Couple scoupr,
		    double *trayeq,double *erayeq,
		    struct SDparall_ray sdparall_ray)
{
#ifdef _SYRTHES_MPI_
  int i,j,n,nb;
  double *dtravt,*dtrave,*drecvt,*drecve;
  int static toto=0;

  toto++;

  dtravt=(double*)malloc(sdparall_ray.nelrayloc_max*sizeof(double));
  dtrave=(double*)malloc(sdparall_ray.nelrayloc_max*sizeof(double));
  for (i=0;i<sdparall_ray.nelrayloc_max; i++) dtravt[i]=dtrave[i]=0;

  drecvt=(double*)malloc(sdparall_ray.nelrayloc_max*sizeof(double));
  drecve=(double*)malloc(sdparall_ray.nelrayloc_max*sizeof(double));


  /* si le proc fait du ray , on remplit les trav - sinon, il y a ce qu'il y a dedans ! */
  if (sdparall_ray.rangray>-1)
    {
      for (i=0;i<sdparall_ray.nelrayloc[sdparall_ray.rangray];i++)
	{
	  dtravt[i]=trayeq[i];
	  dtrave[i]=erayeq[i];
/* 	  printf("==transRS_parall dt=%d p=%d on envoie dtravt[%d]=%f\n",toto,sdparall_ray.rangray,i,dtravt[i]); */
	}
    }

  for (n=0;n<sdparall_ray.nparts;n++)
    {
      if (n!=sdparall_ray.rang)
	{
	  MPI_Sendrecv(dtravt,sdparall_ray.nelrayloc_max,MPI_DOUBLE,n,0,
		       drecvt,sdparall_ray.nelrayloc_max,MPI_DOUBLE,n,0,
		       sdparall_ray.syrthes_comm_world,&status);
	  MPI_Sendrecv(dtrave,sdparall_ray.nelrayloc_max,MPI_DOUBLE,n,0,
		       drecve,sdparall_ray.nelrayloc_max,MPI_DOUBLE,n,0,
		       sdparall_ray.syrthes_comm_world,&status);
	}
      else if (sdparall_ray.rangray>-1) /* si on est sur le meme proc et qu'on fait du rayt */
	{
	  for (i=0;i<sdparall_ray.nelrayloc[sdparall_ray.rangray];i++)
	    {
	      drecvt[i]=dtravt[i];
	      drecve[i]=dtrave[i];
	    }
	}

      for (i=0;i<scoupr.nelem;i++)
	if(sdparall_ray.ieledeb[n]<=scoupr.numcor[i] && scoupr.numcor[i]<sdparall_ray.ieledeb[n+1])
	  {
	    scoupr.t[0][i]=drecvt[scoupr.numcor[i]-sdparall_ray.ieledeb[n]];
	    scoupr.h[0][i]=drecve[scoupr.numcor[i]-sdparall_ray.ieledeb[n]];
/* 	    printf("++transRSparall>>>>>> dt=%d p=%d reception=%d numcor=%d Trecue[%d]=%f\n", */
/* 		   toto,sdparall_ray.rang,n,scoupr.numcor[i]-sdparall_ray.ieledeb[n], */
/* 		   i,scoupr.t[0][i]); */
	  }	 
    }


  for (i=0;i<scoupr.nelem;i++)
    for (j=1;j<ndmat;j++)
      {
	scoupr.t[j][i]=scoupr.t[0][i];
	scoupr.h[j][i]=scoupr.h[0][i];
      }

  free(dtravt); free(dtrave);
  free(drecvt); free(drecve);

  if (affich.passageSR)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf(" -->  transRS_parall : valeur de scoupr apres transfert de la temperature");
	  printf(" issue du rayonnement (1er noeud de la face uniquement)\n");
	  for (i=0;i<scoupr.nelem;i++)
	    printf(" rang %d -- face couplee=%d  t=%25.18e h=%25.18e\n",
		   sdparall_ray.rang,i,scoupr.t[0][i],scoupr.h[0][i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf(" -->  transRS_parall : scoupr value after the temperature transfer");
	  printf(" from the radiation (only from the face first node)\n");
	  for (i=0;i<scoupr.nelem;i++)
	    printf(" rank %d -- coupled face=%d  t=%25.18e h=%25.18e\n",
		   sdparall_ray.rang,i,scoupr.t[0][i],scoupr.h[0][i]);
	}
    }

#endif
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Calcul des correspondances rayt/conduction en parallele            |
  |======================================================================| */
void extract_nodecoupl_parall(struct Maillage maillnodes,
			      struct MaillageCL maillnodeus, struct Couple scoupr,
			      struct Maillage *maillnodecoupl,
			      struct SDparall_ray sdparall_ray)
{
#ifdef _SYRTHES_MPI_
  int i,j,ne,n,nb,np,tag;
  int *itrav;

  if (sdparall_ray.rang==0)
    {
      if (SYRTHES_LANG == FR)
	printf("\n *** Extraction du maillage solide local couple au rayonnnement \n"); 
      else if (SYRTHES_LANG == EN)
	printf("\n *** Extraction of the loacl solid mesh for coupling with radiation \n"); 
    }

  /* extraction */
  /* ========== */

  /* creation d'un maillage de bord solide couple au rayt */
  /* ---------------------------------------------------- */
  maillnodecoupl->nelem=scoupr.nelem;
  maillnodecoupl->ndmat=scoupr.ndmat;
  maillnodecoupl->ndim=maillnodes.ndim;

  maillnodecoupl->node=(int**)malloc(maillnodecoupl->ndmat*sizeof(int*));
  for (i=0;i<maillnodecoupl->ndmat;i++) 
    maillnodecoupl->node[i]=(int*)malloc(maillnodecoupl->nelem*sizeof(int));
  verif_alloue_int2d(maillnodecoupl->ndmat,"corresSR",maillnodecoupl->node);
  
  /* attention, on a detourne nrefe pour y stocke le numero d'un elt nodecoupl dans nodeus */
  maillnodecoupl->nrefe=(int*)malloc(maillnodecoupl->nelem*sizeof(int));
  verif_alloue_int1d("corresRS",maillnodecoupl->nrefe);
  
  for (i=0;i<maillnodecoupl->nelem;i++)
    {
      for (j=0;j<maillnodecoupl->ndmat;j++)
	{
	  ne=scoupr.numf[i];
	  maillnodecoupl->node[j][i]=maillnodeus.node[j][ne];
	  /* attention : on n'a pas les coordonnees !!! */	 
	}
      maillnodecoupl->nrefe[i]=ne;
    }
  

  /* il faut passer les numero de noeud en num locale pour creer la table de coordonnees */
  itrav=(int*)malloc(maillnodes.npoin*sizeof(int));
  for (i=0;i<maillnodes.npoin;i++) itrav[i]=-1;
  
  for (nb=i=0;i<maillnodecoupl->nelem;i++)
    for (j=0;j<maillnodecoupl->ndmat;j++)
      {
	np=maillnodecoupl->node[j][i];
	if (itrav[np]==-1) 
	  {itrav[np]=nb; nb++;}
      }
  
  maillnodecoupl->npoin=nb;
  
  maillnodecoupl->coord=(double**)malloc(maillnodecoupl->ndim*sizeof(double*));
  for (i=0;i<maillnodecoupl->ndim;i++) 
    maillnodecoupl->coord[i]=(double*)malloc(maillnodecoupl->npoin*sizeof(double));
  verif_alloue_double2d(maillnodecoupl->ndim,"corresSR",maillnodecoupl->coord);
  perfo.mem_ray+=(maillnodecoupl->ndim+0.5)*maillnodecoupl->npoin*sizeof(double);
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  
  /* remplissage des coord */
  for (i=0;i<maillnodes.npoin;i++)
    if (itrav[i]>-1)
      for (j=0;j<maillnodecoupl->ndim;j++) maillnodecoupl->coord[j][itrav[i]]=maillnodes.coord[j][i];
  
  /* passage effectif a la num loc */
  for (nb=i=0;i<maillnodecoupl->nelem;i++) 
    for (j=0;j<maillnodecoupl->ndmat;j++)
      maillnodecoupl->node[j][i] = itrav[maillnodecoupl->node[j][i]];

  free(itrav);

#endif
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Calcul des correspondances rayt/conduction en parallele            |
  |======================================================================| */
void alloue_linkcondray_parall(int ndim,struct SDparall_ray *sdparall_ray)
{
#ifdef _SYRTHES_MPI_
  int n,i,j;


  /* nbre de valeurs a envoyer a chaque proc conduction */
  /* -------------------------------------------------- */
  sdparall_ray->nbval_a_envoyer=(int*)malloc(sdparall_ray->nparts*sizeof(int));
  sdparall_ray->num_a_envoyer=(int**)malloc(sdparall_ray->nparts*sizeof(int*));

  /* pour l'envoi a chaque proc de conduction du nombre de facettes pour lesquelles il devra envoyer */
  /* la temperature et pour l'envoi des coeff bary qui vont avec                                     */
  sdparall_ray->bary_a_envoyer=(double***)malloc(sdparall_ray->nparts*sizeof(double**));
  for (n=0;n<sdparall_ray->nparts;n++) 
    {
      sdparall_ray->bary_a_envoyer[n]=(double**)malloc(ndim*sizeof(double*));
      for (i=0;i<ndim;i++) sdparall_ray->bary_a_envoyer[n][i]=NULL;
    }


  /* nbre de valeurs a recevoir de chaque proc conduction */
  /* ---------------------------------------------------- */

  /* pour chaque proc, comptage des facettes recevant des infos de ce proc */
  sdparall_ray->nbval_a_recevoir=(int*)malloc(sdparall_ray->nparts*sizeof(int));
  for (i=0;i<sdparall_ray->nparts;i++) sdparall_ray->nbval_a_recevoir[i]=0;


  sdparall_ray->indice_a_recevoir=(int*)malloc(sdparall_ray->nparts*sizeof(int)); /* indice de depart pour chaque proc */
  sdparall_ray->indice_a_recevoir[0]=0;



  

#endif
}




/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Calcul des correspondances rayt/conduction en parallele            |
  |   n'est appele que si le proc fait du rayt                           |
  |======================================================================| */
void corresRS_parall(struct Maillage maillnodes,struct Maillage maillnodray,
		     struct Maillage maillnodecoupl,
		     struct Couple scoupr,struct Couple rcoups,
		     struct SDparall_ray *sdparall_ray)
{
#ifdef _SYRTHES_MPI_
  int i,j,ne,n,nb,np,nbechang,numproc,recherche;
  int *itravnb,*tabnbelecpl;
  struct Maillage maillrecu;
  struct Couple rcoups_trav;


  if (sdparall_ray->rang==0)
    {
      if (SYRTHES_LANG == FR)
	printf("\n *** Calcul des correspondants rayonnement/solide \n"); 
      else if (SYRTHES_LANG == EN)
	printf("\n *** Cadiation/solid corresponding points calculation \n"); 
    }

  /* si le proc fait du rayt on alloue les tableaux de couplage */

  if(sdparall_ray->rangray>-1)
    {
      
      /* allocation du tableau de stockage du numero du proc du corresp */ 
      sdparall_ray->rcoups_proc=(int*)malloc(rcoups.nelem*sizeof(int));
      
      /* initialisation de rcoups_trav */
      /* ----------------------------- */
      rcoups_trav.nelem=rcoups.nelem;
      rcoups_trav.ndmat=rcoups.ndmat;
      rcoups_trav.numf=(int*)malloc(rcoups.nelem*sizeof(int));
      rcoups_trav.numcor=(int*)malloc(rcoups.nelem*sizeof(int));
      rcoups_trav.dist=(double*)malloc(rcoups.nelem*sizeof(double));
      rcoups_trav.bary=(double**)malloc(maillnodray.ndim*sizeof(double*));
      for (i=0;i<maillnodray.ndim;i++) rcoups_trav.bary[i]=(double*)malloc(rcoups.nelem*sizeof(double));
      for (i=0;i<rcoups.nelem;i++) 
	for (j=0;j<maillnodray.ndim;j++) rcoups.bary[j][i]=0;
      verif_alloue_int1d("corresRS_parall",rcoups.numf);
      verif_alloue_int1d("corresRS_parall",rcoups.numcor);
      verif_alloue_double1d("corresRS_parall",rcoups.dist);
      verif_alloue_double2d(maillnodray.ndim,"corresRS_parall",rcoups.bary);
      
      for (i=0;i<rcoups_trav.nelem;i++)
	{
	  rcoups_trav.numf[i]=rcoups.numf[i];
	  rcoups_trav.numcor[i]=rcoups.numcor[i];
	  rcoups_trav.dist[i]=1.e6;
	}
    }



  /* envoi */
  /* ===== */
  /* rayt ou non, il faut envoyer le maillage de bord solide  a tous les proc de rayt */
  /* mais seulement s'il existe, cad si maillnecoupl.nelem!=0  */

  /* tabnbelecpl = tab des nbres d'elts solides couples au rayt de tous les proc solides */
  tabnbelecpl=(int*)malloc(sdparall_ray->nparts*sizeof(int));
  MPI_Allgather(&(maillnodecoupl.nelem),1,MPI_INT,
		tabnbelecpl,1,MPI_INT,sdparall_ray->syrthes_comm_world);
  
  for (n=0;n<sdparall_ray->npartsray;n++)
    {
      if (n != sdparall_ray->rangray) /* envoi de nodecoupl au proc n */
	{
	  if (maillnodecoupl.nelem>0)
	    {
	      MPI_Send(&(maillnodecoupl.ndmat),1,MPI_INT,n,0,sdparall_ray->syrthes_comm_world);
	      MPI_Send(&(maillnodecoupl.npoin),1,MPI_INT,n,0,sdparall_ray->syrthes_comm_world);
	      MPI_Send(maillnodecoupl.node[0],maillnodecoupl.nelem,MPI_INT,n,0,sdparall_ray->syrthes_comm_world);
	      MPI_Send(maillnodecoupl.node[1],maillnodecoupl.nelem,MPI_INT,n,0,sdparall_ray->syrthes_comm_world);
	      MPI_Send(maillnodecoupl.coord[0],maillnodecoupl.npoin,MPI_DOUBLE,n,0,sdparall_ray->syrthes_comm_world);
	      MPI_Send(maillnodecoupl.coord[1],maillnodecoupl.npoin,MPI_DOUBLE,n,0,sdparall_ray->syrthes_comm_world);
	      if(maillnodecoupl.ndim==3)
		{
		  MPI_Send(maillnodecoupl.node[2],maillnodecoupl.nelem,MPI_INT,n,0,sdparall_ray->syrthes_comm_world);
		  MPI_Send(maillnodecoupl.coord[2],maillnodecoupl.npoin,MPI_DOUBLE,n,0,sdparall_ray->syrthes_comm_world);
		}
	      MPI_Send(maillnodecoupl.nrefe,maillnodecoupl.nelem,MPI_INT,n,0,sdparall_ray->syrthes_comm_world);
	    }
	}
      else
	{
	  /* recherche des correspondants */
	  if (maillnodes.ndim==2)
	    corresRS2d(maillnodecoupl,maillnodray,rcoups_trav);
	  else
	    corresRS3d(maillnodecoupl,maillnodray,rcoups_trav);

	  for (i=0;i<rcoups.nelem;i++)
	    if (rcoups_trav.dist[i]<rcoups.dist[i])
	      {
		rcoups.dist[i]=rcoups_trav.dist[i];
		rcoups.numcor[i]=rcoups_trav.numcor[i];
		rcoups.bary[0][i]=rcoups_trav.bary[0][i];
		rcoups.bary[1][i]=rcoups_trav.bary[1][i];
		if (maillnodray.ndim==3) rcoups.bary[2][i]=rcoups_trav.bary[2][i];
		sdparall_ray->rcoups_proc[i]=n;
	      }


	  for (j=0;j<sdparall_ray->nparts;j++) /* reception de nodecoupl de j et calcul des corresp */
	    {
	      if (j != sdparall_ray->rangray) 
		{
		  /* recevoir le maillage maillrecu (ie maillnodecoupl) (s'il est non nul) */
		  maillrecu.ndim=maillnodecoupl.ndim;
		  maillrecu.nelem=tabnbelecpl[j];
		  if (tabnbelecpl[j]>0)
		    {
		      MPI_Recv(&(maillrecu.ndmat),1,MPI_INT,j,0,sdparall_ray->syrthes_comm_world,&status);
		      MPI_Recv(&(maillrecu.npoin),1,MPI_INT,j,0,sdparall_ray->syrthes_comm_world,&status);
		      maillrecu.node=(int**)malloc(maillrecu.ndmat*sizeof(int*));
		      for (i=0;i<maillrecu.ndmat;i++) 
			maillrecu.node[i]=(int*)malloc(maillrecu.nelem*sizeof(int));
		      verif_alloue_int2d(maillrecu.ndmat,"corresSR",maillrecu.node);
		      maillrecu.coord=(double**)malloc(maillrecu.ndim*sizeof(double*));
		      for (i=0;i<maillrecu.ndim;i++) 
			maillrecu.coord[i]=(double*)malloc(maillrecu.npoin*sizeof(double));
		      verif_alloue_double2d(maillrecu.ndim,"corresSR",maillrecu.coord);
		      /* attention, on a detourne nrefe pour y stocke le numero d'un elt recu dans nodeus */
		      maillrecu.nrefe=(int*)malloc(maillrecu.nelem*sizeof(int));
		      verif_alloue_int1d("corresRS",maillrecu.nrefe);
		      MPI_Recv(maillrecu.node[0],maillrecu.nelem,MPI_INT,j,0,sdparall_ray->syrthes_comm_world,&status);
		      MPI_Recv(maillrecu.node[1],maillrecu.nelem,MPI_INT,j,0,sdparall_ray->syrthes_comm_world,&status);
		      MPI_Recv(maillrecu.coord[0],maillrecu.npoin,MPI_DOUBLE,j,0,sdparall_ray->syrthes_comm_world,&status);
		      MPI_Recv(maillrecu.coord[1],maillrecu.npoin,MPI_DOUBLE,j,0,sdparall_ray->syrthes_comm_world,&status);
		      if(maillnodecoupl.ndim==3) 
			{
			  MPI_Recv(maillrecu.node[2],maillrecu.nelem,MPI_INT,j,0,sdparall_ray->syrthes_comm_world,&status);
			  MPI_Recv(maillrecu.coord[2],maillrecu.npoin,MPI_DOUBLE,j,0,sdparall_ray->syrthes_comm_world,&status);
			}
		      MPI_Recv(maillrecu.nrefe,maillrecu.nelem,MPI_INT,j,0,sdparall_ray->syrthes_comm_world,&status);
		      
		      /* recherche des correspondants */
		      if (maillnodes.ndim==2)
			corresRS2d(maillrecu,maillnodray,rcoups_trav);
		      else
			corresRS3d(maillrecu,maillnodray,rcoups_trav);
		      
		      /* destruction du maillage recu */
		      for (i=0;i<maillrecu.ndmat;i++) free(maillrecu.node[i]);
		      free (maillrecu.node);
		      free (maillrecu.nrefe);
		      for (i=0;i<maillrecu.ndim;i++) free(maillrecu.coord[i]);
		      free (maillrecu.coord);
		  
		      /* recherche du min sur l'ensemble des proc */
		      for (i=0;i<rcoups.nelem;i++)
			if (rcoups_trav.dist[i]<rcoups.dist[i])
			  {
			    rcoups.dist[i]=rcoups_trav.dist[i];
			    rcoups.numcor[i]=rcoups_trav.numcor[i];
			    rcoups.bary[0][i]=rcoups_trav.bary[0][i];
			    rcoups.bary[1][i]=rcoups_trav.bary[1][i];
			    if (maillnodray.ndim==3) rcoups.bary[2][i]=rcoups_trav.bary[2][i];
			    sdparall_ray->rcoups_proc[i]=j;
			  }
		    }

	      
		} /* fin du if j!=rang */
	      
	    } /* fin de la boucle sur j */

	} /* fin de else n!= rang-courant */

    } /* fin de la boucle sur n */


  
  /* histogramme des distances de correspondance */
  histog_corresp(rcoups.nelem,rcoups.dist,*sdparall_ray);

  if (affich.correspSR)
     {
       printf(" *** Impression des correspondants (calcul parallele)\n");
       for (i=0;i<rcoups.nelem;i++)
	 printf("  proc=%d i=%d corresp=%d sur proc=%d\n",sdparall_ray->rangray,i,rcoups.numcor[i],sdparall_ray->rcoups_proc[i]);
     }


  /* nbre de valeurs a recevoir de chaque proc conduction */
  /* ---------------------------------------------------- */
  /* pour chaque proc, comptage des facettes recevant des infos de ce proc */
  for (i=0;i<rcoups.nelem;i++)
    sdparall_ray->nbval_a_recevoir[sdparall_ray->rcoups_proc[i]]++;


  /* on classe les facettes de ray couplees par proc d'ou elles recoivent l'info */
  /* --------------------------------------------------------------------------- */

  sdparall_ray->indice_a_recevoir[0]=0;
  for (i=1;i<sdparall_ray->nparts;i++) 
    sdparall_ray->indice_a_recevoir[i]=sdparall_ray->indice_a_recevoir[i-1]+sdparall_ray->nbval_a_recevoir[i-1];


  itravnb=(int*)malloc(sdparall_ray->nparts*sizeof(int));
  for (i=0;i<sdparall_ray->nparts;i++) itravnb[i]=0;


  for (i=0;i<rcoups.nelem;i++)
    { 
      numproc=sdparall_ray->rcoups_proc[i];
      j=sdparall_ray->indice_a_recevoir[numproc];
      rcoups_trav.numf[j+itravnb[numproc]]=rcoups.numf[i];
      rcoups_trav.numcor[j+itravnb[numproc]]=rcoups.numcor[i];
      rcoups_trav.bary[0][j+itravnb[numproc]]=rcoups.bary[0][i];
      rcoups_trav.bary[1][j+itravnb[numproc]]=rcoups.bary[1][i];
      if (maillnodray.ndim==3) rcoups_trav.bary[2][j+itravnb[numproc]]=rcoups.bary[2][i];
      itravnb[numproc]++;
    }

  /* on recopie la lite triee dans la structure d'origine */
  for (i=0;i<rcoups.nelem;i++)
    { 
      rcoups.numf[i]=rcoups_trav.numf[i];
      rcoups.numcor[i]=rcoups_trav.numcor[i];
      rcoups.bary[0][i]=rcoups_trav.bary[0][i];
      rcoups.bary[1][i]=rcoups_trav.bary[1][i];
      if (maillnodray.ndim==3) rcoups.bary[2][i]=rcoups_trav.bary[2][i];
    }
  

  /* destruction de la structure Couple temporaire */
  if(sdparall_ray->rangray>-1)
    {
      free(rcoups_trav.numf);
      free(rcoups_trav.numcor);
      free(rcoups_trav.dist);
      for (i=0;i<maillnodray.ndim;i++)free(rcoups_trav.bary[i]);
      free(rcoups_trav.bary);
    }



  /* on peut aussi detruire rcoups qui ne sert plus en parallelisme AH BON ???*/
/*   free(rcoups.numf); */
/*   free(rcoups.numcor); */
/*   free(rcoups.dist); */
/*   for (i=0;i<maillnodray.ndim;i++)free(rcoups.bary[i]); */
/*   free(rcoups.bary); */


#endif
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |   Etablissement des listes d'echange entre conduction et rayonnement |
  |======================================================================| */
void link_condray_parall(int ndim, struct Couple rcoups,struct SDparall_ray *sdparall_ray)
{
#ifdef _SYRTHES_MPI_
  int i,j,ne,n,nb,np,nbechang,numproc;
  int *itrav;
  double *dtrav1,*dtrav2,*dtrav3;


  
  itrav=(int*)malloc(sdparall_ray->nelrayloc_max*sizeof(int));
  dtrav1=(double*)malloc(sdparall_ray->nelrayloc_max*sizeof(double));
  dtrav2=(double*)malloc(sdparall_ray->nelrayloc_max*sizeof(double));
  if (ndim==3) dtrav3=(double*)malloc(sdparall_ray->nelrayloc_max*sizeof(double));
  
  /* ???????????????????? attention : voir comment c'est gere quand il y a 0 valeurs a tranferer ??????????????? */ 
  for (n=0;n<sdparall_ray->nparts;n++)
    {
      if (n!=sdparall_ray->rang)
	{ 
	  nbechang=sdparall_ray->nbval_a_recevoir[n];
	  /* ex : le proc courant 0 envoie a 1 le nombre de valeur dont il a besoin (de 1) */
          /*      en retour, 1 envoie a 0 le nombre de valeurs qu'il faudra que 0 envoie a 1 */
	  MPI_Sendrecv_replace(&nbechang,1,MPI_INT,n,0,n,0,sdparall_ray->syrthes_comm_world,&status);
	  sdparall_ray->nbval_a_envoyer[n]=nbechang;
	  
	  /* on peut maintenant allouer les tableaux de valeurs a envoyer */
	  if (nbechang>0)
	    {
	      for (i=0;i<ndim;i++) sdparall_ray->bary_a_envoyer[n][i]=(double*)malloc(nbechang*sizeof(double));
	      sdparall_ray->num_a_envoyer[n]=(int*)malloc(nbechang*sizeof(int));
	    }

	  /* maintenant, on echange la liste des numeros de face */
	  /* la taille de ce que l'on envoie peut etre differente de ce que l'on recoit */
          /* du coup, on passe par un tab de trav intermediaire */
	  for (i=0;i<sdparall_ray->nbval_a_recevoir[n];i++)
	    {
	      itrav[i]=rcoups.numcor[sdparall_ray->indice_a_recevoir[n]+i];
	      dtrav1[i]=rcoups.bary[0][sdparall_ray->indice_a_recevoir[n]+i];
	      dtrav2[i]=rcoups.bary[1][sdparall_ray->indice_a_recevoir[n]+i];
	      if (ndim==3) dtrav3[i]=rcoups.bary[2][sdparall_ray->indice_a_recevoir[n]+i];
	    }
	  MPI_Sendrecv_replace(itrav,sdparall_ray->nelrayloc_max,MPI_INT,n,0,n,0,sdparall_ray->syrthes_comm_world,&status);
	  MPI_Sendrecv_replace(dtrav1,sdparall_ray->nelrayloc_max,MPI_DOUBLE,n,0,n,0,sdparall_ray->syrthes_comm_world,&status);
	  MPI_Sendrecv_replace(dtrav2,sdparall_ray->nelrayloc_max,MPI_DOUBLE,n,0,n,0,sdparall_ray->syrthes_comm_world,&status);
	  if (ndim==3) MPI_Sendrecv_replace(dtrav3,sdparall_ray->nelrayloc_max,MPI_DOUBLE,n,0,n,0,sdparall_ray->syrthes_comm_world,&status);

	  for (i=0;i<sdparall_ray->nbval_a_envoyer[n];i++)
	    {
	      sdparall_ray->num_a_envoyer[n][i]=itrav[i];
	      sdparall_ray->bary_a_envoyer[n][0][i]=dtrav1[i];
	      sdparall_ray->bary_a_envoyer[n][1][i]=dtrav2[i];
	      if (ndim==3)  sdparall_ray->bary_a_envoyer[n][2][i]=dtrav3[i];
	    }
	  
	}
      else if (n==sdparall_ray->rang && sdparall_ray->rangray>-1 )
	{
	  nbechang=sdparall_ray->nbval_a_envoyer[n]=sdparall_ray->nbval_a_recevoir[n];
	  if (nbechang>0)
	    {
	      for (i=0;i<ndim;i++) sdparall_ray->bary_a_envoyer[n][i]=(double*)malloc(nbechang*sizeof(double));
	      sdparall_ray->num_a_envoyer[n]=(int*)malloc(nbechang*sizeof(int));
	      for (i=0;i<sdparall_ray->nbval_a_envoyer[n];i++)
		{
		  sdparall_ray->num_a_envoyer[n][i]=rcoups.numcor[sdparall_ray->indice_a_recevoir[n]+i];
		  sdparall_ray->bary_a_envoyer[n][0][i]=rcoups.bary[0][sdparall_ray->indice_a_recevoir[n]+i];
		  sdparall_ray->bary_a_envoyer[n][1][i]=rcoups.bary[1][sdparall_ray->indice_a_recevoir[n]+i];
		  if (ndim==3)  sdparall_ray->bary_a_envoyer[n][2][i]=rcoups.bary[2][sdparall_ray->indice_a_recevoir[n]+i];
		}
	    } /* fin : if nbechang>0 */
	}
    }

  free(itrav); free(dtrav1); free(dtrav2); 
  if (ndim==3) free(dtrav3);


#endif
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP,   C. PENIGUEL                                    |
  |======================================================================|
  |  transfert des informations de la conduction vers le rayt en //      |
  |======================================================================| */
void transSR_parall(int ndim,int **nodeus,struct Couple rcoups,
		    double *tmps,double *tmpray,
		    struct SDparall_ray sdparall_ray)
{
#ifdef _SYRTHES_MPI_
  int i,n1,n2,n3,n,ne;
  double *dtrav,b1,b2,b3;

  dtrav=(double*)malloc(sdparall_ray.nelrayloc_max*sizeof(double));


  for (n=0;n<sdparall_ray.npartsray;n++) /* on ne va envoyer tmps qu'aux rgs de ray */
    {
      if (ndim==2)
	for (i=0;i<sdparall_ray.nbval_a_envoyer[n];i++)
	  {
	    ne=sdparall_ray.num_a_envoyer[n][i];
	    n1=nodeus[0][ne]; n2=nodeus[1][ne]; 
	    b1=sdparall_ray.bary_a_envoyer[n][0][i]; b2=sdparall_ray.bary_a_envoyer[n][1][i];
	    dtrav[i]=b1*tmps[n1]+b2*tmps[n2];
	  }
      else
	for (i=0;i<sdparall_ray.nbval_a_envoyer[n];i++)
	  {
	    ne=sdparall_ray.num_a_envoyer[n][i];
	    n1=nodeus[0][ne]; n2=nodeus[1][ne]; n3=nodeus[2][ne]; 
	    b1=sdparall_ray.bary_a_envoyer[n][0][i]; 
	    b2=sdparall_ray.bary_a_envoyer[n][1][i];
	    b3=sdparall_ray.bary_a_envoyer[n][2][i];
	    dtrav[i]=b1*tmps[n1]+b2*tmps[n2]+b3*tmps[n3];
	  }


      if (n==sdparall_ray.rangray) /* on est le proc courant (et on fait du rayt) */
	{
	  for (i=0;i<sdparall_ray.nbval_a_recevoir[n];i++)
	    tmpray[rcoups.numf[sdparall_ray.indice_a_recevoir[n]+i]]=dtrav[i];
	}
      else if (sdparall_ray.rangray>-1)  /* si on est  sur un proc qui fait du ray */
	{
	  MPI_Sendrecv_replace(dtrav,sdparall_ray.nelrayloc_max,MPI_DOUBLE,
			       n,0,n,0,sdparall_ray.syrthes_comm_world,&status);
	                                      /* ou syrthes_comm_ray, ca doit etre pareil ici */
	  for (i=0;i<sdparall_ray.nbval_a_recevoir[n];i++)
	    tmpray[rcoups.numf[sdparall_ray.indice_a_recevoir[n]+i]]=dtrav[i];
	}
      
      else /* on est  sur un proc qui ne fait pas de ray */
	{ 
	  MPI_Send(dtrav,sdparall_ray.nelrayloc_max,MPI_DOUBLE,n,0,sdparall_ray.syrthes_comm_world);
	}

    }
  
  /* si on est sur un proc avec ray, il reste a recevoir les Tmps des proc qui ne font pas de rayt */
  if (sdparall_ray.rangray>-1)
    {
      for (n=sdparall_ray.npartsray;n<sdparall_ray.nparts;n++)
	{
	  MPI_Recv(dtrav,sdparall_ray.nelrayloc_max,MPI_DOUBLE,n,0,sdparall_ray.syrthes_comm_world,&status);
	  for (i=0;i<sdparall_ray.nbval_a_recevoir[n];i++)
	    tmpray[rcoups.numf[sdparall_ray.indice_a_recevoir[n]+i]]=dtrav[i];
	}
    }



  if (affich.passageSR && sdparall_ray.rangray>-1)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf(" -->  transSR : valeur de tmpray apres transfert de la temperature");
	  printf(" issue du solide\n");
	  for (i=0;i<sdparall_ray.nelrayloc[sdparall_ray.rangray];i++)
	    printf(" rang %d ++  i=%d temray=%25.18e\n",
		   sdparall_ray.rangray,i,tmpray[i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf(" -->  transSR : tmpray value after the temperature transfer");
	  printf(" from the solid\n");
	  for (i=0;i<sdparall_ray.nelrayloc[sdparall_ray.rangray];i++)
	    printf(" rank %d ++  i=%d temray=%25.18e\n",
		   sdparall_ray.rangray,i,tmpray[i]);
	}
      printf("\n");
    }

#endif
}
