/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
# include "syr_abs.h"
# include "syr_bd.h"
# include "syr_tree.h"
# include "syr_proto.h"

/* # include "mpi.h" */

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | derriere                                                             |
  |         Detection des segments en arriere et retour d'un code de     |
  |         Classement                                                   |
  |======================================================================| */
void derriere_2d (int nel_i, int nel_j, 
		  double **xnf,double *pland,double xi[],double yi[],
		  double dsign[],int *code_decoupe)
{
  int k;
  double epsder;

  epsder = 1e-3;


  for (k=0;k<2;k++)
    dsign[k]=xnf[0][nel_j]*xi[k]+xnf[1][nel_j]*yi[k] + pland[nel_j] ;

  for (k=2;k<4;k++)
    dsign[k]=xnf[0][nel_i]*xi[k]+xnf[1][nel_i]*yi[k] + pland[nel_i] ;

  if (( dsign[0]< epsder && dsign[1]< epsder) ||
      ( dsign[2]< epsder && dsign[3]< epsder))          *code_decoupe = -10;

  else if ( dsign[0]> -epsder && dsign[1]> -epsder &&
	    dsign[2]> -epsder && dsign[3]> -epsder )    *code_decoupe =   0;

  else if ( dsign[0]> -epsder && dsign[1]> -epsder &&
	    dsign[2]> -epsder && dsign[3]<  epsder)     *code_decoupe =   1;

  else if ( dsign[0]> -epsder && dsign[1]> -epsder &&
	    dsign[3]> -epsder && dsign[2]<  epsder)     *code_decoupe =  -1;

  else if ( dsign[2]> -epsder && dsign[3]> -epsder &&
	    dsign[0]> -epsder && dsign[1]<  epsder)     *code_decoupe =   2;

  else if ( dsign[2]> -epsder && dsign[3]> -epsder &&
	    dsign[1]> -epsder && dsign[0]<  epsder)     *code_decoupe =  -2;

  else if ( dsign[0]*dsign[1]< epsder &&
	    dsign[2]*dsign[3]< epsder )
    {
      if (SYRTHES_LANG == FR)
	{
	  printf(" ERREUR derriere_2d : il semble y avoir une erreur dans le maillage.");
	  printf("                      les elements %d et %d sont superposes \n",nel_i+1,nel_j+1) ;
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf(" ERROR derriere_2d : It is likely an error exists in the mesh");
	  printf("                     elements %d and %d are collapsed\n",nel_i+1,nel_j+1) ;
	}
      syrthes_exit(1);
    }
  else
    {
      if (SYRTHES_LANG == FR)
	printf(" Cas non prevu pour les elements nel_i= %d nel_j=%d \n",nel_i+1,nel_j+1); 
      else if (SYRTHES_LANG == EN)
	printf(" Non identified case for the elements nel_i= %d nel_j=%d \n",nel_i+1,nel_j+1); 
      *code_decoupe = -6;
    }

}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | decoupe_seg                                                          |
  |         Decoupage de segment pour optimisation de la qualite         |
  |                                                                      |
  |======================================================================| */

void decoupe_seg (int nel_i, int nel_j, 
		  double **xnf,double *pland,double xi[],double yi[],
		  double dsign[],int code_decoupe)
     
{
  int i;
  double xnj1,xnj2;
  double denom,numer,alfa;
  double pa[2],plac;
  double epsd=1.e-5,eps=1.e-6;

  if ( code_decoupe == 1 ) 
    {
      xnj1 = xnf[0][nel_i];
      xnj2 = xnf[1][nel_i];
      plac = pland[nel_i];
 
      /* Determination de la racine pa  */
      denom = xnj1*(xi[3]-xi[2])
             +xnj2*(yi[3]-yi[2]);

      if ( fabs(denom) > eps )
	{
	  numer =  xnj1*xi[2]+ xnj2*yi[2]+ plac ;
	  alfa  = - numer/denom - epsd ;

	  pa[0]= xi[2]+ alfa*(xi[3]-xi[2]);
	  pa[1]= yi[2]+ alfa*(yi[3]-yi[2]);
	}

      xi[0]=xi[0];  yi[0]=yi[0];   
      xi[1]=xi[1];  yi[1]=yi[1];   
      xi[2]=xi[2];  yi[2]=yi[2];  
      xi[3]=pa[0];  yi[3]=pa[1];  
    }

  else if( code_decoupe == -1 )
    {
      xnj1 = xnf[0][nel_i];
      xnj2 = xnf[1][nel_i];
      plac = pland[nel_i];
 
      denom = xnj1*(xi[3]-xi[2])
             +xnj2*(yi[3]-yi[2]);

      if ( fabs(denom) > eps )
	{
	  numer =  xnj1*xi[2]+ xnj2*yi[2]+ plac ;
	  alfa  = - numer/denom + epsd ;

	  pa[0]= xi[2]+ alfa*(xi[3]-xi[2]);
	  pa[1]= yi[2]+ alfa*(yi[3]-yi[2]);
	}

      xi[0]=xi[0];  yi[0]=yi[0];   
      xi[1]=xi[1];  yi[1]=yi[1];   
      xi[2]=pa[0];  yi[2]=pa[1];  
      xi[3]=xi[3];  yi[3]=yi[3];  
    }


   else if( code_decoupe == 2 )
    {
      xnj1 = xnf[0][nel_j];
      xnj2 = xnf[1][nel_j];
      plac = pland[nel_j];
 
      denom = xnj1*(xi[1]-xi[0])
             +xnj2*(yi[1]-yi[0]);

      if ( fabs(denom) > eps )
	{
	  numer =  xnj1*xi[0]+ xnj2*yi[0]+ plac ;
	  alfa  = - numer/denom - epsd ;

	  pa[0]= xi[0]+ alfa*(xi[1]-xi[0]);
	  pa[1]= yi[0]+ alfa*(yi[1]-yi[0]);
	}

      xi[0]=xi[0];  yi[0]=yi[0];   
      xi[1]=pa[0];  yi[1]=pa[1];   
      xi[2]=xi[2];  yi[2]=yi[2];  
      xi[3]=xi[3];  yi[3]=yi[3];  
    }


   else if( code_decoupe == -2 )
    {
      xnj1 = xnf[0][nel_j];
      xnj2 = xnf[1][nel_j];
      plac = pland[nel_j];
 
      denom = xnj1*(xi[1]-xi[0])
             +xnj2*(yi[1]-yi[0]);

      if ( fabs(denom) > eps )
	{
	  numer =  xnj1*xi[0]+ xnj2*yi[0]+ plac ;
	  alfa  = - numer/denom + epsd ;

	  pa[0]= xi[0]+ alfa*(xi[1]-xi[0]);
	  pa[1]= yi[0]+ alfa*(yi[1]-yi[0]);
	}

      xi[0]=pa[0];  yi[0]=pa[1];   
      xi[1]=xi[1];  yi[1]=yi[1];   
      xi[2]=xi[2];  yi[2]=yi[2];  
      xi[3]=xi[3];  yi[3]=yi[3];  
    }

   else
     if (SYRTHES_LANG == FR)
       printf(" ERREUR derriere_2d : Cas non prevu \n" );
     else if (SYRTHES_LANG == EN)
       printf(" ERROR derriere_2d : Case not available \n" );

     
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | derriere                                                             |
  |         Detection des faces en arriere et retour d'un code de        |
  |         Classement                                                   |
  |======================================================================| */
void derriere_3d (int nel_i, int nel_j, 
		  double **xnf,double *pland,double xi[],double yi[],double zi[],
		  double dsign[],int *code_decoupe)

{
  int k;
  double epsder,testn,epsma;

  epsder = 5e-4;
  epsma  = 1e-12;

  for (k=0;k<3;k++)
    dsign[k] = xnf[0][nel_j]*xi[k]+xnf[1][nel_j]*yi[k]+xnf[2][nel_j]*zi[k]+pland[nel_j] ;

  for (k=3;k<6;k++)
    dsign[k]  = xnf[0][nel_i]*xi[k]+xnf[1][nel_i]*yi[k]+xnf[2][nel_i]*zi[k]+pland[nel_i] ;

  if (( dsign[0]< epsder && dsign[1]< epsder && dsign[2]< epsder) ||
      ( dsign[3]< epsder && dsign[4]< epsder && dsign[5]< epsder)) *code_decoupe = -10;

  else if ( dsign[0]> -epsder && dsign[1]> -epsder && dsign[2]> -epsder &&
	    dsign[3]> -epsder && dsign[4]> -epsder && dsign[5]> -epsder ) *code_decoupe = 0;

  else if (( dsign[0]> -epsder && dsign[1]> -epsder && dsign[2]> -epsder) &&
	   ( dsign[3]< epsder  || dsign[4]< epsder  || dsign[5]< epsder ) &&
           (  dsign[3]*dsign[4]*dsign[5]>= -epsma) )                  *code_decoupe = 1;

  else if (( dsign[3]> -epsder && dsign[4]> -epsder && dsign[5]> -epsder) &&
	   ( dsign[0]< epsder  || dsign[1]< epsder  || dsign[2]< epsder ) &&
           (  dsign[0]*dsign[1]*dsign[2]>= -epsma) )                  *code_decoupe = -1;

  else if (( dsign[0]< epsder || dsign[1]< epsder || dsign[2]< epsder) &&
	   ( dsign[3]< epsder || dsign[4]< epsder || dsign[5]< epsder )&&
           ( dsign[0]*dsign[1]*dsign[2]>= -epsma) &&
           ( dsign[3]*dsign[4]*dsign[5]>= -epsma) )                  *code_decoupe =  3;

  else if (( dsign[0]> -epsder && dsign[1]> -epsder && dsign[2]> -epsder) &&
	   ( dsign[3]< epsder  || dsign[4]< epsder  || dsign[5]< epsder ) &&
           (  dsign[3]*dsign[4]*dsign[5]< -epsma) )                  *code_decoupe = 2;

  else if (( dsign[3]> -epsder && dsign[4]> -epsder && dsign[5]> -epsder) &&
	   ( dsign[0]< epsder  || dsign[1]< epsder  || dsign[2]< epsder ) &&
           (  dsign[0]*dsign[1]*dsign[2]< -epsma) )                  *code_decoupe = -2;

  else if (( dsign[0]< epsder || dsign[1]< epsder || dsign[2]< epsder) &&
	   ( dsign[3]< epsder || dsign[4]< epsder || dsign[5]< epsder )&&
           ( dsign[0]*dsign[1]*dsign[2]< -epsma) &&
           ( dsign[3]*dsign[4]*dsign[5]< -epsma) )                   *code_decoupe =  4;

  else if (( dsign[0]< epsder || dsign[1]< epsder || dsign[2]< epsder) &&
	   ( dsign[3]< epsder || dsign[4]< epsder || dsign[5]< epsder )&&
           ( dsign[0]*dsign[1]*dsign[2]> epsma) &&
           ( dsign[3]*dsign[4]*dsign[5]< -epsma) )                   *code_decoupe =  5;

  else if (( dsign[0]< epsder || dsign[1]< epsder || dsign[2]< epsder) &&
	   ( dsign[3]< epsder || dsign[4]< epsder || dsign[5]< epsder )&&
           ( dsign[0]*dsign[1]*dsign[2]< -epsma) &&
           ( dsign[3]*dsign[4]*dsign[5]> epsma) )                   *code_decoupe = -5;





  else if ((fabs(dsign[0]) < epsder || fabs(dsign[1])< epsder || fabs(dsign[2])< epsder) ||
	   (fabs(dsign[3]) < epsder || fabs(dsign[4])< epsder || fabs(dsign[5])< epsder))
    {
	 if ((fabs(dsign[3]) < epsder && fabs(dsign[4]) > epsder && fabs(dsign[5]) > epsder ) ||
	     (fabs(dsign[4]) < epsder && fabs(dsign[3]) > epsder && fabs(dsign[5]) > epsder ) ||
	     (fabs(dsign[5]) < epsder && fabs(dsign[3]) > epsder && fabs(dsign[4]) > epsder ))
	   {
	     /* Soit le premier triangle n'a pas de point nul */
	     if (fabs(dsign[0])> epsder && fabs(dsign[1])>epsder && fabs(dsign[2]) > epsder )
	       {
		 if (dsign[0]>epsder && dsign[1]>epsder && dsign[2]>epsder)
		   *code_decoupe = 1;
		 else if (dsign[0]*dsign[1]*dsign[2]<epsma)
		   *code_decoupe = -5;
		 else
		   *code_decoupe = 3;		   
	       }

	     else if ((fabs(dsign[0]) < epsder && fabs(dsign[1]) > epsder && fabs(dsign[2]) > epsder ) ||
		      (fabs(dsign[1]) < epsder && fabs(dsign[2]) > epsder && fabs(dsign[0]) > epsder ) ||
		      (fabs(dsign[2]) < epsder && fabs(dsign[0]) > epsder && fabs(dsign[1]) > epsder ))
	       if ( fabs(dsign[0]) < epsder )
		 {
		   /* triangle 1 partage */
		   if (dsign[1]*dsign[2] < -epsma ) *code_decoupe = 3;
		   else
		     /* pas de redecoupage du triangle 1 */
		     *code_decoupe = 1;
		 }
	       else if ( fabs(dsign[1]) < epsder )
		 {
		   /* triangle 1 partage */
		   if (dsign[0]*dsign[2] < -epsma ) *code_decoupe = 3;
		   else
		     /* pas de redecoupage du triangle 1 */
		     *code_decoupe = 1;
		 }
	       else if ( fabs(dsign[2]) < epsder )
		 {
		   /* triangle 1 partage */
		   if (dsign[0]*dsign[1] < -epsma ) *code_decoupe = 3;
		   else
		     /* pas de redecoupage du triangle 1 */
		     *code_decoupe = 1;
		 }


	       else
		 if (SYRTHES_LANG == FR)
		   printf(" Ce cas aurait deja du etre traite facettes : %d %d \n",nel_i+1,nel_j+1);
		 else if (SYRTHES_LANG == EN)
		   printf(" This case should already have been treated - faces : %d %d \n",nel_i+1,nel_j+1);
	   }

	 else if ((fabs(dsign[0]) < epsder && fabs(dsign[1]) > epsder && fabs(dsign[2]) > epsder ) ||
		  (fabs(dsign[1]) < epsder && fabs(dsign[2]) > epsder && fabs(dsign[0]) > epsder ) ||
		  (fabs(dsign[2]) < epsder && fabs(dsign[0]) > epsder && fabs(dsign[1]) > epsder ))
	   {
	     /* Soit le deuxieme triangle n'a pas de point nul */
	     if (fabs(dsign[3])> epsder && fabs(dsign[4])>epsder && fabs(dsign[5]) > epsder )
	       {
		 if (dsign[3]>epsder && dsign[4]>epsder && dsign[5]>epsder)
		   *code_decoupe = -1;
		 else if (dsign[3]*dsign[4]*dsign[5]<epsma)
		   *code_decoupe = 5;
		 else
		   *code_decoupe = 3;		   
	       }

	     else
	       if (SYRTHES_LANG == FR)
		 printf(" Ce cas aurait deja du etre traite  facettes : %d %d \n",nel_i+1,nel_j+1);
	       else if (SYRTHES_LANG == EN)
		 printf(" This case should already have been treated - faces : %d %d \n",nel_i+1,nel_j+1);
	   }
	 else
	   printf("Erreur derriere\n");
       }

  else
    {
      testn = xnf[0][nel_j]*xnf[0][nel_i]+xnf[1][nel_j]*xnf[1][nel_i]+xnf[2][nel_j]*xnf[2][nel_i];
      if(fabs(testn)>=0.999)
	{
	  if (SYRTHES_LANG == FR)
	    printf("cas legerement ambigu mais sans consequence nel_i %d nel_j %d \n",nel_i+1,nel_j+1); 
	  else if (SYRTHES_LANG == EN)
	    printf("case shlightly ambigous, but without consequence nel_i %d nel_j %d \n",nel_i+1,nel_j+1); 
	}
      else
	{
	  if (SYRTHES_LANG == FR)
	    printf(" Cas ambigu pour les elements nel_i= %d nel_j=%d \n",nel_i+1,nel_j+1); 
	  else if (SYRTHES_LANG == EN)
	    printf("case slightly ambigous for the elements nel_i= %d nel_j=%d \n",nel_i+1,nel_j+1); 

	  printf("xi,yi,zi\n %f %f %f \n %f %f %f \n %f %f %f \n %f %f %f \n %f %f %f \n %f %f %f \n",
		 xi[0],yi[0],zi[0],xi[1],yi[1],zi[1],xi[2],yi[2],zi[2],
		 xi[3],yi[3],zi[3],xi[4],yi[4],zi[4],xi[5],yi[5],zi[5]); 
	  printf(" xn_i yn_i zn_i pland %f %f %f %f \n",
		 xnf[0][nel_i],xnf[1][nel_i],xnf[2][nel_i],pland[nel_i]);
	  printf(" xn_j yn_j zn_j pland %f %f %f %f \n",
		 xnf[0][nel_j],xnf[1][nel_j],xnf[2][nel_j],pland[nel_j]);
	  printf(" p0 p1 p2 : %f %f %f \n",dsign[0]*1e3,dsign[1]*1e3,dsign[2]*1e3);
	  printf(" p3 p4 p5 : %f %f %f \n",dsign[3]*1e3,dsign[4]*1e3,dsign[5]*1e3);
	}
      *code_decoupe = -6;
    }

}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | decoupe_totd                                                         |
  |         Apres inversion eventuelle entre nel_i et nel_j              |
  |         Decoupage du triangle nel_j pour donner un triangle          | 
  |         le parametre code_decoupe est egal a 1 ou -1                 |
  |                                                                      |
  |======================================================================| */

void decoupe_totd (int nel_i, int nel_j, 
                   double **xnf,double *pland,double xi[],double yi[],double zi[],
                   double dsign[],int code_decoupe)

{
  int i,kdeb,seg1n1,seg1n2,seg2n1,seg2n2,kdebm1,kdebp1;
  double xa,ya,za,xb,yb,zb,xc,yc,zc,da,db,dc;
  double xnj1,xnj2,xnj3;
  double denom,numer,alfa;
  double xka,yka,zka,plac;
  double pa[3],pb[3];
  double epsma=1e-10,epsd=1.e-5,eps=1.e-6;

  xnj1 = xnf[0][nel_i];
  xnj2 = xnf[1][nel_i];
  xnj3 = xnf[2][nel_i];
  plac = pland[nel_i];

  if ( code_decoupe == -1 ) 
    {
      xa=xi[0];   ya=yi[0];   za=zi[0];
      xb=xi[1];   yb=yi[1];   zb=zi[1];
      xc=xi[2];   yc=yi[2];   zc=zi[2];
      da=dsign[0];db=dsign[1];dc=dsign[2];

      xi[0]=xi[3]; yi[0]=yi[3]; zi[0]=zi[3];
      xi[1]=xi[4]; yi[1]=yi[4]; zi[1]=zi[4];
      xi[2]=xi[5]; yi[2]=yi[5]; zi[2]=zi[5];
      dsign[0]=dsign[3]; dsign[1]=dsign[4]; dsign[2]=dsign[5];

      xi[3]=xa;   yi[3]=ya;   zi[3]=za;
      xi[4]=xb;   yi[4]=yb;   zi[4]=zb;
      xi[5]=xc;   yi[5]=yc;   zi[5]=zc;
      dsign[3]=da; dsign[4]=db; dsign[5]=dc;

      xnj1 = xnf[0][nel_j];
      xnj2 = xnf[1][nel_j];
      xnj3 = xnf[2][nel_j];
      plac = pland[nel_j];     
    }

  for ( i=3;i<6;i++ )if ( dsign[i]> epsma ) kdeb = i ;

  if ( kdeb == 3 )      {seg1n1 = 5 ; seg1n2 = 3 ;seg2n1 = 3 ; seg2n2 = 4; }
  else if ( kdeb == 4 ) {seg1n1 = 3 ; seg1n2 = 4 ;seg2n1 = 4 ; seg2n2 = 5; }
  else                  {seg1n1 = 4 ; seg1n2 = 5 ;seg2n1 = 5 ; seg2n2 = 3; }


  denom = xnj1*(xi[seg1n2]-xi[seg1n1])
         +xnj2*(yi[seg1n2]-yi[seg1n1])
         +xnj3*(zi[seg1n2]-zi[seg1n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg1n1]+ xnj2*yi[seg1n1]+ xnj3*zi[seg1n1]+ plac ;
      alfa  = - numer/denom + epsd ;

      pa[0]= xi[seg1n1]+ alfa*(xi[seg1n2]-xi[seg1n1]);
      pa[1]= yi[seg1n1]+ alfa*(yi[seg1n2]-yi[seg1n1]);
      pa[2]= zi[seg1n1]+ alfa*(zi[seg1n2]-zi[seg1n1]);
    }

  denom = xnj1*(xi[seg2n2]-xi[seg2n1])
         +xnj2*(yi[seg2n2]-yi[seg2n1])
         +xnj3*(zi[seg2n2]-zi[seg2n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg2n1]+ xnj2*yi[seg2n1]+ xnj3*zi[seg2n1]+ plac ;
      alfa  = - numer/denom - epsd ;

      pb[0]= xi[seg2n1]+ alfa*(xi[seg2n2]-xi[seg2n1]);
      pb[1]= yi[seg2n1]+ alfa*(yi[seg2n2]-yi[seg2n1]);
      pb[2]= zi[seg2n1]+ alfa*(zi[seg2n2]-zi[seg2n1]);
    }

  if ( kdeb == 3)       { kdebm1=5 ; kdebp1=4;}
  else if ( kdeb == 4 ) { kdebm1=3 ; kdebp1=5;}
  else                  { kdebm1=4 ; kdebp1=3;}

  xka=xi[kdeb];yka=yi[kdeb];zka=zi[kdeb];

  xi[3]=pa[0];  yi[3]=pa[1];  zi[3]=pa[2];
  xi[4]=xka  ;  yi[4]=yka  ;  zi[4]=zka  ;
  xi[5]=pb[0];  yi[5]=pb[1];  zi[5]=pb[2];

}



      
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | decoupe_toqd                                                         |
  |         Apres inversion eventuelle entre nel_i et nel_j              |
  |         Decoupage du triangle nel_j pour donner deux triangles       | 
  |         le parametre code_decoupe est egal a 2 ou -2                 |
  |                                                                      |
  |======================================================================| */

void decoupe_toqd (int nel_i, int nel_j, 
                   double **xnf,double *pland,double xi[],double yi[],
                   double zi[],
                   double xp[],double yp[],double zp[],
                   double dsign[],int code_decoupe)


{
  int i,kdeb,seg1n1,seg1n2,seg2n1,seg2n2,kdebm1,kdebp1;
  double xa,ya,za,xb,yb,zb,xc,yc,zc,da,db,dc;
  double xnj1,xnj2,xnj3;
  double denom,numer,alfa,epsd=1.e-5,eps=1.e-6;
  double diag1,diag2,plac;
  double pa[3],pb[3];

  xnj1 = xnf[0][nel_i];
  xnj2 = xnf[1][nel_i];
  xnj3 = xnf[2][nel_i];
  plac = pland[nel_i];

  if ( code_decoupe == -2 ) /* inversion des triangles */
    {
      xa=xi[0];   ya=yi[0];   za=zi[0];
      xb=xi[1];   yb=yi[1];   zb=zi[1];
      xc=xi[2];   yc=yi[2];   zc=zi[2];
      da=dsign[0];db=dsign[1];dc=dsign[2];

      xi[0]=xi[3]; yi[0]=yi[3]; zi[0]=zi[3];
      xi[1]=xi[4]; yi[1]=yi[4]; zi[1]=zi[4];
      xi[2]=xi[5]; yi[2]=yi[5]; zi[2]=zi[5];
      dsign[0]=dsign[3]; dsign[1]=dsign[4]; dsign[2]=dsign[5];

      xi[3]=xa;   yi[3]=ya;   zi[3]=za;
      xi[4]=xb;   yi[4]=yb;   zi[4]=zb;
      xi[5]=xc;   yi[5]=yc;   zi[5]=zc;
      dsign[3]=da; dsign[4]=db; dsign[5]=dc;

      xnj1 = xnf[0][nel_j];
      xnj2 = xnf[1][nel_j];
      xnj3 = xnf[2][nel_j];
      plac = pland[nel_j];     
    }

  for ( i=3;i<6;i++ )if ( dsign[i]< 0 ) kdeb = i ;

  if ( kdeb == 3 )      {seg1n1 = 5 ; seg1n2 = 3 ;seg2n1 = 3 ; seg2n2 = 4; }
  else if ( kdeb == 4 ) {seg1n1 = 3 ; seg1n2 = 4 ;seg2n1 = 4 ; seg2n2 = 5; }
  else                  {seg1n1 = 4 ; seg1n2 = 5 ;seg2n1 = 5 ; seg2n2 = 3; }


  denom = xnj1*(xi[seg1n2]-xi[seg1n1])
         +xnj2*(yi[seg1n2]-yi[seg1n1])
         +xnj3*(zi[seg1n2]-zi[seg1n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg1n1]+ xnj2*yi[seg1n1]+ xnj3*zi[seg1n1]+ plac ;
      alfa  = - numer/denom - epsd ;

      pa[0]= xi[seg1n1]+ alfa*(xi[seg1n2]-xi[seg1n1]);
      pa[1]= yi[seg1n1]+ alfa*(yi[seg1n2]-yi[seg1n1]);
      pa[2]= zi[seg1n1]+ alfa*(zi[seg1n2]-zi[seg1n1]);
    }

  denom = xnj1*(xi[seg2n2]-xi[seg2n1])
         +xnj2*(yi[seg2n2]-yi[seg2n1])
         +xnj3*(zi[seg2n2]-zi[seg2n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg2n1]+ xnj2*yi[seg2n1]+ xnj3*zi[seg2n1]+ plac ;
      alfa  = - numer/denom + epsd ;

      pb[0]= xi[seg2n1]+ alfa*(xi[seg2n2]-xi[seg2n1]);
      pb[1]= yi[seg2n1]+ alfa*(yi[seg2n2]-yi[seg2n1]);
      pb[2]= zi[seg2n1]+ alfa*(zi[seg2n2]-zi[seg2n1]);
    }

  if ( kdeb == 3)       { kdebm1=5 ; kdebp1=4;}
  else if ( kdeb == 4 ) { kdebm1=3 ; kdebp1=5;}
  else                  { kdebm1=4 ; kdebp1=3;}

  diag1 = (xi[kdebp1]-pa[0])*(xi[kdebp1]-pa[0]) +
          (yi[kdebp1]-pa[1])*(yi[kdebp1]-pa[1]) +
          (zi[kdebp1]-pa[2])*(zi[kdebp1]-pa[2])      ;
  diag2 = (xi[kdebm1]-pb[0])*(xi[kdebm1]-pb[0]) +
          (yi[kdebm1]-pb[1])*(yi[kdebm1]-pb[1]) +
          (zi[kdebm1]-pb[2])*(zi[kdebm1]-pb[2])      ;

  if ( diag1 < diag2 ) 
    {
      xp[0]=pa[0]     ;  yp[0]=pa[1]     ;  zp[0]=pa[2]     ;
      xp[1]=pb[0]     ;  yp[1]=pb[1]     ;  zp[1]=pb[2]     ;
      xp[2]=xi[kdebp1];  yp[2]=yi[kdebp1];  zp[2]=zi[kdebp1];
      xp[3]=xi[kdebm1];  yp[3]=yi[kdebm1];  zp[3]=zi[kdebm1];
    }
  else
    {
      xp[0]=xi[kdebm1];  yp[0]=yi[kdebm1];  zp[0]=zi[kdebm1];
      xp[1]=pa[0]     ;  yp[1]=pa[1]     ;  zp[1]=pa[2]     ;
      xp[2]=pb[0]     ;  yp[2]=pb[1]     ;  zp[2]=pb[2]     ;
      xp[3]=xi[kdebp1];  yp[3]=yi[kdebp1];  zp[3]=zi[kdebp1];
    }
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | decoupe_tdtd                                                         |
  |         Decoupage du triangle nel_i pour donner un triangle          | 
  |         Decoupage du triangle nel_j pour donner un triangle          | 
  |         le parametre code_decoupe est egal a 3                       |
  |                                                                      |
  |======================================================================| */

void decoupe_tdtd (int nel_i, int nel_j, 
                   double **xnf,double *pland,double xi[],double yi[],double zi[],
                   double dsign[],int code_decoupe)

{
  int i,kdeb,seg1n1,seg1n2,seg2n1,seg2n2,kdebm1,kdebp1;
  double xnj1,xnj2,xnj3;
  double denom,numer,alfa;
  double xka,yka,zka,plac;
  double pa[3],pb[3];
  double x01,y01,z01,x02,y02,z02;
  double epsma=1e-14,epsd=1e-5,eps=1e-6;

  xnj1 = xnf[0][nel_i];
  xnj2 = xnf[1][nel_i];
  xnj3 = xnf[2][nel_i];
  plac = pland[nel_i];


  for ( i=3;i<6;i++ )if ( dsign[i]> epsma ) kdeb = i ;

  if ( kdeb == 3 )      {seg1n1 = 5 ; seg1n2 = 3 ;seg2n1 = 3 ; seg2n2 = 4; }
  else if ( kdeb == 4 ) {seg1n1 = 3 ; seg1n2 = 4 ;seg2n1 = 4 ; seg2n2 = 5; }
  else                  {seg1n1 = 4 ; seg1n2 = 5 ;seg2n1 = 5 ; seg2n2 = 3; }


  denom = xnj1*(xi[seg1n2]-xi[seg1n1])
         +xnj2*(yi[seg1n2]-yi[seg1n1])
         +xnj3*(zi[seg1n2]-zi[seg1n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg1n1]+ xnj2*yi[seg1n1]+ xnj3*zi[seg1n1]+ plac ;
      alfa  = - numer/denom + epsd ;

      pa[0]= xi[seg1n1]+ alfa*(xi[seg1n2]-xi[seg1n1]);
      pa[1]= yi[seg1n1]+ alfa*(yi[seg1n2]-yi[seg1n1]);
      pa[2]= zi[seg1n1]+ alfa*(zi[seg1n2]-zi[seg1n1]);
    }

  denom = xnj1*(xi[seg2n2]-xi[seg2n1])
         +xnj2*(yi[seg2n2]-yi[seg2n1])
         +xnj3*(zi[seg2n2]-zi[seg2n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg2n1]+ xnj2*yi[seg2n1]+ xnj3*zi[seg2n1]+ plac ;
      alfa  = - numer/denom -epsd ;

      pb[0]= xi[seg2n1]+ alfa*(xi[seg2n2]-xi[seg2n1]);
      pb[1]= yi[seg2n1]+ alfa*(yi[seg2n2]-yi[seg2n1]);
      pb[2]= zi[seg2n1]+ alfa*(zi[seg2n2]-zi[seg2n1]);
    }

  if ( kdeb == 3)       { kdebm1=5 ; kdebp1=4;}
  else if ( kdeb == 4 ) { kdebm1=3 ; kdebp1=5;}
  else                  { kdebm1=4 ; kdebp1=3;}

  xka=xi[kdeb];yka=yi[kdeb];zka=zi[kdeb];

  xi[3]=pa[0];  yi[3]=pa[1];  zi[3]=pa[2];
  xi[4]=xka  ;  yi[4]=yka  ;  zi[4]=zka  ;
  xi[5]=pb[0];  yi[5]=pb[1];  zi[5]=pb[2];



  xnj1 = xnf[0][nel_j];
  xnj2 = xnf[1][nel_j];
  xnj3 = xnf[2][nel_j];
  plac = pland[nel_j];     

  for ( i=0;i<3;i++ )if ( dsign[i]> epsma ) kdeb = i ;

  if ( kdeb == 0 )      {seg1n1 = 2 ; seg1n2 = 0 ;seg2n1 = 0 ; seg2n2 = 1; }
  else if ( kdeb == 1 ) {seg1n1 = 0 ; seg1n2 = 1 ;seg2n1 = 1 ; seg2n2 = 2; }
  else                  {seg1n1 = 1 ; seg1n2 = 2 ;seg2n1 = 2 ; seg2n2 = 0; }


  denom = xnj1*(xi[seg1n2]-xi[seg1n1])
         +xnj2*(yi[seg1n2]-yi[seg1n1])
         +xnj3*(zi[seg1n2]-zi[seg1n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg1n1]+ xnj2*yi[seg1n1]+ xnj3*zi[seg1n1]+ plac ;
      alfa  = - numer/denom + epsd  ;

      pa[0]= xi[seg1n1]+ alfa*(xi[seg1n2]-xi[seg1n1]);
      pa[1]= yi[seg1n1]+ alfa*(yi[seg1n2]-yi[seg1n1]);
      pa[2]= zi[seg1n1]+ alfa*(zi[seg1n2]-zi[seg1n1]);
    }

  denom = xnj1*(xi[seg2n2]-xi[seg2n1])
         +xnj2*(yi[seg2n2]-yi[seg2n1])
         +xnj3*(zi[seg2n2]-zi[seg2n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg2n1]+ xnj2*yi[seg2n1]+ xnj3*zi[seg2n1]+ plac ;
      alfa  = - numer/denom -epsd ;

      pb[0]= xi[seg2n1]+ alfa*(xi[seg2n2]-xi[seg2n1]);
      pb[1]= yi[seg2n1]+ alfa*(yi[seg2n2]-yi[seg2n1]);
      pb[2]= zi[seg2n1]+ alfa*(zi[seg2n2]-zi[seg2n1]);
    }

  if ( kdeb == 0)       { kdebm1=2 ; kdebp1=1;}
  else if ( kdeb == 1 ) { kdebm1=0 ; kdebp1=2;}
  else                  { kdebm1=1 ; kdebp1=0;}

  xka=xi[kdeb];yka=yi[kdeb];zka=zi[kdeb];

  xi[0]=pa[0];  yi[0]=pa[1];  zi[0]=pa[2];
  xi[1]=xka  ;  yi[1]=yka  ;  zi[1]=zka  ;
  xi[2]=pb[0];  yi[2]=pb[1];  zi[2]=pb[2];


}




/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | decoupe_qdqd                                                         |
  |         Decoupage du triangle nel_i pour donner deux triangles       | 
  |         Decoupage du triangle nel_j pour donner deux triangles       | 
  |         le parametre code_decoupe est egal a 4                       |
  |                                                                      |
  |======================================================================| */

void decoupe_qdqd (int nel_i, int nel_j, 
                   double **xnf,double *pland,double xi[],double yi[],
                   double zi[],
                   double xp[],double yp[],double zp[],
                   double xq[],double yq[],double zq[],
                   double dsign[],int code_decoupe)


{
  int i,kdeb,seg1n1,seg1n2,seg2n1,seg2n2,kdebm1,kdebp1;
  double xnj1,xnj2,xnj3;
  double denom,numer,alfa;
  double diag1,diag2,plac;
  double pa[3],pb[3];
  double x01,y01,z01,x02,y02,z02,x03,y03,z03;
  double epsd=1e-5,eps=1e-6;

  xnj1 = xnf[0][nel_i];
  xnj2 = xnf[1][nel_i];
  xnj3 = xnf[2][nel_i];
  plac = pland[nel_i];


  for ( i=3;i<6;i++ )if ( dsign[i]< 0 ) kdeb = i ;

  if ( kdeb == 3 )      {seg1n1 = 5 ; seg1n2 = 3 ;seg2n1 = 3 ; seg2n2 = 4; }
  else if ( kdeb == 4 ) {seg1n1 = 3 ; seg1n2 = 4 ;seg2n1 = 4 ; seg2n2 = 5; }
  else                  {seg1n1 = 4 ; seg1n2 = 5 ;seg2n1 = 5 ; seg2n2 = 3; }


  denom = xnj1*(xi[seg1n2]-xi[seg1n1])
         +xnj2*(yi[seg1n2]-yi[seg1n1])
         +xnj3*(zi[seg1n2]-zi[seg1n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg1n1]+ xnj2*yi[seg1n1]+ xnj3*zi[seg1n1]+ plac ;
      alfa  = - numer/denom - epsd ;

      pa[0]= xi[seg1n1]+ alfa*(xi[seg1n2]-xi[seg1n1]);
      pa[1]= yi[seg1n1]+ alfa*(yi[seg1n2]-yi[seg1n1]);
      pa[2]= zi[seg1n1]+ alfa*(zi[seg1n2]-zi[seg1n1]);
    }

  denom = xnj1*(xi[seg2n2]-xi[seg2n1])
         +xnj2*(yi[seg2n2]-yi[seg2n1])
         +xnj3*(zi[seg2n2]-zi[seg2n1]);

  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg2n1]+ xnj2*yi[seg2n1]+ xnj3*zi[seg2n1]+ plac ;
      alfa  = - numer/denom + epsd ;

      pb[0]= xi[seg2n1]+ alfa*(xi[seg2n2]-xi[seg2n1]);
      pb[1]= yi[seg2n1]+ alfa*(yi[seg2n2]-yi[seg2n1]);
      pb[2]= zi[seg2n1]+ alfa*(zi[seg2n2]-zi[seg2n1]);
    }

  if ( kdeb == 3)       { kdebm1=5 ; kdebp1=4;}
  else if ( kdeb == 4 ) { kdebm1=3 ; kdebp1=5;}
  else                  { kdebm1=4 ; kdebp1=3;}

  diag1 = (xi[kdebp1]-pa[0])*(xi[kdebp1]-pa[0]) +
          (yi[kdebp1]-pa[1])*(yi[kdebp1]-pa[1]) +
          (zi[kdebp1]-pa[2])*(zi[kdebp1]-pa[2])      ;
  diag2 = (xi[kdebm1]-pb[0])*(xi[kdebm1]-pb[0]) +
          (yi[kdebm1]-pb[1])*(yi[kdebm1]-pb[1]) +
          (zi[kdebm1]-pb[2])*(zi[kdebm1]-pb[2])      ;

  if ( diag1 < diag2 ) 
    {
      xq[0]=pa[0]     ;  yq[0]=pa[1]     ;  zq[0]=pa[2]     ;
      xq[1]=pb[0]     ;  yq[1]=pb[1]     ;  zq[1]=pb[2]     ;
      xq[2]=xi[kdebp1];  yq[2]=yi[kdebp1];  zq[2]=zi[kdebp1];
      xq[3]=xi[kdebm1];  yq[3]=yi[kdebm1];  zq[3]=zi[kdebm1];
    }
  else
    {
      xq[0]=xi[kdebm1];  yq[0]=yi[kdebm1];  zq[0]=zi[kdebm1];
      xq[1]=pa[0]     ;  yq[1]=pa[1]     ;  zq[1]=pa[2]     ;
      xq[2]=pb[0]     ;  yq[2]=pb[1]     ;  zq[2]=pb[2]     ;
      xq[3]=xi[kdebp1];  yq[3]=yi[kdebp1];  zq[3]=zi[kdebp1];
    }



  xnj1 = xnf[0][nel_j];
  xnj2 = xnf[1][nel_j];
  xnj3 = xnf[2][nel_j];
  plac = pland[nel_j];     

  for ( i=0;i<3;i++ )if ( dsign[i]< 0 ) kdeb = i ;

  if ( kdeb == 0 )      {seg1n1 = 2 ; seg1n2 = 0 ;seg2n1 = 0 ; seg2n2 = 1; }
  else if ( kdeb == 1 ) {seg1n1 = 0 ; seg1n2 = 1 ;seg2n1 = 1 ; seg2n2 = 2; }
  else                  {seg1n1 = 1 ; seg1n2 = 2 ;seg2n1 = 2 ; seg2n2 = 0; }


  denom = xnj1*(xi[seg1n2]-xi[seg1n1])
         +xnj2*(yi[seg1n2]-yi[seg1n1])
         +xnj3*(zi[seg1n2]-zi[seg1n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg1n1]+ xnj2*yi[seg1n1]+ xnj3*zi[seg1n1]+ plac ;
      alfa  = - numer/denom - epsd ;

      pa[0]= xi[seg1n1]+ alfa*(xi[seg1n2]-xi[seg1n1]);
      pa[1]= yi[seg1n1]+ alfa*(yi[seg1n2]-yi[seg1n1]);
      pa[2]= zi[seg1n1]+ alfa*(zi[seg1n2]-zi[seg1n1]);
    }

  denom = xnj1*(xi[seg2n2]-xi[seg2n1])
         +xnj2*(yi[seg2n2]-yi[seg2n1])
         +xnj3*(zi[seg2n2]-zi[seg2n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg2n1]+ xnj2*yi[seg2n1]+ xnj3*zi[seg2n1]+ plac ;
      alfa  = - numer/denom + epsd ;

      pb[0]= xi[seg2n1]+ alfa*(xi[seg2n2]-xi[seg2n1]);
      pb[1]= yi[seg2n1]+ alfa*(yi[seg2n2]-yi[seg2n1]);
      pb[2]= zi[seg2n1]+ alfa*(zi[seg2n2]-zi[seg2n1]);
    }

  if ( kdeb == 0)       { kdebm1=2 ; kdebp1=1;}
  else if ( kdeb == 1 ) { kdebm1=0 ; kdebp1=2;}
  else                  { kdebm1=1 ; kdebp1=0;}

  diag1 = (xi[kdebp1]-pa[0])*(xi[kdebp1]-pa[0]) +
          (yi[kdebp1]-pa[1])*(yi[kdebp1]-pa[1]) +
          (zi[kdebp1]-pa[2])*(zi[kdebp1]-pa[2])      ;
  diag2 = (xi[kdebm1]-pb[0])*(xi[kdebm1]-pb[0]) +
          (yi[kdebm1]-pb[1])*(yi[kdebm1]-pb[1]) +
          (zi[kdebm1]-pb[2])*(zi[kdebm1]-pb[2])      ;

  if ( diag1 < diag2 ) 
    {
      xp[0]=pa[0]     ;  yp[0]=pa[1]     ;  zp[0]=pa[2]     ;
      xp[1]=pb[0]     ;  yp[1]=pb[1]     ;  zp[1]=pb[2]     ;
      xp[2]=xi[kdebp1];  yp[2]=yi[kdebp1];  zp[2]=zi[kdebp1];
      xp[3]=xi[kdebm1];  yp[3]=yi[kdebm1];  zp[3]=zi[kdebm1];
    }
  else
    {
      xp[0]=xi[kdebm1];  yp[0]=yi[kdebm1];  zp[0]=zi[kdebm1];
      xp[1]=pa[0]     ;  yp[1]=pa[1]     ;  zp[1]=pa[2]     ;
      xp[2]=pb[0]     ;  yp[2]=pb[1]     ;  zp[2]=pb[2]     ;
      xp[3]=xi[kdebp1];  yp[3]=yi[kdebp1];  zp[3]=zi[kdebp1];
    }

}




/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | decoupe_tdqd                                                         |
  |         Apres inversion eventuelle entre nel_i et nel_j              |
  |         Decoupage du triangle nel_i pour donner un triangle          | 
  |         Decoupage du triangle nel_j pour donner deux triangles       | 
  |         le parametre code_decoupe est egal a 5 ou -5                 |
  |                                                                      |
  |======================================================================| */

void decoupe_tdqd (int nel_i, int nel_j, 
                   double **xnf,double *pland,double xi[],double yi[],
                   double zi[],double xt[],double yt[],double zt[],
                   double xq[],double yq[],double zq[],
                   double dsign[],int code_decoupe)


{
  int i,kdeb,seg1n1,seg1n2,seg2n1,seg2n2,kdebm1,kdebp1;
  double xa,ya,za,xb,yb,zb,xc,yc,zc,da,db,dc;
  double xnj1,xnj2,xnj3;
  double denom,numer,alfa;
  double diag1,diag2,plac;
  double pa[3],pb[3];
  double x01,y01,z01,x02,y02,z02;
  double epsd=1e-5,epsma=1e-14,eps=1e-6;


  xnj1 = xnf[0][nel_i];
  xnj2 = xnf[1][nel_i];
  xnj3 = xnf[2][nel_i];
  plac = pland[nel_i];


  if ( code_decoupe == -5 ) /* inversion des triangles */
    {
      xa=xi[0];   ya=yi[0];   za=zi[0];
      xb=xi[1];   yb=yi[1];   zb=zi[1];
      xc=xi[2];   yc=yi[2];   zc=zi[2];
      da=dsign[0];db=dsign[1];dc=dsign[2];

      xi[0]=xi[3]; yi[0]=yi[3]; zi[0]=zi[3];
      xi[1]=xi[4]; yi[1]=yi[4]; zi[1]=zi[4];
      xi[2]=xi[5]; yi[2]=yi[5]; zi[2]=zi[5];
      dsign[0]=dsign[3]; dsign[1]=dsign[4]; dsign[2]=dsign[5];

      xi[3]=xa;   yi[3]=ya;   zi[3]=za;
      xi[4]=xb;   yi[4]=yb;   zi[4]=zb;
      xi[5]=xc;   yi[5]=yc;   zi[5]=zc;
      dsign[3]=da; dsign[4]=db; dsign[5]=dc;

      xnj1 = xnf[0][nel_j];
      xnj2 = xnf[1][nel_j];
      xnj3 = xnf[2][nel_j];
      plac = pland[nel_j];     
    }

  for ( i=3;i<6;i++ )if ( dsign[i]< -epsma ) kdeb = i ;

  if ( kdeb == 3 )      {seg1n1 = 5 ; seg1n2 = 3 ;seg2n1 = 3 ; seg2n2 = 4; }
  else if ( kdeb == 4 ) {seg1n1 = 3 ; seg1n2 = 4 ;seg2n1 = 4 ; seg2n2 = 5; }
  else                  {seg1n1 = 4 ; seg1n2 = 5 ;seg2n1 = 5 ; seg2n2 = 3; }


  denom = xnj1*(xi[seg1n2]-xi[seg1n1])
         +xnj2*(yi[seg1n2]-yi[seg1n1])
         +xnj3*(zi[seg1n2]-zi[seg1n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg1n1]+ xnj2*yi[seg1n1]+ xnj3*zi[seg1n1]+ plac ;
      alfa  = - numer/denom - epsd;

      pa[0]= xi[seg1n1]+ alfa*(xi[seg1n2]-xi[seg1n1]);
      pa[1]= yi[seg1n1]+ alfa*(yi[seg1n2]-yi[seg1n1]);
      pa[2]= zi[seg1n1]+ alfa*(zi[seg1n2]-zi[seg1n1]);
    }

  denom = xnj1*(xi[seg2n2]-xi[seg2n1])
         +xnj2*(yi[seg2n2]-yi[seg2n1])
         +xnj3*(zi[seg2n2]-zi[seg2n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg2n1]+ xnj2*yi[seg2n1]+ xnj3*zi[seg2n1]+ plac ;
      alfa  = - numer/denom + epsd;

      pb[0]= xi[seg2n1]+ alfa*(xi[seg2n2]-xi[seg2n1]);
      pb[1]= yi[seg2n1]+ alfa*(yi[seg2n2]-yi[seg2n1]);
      pb[2]= zi[seg2n1]+ alfa*(zi[seg2n2]-zi[seg2n1]);
    }

  if ( kdeb == 3)       { kdebm1=5 ; kdebp1=4;}
  else if ( kdeb == 4 ) { kdebm1=3 ; kdebp1=5;}
  else                  { kdebm1=4 ; kdebp1=3;}

  diag1 = (xi[kdebp1]-pa[0])*(xi[kdebp1]-pa[0]) +
          (yi[kdebp1]-pa[1])*(yi[kdebp1]-pa[1]) +
          (zi[kdebp1]-pa[2])*(zi[kdebp1]-pa[2])      ;
  diag2 = (xi[kdebm1]-pb[0])*(xi[kdebm1]-pb[0]) +
          (yi[kdebm1]-pb[1])*(yi[kdebm1]-pb[1]) +
          (zi[kdebm1]-pb[2])*(zi[kdebm1]-pb[2])      ;

  if ( diag1 < diag2 ) 
    {
      xq[0]=pa[0]     ;  yq[0]=pa[1]     ;  zq[0]=pa[2]     ;
      xq[1]=pb[0]     ;  yq[1]=pb[1]     ;  zq[1]=pb[2]     ;
      xq[2]=xi[kdebp1];  yq[2]=yi[kdebp1];  zq[2]=zi[kdebp1];
      xq[3]=xi[kdebm1];  yq[3]=yi[kdebm1];  zq[3]=zi[kdebm1];
    }
  else
    {
      xq[0]=xi[kdebm1];  yq[0]=yi[kdebm1];  zq[0]=zi[kdebm1];
      xq[1]=pa[0]     ;  yq[1]=pa[1]     ;  zq[1]=pa[2]     ;
      xq[2]=pb[0]     ;  yq[2]=pb[1]     ;  zq[2]=pb[2]     ;
      xq[3]=xi[kdebp1];  yq[3]=yi[kdebp1];  zq[3]=zi[kdebp1];
    }



  xnj1 = xnf[0][nel_j];
  xnj2 = xnf[1][nel_j];
  xnj3 = xnf[2][nel_j];
  plac = pland[nel_j];   
  
  if ( code_decoupe == -5 ) /* il faut penser a inverser les normales */
    {
      xnj1 = xnf[0][nel_i];
      xnj2 = xnf[1][nel_i];
      xnj3 = xnf[2][nel_i];
      plac = pland[nel_i];     
    }

  for ( i=0;i<3;i++ )if ( dsign[i]> epsma ) kdeb = i ;

  if ( kdeb == 0 )      {seg1n1 = 2 ; seg1n2 = 0 ;seg2n1 = 0 ; seg2n2 = 1; }
  else if ( kdeb == 1 ) {seg1n1 = 0 ; seg1n2 = 1 ;seg2n1 = 1 ; seg2n2 = 2; }
  else                  {seg1n1 = 1 ; seg1n2 = 2 ;seg2n1 = 2 ; seg2n2 = 0; }


  denom = xnj1*(xi[seg1n2]-xi[seg1n1])
         +xnj2*(yi[seg1n2]-yi[seg1n1])
         +xnj3*(zi[seg1n2]-zi[seg1n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg1n1]+ xnj2*yi[seg1n1]+ xnj3*zi[seg1n1]+ plac ;
      alfa  = - numer/denom + epsd ;

      pa[0]= xi[seg1n1]+ alfa*(xi[seg1n2]-xi[seg1n1]);
      pa[1]= yi[seg1n1]+ alfa*(yi[seg1n2]-yi[seg1n1]);
      pa[2]= zi[seg1n1]+ alfa*(zi[seg1n2]-zi[seg1n1]);
    }

  denom = xnj1*(xi[seg2n2]-xi[seg2n1])
         +xnj2*(yi[seg2n2]-yi[seg2n1])
         +xnj3*(zi[seg2n2]-zi[seg2n1]);
  if ( fabs(denom) > eps )
    {
      numer =  xnj1*xi[seg2n1]+ xnj2*yi[seg2n1]+ xnj3*zi[seg2n1]+ plac ;
      alfa  = - numer/denom - epsd ;

      pb[0]= xi[seg2n1]+ alfa*(xi[seg2n2]-xi[seg2n1]);
      pb[1]= yi[seg2n1]+ alfa*(yi[seg2n2]-yi[seg2n1]);
      pb[2]= zi[seg2n1]+ alfa*(zi[seg2n2]-zi[seg2n1]);
    }

  if ( kdeb == 0)       { kdebm1=2 ; kdebp1=1;}
  else if ( kdeb == 1 ) { kdebm1=0 ; kdebp1=2;}
  else                  { kdebm1=1 ; kdebp1=0;}

  if ( kdeb == 0)       { kdebm1=2 ; kdebp1=1;}
  else if ( kdeb == 1 ) { kdebm1=0 ; kdebp1=2;}
  else                  { kdebm1=1 ; kdebp1=0;}

  xt[0]=pa[0];     yt[0]=pa[1];     zt[0]=pa[2];
  xt[1]=xi[kdeb];  yt[1]=yi[kdeb];  zt[1]=zi[kdeb];
  xt[2]=pb[0];     yt[2]=pb[1];     zt[2]=pb[2];


}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | bary_cache_3d                                                        |
  |                                                                      |
  |======================================================================| */
int bary_cache_3d(double xi[],double yi[],double zi[],
		  double xn1,double yn1,double zn1)
{
  double xg1,yg1,zg1,xg2,yg2,zg2,untier;
  double xgg,ygg,zgg,xn,epsi;

  epsi=1.E-4;
  untier=1./3.;

  xg1=(xi[0]+xi[1]+xi[2])*untier;
  yg1=(yi[0]+yi[1]+yi[2])*untier;
  zg1=(zi[0]+zi[1]+zi[2])*untier;

  xg2=(xi[3]+xi[4]+xi[5])*untier;
  yg2=(yi[3]+yi[4]+yi[5])*untier;
  zg2=(zi[3]+zi[4]+zi[5])*untier;

  xgg=xg2-xg1; ygg=yg2-yg1; zgg=zg2-zg1;
  xn=sqrt(xgg*xgg+ygg*ygg+zgg*zgg);
  xgg /=xn; ygg/=xn; zgg /=xn;

  if (xgg*xn1+ygg*yn1+zgg*zn1 < epsi)
    return(1);
  else
    return(0);

}
