/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>
#include <string.h>

#include "syr_usertype.h"
#include "syr_bd.h"
#include "syr_option.h"
#include "syr_const.h"
#include "syr_proto.h"
#include "syr_parall.h"

#define LISTE_MAT 1

#include "syr_hmt_bd.h"
#include "syr_hmt_libmat.h"
#include "syr_hmt_proto.h"

extern struct Performances perfo;
extern struct Affichages affich;
extern FILE  *fdata;
extern int nbVar;

static char ch[CHLONG],motcle[CHLONG];
static int ilist[MAX_REF];

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Initialisation des constantes pour le bati                           |
  |======================================================================| */
void hmt_init(struct Maillage maillnodes, struct ConstPhyhmt *constphyhmt,
	      struct ConstMateriaux **constmateriaux,struct Humid *humid,
	      double ***tmps_ele)
{
  int i;

  /* allocations */
  *constmateriaux=(struct ConstMateriaux*)malloc(NB_MAT*sizeof(struct ConstMateriaux));
  humid->mat=(int*)malloc(maillnodes.nelem*sizeof(int));
  for (i=0;i<maillnodes.nelem;i++) humid->mat[i]=-1;

  *tmps_ele=(double**)malloc(3*sizeof(double*));
  for (i=0;i<nbVar;i++) (*tmps_ele)[i]=(double*)malloc(maillnodes.nelem*sizeof(double));
  verif_alloue_double2d(nbVar,"syrthes",(*tmps_ele));
  

  /* initialisation des constantes independantes des materiaux */
  hmt_iniconstphyhmt(constphyhmt);

  /* initialisation des constantes materiaux */
  hmt_iniconstmat(*constmateriaux,*constphyhmt);

  /* affectation des materiaux */
  hmt_lire_materiaux(maillnodes,*humid);
  user_hmt_affectmat(maillnodes,*humid);
  hmt_verif_affectmat(maillnodes,*humid);

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Initialisation des constantes pour le bati                           |
  |======================================================================| */
void hmt_iniconstphyhmt(struct ConstPhyhmt *constphyhmt)
{
  constphyhmt->rhol=1000;      /* Masse volumique du liquide                  */
  constphyhmt->rhot=1.2;     /* Masse volumique de l'air                    */
  constphyhmt->R=8.314;        /* Constante des gaz parfaits                  */
  constphyhmt->xmv=0.018;      /* Masse molaire de la vapeur                  */
  constphyhmt->xmas=0.029;     /* Masse molaire de l'air sec                  */
  constphyhmt->Rv=461.889;     /* Constante massique de la vapeur             */
  constphyhmt->Ras=286.69;     /* Constante massique de l'air sec             */
  constphyhmt->Cpv=2050;       /* Capacite calorifique massique de la vapeur  */
  constphyhmt->Cpas=1006;       /* Capacite calorifique massique de l'air sec  */
  constphyhmt->Cpl=4180;       /*  Capacite calorifique massique de l'eau     */
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Initialisation des constantes des materiaux                          |
  |======================================================================| */
void hmt_iniconstmat(struct ConstMateriaux *constmateriaux,
		     struct ConstPhyhmt constphyhmt)
{
  int n;

  for (n=0;n<NB_MAT;n++)
    (*fmat_const[n])(&(constmateriaux[n]),constphyhmt);
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture des references des materiaux                                 |
  |======================================================================| */
void hmt_lire_materiaux(struct Maillage maillnodes,struct Humid humid)
{
  int i,j;
  int i1,i2,i3,i4,id,ii,nb,nr,n,nummat;
  
  if (syrglob_nparts==1 ||syrglob_rang==0)
    if (SYRTHES_LANG == FR)  
      printf("\n *** LECTURE DES MATERIAUX DANS LE FICHIER DE DONNEES\n");
    else if (SYRTHES_LANG == EN)
      printf("\n *** READING OF MATERIAL IN THE DATA FILE\n");
  


  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"HMT_MAT")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      nummat=-1;
	      for (j=0;j<NB_MAT;j++)
		if (!strcmp(motcle,liste_mat[j]))
		  nummat=j;

	      if (nummat<0){
		if (SYRTHES_LANG == FR)
		  printf("\n\n ERROR lire_materiaux : <%s>  materiau inconnu\n\n",motcle);
		else if (SYRTHES_LANG == EN)
		  printf("\n\n ERROR lire_materiaux : <%s>  unknown material\n\n",motcle);
		exit(1);
	      }

	      rep_listint(ilist,&nb,ch+id);
	      if (syrglob_nparts==1 ||syrglob_rang==0)
		{
		  if (SYRTHES_LANG == FR)
		    printf("        --> materiau <%s> impose sur les references",motcle);
		  else if (SYRTHES_LANG == EN)
		    printf("        --> <%s> material imposed on the references",motcle);
		  for (n=0;n<nb;n++) printf(" %d",ilist[n]); printf("\n");
		}

	      for (n=0;n<nb;n++)
		{
		  nr=ilist[n];
		  if (nr==-1)
		    for (i=0;i<maillnodes.nelem;i++) humid.mat[i]=nummat;
		  else
		    for (i=0;i<maillnodes.nelem;i++) 
		      if (maillnodes.nrefe[i]==nr) humid.mat[i]=nummat;
		}


	    }
	}
}


}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture des references des materiaux                                 |
  |======================================================================| */
void hmt_verif_affectmat(struct Maillage maillnodes,struct Humid humid)
{
  int i,ok=1;

  for (i=0;i<maillnodes.nelem;i++) 
    if (humid.mat[i]<0) ok=0;

  if (!ok)
    {
      if (syrglob_nparts==1 ||syrglob_rang==0)
	if (SYRTHES_LANG == FR)  
	  printf("\n\n ERROR hmt_verif affectmat : il y a des elements pour lesquels aucun materiau n'a ete defini\n\n");
	else if (SYRTHES_LANG == EN)
	  printf("\n\n ERROR hmt_verif affectmat : some elements have no material affected\n\n");
      
      exit(1);
    }
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Fonctions non dependantes directement des materiaux                  |
  |======================================================================| */

/*=======================================*/
/* Chaleur latente d'evaporation         */
/*=======================================*/
/* avec t en  Kelvin                     */
double fphyhmt_fxl(struct ConstPhyhmt constphyhmt,double t)
{
  double a,b;
  a=17.4/(t-33.15);
  b=17.4*(t-tkel)/(t-33.15)/(t-33.15);
  return constphyhmt.R*t*t*(a-b)/(constphyhmt.xmv);
}

/*=======================================*/
/* Derivee chaleur latente d'evaporation */
/*=======================================*/
double fphyhmt_fdl(struct ConstPhyhmt constphyhmt,double t)
{
  return constphyhmt.R*t/constphyhmt.xmv*(34.8/(t-33.15)-34.8*(t-tkel)/pow((t-33.15),2))
    + constphyhmt.R*t*t/constphyhmt.xmv*(-34.8/pow((t-33.15),2)+34.8*(t-tkel)/pow((t-33.15),3));
}

/*=======================================*/
/* Pression de vapeur saturante          */
/*=======================================*/
double fphyhmt_fpsat(double t)
{
  //  return exp(23.5771-4042.9/(t-37.58));
  return exp( 17.4*(t-tkel)/(t-33.15) + 6.41 );

}


/*=======================================*/
/* Viscosite dynamique de la vapeur      */
/*=======================================*/
double fphyhmt_fxmg(double t)
{
  return (17.1e-6)*(sqrt(t/tkel))*((1+123.6/tkel)/(1+123.6/t));
}

/*=======================================*/
/* Viscosite dynamique de l'eau          */
/*=======================================*/
double fphyhmt_fxml(double t)
{
  return (1e14)*(pow(t,-6.92854));
}

/*=======================================*/
/* Porosite du materiau humide           */
/*=======================================*/
double fphyhmt_feps(struct ConstPhyhmt constphyhmt,
		  struct ConstMateriaux constmat,
		  double tauv)
{
  return constmat.eps0-tauv/constphyhmt.rhol;
}

/*=======================================*/
/* Permeabilite de l'air                 */
/*=======================================*/
double fphyhmt_fxkt(struct ConstPhyhmt constphyhmt,
		  struct ConstMateriaux constmat,
		  double t,double pv,double pas,
		  double xkrg)
{
  return (constphyhmt.xmv*pv+constphyhmt.xmas*pas)*xkrg*constmat.xk/
    (constphyhmt.R*t*fphyhmt_fxmg(t)); 
}

/*=======================================*/
/* Permeabilite au liquide               */
/*=======================================*/
double fphyhmt_fxkl(struct ConstPhyhmt constphyhmt,
		  struct ConstMateriaux constmat,
		 double t,double xkrl) 
{
  return constphyhmt.rhol*constmat.xk*xkrl/fphyhmt_fxml(t);

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Fonctions dependantes des materiaux                                  |
  |======================================================================| */

#include "syr_hmt_libmat.c"
