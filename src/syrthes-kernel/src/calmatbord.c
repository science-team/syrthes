/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_hmt_libmat.h"
#include "syr_bd.h"
#include "syr_hmt_bd.h"
#include "syr_option.h"
#include "syr_abs.h"
#include "syr_tree.h"
#include "syr_const.h"
#include "syr_parall.h"

#include "syr_proto.h"
#include "syr_hmt_proto.h"

#ifdef _SYRTHES_CFD_
extern int nbCFD;
#endif

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des conditions aux limites implicites                |
  | trav0 est de la taille de nodeus (nelem*ndmat)                       |
  | on travaille et assemble uniquement sur nodeus                       |
  |======================================================================| */

void mafcli(struct Clim echang,struct Couple scoupr,
            struct Clim rayinf,struct Climcfd scoupf,
	    struct Maillage maillnodes,struct MaillageCL maillnodeus,
	    double *tmpsa,
	    double *res,double *trav0)
{
  int i,j,nf,ng,n,nel;
  int **nodeus;
  double hray,tr;


  nel=maillnodeus.nelem; 
  nodeus=maillnodeus.node;

  for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}

  /* trav0 est de la taille de nodeus (nelem*ndmat) */
  /* on travaille et assemble uniquement sur nodeus*/

  /*2.1 Termes de couplage thermique avec le fluide */
#ifdef _SYRTHES_CFD_
  if (scoupf.exist > 0) {
    for (n=0;n<nbCFD;n++)
      if (scoupf.nelem[n])
        for (j=0;j<scoupf.ndmat;j++)
          for (i=0;i<scoupf.nelem[n];i++)
            trav0[scoupf.nume[n][i]+nel*j]=scoupf.hfluid[n][i];
  }
#endif

  /*2.2 Termes du au coefficient d'echange */
  for (j=0;j<echang.ndmat;j++)
    for (i=0;i<echang.nelem;i++)
      trav0[echang.numf[i]+nel*j]=echang.val2[j][i];

  /*2.3 Termes du au rayonnement infini */
  for (j=0;j<rayinf.ndmat;j++)
    for (i=0;i<rayinf.nelem;i++)
      {
	nf=rayinf.numf[i]; ng=*(*(nodeus+j)+nf);
	tr=rayinf.val1[j][i];
	hray=rayinf.val2[j][i]*sigma*(tmpsa[ng]+tr+2*tkel)* 
	      ((tmpsa[ng]+tkel)*(tmpsa[ng]+tkel)+(tr+tkel)*(tr+tkel));
	trav0[nf+nel*j]+=hray;
      }

  /*2.3 Termes du au rayonnement  */
  if (scoupr.nelem)
    for (j=0;j<maillnodeus.ndmat;j++)
      for (i=0;i<scoupr.nelem;i++)
	{
	  nf=scoupr.numf[i]; ng=*(*(nodeus+j)+nf);
	  tr=scoupr.t[j][i];
	  hray=scoupr.h[j][i]*sigma*(tmpsa[ng]+tr+2*tkel)* 
	    ((tmpsa[ng]+tkel)*(tmpsa[ng]+tkel)+(tr+tkel)*(tr+tkel));
	  trav0[nf+nel*j]+=hray;
	}
/*   printf("impression dans MAFCLI\n"); */
/*   for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) printf("i=%d trav0=%f\n",i,trav0[i]); */

  /* calcul de la matrice elementaire */
  matbord(maillnodes,maillnodeus,trav0,res);

}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des conditions aux limites implicites pour les RC    |
  | trav0 est de la taille de nodeus (nelem*ndmat)                       |
  | on travaille et assemble uniquement sur nodeus                       |
  |======================================================================| */

void mafclc(struct Contact rescon,
	    struct Maillage maillnodes,struct MaillageCL maillnodeus,
	    double *tmpsa,
	    double *res,double *trav0)
{
  int i,j,nel;

  for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}
  nel=maillnodeus.nelem;

  /* trav0 est de la taille de nodeus (nelem*ndmat) */
  /* on travaille et assemble uniquement sur nodeus*/


  /* Termes du aux resistances de contact */
  for (j=0;j<rescon.ndmat;j++)
    for (i=0;i<rescon.nelem;i++)
      trav0[rescon.numf[i]+nel*j]=rescon.g[0][j][i];


  /* calcul de la matrice elementaire */
  matbord(maillnodes,maillnodeus,trav0,res);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2014 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des conditions aux limites implicites pour les RC    |
  | portant sur la variable T                                            |
  | trav0 est de la taille de nodeus (nelem*ndmat)                       |
  | on travaille et assemble uniquement sur nodeus                       |
  | en fait tmpsa ne sert pas                                            |
  |======================================================================| */

void hmt_mafclc_t(struct Contact rescon,
	    struct Maillage maillnodes,struct MaillageCL maillnodeus,
	    double *tmpsa,
	    double *res,double *trav0)
{
  int i,j,nel;

  for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}
  nel=maillnodeus.nelem;

  /* trav0 est de la taille de nodeus (nelem*ndmat) */
  /* on travaille et assemble uniquement sur nodeus*/


  /* Termes du aux resistances de contact */
  for (j=0;j<rescon.ndmat;j++)
    for (i=0;i<rescon.nelem;i++)
      trav0[rescon.numf[i]+nel*j]=rescon.g[0][j][i];


  /* calcul de la matrice elementaire */
  matbord(maillnodes,maillnodeus,trav0,res);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2014 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des conditions aux limites implicites pour les RC    |
  | portant sur la variable pv                                           |
  | trav0 est de la taille de nodeus (nelem*ndmat)                       |
  | on travaille et assemble uniquement sur nodeus                       |
  | en fait tmpsa ne sert pas                                            |
  |======================================================================| */

void hmt_mafclc_pv(struct Contact rescon,
	    struct Maillage maillnodes,struct MaillageCL maillnodeus,
	    double *tmpsa,
	    double *res,double *trav0)
{
  int i,j,nel;

  for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}
  nel=maillnodeus.nelem;

  /* trav0 est de la taille de nodeus (nelem*ndmat) */
  /* on travaille et assemble uniquement sur nodeus*/


  /* Termes du aux resistances de contact */
  for (j=0;j<rescon.ndmat;j++)
    for (i=0;i<rescon.nelem;i++)
      trav0[rescon.numf[i]+nel*j]=rescon.g[1][j][i];


  /* calcul de la matrice elementaire */
  matbord(maillnodes,maillnodeus,trav0,res);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2014 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des conditions aux limites implicites pour les RC    |
  | portant sur la variable pt                                           |
  | trav0 est de la taille de nodeus (nelem*ndmat)                       |
  | on travaille et assemble uniquement sur nodeus                       |
  | en fait tmpsa ne sert pas                                            |
  |======================================================================| */

void hmt_mafclc_pt(struct Contact rescon,
	    struct Maillage maillnodes,struct MaillageCL maillnodeus,
	    double *tmpsa,
	    double *res,double *trav0)
{
  int i,j,nel;

  for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}
  nel=maillnodeus.nelem;

  /* trav0 est de la taille de nodeus (nelem*ndmat) */
  /* on travaille et assemble uniquement sur nodeus*/


  /* Termes du aux resistances de contact */
  for (j=0;j<rescon.ndmat;j++)
    for (i=0;i<rescon.nelem;i++)
      trav0[rescon.numf[i]+nel*j]=rescon.g[2][j][i];


  /* calcul de la matrice elementaire */
  matbord(maillnodes,maillnodeus,trav0,res);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des conditions aux limites implicites                |
  | trav0 est de la taille de nodeus (nelem*ndmat)                       |
  | on travaille et assemble uniquement sur nodeus                       |
  |======================================================================| */

void hmt_mafcli_t(struct HmtClimhhh hmtclimhhh,struct Couple scoupr,
		  struct Clim rayinf,struct Climcfd scoupf,
		  struct Maillage maillnodes,struct MaillageCL maillnodeus,
		  double *tmpsa,
		  double *res,double *trav0)
{
  int i,j,nf,ng,n,nel;
  int **nodeus;
  double hray,tr;


  nel=maillnodeus.nelem; 
  nodeus=maillnodeus.node;

  for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}

  /* trav0 est de la taille de nodeus (nelem*ndmat) */
  /* on travaille et assemble uniquement sur nodeus*/

  /*2.1 Termes de couplage thermique avec le fluide */
#ifdef _SYRTHES_CFD_
  if (scoupf.exist > 0) {
    for (n=0;n<nbCFD;n++)
      if (scoupf.nelem[n])
        for (j=0;j<scoupf.ndmat;j++)
          for (i=0;i<scoupf.nelem[n];i++)
            trav0[scoupf.nume[n][i]+nel*j]=scoupf.hfluid[n][i];
  }
#endif

  /*2.2 Termes du au coefficient d'echange */
  for (j=0;j<hmtclimhhh.ndmat;j++)
    for (i=0;i<hmtclimhhh.nelem;i++)
      trav0[hmtclimhhh.numf[i]+nel*j]=hmtclimhhh.t_h[j][i];

  /*2.3 Termes du au rayonnement infini */
  for (j=0;j<rayinf.ndmat;j++)
    for (i=0;i<rayinf.nelem;i++)
      {
	nf=rayinf.numf[i]; ng=*(*(nodeus+j)+nf);
	tr=rayinf.val1[j][i];
	hray=rayinf.val2[j][i]*sigma*(tmpsa[ng]+tr+2*tkel)* 
	      ((tmpsa[ng]+tkel)*(tmpsa[ng]+tkel)+(tr+tkel)*(tr+tkel));
	trav0[nf+nel*j]+=hray;
      }

  /*2.3 Termes du au rayonnement  */
  if (scoupr.nelem)
    for (j=0;j<maillnodeus.ndmat;j++)
      for (i=0;i<scoupr.nelem;i++)
	{
	  nf=scoupr.numf[i]; ng=*(*(nodeus+j)+nf);
	  tr=scoupr.t[j][i];
	  hray=scoupr.h[j][i]*sigma*(tmpsa[ng]+tr+2*tkel)* 
	    ((tmpsa[ng]+tkel)*(tmpsa[ng]+tkel)+(tr+tkel)*(tr+tkel));
	  trav0[nf+nel*j]+=hray;
	}
/*   printf("impression dans MAFCLI\n"); */
/*   for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) printf("i=%d trav0=%f\n",i,trav0[i]); */

  /* calcul de la matrice elementaire */
  matbord(maillnodes,maillnodeus,trav0,res);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des conditions aux limites implicites                |
  | trav0 est de la taille de nodeus (nelem*ndmat)                       |
  | on travaille et assemble uniquement sur nodeus                       |
  |======================================================================| */

void hmt_mafcli_pv(struct HmtClimhhh hmtclimhhh,
		   struct Maillage maillnodes,struct MaillageCL maillnodeus,
		   struct Variable variable,struct ConstPhyhmt constphyhmt,
		   double *res,double *trav0)
{
  int i,j,nf,ng,n,nel;
  int **nodeus;
  double *pt;

  nel=maillnodeus.nelem; 
  nodeus=maillnodeus.node;

  pt=variable.var[variable.adr_pt_m1];

  for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}

  /* trav0 est de la taille de nodeus (nelem*ndmat) */
  /* on travaille et assemble uniquement sur nodeus*/


  /*2.2 Termes du au coefficient d'echange */
  for (j=0;j<hmtclimhhh.ndmat;j++)
    for (i=0;i<hmtclimhhh.nelem;i++)
      {
	nf=hmtclimhhh.numf[i]; ng=*(*(nodeus+j)+nf);

	trav0[hmtclimhhh.numf[i]+nel*j]=hmtclimhhh.pv_h[j][i];

	/* remplacer par ce qui suit si on veut les CL completes */
	/* trav0[hmtclimhhh.numf[i]+nel*j]=hmtclimhhh.pv_h[j][i]/pt[ng]; */

      }

  /* calcul de la matrice elementaire */
  matbord(maillnodes,maillnodeus,trav0,res);

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des conditions aux limites implicites                |
  | trav0 est de la taille de nodeus (nelem*ndmat)                       |
  | on travaille et assemble uniquement sur nodeus                       |
  |======================================================================| */

void hmt_mafcli_pt(struct HmtClimhhh hmtclimhhh,
		   struct Maillage maillnodes,struct MaillageCL maillnodeus,
		   struct Variable variable,struct ConstPhyhmt constphyhmt,
		   double *res,double *trav0)
{
  int i,j,nf,ng,n,nel;
  int **nodeus;
  double *pv,*pt;
  double omegamv,has,pp;

  pv=variable.var[variable.adr_pv_m1];
  pt=variable.var[variable.adr_pt_m1];

  nel=maillnodeus.nelem; 
  nodeus=maillnodeus.node;

  for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}

  /* trav0 est de la taille de nodeus (nelem*ndmat) */
  /* on travaille et assemble uniquement sur nodeus*/


  /*2.2 Termes du au coefficient d'echange */
  for (j=0;j<hmtclimhhh.ndmat;j++)
    for (i=0;i<hmtclimhhh.nelem;i++)
      {
	nf=hmtclimhhh.numf[i]; ng=*(*(nodeus+j)+nf);

	trav0[hmtclimhhh.numf[i]+nel*j] = hmtclimhhh.pt_h[j][i];

	/* remplacer par ce qui suit si on veut les CL completes */
	/* pp=hmtclimhhh.pt_ext[j][i];  */
	/* if (pt[ng]>hmtclimhhh.pt_ext[j][i]) pp=pt[ng]; */

	/* omegamv=constphyhmt.xmv*pv[ng]/ */
	/*   (constphyhmt.xmas*pp+(constphyhmt.xmv-constphyhmt.xmas)*pv[ng]); */

	/* trav0[hmtclimhhh.numf[i]+nel*j] = (1-omegamv)*hmtclimhhh.pt_h[j][i]; */

      }

  /* calcul de la matrice elementaire */
  matbord(maillnodes,maillnodeus,trav0,res);

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des conditions aux limites implicites                |
  |                                                                      |
  | trav0 est de la taille de nodeus (nelem*ndmat)                       |
  | on travaille et assemble uniquement sur nodeus                       |
  |======================================================================| */

void matbord(struct Maillage maillnodes,struct MaillageCL maillnodeus,
	     double *trav0,double *res)
{
  int i,nca,nel,nel2;
  int *ne0,*ne1,*ne2,**nodeus;
  double r1,r2,cl1,cl2,cl3;
  double s12=1./12.,sv12,s6=1./6.,sv6;
  double **coords;

  if (maillnodes.iaxisy==1) nca=1; else nca=0;
  nel=maillnodeus.nelem; nel2=nel*2;
  coords=maillnodes.coord;
  nodeus=maillnodeus.node;

  /* CALCUL DE LA MATRICE ELEMENTAIRE */
  if (maillnodeus.ndim==2 && maillnodeus.iaxisy==0)
    for (i=0,ne0=*(nodeus),ne1=*(nodeus+1);i<maillnodeus.nelem;i++,ne0++,ne1++)
      {
	sv6=s6*maillnodeus.volume[i];     
	cl1 =trav0[i]*sv6;
	cl2 =trav0[i+nel]*sv6;
	res[*ne0]+=2*cl1 + cl2;
	res[*ne1]+=2*cl2 + cl1;
      }
  else if (maillnodes.ndim==2)
    for (i=0,ne0=*(nodeus),ne1=*(nodeus+1);i<maillnodeus.nelem;i++,ne0++,ne1++)
      {
	sv12=s12*maillnodeus.volume[i];
	r1=fabs(*(*(coords+nca)+*ne0));
	r2=fabs(*(*(coords+nca)+*ne1));
	cl1 =trav0[i]*sv12; 
	cl2 =trav0[i+nel] *sv12;
	res[*ne0]+=cl1*(3*r1+r2)+cl2*(r1+r2);
	res[*ne1]+=cl1*(r1+r2)+cl2*(r1+3*r2);

      } 
  else 
    for (i=0,ne0=*(nodeus),ne1=*(nodeus+1),ne2=*(nodeus+2);i<maillnodeus.nelem;i++,ne0++,ne1++,ne2++)
      {
	sv12=s12*maillnodeus.volume[i];
	cl1 =trav0[i]*sv12; 
	cl2 =trav0[i+nel] *sv12; 
	cl3 =trav0[i+nel2]* sv12; 
	res[*ne0]+= 2*cl1 + cl2 + cl3; 
        res[*ne1]+= 2*cl2 + cl1 + cl3; 
        res[*ne2]+= 2*cl3 + cl1 + cl2; 
      }
}
