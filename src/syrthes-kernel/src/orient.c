/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "syr_usertype.h"
# include "syr_tree.h"
# include "syr_abs.h"
# include "syr_bd.h"
# include "syr_parall.h"
# include "syr_proto.h"
# include "syr_option.h"
/* # include "mpi.h" */

extern struct Performances perfo;
extern struct Affichages affich;

int nelvoip;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | orient2d                                                             |
  |                                                                      |
  |======================================================================| */
void orient2d (struct Maillage maillnodray,
	       struct Compconnexe volconnexe,int *grconv)
{
  int *grconx, *norini, *ifabor;
  int numg,i,ielem,nbmalo;

  ifabor = (int*)malloc(maillnodray.nelem*2*sizeof(int)); verif_alloue_int1d("orient2d",ifabor);
  grconx = (int*)malloc(maillnodray.nelem*sizeof(int));   verif_alloue_int1d("orient2d",grconx);
  perfo.mem_ray+=maillnodray.nelem*3 * sizeof(int);
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
 
  nbmalo = 0;
  
  voisic_2d(ifabor,maillnodray.node, maillnodray.nelem, maillnodray.npoin);
  
  connex_2d(ifabor,grconx,maillnodray.nelem,maillnodray.npoin,&numg,volconnexe.nb);
  
  /* On stockera autant d'elements de depart que de surfaces connexes  trouves */
  norini = (int *)malloc(numg *sizeof(int)); verif_alloue_int1d("orient2d",norini);
  perfo.mem_ray+=numg * sizeof(int);
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  
  iniori_2d(ifabor,maillnodray.node,maillnodray.coord,grconx,maillnodray.nelem,maillnodray.npoin,
	    volconnexe,numg,norini,grconv,&nbmalo);
  
  
  oriene_2d(ifabor,maillnodray.node,maillnodray.nelem,grconx,norini,numg,&nbmalo);
  
  
  /* 4- Post processing pour developpeur (IR,CP)
     ---------------------------------------------- */
  if (affich.ray_orient) 
    {
      for (i=0; i < 2*maillnodray.nelem; i++)  if (ifabor[i] != -1)  ifabor[i] += 1; 
      if (SYRTHES_LANG == FR)
	printf(" \n Table des elements colles aux faces de chaque element \n ");
      else if (SYRTHES_LANG == EN)
	printf(" \n Table of elements in contact with each element face \n ");

      for (ielem=0; ielem<maillnodray.nelem; ielem++)
	printf(" Element %d  :  %d %d  \n",ielem+1,ifabor[ielem],ifabor[ielem+maillnodray.nelem]);
      
      for (ielem=0; ielem<maillnodray.nelem; ielem++)
	printf(" Element %d  :  %d %d  \n",ielem+1,maillnodray.node[0][ielem],maillnodray.node[1][ielem]);
    }
  
  free(grconx);free(norini);
  perfo.mem_ray-=(maillnodray.nelem+numg) * sizeof(int);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | voisic_2d                                                            |
  |                                                                      |
  |======================================================================| */
void voisic_2d(int *ifabor,int **nodray,int nelray,int npoinr)
{    
    int i,m1,m2;
    int *i1,*i2,imax,npmax;
    int iface,iface2,ielem,ielem2;
    int *itrav,*n1,*n2;


    itrav = (int *)malloc(2*npoinr*sizeof(int)); verif_alloue_int1d("voisic_2d",itrav);
    perfo.mem_ray+=2*npoinr * sizeof(int);
    if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;

    /* 1- INITIALISATION 
       ==================== */
    for (i=0;i<2*npoinr;i++) *(itrav+i) = -1;
    for (i=0;i<2*nelray;i++) *(ifabor+i)=-1;

    for (ielem=0,i1=*nodray,i2=*(nodray+1);ielem<nelray;ielem++,i1++,i2++)
      {
	if(itrav[*i1]==-1)
	  {
	    itrav[*i1]=ielem;
	    if(itrav[*i2]==-1) itrav[*i2]=ielem;
	    else if(itrav[*i2+npoinr]==-1) itrav[*i2+npoinr]=ielem;
	    else 
	      {
		if (SYRTHES_LANG == FR)
		  printf("\n *** voisic_2d : erreur 1 pour element ielem %d i1=%d i2=%d \n",ielem,*i1,*i2);
		else if (SYRTHES_LANG == EN)
		  printf("\n *** voisic_2d : error 1 for element ielem %d i1=%d i2=%d \n",ielem,*i1,*i2);
	      }
	  }
	else if(itrav[*i1+npoinr]==-1)
	  {
	    itrav[*i1+npoinr]=ielem;
	    if(itrav[*i2]==-1) itrav[*i2]=ielem;
	    else if(itrav[*i2+npoinr]==-1) itrav[*i2+npoinr]=ielem;
	    else 
	      {
		if (SYRTHES_LANG == FR)
		  printf("\n *** voisic_2d : erreur 2 pour element ielem %d i1=%d i2=%d \n",ielem,*i1,*i2);
		else if (SYRTHES_LANG == EN)
		  printf("\n *** voisic_2d : error 2 for element ielem %d i1=%d i2=%d \n",ielem,*i1,*i2);
	      }
	  }
	else
	  {
	    if (SYRTHES_LANG == FR)
	      printf("\n *** VOISIC_2D : erreur 3 pour element ielem %d i1=%d i2=%d \n",ielem,*i1,*i2);
	    else if (SYRTHES_LANG == EN)
	      printf("\n *** voisic_2d : error 3 for element ielem %d i1=%d i2=%d \n",ielem,*i1,*i2);
	  }
      }
    
    if(affich.ray_orient)
      {
	if (SYRTHES_LANG == FR)
	  for (i=0;i<npoinr;i++)
	    printf("voisic_2d : noeud %d : element_1= %d element_2=%d \n",i,itrav[i],itrav[i+npoinr]);
	else if (SYRTHES_LANG == EN)
	  for (i=0;i<npoinr;i++)
	    printf("voisic_2d : node %d : element_1= %d element_2=%d \n",i,itrav[i],itrav[i+npoinr]);
      }

    for (ielem=0,i1=*nodray,i2=*(nodray+1);ielem<nelray;ielem++,i1++,i2++)
      {
	if(itrav[*i1]==ielem) ifabor[ielem]=itrav[*i1+npoinr];
	else ifabor[ielem]=itrav[*i1];
    
	if(itrav[*i2]==ielem) ifabor[ielem+nelray]=itrav[*i2+npoinr];
	else ifabor[ielem+nelray]=itrav[*i2];
      }



      /* 4- Post processing uniquement pour developpeur (IR,CP)
      ---------------------------------------------- */
     if (affich.ray_orient) 
      {
         for (i=0; i<2*nelray;i++)  if (ifabor[i]!= -1) ifabor[i]++; 

	 if (SYRTHES_LANG == FR)
	   printf(" voisic_2d : Table des elements colles aux faces de chaque element \n ");
	 else if (SYRTHES_LANG == EN)
	   printf(" voisic_2d : Table of elements in contact with each element face \n ");

         for (ielem=0; ielem < nelray; ielem++)
	   printf(" voisic_2d :Element %d  :  %d %d  \n",ielem,ifabor[ielem],ifabor[ielem+nelray]);

         for (i=0;i<2*nelray;i++)  if (ifabor[i]!=-1) ifabor[i]--; 
      }



     free(itrav);
     perfo.mem_ray-=2*npoinr * sizeof(int);


}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | connex_2d                                                            |
  | ifabor[nelem][extremite]=ifabor[j][i]=ifabor[j + i*nelray]           |
  |======================================================================| */
void connex_2d(int *ifabor,int *grconx,int nelray,int npoinr,
	       int *numg,int numgu) 
    
{
    int i;
    int iel1,iel2;

    for (i=0;i<nelray;i++) *(grconx+i)=0;

    iel1=0;
    iel2=-10;
    *numg=1;
    grconx[0]=*numg;

    for (i=0; i< 2;i++) 
      if (ifabor[iel1+i* nelray] != -1) iel2=ifabor[iel1+i* nelray];
       

    if (iel2==-10)
      {
	if (SYRTHES_LANG == FR)
	  printf(" $$ ATTENTION connexe_2d : Element %d isole \n" ,iel1+1);
	else if (SYRTHES_LANG == EN)
	  printf(" $$ Warning connex_2d : Element %d isolated \n" ,iel1+1);
      }
    else
      for (i=0; i< 2;i++)
	{
	  iel2=ifabor[iel1+i* nelray];
	  if (iel2==-1) continue;
	  if (grconx[iel2]!=0) continue;      
	  group_2d(iel1,iel2,nelray,grconx,ifabor);
	}

    for (i=0;i<nelray;i++)
      if (grconx[i]==0)
	{
          if (affich.ray_orient)
	    if (SYRTHES_LANG == FR)
	      printf(" connexe_2d : Le segment %d n'appartient pas encore a un groupe \n",i+1);
	    else if (SYRTHES_LANG == EN)
	      printf(" connexe_2d : segment %d does not belong to a group yet \n",i+1);

          iel1=i; iel2=-10; *numg += 1;
          grconx[i]=*numg;
	  
	  if(ifabor[iel1+nelray]+ifabor[iel1]==-2)
	    {
	      if (SYRTHES_LANG == FR)
		printf(" $$ ATTENTION connexe_2d : Element %d isole \n" ,iel1) ;
	      else if (SYRTHES_LANG == EN)
		printf(" $$ Warning connex_2d : Element %d isolated \n" ,iel1+1);
	      continue;
	    }
	  
          for (i=0; i< 2;i++)
	    {
	      iel2=ifabor[iel1+i*nelray];
	      if (iel2==-1) continue;
	      if (grconx[iel2]!=0) continue;      
	      group_2d(iel1,iel2,nelray,grconx,ifabor);
	      
	    }   /* fin de la boucle sur tous les cotes */
	  
        }      /* Fin du test sur l'appartenance a un groupe */
    
  
    if (syrglob_nparts==1 || syrglob_rang==0)
      if (SYRTHES_LANG == FR)
	printf("\n *** connexe_2d : Le maillage surfacique contient %d surfaces connexes et %d volumes connexes \n",
	       *numg,numgu);
      else if (SYRTHES_LANG == EN)
	printf("\n *** connexe_2d : The surfacique mesh contains %d  connex surfaces and %d connex volumes  \n",
	       *numg,numgu);
    
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | group_2d                                                             |
  |                                                                      |
  |======================================================================| */
void group_2d(int iel1,int iel2,int nelray,int *grconx,int *ifabor)

{
    int i;
    int ielc,ielv;

    grconx[iel2]=grconx[iel1];

/*    printf(" dans group2 : iel1+1=%d iel2+1=%d \n",iel1+1,iel2+1); */
/*    printf(" grconx[iel1]+1 : %d",grconx[iel1]); */

    for (i=0; i< 2;i++)
    {
      ielv=ifabor[iel2+i* nelray];
      if (ielv != -1 && grconx[ielv]==0)
          group_2d(iel2,ielv,nelray,grconx,ifabor);
    }
}
    
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | oriene_2d                                                            |
  |                                                                      |
  |======================================================================| */
void oriene_2d(int *ifabor,int **nodray,int nelray,
	       int *grconx,int *norini,int numg,int *nbmalo)

{
  int i,ip,iel1,iel2;
  int ifp,ielem;
  
  for (i=0;i<numg;i++) 
    {        
      iel1=norini[i];
      
      ifp=0;
      ip=nodray[0][iel1];
      iel2=ifabor[iel1];
      if (iel2!=-1 && grconx[iel2]>0) 
	rorien_2d(ifp,ip,iel2,ifabor,grconx,nodray,nelray,nbmalo);

      ifp=1;
      ip=nodray[1][iel1];
      iel2=ifabor[iel1+nelray];
      if (iel2!=-1 && grconx[iel2]>0) 
	rorien_2d(ifp,ip,iel2,ifabor,grconx,nodray,nelray,nbmalo);
    }
  
  if (syrglob_nparts==1 || syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf("\n *** oriene_2d : Le nombre de facettes reorientees est : %d \n",*nbmalo);
    else if (SYRTHES_LANG == EN)
      printf("\n *** oriene_2d : The number of reoriented faces is : %d \n",*nbmalo);
}  


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | rorien_2d                                                            |
  |                                                                      |
  |======================================================================| */
void rorien_2d(int ifp,int ip,int iel2,int *ifabor,int *grconx,
	       int **nodray,int nelray,int *nbmalo)

{ 
  int j,jp;
  int ielv;
  int lmalo,nodaux,neleaux;
  int somfac[6];
  
  lmalo=0;
  
  jp=nodray[ifp][iel2];
             
  if (ip==jp) lmalo=1;

  if (lmalo==1)
    {
      *nbmalo += 1;
      nodaux=nodray[0][iel2];
      nodray[0][iel2]=nodray[1][iel2]; 
      nodray[1][iel2]=nodaux;
      
      neleaux=ifabor[iel2];
      ifabor[iel2]=ifabor[iel2+nelray ];
      ifabor[iel2+nelray]=neleaux;
      
      grconx[iel2]=- grconx[iel2];
      
      if (affich.ray_orient) 
	{
	  if (SYRTHES_LANG == FR)
	    printf("rorien_2d : La facette %d etait mal orientee \n",iel2);
	  else if (SYRTHES_LANG == EN)
	    printf("rorien_2d : Face %d was wrongly oriented \n",iel2);
	}
    }
  else
    {
      grconx[iel2]=- grconx[iel2];    
    }     
  

  jp=nodray[ifp][iel2];            
  ielv=ifabor[iel2 + ifp* nelray];
  if (ielv != -1 && grconx[ielv]>0)
    rorien_2d(ifp,jp,ielv,ifabor,grconx,nodray,nelray,nbmalo);     
  
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | iniori_2d                                                            |
  |        Initialisation des processus recursifs                        |
  |======================================================================| */
void iniori_2d(int *ifabor,int **nodray,double **coordray,
            int *grconx,int nelray,int npoinr,
            struct Compconnexe volconnexe,int numg,int *norini,
	    int *grconv,int *nbmalo)

{
  int i,j,k,ref,ngroup,imin;
  int n1,n2,*ne1,*ne2;
  int iel,nodaux,neleaux;
  int iv,pintok,dejaunpoint;
  double xint,yint;
  double xx,yy,di1,di2,xi1,xi2,dmin,x1,y1,x2,y2;
  double xnorelx,xnorely,xnormel;
  double xvecgix,xvecgiy,xnormve,xvec1ix,xvec1iy;
  double pscal,xxg,yyg,dist;
  int *norinit,*npvint,*ncompteur;
  double eps=1e-6;
  

  norinit=(int *)malloc(numg*volconnexe.nb*sizeof(int)); verif_alloue_int1d("inioric_2d",norinit);
  npvint=(int *)malloc(numg*sizeof(int));                verif_alloue_int1d("inioric_2d",npvint);
  ncompteur=(int *)malloc(numg*sizeof(int));             verif_alloue_int1d("inioric_2d",ncompteur);
  perfo.mem_ray+=(numg*volconnexe.nb+2*numg) * sizeof(int);
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  
  for (i=0; i<nelray;i++) *(grconv+i)=0;
  for (i=0; i<numg;i++) *(npvint+i)=0;
  for (i=0; i<numg;i++) *(ncompteur+i)=0;
  for (i=0; i<numg*volconnexe.nb;i++) *(norinit+i)=0;
  
  if (affich.ray_orient)
    {
      for (i=0;i<nelray;i++) printf(" iniori_2D : Element %d grconx : %d \n",i,grconx[i]);
      printf("    \n");
      for (i=0;i<npoinr;i++)
	{
	  if (SYRTHES_LANG == FR)
	    printf(" iniori_2d : Noeud %d  x y :   %f %f \n",
		   i+1,coordray[0][i],coordray[1][i]);
	  else if (SYRTHES_LANG == EN)
	    printf(" iniori_2d : Node %d  x y :   %f %f \n",
		   i+1,coordray[0][i],coordray[1][i]);
	}
    }
  
  /* Boucle sur les surfaces connexes */
  for (j=0;j<numg;j++)
    {
      /* boucle sur les points volumiques internes */
      for (iv=0;iv<volconnexe.nb;iv++)
	{
	  xint=volconnexe.x[iv]; yint=volconnexe.y[iv];
	  dmin=1.E6; dist=1.E+6;
	  for (i=0,ne1=*nodray,ne2=*(nodray+1);i<nelray;i++,ne1++,ne2++)
	    if (grconx[i]-1==j)
	      /* On se restreint ici a la composante connexe j  vis a vis du point volumique iv */
	      {
		x1=coordray[0][*ne1]; y1=coordray[1][*ne1];
		x2=coordray[0][*ne2]; y2=coordray[1][*ne2];
		xxg=(x1+x2)/2.; yyg=(y1+y2)/2.;
		/* On commence par se baser sur un test peu fiable 
		   la distance minimal G-PINT*/
		dist=(xxg-xint)*(xxg-xint)+(yyg-yint)*(yyg-yint);
		if (dist<dmin) {dmin=dist;imin=i;}
	      }
	  
	  if (dist<eps*eps*1.)
	    {
	      if (SYRTHES_LANG == FR)
		printf("\n *** iniori_2d : Point interne %d mal choisi : sur la facette %d \n",iv+1,imin+1);
	      else if (SYRTHES_LANG == EN)
		printf("\n *** iniori_2d : Internal point %d badly chosen : on face %d \n",iv+1,imin+1);
	      syrthes_exit(1);   
	    }
	  
	  /* La facette potentielle que l'on va tester est imin */
	  i= imin;
	  n1=nodray[0][imin]; n2=nodray[1][imin];                  
	  x1=coordray[0][n1]; y1=coordray[1][n1];
	  x2=coordray[0][n2]; y2=coordray[1][n2];
	  xxg=(x1+x2)/2.;     yyg=(y1+y2)/2.;
	  xnorelx=-y2+y1;     xnorely= x2-x1;
	  xnormel=sqrt(xnorelx*xnorelx + xnorely*xnorely);
	  if (xnormel>-eps && xnormel<eps)
	    {
	      if (SYRTHES_LANG == FR)
		printf("\n *** iniori_2d : L'element %d est vraisemblablement degenere (applati)\n",i+1); 
	      else if (SYRTHES_LANG == EN)
		printf("\n *** iniori_2d : Element %d is likely to be degenerated (flat)\n",i+1); 
	      syrthes_exit(1);	 
	    }
	  else
	    {xnorelx=xnorelx / xnormel; xnorely=xnorely / xnormel;}
	  
	  xvecgix=xint-xxg; xvecgiy=yint-yyg;
	  xnormve=sqrt(xvecgix*xvecgix + xvecgiy*xvecgiy);
	  xvecgix/=xnormve; xvecgiy/=xnormve;
	  
	  pscal=xnorelx*xvecgix + xnorely*xvecgiy;
	  if (pscal>-eps && pscal<eps)
	    {
	      if (SYRTHES_LANG == FR)
		printf("\n *** iniori_2d : Le point interne %d de coordonnees xc=%f , yc=%f  \n"
		       "                   est mal choisi, car dans le plan de la facette %d \n",
		       j+1,xint,yint,i+1);
	      else if (SYRTHES_LANG == EN)
		printf("\n *** iniori_2d : Internal Point %d with coordinates xc=%f , yc=%f  \n"
		       "                   is badly chosen, it belongs to the plane of face %d \n",
		       j+1,xint,yint,i+1);
	      syrthes_exit(1);
	    }
	  
	  /* Verif de non recoupage de la surface connexe j */
	  xvecgix*=xnormve; xvecgiy*=xnormve;
	  for (k=0;k<nelray;k++)
	    {
	      if (grconx[k]-1==j && k!=i)
		{
		  n1=nodray[0][k];   n2=nodray[1][k];                  
		  x1=coordray[0][n1]; y1=coordray[1][n1];
		  x2=coordray[0][n2]; y2=coordray[1][n2];
		  
		  if (racines_2d(x1,y1,x2,y2,xvecgix,xvecgiy,xxg,yyg,k))
		    {
		      if (SYRTHES_LANG == FR)
			printf("\n *** iniori_2d : Il y a une ambiguite --> changer la position du point %d \n",iv+1);
		      else if (SYRTHES_LANG == EN)
			printf("\n *** iniori_2d : Ambiguity  --> Please change the position of point %d \n",iv+1);
		      syrthes_exit(1);
		    }
		}
	    }
	  
	  /* Stockage tous les elements correspondants au couple iv et surface connexe j */
	  norinit[iv +j*volconnexe.nb]=imin;
	  if (affich.ray_orient) 
	    for (k=0;k<numg*volconnexe.nb;k++) printf (" iniori_2d : norinit[ %d ]=%d \n",k+1,norinit[k]+1);
	}
    }
  
  if (affich.ray_orient)
    {
      printf("   \n");
      for (j=0;j<numg;j++)
	{
	  printf("  \n");
	  if (SYRTHES_LANG == FR)
	    {
	      for (iv=0;iv<volconnexe.nb;iv++)
		printf(" iniori_2d : composante connexe %d , face initiale %d pour le noeud volumique %d  \n",
		       j+1,norinit[ iv +j* volconnexe.nb]+1,iv+1);
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      for (iv=0;iv<volconnexe.nb;iv++)
		printf(" iniori_2d : connex set %d , initial face %d for the internal point  %d  \n",
		       j+1,norinit[ iv +j* volconnexe.nb]+1,iv+1);
	    }
	}
    }
  
  /* Determination du point interieur iv qui convient a la surface connexe j
     Recoupage pair des autres surfaces connexes necessaire
  */
  
  for (j=0;j<numg;j++)
    {	  
      dejaunpoint=0;
      for (iv=0;iv<volconnexe.nb;iv++)
	{
	  xint=volconnexe.x[iv]; yint=volconnexe.y[iv];
	  for (i=0; i<numg; i++) ncompteur[i]=0;
	  iel=norinit[iv + j* volconnexe.nb];
	  n1=nodray[0][iel];  n2=nodray[1][iel];                  
	  x1=coordray[0][n1]; y1=coordray[1][n1];
	  x2=coordray[0][n2]; y2=coordray[1][n2];
	  xxg=(x1+x2)/2.;     yyg=(y1+y2)/2.;
	  xvecgix=xint-xxg;   xvecgiy=yint-yyg;
	  
	  for (k=0;k<nelray;k++)
	    {
	      if (grconx[k]-1 != j)
		{
		  n1=nodray[0][k];   n2=nodray[1][k];                  
		  x1=coordray[0][n1]; y1=coordray[1][n1];
		  x2=coordray[0][n2]; y2=coordray[1][n2];
		  
		  if (affich.ray_orient)
		    if (SYRTHES_LANG == FR)
		      printf(" comp connexe %d face depart %d noeud volumique %d facette ocultrice k=%d \n",
			     j+1,iel+1,iv+1,k+1);
		    else if (SYRTHES_LANG == EN)
		      printf(" connex comp %d starting face %d internal node %d masking face k=%d \n",
			     j+1,iel+1,iv+1,k+1);

		  if (racines_2d(x1,y1,x2,y2,xvecgix,xvecgiy,xxg,yyg,k))
		    {
		      if (affich.ray_orient)
			{
			  if (SYRTHES_LANG == FR)
			    {
			      printf(" iniori_2d : Intersection avec une autre surface connexe (face %d)",k+1);
			      printf(" composante connexe %d \n",grconx[k]-1);
			    }
			  else if (SYRTHES_LANG == EN)
			    {
			      printf(" iniori_2d : Intersection with another connexe surface (face %d)",k+1);
			      printf(" connex set %d \n",grconx[k]-1);
			    }
			}
		      ncompteur[grconx[k]-1] += 1;
		    }
		}
	      
	    }

	  if (affich.ray_orient)
	    if (SYRTHES_LANG == FR)
	      printf("iniori_2d : composante connexe j=%d noeud interieur iv=%d \n",j+1,iv+1);
	    else if (SYRTHES_LANG == EN)
	      printf("iniori_2d : connexe set j=%d internal node iv=%d \n",j+1,iv+1);

	  if (affich.ray_orient) 
	    {
	      if (SYRTHES_LANG == FR)
		for (i=0;i<numg;i++) printf(" iniori_2d : ncompteur[ %d ]=%d \n",i+1,ncompteur[i]);
	      else if (SYRTHES_LANG == EN)
		for (i=0;i<numg;i++) printf(" iniori_2d : ncompteur[ %d ]=%d \n",i+1,ncompteur[i]);		
	    }

	  pintok =1;
	  /* flag  des intersections impaires */
	  for (i=0; i<numg;i++)
	      if ((ncompteur[i]/2)*2-ncompteur[i]!=0) pintok=0;

	  if (pintok==0)
	    {
	      if (affich.ray_orient) 
		if (SYRTHES_LANG == FR)
		  printf(" iniori_2d : le point interieur %d non ok pour la surface connexe  %d \n",iv+1,j+1);
		else if (SYRTHES_LANG == EN)
		  printf(" iniori_2d : internal point %d not ok for connex surface  %d \n",iv+1,j+1);
	    }
	  else
	    { 
	      if(affich.ray_orient)
		printf(" iniori_2D : le point interieur %d semble ok pour la surface connexe  %d \n",iv+1,j+1);
	      if (dejaunpoint==1)
		{
		  if (SYRTHES_LANG == FR)
		    {
		      printf("\n *** iniori_2d : Ce volume est :\n");
		      printf("                   soit deja defini par un point precedent \n");
		      printf("                   soit une ambiguite existe sur le volume ---> ");
		      printf("                   a verifier tres soigneusement \n");
		      printf("                   --> essayer de modifier la position du noeud interieur %d \n",iv+1);
		    }
		  else if (SYRTHES_LANG == EN)
		    {
		      printf("\n *** iniori_2d : This volume is :\n");
		      printf("                   either already defined by a previous point \n");
		      printf("                   either an ambiguity exists on the volume ---> ");
		      printf("                   check very carefully please \n");
		      printf("                   --> try to modify the internal point position %d \n",iv+1);
		    }
		  syrthes_exit(1);
		}
	      else	  
		{norini[j]=iel;npvint[j]=iv;dejaunpoint=1;}
	    }
	}
      
      if (dejaunpoint==0) 
	{		
	  if (SYRTHES_LANG == FR)
	    {
	      printf("\n *** iniori_2d : Il y a un probleme pour la surface connexe j= %d\n" ,j+1);
	      printf("                   Aucun des points fournis par l'utilisateur  ne permet de la definir \n");
	      printf("                   Verifier bien que tous les volumes independants ont ");
	      printf("                   ete definis par un point \n");
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      printf("\n *** iniori_2d : There is a problem for connex surface j= %d\n" ,j+1);
	      printf("                   None of the point given by user allows to define it \n");
	      printf("                   Please check carefully that all independent volumes have ");
	      printf("                   been defined by an internal point \n");
	    }
	  syrthes_exit(1);
	}
      
    }
  
  if (affich.ray_orient)
    {
      printf("\n");
      if (SYRTHES_LANG == FR)
	for (i=0;i<numg;i++) printf(" *** iniori_2d : Composante connexe %d face de depart %d point interieur %d\n",
				    i+1,norini[i]+1,npvint[i]+1);
      else if (SYRTHES_LANG == EN)
      	for (i=0;i<numg;i++) printf(" *** iniori_2d : Connexe set %d starting face %d internal point %d\n",
				    i+1,norini[i]+1,npvint[i]+1);
    }

  /* Traitement proprement dit */
  for (j= 0;j<numg;j++)
       { 
	  xint=volconnexe.x[npvint[j]]; yint=volconnexe.y[npvint[j]];
	  iel=norini[j];
	  n1=nodray[0][iel];  n2=nodray[1][iel];                  
	  x1=coordray[0][n1]; y1=coordray[1][n1];
	  x2=coordray[0][n2]; y2=coordray[1][n2];
	  xnorelx=-y2+y1;     xnorely= x2-x1;
	  xnormel=sqrt(xnorelx*xnorelx + xnorely*xnorely);
	  if (xnormel>-eps && xnormel<eps)
	    {
	      if (SYRTHES_LANG == FR)
		printf("\n ERREUR connexe_2d : L'element %d est vraisemblablement degenere (longeur nulle) \n",iel+1);
	      else if (SYRTHES_LANG == EN)
		printf("\n ERROR connexe_2d : Element %d is likely to be degenerated (length is zero) \n",iel+1);
	      syrthes_exit(1);
	    }
           else
	     {xnorelx/=xnormel; xnorely/=xnormel;}

           xvec1ix=xint-x1; xvec1iy=yint-y1;
           xnormve=sqrt(xvec1ix*xvec1ix + xvec1iy*xvec1iy);
           if (xnormve>-eps && xnormve<eps)
	     {
	      if (SYRTHES_LANG == FR)
		printf("\n *** iniori_2d : Le point interne %d de coordonnees xc=%f , yc=%f \n"
		       "                   est mal choisi, car confondu avec le point %d \n",
		       j+1,xint,yint,n1);
	      else if (SYRTHES_LANG == EN)
		printf("\n *** iniori_2d : Internal point %d with coordinates xc=%f , yc=%f \n"
		       "                   is badly chosen, identical to point %d \n",
		       j+1,xint,yint,n1);
	      syrthes_exit(1);
	     }
           else
	     {xvec1ix/=xnormve; xvec1iy/=xnormve;}

           pscal=xnorelx*xvec1ix + xnorely*xvec1iy;
           if (pscal>-eps && pscal<eps)
	     {

	      if (SYRTHES_LANG == FR)
		printf("\n *** iniori_2d : Le point interne %d de coordonnees xc=%f , yc=%f \n"
		       "                   est mal choisi, car dans l'alignement de l'element %d \n",
		       j+1,xint,yint,iel);
	      else if (SYRTHES_LANG == EN)
		printf("\n *** iniori_2d : Internal point %d with coordinates xc=%f , yc=%f \n"
		       "                   is badly chosen, being aligned with element %d \n",
		       j+1,xint,yint,iel);
	      syrthes_exit(1);
	     }
          
           if (pscal<-eps)
	     {
               nodaux=nodray[1][iel];
               nodray[1][iel]=nodray[0][iel]; 
               nodray[0][iel]=nodaux;

               neleaux=ifabor[iel];
               ifabor[iel]=ifabor[iel+nelray];
               ifabor[iel+nelray]=neleaux;

               *nbmalo += 1;
               if (affich.ray_orient) 
		 {
		   if (SYRTHES_LANG == FR)
		     printf("\n iniori_2d : Le segment %d etait mal orientee \n",iel+1);
		   else if (SYRTHES_LANG == EN)
		     printf("\n iniori_2d : Segment %d was wrongly oriented \n",iel+1);
		 }
	     }
	}


  /* Volumes connexes  */
  for (j=0;j<numg;j++)
    for (k=0;k<nelray;k++)
      if(grconx[k]-1==j) grconv[k]= npvint[j];

  if (affich.ray_orient) 
    {printf("\n");
     for (i=0;i<nelray;i++) printf(" iniori_2d : element , grconv : %d   %d \n",i,grconv[i]);}

  free(norinit);free(npvint);free(ncompteur);
  perfo.mem_ray-=(numg*volconnexe.nb+2*numg) * sizeof(int);
}
           

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | racines_2d                                                           |
  |        Calcul de la racine eventuelle entre la facette consideree    |
  |        et le vecteur xvec,yvec                                       |
  |        Si l'intersection n'est pas dans le sgement on met 1e6        |
  |        sur chaque composante                                         |
  |======================================================================| */
int racines_2d(double x1,double y1,double x2,double y2,
	       double xvec,double yvec,double xxg,double yyg,int iel)
{
  int ii;
  double eps,xp,yp;
  double denom,numer,alfa,d;
  double xnorelx,xnorely,xnormel;



  eps=1.e-5;

  xnorelx=-y2+y1;
  xnorely= x2-x1;
  xnormel=sqrt(xnorelx*xnorelx + xnorely*xnorely);
  if (xnormel>-eps && xnormel<eps)
    {
      if (SYRTHES_LANG == FR)
	printf("\n *** racines_2d : L'element %d est vraisemblablement degenere (longueur nulle) \n",
	       iel+1);
      else if (SYRTHES_LANG == EN)
	printf("\n *** racines_2d : Element %d is likely to be degenerated (length is zero) \n",
	       iel+1);
      syrthes_exit(1);
    }
  else
    {xnorelx/=xnormel; xnorely/=xnormel;}

  d=-x1*xnorelx-y1*xnorely;

  denom=xnorelx*xvec+xnorely*yvec;
  if (fabs(denom)>eps) 
    {
      numer=- (xnorelx*xxg + xnorely*yyg + d);
      alfa =numer/denom;
    }
  else
    alfa=1.e6;

  if ((0.<alfa) && (alfa<1.))
    {
      xp=xxg+alfa*xvec;  yp=yyg+alfa*yvec;  
      ii=in_seg(x1,y1,x2,y2,xp,yp);
      if (! ii)
	return(0);
      else
	return(1);
    }
  else
    return(0);

}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | orient2d                                                             |
  |                                                                      |
  |======================================================================| */
void orient3d (struct Maillage maillnodray,
	       struct Compconnexe volconnexe,int *grconv)
{
  int *grconx, *norini, *ifabor ;
  int numg,i,ielem,nbmalo ;
  
  ifabor = (int *)malloc(maillnodray.nelem* 3 * sizeof(int)); verif_alloue_int1d("orient3d",ifabor);
  grconx = (int *)malloc(maillnodray.nelem* sizeof(int));     verif_alloue_int1d("orient3d",grconx);
  perfo.mem_ray+=maillnodray.nelem* 4 * sizeof(int);
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  
  nbmalo = 0 ;
  
  
  voisic_3d(ifabor,maillnodray.node,maillnodray.nelem,maillnodray.npoin) ;
  
  connex_3d(ifabor,grconx,maillnodray.nelem,maillnodray.npoin,&numg,volconnexe.nb) ;

  /* On stockera autant d'elements de depart que de surfaces connexes  trouves */
  norini = (int *)malloc( numg  * sizeof(int)); verif_alloue_int1d("orient3d",norini);
  perfo.mem_ray+=numg * sizeof(int);
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  
  iniori_3d(ifabor,maillnodray.node,maillnodray.coord,grconx,maillnodray.nelem,maillnodray.npoin,
	    volconnexe,numg,norini,grconv,&nbmalo) ;
  
  oriene_3d(ifabor,maillnodray.node,maillnodray.nelem,grconx,norini,numg,&nbmalo);
  
  
  /* 4- Post processing uniquement pour developpeurs (IR,CP)
     ---------------------------------------------- */
  if (affich.ray_orient) 
    {
      for (i=0; i < 3*maillnodray.nelem; i++)  if (ifabor[i] != -1)  ifabor[i] += 1; 
      if (SYRTHES_LANG == FR)
	printf(" \n Table des elements colles aux faces de chaque element \n ");
      else if (SYRTHES_LANG == EN)
	printf(" \n Table of elements in contact with each element face \n ");

      for (ielem=0; ielem<maillnodray.nelem; ielem++)
	printf(" Element %d  :  %d %d %d \n",
	       ielem+1,ifabor[ielem],ifabor[ielem+maillnodray.nelem],ifabor[ielem+2*maillnodray.nelem]);
      
      for (ielem=0; ielem<maillnodray.nelem; ielem++)
	printf(" Element %d  :  %d %d %d\n",ielem+1,maillnodray.node[0][ielem],
	       maillnodray.node[1][ielem],maillnodray.node[2][ielem]);
    }
  
  free(grconx) ;free(norini) ;
  perfo.mem_ray-=(maillnodray.nelem+numg) * sizeof(int);
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | voisic_3d                                                            |
  |     A remplacer par meilleur algorithme dont on dispose deja         |
  |                                                                      |
  |======================================================================| */
void voisic_3d(int *ifabor,int **nodray,int nelray,int npoinr)
{    
  int i,m1,m2;
  int i1,i2,imax,npmax;
  int iface,iface2,ielem,ielem2;
  int somfac[3][2];
  int *nvois,*iadr,*mat;

  somfac[0][0]=0; somfac[0][1]=1;
  somfac[1][0]=1; somfac[1][1]=2;
  somfac[2][0]=2; somfac[2][1]=0;

  nvois=(int *)malloc( (npoinr) * sizeof(int)); verif_alloue_int1d("voisic_3d",nvois);
  perfo.mem_ray+=npoinr * sizeof(int);
  
  
  /* 1- INITIALISATION 
     ==================== */
  for (i=0; i<npoinr ; i++) *(nvois+i)=2 ;
  
  for (iface=0; iface<3 ; iface++) 
    for ( i=0; i<nelray; i++ )
      {
	i1=nodray[somfac[iface][0]][i];
	i2=nodray[somfac[iface][1]][i];
	nvois[i1]++;
	nvois[i2]++;
      }
  
  for (i=0; i<npoinr ; i++)  nvois[i]=nvois[i]/2;
  
  nelvoip=0;
  for (i=0; i< npoinr;i++) if (nelvoip<nvois[i] ) {nelvoip=nvois[i] ; npmax=i;}

  if (affich.ray_orient)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n *** voisic_3d : Point ayant le plus d'elements attaches : %d\n",npmax+1);
	  printf("                   Nombre d'elements rattaches :             %d\n",nelvoip-1);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n *** voisic_3d : Node having the maximum element attachedhes : %d\n",npmax+1);
	  printf("                   Number of element attached :             %d\n",nelvoip-1);
	}
    }
  
  iadr =(int *)malloc( (npoinr) * sizeof(int));  verif_alloue_int1d("voisic_3d",iadr);
  mat  =(int *)malloc( (npoinr) * 3*nelvoip * sizeof(int)); verif_alloue_int1d("voisic_3d",mat);
  perfo.mem_ray+=(1+3*nelvoip)*npoinr * sizeof(int);
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  
  
  /* 2- Calcul des adresses de chaque point dans une structure
     de type matrice compacte    
     =========================================================  */
  
  iadr[0]=0 ;
  for (i=1; i<npoinr; i++) iadr[i]=iadr[i-1] + nvois[i-1] ;

  imax=iadr[npoinr-1] + nvois[npoinr-1];
  
  if ( imax > 3*nelvoip* npoinr ) 
      if (SYRTHES_LANG == FR)
	printf("\n Augmenter la taille memoire : voir developpeurs (CP,IR)\n");
      else if (SYRTHES_LANG == EN)
	printf("\n Increase memory size : see syrthes conceptors (CP,IR)\n");
  
  for (i=0; i<imax; i++) *(mat+i)=0;
  
  /* 3- BOUCLE SUR LES FACES DE CHAQUE ELEMENT :
     ===========================================    */
  for (iface=0; iface<3; iface++) 
    {
      for ( ielem=0; ielem<nelray; ielem++ )
	{
	  ifabor[ielem+ nelray*iface]=-1;
	  
	  i1=nodray[somfac[iface][0]][ielem];
	  i2=nodray[somfac[iface][1]][ielem];
	  
	  if ( i1<i2 ) {m1=i1; m2=i2;}
	  else {m1=i2;m2=i1;}
	  
	  for ( i=1; i<nvois[m1]+1; i++ )
	    if ( mat[iadr[m1]+i-1] == 0 )
	      {
		mat[iadr[m1]+i-1]=m2; /* ????????????????????? */
		mat[iadr[m1]+i-1+nelvoip*   npoinr]=ielem;
		mat[iadr[m1]+i-1+2*nelvoip* npoinr]=iface;
		goto sortie;
	      }
	    else if  ( mat[iadr[m1]+i-1] == m2 )
	      {
		ielem2=mat[iadr[m1]+i-1+nelvoip* npoinr];
		iface2=mat[iadr[m1]+i-1+2*nelvoip* npoinr];
		ifabor[ielem+nelray*iface]  =ielem2;
		ifabor[ielem2+nelray*iface2]=ielem ;
		goto sortie;
	      }
	  
	  if (SYRTHES_LANG == FR)
	    {
	      printf("\n -->  ERREUR voisic_3d : erreur dans le maillage\n");
	      printf("                           il y a peut etre des points confondus \n");
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      printf("\n -->  ERROR voisic_3d : error in the mesh \n");
	      printf("                          nodes may be collapsed \n");
	    }	  
	sortie :;
	  
	}  /* Fin de la boucle sur les elements */
    }  /* Fin de la boucle sur les faces */



  /* 4- Post processing pour developpeur (IR,CP)
     ---------------------------------------------- */
  if(affich.ray_orient)
    {
      for (i=0;i<3*nelray; i++) if ( ifabor[i] != -1 )  ifabor[i] += 1; 
      if (SYRTHES_LANG == FR)
	printf(" voisic_3d : Table des elements colles aux faces de chaque element \n ");
      else if (SYRTHES_LANG == EN)
	printf(" voisic_3d : Table of elements in contact with each  element faces\n ");

      for ( ielem=0; ielem<nelray; ielem++ )
	printf( " voisic_3d :Element %d  :  %d %d  %d\n", ielem+1,
	       ifabor[ielem] ,ifabor[ielem+ nelray],ifabor[ielem+ 2*nelray] );
      
      for ( i=0; i<3*nelray; i++ ) if ( ifabor[i] != -1 )  ifabor[i] -= 1; 
    }


  free(nvois); free(mat); free(iadr);
  perfo.mem_ray-=(2+3*nelvoip)*npoinr * sizeof(int);


}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | connex_3d                                                            |
  |                                                                      |
  |======================================================================| */
void connex_3d(int *ifabor,int *grconx,int nelray,int npoinr,
	       int *numg,int numgu) 
{
  int i,j;
  int iel1,iel2;
  
  for (i=0; i<nelray; i++) *(grconx+i)=0;
  
  
  *numg=1;
  grconx[0]=*numg;
  iel1=0;


  if (ifabor[iel1+2* nelray]+ifabor[iel1+nelray]+ifabor[iel1]==-3 )
    {
      if (SYRTHES_LANG == FR)
	printf("\n $$ ATTENTION connexe_3d : element %d isole \n" , iel1+1);
      else if (SYRTHES_LANG == EN)
	printf("\n $$ WARNING connexe_3d : element %d isolated \n" , iel1+1);
    }
  else
    for (i=0; i< 3; i++ )
      {
	iel2=ifabor[iel1+i* nelray] ;
	if (iel2==-1) continue;
	if (grconx[iel2]!=0) continue;
	group_3d(iel1,iel2,nelray,grconx,ifabor); 
      }


  for (j=0; j< nelray; j++ )
    if ( grconx[j] == 0 )
      {
	if ( affich.ray_orient ) 
	  if (SYRTHES_LANG == FR)
	    printf(" *** connex_3d : l'element %d n'appartient pas encore a un groupe\n", j+1);
	  else if (SYRTHES_LANG == EN)
	    printf(" *** connex_3d : Element %d does not belong to a group \n", j+1);
	
	iel1=j;  iel2=-10; *numg += 1;
	grconx[j]=*numg;
	
	if(ifabor[iel1+2* nelray]+ifabor[iel1+nelray]+ifabor[iel1]==-3)
	  {
	    if (SYRTHES_LANG == FR)
	      printf("\n $$ ATTENTION connex_3d : element %d isole \n" , iel1+1) ;
	    else if (SYRTHES_LANG == EN)
	      printf("\n $$ WARNING connex_3d : element %d isolated \n" , iel1+1) ;
	    continue;
	  }
	  
	for (i=0; i< 3; i++ )
	  {
	    iel2=ifabor[iel1+i* nelray];
	    if (iel2 ==-1) continue;
	    if (grconx[iel2]!=0) continue;
	    group_3d( iel1,iel2,nelray,grconx,ifabor);
	  }
      }      /* Fin du test sur l'appartenance a un groupe */

   
  if (syrglob_nparts==1 || syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf("\n *** connex_3d : le maillage surfacique contient %d surfaces connexes et %d volumes connexes \n",
	     *numg,numgu );
    else if (SYRTHES_LANG == EN)
      printf("\n *** connex_3d : the surfacique mesh is composed of %d connex surfaces  and  %d connex volumes  \n",
	     *numg,numgu );
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | group_3d                                                             |
  |                                                                      |
  |======================================================================| */
void group_3d(int iel1,int iel2,int nelray,int *grconx,int *ifabor)

{
  int i;
  int ielc,ielv;

  grconx[iel2]=grconx[iel1];

  for (i=0; i< 3; i++ )
    {
      ielv=ifabor[iel2+i* nelray];
      if ( ielv != -1 && grconx[ielv] == 0)
	group_3d( iel2,ielv,nelray,grconx,ifabor);
    }
}
    
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | oriene_3d                                                            |
  |                                                                      |
  |======================================================================| */
void oriene_3d(int *ifabor,int **nodray,int nelray, 
              int *grconx,int *norini,int numg,int* nbmalo)

{
  int i,i1,i2,iel1,iel2;
  int iface,ielem;
  int somfac[3][2];
  
  somfac[0][0]=0; somfac[0][1]=1;
  somfac[1][0]=1; somfac[1][1]=2;
  somfac[2][0]=2; somfac[2][1]=0;

  for ( i=0; i<numg; i++ ) 
    {        
      iel1=norini[i];
      for ( iface=0; iface<3; iface++ )
	{
	  i1=nodray[somfac[iface][0]][iel1];
	  i2=nodray[somfac[iface][1]][iel1];
          iel2=ifabor[iel1 + iface* nelray];
          if ( iel2 != -1 && grconx[iel2] > 0 )
	    rorien_3d(i1,i2,iel2,ifabor,grconx,nodray,nelray,nbmalo);
	}
    }

  if (syrglob_nparts==1 || syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf("\n *** oriene_3d : le nombre de facettes reorientees est : %d \n",
	     *nbmalo);
    else if (SYRTHES_LANG == EN)
      printf("\n *** oriene_3d : The number of reoriented faces is : %d \n",
	     *nbmalo);

}  


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | rorien_3d                                                            |
  |                                                                      |
  |======================================================================| */
void rorien_3d(int i1,int i2,int iel2,int *ifabor,int *grconx,
	       int **nodray,int nelray, int *nbmalo)
{ 
  int j,j1,j2;
  int ielv,iface;
  int lmalo,nodaux,neleaux;
  int somfac[3][2];
  
  somfac[0][0]=0; somfac[0][1]=1;
  somfac[1][0]=1; somfac[1][1]=2;
  somfac[2][0]=2; somfac[2][1]=0;

  lmalo=0;
  
  for ( iface=0; iface<3; iface++ )
    {
      j1=nodray[somfac[iface][0]][iel2];
      j2=nodray[somfac[iface][1]][iel2];
      if ( j1 == i1 && j2 == i2 ) lmalo=1;
    }

  if ( lmalo == 1 )
    {
      *nbmalo += 1;
      nodaux=nodray[somfac[1][0]][iel2];
      nodray[somfac[1][0]][iel2]=nodray[somfac[1][1]][iel2];  
      nodray[somfac[1][1]][iel2]=nodaux;
	
      neleaux=ifabor[iel2];
      ifabor[iel2]=ifabor[iel2 + 2 * nelray ];
      ifabor[iel2 + 2 * nelray]=neleaux;
      
      grconx[iel2]=- grconx[iel2];
      
      if (affich.ray_orient)
	if (SYRTHES_LANG == FR)
	  printf("\n *** rorien_3d : la facette %d etait mal orientee \n",iel2+1 );
	else if (SYRTHES_LANG == EN)
	  printf("\n *** rorien_3d : the face %d was wrongly oriented \n",iel2+1 );
    }
  else
    {
      /* L'element etait deja bien oriente */
      grconx[iel2]=- grconx[iel2];     
    }     
  
  
     
  for ( iface=0; iface<3; iface++ )
    {
      j1=nodray[somfac[iface][0]][iel2];
      j2=nodray[somfac[iface][1]][iel2];
      ielv=ifabor[iel2 + iface* nelray];
      if ( ielv != -1 && grconx[ielv] > 0 )
	rorien_3d(j1,j2,ielv,ifabor,grconx,nodray,nelray,nbmalo);      
    }

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | iniori_3d                                                            |
  |        Initialisation des processus recursifs                        |
  |======================================================================| */
void iniori_3d(int *ifabor,int **nodray,double **coordray,
	       int *grconx,int nelray,int npoinr,
	       struct Compconnexe volconnexe,int numg,int *norini,
	       int *grconv,int *nbmalo)
{
  int i,j,k,ref,ngroup,imin;
  int n1,n2,n3;
  int iel,nodaux,neleaux;
  int iv,pintok,dejaunpoint;
  double xint,yint,zint;
  double xx,yy,zz,di1,di2,di3,xi1,xi2,xi3,dmin;
  double x1,y1,z1,x2,y2,z2,x3,y3,z3;
  double x12,y12,z12,x13,y13,z13;
  double xnorelx,xnorely,xnorelz,xnormel;
  double xvecgix,xvecgiy,xvecgiz,xnormve;
  double xvec1ix,xvec1iy,xvec1iz;
  double pscal;
  double xxg,yyg,zzg;
  double xp,yp,zp,dist;
  int ok,npi,refj;
  
  int *norinit, *npvint, *ncompteur;
  double eps;

  eps=1e-6;

  norinit=(int *)malloc(numg * volconnexe.nb * sizeof(int) ); verif_alloue_int1d("iniori_3d",norinit);
  npvint=(int *)malloc(numg * sizeof(int) );                  verif_alloue_int1d("iniori_3d",npvint);
  ncompteur=(int *)malloc(numg * sizeof(int) );               verif_alloue_int1d("iniori_3d",ncompteur);
  perfo.mem_ray+=(numg*volconnexe.nb+2*numg) * sizeof(int);
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  
  for (i=0; i<nelray; i++) *(grconv+i)=0;
  for (i=0; i<numg; i++) *(npvint+i)=0;
  for (i=0; i<numg; i++) *(ncompteur+i)=0;
  for (i=0; i<numg*volconnexe.nb; i++) *(norinit+i)=0;
  
  if ( affich.ray_orient )
     {
       for ( i=0; i<nelray; i++ ) printf(" iniori_3d : Element , grconx : %d   %d \n",i+1,grconx[i] );
       printf("\n");
       if (SYRTHES_LANG == FR)
	 for ( i=0; i<npoinr; i++ )
	   printf(" iniori_3d : Noeud %d  xyz : %f %f %f \n",i+1,coordray[0][i],coordray[1][i],coordray[2][i]);
       else if (SYRTHES_LANG == EN)
	 for ( i=0; i<npoinr; i++ )
	   printf(" iniori_3d : Node %d  xyz : %f %f %f \n",i+1,coordray[0][i],coordray[1][i],coordray[2][i]);
     }
      
  /* Boucle sur les surfaces connexes */
  for ( j=0; j<numg; j++ )
    {           
      /* boucle sur les points volumiques internes */
      for (iv=0;iv<volconnexe.nb;iv++)
	{
	  xint=volconnexe.x[iv]; yint=volconnexe.y[iv]; zint=volconnexe.z[iv];
	  dmin=1.E6;
	  dist=1.E+6;
	  for ( i=0; i<nelray; i++ )
	    if ( grconx[i]-1 == j )
	      {
		n1=nodray[0][i]; x1=coordray[0][n1]; y1=coordray[1][n1]; z1=coordray[2][n1];
		n2=nodray[1][i]; x2=coordray[0][n2]; y2=coordray[1][n2]; z2=coordray[2][n2];
		n3=nodray[2][i]; x3=coordray[0][n3]; y3=coordray[1][n3]; z3=coordray[2][n3];
		xxg=(x1+x2+x3)/3.; yyg=(y1+y2+y3)/3.; zzg=(z1+z2+z3)/3.;
		/* On commence par se baser sur un test peu fiable 
		   la distance minimal G-PINT*/
		dist=(xxg-xint)*(xxg-xint)+(yyg-yint)*(yyg-yint)+(zzg-zint)*(zzg-zint);
		if (dist<dmin) {dmin=dist; imin=i;}
	      }

	  if (dist<eps*eps*1.1)
	    {
	      if (SYRTHES_LANG == FR)
		printf("\n *** iniori_3d : Point interne %d mal choisi : sur la facette %d \n",iv+1,imin+1);
	      else if (SYRTHES_LANG == EN)
		printf("\n *** iniori_3d : Internal point %d badly chosen : on face %d \n",iv+1,imin+1);
	      syrthes_exit(1);
	    }

	  /* La facette potentielle que l'on va tester est imin */
	  i=imin;
	  n1=nodray[0][i]; x1=coordray[0][n1]; y1=coordray[1][n1]; z1=coordray[2][n1];
	  n2=nodray[1][i]; x2=coordray[0][n2]; y2=coordray[1][n2]; z2=coordray[2][n2];
	  n3=nodray[2][i]; x3=coordray[0][n3]; y3=coordray[1][n3]; z3=coordray[2][n3];
	  xxg=(x1+x2+x3)/3.; yyg=(y1+y2+y3)/3.; zzg=(z1+z2+z3)/3.;
	  x12=x2-x1; y12=y2-y1; z12=z2-z1;
	  x13=x3-x1; y13=y3-y1; z13=z3-z1;
	  xnorelx= y12*z13-z12*y13;
	  xnorely=-x12*z13+z12*x13;
	  xnorelz= x12*y13-y12*x13;
	  xnormel=sqrt( xnorelx*xnorelx + xnorely*xnorely + xnorelz*xnorelz );
	  if ( fabs(xnormel)<eps )
	    { 
	      if (SYRTHES_LANG == FR)
		printf( "\n --> ERREUR iniori_3d : l'element %d est vraisemblablement degenere\n", i+1 ); 
	      else if (SYRTHES_LANG == EN)
		printf( "\n --> ERROR iniori_3d : Element %d is likely to be degenerated \n", i+1 );
	      syrthes_exit(1); 
	    }
	  else
	    {
	      xnorelx/=xnormel; xnorely/=xnormel; xnorelz/=xnormel;
	    }
	  xvecgix=xint-xxg; xvecgiy=yint-yyg; xvecgiz=zint-zzg;
	  xnormve=sqrt( xvecgix*xvecgix + xvecgiy*xvecgiy + xvecgiz*xvecgiz );
	  if ( fabs(xnormve)<eps )
	    {
	      if (SYRTHES_LANG == FR)
		{     
		  printf("\n $$ ATTENTION INIORI_3D : le point interne %d \n",j+1);
		  printf("                           de coordonnees xc=%f , yc=%f , zc=%f \n",
			 xint,yint,zint);
		  printf("                           est mal choisi, car confondu avec le centre de gravite\n");
		}
	      else if (SYRTHES_LANG == EN)
		{     
		  printf("\n $$ ERROR iniori_3d : Internal point %d \n",j+1);
		  printf("                        with coordinates xc=%f , yc=%f , zc=%f \n",
			 xint,yint,zint);
		  printf("                          is badly chosen, on the center of gravitye\n");
		}
	      syrthes_exit(1);
	    }
	  else
	    {
	      xvecgix/=xnormve; xvecgiy/=xnormve; xvecgiz/=xnormve;
	    }

	  pscal=xnorelx*xvecgix + xnorely*xvecgiy + xnorelz*xvecgiz;
	  if ( fabs(pscal)<eps )
	    {
	      if (SYRTHES_LANG == FR)
		{
		  printf("\n  $$ ATTENTION iniori_3d : le point interne %d \n",j+1);
		  printf("                           de coordonnees xc=%f , yc=%f , zc=%f \n",
			 xint,yint,zint);
		  printf("                           est mal choisi, car dans le plan de la facette %d\n",i);
		}
	      else if (SYRTHES_LANG == EN)
		{
		  printf("\n  $$ ERROR INIORI_3D : Internal point %d \n",j+1);
		  printf("                           with coordinates xc=%f , yc=%f , zc=%f \n",
			 xint,yint,zint);
		  printf("                           is badly chosen, in the plane of face  %d\n",i);
		}
	      syrthes_exit(1);
	    }
	  
	  /* Verification  de non recoupage  de la surface connexe j */
	  xvecgix*=xnormve; xvecgiy*=xnormve; xvecgiz*=xnormve;
	  for ( k=0;k<nelray;k++ )
	    if ( grconx[k]-1 == j && k!=i )  
	      {
		n1=nodray[0][k]; x1=coordray[0][n1]; y1=coordray[1][n1]; z1=coordray[2][n1];
		n2=nodray[1][k]; x2=coordray[0][n2]; y2=coordray[1][n2]; z2=coordray[2][n2];
		n3=nodray[2][k]; x3=coordray[0][n3]; y3=coordray[1][n3]; z3=coordray[2][n3];
		
		if (racines_3d(x1,y1,z1,x2,y2,z2,x3,y3,z3,xvecgix,xvecgiy,xvecgiz,xxg,yyg,zzg,k))
		  {
		    if (SYRTHES_LANG == FR)
		      printf("\n *** iniori_3d : Il y a une ambiguite --> changer la position du point %d \n",iv+1);
		    else if (SYRTHES_LANG == EN)
		      printf("\n *** iniori_3d : There is an ambiguity --> Please change the point position %d \n",iv+1);
		    syrthes_exit(1);
		  }
	      }

	  /* Stockage de tous les elements correspondants au couple iv et surface connexe j */
	  norinit[iv +j*volconnexe.nb]=imin;
	  if (affich.ray_orient ) 
	    {for (k=0;k<numg*volconnexe.nb;k++) printf (" INIORI_3D : norinit[ %d ]=%d \n",k+1,norinit[k]+1);}
	  
	}
    }

  if (affich.ray_orient)
    for (iv=0;iv<volconnexe.nb;iv++)
      if (SYRTHES_LANG == FR)
	{
	  for ( j=0;j<numg; j++ )
	    printf(" iniori_3d : composante connexe %d , face initiale %d pour le noeud volumique %d  \n", 
		   j+1,norinit[ iv +j*volconnexe.nb]+1,iv+1);
	}
      else if (SYRTHES_LANG == EN)
	{
	  for ( j=0;j<numg; j++ )
	    printf(" iniori_3d : connex set %d , initial face %d for internal point  %d  \n", 
		   j+1,norinit[ iv +j*volconnexe.nb]+1,iv+1);
	}



/* Determination du point interieur iv qui convient a la surface connexe j
   Recoupage pair des autres surfaces connexes necessaire
   */
  for ( j=0; j<numg; j++ )
    {	  
      dejaunpoint=0;
      for (iv=0;iv<volconnexe.nb;iv++)
	{
	  xint=volconnexe.x[iv]; yint=volconnexe.y[iv]; zint=volconnexe.z[iv];
	  for (i=0; i<numg; i++) ncompteur[i]=0;
	  iel=norinit[iv + j*volconnexe.nb];
	  n1=nodray[0][iel];x1=coordray[0][n1]; y1=coordray[1][n1]; z1=coordray[2][n1];
	  n2=nodray[1][iel];x2=coordray[0][n2]; y2=coordray[1][n2]; z2=coordray[2][n2];
	  n3=nodray[2][iel];x3=coordray[0][n3]; y3=coordray[1][n3]; z3=coordray[2][n3];
	  xxg=(x1+x2+x3)/3.; yyg=(y1+y2+y3)/3.; zzg=(z1+z2+z3)/3.;
	  xvecgix=xint-xxg;  xvecgiy=yint-yyg;  xvecgiz=zint-zzg;
	  
	  if (affich.ray_orient)  
	    if (SYRTHES_LANG == FR)
	      printf(" *** iniori_3d : composante connexe j=%d noeud interieur iv=%d \n",j+1,iv+1);
	    else if (SYRTHES_LANG == EN)
	      printf(" *** iniori_3d : connex set  j=%d noeud internal node iv=%d \n",j+1,iv+1);

	  for ( k=0;k<nelray;k++ )
	    if ( grconx[k]-1 != j )
	      {
		if (affich.ray_orient) 
		  if (SYRTHES_LANG == FR)
		    printf(" comp connex %d    facette testee k=%d \n",j+1,k+1); 
		  else if (SYRTHES_LANG == EN)
		    printf(" connex set %d    face tested k=%d \n",j+1,k+1);

		n1=nodray[0][k];x1=coordray[0][n1]; y1=coordray[1][n1]; z1=coordray[2][n1];
		n2=nodray[1][k];x2=coordray[0][n2]; y2=coordray[1][n2]; z2=coordray[2][n2];
		n3=nodray[2][k];x3=coordray[0][n3]; y3=coordray[1][n3]; z3=coordray[2][n3];
		
		if (racines_3d(x1,y1,z1,x2,y2,z2,x3,y3,z3,xvecgix,xvecgiy,xvecgiz,xxg,yyg,zzg,k))
		  {
		    if (affich.ray_orient ) 
		      if (SYRTHES_LANG == FR)
			printf(" *** iniori_3d : Intersection avec une autre surf connexe (face %d) composante connexe %d \n", 
			       k+1,grconx[k]);
		      else if (SYRTHES_LANG == EN)
			printf(" *** iniori_3d : intersection with another connex surface (face %d) connex set %d \n", 
			       k+1,grconx[k]);

		    ncompteur[grconx[k]-1] += 1;
		  }
	      }

      
	  if (affich.ray_orient) 
	    if (SYRTHES_LANG == FR)
	      printf(" *** iniori_3d : composante connexe j=%d point interieur iv=%d \n",j+1,iv+1);
	    else if (SYRTHES_LANG == EN)
	      printf(" *** iniori_3d : connex set j=%d internal point  iv=%d \n",j+1,iv+1);

	  if (affich.ray_orient) 
	    {for (i=0;i<numg;i++) printf( " INIORI_3D : ncompteur[ %d ]=%d \n",i+1,ncompteur[i]);}

	  pintok =1;
	  /* Flag des intersections impaires */
	  for (i=0; i<numg;i++)
	    {
	      if ((ncompteur[i]/2)*2-ncompteur[i]!=0)
		pintok=0;
	    }
	  if (pintok == 0)
	    {
	      if (affich.ray_orient) 
		if (SYRTHES_LANG == FR)
		  printf( " *** iniori_3d : le point interieur %d non ok pour la surface connexe j %d \n",iv+1,j+1);
		else if (SYRTHES_LANG == EN)
		  printf( " *** iniori_3d : Internal point %d not ok for connex surface j %d \n",iv+1,j+1);
	    }
	  else
	    { if (dejaunpoint==1)
		{
		  if (SYRTHES_LANG == FR)
		    {
		      printf("\n *** iniori_3d : Ce volume est :\n");
		      printf("                   soit deja defini par un point precedent \n");
		      printf("                   soit une ambiguite existe sur le volume ---> a verifier tres soigneusement \n" );
		      printf("                   --> essayer de modifier la position du noeud interieur %d \n",iv+1);
		    }
		  else if (SYRTHES_LANG == EN)
		    {
		      printf("\n *** iniori_3d : This volume is :\n");
		      printf("                   seither already defined by a prvious point \n");
		      printf("                   either an ambiguity exists  ---> Please chek very carefully \n" );
		      printf("                   --> try to modify the position of the internal point %d \n",iv+1);
		    }
		  syrthes_exit(1);
		}
	    else	  
	      {norini[j]=iel;npvint[j]=iv;dejaunpoint=1;}
	    }
	}
  
      if (dejaunpoint==0 ) 
	{
	  if (SYRTHES_LANG == FR)
	    {
	      printf("\n *** iniori_3d : Il y a un probleme pour la surface connexe j= %d\n" ,j+1);
	      printf("                   Aucun des points fournis par l'utilisateur  ne permet de la definir \n");
	      printf("                   Verifier bien que tous les volumes independants ont ete definis par un point \n");
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      printf("\n *** iniori_3d : There is a problem with connex surface  j= %d\n" ,j+1);
	      printf("                   None of the point given by the user allows to define it \n");
	      printf("                   Please check if all independent volumes have been defined by an internal point\n");
	    }
	  syrthes_exit(1);
	}
    }

  if (affich.ray_orient)
    {
      if (SYRTHES_LANG == FR)
	for (i=0;i<numg;i++) 
	  printf(" *** iniori_3d : Composante connexe %d  face de depart %d point interieur %d \n",
		 i+1,norini[i]+1,npvint[i]+1);
      else if (SYRTHES_LANG == EN)
	for (i=0;i<numg;i++) 
	  printf(" *** iniori_3d : Connex set %d  starting face %d internal point %d \n",
		 i+1,norini[i]+1,npvint[i]+1);
    }
  



  /* Traitement proprement dit */
  for ( j= 0; j<numg; j++ )
    { 
      xint=volconnexe.x[npvint[j]]; yint=volconnexe.y[npvint[j]]; zint=volconnexe.z[npvint[j]];
      iel=norini[j];
      n1=nodray[0][iel];  x1=coordray[0][n1]; y1=coordray[1][n1]; z1=coordray[2][n1];
      n2=nodray[1][iel];  x2=coordray[0][n2]; y2=coordray[1][n2]; z2=coordray[2][n2]; 
      n3=nodray[2][iel];  x3=coordray[0][n3]; y3=coordray[1][n3]; z3=coordray[2][n3];
      x12=x2-x1; y12=y2-y1; z12=z2-z1;
      x13=x3-x1; y13=y3-y1; z13=z3-z1;
      xnorelx= y12*z13-z12*y13; xnorely=-x12*z13 + z12*x13; xnorelz= x12*y13-y12*x13;
      xnormel=sqrt( xnorelx*xnorelx + xnorely*xnorely + xnorelz*xnorelz );
      if ( fabs(xnormel)<eps )
	{
	  if (SYRTHES_LANG == FR)
	    printf( "\n --> ERREUR iniori_3d : l'element %d est vraisemblablement degenere\n",
		    iel+1 );
	  else if (SYRTHES_LANG == EN)
	    printf( "\n --> ERROR iniori_3d : Element %d is likely to be degenerated\n",
		    iel+1 );
	  syrthes_exit(1);
	}
      else
	{
	  xnorelx/=xnormel; xnorely/=xnormel; xnorelz/=xnormel;
	}
	    
	    
      xvec1ix=xint-x1; xvec1iy=yint-y1; xvec1iz=zint-z1;
      xnormve=sqrt( xvec1ix*xvec1ix + xvec1iy*xvec1iy + xvec1iz*xvec1iz );
      if ( xnormve > -eps && xnormve<eps )
	{
	  if (SYRTHES_LANG == FR)
	    {
	      printf("\n  --> ERREUR INIORI_3D : le point interne %d \n",j+1);
	      printf("                           de coordonnees xc=%f , yc=%f , zc=%f \n",
		     xint,yint,zint);
	      printf("                           est mal choisi, car confondu avec le noeud %d\n",n1);
	    }	  
	  else if (SYRTHES_LANG == EN)
	    {
	      printf("\n  --> ERROR iniori_3d : Internal point %d \n",j+1);
	      printf("                          with coordinates xc=%f , yc=%f , zc=%f \n",
		     xint,yint,zint);
	      printf("                          is badly chosen, being collapsed with node %d\n",n1);
	    }	  
	  syrthes_exit(1);
	}
      else
	{
	  xvec1ix/=xnormve; xvec1iy/=xnormve; xvec1iz/=xnormve;
	}
	    
      pscal=xnorelx*xvec1ix + xnorely*xvec1iy + xnorelz*xvec1iz;
      if ( fabs(pscal)<eps )
	{
	  if (SYRTHES_LANG == FR)
	    {
	      printf("\n --> ERREUR INIORI_3D : le point interne %d \n",j+1);
	      printf("                          de coordonnees xc=%f , yc=%f , zc=%f \n",
		     xint,yint,zint);
	      printf("                          est mal choisi, car  dans l'alignelement de la facette %d\n",iel);
	    }	  
	  else if (SYRTHES_LANG == EN)
	    {
	      printf("\n --> ERROR iniori_3d : Internal Point %d \n",j+1);
	      printf("                         with coordinates xc=%f , yc=%f , zc=%f \n",
		     xint,yint,zint);
	      printf("                         is badly chosen, being aligned with face  %d\n",iel);
	    }	 
	  syrthes_exit(1);
	}
	    
      if ( pscal<-eps )
	{
	  
	  nodaux=nodray[1][iel];
	  nodray[1][iel]=nodray[2][iel];  
	  nodray[2][iel]=nodaux;
	     
	  neleaux=ifabor[iel];
	  ifabor[iel]=ifabor[iel + 2 * nelray ];
	  ifabor[iel + 2 * nelray]=neleaux;
	     
	  *nbmalo += 1;
	  if ( affich.ray_orient )
	    if (SYRTHES_LANG == FR)
	      printf( " iniori_3d : la facette initiale %d etait mal orientee \n",iel+1 );
	    else if (SYRTHES_LANG == EN)
	      printf( " iniori_3d : starting face %d was wromgly oriented \n",iel+1 );
	  
	}
    }
     
  /* Volumes connexes */
  for (j=0;j<numg;j++)
    for (k=0;k<nelray;k++)
      if( grconx[k]-1==j) grconv[k]= npvint[j];

     
  if (affich.ray_orient) 
    {for ( i=0; i<nelray; i++ ) printf(" iniori_3d : element, grconv : %d %d \n",i,grconv[i]);}
     
  free(norinit);free(npvint);free(ncompteur); 
  perfo.mem_ray-=(numg*volconnexe.nb+2*numg) * sizeof(int);
     
     
}
           

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | racines_3d                                                           |
  |        Calcul de la racine eventuelle entre la facette consideree    |
  |        et le vecteur xvec,yvec,zvec                                  |
  |        Si l'intersection n'est pas dans le triangle on met 1e6       |
  |        sur chaque composante                                         |
  |======================================================================| */
int racines_3d(double x1,double y1,double z1,
	     double x2,double y2,double z2,double x3,double y3,double z3,
	     double xvec,double yvec,double zvec,
	     double xxg,double yyg,double zzg,int iel)
{
  int ii;
  double eps;
  double denom,numer,alfa,d,xp,yp,zp;
  double x12,y12,z12,x13,y13,z13;
  double xnorelx,xnorely,xnorelz,xnormel;

  eps=1.e-8; alfa=1.e6;

  x12=x2-x1; y12=y2-y1; z12=z2-z1;
  x13=x3-x1; y13=y3-y1; z13=z3-z1;

  xnorelx= y12*z13-z12*y13;
  xnorely=-x12*z13 + z12*x13;
  xnorelz= x12*y13-y12*x13;
  xnormel=sqrt( xnorelx*xnorelx + xnorely*xnorely + xnorelz*xnorelz );
  if ( xnormel > -eps && xnormel<eps )
    {
      if (SYRTHES_LANG == FR)
	printf("\n --> ERREUR racines_3d : l'element %d est vraisemblablement degenere\n",iel+1 );
      else if (SYRTHES_LANG == EN)
	printf("\n --> ERROR racines_3d : Element %d is likely to be degenerated\n",iel+1 );
	
      syrthes_exit(1);
    }
  else
    {
      xnorelx/=xnormel;
      xnorely/=xnormel;
      xnorelz/=xnormel;
    }

  d=-x1*xnorelx-y1*xnorely-z1*xnorelz;

  denom=xnorelx*xvec+xnorely*yvec+xnorelz*zvec;
  if ( fabs(denom) > eps ) 
    {
      numer=- ( xnorelx*xxg +  xnorely*yyg + xnorelz*zzg + d);
      alfa =numer/denom;
    }

  if ((0.<alfa) && (alfa<1.))
    {
      xp=xxg+alfa*xvec;  yp=yyg+alfa*yvec;  zp=zzg+alfa*zvec;
      ii=in_triangle(xnorelx,xnorely,xnorelz,d,x1,y1,z1,x12,y12,z12,x13,y13,z13,xp,yp,zp);

      if (! ii)
	return(0);
      else
	return(1);
    }
  else
    return(0);
}







