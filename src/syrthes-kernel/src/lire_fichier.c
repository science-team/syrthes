/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "syr_usertype.h"
#include "syr_parall.h"
#include "syr_option.h"
#include "syr_tree.h"
#include "syr_bd.h"
#include "syr_const.h"
#include "syr_proto.h"

extern struct Performances perfo;
extern struct Affichages affich;

extern char nomgeom[CHLONG],nomsuit[CHLONG],nommeteo[CHLONG];
extern int nbVar;


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture d'un maillage au format Syrthes                              |
  |======================================================================| */
void lire_syrthes(struct Maillage *maillnodes,struct MaillageBord *maillnodebord,
		  struct SDparall *sdparall )
{
  FILE  *fgeom;
  char ch[CHLONG],ch1[CHLONG],chnum[20];
  int bidon;
  int i,j,n,nr,nv,nb,rubrc,rubperio;
  int *ne0,*ne1,*ne2,*ne3;
  int nptot,netot,nebtot;
  double x,y,z;
  int form,*btrav,itrav[100];
  int ndim_lu,npart_lu,rescon_lu,perio_lu;

  /* fichier ascii ou binaire */
  form=1;
  if (!strncmp(nomgeom+strlen(nomgeom)-5, ".syrb",5)) form=0;


  /* ouverture et entete en ascii */
  /* ---------------------------- */
  if (form==1)
    {
      if (sdparall->nparts>1)
	{
	  strcpy(ch1,"\0"); 
	  strncat(ch1,nomgeom,strlen(nomgeom)-4);
	  sprintf(chnum,"_%05dpart%05d\0",sdparall->nparts,sdparall->rang); 
	  strcat(ch1,chnum);
	  strcat(ch1,".syr\0");
	  if ((fgeom=fopen(ch1,"r")) == NULL)
	    {
	      if (SYRTHES_LANG == FR)
		printf("Impossible d'ouvrir le fichier %s\n",ch1);
	      else if (SYRTHES_LANG == EN)
		printf("Impossible to open the file %s\n",ch1);
	      syrthes_exit(1) ;
	    }
	}
      else
	if ((fgeom=fopen(nomgeom,"r")) == NULL)
	  {
	    if (SYRTHES_LANG == FR)
	      printf("Impossible d'ouvrir le fichier %s\n",nomgeom);
	    else if (SYRTHES_LANG == EN)
	      printf("Impossible to open the file %s\n",nomgeom);
	    syrthes_exit(1) ;
	  }
      
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fscanf(fgeom,"%s%s%s%d",ch,ch,ch,&ndim_lu);
      fscanf(fgeom,"%s%s%s%s%s%d",ch,ch,ch,ch,ch,&(maillnodes->ndiele));
      fscanf(fgeom,"%s%s%s%s%s%d",ch,ch,ch,ch,ch,&(maillnodes->npoin));
      fscanf(fgeom,"%s%s%s%s%d",ch,ch,ch,ch,&(maillnodes->nelem));
      fscanf(fgeom,"%s%s%s%s%s%s%d",ch,ch,ch,ch,ch,ch,&(maillnodebord->nelem));
      fscanf(fgeom,"%s%s%s%s%s%s%s%d",ch,ch,ch,ch,ch,ch,ch,&(maillnodes->ndmat));
    }    
      
  else
    {
      /* ouverture et entete en binaire */
      /* ------------------------------ */
      if (sdparall->nparts>1)
	{
	  strcpy(ch1,"\0"); 
	  strncat(ch1,nomgeom,strlen(nomgeom)-5);
	  sprintf(chnum,"_%05dpart%05d\0",sdparall->nparts,sdparall->rang); 
	  strcat(ch1,chnum);
	  strcat(ch1,".syrb\0");
	  if ((fgeom=fopen(ch1,"rb")) == NULL)
	    {
	      if (SYRTHES_LANG == FR)
		printf("Impossible d'ouvrir le fichier %s\n",ch1);
	      else if (SYRTHES_LANG == EN)
		printf("Impossible to open the file %s\n",ch1);
	      syrthes_exit(1) ;
	    }
	}
      else
	{
	  if ((fgeom=fopen(nomgeom,"rb")) == NULL)
	    {
	      if (SYRTHES_LANG == FR)
		printf("Impossible d'ouvrir le fichier %s\n",nomgeom);
	      else if (SYRTHES_LANG == EN)
		printf("Impossible to open the file %s\n",nomgeom);
	      syrthes_exit(1) ;
	    }
	}

      fread(&ndim_lu,sizeof(int),1,fgeom);
      fread(&(maillnodes->ndiele),sizeof(int),1,fgeom);
      fread(&(maillnodes->npoin),sizeof(int),1,fgeom);
      fread(&(maillnodes->nelem),sizeof(int),1,fgeom);
      fread(&(maillnodebord->nelem),sizeof(int),1,fgeom);
      fread(&(maillnodes->ndmat),sizeof(int),1,fgeom);

    }

  /* verification sur la dimension du probleme */
  /* ----------------------------------------- */
  if (ndim_lu != maillnodes->ndim && (syrglob_nparts==1 ||syrglob_rang==0))
    {	    
      if (SYRTHES_LANG == FR){
	printf(" ERREUR : la dimension du maillage ne correspond pas a celle indiquee dans le fichier de donnees\n");
	printf("          voir le mot-cle : DIMENSION DU PROBLEME\n");
      }

      else if (SYRTHES_LANG == EN){
	printf(" ERROR : mesh dimension do not match with the dimension of the problem indicated in the data file\n");
	printf("         see keyword : DIMENSION DU PROBLEME\n");
      }
      syrthes_exit(1);
    }


  /* allocations des tableaux */
  /* ------------------------ */
  maillnodebord->ndim  = maillnodes->ndim;
  maillnodebord->ndmat = maillnodes->ndmat-1;
  maillnodebord->ndiele= maillnodes->ndiele-1;
      
      
  maillnodes->coord=(double**)malloc(maillnodes->ndim*sizeof(double*));
  for (i=0;i<maillnodes->ndim;i++)  maillnodes->coord[i]=(double*)malloc(maillnodes->npoin*sizeof(double)); 
  verif_alloue_double2d(maillnodes->ndim,"lire_syrthes",maillnodes->coord);

  maillnodes->node=(int**)malloc(maillnodes->ndmat*sizeof(int*));
  for (i=0;i<maillnodes->ndmat;i++) maillnodes->node[i]=(int*)malloc(maillnodes->nelem*sizeof(int)); 
  verif_alloue_int2d(maillnodes->ndmat,"lire_syrthes",maillnodes->node);

  maillnodes->nref=(int*)malloc(maillnodes->npoin*sizeof(int));
  verif_alloue_int1d("lire_syrthes",maillnodes->nref);

  maillnodes->nrefe=(int*)malloc(maillnodes->nelem*sizeof(int));
  verif_alloue_int1d("lire_syrthes",maillnodes->nrefe);


  maillnodebord->node=(int**)malloc(maillnodebord->ndmat*sizeof(int*));
  for (i=0;i<maillnodebord->ndmat;i++) maillnodebord->node[i]=(int*)malloc(maillnodebord->nelem*sizeof(int)); 
  verif_alloue_int2d(maillnodebord->ndmat,"lire_syrthes",maillnodebord->node);

  maillnodebord->nrefe=(int*)malloc(maillnodebord->nelem*sizeof(int));
  verif_alloue_int1d("lire_syrthes",maillnodebord->nrefe);

  perfo.mem_cond+=maillnodes->npoin*maillnodes->ndim*sizeof(double);
  perfo.mem_cond+=(maillnodes->ndmat+1)*maillnodes->nelem*sizeof(int);
  perfo.mem_cond+=(maillnodebord->ndmat+1)*maillnodebord->nelem*sizeof(int);
  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

  
  /* maillage SYRTHES en ascii           */
  /* ----------------------------------- */

  if (form==1)
    {

      /* coordonnees */
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);

      if (maillnodes->ndim==2) 
	{
	  for (i=0;i<maillnodes->npoin;i++)
	    fscanf(fgeom,"%d%d%14lf%14lf%14lf",&n,maillnodes->nref+i,
		   (maillnodes->coord[0]+i),(maillnodes->coord[1]+i),&z);
	}
      else
	{
	  for (i=0;i<maillnodes->npoin;i++)
	    fscanf(fgeom,"%d%d%14lf%14lf%14lf",&n,maillnodes->nref+i,
		   (maillnodes->coord[0]+i),(maillnodes->coord[1]+i),(maillnodes->coord[2]+i));
	}
  
  
      /* connectivite maillage volumique SYRTHES */
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
  
      if (maillnodes->ndmat==3)
	for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],ne2=maillnodes->node[2];
	     i<maillnodes->nelem;
	     i++,ne0++,ne1++,ne2++)
	  fscanf(fgeom,"%d%d%d%d%d",&n,maillnodes->nrefe+i,ne0,ne1,ne2);
      
      else
	for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],
	       ne2=maillnodes->node[2],ne3=maillnodes->node[3];
	     i<maillnodes->nelem;
	     i++,ne0++,ne1++,ne2++,ne3++)
	  fscanf(fgeom,"%d%d%d%d%d%d",&n,maillnodes->nrefe+i,ne0,ne1,ne2,ne3);
      
      
  
      /* connectivite maillage de bord SYRTHES */

      if (maillnodebord->nelem>0)
	{
	  fgets(ch,90,fgeom);
	  fgets(ch,90,fgeom);
	  fgets(ch,90,fgeom);
	  fgets(ch,90,fgeom);
	  
	  if (maillnodebord->ndmat==3)
	    for (i=0,ne0=maillnodebord->node[0],ne1=maillnodebord->node[1],ne2=maillnodebord->node[2];
		 i<maillnodebord->nelem;
		 i++,ne0++,ne1++,ne2++)
	      fscanf(fgeom,"%d%d%d%d%d",&n,maillnodebord->nrefe+i,ne0,ne1,ne2);
	  
	  else
	    for (i=0,ne0=maillnodebord->node[0],ne1=maillnodebord->node[1];
		 i<maillnodebord->nelem;
		 i++,ne0++,ne1++)
	      fscanf(fgeom,"%d%d%d%d",&n,maillnodebord->nrefe+i,ne0,ne1);
	}
    }
  /* maillage SYRTHES en binaire         */
  /* ----------------------------------- */
  else
    {
      /* coordonnees */
      fread(maillnodes->nref,sizeof(int),maillnodes->npoin,fgeom);
      for (i=0;i<maillnodes->ndim;i++) fread(maillnodes->coord[i],sizeof(double),maillnodes->npoin,fgeom);

      /* connectivite maillage volumique SYRTHES */
      fread(maillnodes->nrefe,sizeof(int),maillnodes->nelem,fgeom);
      for (i=0;i<maillnodes->ndmat;i++) fread(maillnodes->node[i],sizeof(int),maillnodes->nelem,fgeom);

      /* connectivite maillage de bord SYRTHES */
      fread(maillnodebord->nrefe,sizeof(int),maillnodebord->nelem,fgeom);
      for (i=0;i<maillnodebord->ndmat;i++) 
	fread(maillnodebord->node[i],sizeof(int),maillnodebord->nelem,fgeom);

    }


  /*    imprime_maillage_ref(*maillnodes); */
  /* imprime_maillage_ref(*maillnodebord);*/


  /* numerotation des noeuds a partir de 0 */
  for (i=0;i<maillnodes->nelem;i++)
    for (j=0;j<maillnodes->ndmat;j++)
      maillnodes->node[j][i]--;

  for (i=0;i<maillnodebord->nelem;i++)
    for (j=0;j<maillnodebord->ndmat;j++)
      maillnodebord->node[j][i]--;

  /* -------------------------------------------------------- */
  /* -------------------------------------------------------- */
  /* cas du parallelisme : on lit les tableau complementaires */
  /* -------------------formatte ---------------------------- */
  /* -------------------------------------------------------- */
  if (sdparall->nparts>1 && form == 1)
    {
      /* C$ RUBRIQUE = NBNO PARALLELISME  */
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);

      fscanf(fgeom,"%s%s%d",ch,ch,&(sdparall->nbno_interne));
      fscanf(fgeom,"%s%s%d",ch,ch,&(sdparall->nbno_front));
      fscanf(fgeom,"%s%s%d",ch,ch,&(sdparall->nbno_global));


      /* C$ RUBRIQUE = PARALLELISME - NFOISFRONT */
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      sdparall->nfoisfront=(int*)malloc(sdparall->nbno_front*sizeof(int));
      nv=sdparall->nbno_front/12;
      for (i=0;i<nv*12;i+=12) 
	fscanf(fgeom,"%d%d%d%d%d%d%d%d%d%d%d%d\n",
	       sdparall->nfoisfront+i,  sdparall->nfoisfront+i+1,sdparall->nfoisfront+i+2,
	       sdparall->nfoisfront+i+3,sdparall->nfoisfront+i+4,sdparall->nfoisfront+i+5,
	       sdparall->nfoisfront+i+6,sdparall->nfoisfront+i+7,sdparall->nfoisfront+i+8,
	       sdparall->nfoisfront+i+9,sdparall->nfoisfront+i+10,sdparall->nfoisfront+i+11);
      for (i=nv*12;i<sdparall->nbno_front;i++) fscanf(fgeom,"%d",sdparall->nfoisfront+i);

/*       for (i=0; i<sdparall->nbno_front;i++) */
/* 	printf("i=%d sdparall->nfoisfront=%d\n",i,sdparall->nfoisfront[i]); */


      /* C$ RUBRIQUE = PARALLELISME  - NOMBRE DE NOEUDS COMMUNS AVEC LES AUTRES PARTITIONS */
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      if (strlen(ch)>5) fgets(ch,90,fgeom); 
      sdparall->nbcommun=(int*)malloc(sdparall->nparts*sizeof(int));
      sdparall->adrcommun=(int*)malloc(sdparall->nparts*sizeof(int));
      nv=sdparall->nparts/6;
      for (i=0;i<nv*6;i+=6) 
	fscanf(fgeom,"%d%d%d%d%d%d",
	       sdparall->nbcommun+i,  sdparall->nbcommun+i+1,sdparall->nbcommun+i+2,
	       sdparall->nbcommun+i+3,sdparall->nbcommun+i+4,sdparall->nbcommun+i+5);
      for (i=nv*6;i<sdparall->nparts;i++) fscanf(fgeom,"%d",sdparall->nbcommun+i);
      for (sdparall->adrcommun[0]=0,i=1;i<sdparall->nparts;i++)  
	sdparall->adrcommun[i]=sdparall->adrcommun[i-1]+sdparall->nbcommun[i-1];

      /* printf("nombre de noeuds commun avec les autres part\n");
      for (i=0;i<sdparall->nparts;i++) printf(" %d",sdparall->nbcommun[i]);printf("\n"); */

      /* C$ RUBRIQUE = PARALLELISME  - NOEUDS COMMUNS AVEC LES AUTRES PARTITIONS */
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      if (strlen(ch)>5) fgets(ch,90,fgeom); 
      for (nb=0,i=0;i<sdparall->nparts;i++) nb+=sdparall->nbcommun[i];
      if (nb>0)
	{
	  sdparall->tcommun=(int*)malloc(nb*sizeof(int));
	  nv=nb/6;
	  for (i=0;i<nv*6;i+=6) 
	    fscanf(fgeom,"%d%d%d%d%d%d",
		   sdparall->tcommun+i,  sdparall->tcommun+i+1,sdparall->tcommun+i+2,
		   sdparall->tcommun+i+3,sdparall->tcommun+i+4,sdparall->tcommun+i+5);
	  for (i=nv*6;i<nb;i++) fscanf(fgeom,"%d",sdparall->tcommun+i);
	  for (i=0;i<nb;i++) sdparall->tcommun[i]--;
	}
/*       printf("liste des noeuds communs avec les autres part\n"); */
/*       for (i=0;i<nb;i++) printf(" %d",sdparall->tcommun[i]);printf("\n"); */


      /* C$ RUBRIQUE = NUMEROS GLOBAUX DES NOEUDS */
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      if (strlen(ch)>5) fgets(ch,90,fgeom); 
      sdparall->npglob=(int*)malloc(maillnodes->npoin*sizeof(int));
      nv=maillnodes->npoin/6;
      for (i=0;i<nv*6;i+=6) 
	fscanf(fgeom,"%d%d%d%d%d%d",
	       sdparall->npglob+i,  sdparall->npglob+i+1,sdparall->npglob+i+2,
	       sdparall->npglob+i+3,sdparall->npglob+i+4,sdparall->npglob+i+5);
      for (i=nv*6;i<maillnodes->npoin;i++) fscanf(fgeom,"%d",sdparall->npglob+i);
      for (i=0;i<maillnodes->npoin;i++) sdparall->npglob[i]--;


  /* -------------------------------------------------------------------- */
  /* cas du parallelisme : lecture de la rubrique suivante si elle existe */
  /* -------------------------------------------------------------------- */
      rubrc=rubperio=0;
      fgets(ch,90,fgeom); if (!ch){fclose(fgeom); return;}
      fgets(ch,90,fgeom); if (!ch){fclose(fgeom); return;}
      if (strlen(ch)>5)
	if (!strncmp(ch+3,"RUBRIQUE = PARALLELISME  - RESISTANCES DE CONTACT",49)) 
	  rubrc=1;
	else if (!strncmp(ch+3,"RUBRIQUE = PARALLELISME  - PERIODICITE",38)) 
	  rubperio=1;
      fgets(ch,90,fgeom);
      if (strlen(ch)>5)
	if (!strncmp(ch+3,"RUBRIQUE = PARALLELISME  - RESISTANCES DE CONTACT",49))
	  rubrc=1;
	else if (!strncmp(ch+3,"RUBRIQUE = PARALLELISME  - PERIODICITE",38))  
	  rubperio=1;
      if (strlen(ch)>5) fgets(ch,90,fgeom); 





   /* -------------------------------------------------------- */
  /* cas du parallelisme : lecture des resistances de contact */
  /* -------------------------------------------------------- */
      if (rubrc) 
	{
	  /* C$ RUBRIQUE = PARALLELISME  - RESISTANCES DE CONTACT - NBNO AVEC AUTRES PARTITIONS */
	  if (SYRTHES_LANG == FR)
	    printf(" --> lecture des donnees geometriques relatives aux resistances de contact\n");
	  else if (SYRTHES_LANG == EN)
	    printf(" --> Reading of geometrical data relative to the contact resistance\n");
	  sdparall->nbcommunrc=(int*)malloc(sdparall->nparts*sizeof(int));
	  sdparall->adrcommunrc=(int*)malloc(sdparall->nparts*sizeof(int));
	  nv=sdparall->nparts/6;
	  for (i=0;i<nv*6;i+=6) 
	    fscanf(fgeom,"%d%d%d%d%d%d",
		   sdparall->nbcommunrc+i,  sdparall->nbcommunrc+i+1,sdparall->nbcommunrc+i+2,
		   sdparall->nbcommunrc+i+3,sdparall->nbcommunrc+i+4,sdparall->nbcommunrc+i+5);
	  for (i=nv*6;i<sdparall->nparts;i++) fscanf(fgeom,"%d",sdparall->nbcommunrc+i);

	  for (sdparall->adrcommunrc[0]=0,i=1;i<sdparall->nparts;i++)  
	    sdparall->adrcommunrc[i]=sdparall->adrcommunrc[i-1]+sdparall->nbcommunrc[i-1]*2;
	  
	  if (SYRTHES_LANG == FR)
	    printf("nombre de noeuds commun avec les autres part\n");
	  else if (SYRTHES_LANG == EN)
	    printf("Nodes number in common with other parts \n");
	  for (i=0;i<sdparall->nparts;i++) printf(" %d",sdparall->nbcommunrc[i]);printf("\n");
	  
	  /* C$ RUBRIQUE = PARALLELISME  - RESISTANCES DE CONTACT */
	  fgets(ch,90,fgeom);
	  fgets(ch,90,fgeom);
	  fgets(ch,90,fgeom);
	  if (strlen(ch)>5) fgets(ch,90,fgeom); 
	  for (nb=0,i=0;i<sdparall->nparts;i++) nb+=sdparall->nbcommunrc[i];
	  if (nb>0)
	    {
	      sdparall->tcommunrc=(int*)malloc(nb*2*sizeof(int));
	      sdparall->nfoisrc=(int*)malloc(nb*sizeof(int));
	      nv=nb/2;
	      for (n=i=j=0;n<nv;n++,i+=4,j+=2) 
		{
		  fscanf(fgeom,"%d%d%d%d%d%d",
			 sdparall->tcommunrc+i,  sdparall->tcommunrc+i+1,sdparall->nfoisrc+j,
			 sdparall->tcommunrc+i+2,sdparall->tcommunrc+i+3,sdparall->nfoisrc+j+1);
		  
/* 		  if (SYRTHES_LANG == FR) */
/* 		    printf("on a a lu nv=%d  rc=%d rc=%d nf=%d\n           et   rc=%d rc=%d nf=%d\n", */
/* 			   n,sdparall->tcommunrc[i],  sdparall->tcommunrc[i+1],sdparall->nfoisrc[j], */
/* 			   sdparall->tcommunrc[i+2],sdparall->tcommunrc[i+3],sdparall->nfoisrc[j+1]); */
/* 		  else if (SYRTHES_LANG == EN) */
/* 		    printf("one has read nv=%d  rc=%d rc=%d nf=%d\n           et   rc=%d rc=%d nf=%d\n", */
/* 			   n,sdparall->tcommunrc[i],  sdparall->tcommunrc[i+1],sdparall->nfoisrc[j], */
/* 			   sdparall->tcommunrc[i+2],sdparall->tcommunrc[i+3],sdparall->nfoisrc[j+1]); */
		}

	      /* if (SYRTHES_LANG == FR) */
	      /* 	printf("ON LIT LA FIN %d\n", nb-nv*2); */
	      /* else if (SYRTHES_LANG == EN) */
	      /* 	printf("END being read %d\n", nb-nv*2); */

	      for (i=0;i<nb-nv*2;i++) 
		{
		  fscanf(fgeom,"%d%d%d",
			 sdparall->tcommunrc+nv*4+i*2,sdparall->tcommunrc+nv*4+i*2+1,sdparall->nfoisrc+nv*2+i);
/* 		  if (SYRTHES_LANG == FR) */
/* 		    printf("on a a lu nv=%d  i=%d rc=%d      i=%d  rc=%d nf=%d\n", */
/* 			   i,(nv+i)*2,sdparall->tcommunrc[(nv+i)*2],  (nv+i)*2+1,sdparall->tcommunrc[(nv+i)*2+1],sdparall->nfoisrc[nv*2+i]); */
/* 		  else if (SYRTHES_LANG == EN) */
/* 		    printf("one has read nv=%d  i=%d rc=%d      i=%d  rc=%d nf=%d\n", */
/* 			   i,(nv+i)*2,sdparall->tcommunrc[(nv+i)*2],  (nv+i)*2+1,sdparall->tcommunrc[(nv+i)*2+1],sdparall->nfoisrc[nv*2+i]); */
		}

	      for (i=0;i<nb*2;i++) sdparall->tcommunrc[i]--;

/* 	      if (SYRTHES_LANG == FR) */
/* 		printf("Couples de resistances de contact :\n"); */
/* 	      else if (SYRTHES_LANG == EN) */
/* 		printf("Couple of contact resistance :\n"); */
/* 	      for (i=j=0;i<nb*2;i+=2,j++)  */
/* 		printf("(%d,%d / %d)",sdparall->tcommunrc[i],sdparall->tcommunrc[i+1],sdparall->nfoisrc[j]);printf("\n"); */
	    }

	  /* on prepare la lecture d'une eventuelle rubrique suivante */
	  rubperio=0;
	  fgets(ch,90,fgeom); if (!ch){fclose(fgeom); return;}
	  fgets(ch,90,fgeom); if (!ch){fclose(fgeom); return;}
	  if (strlen(ch)>5 && !strncmp(ch+3,"RUBRIQUE = PARALLELISME  - PERIODICITE",38)) rubperio=1;
	  fgets(ch,90,fgeom);
	  if (strlen(ch)>5 && !strncmp(ch+3,"RUBRIQUE = PARALLELISME  - PERIODICITE",38)) rubperio=1;
	  if (strlen(ch)>5) fgets(ch,90,fgeom); 
	}

      /* -------------------------------------------------------- */
      /* cas du parallelisme : lecture de la periodicite          */
      /* -------------------------------------------------------- */
      if (rubperio)
	{
	  /* C$ RUBRIQUE = PARALLELISME  - PERIODICITE - NBNO AVEC AUTRES PARTITIONS */
	  sdparall->nbcommunperio=(int*)malloc(sdparall->nparts*sizeof(int));
	  sdparall->adrcommunperio=(int*)malloc(sdparall->nparts*sizeof(int));
	  nv=sdparall->nparts/6;
	  for (i=0;i<nv*6;i+=6) 
	    fscanf(fgeom,"%d%d%d%d%d%d",
		   sdparall->nbcommunperio+i,  sdparall->nbcommunperio+i+1,sdparall->nbcommunperio+i+2,
		   sdparall->nbcommunperio+i+3,sdparall->nbcommunperio+i+4,sdparall->nbcommunperio+i+5);
	  for (i=nv*6;i<sdparall->nparts;i++) fscanf(fgeom,"%d",sdparall->nbcommunperio+i);
	  
	  for (sdparall->adrcommunperio[0]=0,i=1;i<sdparall->nparts;i++)  
	    sdparall->adrcommunperio[i]=sdparall->adrcommunperio[i-1]+sdparall->nbcommunperio[i-1]*2;
	  /* if (SYRTHES_LANG == FR) */
	  /*   printf("nombre de noeuds commun avec les autres part\n"); */
	  /* else if (SYRTHES_LANG == EN) */
	  /*   printf("number of nodes common with other parts\n"); */
	  /* for (i=0;i<sdparall->nparts;i++) printf(" %d",sdparall->nbcommunperio[i]);printf("\n"); */
	  
	  /* C$ RUBRIQUE = PARALLELISME  - PERIODICITE */
	  fgets(ch,90,fgeom);
	  fgets(ch,90,fgeom);
	  fgets(ch,90,fgeom);
	  if (strlen(ch)>5) fgets(ch,90,fgeom); 
	  for (nb=0,i=0;i<sdparall->nparts;i++) nb+=sdparall->nbcommunperio[i];
	  if (nb>0)
	    {
	      sdparall->tcommunperio=(int*)malloc(nb*2*sizeof(int));
	      sdparall->nfoisperio=(int*)malloc(nb*sizeof(int));
	      nv=nb/2;
	      for (n=i=j=0;n<nv;n++,i+=4,j+=2) 
		{
		  fscanf(fgeom,"%d%d%d%d%d%d",
			 sdparall->tcommunperio+i,  sdparall->tcommunperio+i+1,sdparall->nfoisperio+j,
			 sdparall->tcommunperio+i+2,sdparall->tcommunperio+i+3,sdparall->nfoisperio+j+1);
		  
/* 		  printf("on a a lu nv=%d  perio=%d perio=%d nf=%d\n           et   perio=%d perio=%d nf=%d\n", */
/* 			 n,sdparall->tcommunperio[i],  sdparall->tcommunperio[i+1],sdparall->nfoisperio[j], */
/* 			 sdparall->tcommunperio[i+2],sdparall->tcommunperio[i+3],sdparall->nfoisperio[j+1]); */
		}
	      printf("ON LIT LA FIN %d\n", nb-nv*2);
	      for (i=0;i<nb-nv*2;i++) 
		{
		  fscanf(fgeom,"%d%d%d",
			 sdparall->tcommunperio+nv*4+i*2,sdparall->tcommunperio+nv*4+i*2+1,sdparall->nfoisperio+nv*2+i);
		  if (SYRTHES_LANG == FR)
		    printf("on a a lu nv=%d  i=%d perio=%d      i=%d  perio=%d nf=%d\n",
			   i,(nv+i)*2,sdparall->tcommunperio[(nv+i)*2],  (nv+i)*2+1,sdparall->tcommunperio[(nv+i)*2+1],sdparall->nfoisperio[nv*2+i]);
		  else if (SYRTHES_LANG == EN)
		    printf("One has read nv=%d  i=%d perio=%d      i=%d  perio=%d nf=%d\n",
			   i,(nv+i)*2,sdparall->tcommunperio[(nv+i)*2],  (nv+i)*2+1,sdparall->tcommunperio[(nv+i)*2+1],sdparall->nfoisperio[nv*2+i]);
		}

	      for (i=0;i<nb*2;i++) sdparall->tcommunperio[i]--;
	      
	      /* if (SYRTHES_LANG == FR) */
	      /* 	printf("Couples de noeuds periodiques : part %d\n",sdparall->rang); */
	      /* else if (SYRTHES_LANG == EN) */
	      /* 	printf("Couples of periodic nodes : part %d\n",sdparall->rang); */
	      /* for (i=j=0;i<nb*2;i+=2,j++)  */
	      /* 	printf("(%d,%d / %d)",sdparall->tcommunperio[i],sdparall->tcommunperio[i+1],sdparall->nfoisperio[j]);printf("\n"); */
	    }

	} /* fin de la periodicite */


      /* allocation des tableaux de travail */

      for (nb=i=0;i<sdparall->nparts;i++) nb+=sdparall->nbcommun[i];
      /* Rq: nb >= sdparall->nbno_front car des noeuds peubvent etre frontiere avec plusieurs domaines */

      sdparall->trav1=(double*)malloc(nb*sizeof(double));
      sdparall->trav2=(double*)malloc(nb*sizeof(double));


    }/* fin du parallelisme en formatte*/


  /* -------------------------------------------------------- */
  /* -------------------------------------------------------- */
  /* cas du parallelisme : on lit les tableau complementaires */
  /* ------------------- binaire ---------------------------- */
  /* -------------------------------------------------------- */
  else if (sdparall->nparts>1 && form == 0)
    {
      /* tableau preliminaire de dimensions */
      fread(itrav,sizeof(int),6,fgeom);
      npart_lu=itrav[0];
      sdparall->nbno_interne=itrav[1];
      sdparall->nbno_front=itrav[2];
      sdparall->nbno_global=itrav[3];
      rescon_lu=itrav[4];
      perio_lu=itrav[5];

      /* C$ RUBRIQUE = PARALLELISME - NFOISFRONT */
      sdparall->nfoisfront=(int*)malloc(sdparall->nbno_front*sizeof(int));
      fread(sdparall->nfoisfront,sizeof(int),sdparall->nbno_front,fgeom);


      /* C$ RUBRIQUE = PARALLELISME  - NOMBRE DE NOEUDS COMMUNS AVEC LES AUTRES PARTITIONS */
      sdparall->nbcommun=(int*)malloc(sdparall->nparts*sizeof(int));
      sdparall->adrcommun=(int*)malloc(sdparall->nparts*sizeof(int));
      fread(sdparall->nbcommun,sizeof(int),sdparall->nparts,fgeom);
      for (sdparall->adrcommun[0]=0,i=1;i<sdparall->nparts;i++)  
	sdparall->adrcommun[i]=sdparall->adrcommun[i-1]+sdparall->nbcommun[i-1];


      /* C$ RUBRIQUE = PARALLELISME  - NOEUDS COMMUNS AVEC LES AUTRES PARTITIONS */
      for (nb=0,i=0;i<sdparall->nparts;i++) nb+=sdparall->nbcommun[i];
      if (nb>0) fread(sdparall->tcommun,sizeof(int),nb,fgeom);
      for (i=0;i<nb;i++) sdparall->tcommun[i]--;


      /* C$ RUBRIQUE = NUMEROS GLOBAUX DES NOEUDS */
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      if (strlen(ch)>5) fgets(ch,90,fgeom); 
      sdparall->npglob=(int*)malloc(maillnodes->npoin*sizeof(int));
      fread(sdparall->npglob,sizeof(int),maillnodes->npoin,fgeom);
      for (i=0;i<maillnodes->npoin;i++) sdparall->npglob[i]--;


 
   /* -------------------------------------------------------- */
  /* cas du parallelisme : lecture des resistances de contact */
  /* -------------------------------------------------------- */
      if (rescon_lu) 
	{
	  /* C$ RUBRIQUE = PARALLELISME  - RESISTANCES DE CONTACT - NBNO AVEC AUTRES PARTITIONS */
	  if (SYRTHES_LANG == FR)
	    printf(" --> lecture des donnees geometriques relatives aux resistances de contact\n");
	  else if (SYRTHES_LANG == EN)
	    printf(" --> Reading of geometrical data relative to the contact resistance\n");
	  sdparall->nbcommunrc=(int*)malloc(sdparall->nparts*sizeof(int));
	  sdparall->adrcommunrc=(int*)malloc(sdparall->nparts*sizeof(int));

	  fread(sdparall->nbcommunrc,sizeof(int),sdparall->nparts,fgeom);

	  for (sdparall->adrcommunrc[0]=0,i=1;i<sdparall->nparts;i++)  
	    sdparall->adrcommunrc[i]=sdparall->adrcommunrc[i-1]+sdparall->nbcommunrc[i-1]*2;
	  
	  if (SYRTHES_LANG == FR)
	    printf("nombre de noeuds commun avec les autres part\n");
	  else if (SYRTHES_LANG == EN)
	    printf("Nodes number in common with other parts \n");
	  for (i=0;i<sdparall->nparts;i++) printf(" %d",sdparall->nbcommunrc[i]);printf("\n");
	  
	  /* C$ RUBRIQUE = PARALLELISME  - RESISTANCES DE CONTACT */
	  for (nb=0,i=0;i<sdparall->nparts;i++) nb+=sdparall->nbcommunrc[i];
	  if (nb>0)
	    {
	      sdparall->tcommunrc=(int*)malloc(nb*2*sizeof(int));
	      sdparall->nfoisrc=(int*)malloc(nb*sizeof(int));

	      btrav=(int*)malloc(nb*3*sizeof(int));
	      fread(btrav,sizeof(int),nb*3,fgeom); 

	      for (n=i=j=0; n<nb; n++,j+=2,i+=3)
		{
		  sdparall->tcommunrc[j]=btrav[i];
		  sdparall->tcommunrc[j+1]=btrav[i+1];
		  sdparall->nfoisrc[n]=btrav[i+2];
		}
	      free(btrav);

/* 	      printf("on a a lu nv=%d  rc=%d rc=%d nf=%d\n           et   rc=%d rc=%d nf=%d\n", */
/* 		     n,sdparall->tcommunrc[i],  sdparall->tcommunrc[i+1],sdparall->nfoisrc[j], */

	      for (i=0;i<nb*2;i++) sdparall->tcommunrc[i]--;

/* 	      if (SYRTHES_LANG == FR) */
/* 		printf("Couples de resistances de contact :\n"); */
/* 	      else if (SYRTHES_LANG == EN) */
/* 		printf("Couple of contact resistance :\n"); */
/* 	      for (i=j=0;i<nb*2;i+=2,j++)  */
/* 		printf("(%d,%d / %d)",sdparall->tcommunrc[i],sdparall->tcommunrc[i+1], */
/* 		       sdparall->nfoisrc[j]);printf("\n"); */
	    }

	}

      /* -------------------------------------------------------- */
      /* cas du parallelisme : lecture de la periodicite          */
      /* -------------------------------------------------------- */
      if (perio_lu)
	{
	  /* C$ RUBRIQUE = PARALLELISME  - PERIODICITE - NBNO AVEC AUTRES PARTITIONS */
	  sdparall->nbcommunperio=(int*)malloc(sdparall->nparts*sizeof(int));
	  sdparall->adrcommunperio=(int*)malloc(sdparall->nparts*sizeof(int));
	  
	  fread(sdparall->nbcommunperio,sizeof(int),sdparall->nparts,fgeom);

	  for (sdparall->adrcommunperio[0]=0,i=1;i<sdparall->nparts;i++)  
	    sdparall->adrcommunperio[i]=sdparall->adrcommunperio[i-1]+sdparall->nbcommunperio[i-1]*2;

/* 	  printf("nombre de noeuds commun avec les autres part\n"); */
/* 	  for (i=0;i<sdparall->nparts;i++) printf(" %d",sdparall->nbcommunperio[i]);printf("\n"); */
	  
	  /* C$ RUBRIQUE = PARALLELISME  - PERIODICITE */
	  for (nb=0,i=0;i<sdparall->nparts;i++) nb+=sdparall->nbcommunperio[i];
	  if (nb>0)
	    {
	      sdparall->tcommunperio=(int*)malloc(nb*2*sizeof(int));
	      sdparall->nfoisperio=(int*)malloc(nb*sizeof(int));
	      
	      btrav=(int*)malloc(nb*3*sizeof(int));
	      fread(btrav,sizeof(int),nb*3,fgeom); 

	      for (n=i=j=0; n<nb; n++,j+=2,i+=3)
		{
		  sdparall->tcommunperio[j]=btrav[i];
		  sdparall->tcommunperio[j+1]=btrav[i+1];
		  sdparall->nfoisperio[n]=btrav[i+2];
		}
	      free(btrav);

/* 	      printf("on a a lu nv=%d  perio=%d perio=%d nf=%d\n           et   perio=%d perio=%d nf=%d\n", */
/* 		     n,sdparall->tcommunperio[i],  sdparall->tcommunperio[i+1],sdparall->nfoisperio[j], */
/* 		     sdparall->tcommunperio[i+2],sdparall->tcommunperio[i+3],sdparall->nfoisperio[j+1]); */
	      
	      for (i=0;i<nb*2;i++) sdparall->tcommunperio[i]--;
	      
/* 	      printf("Couples de noeuds periodiques : part %d\n",sdparall->rang); */
/* 	      for (i=j=0;i<nb*2;i+=2,j++)  */
/* 		printf("(%d,%d / %d)",sdparall->tcommunperio[i],sdparall->tcommunperio[i+1], */
/* 		       sdparall->nfoisperio[j]);printf("\n"); */
	    }

	} /* fin de la periodicite */


      /* allocation des tableaux de travail */

      for (nb=i=0;i<sdparall->nparts;i++) nb+=sdparall->nbcommun[i];
      /* Rq: nb >= sdparall->nbno_front car des noeuds peubvent etre frontiere avec plusieurs domaines */

      sdparall->trav1=(double*)malloc(nb*sizeof(double));
      sdparall->trav2=(double*)malloc(nb*sizeof(double));



    }/* fin du parallelisme binaire*/


  nptot=somme_int_parall(maillnodes->npoin);
  netot=somme_int_parall(maillnodes->nelem);
  nebtot=somme_int_parall(maillnodebord->nelem);

  if (sdparall->nparts== 1 || sdparall->rang==0)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n\n *** MAILLAGE SYRTHES \n");
	  printf("                           |--------------------|------------------|\n");
	  printf("                           | Maillage volumique | Maillage de bord |\n");
	  printf("      ---------------------|--------------------|------------------|\n");
	  printf("      | Dimension          |    %12d    |    %12d  |\n",maillnodes->ndim,maillnodebord->ndim);
	  printf("      | Nombre de noeuds   |    %12d    |     inusite      |\n",nptot);
	  printf("      | Nombre d'elements  |    %12d    |    %12d  |\n",netot,nebtot);
	  printf("      | Nb noeuds par elt  |    %12d    |    %12d  |\n",maillnodes->ndmat,maillnodebord->ndmat);
	  printf("      ---------------------|--------------------|------------------|\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n\n *** SYRTHES MESH \n");
	  printf("                           |--------------------|------------------|\n");
	  printf("                           |   Volumic mesh     |  Boundary mesh   |\n");
	  printf("      ---------------------|--------------------|------------------|\n");
	  printf("      | Dimension          |    %12d    |    %12d  |\n",maillnodes->ndim,maillnodebord->ndim);
	  printf("      | Number of nodes    |    %12d    |      unused      |\n",nptot);
	  printf("      | Number of elements |    %12d    |    %12d  |\n",netot,nebtot);
	  printf("      | Nb nodes per elt   |    %12d    |    %12d  |\n",maillnodes->ndmat,maillnodebord->ndmat);
	  printf("      ---------------------|--------------------|------------------|\n");
	}
    }

  fclose(fgeom);

}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture du fichier suite Syrthes                                     |
  |======================================================================| */
void lire_suite(struct Maillage maillnodes,
		struct PasDeTemps *pasdetemps,
		struct Variable *variable)
{
  FILE  *fs;
  char ch[200],ch4[4],chrien[20],chrien1[20],chrien2[20],chrien3[20];
  int i,nbplu,typlu,nv,ligne_incomplete=0;
  double dt;
  double *tmps,*pv,*pt;
  if ((fs=fopen(nomsuit,"r")) == NULL)
    {
      if (SYRTHES_LANG == FR)
	printf("Impossible d'ouvrir le fichier suite :%s\n",nomsuit);
      else if (SYRTHES_LANG == EN)
	printf("Impossible to open the restart file :%s\n",nomsuit);
      syrthes_exit(1) ;
    }

  pasdetemps->premier=0;
  
  /* lecture de l'entete */
  /* ------------------- */
  fgets(ch,200,fs);
  fgets(ch,200,fs);
  fgets(ch,200,fs);
  fscanf(fs,"%s%d%s%lf%s%lf\n",chrien,&(pasdetemps->ntsyr),
	                       chrien,&(pasdetemps->tempss),
	                       chrien,&dt);

  /* recalage eventuel du temps physique */
  if (pasdetemps->new_t_init>=0) pasdetemps->tempss=pasdetemps->new_t_init;

  if (pasdetemps->dtauto.actif) pasdetemps->rdtts=dt;

  fgets(ch,200,fs);
  fscanf(fs,"%s%s%s%d%s%d\n",chrien,chrien, chrien,&typlu, chrien,&nbplu);


  if (nbplu!=maillnodes.npoin)
    if (SYRTHES_LANG == FR)
      {
	printf("\n %%%% ERREUR dans le fichier suite. C'est le resultat d'un calcul a %d noeuds\n",nbplu);
	printf("           or le calcul en cours possede %d noeuds\n",maillnodes.npoin);
	syrthes_exit(1);
      }
    else if (SYRTHES_LANG == EN)
      {
	printf("\n %%%% ERROR in the restart file. This is the result of a case with %d nodes\n",nbplu);
	printf("          the present calculation has %d nodes\n",maillnodes.npoin);
	syrthes_exit(1);
      }


  nv=maillnodes.npoin/6;
  if (nv*6<maillnodes.npoin) ligne_incomplete=1;

  /* lecture de la temperature */
  /* ------------------------- */
  tmps=variable->var[variable->adr_t];
  for (i=0;i<nv*6;i+=6)  fscanf(fs,"%16lf%16lf%16lf%16lf%16lf%16lf\n",
				tmps+i,tmps+i+1,tmps+i+2,tmps+i+3,tmps+i+4,tmps+i+5);

  if (ligne_incomplete>0)
    {
      for (i=nv*6;i<maillnodes.npoin;i++) fscanf(fs,"%16lf",tmps+i);
      fgets(ch,120,fs);
    }
  

  /* lecture eventuelle des autres variables */
  /* ------------------------- */
  if (variable->nbvar>1)
    {
      /* lecture de PV */
      /* ------------- */
      
      pv=variable->var[variable->adr_pv];
      fgets(ch,120,fs);
      for (i=0;i<nv*6;i+=6)  fscanf(fs,"%16lf%16lf%16lf%16lf%16lf%16lf\n",
				    pv+i,pv+i+1,pv+i+2,pv+i+3,pv+i+4,pv+i+5);
      
      if (ligne_incomplete>0)
	{
	  for (i=nv*6;i<maillnodes.npoin;i++) fscanf(fs,"%16lf",pv+i);
	  fgets(ch,120,fs);
	}
 
     
      /* lecture de PT */
      /* ------------- */
      pt=variable->var[variable->adr_pt];
      fgets(ch,120,fs);
      for (i=0;i<nv*6;i+=6)  fscanf(fs,"%16lf%16lf%16lf%16lf%16lf%16lf\n",
				    pt+i,pt+i+1,pt+i+2,pt+i+3,pt+i+4,pt+i+5);
      
      if (ligne_incomplete>0)
	{
	  for (i=nv*6;i<maillnodes.npoin;i++) fscanf(fs,"%16lf",pt+i);
	  fgets(ch,120,fs);
	}
      

    }


  if (syrglob_nparts==1 ||syrglob_rang==0)
    {	    
      if (SYRTHES_LANG == FR)
	{
	  printf("\n *** SUITE DE CALCUL - On reprend le calcul :\n");
	  printf("      - a l'iteration %d\n",pasdetemps->ntsyr);
	  printf("      - au temps %16.9e\n",pasdetemps->tempss);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n *** RESTART CALCULATION  :\n");
	  printf("      - at time step %d\n",pasdetemps->ntsyr);
	  printf("      - at time %16.9e\n",pasdetemps->tempss);
	}
    }

  fclose(fs);
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture du fichier meteo                                             |
  |                                                                      |
  | Structure du fichier meteo imposee :                                 | 
  | 1ere ligne = nbre de variable a lire 9ie = nbre de colonnes          |
  | On donne ensuite sur la meme ligne les n variables                   |
  | Le nombre de lignes est ensuite quelconque                           |
  |======================================================================| */
void lire_meteo(struct Meteo *meteo,int nbande)
{
  int i,n;
  double *p,t,fg,fd,te,vv,hr,xpv,xpt;
  char ch[CHLONG];
  FILE *fmeteo; 

  if ((fmeteo=fopen(nommeteo,"r")) == NULL)
    {
      if (SYRTHES_LANG == FR)
	printf("\nImpossible d'ouvrir le fichier meteo : %s\n",nommeteo);
      else if (SYRTHES_LANG == EN)
	printf("\nImpossible to open the weather file : %s\n",nommeteo);
      syrthes_exit(1) ;
    }
  else{
    if (SYRTHES_LANG == FR)
      printf("\n *** LECTURE DU FICHIER METEO :%s\n",nommeteo);
    else if (SYRTHES_LANG == EN)
      printf("\n *** READING WEATHER FILE :%s\n",nommeteo);
  }


  fscanf(fmeteo,"%d",&(meteo->nbvar)); fgets(ch,CHLONG,fmeteo);

  i=0;
  while(fgets(ch,CHLONG,fmeteo), !feof(fmeteo)) i++;
  if (SYRTHES_LANG == FR)
    printf("      - le fichier meteo contient %d pas de temps\n",i);
  else if (SYRTHES_LANG == EN)
    printf("      - the weather file contains %d time steps\n",i);


  /* allocations */
  meteo->nelem=i;
  meteo->var=(double**)malloc(meteo->nbvar*sizeof(double*));
  for (n=0;n<meteo->nbvar;n++)
    meteo->var[n]=(double*)malloc(meteo->nelem*sizeof(double));

  verif_alloue_double2d(meteo->nbvar,"lire_meteo",meteo->var);


  fseek(fmeteo,0L,SEEK_SET);

  fscanf(fmeteo,"%d",&i);fgets(ch,CHLONG,fmeteo);

  for (n=0;n<meteo->nelem;n++)
    {
      for (i=0;i<meteo->nbvar;i++) fscanf(fmeteo,"%lf",&(meteo->var[i][n]));
      fgets(ch,CHLONG,fmeteo);
    }

  fclose(fmeteo);
      
}
 





