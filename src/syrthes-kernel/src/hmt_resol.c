/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_bd.h"
#include "syr_hmt_libmat.h"
#include "syr_hmt_bd.h"
#include "syr_option.h"
#include "syr_abs.h"
#include "syr_tree.h"
#include "syr_const.h"
#include "syr_parall.h"

#include "syr_proto.h"
#include "syr_hmt_proto.h"

#ifdef _SYRTHES_MPI_
#include "mpi.h"
MPI_Status status;
#endif

extern struct Performances perfo;
extern struct Affichages affich;
extern int optim,lmst;

extern int nbVar;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |   Gestion de la resolution de la conduction                          |
  |======================================================================| */
void hmt_resol(struct PasDeTemps *pasdetemps,
	       struct Maillage maillnodes, struct MaillageCL maillnodeus,  
	       struct MaillageCL maillnoderc,struct MaillageBord maillnodebord,  
	       struct Cperio perio,
	       struct Climcfd scoupf,struct Climcfd scouvf,
	       struct Contact rescon,struct Couple scoupr,struct Clim rayinf,
	       struct HmtClimhhh hmtclimhhh,
	       struct Cvol *fluxvol,struct Mst mst,
	       struct Variable variable,double **var_ele,
	       struct Humid humid,
	       struct ConstPhyhmt constphyhmt,struct ConstMateriaux *constmateriaux,
	       struct Meteo meteo,struct Myfile myfile,
	       struct Matrice matr,struct Travail trav,struct Travail trave,
	       struct SDparall sdparall)
{
  int i,j,n;
  double s,t;
  double *p,*q,*var;

  double t_aux,pv_aux,psat;

  /* incrementation des variables :  n-2 = n-1 */
  p=variable.var[variable.adr_t_m2];  q=variable.var[variable.adr_t_m1];  for (i=0;i<maillnodes.npoin;i++) *(p+i) = *(q+i);
  p=variable.var[variable.adr_pv_m2]; q=variable.var[variable.adr_pv_m1]; for (i=0;i<maillnodes.npoin;i++) *(p+i) = *(q+i);
  p=variable.var[variable.adr_pt_m2]; q=variable.var[variable.adr_pt_m1]; for (i=0;i<maillnodes.npoin;i++) *(p+i) = *(q+i);

  /* incrementation des variables :  n-1 = n */
  p=variable.var[variable.adr_t_m1];  q=variable.var[variable.adr_t];  for (i=0;i<maillnodes.npoin;i++) *(p+i) = *(q+i);
  p=variable.var[variable.adr_pv_m1]; q=variable.var[variable.adr_pv]; for (i=0;i<maillnodes.npoin;i++) *(p+i) = *(q+i);
  p=variable.var[variable.adr_pt_m1]; q=variable.var[variable.adr_pt]; for (i=0;i<maillnodes.npoin;i++) *(p+i) = *(q+i);


  /* conditions aux limites */ 
  user_hmt_limfso_fct(maillnodes,maillnodeus,
		      variable.var[variable.adr_t_m1],
		      variable.var[variable.adr_pv_m1],
		      variable.var[variable.adr_pt_m1],
		      hmtclimhhh,rayinf,pasdetemps->tempss);

  user_hmt_limfso(maillnodes,maillnodeus,
		  variable.var[variable.adr_t_m1],
		  variable.var[variable.adr_pv_m1],
		  variable.var[variable.adr_pt_m1],
		  hmtclimhhh,rayinf,pasdetemps,meteo,myfile);
  
  /* flux volumiques */
  if (fluxvol[ADR_T].nelem || fluxvol[ADR_PV].nelem || fluxvol[ADR_PT].nelem ) 
    user_hmt_cfluvs_fct(maillnodes,
			variable.var[variable.adr_t_m1], fluxvol[ADR_T],
			variable.var[variable.adr_pv_m1],fluxvol[ADR_PV],
			variable.var[variable.adr_pt_m1],fluxvol[ADR_PT],
			pasdetemps->tempss);

  user_hmt_cfluvs(maillnodes,
		  variable.var[variable.adr_t_m1], fluxvol[ADR_T],
		  variable.var[variable.adr_pv_m1],fluxvol[ADR_PV],
		  variable.var[variable.adr_pt_m1],fluxvol[ADR_PT],
		  pasdetemps,humid,meteo,myfile);
  


  /* variables sur les elements */
  for (n=0;n<variable.nbvar;n++)
    {
      switch (n){
      case 0: var=variable.var[variable.adr_t_m1]; break;
      case 1: var=variable.var[variable.adr_pv_m1];break;
      case 2: var=variable.var[variable.adr_pt_m1];break;
      }
      
      for (i=0;i<maillnodes.nelem;i++)
	{
	  for (t=0,j=0;j<maillnodes.ndmat;j++) t+=var[maillnodes.node[j][i]];
	  var_ele[n][i]=t/maillnodes.ndmat;
	}
    }

  /*clipping chris  tres provisoire de la pression de vapeur */
  for (i=0;i<maillnodes.nelem;i++)
    {
      t_aux=var_ele[0][i]+tkel;
      pv_aux=var_ele[1][i];
      psat=fphyhmt_fpsat(t_aux);
      if (psat<pv_aux) 
	{
	  printf("Clip_e : ele_i=%d T=%15.7e pv=%15.7e psat=%15.7e \n",i,t_aux-tkel,pv_aux,psat);
	  var_ele[1][i]=psat*0.999;
	}
    }

  

  /* non actif en version actuelle */
  user_hmt_rescon(maillnodes, maillnodeus,
		  variable.var[variable.adr_t_m1],variable.var[variable.adr_pv_m1],variable.var[variable.adr_pt_m1],
		  trav.tab[0],trav.tab[1],trav.tab[2],
		  rescon,pasdetemps,sdparall);
  

  if (syrglob_nparts==1 ||syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR)        printf("\n ---RESOLUTION DE LA TEMPERATURE---\n");
      else if (SYRTHES_LANG == EN)   printf("\n ---TEMPERATURE SOLVER---\n");
      
    }
  hmt_diffus_t(*pasdetemps,
	       maillnodes,maillnodeus,maillnodebord,
	       variable,var_ele,rescon,scoupr,rayinf,scoupf,scouvf,
	       hmtclimhhh,
	       fluxvol,
	       perio,humid,constphyhmt,constmateriaux,mst,matr,trav,trave,
	       sdparall);

  if (syrglob_nparts==1 ||syrglob_rang==0)
    {
      if (SYRTHES_LANG == FR)        printf("\n ---RESOLUTION DE PV---\n");
      else if (SYRTHES_LANG == EN)   printf("\n ---PV SOLVER---\n");
      
    }
  hmt_diffus_pv(*pasdetemps,
		maillnodes,maillnodeus,maillnodebord,
		variable,var_ele,rescon,scoupr,rayinf,scoupf,scouvf,  
		hmtclimhhh,
		fluxvol,
		perio,humid,constphyhmt,constmateriaux,mst,matr,trav,trave,
		sdparall);


  /* A resoudre uniquement si modele a 3 equations */
  if (nbVar==3)
    {
      if (syrglob_nparts==1 ||syrglob_rang==0)
	{
	  if (SYRTHES_LANG == FR)        printf("\n ---RESOLUTION DE PT---\n");
	  else if (SYRTHES_LANG == EN)   printf("\n ---PT SOLVER---\n");
	  
	}
      hmt_diffus_pt(*pasdetemps,
		    maillnodes,maillnodeus,maillnodebord,
		    variable,var_ele,rescon,scoupr,rayinf,scoupf,scouvf,
		    hmtclimhhh,
		    fluxvol,
		    perio,humid,constphyhmt,constmateriaux,mst,matr,trav,trave,
		    sdparall);
    }

  /*clipping chris  tres provisoire de la pression de vapeur */
  for (i=0;i<maillnodes.npoin;i++)
    {
      t_aux=variable.var[variable.adr_t][i]+tkel;
      pv_aux=variable.var[variable.adr_pv][i];
      psat=fphyhmt_fpsat(t_aux);
      if (psat<pv_aux) 
	{
	  printf("Clip_n : i=%d T=%15.7e pv=%15.7e psat=%15.7e \n",i,t_aux-tkel,pv_aux,psat);
	  variable.var[variable.adr_pv][i]=psat*0.999;
	}
    }

}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Resolution de la diffusion                                    |
  |======================================================================| */

void hmt_diffus_t(struct PasDeTemps pasdetemps,
		struct Maillage maillnodes,
		struct MaillageCL maillnodeus,struct MaillageBord maillnodebord,
		struct Variable variable,double **var_ele,
		struct Contact rescon,struct Couple scoupr,struct Clim rayinf,
		struct Climcfd scoupf,struct Climcfd scouvf,
		struct HmtClimhhh hmtclimhhh,
		struct Cvol *fluxvol,
		struct Cperio perio,struct Humid humid,
		struct ConstPhyhmt constphyhmt,struct ConstMateriaux *constmateriaux,
		struct Mst mst,struct Matrice matr,struct Travail trav,struct Travail trave,
		struct SDparall sdparall )
{
  int i,j,nn;
  int numvar; /* pour le moment en interne. boucle sur diffu ? */
  double rindts,*trav0,*trav1,*trav2,*trav3,*trav5,*coeff;
  int lseg=1;
  static int prem=1;

  int ibug1,ibug2;

  trav0=trav.tab[0];
  trav1=trav.tab[1];
  trav2=trav.tab[2];
  trav3=trav.tab[3];
  trav5=trav.tab[5];
  
  coeff=trave.tab[0];

  /* ============================ */
  /* Resolution de la temperature */
  /* ============================ */

  numvar=ADR_T;


  /* ************************************************************/
  /* Calcul du second membre explicite                          */
  /* c'est fait en premier pour utiliser provisoirement         */
  /* la matrice matr avant le calcul de matr pour la resolution */
  /* ************************************************************/

  azero1D(maillnodes.npoin,matr.b);
  azero1D(maillnodes.npoin,trav5);

  /* prise en compte (explicite) du terme : div(COEFF*grad(PV)) */
  /* ---------------------------------------------------------- */
  azero1D(maillnodes.npoin,matr.dmat);
  azero1D(maillnodes.nbarete,matr.xdms);
  if (nbVar==3) /* modele a trois equations */
    hmt_coeffequa(numvar,3,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  else if (nbVar==2) /* modele a deux equations */
    hmt_coeffequa(numvar,30,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  hmt_madif(maillnodes,coeff,matr.dmat,matr.xdms);
  if (sdparall.nparts>1) assemb_complt(matr.dmat,maillnodes.npoin,trav0,sdparall);

  if (sdparall.nparts>1)
    omv_parall(maillnodes,matr.b,variable.var[variable.adr_pv_m1],matr,perio,trav5,sdparall);
  else
    omv(maillnodes,matr.b,variable.var[variable.adr_pv_m1],matr,perio,trav5); 

  if (affich.cond_mat) 
	{
	  if (sdparall.nparts > 1)
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_a rang=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_a matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
  
/*   if (ecraddfile) add_var_in_file(maillnodes.npoin,trav5,"toto",3); */

/*   ibug1=0; */
/*     printf(" div(COEFF*grad(PV)) : T : trav1[%d]=%25.18e\n",ibug1,matr.b[ibug1]); */
/*   ibug2=1; */
/*     printf(" div(COEFF*grad(PV)) : T : trav1[%d]=%25.18e\n",ibug2,matr.b[ibug2]); */


  /* prise en compte (explicite) du terme : div(COEFF*grad(PT)) */
  /* ---------------------------------------------------------- */
  if (nbVar==3) /*uniquement modele a trois equations */
    {
      azero1D(maillnodes.npoin,matr.dmat);
      azero1D(maillnodes.nbarete,matr.xdms);
      hmt_coeffequa(numvar,4,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
      hmt_madif(maillnodes,coeff,matr.dmat,matr.xdms);
      if (sdparall.nparts>1) assemb_complt(matr.dmat,maillnodes.npoin,trav0,sdparall);
      
      if (sdparall.nparts>1)
	omv_parall(maillnodes,matr.b,variable.var[variable.adr_pt_m1],matr,perio,trav5,sdparall);
      else
	omv(maillnodes,matr.b,variable.var[variable.adr_pt_m1],matr,perio,trav5);
      
      if (affich.cond_mat) 
	{
	  if (sdparall.nparts > 1)
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_b rang=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_b matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
    }
  
  /* prise en compte (explicite) du terme :  dpv/dt */
  /* ---------------------------------------------- */
  azero1D(maillnodes.npoin,trav1);
  if (nbVar==3) /* modele a trois equations */
    hmt_coeffequa(numvar,2,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  else if (nbVar==2) /* modele a deux equations */
    hmt_coeffequa(numvar,20,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);

  mamass(pasdetemps,maillnodes,coeff,trav1);
  if (sdparall.nparts>1) assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);

  for (i=0;i<maillnodes.npoin;i++) 
    *(matr.b+i)+=*(trav1+i) * (variable.var[variable.adr_pv_m1][i]-variable.var[variable.adr_pv_m2][i])
                            * pasdetemps.rdtts/pasdetemps.rdttsprec;

      if (affich.cond_mat) 
	{
	  
	  if (sdparall.nparts > 1)
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_c rang=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_c matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
  
  
  /* prise en compte (explicite) du terme :  dpt/dt */
  /* ---------------------------------------------- */
  if (nbVar==3) /*uniquement modele a trois equations */
    {
      azero1D(maillnodes.npoin,trav1);
      hmt_coeffequa(numvar,5,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
      mamass(pasdetemps,maillnodes,coeff,trav1);
      if (sdparall.nparts>1) assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
      
      for (i=0;i<maillnodes.npoin;i++) 
	*(matr.b+i)+=*(trav1+i) * (variable.var[variable.adr_pt_m1][i]-variable.var[variable.adr_pt_m2][i])
	  * pasdetemps.rdtts/pasdetemps.rdttsprec;
      
      if (affich.cond_mat) 
	{
	  
	  if (sdparall.nparts > 1)
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_d rang=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_d matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
    }

  

  /* ************************************************************/
  /* Calcul des matrices pour la resolution                     */
  /* ************************************************************/

  azero1D(maillnodes.npoin,matr.dmat);
  azero1D(maillnodes.nbarete,matr.xdms);

  /* calcul de la matrice de masse */
  /* ----------------------------- */
  azero1D(maillnodes.npoin,trav1);
  if (nbVar==3) /* modele a trois equations */
    hmt_coeffequa(numvar,0,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  else if (nbVar==2)/* modele a deux equations */
    hmt_coeffequa(numvar,10,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
    

  mamass(pasdetemps,maillnodes,coeff,trav1);
  if (sdparall.nparts>1) assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
  
  /* ----> ici trav1 contient la matrice de masse finale */
  if (affich.cond_mat) 
    {
      if (sdparall.nparts > 1)
	for (i=0;i<maillnodes.npoin;i++) 
	  printf("mass matrix : rank=%d nglob=%d trav1[%d]=%25.18e\n",
		 sdparall.rang,sdparall.npglob[i],i,trav1[i]);
      else
	for (i=0;i<maillnodes.npoin;i++) printf("mass matrix : matr.dmat[%d]=%25.18e\n",i,trav1[i]);
    }

  /* matrice de diffusion  */
  /* --------------------- */
  azero1D(maillnodes.npoin,trav2);
  hmt_coeffequa(numvar,1,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  hmt_madif(maillnodes,coeff,trav2,matr.xdms);
  if (sdparall.nparts>1) assemb_complt(trav2,maillnodes.npoin,trav0,sdparall);
  
  /* ----> ici trav2 contient la diagonale de la matrice de diffusion */
  if (affich.cond_mat) 
    {
      if (sdparall.nparts > 1)
	for (i=0;i<maillnodes.npoin;i++) 
	  printf("rank=%d, nglob=%d trav2[%d]=%25.18e\n",
		 sdparall.rang,sdparall.npglob[i],i,trav2[i]);
      else
	for (i=0;i<maillnodes.npoin;i++) 
	  printf("table trav2[%d]=%25.18e\n",i,trav2[i]);
    }

  if (prem) {prem=0; free(matr.ddiff);}


  /* contribution des CL implicites */
  /* ------------------------------ */
  azero1D(maillnodes.npoin,trav3);
  if (maillnodeus.nelem)
    {
      hmt_mafcli_t(hmtclimhhh,scoupr,rayinf,scoupf,maillnodes,maillnodeus,
		   variable.var[variable.adr_t_m1],trav3,trav0);
      if (sdparall.nparts>1)      
	assemb_complt(trav3,maillnodes.npoin,trav0,sdparall);


      if (affich.cond_mat) 
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) 
	      printf("after mafcli T : rank=%d nglob=%d trav3[%d]=%25.18e\n",
		     sdparall.rang,sdparall.npglob[i],i,trav3[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) 
	      printf("after mafcli T : trav3[%d]=%25.18e\n",i,trav3[i]);
	}
      
    }
  else if (sdparall.nparts>1) 
    assemb_complt(trav3,maillnodes.npoin,trav0,sdparall);


  /* diagonale de la matrice (masse+diff+CL) */
  /* --------------------------------------- */
  for (i=0;i<maillnodes.npoin;i++) *(matr.dmat+i)=*(trav1+i)+*(trav2+i)+*(trav3+i);

  if (affich.cond_mat) 
    {
      if (SYRTHES_LANG == FR)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("rang=%d nglob=%d matr.dmat[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.dmat[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("matr.dmat[%d]=%25.18e\n",i,matr.dmat[i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("rank=%d nglob=%d matr.dmat[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.dmat[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("matr.dmat[%d]=%25.18e\n",i,matr.dmat[i]);
	}
    }

  /* Prise en compte de la partie implicite (si elle existe) des flux volumiques thermiques
       - on a alors mis le flux volumique 
       sous la forme A*(B-T) : A et B calcule a partir de a et b (donnes dans .syd et l'interface
     Rq: si A=0 alors on est purement en explicite */
  /* ------------------------------------------------------------ */
  if (fluxvol[ADR_T].existglob)
    {
      azero1D(maillnodes.npoin,trav2);
      if (fluxvol[ADR_T].nelem)
	smfvos_impl(maillnodes,fluxvol[ADR_T],trav2); 
      else if (sdparall.nparts>1)
	assemb_complt(trav2,maillnodes.npoin,trav0,sdparall);
      
      for (i=0;i<maillnodes.npoin;i++) *(matr.dmat+i)+=*(trav2+i);
    }



  /* contribution des Resistances de contact */
  /* --------------------------------------- */
  if (rescon.existglob)
    {
      if (rescon.npoin)
	{
	  for (i=0;i<maillnodes.npoin;i++) *(trav5+i)=0;
	  hmt_mafclc_t(rescon,maillnodes,maillnodeus,variable.var[variable.adr_t_m1],trav5,trav0);
	  
	  if (sdparall.nparts>1) 
	    for (j=nn=0;nn<sdparall.nparts;nn++)
	      for (i=0;i<sdparall.nbcommunrc[nn];i++)
		{rescon.gnonassc[j]=trav5[sdparall.tcommunrc[sdparall.adrcommunrc[nn]+2*i]]; j++;}
	  else
	    for (i=0;i<rescon.npoin;i++)
	      rescon.gnonassc[i]=rescon.gassc[i]=trav5[rescon.nump[i]];
	  
	  /* on fait maintenant l'assemblage complementaire */
 	  if (sdparall.nparts>1)       
	    {
	      assemb_complt(trav5,maillnodes.npoin,trav0,sdparall); 
	      for (j=nn=0;nn<sdparall.nparts;nn++)
		for (i=0;i<sdparall.nbcommunrc[nn];i++)
		  {rescon.gassc[j]=trav5[sdparall.tcommunrc[sdparall.adrcommunrc[nn]+2*i]]; j++;}
	    }
	  if (affich.cond_mat) 
	    {
	      if (SYRTHES_LANG == FR)
		{
		  if (sdparall.nparts > 1)	
		    for (i=0;i<maillnodes.npoin;i++) printf("apres mafclc T : rang=%d nglob=%d trav5[%d]=%25.18e\n",
							    sdparall.rang,sdparall.npglob[i],i,trav5[i]);
		  else
		    for (i=0;i<maillnodes.npoin;i++) printf("apres mafclc T : trav5[%d]=%25.18e\n",i,trav5[i]);
		}
	      else if (SYRTHES_LANG == EN)
		{
		  if (sdparall.nparts > 1)	
		    for (i=0;i<maillnodes.npoin;i++) printf("after mafclc T : rank=%d nglob=%d trav5[%d]=%25.18e\n",
							    sdparall.rang,sdparall.npglob[i],i,trav5[i]);
		  else
		    for (i=0;i<maillnodes.npoin;i++) printf("after mafclc T : trav5[%d]=%25.18e\n",i,trav5[i]);
		}
	    }
	}
      else if (sdparall.nparts>1) 
	assemb_complt(trav5,maillnodes.npoin,trav0,sdparall); /* erreur_chris : ici je ne comprends pas bien voir isa*/
      
    }

  /* second membre */
  /* ------------- */
  for (i=0;i<maillnodes.npoin;i++) *(matr.b+i) += *(trav1+i) * variable.var[variable.adr_t_m1][i];

  if (affich.cond_mat) 
    {
      if (SYRTHES_LANG == FR)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_1 rang=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_1 matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_1 rank=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_1 matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
    }


  /* prise en compte de la partie explicite des flux volumique de temperature */
  /* ------------------------------------------------------------- */
  if (fluxvol[ADR_T].nelem)
    {
      azero1D(maillnodes.npoin,trav1);
      smfvos_expl (maillnodes,fluxvol[ADR_T],trav1);
      if (sdparall.nparts>1)      
	assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
      for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)+=*(trav1+i);

      if (affich.cond_mat) 
	if (SYRTHES_LANG == FR)
	  for (i=0;i<maillnodes.npoin;i++) printf("apres fluxvol : matr.b[%d]=%f\n",i,matr.b[i]);
	else if (SYRTHES_LANG == EN)
	  for (i=0;i<maillnodes.npoin;i++) printf("after fluxvol : matr.b[%d]=%f\n",i,matr.b[i]);
    }

  if (lmst)
    {/* ????????????????? smfvol pas mis a jour - a voir par chris ulterieurement*/
/*       smfvol(maillnodes,mst.nelem,mst.nume,mst.divflux,matr.b,matr.wct); */
/*       azero1D(maillnodes.npoin,trav1); */
/*       assemb(maillnodes,trav1,matr.wct); */
/*       for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)+=*(trav1+i); */
    }

  /* traitement ulterieur de la periodicite */
/*   if (perio.existglob) */
/*     if (sdparall.nparts==1)  */
/*       { */
/* 	period(maillnodes,matr.dmat,trav1,perio); */
/* 	period(maillnodes,matr.b,trav1,perio); */
/*       } */
/*     else */
/*       { */
/* 	period_parall(maillnodes,matr.dmat,trav1,perio,sdparall); */
/* 	period_parall(maillnodes,matr.b,trav1,perio,sdparall); */
/*       } */


  
  /* *******************************************************   */
  /* Traitement des conditions aux limites (partie explicite)  */
  /* *******************************************************   */

  if (maillnodeus.nelem)
    {
 
      azero1D(maillnodes.npoin,trav1);
      for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}

      /* partie explicite des CL temperature (temperature et flux de vapeur) */
      /* ------------------------------------------------------------------- */
      hmt_smexp_t(hmtclimhhh,maillnodes,maillnodeus,variable,constphyhmt,
		  trav0);
      
      /* assemblage de la matrice sur le bord */
      matbord(maillnodes,maillnodeus,trav0,trav1);
      if (sdparall.nparts>1)      
	assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
      
      for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)+=*(trav1+i);


      if (affich.cond_mat){
	if (SYRTHES_LANG == FR){
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_2 rang=%d nglob=%d trav1[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,trav1[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_2 trav1[%d]=%25.18e\n",i,trav1[i]);
	}
	else if (SYRTHES_LANG == EN){
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_2 rank=%d nglob=%d trav1[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,trav1[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_2 trav1[%d]=%25.18e\n",i,trav1[i]);
	}
      }
      

    }
  else if (sdparall.nparts>1) /* pas de cond de flux mais du parallelisme : il faut assurer les communications */
    {
      azero1D(maillnodes.npoin,trav1);
      assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
    }


  if (sdparall.nparts==1)           
    grconj(maillnodes,variable.var[variable.adr_t],rescon,perio,trav,matr,epsgcs_t,nitmx_t);
  else
    grconj_parall(maillnodes,variable.var[variable.adr_t],rescon,perio,trav,matr,epsgcs_t,nitmx_t,sdparall);


  /* if (sdparall.nparts > 1) */
  /*   for (i=0;i<maillnodes.npoin;i++) printf("Temperature rang=%d nglob=%d tmps[%d]=%25.18e\n", */
  /* 					    sdparall.rang,sdparall.npglob[i],i,variable.var[variable.adr_t][i]); */
  /* else */
  /*   for (i=0;i<maillnodes.npoin;i++) printf("Temperature tmps[%d]=%25.18e\n",i,variable.var[variable.adr_t][i]); */
  

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Resolution de la diffusion                                    |
  |======================================================================| */

void hmt_diffus_pv(struct PasDeTemps pasdetemps,
		struct Maillage maillnodes,
		struct MaillageCL maillnodeus,struct MaillageBord maillnodebord,
		struct Variable variable,double **var_ele,
		struct Contact rescon,struct Couple scoupr,struct Clim rayinf,
		struct Climcfd scoupf,struct Climcfd scouvf,
		struct HmtClimhhh hmtclimhhh,
		struct Cvol *fluxvol,
		struct Cperio perio,struct Humid humid,
		struct ConstPhyhmt constphyhmt,struct ConstMateriaux *constmateriaux,
		struct Mst mst,struct Matrice matr,struct Travail trav,struct Travail trave,
		struct SDparall sdparall )
{
  int i,j,nn;
  int numvar; /* pour le moment en interne. boucle sur diffu ? */
  double rindts,*trav0,*trav1,*trav2,*trav3,*trav5,*coeff;
  int lseg=1;
  static int prem=1;

  trav0=trav.tab[0];
  trav1=trav.tab[1];
  trav2=trav.tab[2];
  trav3=trav.tab[3];
  trav5=trav.tab[5];
  
  coeff=trave.tab[0];

  /* =================================== */
  /* Resolution de la pression de vapeur */
  /* =================================== */

  numvar=ADR_PV;

  /* ************************************************************/
  /* Calcul du second memebre explicite                         */
  /* c'est fait en premier pour utiliser provisoirement         */
  /* la matrice matr avant le calcul de matr pour la resolution */
  /* ************************************************************/

  azero1D(maillnodes.npoin,matr.b);
  azero1D(maillnodes.npoin,trav5);


  /* prise en compte (explicite) du terme : div(COEFF*grad(T)) */
  /* ---------------------------------------------------------- */
  azero1D(maillnodes.npoin,matr.dmat);
  azero1D(maillnodes.nbarete,matr.xdms);
  hmt_coeffequa(numvar,3,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  hmt_madif(maillnodes,coeff,matr.dmat,matr.xdms);
  if (sdparall.nparts>1) assemb_complt(matr.dmat,maillnodes.npoin,trav0,sdparall);

  if (sdparall.nparts>1)
    omv_parall(maillnodes,matr.b,variable.var[variable.adr_t_m1],matr,perio,trav5,sdparall);
  else
    omv(maillnodes,matr.b,variable.var[variable.adr_t_m1],matr,perio,trav5);

 if (affich.cond_mat) 
	{
	  if (sdparall.nparts > 1)
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_PV_a rang=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_PV_a matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
  
  /* prise en compte (explicite) du terme : div(COEFF*grad(PT)) */
  /* ---------------------------------------------------------- */
  if (nbVar==3) /*uniquement modele a trois equations */
    {
      azero1D(maillnodes.npoin,matr.dmat);
      azero1D(maillnodes.nbarete,matr.xdms);
      hmt_coeffequa(numvar,4,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
      hmt_madif(maillnodes,coeff,matr.dmat,matr.xdms);
      if (sdparall.nparts>1) assemb_complt(matr.dmat,maillnodes.npoin,trav0,sdparall);
      
      if (sdparall.nparts>1)
	omv_parall(maillnodes,matr.b,variable.var[variable.adr_pt_m1],matr,perio,trav5,sdparall);
      else
	omv(maillnodes,matr.b,variable.var[variable.adr_pt_m1],matr,perio,trav5);
      if (affich.cond_mat) 
	{
	  if (sdparall.nparts > 1)
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_PV_b rang=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_PV_b matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
      

    }
  
  /* prise en compte (explicite) du terme :  dT/dt */
  /* ---------------------------------------------- */
  azero1D(maillnodes.npoin,trav1);
  hmt_coeffequa(numvar,2,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  mamass(pasdetemps,maillnodes,coeff,trav1);
  if (sdparall.nparts>1) assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);

  for (i=0;i<maillnodes.npoin;i++) 
    *(matr.b+i)+=*(trav1+i)* (variable.var[variable.adr_t_m1][i]-variable.var[variable.adr_t_m2][i])
      * pasdetemps.rdtts/pasdetemps.rdttsprec;

  if (affich.cond_mat) 
    {
      if (sdparall.nparts > 1)
	for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_PV_c rang=%d nglob=%d matr.b[%d]=%25.18e\n",
						sdparall.rang,sdparall.npglob[i],i,matr.b[i]);
      else
	for (i=0;i<maillnodes.npoin;i++) printf("hmt_resol_PV_c matr.b[%d]=%25.18e\n",i,matr.b[i]);
    }



  /* ************************************************************/
  /* Calcul des matrices pour la resolution                     */
  /* ************************************************************/

  azero1D(maillnodes.npoin,matr.dmat);

  /* chris :pas sur qu'il y ait besoin de le faire la, on le fait avant la diffusion */
  /* azero1D(maillnodes.nbarete,matr.xdms); */

  /* calcul de la matrice de masse */
  /* ----------------------------- */
  azero1D(maillnodes.npoin,trav1);
  hmt_coeffequa(numvar,0,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  mamass(pasdetemps,maillnodes,coeff,trav1);
  if (sdparall.nparts>1) assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
  
  /* ----> ici trav1 contient la matrice de masse finale */
  if (affich.cond_mat) 
    {
      if (sdparall.nparts > 1)
	for (i=0;i<maillnodes.npoin;i++) 
	  printf("mass matrix pv : rank=%d nglob=%d trav1[%d]=%25.18e\n",
		 sdparall.rang,sdparall.npglob[i],i,trav1[i]);
      else
	for (i=0;i<maillnodes.npoin;i++) printf("mass matrix pv : trav1[%d]=%25.18e\n",i,trav1[i]);
    }


  /* matrice de diffusion  */
  /* --------------------- */
  azero1D(maillnodes.npoin,trav2);
  azero1D(maillnodes.nbarete,matr.xdms);
  if (nbVar==3) /* modele a trois equations */
    hmt_coeffequa(numvar,1,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  else if (nbVar==2) /* modele a deux equations */
    hmt_coeffequa(numvar,11,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);

  hmt_madif(maillnodes,coeff,trav2,matr.xdms);
  if (sdparall.nparts>1) assemb_complt(trav2,maillnodes.npoin,trav0,sdparall);
  
  /* ----> ici trav2 contient la diagonale de la matrice de diffusion */
  if (affich.cond_mat) 
    {
      if (sdparall.nparts > 1)
	for (i=0;i<maillnodes.npoin;i++) 
	  printf("pv rank=%d, nglob=%d trav2[%d]=%25.18e\n",
		 sdparall.rang,sdparall.npglob[i],i,trav2[i]);
      else
	for (i=0;i<maillnodes.npoin;i++) 
	  printf("pv diff_diag trav2[%d]=%25.18e\n",i,trav2[i]);
    }
  

  
  /* contribution des CL implicites */
  /* ------------------------------ */
  azero1D(maillnodes.npoin,trav3);
  if (maillnodeus.nelem)
    {
      hmt_mafcli_pv(hmtclimhhh,maillnodes,maillnodeus,
		    variable,constphyhmt,trav3,trav0);
      if (sdparall.nparts>1)      
	assemb_complt(trav3,maillnodes.npoin,trav0,sdparall);


      if (affich.cond_mat) 
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) 
	      printf("after mafcli_pv : rank=%d nglob=%d trav3[%d]=%25.18e\n",
		     sdparall.rang,sdparall.npglob[i],i,trav3[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) 
	      printf("after mafcli_pv : trav3[%d]=%25.18e\n",i,trav3[i]);
	}
      
    }
  else if (sdparall.nparts>1) 
    assemb_complt(trav3,maillnodes.npoin,trav0,sdparall);


  /* diagonale de la matrice (masse+diff+CL) */
  /* --------------------------------------- */
  for (i=0;i<maillnodes.npoin;i++) *(matr.dmat+i)=*(trav1+i)+*(trav2+i)+*(trav3+i);

  if (affich.cond_mat) 
    {
      if (SYRTHES_LANG == FR)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("pv rang=%d nglob=%d matr.dmat[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.dmat[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("pv matr.dmat[%d]=%25.18e\n",i,matr.dmat[i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("pv rank=%d nglob=%d matr.dmat[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.dmat[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("pv matr.dmat[%d]=%25.18e\n",i,matr.dmat[i]);
	}
    }


  /* Prise en compte de la partie implicite (si elle existe) des flux volumiques pv
       - on a alors mis le flux volumique 
       sous la forme A*(B-Pv) : A et B calcule a partir de a et b (donnes dans .syd et l'interface
     Rq: si A=0 alors on est purement en explicite */
  /* ------------------------------------------------------------ */
  if (fluxvol[ADR_PV].existglob)
    {
      azero1D(maillnodes.npoin,trav2);
      if (fluxvol[ADR_PV].nelem)
	smfvos_impl(maillnodes,fluxvol[ADR_PV],trav2); 
      else if (sdparall.nparts>1)
	assemb_complt(trav2,maillnodes.npoin,trav0,sdparall);
      
      for (i=0;i<maillnodes.npoin;i++) *(matr.dmat+i)+=*(trav2+i);


  if (affich.cond_mat) 
    {
      if (SYRTHES_LANG == FR)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("pv rang=%d nglob=%d trav2[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,trav2[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("pv trav2[%d]=%25.18e\n",i,trav2[i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("pv rank=%d nglob=%d trav2matr.dmat[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,trav2[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("pv trav2[%d]=%25.18e\n",i,trav2[i]);
	}
    }



    }


  /* contribution des Resistances de contact pour du pv (non implante actuellement et peut etre jamais)*/
  /* ------------------------------------------------------------------ */
  if (rescon.existglob)
    {
      if (rescon.npoin)
	{
	  for (i=0;i<maillnodes.npoin;i++) *(trav5+i)=0;
	  hmt_mafclc_pv(rescon,maillnodes,maillnodeus,variable.var[variable.adr_pv_m1],trav5,trav0);
	  
	  if (sdparall.nparts>1) 
	    for (j=nn=0;nn<sdparall.nparts;nn++)
	      for (i=0;i<sdparall.nbcommunrc[nn];i++)
		{rescon.gnonassc[j]=trav5[sdparall.tcommunrc[sdparall.adrcommunrc[nn]+2*i]]; j++;}
	  else
	    for (i=0;i<rescon.npoin;i++)
	      rescon.gnonassc[i]=rescon.gassc[i]=trav5[rescon.nump[i]];
	  
	  /* on fait maintenant l'assemblage complementaire */
	  if (sdparall.nparts>1)       
	    {
	      assemb_complt(trav5,maillnodes.npoin,trav0,sdparall); 
	      for (j=nn=0;nn<sdparall.nparts;nn++)
		for (i=0;i<sdparall.nbcommunrc[nn];i++)
		  {rescon.gassc[j]=trav5[sdparall.tcommunrc[sdparall.adrcommunrc[nn]+2*i]]; j++;}
	    }
	  if (affich.cond_mat) 
	    {
	      if (SYRTHES_LANG == FR)
		{
		  if (sdparall.nparts > 1)	
		    for (i=0;i<maillnodes.npoin;i++) printf("apres mafclc pv: rang=%d nglob=%d trav5[%d]=%25.18e\n",
							    sdparall.rang,sdparall.npglob[i],i,trav5[i]);
		  else
		    for (i=0;i<maillnodes.npoin;i++) printf("apres mafclc pv: trav5[%d]=%25.18e\n",i,trav5[i]);
		}
	      else if (SYRTHES_LANG == EN)
		{
		  if (sdparall.nparts > 1)	
		    for (i=0;i<maillnodes.npoin;i++) printf("after mafclc pv: rank=%d nglob=%d trav5[%d]=%25.18e\n",
							    sdparall.rang,sdparall.npglob[i],i,trav5[i]);
		  else
		    for (i=0;i<maillnodes.npoin;i++) printf("after mafclc pv: trav5[%d]=%25.18e\n",i,trav5[i]);
		}
	    }
	}
      else if (sdparall.nparts>1) 
	assemb_complt(trav5,maillnodes.npoin,trav0,sdparall); /* erreur_chris : ici je ne comprends pas bien voir isa*/    
    }
  
  /* second membre */
  /* ------------- */
  for (i=0;i<maillnodes.npoin;i++) *(matr.b+i) += *(trav1+i) * variable.var[variable.adr_pv_m1][i];

  if (affich.cond_mat) 
    {
      if (SYRTHES_LANG == FR)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("rang=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i]-1,i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("rank=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i]-1,i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
    }


  /* prise en compte de la partie explicite des flux volumique de Pv */
  /* ------------------------------------------------------------- */
  if (fluxvol[ADR_PV].nelem)
    {
      azero1D(maillnodes.npoin,trav1);
      smfvos_expl (maillnodes,fluxvol[ADR_PV],trav1);
      if (sdparall.nparts>1)      
	assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
      for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)+=*(trav1+i);

      if (affich.cond_mat) 
	if (SYRTHES_LANG == FR)
	  for (i=0;i<maillnodes.npoin;i++) printf("apres fluxvol : matr.b[%d]=%f\n",i,matr.b[i]);
	else if (SYRTHES_LANG == EN)
	  for (i=0;i<maillnodes.npoin;i++) printf("after fluxvol : matr.b[%d]=%f\n",i,matr.b[i]);
    }


  
  /* *******************************************************   */
  /* Traitement des conditions aux limites (partie explicite)  */
  /* *******************************************************   */

  if (maillnodeus.nelem)
    {
 
      azero1D(maillnodes.npoin,trav1);
      for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}

      /* partie explicite des CL sur PV */
      /* ------------------------------ */
      hmt_smexp_pv(hmtclimhhh,maillnodes,maillnodeus,variable,constphyhmt,
		   trav0);
      
      /* assemblage de la matrice sur le bord */
      matbord(maillnodes,maillnodeus,trav0,trav1);



      if (affich.cond_mat) 
	{
	  if (SYRTHES_LANG == FR)
	    {
	      if (sdparall.nparts > 1)	
		for (i=0;i<maillnodes.npoin;i++) printf("rang=%d nglob=%d trav1[%d]=%25.18e\n",
							sdparall.rang,sdparall.npglob[i],i,trav1[i]);
	      else
		for (i=0;i<maillnodes.npoin;i++) printf("trav1[%d]=%25.18e\n",i,trav1[i]);
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      if (sdparall.nparts > 1)	
		for (i=0;i<maillnodes.npoin;i++) printf("rank=%d nglob=%d trav1[%d]=%25.18e\n",
							sdparall.rang,sdparall.npglob[i],i,trav1[i]);
	      else
		for (i=0;i<maillnodes.npoin;i++) printf("trav1[%d]=%25.18e\n",i,trav1[i]);
	    }
	}

      if (sdparall.nparts>1)      
	assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);


      if (affich.cond_mat) 
	{
	  if (SYRTHES_LANG == FR)
	    {
	      if (sdparall.nparts > 1)	
		for (i=0;i<maillnodes.npoin;i++) printf("rang=%d nglob=%d trav1[%d]=%25.18e\n",
							sdparall.rang,sdparall.npglob[i],i,trav1[i]);
	      else
		for (i=0;i<maillnodes.npoin;i++) printf("trav1[%d]=%25.18e\n",i,trav1[i]);
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      if (sdparall.nparts > 1)	
		for (i=0;i<maillnodes.npoin;i++) printf("rank=%d nglob=%d trav1[%d]=%25.18e\n",
							sdparall.rang,sdparall.npglob[i],i,trav1[i]);
	      else
		for (i=0;i<maillnodes.npoin;i++) printf("trav1[%d]=%25.18e\n",i,trav1[i]);
	    }
	}

      /* ajout au second membre */
      /* for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)+=*(trav1+i); */
      for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)+=*(trav1+i);

    }
  else if (sdparall.nparts>1) /* pas de cond de flux mais du parallelisme : il faut assurer les communications */
    {
      azero1D(maillnodes.npoin,trav1);
      assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
    }




  /* if (sdparall.nparts > 1) */
  /*   for (i=0;i<maillnodes.npoin;i++) printf("Avant solveur PV : rang=%d nglob=%d PV[%d]=%25.18e\n", */
  /* 					    sdparall.rang,sdparall.npglob[i],i,variable.var[variable.adr_pv][i]); */
  /* else */
  /*   for (i=0;i<maillnodes.npoin;i++) printf("Avant solveur PV : PV[%d]=%25.18e\n",i,variable.var[variable.adr_pv][i]); */


  if (sdparall.nparts==1)           
    grconj(maillnodes,variable.var[variable.adr_pv],rescon,perio,trav,matr,epsgcs_pv,nitmx_pv);
  else
    grconj_parall(maillnodes,variable.var[variable.adr_pv],rescon,perio,trav,matr,epsgcs_pv,nitmx_pv,sdparall);


  /* if (sdparall.nparts > 1) */
  /*   for (i=0;i<maillnodes.npoin;i++) printf("Apres solveur PV : rang=%d nglob=%d PV[%d]=%25.18e\n", */
  /* 					    sdparall.rang,sdparall.npglob[i],i,variable.var[variable.adr_pv][i]); */
  /* else */
  /*   for (i=0;i<maillnodes.npoin;i++) printf("Apres solveur PV : PV[%d]=%25.18e\n",i,variable.var[variable.adr_pv][i]); */
  

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Resolution de la diffusion                                    |
  |======================================================================| */

void hmt_diffus_pt(struct PasDeTemps pasdetemps,
		   struct Maillage maillnodes,
		   struct MaillageCL maillnodeus,struct MaillageBord maillnodebord,
		   struct Variable variable,double **var_ele,
		   struct Contact rescon,struct Couple scoupr,struct Clim rayinf,
		   struct Climcfd scoupf,struct Climcfd scouvf,
		   struct HmtClimhhh hmtclimhhh,
		   struct Cvol *fluxvol,
		   struct Cperio perio,struct Humid humid,
		   struct ConstPhyhmt constphyhmt,struct ConstMateriaux *constmateriaux,
		   struct Mst mst,struct Matrice matr,struct Travail trav,struct Travail trave,
		   struct SDparall sdparall )
{
  int i,j,nn;
  int numvar; /* pour le moment en interne. boucle sur diffu ? */
  double rindts,*trav0,*trav1,*trav2,*trav3,*trav5,*coeff;
  int lseg=1;
  static int prem=1;

  trav0=trav.tab[0];
  trav1=trav.tab[1];
  trav2=trav.tab[2];
  trav3=trav.tab[3];
  trav5=trav.tab[5];
  
  coeff=trave.tab[0];

  /* =================================== */
  /* Resolution de la pression totale    */
  /* =================================== */

  numvar=ADR_PT;


  /* ************************************************************/
  /* Calcul du second memebre explicite                         */
  /* c'est fait en premier pour utiliser provisoirement         */
  /* la matrice matr avant le calcul de matr pour la resolution */
  /* ************************************************************/

  azero1D(maillnodes.npoin,matr.b);
  azero1D(maillnodes.npoin,trav5);

  /* prise en compte (explicite) du terme : div(COEFF*grad(PV)) */
  /* ---------------------------------------------------------- */
  azero1D(maillnodes.npoin,matr.dmat);
  azero1D(maillnodes.nbarete,matr.xdms);
  hmt_coeffequa(numvar,4,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  hmt_madif(maillnodes,coeff,matr.dmat,matr.xdms);
  if (sdparall.nparts>1) assemb_complt(matr.dmat,maillnodes.npoin,trav0,sdparall);
  
  if (sdparall.nparts>1)
    omv_parall(maillnodes,matr.b,variable.var[variable.adr_pv_m1],matr,perio,trav5,sdparall);
  else
    omv(maillnodes,matr.b,variable.var[variable.adr_pv_m1],matr,perio,trav5); 

  /* prise en compte (explicite) du terme :  dT/dt */
  /* ---------------------------------------------- */
  azero1D(maillnodes.npoin,trav1);
  hmt_coeffequa(numvar,2,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  mamass(pasdetemps,maillnodes,coeff,trav1);
  if (sdparall.nparts>1) assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);

  for (i=0;i<maillnodes.npoin;i++) 
    *(matr.b+i)+=*(trav1+i)* (variable.var[variable.adr_t_m1][i]-variable.var[variable.adr_t_m2][i])
      * pasdetemps.rdtts/pasdetemps.rdttsprec;

  
  /* prise en compte (explicite) du terme :  dPV/dt */
  /* ---------------------------------------------- */
  azero1D(maillnodes.npoin,trav1);
  hmt_coeffequa(numvar,3,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  mamass(pasdetemps,maillnodes,coeff,trav1);
  if (sdparall.nparts>1) assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);

  for (i=0;i<maillnodes.npoin;i++) 
    *(matr.b+i)+=*(trav1+i)* (variable.var[variable.adr_pv_m1][i]-variable.var[variable.adr_pv_m2][i])
      * pasdetemps.rdtts/pasdetemps.rdttsprec;


  /* ************************************************************/
  /* Calcul des matrices pour la resolution                     */
  /* ************************************************************/

  azero1D(maillnodes.npoin,matr.dmat);
  azero1D(maillnodes.nbarete,matr.xdms);


  /* calcul de la matrice de masse */
  /* ----------------------------- */
  azero1D(maillnodes.npoin,trav1);
  hmt_coeffequa(numvar,0,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  mamass(pasdetemps,maillnodes,coeff,trav1);
  if (sdparall.nparts>1) assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
  
  /* ----> ici trav1 contient la matrice de masse finale */
  if (affich.cond_mat) 
    {
      if (sdparall.nparts > 1)
	for (i=0;i<maillnodes.npoin;i++) 
	  printf("mass matrix pt : rank=%d nglob=%d trav1[%d]=%25.18e\n",
		 sdparall.rang,sdparall.npglob[i],i,trav1[i]);
      else
	for (i=0;i<maillnodes.npoin;i++) printf("mass matrix pt : trav1[%d]=%25.18e\n",i,trav1[i]);
    }


  /* matrice de diffusion  */
  /* --------------------- */
  azero1D(maillnodes.npoin,trav2);
  azero1D(maillnodes.nbarete,matr.xdms);
  hmt_coeffequa(numvar,1,maillnodes,humid,var_ele,constphyhmt,constmateriaux,coeff);
  hmt_madif(maillnodes,coeff,trav2,matr.xdms);
  if (sdparall.nparts>1) assemb_complt(trav2,maillnodes.npoin,trav0,sdparall);
  
  /* ----> ici trav2 contient la diagonale de la matrice de diffusion */
  if (affich.cond_mat) 
    {
      if (sdparall.nparts > 1)
	for (i=0;i<maillnodes.npoin;i++) 
	  printf("pt rank=%d, nglob=%d trav2[%d]=%25.18e\n",
		 sdparall.rang,sdparall.npglob[i],i,trav2[i]);
      else
	for (i=0;i<maillnodes.npoin;i++) 
	  printf("pt table trav2[%d]=%25.18e\n",i,trav2[i]);
    }
  


  /* contribution des CL implicites */
  /* ------------------------------ */
  azero1D(maillnodes.npoin,trav3);
  if (maillnodeus.nelem)
    {
      hmt_mafcli_pt(hmtclimhhh,maillnodes,maillnodeus,
		    variable,constphyhmt,trav3,trav0);
      if (sdparall.nparts>1)      
	assemb_complt(trav3,maillnodes.npoin,trav0,sdparall);


      if (affich.cond_mat) 
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) 
	      printf("after mafcli_pt : rank=%d nglob=%d trav3[%d]=%25.18e\n",
		     sdparall.rang,sdparall.npglob[i],i,trav3[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) 
	      printf("after mafcli_pt : trav3[%d]=%25.18e\n",i,trav3[i]);
	}
      
    }
  else if (sdparall.nparts>1) 
    assemb_complt(trav3,maillnodes.npoin,trav0,sdparall);


  /* diagonale de la matrice (masse+diff+CL) */
  /* --------------------------------------- */
  for (i=0;i<maillnodes.npoin;i++) *(matr.dmat+i)=*(trav1+i)+*(trav2+i)+*(trav3+i);

  if (affich.cond_mat) 
    {
      if (SYRTHES_LANG == FR)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("diag pt rang=%d nglob=%d matr.dmat[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.dmat[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("diag pt matr.dmat[%d]=%25.18e\n",i,matr.dmat[i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("diag pt rank=%d nglob=%d matr.dmat[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i],i,matr.dmat[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("diag pt matr.dmat[%d]=%25.18e\n",i,matr.dmat[i]);
	}
    }


  /* Prise en compte de la partie implicite (si elle existe) des flux volumiques pv
       - on a alors mis le flux volumique 
       sous la forme A*(B-Pt) : A et B calcule a partir de a et b (donnes dans .syd et l'interface
     Rq: si A=0 alors on est purement en explicite */
  /* ------------------------------------------------------------ */
  if (fluxvol[ADR_PT].existglob)
    {
      azero1D(maillnodes.npoin,trav2);
      if (fluxvol[ADR_PT].nelem)
	smfvos_impl(maillnodes,fluxvol[ADR_PT],trav2); 
      else if (sdparall.nparts>1)
	assemb_complt(trav2,maillnodes.npoin,trav0,sdparall);
      
      for (i=0;i<maillnodes.npoin;i++) *(matr.dmat+i)+=*(trav2+i);
    }


  /* contribution des Resistances de contact pour du pt (non implante actuellement et peut etre jamais)*/
  /* ------------------------------------------------------------------ */
  if (rescon.existglob)
    {
      if (rescon.npoin)
	{
	  for (i=0;i<maillnodes.npoin;i++) *(trav5+i)=0;
	  hmt_mafclc_pt(rescon,maillnodes,maillnodeus,variable.var[variable.adr_pt_m1],trav5,trav0);
	  
	  if (sdparall.nparts>1) 
	    for (j=nn=0;nn<sdparall.nparts;nn++)
	      for (i=0;i<sdparall.nbcommunrc[nn];i++)
		{rescon.gnonassc[j]=trav5[sdparall.tcommunrc[sdparall.adrcommunrc[nn]+2*i]]; j++;}
	  else
	    for (i=0;i<rescon.npoin;i++)
	      rescon.gnonassc[i]=rescon.gassc[i]=trav5[rescon.nump[i]];
	  
	  /* on fait maintenant l'assemblage complementaire */
	  if (sdparall.nparts>1)       
	    {
	      assemb_complt(trav5,maillnodes.npoin,trav0,sdparall); 
	      for (j=nn=0;nn<sdparall.nparts;nn++)
		for (i=0;i<sdparall.nbcommunrc[nn];i++)
		  {rescon.gassc[j]=trav5[sdparall.tcommunrc[sdparall.adrcommunrc[nn]+2*i]]; j++;}
	    }
	  if (affich.cond_mat) 
	    {
	      if (SYRTHES_LANG == FR)
		{
		  if (sdparall.nparts > 1)	
		    for (i=0;i<maillnodes.npoin;i++) printf("apres mafclc pt : rang=%d nglob=%d trav5[%d]=%25.18e\n",
							    sdparall.rang,sdparall.npglob[i],i,trav5[i]);
		  else
		    for (i=0;i<maillnodes.npoin;i++) printf("apres mafclc pt: trav5[%d]=%25.18e\n",i,trav5[i]);
		}
	      else if (SYRTHES_LANG == EN)
		{
		  if (sdparall.nparts > 1)	
		    for (i=0;i<maillnodes.npoin;i++) printf("after mafclc pt: rank=%d nglob=%d trav5[%d]=%25.18e\n",
							    sdparall.rang,sdparall.npglob[i],i,trav5[i]);
		  else
		    for (i=0;i<maillnodes.npoin;i++) printf("after mafclc pt: trav5[%d]=%25.18e\n",i,trav5[i]);
		}
	    }
	}
      else if (sdparall.nparts>1) 
	assemb_complt(trav5,maillnodes.npoin,trav0,sdparall); /* erreur_chris : ici je ne comprends pas bien voir isa*/
      
    }

  /* second membre */
  /* ------------- */
  for (i=0;i<maillnodes.npoin;i++) *(matr.b+i) += *(trav1+i) * variable.var[variable.adr_pt_m1][i];

  if (affich.cond_mat) 
    {
      if (SYRTHES_LANG == FR)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("pt rang=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i]-1,i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("pt matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
      else if (SYRTHES_LANG == EN)
	{
	  if (sdparall.nparts > 1)	
	    for (i=0;i<maillnodes.npoin;i++) printf("pt rank=%d nglob=%d matr.b[%d]=%25.18e\n",
						    sdparall.rang,sdparall.npglob[i]-1,i,matr.b[i]);
	  else
	    for (i=0;i<maillnodes.npoin;i++) printf("pt matr.b[%d]=%25.18e\n",i,matr.b[i]);
	}
    }



  /* prise en compte de la partie explicite des flux volumique de PT */
  /* ------------------------------------------------------------- */
  if (fluxvol[ADR_PT].nelem)
    {
      azero1D(maillnodes.npoin,trav1);
      smfvos_expl (maillnodes,fluxvol[ADR_PT],trav1);
      if (sdparall.nparts>1)      
	assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
      for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)+=*(trav1+i);

      if (affich.cond_mat) 
	if (SYRTHES_LANG == FR)
	  for (i=0;i<maillnodes.npoin;i++) printf("apres fluxvol : matr.b[%d]=%f\n",i,matr.b[i]);
	else if (SYRTHES_LANG == EN)
	  for (i=0;i<maillnodes.npoin;i++) printf("after fluxvol : matr.b[%d]=%f\n",i,matr.b[i]);
    }


  
  /* *******************************************************   */
  /* Traitement des conditions aux limites (partie explicite)  */
  /* *******************************************************   */

  if (maillnodeus.nelem)
    {
 
      azero1D(maillnodes.npoin,trav1);
      for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}

      /* partie explicite des CL temperature - partie flux d'air sec */
      /* ----------------------------------------------------------- */
      hmt_smexp_pt(hmtclimhhh,maillnodes,maillnodeus,variable,constphyhmt,
		   trav0);
      
      /* assemblage de la matrice sur le bord */
      matbord(maillnodes,maillnodeus,trav0,trav1);

      /* ajout au second membre */
/*       for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)+=*(trav1+i); */


      if (affich.cond_mat) 
	{
	  if (SYRTHES_LANG == FR)
	    {
	      if (sdparall.nparts > 1)	
		for (i=0;i<maillnodes.npoin;i++) printf("pt rang=%d nglob=%d trav1[%d]=%25.18e\n",
							sdparall.rang,sdparall.npglob[i],i,trav1[i]);
	      else
		for (i=0;i<maillnodes.npoin;i++) printf("pt trav1[%d]=%25.18e\n",i,trav1[i]);
	    }
	  else if (SYRTHES_LANG == EN)
	    {
	      if (sdparall.nparts > 1)	
		for (i=0;i<maillnodes.npoin;i++) printf("pt rank=%d nglob=%d trav1[%d]=%25.18e\n",
							sdparall.rang,sdparall.npglob[i],i,trav1[i]);
	      else
		for (i=0;i<maillnodes.npoin;i++) printf("pt trav1[%d]=%25.18e\n",i,trav1[i]);
	    }
	}

      if (sdparall.nparts>1)      
	assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
      for (i=0;i<maillnodes.npoin;i++) *(matr.b+i)+=*(trav1+i);

    }
  else if (sdparall.nparts>1) /* pas de cond de flux mais du parallelisme : il faut assurer les communications */
    {
      azero1D(maillnodes.npoin,trav1);
      assemb_complt(trav1,maillnodes.npoin,trav0,sdparall);
    }


  if (sdparall.nparts==1)           
    grconj(maillnodes,variable.var[variable.adr_pt],rescon,perio,trav,matr,epsgcs_pt,nitmx_pt);
  else
    grconj_parall(maillnodes,variable.var[variable.adr_pt],rescon,perio,trav,matr,epsgcs_pt,nitmx_pt,sdparall);


/*   if (sdparall.nparts > 1)	 */
/*     for (i=0;i<maillnodes.npoin;i++) printf("rang=%d nglob=%d tmps[%d]=%25.18e\n", */
/* 					    sdparall.rang,sdparall.npglob[i],i,tmps[i]); */
/*   else */
/*     for (i=0;i<maillnodes.npoin;i++) printf("tmps[%d]=%25.18e\n",i,tmps[i]); */
  

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des conditions du second membre explicite sur T      |
  |======================================================================| */
void hmt_smexp_t(struct HmtClimhhh hmtclimhhh,
		 struct Maillage maillnodes,struct MaillageCL maillnodeus,
		 struct Variable variable,struct ConstPhyhmt constphyhmt,
		 double *trav0)
{
  int i,j,nf,ng,n;
  int nel,**nodeus;
  double omegamv,phiv,tt,pp;
  double *t,*pv,*pt;


  for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}
  nel=maillnodeus.nelem; 
  nodeus=maillnodeus.node;

  t=variable.var[variable.adr_t_m1];
  pv=variable.var[variable.adr_pv_m1];
  pt=variable.var[variable.adr_pt_m1];

  /* | ATTENTION  (pense bête Chris et isa pour version 4.3)
     ! sous programme en cours il faudra verifier et ameliorer lorsque eps0=0 (metaux) |
     | pourrait actuellement poser probleme |
  */


  for (j=0;j<hmtclimhhh.ndmat;j++)
    for (i=0;i<hmtclimhhh.nelem;i++)
      {

	/* partie explicite des coefficients d'echange sur T */
	trav0[hmtclimhhh.numf[i]+nel*j]=hmtclimhhh.t_h[j][i]*hmtclimhhh.t_ext[j][i];
	
	/* partie purement explicicte */
	nf=hmtclimhhh.numf[i]; ng=maillnodeus.node[j][nf];

	/* on prend la pression totale la plus forte pour le calcul de omegamv */ 
	if (pt[ng]>hmtclimhhh.pt_ext[j][i]) pp=pt[ng];
	else pp=hmtclimhhh.pt_ext[j][i];

	omegamv=constphyhmt.xmv*pv[ng]/
	  (constphyhmt.xmas*pp + (constphyhmt.xmv-constphyhmt.xmas)*pv[ng]);

	phiv = hmtclimhhh.pv_h[j][i]* hmtclimhhh.pt_ext[j][i]
	  * (hmtclimhhh.pv_ext[j][i]/hmtclimhhh.pt_ext[j][i] - pv[ng]/pt[ng]) 
	  + omegamv*hmtclimhhh.pt_h[j][i]*(hmtclimhhh.pt_ext[j][i]-pt[ng]) ;
	
	if (phiv<0) tt=t[ng]+tkel;                 /* on prend L(Tparoi)  si flux de vapeur sortant */ 
	else tt=hmtclimhhh.t_ext[j][i]+tkel;       /* on prend L(Text)    si flux de vapeur entrant */ 
	
	trav0[hmtclimhhh.numf[i]+nel*j] += fphyhmt_fxl(constphyhmt,tt)*phiv;
	
      }

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des conditions du second membre explicite sur PV     |
  |======================================================================| */
void hmt_smexp_pv(struct HmtClimhhh hmtclimhhh,
		  struct Maillage maillnodes,struct MaillageCL maillnodeus,
		  struct Variable variable,struct ConstPhyhmt constphyhmt,
		  double *trav0)
{
  int i,j,nf,ng,n;
  int nel,**nodeus;
  double omegamv,pp;
  double *pv,*pt;

  pv=variable.var[variable.adr_pv_m1];
  pt=variable.var[variable.adr_pt_m1];

  for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}
  nel=maillnodeus.nelem; 
  nodeus=maillnodeus.node;

  /* | ATTENTION  (pense bête Chris et isa pour version 4.3)
     ! sous programme en cours il faudra verifier et ameliorer lorsque eps0=0 (metaux) |
     | pourrait actuellement poser probleme |
  */


  for (j=0;j<hmtclimhhh.ndmat;j++)
    for (i=0;i<hmtclimhhh.nelem;i++)
      {
	/* flux de chaleur (temperature et flux de vapeur) */
	nf=hmtclimhhh.numf[i]; ng=maillnodeus.node[j][nf];

	trav0[hmtclimhhh.numf[i]+nel*j] = hmtclimhhh.pv_h[j][i]*hmtclimhhh.pv_ext[j][i];

	/* partie a decommenter si on veut completer les CL couplees */
	/* trav0[hmtclimhhh.numf[i]+nel*j] = hmtclimhhh.pv_h[j][i]/pt[ng]*hmtclimhhh.pv_ext[j][i]; */

	/* partie purement explicite */
	/* pp=hmtclimhhh.pt_ext[j][i];  */
	/* if (pt[ng]>hmtclimhhh.pt_ext[j][i]) pp=pt[ng]; */

	/* omegamv=constphyhmt.xmv*pv[ng]/ */
	/*   (constphyhmt.xmas*pp+(constphyhmt.xmv-constphyhmt.xmas)*pv[ng]); */

       	/* trav0[hmtclimhhh.numf[i]+nel*j] +=  omegamv*hmtclimhhh.pt_h[j][i] * (hmtclimhhh.pt_ext[j][i]-pt[ng]);  */
      }

}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Prise en compte des conditions du second membre explicite sur PT     |
  |======================================================================| */
void hmt_smexp_pt(struct HmtClimhhh hmtclimhhh,
		  struct Maillage maillnodes,struct MaillageCL maillnodeus,
		  struct Variable variable,struct ConstPhyhmt constphyhmt,
		  double *trav0)
{
  int i,j,nf,ng,n;
  int nel,**nodeus;
  double omegamv,has,pp;
  double *pv,*pt;


  for (i=0;i<maillnodeus.nelem*maillnodeus.ndmat;i++) {*(trav0+i)=0.;}
  nel=maillnodeus.nelem; 
  nodeus=maillnodeus.node;

  pv=variable.var[variable.adr_pv_m1];
  pt=variable.var[variable.adr_pt_m1];

  /* | ATTENTION  (pense bête Chris et isa pour version 4.3)
     ! sous programme en cours il faudra verifier et ameliorer lorsque eps0=0 (metaux) |
     | pourrait actuellement poser probleme |
  */

  /* flux de chaleur (temperature et flux air sec) */
  for (j=0;j<hmtclimhhh.ndmat;j++)
    for (i=0;i<hmtclimhhh.nelem;i++)
      {
	nf=hmtclimhhh.numf[i]; ng=*(*(nodeus+j)+nf);

	trav0[hmtclimhhh.numf[i]+nel*j] = hmtclimhhh.pt_h[j][i]*hmtclimhhh.pt_ext[j][i];


	/* remplacer par ce qui suit si on veut les CL completes */
	/* partie explicite des coefficients d'echange sur PT coherente avec implicite*/
	/* pp=hmtclimhhh.pt_ext[j][i];  */
	/* if (pt[ng]>hmtclimhhh.pt_ext[j][i]) pp=pt[ng]; */

	/* omegamv=constphyhmt.xmv*pv[ng]/ */
	/*   (constphyhmt.xmas*pp+(constphyhmt.xmv-constphyhmt.xmas)*pv[ng]); */

	/* trav0[hmtclimhhh.numf[i]+nel*j] = (1-omegamv)*hmtclimhhh.pt_h[j][i]*hmtclimhhh.pt_ext[j][i]; */


	/* partie purement explicite */
	/* has=constphyhmt.xmas/constphyhmt.xmv * hmtclimhhh.pv_h[j][i]; */

	/* trav0[hmtclimhhh.numf[i]+nel*j] += has/pt[ng]*hmtclimhhh.pv_ext[j][i]*(pv[ng]-hmtclimhhh.pv_ext[j][i]); */

      }

}
