/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>


#include "syr_tree.h"
#include "syr_bd.h"
#include "syr_abs.h"
#include "syr_option.h"
#include "syr_const.h"
#include "syr_proto.h"
#include "syr_hmt_proto.h"

/* 
 Fichiers vides pour permettre l'edition des liens quand l'utilisateur 
 ne fournit aucune fonction interpretee dans le fichier de donnees
*/


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | Conditions initiales                                                 |
  |======================================================================| */
void user_cini_fct(struct Maillage maillnodes,double *tmps)
{
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Proprietes physiques                                          |
  |======================================================================| */
void user_cphyso_fct(struct Maillage maillnodes,
		     double *tmps,struct Prophy physol,double tempss)
{
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Conditions aux limites                                        |
  |======================================================================| */
void user_limfso_fct(struct Maillage maillnodes,struct MaillageBord maillnodebord,
		     struct MaillageCL maillnodeus,
		     double *t,struct Clim diric,struct Clim flux,
		     struct Clim echang,struct Clim rayinf,double tempss)
{}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Flux volumiques                                               |
  |======================================================================| */
void user_cfluvs_fct(struct Maillage maillnodes,
		     double *tmps,struct Cvol fluxvol,double tempss)
{}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Conditions aux limites - resistances de contact               |
  |======================================================================| */
void user_rescon_fct(struct Maillage maillnodes,struct MaillageCL maillnodeus,
		     double *t,double *tcor,struct Contact rescon,double tempss,
		     struct SDparall sdparall)
{}
      
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2010 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Conditions initiales pour les transferts couples                     |
  |======================================================================| */
void user_hmt_cini_fct(struct Maillage maillnodes,
		       double *t,double *pv,double *pt)
{}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Conditions aux limites pour les transferts couples            |
  |======================================================================| */
void user_hmt_limfso_fct(struct Maillage maillnodes,struct MaillageCL maillnodeus,
			 double *t,double *pv,double *pt,
			 struct HmtClimhhh hmtclimhhh,
			 struct Clim rayinf,double tempss)
{}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  |        Conditions aux limites pour les transferts couples            |
  |======================================================================| */
void user_hmt_cfluvs_fct(struct Maillage maillnodes,
			 double *t,  struct Cvol fluxvol_t,
			 double *pv, struct Cvol fluxvol_pv,
			 double *pt, struct Cvol fluxvol_pt,
			 double tempss)
{}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Resistances de contact pour les transferts couples                   |
  |======================================================================| */
void user_hmt_rescon_fct(struct Maillage maillnodes,struct MaillageCL maillnodeus,
			 double *t,double *pv,double *pt,
			 double *tcor,double *pvcor,double *ptcor,
			 struct Contact rescon,double tempss,
			 struct SDparall sdparall)
{}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  Donnees de la transformation geometrique pour la periodicite        |
  |  dans le cas ou ce n'est ni une translation ni une rotation          | 
  |======================================================================| */
void user_transfo_perio_fct(int ndim,
			    int nb1,int *iref, 
			    double x,double y,double z,
			    double *xt, double *yt, double *zt)
{}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Proprietes physiques et conditions aux limites rayonnement    |                                    |
  |======================================================================| */
void user_ray_fct(struct Maillage maillnodray,
		  double *tmpray,struct ProphyRay phyray,
		  struct PropInfini *propinf,struct Clim fimpray,struct Vitre vitre,
		  double tempss)
{}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  Calcul personnel des flux solaires directs et diffus                |  
  |======================================================================| */
void user_solaire_fct(double *fdirect,double *fdiffus, int nbande,double tempss)
{}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |  Calcul personnel des proprietes physiques radiatives en fonction    | 
  |  de l'angle d'incidence                                              | 
  |======================================================================| */
void user_propincidence_fct(int n,int nbande,double teta,
			    double **e,double **r,double **a,double **t,
			    double **emissi,double **reflec,
			    double **absorb,double **transm)
{}

