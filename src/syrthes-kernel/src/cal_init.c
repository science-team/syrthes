/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_bd.h"
#include "syr_option.h"
#include "syr_abs.h"
#include "syr_proto.h"

/* #include "mpi.h" */

struct Performances perfo;
struct Affichages affich;

extern int nbVar;

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void touta0(struct Maillage *maillnodes,struct MaillageBord *maillnodebord,  
	    struct MaillageCL *maillnodeus, 
	    struct Clim *diric,struct Cperio *perio,
	    struct Clim *flux,struct Clim *echang,struct Contact *rescon,
	    struct Clim *rayinf,struct Cvol *fluxvol,
	    struct Couple *scoupr,struct Couple *rcoups,
	    struct HmtClimhhh *hmtclimhhh)

{
  int i,j;


  maillnodes->npoin=maillnodes->nelem=maillnodes->ndmat=0;
  maillnodebord->nelem=maillnodebord->ndmat=0;

  rayinf->npoin=rayinf->nelem=rayinf->ndmat=0;
  rescon->nelem=rescon->ndmat=0;

  perio->npoin=perio->nelem=perio->ndmat=0;

  scoupr->nelem=rcoups->nelem=0;

  maillnodeus->nelem=maillnodeus->ndmat=0;
  
  diric->npoin=diric->nelem=diric->ndmat=0;
  flux->npoin=flux->nelem=flux->ndmat=0;
  echang->npoin=echang->nelem=echang->ndmat=0;
  
  for (i=0;i<nbVar;i++)  fluxvol[i].nelem=0;


  hmtclimhhh->npoin = hmtclimhhh->nelem = hmtclimhhh->ndmat = 0;
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */

void svolum(struct Maillage *maillnodes,struct MaillageCL *maillnodeus)
{
  int i,n;
  int npetit,npetiu,np,np2;
  int *ne0,*ne1,*ne2,*ne3;
  double epsvol,epsvou,s6,s23;
  double x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4;
  double x12,y12,z12,x13,y13,z13,x14,y14,z14;
  double grand,*c0,*c1,*c2;
  struct Liste_entier *listv,*lists,*pv,*ps;


  grand  = 1.0e+6;  epsvol = 1.0e-6; npetit=npetiu=0;
  s6 = 1. / 6.;     s23 = 2. / 3.;
  np=maillnodes->npoin;    np2=np*2; ;
  maillnodes->volume=(double*)malloc(maillnodes->nelem*sizeof(double));
  verif_alloue_double1d("svolum",maillnodes->volume);
  perfo.mem_cond+=maillnodes->nelem * sizeof(double);

  for (i=0;i<maillnodes->nelem;maillnodes->volume[i]=0.,i++);

  if (maillnodeus->nelem)
    { 
      maillnodeus->volume=(double*)malloc(maillnodeus->nelem*sizeof(double));
      verif_alloue_double1d("svolum",maillnodeus->volume);
      perfo.mem_cond+=maillnodeus->nelem * sizeof(double);
      for (i=0;i<maillnodeus->nelem;i++) maillnodeus->volume[i]=0.;
    }


  listv=(struct Liste_entier*)malloc(sizeof(struct Liste_entier));  listv->suivant=NULL;
  lists=(struct Liste_entier*)malloc(sizeof(struct Liste_entier));  lists->suivant=NULL;
  pv=listv;  ps=lists;

  if (maillnodes->ndim==2)
    {
      epsvou=epsvol;  epsvol=epsvol*epsvol; 
      c0=maillnodes->coord[0]; c1=maillnodes->coord[1];
      for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],ne2=maillnodes->node[2];
	   i<maillnodes->nelem;
	   i++,ne0++,ne1++,ne2++)
	{
	  x1=*(c0+*ne0);  y1=*(c1+*ne0);
	  x2=*(c0+*ne1);  y2=*(c1+*ne1);
	  x3=*(c0+*ne2);  y3=*(c1+*ne2);
	  x12=x2-x1;  y12=y2-y1;
	  x13=x3-x1;  y13=y3-y1;
	  maillnodes->volume[i]=0.5* fabs( x12*y13 - y12*x13 );
	  if (maillnodes->volume[i]<epsvol) 
	    {
	      pv->num=i;
	      pv->suivant=(struct Liste_entier*)malloc(sizeof(struct Liste_entier));
	      pv=pv->suivant; pv->suivant=NULL;
	      npetit++;
	    }
	}

      if (maillnodeus->nelem) 
	for (i=0,ne0=maillnodeus->node[0],ne1=maillnodeus->node[1];i<maillnodeus->nelem;i++,ne0++,ne1++)
	  {
	    x1=*(c0+*ne0);  y1=*(c1+*ne0);
	    x2=*(c0+*ne1);  y2=*(c1+*ne1);
	    x12=x2-x1;  y12=y2-y1;
	    maillnodeus->volume[i] = sqrt(x12*x12 + y12*y12);
	    if (maillnodeus->volume[i]<epsvou) 
	      {
		ps->num=i;
		ps->suivant=(struct Liste_entier*)malloc(sizeof(struct Liste_entier));
		ps=ps->suivant; ps->suivant=NULL;
		npetiu++;
	      }
	  }
    }
  else
    {
      epsvou = epsvol*epsvol;   epsvol = epsvol*epsvol*epsvol;
      c0=maillnodes->coord[0]; c1=maillnodes->coord[1]; c2=maillnodes->coord[2];
      for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],ne2=maillnodes->node[2],ne3=maillnodes->node[3];
	   i<maillnodes->nelem;
	   i++,ne0++,ne1++,ne2++,ne3++)
	{
	  x1=*(c0+*ne0);  y1=*(c1+*ne0); z1=*(c2+*ne0);
	  x2=*(c0+*ne1);  y2=*(c1+*ne1); z2=*(c2+*ne1);
	  x3=*(c0+*ne2);  y3=*(c1+*ne2); z3=*(c2+*ne2);
	  x4=*(c0+*ne3);  y4=*(c1+*ne3); z4=*(c2+*ne3);

	  x12=x2-x1;  y12=y2-y1;   z12=z2-z1;  
	  x13=x3-x1;  y13=y3-y1;   z13=z3-z1;
	  x14=x4-x1;  y14=y4-y1;   z14=z4-z1;
	  maillnodes->volume[i] = s6 * fabs( x12 * (y13*z14-z13*y14) - x13*(y12*z14-z12*y14)
                              + x14*(y12*z13-z12*y13) );

	  if (maillnodes->volume[i]<epsvol) 
	    {
	      pv->num=i;
	      pv->suivant=(struct Liste_entier*)malloc(sizeof(struct Liste_entier));
	      pv=pv->suivant; pv->suivant=NULL;
	      npetit++;
	    }
	}

      if (maillnodeus->nelem) 
	for (i=0,ne0=maillnodeus->node[0],ne1=maillnodeus->node[1],ne2=maillnodeus->node[2];
	     i<maillnodeus->nelem;i++,ne0++,ne1++,ne2++)
	  {
	    x1=*(c0+*ne0);  y1=*(c1+*ne0); z1=*(c2+*ne0);
	    x2=*(c0+*ne1);  y2=*(c1+*ne1); z2=*(c2+*ne1);
	    x3=*(c0+*ne2);  y3=*(c1+*ne2); z3=*(c2+*ne2);
	    x12=x2-x1;  y12=y2-y1;   z12=z2-z1;  
	    x13=x3-x1;  y13=y3-y1;   z13=z3-z1;
	    maillnodeus->volume[i] = 0.5 * sqrt ( (y12*z13-z12*y13)*(y12*z13-z12*y13)
						  +(x12*z13-z12*x13)*(x12*z13-z12*x13)
						  +(x12*y13-y12*x13)*(x12*y13-y12*x13) );
	    if (maillnodeus->volume[i]<epsvou) 
	      {
		ps->num=i;
		ps->suivant=(struct Liste_entier*)malloc(sizeof(struct Liste_entier));
		ps=ps->suivant; ps->suivant=NULL;
		npetiu++;
	      }
	  }
    }

  if (perfo.mem_cond>perfo.mem_cond_max) perfo.mem_cond_max=perfo.mem_cond;

  if (npetit>0)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n  %%%% ERREUR svolum : \n");
	  printf("           Il y a des elements degeneres dans le maillage solide : %d elements\n",npetit);
	  printf("           Liste des elements :\n           ");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n  %%%% ERROR svolum : \n");
	  printf("           Some elements of the mesh are degenerated : %d elements\n",npetit);
	  printf("           Element list :\n           ");
	}
      pv=listv;
      for (i=0;i<npetit;i++)
	{printf("%d ",pv->num+1); pv=pv->suivant;}
      printf("\n");
    }

  if (npetiu>0)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n  %%%% ERREUR svolum : \n");
	  printf("           Il y a des elements degeneres dans le maillage de bord solide : %d elements\n",npetiu);
	  printf("           Liste des elements :\n           ");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n  %%%% ERROR svolum : \n");
	  printf("           Some elements of the boundary mesh are degenerated : %d elements\n",npetiu);
	  printf("           Element list :\n           ");
	}
      ps=lists;
      for (i=0;i<npetiu;i++)
	{printf("%d ",ps->num+1); ps=ps->suivant;}
      printf("\n");
    }

  if (npetit>0 || npetiu>0) syrthes_exit(1);

}






