/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <math.h>

#include "syr_usertype.h"
#include "syr_const.h"
#include "syr_bd.h"
#include "syr_tree.h"
#include "syr_option.h"
#include "syr_parall.h"
#include "syr_proto.h"
#include "syr_abs.h"

#ifdef _SYRTHES_MPI_
#include "mpi.h"
MPI_Status status;
#endif

extern char nomdata[CHLONG];
extern FILE *fdata;

struct Performances perfo;
struct Affichages affich;

static char ch[CHLONG],motcle[CHLONG],repc[CHLONG];
static double dlist[100];
static int ilist[MAX_REF];

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void init_radiation(struct GestionFichiers gestionfichiers,
		    struct Maillage *maillnodray,struct Maillage maillnodes, 
		    struct MaillageBord maillnodebord,
		    struct MaillageCL maillnodeus,struct FacForme ****listefdf,double **diagfdf,
		    struct Couple *scoupr,struct Couple *rcoups,
		    struct Compconnexe *volconnexe,struct PropInfini *propinf,
		    struct Clim *fimpray,struct Clim *timpray,
		    struct ProphyRay *phyray,struct SymPer *raysymper,
		    struct Soleil *soleil,struct Bilan *bilansolaire,
		    struct node **arbre_solaire,double *size_min_solaire,double *dim_boite_solaire,
		    struct Lieu *lieu,struct Meteo meteo,struct Horizon* horiz,
		    struct Vitre *vitre,struct Mask *mask,
		    double **tmpray,double ***radios,double ***firay,
		    double **trayeq,double **erayeq,double ***epropr,
		    struct SDparall_ray *sdparall_ray,struct SDparall sdparall)
{
  int i,j;
  double tcpu1,tcpu2;
  struct Maillage maillnodecoupl;

  tcpu1=cpusyrt();
  
  lire_divers_ray(*maillnodray,raysymper,volconnexe,phyray->bandespec,
		  propinf,mask,
		  soleil,lieu,bilansolaire,horiz);
  
  /* initialisation du type des faces sur le maillage total */ 
  decode_clim_ray(maillnodray);
  
  /* calcul des facteurs de forme */
  inifdf(gestionfichiers,maillnodray,*raysymper,*volconnexe,propinf,
	 listefdf,diagfdf,*mask,vitre,horiz,sdparall_ray);
  
  
  /* calcul des correspondants du solide  */
  init_scoupr(maillnodeus,scoupr,*sdparall_ray);
  corresSR(gestionfichiers,maillnodes,maillnodeus,*maillnodray,*scoupr,*sdparall_ray);
  
  
  
  /* regularisation des facteurs de forme */
  regufdf(gestionfichiers,*maillnodray,*propinf,*listefdf,*diagfdf,
	  *mask,*horiz,*sdparall_ray);
  
  
  /* calcul des correspondants du rayonnement */
  init_rcoups(*maillnodray,rcoups);
  corresRS(gestionfichiers,maillnodebord,maillnodes,maillnodeus,
	   *maillnodray,*scoupr,*rcoups,*sdparall_ray);
  
  
  limfray(*maillnodray,timpray,fimpray,vitre);
  
  alloueray(*maillnodray,phyray,fimpray,
	    tmpray,radios,firay,trayeq,erayeq,epropr);
  
  lire_limite_ray(*maillnodray,*phyray,vitre,fimpray,*tmpray);
  
  
  /* ??????????? attention,  voir s'il ne faut pas allouer quand meme les tableaux
     pour les vitres meme si on n'est pas en global */
  if (vitre->actif && !vitre->prop_glob)
    { 
      vitre->refrac=(double**)malloc(phyray->bandespec.nb*sizeof(double*));
      for (j=0;j<phyray->bandespec.nb;j++) 
	{
	  vitre->refrac[j]=(double*)malloc(maillnodray->nelem*sizeof(double));
	  for (i=0;i<maillnodray->nelem;i++) vitre->refrac[j][i]=1.;
	}
      verif_alloue_double2d(phyray->bandespec.nb,"alloueray",vitre->refrac);
      perfo.mem_ray+=phyray->bandespec.nb*maillnodray->nelem*sizeof(double);
      
      proprvitre(*vitre,*maillnodray,*phyray);
    }
  
  
  if (soleil->actif) initsolaire(soleil,*lieu,arbre_solaire,*maillnodray,*phyray,
				 size_min_solaire,dim_boite_solaire,sdparall);
}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void init_radiation_parall(struct GestionFichiers gestionfichiers,
			   struct Maillage *maillnodray,struct Maillage maillnodes, struct MaillageBord maillnodebord,
			   struct MaillageCL maillnodeus,struct FacForme ****listefdf,double **diagfdf,
			   struct Couple *scoupr,struct Couple *rcoups,
			   struct Compconnexe *volconnexe,struct PropInfini *propinf,
			   struct Clim *fimpray,struct Clim *timpray,
			   struct ProphyRay *phyray,struct SymPer *raysymper,
			   struct Soleil *soleil,struct Bilan *bilansolaire,
			   struct node **arbre_solaire,double *size_min_solaire,double *dim_boite_solaire,
			   struct Lieu *lieu,struct Meteo meteo,struct Horizon* horiz,
			   struct Vitre *vitre,struct Mask *mask,
			   double **tmpray,double ***radios,double ***firay,
			   double **trayeq,double **erayeq,double ***epropr,
			   struct SDparall_ray *sdparall_ray,struct SDparall sdparall)
{
  int i,j;
  double tcpu1,tcpu2;
  struct Maillage maillnodecoupl;

  tcpu1=cpusyrt();

  iniparallray(*maillnodray,sdparall_ray);
  
  lire_divers_ray(*maillnodray,raysymper,volconnexe,phyray->bandespec,
		  propinf,mask,
		  soleil,lieu,bilansolaire,horiz);



  /* initialisation du type des faces sur le maillage total */ 
  decode_clim_ray(maillnodray);
  
  /* calcul des facteurs de forme si le proc fait du rayt*/
  if (sdparall_ray->rangray>-1) inifdf(gestionfichiers,maillnodray,*raysymper,*volconnexe,propinf,
				       listefdf,diagfdf,*mask,vitre,horiz,sdparall_ray);
  
  
  /* calcul des correspondants du solide : sur tous les proc */
  init_scoupr(maillnodeus,scoupr,*sdparall_ray);
  corresSR(gestionfichiers,maillnodes,maillnodeus,*maillnodray,*scoupr,*sdparall_ray);
  
  
  /* si le proc fait du rayt */
  if (sdparall_ray->rangray>-1)
    {
      /* reduction du maillage de rayonnement */
      reduire_maillage(maillnodray,*scoupr,sdparall_ray);
      
      /* regularisation des facteurs de forme (sur les proc de rayt seulement) */
      regufdf(gestionfichiers,*maillnodray,*propinf,*listefdf,*diagfdf,
	      *mask,*horiz,*sdparall_ray);
      
      /* initialisation pour les correspondants du rayonnement (proc de rayt seult)*/
      init_rcoups(*maillnodray,rcoups);
    }
  
  
/*   if (gestionfichiers.lec_corr_sr) option supprimee */
/*     { */
/*       lire_corrRS(maillnodes.ndim,*rcoups); */
/*     } */
/*   else */

  /* extraction et envoi du maillage solide couple au rayt (sur tous les proc) */
  extract_nodecoupl_parall(maillnodes,maillnodeus,*scoupr,&maillnodecoupl,*sdparall_ray);
  
  alloue_linkcondray_parall(maillnodes.ndim,sdparall_ray);
  
  /* reception des maillages et calcul des correspondants du rayonnement */
  corresRS_parall(maillnodes,*maillnodray,maillnodecoupl,*scoupr,*rcoups,sdparall_ray);
  
  /*   if (sdparall_ray->rangray>-1 && gestionfichiers.stock_corr_sr) option supprimee */
  /*     ecrire_corrRS(maillnodes.ndim,*rcoups); */
  
  link_condray_parall(maillnodes.ndim,*rcoups,sdparall_ray);
  
  /* destruction du maillage de bord solide couple au rayt */
  for (i=0;i<maillnodecoupl.ndmat;i++) free(maillnodecoupl.node[i]);
  free (maillnodecoupl.node);
  free (maillnodecoupl.nrefe);
  perfo.mem_ray-=(maillnodecoupl.ndmat+1)*maillnodecoupl.nelem*sizeof(double);

  
  
  /* si le proc fait du rayt */
  if (sdparall_ray->rangray>-1)
    {
      limfray(*maillnodray,timpray,fimpray,vitre);
      
      alloueray(*maillnodray,phyray,fimpray,
		tmpray,radios,firay,trayeq,erayeq,epropr);
      
      lire_limite_ray(*maillnodray,*phyray,vitre,fimpray,*tmpray);
      
      
      /* ??????????? attention,  voir s'il ne faut pas allouer quand meme les tableaux
	 pour les vitres meme si on n'est pas en global */
      if (vitre->actif && !vitre->prop_glob)
	{ 
	  vitre->refrac=(double**)malloc(phyray->bandespec.nb*sizeof(double*));
	  for (j=0;j<phyray->bandespec.nb;j++) 
	    {
	      vitre->refrac[j]=(double*)malloc(maillnodray->nelem*sizeof(double));
	      for (i=0;i<maillnodray->nelem;i++) vitre->refrac[j][i]=1.;
	    }
	  verif_alloue_double2d(phyray->bandespec.nb,"alloueray",vitre->refrac);
	  perfo.mem_ray+=phyray->bandespec.nb*maillnodray->nelem*sizeof(double);
	  
	  proprvitre(*vitre,*maillnodray,*phyray);
	}
      
      
      if (soleil->actif) initsolaire(soleil,*lieu,arbre_solaire,*maillnodray,*phyray,
				     size_min_solaire,dim_boite_solaire,sdparall);
    }
  

}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |    typage des faces de bord                                          |
  |======================================================================| */
void decode_clim_ray(struct Maillage *maillnodray)
{
  int n,i,j,nr,numvar;
  int n1,n2,n3,n4,n5,n6,n7,n8;
  int nbcoups,nbti,nbfi,nbvitre;
  int i1,i2,i3,i4,ii,id,nb,ok;
  double val;

  int nbc=4;  /* 0=coupl 1=Timp 2=Fimp 3=transp */
  int **ref;


  /* Allocations */
  /* ----------- */
  maillnodray->type=(int*)malloc(maillnodray->nelem*sizeof(int));
  for (i=0;i<maillnodray->nelem;i++) maillnodray->type[i]=0;


  ref=(int**)malloc(nbc*sizeof(int*));
  for (i=0;i<nbc;i++)                    
    {
      ref[i]=(int*)malloc(MAX_REF*sizeof(int));
      for (j=0;j<MAX_REF;j++) ref[i][j]=0;
    }

  /* premier passage : decodage des references des CL */
  /* ------------------------------------------------ */

  fseek(fdata,0,SEEK_SET);
  
  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"CLIM_RAYT")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"COUPLAGE_CONDUCTION")) 
		{
		  rep_listint(ilist,&nb,ch+id);
		  verif_liste_ref_0N("COUPLAGE_CONDUCTION",nb,ilist);
		  for (n=0;n<nb;n++) ref[0][ilist[n]]=1;
		}
	      else if (!strcmp(motcle,"TEMPERATURE_IMPOSEE")) 
		{
		  rep_ndbl(1,&val,&ii,ch+id);
		  rep_listint(ilist,&nb,ch+id+ii);
		  verif_liste_ref_0N("TEMPERATURE_IMPOSEE",nb,ilist);
		  for (n=0;n<nb;n++) ref[1][ilist[n]]=1;
		}
	      else if (!strcmp(motcle,"FLUX_IMPOSE_PAR_BANDE")) 
		{
		  rep_ndbl(1,&val,&ii,ch+id);
		  rep_listint(ilist,&nb,ch+id+ii);
		  verif_liste_ref_0N("FLUX_IMPOSE_PAR_BANDE",nb,ilist);
		  for (n=0;n<nb;n++) ref[2][ilist[n]]=1;
		}
	      else if (!strcmp(motcle,"SEMI_TRANSPARENT")) 
		{
		  rep_ndbl(1,&val,&ii,ch+id);
		  rep_listint(ilist,&nb,ch+id+ii);
		  verif_liste_ref_0N("SEMI_TRANSPARENT",nb,ilist);
		  for (n=0;n<nb;n++) ref[3][ilist[n]]=1;
		}
	    }
	}
    }

  /* initialisation des type de facettes */
  /* ----------------------------------- */
  nbcoups=nbti=nbfi=nbvitre=0;
   for (i=0;i<maillnodray->nelem;i++)
    {
      nr=maillnodray->nrefe[i];
      if (ref[0][nr]) {maillnodray->type[i]=TYPRCOUPS;  nbcoups++;}
      else if (ref[1][nr]) {maillnodray->type[i]=TYPRTI;  nbti++; }
      else if (ref[2][nr]) {maillnodray->type[i]=TYPRFI;  nbfi++;}
      else if (ref[3][nr]) {maillnodray->type[i]=TYPRVITRE;  nbvitre++;}
    }

   for (i=0;i<nbc;i++) free(ref[i]);
   free(ref);
   

  /* impressions */
  /* ----------- */
   if (syrglob_nparts==1 || syrglob_rang==0)
     if (SYRTHES_LANG == FR)
       {
	 printf("\n *** decode_clim_ray : type des faces de rayonnement en fonction des CL\n");
	 printf("           -----------------------------------------------\n");
	 printf("           |    Condition limite        |  Nbre de faces |\n");
	 printf("           -----------------------------|-----------------\n");
	 printf("           |  Couplage avec le solide   | %10d     |\n",nbcoups);
	 printf("           |     Temperature imposee    | %10d     |\n",nbti);
	 printf("           |        Flux impose         | %10d     |\n",nbfi);
	 printf("           |         Vitrage            | %10d     |\n",nbvitre);
	 printf("           -----------------------------------------------\n");
       }
     else if (SYRTHES_LANG == EN)
       {
	 printf("\n *** decode_clim_ray : Radiation face face type according the boundary conditions\n");
	 printf("           -----------------------------------------------\n");
	 printf("           |   Boundary conditions      |  Nbre of faces |\n");
	 printf("           -----------------------------|-----------------\n");
	 printf("           |   Coupled with the solid   | %10d     |\n",nbcoups);
	 printf("           |     Imposed temperature    | %10d     |\n",nbti);
	 printf("           |      Prescribed flux       | %10d     |\n",nbfi);
	 printf("           |         Windows            | %10d     |\n",nbvitre);
	 printf("           -----------------------------------------------\n");
       }

  /* Verifications */
  /* ------------- */
   if (nbcoups==0)
     {
       if (syrglob_nparts==1 || syrglob_rang==0)
	 if (SYRTHES_LANG == FR)
	   {
	     printf("\n\n %%%% ERREUR decode_clim_ray : Il n'y a pas de facettes ");
	     printf("de rayonnement couplee avec la conduction\n ");
	     printf("                             Voir le mot-cle 'CLIM_RAYT= COUPLAGE_CONDUCTION'");
	     printf(" dans le fichier de donnees (syrthes.data)\n\n"); 
	   }
	 else
	   {
	     printf("\n\n %%%% ERREUR decode_clim_ray : There is no radiation face coupled ");
	     printf("with the conduction mesh\n ");
	     printf("                             Check the keyword 'CLIM_RAYT= COUPLAGE_CONDUCTION'");
	     printf(" in the data file (syrthes.data)\n\n"); 
	   }
       syrthes_exit(1);
     }

}

/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void alloueray(struct Maillage maillnodray,struct ProphyRay *phyray,
	       struct Clim *fimpray,double **tmpray,
	       double ***radios,double ***firay,double **trayeq,double **erayeq,
	       double ***epropr)
{
  int i,j;


  phyray->emissi=(double**)malloc(phyray->bandespec.nb*sizeof(double*));
  phyray->transm=(double**)malloc(phyray->bandespec.nb*sizeof(double*));
  phyray->reflec=(double**)malloc(phyray->bandespec.nb*sizeof(double*));
  phyray->absorb=(double**)malloc(phyray->bandespec.nb*sizeof(double*));
  
  for (j=0;j<phyray->bandespec.nb;j++)
    { 
      phyray->emissi[j]=(double*)malloc(maillnodray.nelem*sizeof(double));
      phyray->transm[j]=(double*)malloc(maillnodray.nelem*sizeof(double));
      phyray->reflec[j]=(double*)malloc(maillnodray.nelem*sizeof(double));
      phyray->absorb[j]=(double*)malloc(maillnodray.nelem*sizeof(double));
    }

  for (j=0;j<phyray->bandespec.nb;j++)
    for (i=0;i<maillnodray.nelem;i++)
      {
	phyray->emissi[j][i] = 1.;
	phyray->transm[j][i] = 0.;
	phyray->reflec[j][i] = 0.;
	phyray->absorb[j][i] = 1.;
      }

  verif_alloue_double2d(phyray->bandespec.nb,"alloueray",phyray->emissi);
  verif_alloue_double2d(phyray->bandespec.nb,"alloueray",phyray->transm);
  verif_alloue_double2d(phyray->bandespec.nb,"alloueray",phyray->reflec);
  verif_alloue_double2d(phyray->bandespec.nb,"alloueray",phyray->absorb);

  perfo.mem_ray+=phyray->bandespec.nb*maillnodray.nelem*2*sizeof(double);

  
  *tmpray=(double*)malloc(maillnodray.nelem*sizeof(double));
  for (i=0;i<maillnodray.nelem;i++) *(*tmpray+i)=20.;
  *trayeq=(double*)malloc(maillnodray.nelem*sizeof(double));
  *erayeq=(double*)malloc(maillnodray.nelem*sizeof(double));
  verif_alloue_double1d("alloueray",*tmpray);
  verif_alloue_double1d("alloueray",*trayeq);
  verif_alloue_double1d("alloueray",*erayeq);
  perfo.mem_ray+=maillnodray.nelem*3*sizeof(double);


  *firay=(double**)malloc(phyray->bandespec.nb*sizeof(double));
  for (i=0;i<phyray->bandespec.nb;i++) *(*firay+i)=(double*)malloc(maillnodray.nelem*sizeof(double));
  verif_alloue_double2d(phyray->bandespec.nb,"alloueray",*firay);
  perfo.mem_ray+=phyray->bandespec.nb*maillnodray.nelem*sizeof(double);

  *radios=(double**)malloc(phyray->bandespec.nb*sizeof(double));
  for (i=0;i<phyray->bandespec.nb;i++) *(*radios+i)=(double*)malloc(maillnodray.nelem*sizeof(double));
  verif_alloue_double2d(phyray->bandespec.nb,"alloueray",*radios);
  perfo.mem_ray+=phyray->bandespec.nb*maillnodray.nelem*sizeof(double);
 
  *epropr=(double**)malloc(phyray->bandespec.nb*sizeof(double));
  for (i=0;i<phyray->bandespec.nb;i++) *(*epropr+i)=(double*)malloc(maillnodray.nelem*sizeof(double));
  verif_alloue_double2d(phyray->bandespec.nb,"alloueray",*epropr);
  perfo.mem_ray+=phyray->bandespec.nb*maillnodray.nelem*sizeof(double);


  if (fimpray->nelem>0) 
    {
      fimpray->val1=(double**)malloc(phyray->bandespec.nb*sizeof(double*));
      for (i=0;i<phyray->bandespec.nb;i++)
	fimpray->val1[i]=(double*)malloc(fimpray->nelem*sizeof(double));
      verif_alloue_double2d(phyray->bandespec.nb,"alloueray",fimpray->val1);
      perfo.mem_ray+=phyray->bandespec.nb*fimpray->nelem*sizeof(double);
  
      for (j=0;j<phyray->bandespec.nb;j++)
	for (i=0;i<fimpray->nelem;i++)
	  fimpray->val1[j][i]=0;
    }


  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;


}

  
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void limfray(struct Maillage maillnodray,
	     struct Clim *timpray,struct Clim *fimpray,struct Vitre *vitre)

{
  int i,j,nr,nf;
  int n1,n2;


  /* Compte des facettes avec CL particuliere */
  timpray->nelem=fimpray->nelem=vitre->nelem=0;
  for (i=0;i<maillnodray.nelem;i++)
    switch (maillnodray.type[i])
      {
      case TYPRTI : (timpray->nelem)++;  break;
      case TYPRFI : (fimpray->nelem)++;  break;
      case TYPRVITRE : (vitre->nelem)++; break;
      }
    

  if (timpray->nelem) 
    {timpray->numf=(int*)malloc(timpray->nelem*sizeof(int)); 
      verif_alloue_int1d("limfray",timpray->numf);}
  if (fimpray->nelem) 
    {fimpray->numf=(int*)malloc(fimpray->nelem*sizeof(int)); 
      verif_alloue_int1d("limfray",fimpray->numf);}
  perfo.mem_ray+=(timpray->nelem+fimpray->nelem)*sizeof(double);
  
  for (i=n1=n2=0,nr=maillnodray.nrefe[0];i<maillnodray.nelem;nr=maillnodray.nrefe[i],i++) 
    switch (maillnodray.type[i])
      {
      case TYPRTI : timpray->numf[n1]=i;n1++; break;
      case TYPRFI : fimpray->numf[n2]=i;n2++; break;
      }

  vitre->actif=0;
  if (vitre->nelem) vitre->actif=1;

 
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;

}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void verif_fdf(int nelray,double *sufray,struct FacForme ***listefdf,double *diagfdf,
	       int npartsray)
{
  int i,j,nb=0;
  double s;
  struct FacForme *pff;

/*   if (syrglob_nparts==1 || syrglob_rang==0) */
/*     if (SYRTHES_LANG == FR) */
/*       printf("\n\n *** verif_fdf : Liste des facettes ayant un facteur de forme mediocre :\n"); */
/*     else if (SYRTHES_LANG == EN) */
/*       printf("\n\n *** verif_fdf : List of faces having a poor quality view factor :\n"); */

  for (i=0;i<nelray;i++)
    {
      s=0;
      for (j=0;j<npartsray;j++)
	{
	  pff=listefdf[j][i];
	  while(pff) {s+=pff->fdf; pff=pff->suivant;}
	}
      s+=diagfdf[i];
      
      if (fabs(s/sufray[i]-1)>0.07)
	{
	  nb++;
/* 	  if (SYRTHES_LANG == FR)	   */
/* 	    printf("                 Facette %d   Erreur %.2f %%\n",i,fabs(s/sufray[i]-1)*100); */
/* 	  else if (SYRTHES_LANG == EN) */
/* 	    printf("                 Face %d   Error %.2f %%\n",i,fabs(s/sufray[i]-1)*100); */
	}
    }

  if (syrglob_nparts==1 || syrglob_rang==0)
    if (SYRTHES_LANG == FR)
      printf("\n\n *** verif_fdf : %d facette(s) ont un facteur de forme mediocre\n",nb);
    else if (SYRTHES_LANG == EN)
      printf("\n\n *** verif_fdf : %d face(s) have a poor quality view factor\n",nb);

}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | cfdfnp1                                                              |
  |                                                                      |
  |======================================================================| */
void calfdfnp1(struct Maillage maillnodray,
	       struct FacForme ***listefdf,double *diagfdf,
	       struct PropInfini *propinf,
	       struct Horizon horizon,
	       int npartsray)
{
  int n,i,j,nelray;
  double s;
  struct FacForme *pff;

  propinf->fdfnp1=(double*)malloc(maillnodray.nelem*sizeof(double));
  perfo.mem_ray+=maillnodray.nelem*sizeof(double);
  verif_alloue_double1d("calfdfnp1",propinf->fdfnp1);
 
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  
  for (i=0;i<maillnodray.nelem;i++)
    {
      s=0;
      for (j=0;j<npartsray;j++)
	{
	  pff=listefdf[j][i];
	  while(pff) {s+=pff->fdf;  pff=pff->suivant;}
	}
      s+=diagfdf[i];
      if (horizon.actif) s+=horizon.fdf[i];
      s/=maillnodray.volume[i];
      if (s>=1.) propinf->fdfnp1[i]=0;
      else  propinf->fdfnp1[i]=(1.-s)*maillnodray.volume[i];
    }
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | smoogc                                                               |
  |                                                                      |
  |======================================================================| */
void smoogc(int nelray,struct FacForme **listefdf,
	    double *diagfdf,double *sufray)
{
  int n=0,i,j,nitsmo=100;
  double *x,*b,*dd,*gd,*res,*z,*di;
  double x0,resnor,sl,rgrg,ro,prsca1,prsca2,f;
  double alp,epsis,zero=0.,epssmo=1.e-8,s,ss,*xx;
  struct FacForme *pff;

  x=(double*)malloc(nelray*sizeof(double));     verif_alloue_double1d("smoogc",x);
  b=(double*)malloc(nelray*sizeof(double));     verif_alloue_double1d("smoogc",b);
  dd=(double*)malloc(nelray*sizeof(double));    verif_alloue_double1d("smoogc",dd);
  gd=(double*)malloc(nelray*sizeof(double));    verif_alloue_double1d("smoogc",gd);
  res=(double*)malloc(nelray*sizeof(double));   verif_alloue_double1d("smoogc",res);
  z=(double*)malloc(nelray*sizeof(double));     verif_alloue_double1d("smoogc",z);
  di=(double*)malloc(nelray*sizeof(double));    verif_alloue_double1d("smoogc",di);
  
 
  perfo.mem_ray+=nelray*7*sizeof(double);
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  

  for (i=0;i<nelray;i++)
    {
      s=ss=0;
      pff=listefdf[i];
      while(pff) {f=pff->fdf; 
	s+=f; ss+=f*f; pff=pff->suivant;}
      s+=diagfdf[i];
      ss+=2*diagfdf[i]*diagfdf[i];
      b[i]=sufray[i]-s;
      di[i]=ss;
      x[i]=0;
    }

  /* Norme du second membre */
  x0=1./nelray; epsis=1.e-6 * x0;
  for (i=0;i<nelray;i++) res[i] = -b[i];
  for (i=0,prsca1=0,xx=res;i<nelray;i++,xx++) prsca1+= *xx * *xx; resnor=sqrt(prsca1);  

/*   printf("smoogc_seq_ini : resnor=%25.18e \n",resnor); */
  
  if (resnor<=epsis && resnor<=epssmo*sqrt((double)(nelray)))
    {  
      if (SYRTHES_LANG == FR)
	printf("\n *** smoogc: ITERATION   PRECISION RELATIVE  PRECISION ABSOLUE\n");
      else if (SYRTHES_LANG == EN)
	printf("\n *** smoogc: ITERATION   RELATIVE PRECISION  ABSOLUTE PRECISION\n");
      printf("                %4d         %12.5e         %12.5e\n",
	     n,resnor/x0,resnor/sqrt((double)(nelray)));
    }
  else
    {
      
      if (SYRTHES_LANG == FR)
	printf("\n *** smoogc: ITERATION   PRECISION RELATIVE  PRECISION ABSOLUE\n");
      else if (SYRTHES_LANG == EN)
	printf("\n *** smoogc: ITERATION   RELATIVE PRECISION  ABSOLUTE PRECISION\n");
      
      do
	{
	  n++;
	  for (i=0;i<nelray;i++) gd[i]=res[i]/di[i];
	  for (i=0,sl=0;i<nelray;i++) sl+=res[i]*gd[i];
	  if (n==1) for (i=0;i<nelray;i++) dd[i]=gd[i];
	  else {alp=sl/rgrg; for (i=0;i<nelray;i++) dd[i]=gd[i]+dd[i]*alp;}
	  rgrg=sl;

	  for (i=0;i<nelray;i++)
	    {
	      s=0;
	      pff=listefdf[i];
	      while(pff) {f=pff->fdf; s+=f*f*dd[pff->numenface];pff=pff->suivant;}
	      s+=di[i]*dd[i];
	      z[i]=s;
	    }
	  for (i=0,prsca1=0;i<nelray;i++,xx++) prsca1+=res[i]*dd[i]; 
	  for (i=0,prsca2=0;i<nelray;i++,xx++) prsca2+=z[i]*dd[i];   

	  ro=- prsca1 / prsca2;
	  for (i=0;i<nelray;i++) x[i]+=ro*dd[i];
	  for (i=0;i<nelray;i++) res[i]+=ro*z[i];
	  for (i=0,prsca1=0,xx=res;i<nelray;i++,xx++) prsca1+= *xx * *xx; 
	  resnor=sqrt(prsca1);
	  if (affich.ray_fdf) 
	    printf("           %4d         %12.5e                %12.5e\n",
		   n,resnor/x0,resnor/sqrt((double)(nelray)));
	}
      while(!( (resnor<=epsis &&  resnor<=epssmo*sqrt((double)(nelray))) || n>=nitsmo ));
  
      if (SYRTHES_LANG == FR)
	printf("  %4d ITERATIONS  PRECISION RELATIVE=%12.5e PRECISION ABSOLUE=%12.5e\n",
	       n,resnor/x0,resnor/sqrt((double)(nelray)));
      else if (SYRTHES_LANG == EN)
	printf("  %4d ITERATIONS  RELATIVE PRECISION=%12.5e ABSOLUTE PRECISION=%12.5e\n",
	       n,resnor/x0,resnor/sqrt((double)(nelray)));
    }

  /* Modification des coefs par multiplicateurs de lagrange */
  for (i=0;i<nelray;i++)
    {
      pff=listefdf[i];
      while(pff) 
	{
	  pff->fdf= pff->fdf*(1. + (x[i]+x[pff->numenface])*pff->fdf );
	  pff=pff->suivant;
	} 
      diagfdf[i]= diagfdf[i] * (1. + (x[i]+x[i])*diagfdf[i]);
    }


  free(x);free(b);free(dd);free(gd);free(res);free(z);free(di);
  perfo.mem_ray-=nelray*7*sizeof(double);
}



/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void extraitmask(struct Maillage *maillnodray,
		 struct Mask *mask,struct Horizon *horiz)
{
  int i,nbm,nbh,nbrm,nbrh,okm,okh,nb1m,nb1h,nb2,nr,j;
  int i1,i2,i3,i4,id,n,nb,ii;
  int **node,*nref;
  int *refm,*refh;
  double val;


  /* premier passage : reperage des ombrages s'il y en a */
  /* --------------------------------------------------- */
  refm=(int*)malloc(MAX_REF*sizeof(int));
  for (i=0;i<MAX_REF;i++) refm[i]=0;
  refh=(int*)malloc(MAX_REF*sizeof(int));
  for (i=0;i<MAX_REF;i++) refh[i]=0;

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"CLIM_RAYT")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"OMBRAGE")) 
		{
		  rep_ndbl(1,&val,&ii,ch+id);
		  rep_listint(ilist,&nb,ch+id+ii);
		  verif_liste_ref_0N("CLIM_RAYT   OMBRAGE",nb,ilist);
		  for (n=0;n<nb;n++) for (n=0;n<nb;n++) refm[ilist[n]]=1;
		}
	      else if (!strcmp(motcle,"OMBRAGE")) 
		{
		  rep_ndbl(1,&val,&ii,ch+id);
		  rep_listint(ilist,&nb,ch+id+ii);
		  verif_liste_ref_0N("CLIM_RAYT   HORIZON",nb,ilist);
		  for (n=0;n<nb;n++) for (n=0;n<nb;n++) refh[ilist[n]]=1;
		}
	    }
	}
    }

  
  mask->nelem=horiz->nelem=0;
  horiz->actif=0;

  for (i=nbrm=0;i<MAX_REF;i++) nbrm+=refm[i];
  for (i=nbrh=0;i<MAX_REF;i++) nbrh+=refh[i];
  
  if (nbrm || nbrh)
    {
      node=(int**)malloc(maillnodray->ndmat*sizeof(int*));
      for (i=0;i<maillnodray->ndmat;i++) *(node+i)=(int*)malloc(maillnodray->nelem*sizeof(int)); 
      verif_alloue_int2d(maillnodray->ndmat,"extraitmask",node);
      perfo.mem_ray+=maillnodray->nelem*maillnodray->ndmat*sizeof(int);

      nref=(int*)malloc(maillnodray->nelem*sizeof(int));
      verif_alloue_int1d("extraitmask",nref);
      for (i=0;i<maillnodray->nelem;i++) *(nref+i)=0;

      for (i=0,nbm=nbh=0;i<maillnodray->nelem;i++)
	{
	  nr=maillnodray->nrefe[i];	
	  if (refm[nr])  nbm+=1;
	  if (refh[nr])  nbh+=1;
	}
      if (nbm)
	{
	  mask->ndmat=maillnodray->ndmat;
	  mask->nelem=nbm;
	  mask->node=(int**)malloc(mask->ndmat*sizeof(int*));
	  for (i=0;i<mask->ndmat;i++) mask->node[i]=(int*)malloc(mask->nelem*sizeof(int)); 
	  verif_alloue_int2d(mask->ndmat,"extraitmask",mask->node);
	  mask->ref=(int*)malloc(mask->nelem*sizeof(int));
	  verif_alloue_int1d("extraitmask",mask->ref);
	  perfo.mem_ray+=mask->nelem*(mask->ndmat+1)*sizeof(int);
	}
      if (nbh)
	{
	  horiz->ndmat=maillnodray->ndmat;
	  horiz->nelem=nbh;
	  horiz->node=(int**)malloc(horiz->ndmat*sizeof(int*));
	  for (i=0;i<horiz->ndmat;i++) horiz->node[i]=(int*)malloc(horiz->nelem*sizeof(int)); 
	  verif_alloue_int2d(horiz->ndmat,"extraitmask",horiz->node);
	  horiz->ref=(int*)malloc(horiz->nelem*sizeof(int));
	  verif_alloue_int1d("extraitmask",horiz->ref);
	  perfo.mem_ray+=horiz->nelem*(horiz->ndmat+1)*sizeof(int);
	}

      for (i=0,nb1m=nb1h=nb2=0;i<maillnodray->nelem;i++)
	{
	  nr=maillnodray->nrefe[i];	
	  if (refm[nr])
	    {
	      for (j=0;j<mask->ndmat;j++) mask->node[j][nb1m]=maillnodray->node[j][i];
	      mask->ref[nb1m]=maillnodray->nrefe[i];
	      nb1m++;
	    }
	  else if  (refh[nr])
	    {
	      for (j=0;j<horiz->ndmat;j++) horiz->node[j][nb1h]=maillnodray->node[j][i];
	      horiz->ref[nb1h]=maillnodray->nrefe[i];
	      nb1h++;
	    }
	  else
	    {
	      for (j=0;j<maillnodray->ndmat;j++) node[j][nb2]=maillnodray->node[j][i];
	      nref[nb2]=maillnodray->nrefe[i];
	      nb2++;
	    }
	}
      for (j=0;j<maillnodray->ndmat;j++)
	for (i=0;i<maillnodray->nelem-mask->nelem-horiz->nelem;i++) maillnodray->node[j][i]=node[j][i];
      for (i=0;i<maillnodray->nelem-mask->nelem-horiz->nelem;i++) maillnodray->nrefe[i]=nref[i];
      maillnodray->nelem-=(mask->nelem+horiz->nelem);

      if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;

      for (i=0;i<maillnodray->ndmat;i++) free(node[i]);
      free(node); free(nref);
      perfo.mem_ray-=maillnodray->nelem*(maillnodray->ndmat+1)*sizeof(int);
      
      if (horiz->nelem)  horiz->actif=1;

      if (nbrh && !horiz->nelem)
	{
	  if (SYRTHES_LANG == FR)
	    {  
	      printf("\n Probleme de donnees :\n");
	      printf("   Vous avez indiquer des references de facettes de type 'horizon'\n");
	      printf("   Aucune facette du maillage ne porte ces references\n\n");
	    }
	  else if (SYRTHES_LANG == EN)
	    { 
	      printf("\n User data problem :\n");
	      printf("   User has indicated face references with 'horizon' type \n");
	      printf("   None of the mesh faces has these references\n\n");
	    }
	  syrthes_exit(1);
	}

      if (SYRTHES_LANG == FR)
	{
	  printf("\n *** Prise en compte de facettes particulieres\n");
	  printf("     Nombre de facettes 'masques'=%d\n",mask->nelem);
	  printf("     Nombre de facettes 'horizon'=%d\n",horiz->nelem);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n *** Taking into account particular faces\n");
	  printf("     Number of faces of type 'mask'=%d\n",mask->nelem);
	  printf("     Number of faces of type 'horizon'=%d\n",horiz->nelem);
	}
    }


}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void couplevitre(struct Vitre *vitre,struct Maillage maillnodray,
		 struct Compconnexe volconnexe)
{
  int i,n,nem,na,nb,nc,nume;
  double x1,y1,z1,x2,y2,z2,dm,dmax=0.,d,r1,r2,t,deno;
  double tiers=1./3.;
  int **nodray,*typfacray;
  double *sufray,**coordray;

  nodray=maillnodray.node;
  coordray=maillnodray.coord;
  sufray=maillnodray.volume;
  typfacray=maillnodray.type;

  vitre->voisin=(int*)malloc(maillnodray.nelem*sizeof(int));
  verif_alloue_int1d("couplevitre",vitre->voisin);
  for (i=0;i<maillnodray.nelem;i++) vitre->voisin[i]=-1;

  if (maillnodray.ndim==2)
    for (n=0;n<maillnodray.nelem;n++)
      {
	if (vitre->voisin[n]<0 && typfacray[n]==TYPRVITRE)
	  {
	    na=nodray[0][n]; nb=nodray[1][n];
	    x1=0.5*(coordray[0][na]+coordray[0][nb]); y1=0.5*(coordray[1][na]+coordray[1][nb]);
	    dm=1.e6; nem=0;
	    for (i=0;i<maillnodray.nelem;i++)
	      if (vitre->voisin[i]<0 && typfacray[i]==TYPRVITRE && i!=n)
		{
		  na=nodray[0][i]; nb=nodray[1][i];
		  x2=0.5*(coordray[0][na]+coordray[0][nb]); y2=0.5*(coordray[1][na]+coordray[1][nb]);
		  d=(x2-x1)*(x2-x1)+(y2-y1)*(y2-y1);
		  if (d<dm) {dm=d; nem=i;}
		}
	    if (dm>dmax) dmax=dm;
	    vitre->voisin[n]=nem; vitre->voisin[nem]=n;
	  }
      }
  else
    for (n=0;n<maillnodray.nelem;n++) 
      if (vitre->voisin[n] && typfacray[n]==TYPRVITRE)
	{
	  na=nodray[0][n]; nb=nodray[1][n]; nc=nodray[2][n];
	  x1=tiers*(coordray[0][na]+coordray[0][nb]+coordray[0][nc]); 
	  y1=tiers*(coordray[1][na]+coordray[1][nb]+coordray[1][nc]);
	  z1=tiers*(coordray[2][na]+coordray[2][nb]+coordray[2][nc]);
	  dm=1.e6; nem=0;
	  for (i=0;i<maillnodray.nelem;i++)
	    if (vitre->voisin[n] && typfacray[n]==TYPRVITRE)
	      {
		na=nodray[0][n]; nb=nodray[1][n];
		x2=tiers*(coordray[0][na]+coordray[0][nb]+coordray[0][nc]);
		y2=tiers*(coordray[1][na]+coordray[1][nb]+coordray[1][nc]);
		z2=tiers*(coordray[2][na]+coordray[2][nb]+coordray[2][nc]);
		d=(x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)+(z2-z1)*(z2-z1);
		if (d<dm) {dm=d; nem=i;}
	      }
	  if (dm>dmax) dmax=dm;
	  vitre->voisin[n]=nem; vitre->voisin[nem]=n;
	}

  if (SYRTHES_LANG == FR)
    printf("\n *** couplevitre : distance maximale pour les couples de facettes %e\n",sqrt(dmax));
  else if (SYRTHES_LANG == EN)
    printf("\n *** couplevitre : maximum distance for the couples of faces%e\n",sqrt(dmax));

  vitre->voisvol=(int*)malloc(maillnodray.nelem*sizeof(int));
  verif_alloue_int1d("couplevitre",vitre->voisvol);
  vitre->voisloc=(int*)malloc(maillnodray.nelem*sizeof(int));
  verif_alloue_int1d("couplevitre",vitre->voisloc);
  for (n=0;n<maillnodray.nelem;n++) {vitre->voisvol[n]=-1;vitre->voisloc[n]=-1;}

  for (i=0;i<maillnodray.nelem;i++)
    if (vitre->voisin[i]>-1)
      {
	vitre->voisvol[i]=n;
	vitre->voisloc[i]=i;
      }
  
}
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |        Lecture des donnees                                           |
  |======================================================================| */
void proprvitre(struct Vitre vitre,struct Maillage maillnodray,
		struct ProphyRay phyray)
{
  int i,j,n,nv,nb;
  double r1,r2,t,deno;
  int *ok;

  ok=(int*)malloc(maillnodray.nelem*sizeof(int));
  verif_alloue_int1d("proprvitre",ok);
  for (n=0;n<maillnodray.nelem;n++) ok[n]=0;
  perfo.mem_ray+=maillnodray.nelem*sizeof(int);




  for (n=0;n<maillnodray.nelem;n++)
    {
      if ((nv=vitre.voisin[n])>=0 && !ok[n])
	{
	  ok[n]=ok[nv]=1;
	  for (nb=0;nb<phyray.bandespec.nb;nb++)
	    {
	      r1=phyray.reflec[nb][n]; r2=phyray.reflec[nb][nv];
	      t=phyray.transm[nb][n]; /* rq : t2=t1 par def */
	      deno=(1-r1*r2*t*t);
	      phyray.reflec[nb][n]  =(r1+r2*(1-2*r1)*t*t)/deno;
	      phyray.reflec[nb][nv]=(r2+r1*(1-2*r2)*t*t)/deno;
	      phyray.transm[nb][n]=phyray.transm[nb][nv]=((1-r1)*(1-r2)*t)/deno;
	      phyray.absorb[nb][n]  =1.-phyray.reflec[nb][n]-phyray.transm[nb][n];
	      phyray.absorb[nb][nv]=1.-phyray.reflec[nb][nv]-phyray.transm[nb][nv];
	      phyray.emissi[nb][n]  =(1.-r1)*(1+r2*t)*(1.-t)/(1-r1*r2*t*t)*vitre.refrac[nb][n] ;
	      phyray.emissi[nb][nv]=(1.-r2)*(1+r1*t)*(1.-t)/(1-r2*r1*t*t)*vitre.refrac[nb][n] ;
	      if (SYRTHES_LANG == FR)
		printf("facette %d R=%f T=%f A=%f E=%f\n",n,phyray.reflec[nb][n],phyray.transm[nb][n],
		       phyray.absorb[nb][n],phyray.emissi[nb][n]);
	      else if (SYRTHES_LANG == EN)
		printf("face %d R=%f T=%f A=%f E=%f\n",n,phyray.reflec[nb][n],phyray.transm[nb][n],
		       phyray.absorb[nb][n],phyray.emissi[nb][n]);
	      printf("        %d R=%f T=%f A=%f E=%f\n",nv,phyray.reflec[nb][nv],phyray.transm[nb][nv],
		     phyray.absorb[nb][nv],phyray.emissi[nb][nv]);
	    }
	}
    }

  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  
  free(ok);
  perfo.mem_ray-=maillnodray.nelem*sizeof(int);
}


/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | smoogc                                                               |
  |                                                                      |
  |======================================================================| */
void smoogc_parall(int nelray,struct FacForme ***listefdf,
		   double *diagfdf,double *sufray,
		   struct SDparall_ray sdparall_ray)
{
#ifdef _SYRTHES_MPI_
  int nn=0,n=0,i,j,nitsmo=100;
  double *x,*b,*dd,*gd,*res,*z,*di,*trav;
  double x0,resnor,sl,rgrg,ro,prsca1,prsca2,f;
  double alp,epsis,zero=0.,epssmo=1.e-8,s,ss,*xx;
  struct FacForme *pff;

  x=(double*)malloc(nelray*sizeof(double));     verif_alloue_double1d("smoogc",x);
  b=(double*)malloc(nelray*sizeof(double));     verif_alloue_double1d("smoogc",b);
  dd=(double*)malloc(nelray*sizeof(double));    verif_alloue_double1d("smoogc",dd);
  gd=(double*)malloc(nelray*sizeof(double));    verif_alloue_double1d("smoogc",gd);
  res=(double*)malloc(nelray*sizeof(double));   verif_alloue_double1d("smoogc",res);
  z=(double*)malloc(nelray*sizeof(double));     verif_alloue_double1d("smoogc",z);
  di=(double*)malloc(nelray*sizeof(double));    verif_alloue_double1d("smoogc",di);
  trav=(double*)malloc(sdparall_ray.nelrayloc_max*sizeof(double)); verif_alloue_double1d("smoogc",trav);
 
  perfo.mem_ray+=nelray*8*sizeof(double);
  if (perfo.mem_ray>perfo.mem_ray_max) perfo.mem_ray_max=perfo.mem_ray;
  

  for (i=0;i<nelray;i++)
    {
      s=ss=0;
      for (j=0;j<sdparall_ray.npartsray;j++)
	{
	  pff=listefdf[j][i];
	  while(pff) {f=pff->fdf; 
	    s+=f; ss+=f*f; pff=pff->suivant;}
	}
      s+=diagfdf[i];
      ss+=2*diagfdf[i]*diagfdf[i];
      b[i]=sufray[i]-s;
      di[i]=ss;
      x[i]=0;
    }

  /* Norme du second membre */
  x0=1./sdparall_ray.nelraytot; epsis=1.e-6 * x0;
  for (i=0;i<nelray;i++) res[i] = -b[i];
  prosca_ray_parall(&prsca1,res,res,sdparall_ray);   resnor=sqrt(prsca1); 
/*   printf("smoogc_parall_ini : resnor=%25.18e \n",resnor); */


  if (resnor<=epsis && resnor<=epssmo*sqrt((double)(sdparall_ray.nelraytot)))
    {
      if (sdparall_ray.rang==0)
	{
	  if (SYRTHES_LANG == FR)
	    printf("\n *** smoogc_parall : ITERATION   PRECISION RELATIVE  PRECISION ABSOLUE\n");
	  else if (SYRTHES_LANG == EN)
	    printf("\n *** smoogc_parall : ITERATION   RELATIVE PRECISION  ABSOLUTE PRECISION\n");
	  printf("                %4d         %12.5e         %12.5e\n",
		 n,resnor/x0,resnor/sqrt((double)(sdparall_ray.nelraytot)));
	}
    }
  else
    {
      if (sdparall_ray.rang==0) 
	if (SYRTHES_LANG == FR)
	  printf("\n *** smoogc_parall : ITERATION   PRECISION RELATIVE  PRECISION ABSOLUE\n");
	else if (SYRTHES_LANG == EN)
	  printf("\n *** smoogc_parall : ITERATION   RELATIVE PRECISION  ABSOLUTE PRECISION\n");
      
      do
	{
	  nn++;
	  for (i=0;i<nelray;i++) gd[i]=res[i]/di[i];
	  prosca_ray_parall(&sl,res,gd,sdparall_ray);

	  if (nn==1) for (i=0;i<nelray;i++) dd[i]=gd[i];
	  else {alp=sl/rgrg; for (i=0;i<nelray;i++) dd[i]=gd[i]+dd[i]*alp;}
	  rgrg=sl;

	  for (i=0;i<nelray;i++) z[i]=0;

	  for (n=0;n<sdparall_ray.npartsray;n++)
	    {
	      if (n==sdparall_ray.rangray) /* sur le proc local */
		{
		  for (i=0;i<nelray;i++)
		    {
		      s=0;
		      pff=listefdf[n][i];
		      while(pff) {f=pff->fdf; s+=f*f*dd[pff->numenface]; pff=pff->suivant;}
		      s+=di[i]*dd[i];
		      z[i]+=s;
		    }
		}
	      else if (sdparall_ray.fdfproc[n]) /* si ce n'est pas le proc courant  et qu'il y a */
		/* des fdf a echanger avec ce proc               */
		{
		  for (i=0;i<nelray;i++) trav[i]=dd[i];
		  for (i=nelray;i<sdparall_ray.nelrayloc_max;i++) trav[i]=0.;
		  
		  MPI_Sendrecv_replace(trav,sdparall_ray.nelrayloc_max,MPI_DOUBLE,n,0,n,0,sdparall_ray.syrthes_comm_ray,&status);
		  
		  /* on fait la multiplication avec ce que l'on a recu */
		  for (i=0;i<nelray;i++)
		    {
		      s=0;
		      pff=listefdf[n][i];
		      while(pff) {f=pff->fdf; s+=f*f*trav[pff->numenface]; pff=pff->suivant;}
		      z[i]+=s;
		    }
		}
	    }

	  prosca_ray_parall(&prsca1,res,dd,sdparall_ray);
	  prosca_ray_parall(&prsca2,z,dd,sdparall_ray);

	  ro=- prsca1 / prsca2;
	  for (i=0;i<nelray;i++) x[i]+=ro*dd[i];
	  for (i=0;i<nelray;i++) res[i]+=ro*z[i];
	  prosca_ray_parall(&prsca1,res,res,sdparall_ray);
	  resnor=sqrt(prsca1);
	  if (affich.ray_fdf) 
	    if (sdparall_ray.rang==0) printf("           %4d         %12.5e                %12.5e\n",
		   nn,resnor/x0,resnor/sqrt((double)(sdparall_ray.nelraytot)));
	}
      while(!( (resnor<=epsis &&  resnor<=epssmo*sqrt((double)(sdparall_ray.nelraytot))) || nn>=nitsmo ));
  
      if (sdparall_ray.rang==0) 
	if (SYRTHES_LANG == FR)
	  printf("  %4d ITERATIONS  PRECISION RELATIVE=%12.5e PRECISION ABSOLUE=%12.5e\n",
		 nn,resnor/x0,resnor/sqrt((double)(sdparall_ray.nelraytot)));
	else if (SYRTHES_LANG == EN)
	  printf("  %4d ITERATIONS  RELATIVE PRECISION=%12.5e ABSOLUTE PRECISION=%12.5e\n",
		 nn,resnor/x0,resnor/sqrt((double)(sdparall_ray.nelraytot)));
    }

  /* Modification des coefs par multiplicateurs de lagrange */
  for (n=0;n<sdparall_ray.npartsray;n++)
    {
      if (n==sdparall_ray.rangray) /* sur le proc local */
	{
	  for (i=0;i<nelray;i++)
	    {
	      pff=listefdf[n][i];
	      while(pff) 
		{
		  pff->fdf= pff->fdf*(1. + (x[i]+x[pff->numenface])*pff->fdf ); /* parall on n'a pas le x d'en face */
		  pff=pff->suivant;
		} 
	      diagfdf[i]= diagfdf[i] * (1. + (x[i]+x[i])*diagfdf[i]);
	    }
	}
      else if (sdparall_ray.fdfproc[n]) /* si ce n'est pas le proc courant  et qu'il y a */
	                                /* des fdf a echanger avec ce proc               */
	{
	  for (i=0;i<nelray;i++) trav[i]=x[i];
	  for (i=nelray;i<sdparall_ray.nelrayloc_max;i++) trav[i]=0.;

	  MPI_Sendrecv_replace(trav,sdparall_ray.nelrayloc_max,MPI_DOUBLE,n,0,n,0,sdparall_ray.syrthes_comm_ray,&status);

	  /* on fait la multiplication avec ce que l'on a recu */
	  for (i=0;i<nelray;i++)
	    {
	      pff=listefdf[n][i];
	      while(pff) 
		{
		  pff->fdf= pff->fdf*(1. + (x[i]+trav[pff->numenface])*pff->fdf ); /* parall on n'a pas le x d'en face */
		  pff=pff->suivant;
		} 
	    }
	}
    }


  free(x);free(b);free(dd);free(gd);free(res);free(z);free(di); free(trav);
  perfo.mem_ray-=nelray*8*sizeof(double);

#endif

}

