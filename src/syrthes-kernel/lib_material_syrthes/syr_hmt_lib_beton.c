/*-----------------------------------------------------------------------

                         SYRTHES version 4.1
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

/*|======================================================================|
  | SYRTHES 4.1                                       COPYRIGHT EDF 2008 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |    Proprietes du beton                                               |
  |======================================================================| */

/* Initialisations des constantes */
double fmat_const_beton(struct ConstMateriaux *constmat,
			struct ConstPhyhmt constphyhmt)
{
  constmat->eps0=0.122;
  constmat->rhos=2200;
  constmat->cs=1000;
  constmat->xknv=0;
  constmat->xk=3.e-18;
/*   constmat->xk=3.e-19; commente pour test */
  constmat->taumax=122.;
  return 1;
}


/* taux d'humidite volumique */
/* ---------------------------------------------------------------------- */
double fmat_ftauv_beton(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double xpv,double psat,double t)
{
  double x,a,b,tauv;

  x=xpv/psat;

  if (x < 0.120e0)
    tauv=0.1237500000e3 * x;
  else if ( x < 0.230e0)
    tauv=0.1110818182e2 + 0.3118181818e2 * x;
  else if ( x < 0.330e0) 
    tauv=0.1565800000e2 + 0.1140000000e2 * x;
  else if (  x < 0.440e0)
    tauv=0.1600000000e2 + 0.1036363636e2 * x;
  else if (  x < 0.540e0)
    tauv=0.1048400000e2 + 0.2290000000e2 * x;
  else if (  x < 0.630e0)
    tauv=-0.4570000000e1 + 0.5077777778e2 * x;
  else if ( x < 0.710e0)
    tauv=0.4875000000e0 + 0.4275000000e2 * x;
  else if (  x < 0.8e0)
    tauv=-0.2327777778e2 + 0.7622222222e2 * x;
  else if (  x < 0.9e0)
    tauv=-0.4454000000e2 + 0.1028000000e3 * x;
  else if (  x < 0.97e0)
    tauv=-0.2899057143e3 + 0.3754285714e3 * x;
  else if (x<=1.)
    tauv=-0.1469333333e4 + 0.1591333333e4 * x;
  else
    {
/*       printf("error beton tauv : pv/psat>1 (= %f psat=%f)\n",x,psat); */
/*       exit(1); */
      tauv=constmat.taumax;
    }
  return tauv;
}

/* pente de l'isotherme de sorption */
/* ---------------------------------------------------------------------- */
double fmat_falpha_beton(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double pv,double psat,double t)
{
  double x,a,alpha;
  
  x=pv/psat;
  
  if (x < 0.230e0)
    alpha=0.2247334711e3 - 0.8415289256e3 * x;
  else if ( x < 0.330e0)
    alpha=0.7668000000e2 - 0.1978181818e3 * x;
  else if ( x < 0.440e0)
    alpha=0.1450909092e2 - 0.9421487636e1 * x;
  else if ( x < 0.540e0)
    alpha=-0.4479636366e2 + 0.1253636364e3 * x;
  else if ( x < 0.630e0)
    alpha=-0.1443666667e3 + 0.3097530864e3 * x;
  else if ( x < 0.710e0)
    alpha=0.1139965278e3 - 0.1003472222e3 * x;
  else if ( x < 0.8e0)
    alpha=-0.2213086420e3 + 0.3719135802e3 * x;
  else if ( x < 0.9e0)
    alpha=-0.1364000000e3 + 0.2657777778e3 * x;
  else if ( x < 0.97e0)
    alpha=-0.3402424490e4 + 0.3894693877e4 * x;
  else if (x<=1.)
    alpha=-0.3893882540e5 + 0.4053015873e5 * x;
  else
    {
/*       printf("error beton alfa : pv/psat>1 (= %f psat=%f)\n",x,psat); */
/*       exit(1); */
      alpha=-0.3893882540e5 + 0.4053015873e5 * 1.;
    }



  return alpha/psat;
}

/* permeabilite relative au gaz */
/* ---------------------------------------------------------------------- */
double fmat_fkrg_beton(struct ConstMateriaux constmat,double tauv)
{

  double x;
  x=tauv/constmat.taumax;
  
  return  sqrt(1.-x)*pow(1.-pow(x,2.2748),(2./2.2748));
}

/* permeabilite relative au liquide */
/* ---------------------------------------------------------------------- */
double fmat_fkrl_beton(struct ConstMateriaux constmat,double tauv)
{
  double x;
  x=tauv/constmat.taumax;

  return sqrt(x)*pow(1.-(pow(1.-pow(x,2.2748),(1/2.2748))),2);

}


/* conductivite thermique du materiau humide */
/* ---------------------------------------------------------------------- */
double fmat_fklambt_beton(double t,double tauv)
{
  /* Attention cette valeur semble varier suivant les sources */
/*   return 1.4; cp pour test*/
  return 2.3;
}


/* coefficient de diffusion de la vapeur dans le materiau */
/* ---------------------------------------------------------------------- */
  double fmat_fpiv_beton(double xpv, double psat, double t)
{
/*   return 6.3e-7; cp pour test*/
  return 8.5e-7;
}

/* Fonction utilisateur hm : chaleur latente complementaire */
/* ---------------------------------------------------------------------- */
double fmat_fhm_beton(double tauv)
{
  return 0.;
}

/* derivee par rapport a tauv de la fonction hm */
/* ---------------------------------------------------------------------- */
double fmat_fdhmdtauv_beton(double tauv)
{
  return 0.;
}

/* Fonction utilisateur fbetap */
/* ---------------------------------------------------------------------- */
double fmat_fbetap_beton(struct ConstPhyhmt constphyhmt,
			 struct ConstMateriaux constmat,
			 double xpv,double psat,double tauv,double t)
{
  return -fmat_falpha_beton(constphyhmt,constmat,xpv,psat,t)*xpv
    *(fphyhmt_fxl(constphyhmt,t)+fmat_fhm_beton(tauv))/constphyhmt.Rv/(t*t);
}

/* fonction supplementaire dhp - betap * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdhp_beton(double betap, double tauv)
{
  return 0.;
}

/* fonction supplementaire dht - alpha * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdht_beton(double alphat, double tauv)
{
  return 0.;
}


