/*-----------------------------------------------------------------------

                         SYRTHES version 4.0
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

/*|======================================================================|
  | SYRTHES 4.0                                       COPYRIGHT EDF 2008 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |    Proprietes du acier                                               |
  |    Nouvelle modelisation acier pour 4.0                              |
  |======================================================================| */

/* Initialisations des constantes */
double fmat_const_acier(struct ConstMateriaux *constmat,
			struct ConstPhyhmt constphyhmt)
{
  constmat->eps0=0;
  constmat->rhos=7800;
  constmat->cs=450;
  constmat->xknv=0;
  constmat->xk=0.;
  constmat->taumax=0;
  return 1;
}


/* taux d'humidite volumique */
/* ---------------------------------------------------------------------- */
double fmat_ftauv_acier(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double xpv,double psat,double t)
{
  double x,a,b,tauv;

  x=xpv/psat;
  /* Nouvelle modelisation 4.0 - essai */
  tauv=0.;

  return tauv;
}

/* pente de l'isotherme de sorption */
/* ---------------------------------------------------------------------- */
double fmat_falpha_acier(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double pv,double psat,double t)
{
  double x,a,alpha;

  x=pv/psat;

  /* Nouvelle modelisation 4.0 - essai */
  alpha=0.;
  return alpha/psat;
}

/* permeabilite relative au gaz */
/* ---------------------------------------------------------------------- */
double fmat_fkrg_acier(struct ConstMateriaux constmat,double tauv)
{
  /* chris : est-ce que ce ne serait pas plus intelligent de mettre return 0 */
  /* return 1-tauv/constmat.taumax; */
  return 0.;
}

/* permeabilite relative au liquide */
/* ---------------------------------------------------------------------- */
double fmat_fkrl_acier(struct ConstMateriaux constmat,double tauv)
{

  /* Nouvelle modelisation 4.0 - essai */
  return 0;
}


/* conductivite thermique du materiau humide */
/* ---------------------------------------------------------------------- */
double fmat_fklambt_acier(double t,double tauv)
{
  return 50;
}


/* coefficient de diffusion de la vapeur dans le materiau */
/* ---------------------------------------------------------------------- */
double fmat_fpiv_acier(double xpv,double psat, double t)
{
  /* Nouvelle modelisation 4.0 - essai */
  return 0.;
}

/* Fonction utilisateur hm : chaleur latente complementaire */
/* ---------------------------------------------------------------------- */
double fmat_fhm_acier(double tauv)
{
  return 0.;
}

/* derivee par rapport a tauv de la fonction hm */
/* ---------------------------------------------------------------------- */
double fmat_fdhmdtauv_acier(double tauv)
{
  return 0.;
}

/* Fonction utilisateur fbetap */
/* ---------------------------------------------------------------------- */
double fmat_fbetap_acier(struct ConstPhyhmt constphyhmt,
			 struct ConstMateriaux constmat,
			 double xpv,double psat,double tauv,double t)
{
  /* Nouvelle modelisation 4.0 - essai */
  return 0.;
}

/* fonction supplementaire dhp - betap * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdhp_acier(double betap, double tauv)
{
  return 0.;
}

/* fonction supplementaire dht - alpha * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdht_acier(double alphat, double tauv)
{
  return 0.;
}


