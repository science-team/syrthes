/*-----------------------------------------------------------------------

                         SYRTHES version 4.1
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

/*|======================================================================|
  | SYRTHES 4.1                                       COPYRIGHT EDF 2008 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |    Proprietes du pse_normal                                               |
  |======================================================================| */

/* Initialisations des constantes */
double fmat_const_pse_normal(struct ConstMateriaux *constmat,
			     struct ConstPhyhmt constphyhmt)
{
  constmat->eps0   = 0.98;
  constmat->rhos   = 18.71;
  constmat->cs     = 1450;
  constmat->xknv   = 0;
  constmat->xk     = 3.e-16;
  constmat->taumax = 980.;
  return 1;
}


/* taux d'humidite volumique */
/* ---------------------------------------------------------------------- */
double fmat_ftauv_pse_normal(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double xpv,double psat,double t)
{
  double x,tauv;

  x=xpv/psat;

  if (x < 0.990) 
    tauv=0.0;
  else if ( x < 0.992) 
    tauv= -0.3880800000e4 + 0.3920000000e4 * x;
  else if ( x < 0.994)
    tauv= -0.2720272000e5 + 0.2743000000e5 * x;
  else if ( x < 0.996)
    tauv=  -0.7413940000e5 + 0.7465000000e5 * x;
  else if ( x < 0.998) 
    tauv=  -0.1442080000e6 + 0.1450000000e6 * x;
  else if (x<=1.)
    tauv=  -0.2380200000e6 + 0.2390000000e6 * x;
  else
    {
/*       printf("error pse_normal : pv/psat>1 (= %f taumax=%f)\n",x,constmat.taumax); */
/*       exit(1); */
      tauv=constmat.taumax;
    }
  return tauv;
}

/* pente de l'isotherme de sorption */
/* ---------------------------------------------------------------------- */
double fmat_falpha_pse_normal(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double pv,double psat,double t)
{
  double x,alpha;

  x=pv/psat;
  if (x < 0.990e0)
   alpha=0.0e0;
  else if ( x < 0.992e0)
    alpha= -0.1940400000e7 + 0.1960000000e7 * x;
  else if ( x < 0.994e0)
    alpha= -0.1165704000e8 + 0.1175500000e8 * x;
  else if ( x < 0.996e0)
    alpha= -0.2344091000e8 + 0.2361000000e8 * x;
  else if ( x < 0.998e0)
    alpha= -0.3495965000e8 + 0.3517500000e8 * x;
  else if ( x<=1.)
    alpha=-0.4676100000e8 + 0.4700000000e8 * x;
  else
    {
/*          printf("error pse_normal  alfa : pv/psat>1 =%f psat=%f \n",x,psat); */
/* 	 exit(1); */
      alpha=-0.4676100000e8 + 0.4700000000e8 * 1.;
    }
  return alpha/psat;

}

/* permeabilite relative au gaz */
/* ---------------------------------------------------------------------- */
double fmat_fkrg_pse_normal(struct ConstMateriaux constmat,double tauv)
{
  return 1-tauv/constmat.taumax;
}

/* permeabilite relative au liquide */
/* ---------------------------------------------------------------------- */
double fmat_fkrl_pse_normal(struct ConstMateriaux constmat,double tauv)
{
  return 0.;
}


/* conductivite thermique du materiau humide */
/* ---------------------------------------------------------------------- */
double fmat_fklambt_pse_normal(double t,double tauv)
{
  if(tauv < 0.22e0)
    return 0.3850000000e-1 + 0.1363636364e-2 * tauv;
  else if ( tauv < 0.24e0)
    return 0.3550000000e-1 + 0.1500000000e-1 * tauv;
  else if ( tauv < 0.15e1)
    return 0.3894761905e-1 + 0.6349206349e-3 * tauv;
  else if( tauv < 0.56e1)
    return 0.3894878049e-1 + 0.6341463415e-3 * tauv;
  else if ( tauv < 0.98e1)
    return 0.4170000000e-1 + 0.1428571429e-3 * tauv;
  else if ( tauv < 0.295e2)
    return 0.4225431472e-1 + 0.8629441624e-4 * tauv;
  else if ( tauv < 0.342e2) 
    return 0.4291702128e-1 + 0.6382978723e-4 * tauv;
  else if ( tauv < 0.369e2)
    return 0.3750000000e-1 + 0.2222222222e-3 * tauv;
  else
    return 0.4450967742e-1 + 0.3225806452e-4 * tauv;
}


/* coefficient de diffusion de la vapeur dans le materiau */
/* ---------------------------------------------------------------------- */
double fmat_fpiv_pse_normal(double xpv,double psat,double t)
{
  if (t < 0.28315e3)
    return 0.1000000000e-5;
  else if (t < 0.33315e3)
    return -0.5965490000e-5 + 0.2460000000e-7 * t;
  else
    return 0.2230000000e-5;
}

/* Fonction utilisateur hm : chaleur latente complementaire */
/* ---------------------------------------------------------------------- */
double fmat_fhm_pse_normal(double tauv)
{
  return 0.;
}

/* derivee par rapport a tauv de la fonction hm */
/* ---------------------------------------------------------------------- */
double fmat_fdhmdtauv_pse_normal(double tauv)
{
  return 0.;
}

/* Fonction utilisateur fbetap */
/* ---------------------------------------------------------------------- */
double fmat_fbetap_pse_normal(struct ConstPhyhmt constphyhmt,
			 struct ConstMateriaux constmat,
			 double xpv,double psat,double tauv,double t)
{
  return -fmat_falpha_pse_normal(constphyhmt,constmat,xpv,psat,t)*xpv
    *(fphyhmt_fxl(constphyhmt,t)+fmat_fhm_pse_normal(tauv))/constphyhmt.Rv/(t*t);
}

/* fonction supplementaire dhp - betap * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdhp_pse_normal(double betap, double tauv)
{
  return 0.;
}

/* fonction supplementaire dht - alpha * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdht_pse_normal(double alphat, double tauv)
{
  return 0.;
}


