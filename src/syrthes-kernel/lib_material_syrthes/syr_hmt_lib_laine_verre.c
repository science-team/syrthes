/*-----------------------------------------------------------------------

                         SYRTHES version 4.1
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

/*|======================================================================|
  | SYRTHES 4.1                                       COPYRIGHT EDF 2008 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |    Proprietes du laine_verre                                               |
  |======================================================================| */

/* Initialisations des constantes */
double fmat_const_laine_verre(struct ConstMateriaux *constmat,
			struct ConstPhyhmt constphyhmt)
{
  constmat->eps0=0.990;
  constmat->rhos=18;
  constmat->cs=1030;
  constmat->xknv=0;
  constmat->xk=3.5e-11;
  constmat->taumax=990;
  return 1;
}


/* taux d'humidite volumique */
/* ---------------------------------------------------------------------- */
double fmat_ftauv_laine_verre(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double xpv,double psat,double t)
{
  double x,tauv;

  x=xpv/psat;
  if (x < 0.204e0)
    tauv=0.7647058824e0 * x;
  else if (x < 0.434e0)
    tauv=0.9639652174e-1 + 0.2921739130e0 * x;
  else if ( x < 0.645e0)
    tauv=0.1491526066e0 + 0.1706161137e0 * x;
  else if ( x < 0.849e0)
    tauv=0.9415588235e-1 + 0.2558823529e0 * x;
  else if ( x < 0.951e0)
    tauv=0.8666470588e-1 + 0.2647058824e0 * x;
  else if ( x < 0.97e0)
    tauv=-0.4832501053e3 + 0.5085052632e3 * x;
  else if ( x < 0.98e0)
    tauv=-0.9600000000e3 + 0.1000000000e4 * x;
  else if ( x < 0.99e0)
    tauv=-0.7820000000e4 + 0.8000000000e4 * x;
  else if ( x < 0.995e0)
    tauv=-0.2564000000e5 + 0.2600000000e5 * x;
  else if ( x < 0.9975e0)
    tauv=-0.6743000000e5 + 0.6800000000e5 * x;
  else if ( x < 0.999e0)
    tauv=-0.1326000000e6 + 0.1333333333e6 * x;
  else if (x<=1.)
    tauv=-0.3890100000e6 + 0.3900000000e6 * x;
  else
    {
/*       printf("error laine_verre tauv : pv/psat>1 (= %f psat=%f)\n",x,psat); */
/*       exit(1); */
      tauv=constmat.taumax;
    }
  return tauv;

}

/* pente de l'isotherme de sorption */
/* ---------------------------------------------------------------------- */
double fmat_falpha_laine_verre(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double pv,double psat,double t)
{
  double x,alpha;
  
  x=pv/psat;
  if (x < 0.434e0)
    alpha=0.1183821195e1 - 0.2054486823e1 * x;
  else if ( x < 0.645e0)
    alpha=0.5422027517e0 - 0.5761033142e0 * x;
  else if ( x < 0.849e0)
    alpha=-0.9897567206e-1 + 0.4179717608e0 * x;
  else if ( x < 0.951e0)
    alpha= 0.1824394451e0 + 0.8650519118e-1 * x;
  else if ( x < 0.97e0)
    alpha=-0.2543851266e5 + 0.2674950302e5 * x;
  else if ( x < 0.98e0)
    alpha=-0.4716648421e5 + 0.4914947368e5 * x;
  else if ( x < 0.99e0)
    alpha=-0.6850000000e6 + 0.7000000000e6 * x;
  else if ( x < 0.995e0)
    alpha=-0.3556000000e7 + 0.3600000000e7 * x;
  else if ( x < 0.9975e0)
    alpha=-0.1669000000e8 + 0.1680000000e8 * x;
  else if ( x < 0.999e0)
    alpha=-0.4337866667e8 + 0.4355555553e8 * x;
  else if (x<=1.)
    alpha=-0.2562766667e9 + 0.2566666667e9 * x;
  else
    {
/*       printf("error laine_verre alfa : pv/psat>1 (= %f psat=%f)\n",x,psat); */
/*       exit(1); */
      alpha=-0.2562766667e9 + 0.2566666667e9 * 1.;
    }

  return alpha/psat;
}

/* permeabilite relative au gaz */
/* ---------------------------------------------------------------------- */
double fmat_fkrg_laine_verre(struct ConstMateriaux constmat,double tauv)
{
  return 1-tauv/constmat.taumax;
}

/* permeabilite relative au liquide */
/* ---------------------------------------------------------------------- */
double fmat_fkrl_laine_verre(struct ConstMateriaux constmat,double tauv)
{
  double x,krl;
  x=tauv/constmat.taumax;

  krl=0.;
  return krl;
}


/* conductivite thermique du materiau humide */
/* ---------------------------------------------------------------------- */
double fmat_fklambt_laine_verre(double t,double tauv)
{
  return 0.038;
}


/* coefficient de diffusion de la vapeur dans le materiau */
/* ---------------------------------------------------------------------- */
double fmat_fpiv_laine_verre(double xpv,double psat,double t)
{
  return 1.69e-5;
}

/* Fonction utilisateur hm : chaleur latente complementaire */
/* ---------------------------------------------------------------------- */
double fmat_fhm_laine_verre(double tauv)
{
  return 0.;
}

/* derivee par rapport a tauv de la fonction hm */
/* ---------------------------------------------------------------------- */
double fmat_fdhmdtauv_laine_verre(double tauv)
{
  return 0.;
}

/* Fonction utilisateur fbetap */
/* ---------------------------------------------------------------------- */
double fmat_fbetap_laine_verre(struct ConstPhyhmt constphyhmt,
			 struct ConstMateriaux constmat,
			 double xpv,double psat,double tauv,double t)
{
  return -fmat_falpha_laine_verre(constphyhmt,constmat,xpv,psat,t)*xpv
    *(fphyhmt_fxl(constphyhmt,t)+fmat_fhm_laine_verre(tauv))/constphyhmt.Rv/(t*t);
}

/* fonction supplementaire dhp - betap * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdhp_laine_verre(double betap, double tauv)
{
  return 0.;
}

/* fonction supplementaire dht - alpha * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdht_laine_verre(double alphat, double tauv)
{
  return 0.;
}


