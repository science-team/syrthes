/*-----------------------------------------------------------------------

                         SYRTHES version 4.1
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

/*|======================================================================|
  | SYRTHES 4.1                                       COPYRIGHT EDF 2008 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |    prototype des fonction pour les proprietes du bois_agglo               |
  |======================================================================| */

double fmat_const_bois_agglo(struct ConstMateriaux*,struct ConstPhyhmt);
double fmat_ftauv_bois_agglo(struct ConstPhyhmt,struct ConstMateriaux,double,double,double);
double fmat_falpha_bois_agglo(struct ConstPhyhmt,struct ConstMateriaux,double,double,double);
double fmat_fkrg_bois_agglo(struct ConstMateriaux,double);
double fmat_fkrl_bois_agglo(struct ConstMateriaux,double);
double fmat_fklambt_bois_agglo(double,double);
double fmat_fpiv_bois_agglo(double,double,double);
double fmat_fhm_bois_agglo(double);
double fmat_fdhmdtauv_bois_agglo(double);
double fmat_fbetap_bois_agglo(struct ConstPhyhmt,struct ConstMateriaux,
			 double,double,double,double);
double fmat_fdhp_bois_agglo(double,double);
double fmat_fdht_bois_agglo(double,double);


