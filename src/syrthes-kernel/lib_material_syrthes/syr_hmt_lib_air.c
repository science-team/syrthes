/*-----------------------------------------------------------------------

                         SYRTHES version 4.1
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

/*|======================================================================|
  | SYRTHES 4.1                                       COPYRIGHT EDF 2008 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |    Proprietes de l'air                                               |
  |======================================================================| */

/* Initialisations des constantes */
double fmat_const_air(struct ConstMateriaux *constmat,
			struct ConstPhyhmt constphyhmt)
{
  constmat->eps0=1.0;
  constmat->rhos=1.2;
  constmat->cs=1006.;
  constmat->xknv=0.;
  constmat->xk=1.e-10;
/*   constmat->xk=3.e-19; commente pour test */
  constmat->taumax=1000.;
  return 1;
}


/* taux d'humidite volumique */
/* ---------------------------------------------------------------------- */
double fmat_ftauv_air(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double xpv,double psat,double t)
{
  double x,a,b,tauv;

  x=xpv/psat;

  tauv = 0.;

  return tauv;
}

/* pente de l'isotherme de sorption */
/* ---------------------------------------------------------------------- */
double fmat_falpha_air(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double pv,double psat,double t)
{
  double x,a,alpha;
  
  x=pv/psat;
  
  alpha = 0.;

  return alpha/psat;
}

/* permeabilite relative au gaz */
/* ---------------------------------------------------------------------- */
double fmat_fkrg_air(struct ConstMateriaux constmat,double tauv)
{

  double x;
  x=tauv/constmat.taumax;
  
  return  1;
}

/* permeabilite relative au liquide */
/* ---------------------------------------------------------------------- */
double fmat_fkrl_air(struct ConstMateriaux constmat,double tauv)
{
  double x;
  x=tauv/constmat.taumax;

  return 0;

}


/* conductivite thermique du materiau humide */
/* ---------------------------------------------------------------------- */
double fmat_fklambt_air(double t,double tauv)
{

  return 0.025;
}


/* coefficient de diffusion de la vapeur dans le materiau */
/* ---------------------------------------------------------------------- */
  double fmat_fpiv_air(double xpv, double psat, double t)
{
  return 1.9e-12*pow((t+273.15),0.8)*101300.;
}

/* Fonction utilisateur hm : chaleur latente complementaire */
/* ---------------------------------------------------------------------- */
double fmat_fhm_air(double tauv)
{
  return 0.;
}

/* derivee par rapport a tauv de la fonction hm */
/* ---------------------------------------------------------------------- */
double fmat_fdhmdtauv_air(double tauv)
{
  return 0.;
}

/* Fonction utilisateur fbetap */
/* ---------------------------------------------------------------------- */
double fmat_fbetap_air(struct ConstPhyhmt constphyhmt,
			 struct ConstMateriaux constmat,
			 double xpv,double psat,double tauv,double t)
{
  return -fmat_falpha_air(constphyhmt,constmat,xpv,psat,t)*xpv
    *(fphyhmt_fxl(constphyhmt,t)+fmat_fhm_air(tauv))/constphyhmt.Rv/(t*t);
}

/* fonction supplementaire dhp - betap * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdhp_air(double betap, double tauv)
{
  return 0.;
}

/* fonction supplementaire dht - alpha * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdht_air(double alphat, double tauv)
{
  return 0.;
}


