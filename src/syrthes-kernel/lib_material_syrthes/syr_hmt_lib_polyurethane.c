/*-----------------------------------------------------------------------

                         SYRTHES version 4.1
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the Code_Saturne Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

/*|======================================================================|
  | SYRTHES 4.1                                       COPYRIGHT EDF 2008 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |    Proprietes du polyurethane                                               |
  |======================================================================| */

/* Initialisations des constantes */
double fmat_const_polyurethane(struct ConstMateriaux *constmat,
			struct ConstPhyhmt constphyhmt)
{
  constmat->eps0=0.95;
  constmat->rhos=30;
  constmat->cs=1400;
  constmat->xknv=0;
  constmat->xk=3.e-16;
  constmat->taumax=950.;
  return 1;
}


/* taux d'humidite volumique */
/* ---------------------------------------------------------------------- */
double fmat_ftauv_polyurethane(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double xpv,double psat,double t)
{
  double x,tauv;
  x=xpv/psat;

  if (x < 0.202e0)
    tauv=0.7549504950e1 * x;
  else if ( x < 0.434e0)
    tauv=0.1459698276e1 + 0.3232758621e0 * x;
  else if ( x < 0.650e0)
    tauv=0.6456018519e0 + 0.2199074074e1 * x;
  else if ( x < 0.852e0)
    tauv=0.6386138614e-1 + 0.3094059406e1 * x;
  else if ( x < 0.977e0)
    tauv=0.3144000000e0 + 0.2800000000e1 * x;
  else if (x<=1.)
    tauv=-0.4022173913e5 + 0.4117173913e5 * x;
  else
    {
/*       printf("error polyurethane tauv : pv/psat>1 (= %f psat=%f)\n",x,psat); */
/*       exit(1); */
      tauv=constmat.taumax;
    }
  return tauv;
}

/* pente de l'isotherme de sorption */
/* ---------------------------------------------------------------------- */
double fmat_falpha_polyurethane(struct ConstPhyhmt constphyhmt,struct ConstMateriaux constmat,
			double pv,double psat,double t)
{
  double x,alpha;
  x=pv/psat;

  if (x < 0.434e0)
    alpha=0.1384130786e2 - 0.3114753917e2 * x;
  else if ( x < 0.650e0)
    alpha=-0.3445689063e1 + 0.8684250981e1 * x;
  else if ( x < 0.852e0)
    alpha=-0.6808292228e0 + 0.4430620455e1 * x;
  else if ( x < 0.977e0)
    alpha=-0.1717631680e1 + 0.5647524752e1 * x;
  else if (x<=1.)
    alpha=-0.1748738658e7 + 0.1789910397e7 * x;
  else
    {
/*       printf("error polyurethane alfa : pv/psat>1 (= %f psat=%f)\n",x,psat); */
/*       exit(1); */
      alpha=-0.1748738658e7 + 0.1789910397e7 * 1.;
    }

  return alpha/psat;  
}

/* permeabilite relative au gaz */
/* ---------------------------------------------------------------------- */
double fmat_fkrg_polyurethane(struct ConstMateriaux constmat,double tauv)
{
  return 1-tauv/constmat.taumax;
}

/* permeabilite relative au liquide */
/* ---------------------------------------------------------------------- */
double fmat_fkrl_polyurethane(struct ConstMateriaux constmat,double tauv)
{
  double x;
  x=tauv/constmat.taumax;

  return 0;
}


/* conductivite thermique du materiau humide */
/* ---------------------------------------------------------------------- */
double fmat_fklambt_polyurethane(double t,double tauv)
{
  return 0.025;
}


/* coefficient de diffusion de la vapeur dans le materiau */
/* ---------------------------------------------------------------------- */
double fmat_fpiv_polyurethane(double xpv,double psat,double t)
{
  return 2.82e-7;
}

/* Fonction utilisateur hm : chaleur latente complementaire */
/* ---------------------------------------------------------------------- */
double fmat_fhm_polyurethane(double tauv)
{
  return 0.;
}

/* derivee par rapport a tauv de la fonction hm */
/* ---------------------------------------------------------------------- */
double fmat_fdhmdtauv_polyurethane(double tauv)
{
  return 0.;
}

/* Fonction utilisateur fbetap */
/* ---------------------------------------------------------------------- */
double fmat_fbetap_polyurethane(struct ConstPhyhmt constphyhmt,
			 struct ConstMateriaux constmat,
			 double xpv,double psat,double tauv,double t)
{
  return -fmat_falpha_polyurethane(constphyhmt,constmat,xpv,psat,t)*xpv
    *(fphyhmt_fxl(constphyhmt,t)+fmat_fhm_polyurethane(tauv))/constphyhmt.Rv/(t*t);
}

/* fonction supplementaire dhp - betap * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdhp_polyurethane(double betap, double tauv)
{
  return 0.;
}

/* fonction supplementaire dht - alpha * d hm/d tauv */
/* ---------------------------------------------------------------------- */
double fmat_fdht_polyurethane(double alphat, double tauv)
{
  return 0.;
}


