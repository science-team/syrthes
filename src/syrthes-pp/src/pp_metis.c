/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include "pp_usertype.h"
# include "pp_bd.h"
# include "pp_proto.h"

#ifdef _METIS_
# include <metis.h>
static void extrarete_metis(rp_int,rp_int,rp_int,rp_int**,idx_t**,idx_t**);
#endif

/*|======================================================================|
  | SYRTHES PARALLELE                                  JANV 07           |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | METIS  (Decomposition d'un maillage 2D/3D) :                         |
  |                                                                      |
  |    Creation du partitionnement                                       |
  |                                                                      |
  |======================================================================|*/

void pp_metis(struct Maillage maillnodes,rp_int nparts,rp_int *numdome)
{
  /*|=======================================================================|
    |   Nom    | Type | Mode |             Role                             |
    |=======================================================================|
    | nelems   |  e   |  d   | nombre d'elements du maillage                |
    | npoins   |  e   |  d   | nombre de noeuds du maillage                 |
    | ndiele   |  e   |  d   | dimension des elements du probleme           |
    | nparts   |  e   |  d   | nombre de partitions reelles                 |
    | numdome  |  te  |  r   | attribution d'une partition a chaque element |
    | nodes    |  te  |  d   | connectivite globale                         |
    |=======================================================================| 

    Type : e (entier), r (reel), t (tableau)
    Mode : d (donnee non modifiee), r (resultat), m (donnee modifiee)      
    Rq : on suppose que le maillage d'entree est P1                         */
        
#ifdef _METIS_
  
  /* variables : 
     ----------- */
  rp_int i,j,nn,nbno,nbnosom,nb_edgecut,nbface;
  rp_int wgtflag=0,numflag=0;
  idx_t ncon = 1;
  idx_t edgecut;
  idx_t *xadj,*adjncy,*part;
  rp_int options[5]={0,0,0,0,0};
  rp_int **nvoisin;

  if (SYRTHES_LANG == FR) 
    printf("\n *** PEPARATION DES DONNEES POUR LE PARTITIONNEMENT...\n");
  else if (SYRTHES_LANG == EN)
    printf("\n *** PREPARING DATA FOR MESH PARTITIONING...\n");


  nbno=maillnodes.ndiele+1;  /* nbre de noeuds P1 par elements */
  part=(idx_t *)malloc(maillnodes.nelem * sizeof(idx_t));


  /* Construction table de voisins (pour graphe d'elements) */
  /* ----------------------------------------------------- */
  if (maillnodes.ndim==2) nbface=3;
  else nbface=4;

  nvoisin=(rp_int**)malloc(nbface*sizeof(rp_int*));
  for (i=0; i<nbface; i++) nvoisin[i]=(rp_int*)malloc(maillnodes.nelem*sizeof(rp_int));
  verif_alloue_int2d(nbface,"pp_metis",nvoisin);

  if (maillnodes.ndim==2)  extrvois2(maillnodes,nvoisin);
  else                     extrvois3(maillnodes,nvoisin);


  /* Construction table de voisins (pour graphe d'elements) */
  /* ----------------------------------------------------- */
  extrarete_metis(maillnodes.nelem,nbface,maillnodes.ndim,nvoisin,&xadj,&adjncy);
  
  for (i=0; i<nbface; i++) free(nvoisin[i]);
  free(nvoisin);

  fflush(stdout);

  if (SYRTHES_LANG == FR) {
    printf("\n *** METIS : PARTITIONNEMENT DU MAILLAGE POUR LE PARALLELISME\n");
    printf("             nombre de noeuds %d\n",maillnodes.npoin);
    printf("             nombre d'elements %d\n",maillnodes.nelem);
  }
  else if (SYRTHES_LANG == EN) {
    printf("\n *** METIS : MESH PARTITIONING\n");
    printf("             number of nodes %d\n",maillnodes.npoin);
    printf("             number of elements %d\n",maillnodes.nelem);
  }

  if (nparts<9)
    METIS_PartGraphRecursive(&(maillnodes.nelem),&ncon,xadj,adjncy,NULL,NULL,NULL,&nparts,
			     NULL,NULL,NULL,&nb_edgecut,part);
  else
    METIS_PartGraphKway(&(maillnodes.nelem),&ncon,xadj,adjncy,NULL,NULL,NULL,&nparts,
                        NULL,NULL,NULL,&nb_edgecut,part);

  if (SYRTHES_LANG == FR) {
    printf("             partionnement termine\n");
  }
  else if (SYRTHES_LANG == EN) {
    printf("             end of partitioning\n");
  }

  for (i=0;i<maillnodes.nelem;i++) numdome[i]=(rp_int)part[i];

  free(xadj); free(adjncy); free(part);


/*   printf("             Impression des partitions\n"); */
/*   for (i=0;i<maillnodes.nelem;i++) */
/*     printf("             elt %d  --> partition %d\n",i,numdome[i]); */
  
#endif
}  


#ifdef _METIS_
/*|======================================================================|
  | SYRTHES 4.3                                       COPYRIGHT EDF 2009 |
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL,                                     |
  |======================================================================|
  | Extraction de la table des aretes du graphe des elements             |
  | Dans cette version, on stocke les aretes AB et BA                    |
  |======================================================================| */
static void extrarete_metis(rp_int nelems,rp_int nbface,rp_int ndim,rp_int **voisin,
			    idx_t **xadj,idx_t **adjncy)
{
struct BoutArete
{
  rp_int num;
  struct BoutArete *suivant;
};

 rp_int n,n1,n2,m,nn,pastrouve,nba,pos;
 rp_int nbareparele,nbarete;
 struct BoutArete **tabarete,*p,*pp,*q;
 
 tabarete=(struct BoutArete**)malloc(nelems*sizeof(struct BoutArete*));
 for (n=0;n<nelems;n++) tabarete[n]=NULL;
 
 for (nbarete=n=0;n<nelems;n++)
   {
     for (m=0;m<nbface;m++)
       {
	 n1 = n;
	 n2 = voisin[m][n];
	 if (n2>=0)
	   {
	 
	     if (!tabarete[n1])
	       {
		 q=(struct BoutArete*)malloc(sizeof(struct BoutArete));
		 q->num=n2;
		 q->suivant=NULL;
		 tabarete[n1]=q;
		 nbarete++;
	       }
	     else
	       {
		 pastrouve=1;
		 p=pp=tabarete[n1];
		 while(p && pastrouve)
		   {
		     if (p->num==n2) {pastrouve=0; break;}
		     pp=p;
		     p=p->suivant;
		   }
		 if (pastrouve)
		   {
		     q=(struct BoutArete*)malloc(sizeof(struct BoutArete));
		     q->num=n2;
		     q->suivant=NULL;
		     pp->suivant=q;
		     nbarete++;
		   }
	       }
	     /* on recommence avec n2 */
	     if (!tabarete[n2])
	       {
		 q=(struct BoutArete*)malloc(sizeof(struct BoutArete));
		 q->num=n1;
		 q->suivant=NULL;
		 tabarete[n2]=q;
		 nbarete++;
	       }
	     else
	       {
		 pastrouve=1;
		 p=pp=tabarete[n2];
		 while(p && pastrouve)
		   {
		     if (p->num==n1) {pastrouve=0; break;}
		     pp=p;
		     p=p->suivant;
		   }
		 if (pastrouve)
		   {
		     q=(struct BoutArete*)malloc(sizeof(struct BoutArete));
		     q->num=n1;
		     q->suivant=NULL;
		     pp->suivant=q;
		     nbarete++;
		   }
	       }
	   } /* fin de n2>=0 */
       }
   }
 
 /* stockage CSR de la table d'aretes */
 
 *xadj=(idx_t *)malloc((nelems+1) * sizeof(idx_t));
 *adjncy=(idx_t *)malloc(nbarete * sizeof(idx_t));
 
 
 for ((*xadj)[0]=pos=n=0;n<nelems;n++){
   nba=0;
   if (tabarete[n])
     {
       p=tabarete[n];
       while(p){
	 (*adjncy)[pos+nba]=p->num;
	 p=p->suivant;
	 nba++;
       }
     }
   pos+=nba;
   (*xadj)[n+1]=pos;
 }
 
 
 /* destruction des listes rp_intermediaires */
 for (n=0;n<nelems;n++)
   if (tabarete[n]){
     p=tabarete[n];
     while(p){q=p->suivant; free(p); p=q;}
   }
 free(tabarete);
 
 /* impression de controle */
 if (SYRTHES_LANG == FR)
   printf("\n *** GRAPHE DES ELEMENTS :\n             nombre d'aretes = %d\n",nbarete);
 else if (SYRTHES_LANG == EN)
   printf("\n *** ELEMENTS GRAPH :\n             number of edges = %d\n",nbarete);
 
/*     printf("xadj\n"); */
/*     for (n=0;n<nelems;n++) printf("%d ",(*xadj)[n]+1); */
/*     printf("\n adjcy\n");  */
/*     for (n=0;n<nbarete;n++) printf("%d ",(*adjncy)[n]+1); */
/*     printf("\n\n");  */
 
 
}

#endif
