/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pp_usertype.h"
#include "pp_bd.h"
#include "pp_proto.h"

extern char nomgeom[200];


/*|======================================================================|
  | SYRTHES 4.3.0                                      COPYRIGHT EDF 2007|
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture d'in maillage au format Simail                               |
  |======================================================================| */
void lire_simail (struct Maillage *maillnodes,struct MaillageBord *maillnodebord)
{
  FILE  *fmaill;
  int ideb,idebe,nmae,ining,nno,npo,nare,i,j,le;
  int n1,n2,n3,n4,n5,n6,n7,n8,n9,n10;
  int *ne0,*ne1,*ne2,*ne3,*ne4,*ne5,*ne6,*ne7,*ne8,*ne9;
  int *m,*rien,*taille,*it5,nfsy[4];
  int *cmptref,*cmptssd;
  float *co;
  int itmp,ficswap;
  rp_int **nrefac;

  if ((fmaill=fopen(nomgeom,"r")) == NULL)
    {
      if (SYRTHES_LANG == FR) printf("Impossible d'ouvrir le fichier %s\n",nomgeom);	
      else if (SYRTHES_LANG == EN) printf("Unable to open file : %s\n",nomgeom);	
      exit(1) ;
    }

  /* Teste si le fichier binaire est "big endian" ou "little endian"    */
  /* On sait que le premier enregistrement du fichier est de longueur 4 */
  /* -----------------------------------------------------------------  */
  fread(&itmp,4,1,fmaill);
  ficswap = 0;
  if (itmp != 56) 
    {
      fic_bin_f__endswap(&itmp, sizeof(int), 1);

      if (itmp == 56)
	ficswap = 1;
      else
	{
	  if (SYRTHES_LANG == FR) printf("\n %%%% Erreur d'identification du type de fichier (little/big endian)");
	  else if (SYRTHES_LANG == EN) printf("\n %%%% Error : dertermination of type of file error (little/big endian)");
	  exit(1);
	}
    }
  
  /* On remet le pointeur en debut de fichier */
  fseek(fmaill,0L,SEEK_SET);

  taille=(int*)malloc(100*sizeof(int));   verif_alloue_int1d("lire_simail",taille);
  rien=(int*)malloc(50*sizeof(int));      verif_alloue_int1d("lire_simail",rien);
  m=(int*)malloc(50*sizeof(int));         verif_alloue_int1d("lire_simail",m);
  

  /* Lecture de l'enregistrement avant le tableau 0 */
  fread(m,sizeof(int),2,fmaill); if (ficswap) fic_bin_f__endswap(m,sizeof(int),2);
  fread(taille,sizeof(int),m[1],fmaill); if (ficswap) fic_bin_f__endswap(taille,sizeof(int),m[1]);

  /* Lecture du tableau 0  */
  fread(rien,sizeof(int),3,fmaill);
  fread(m,sizeof(int),taille[1],fmaill); if (ficswap) fic_bin_f__endswap(m,sizeof(int),taille[1]);
  if (m[31]) 
    {
      if (SYRTHES_LANG == FR) printf("\n %%%% ERREUR lire_simail : UTILISATION D'ELEMENTS NON CONFORMES\n");
      else if (SYRTHES_LANG == EN) printf("\n %%%% error lire_simail : UTILISATION OF NON AUTHORIZED ELEMENTS\n");
      exit(1);
    }

  /* Lecture du tableau 2  */
  fread(rien,sizeof(int),3,fmaill);
  fread(m,sizeof(int),taille[3],fmaill); if (ficswap) fic_bin_f__endswap(m,sizeof(int),taille[3]);
  if (m[3]==0) 
    {
      if (SYRTHES_LANG == FR) printf("\n %%%% Erreur lire_simail : le maillage fourni n'est pas P1\n\n");
      else if (SYRTHES_LANG == EN) printf("\n %%%% Error lire_simail : the mesh is not P1\n\n");
      exit(1);
    }

  maillnodes->ndim=m[0];
  if (maillnodes->ndim==2) 
    {maillnodes->ndmat=3; maillnodes->nbface=3;maillnodes->ndiele=2;}
  else 
    {maillnodes->ndmat=4; maillnodes->nbface=4;maillnodes->ndiele=3;}
  maillnodes->nelem=m[4];
  maillnodes->npoin=m[14];
  /* if (m[3]) */

  /* Lecture du tableau 3  */
  if (taille[4])
    { 
      fread(rien,sizeof(int),3,fmaill);
      fread(m,sizeof(int),taille[4],fmaill); }
     
  /* Lecture du tableau 4  */
  co=(float*)malloc(taille[5]*sizeof(float)); if (!co) {printf("\n %%%% Error : allocation memory error\n");exit(1);}

  fread(rien,sizeof(int),3,fmaill);
  fread(co,sizeof(float),taille[5],fmaill); if (ficswap) fic_bin_f__endswap(co,sizeof(float),taille[5]);
     
  
  /* Lecture du tableau 5  */
  it5=(int*)malloc(taille[6]*sizeof(int)); verif_alloue_int1d("lire_simail",it5);
  fread(rien,sizeof(int),3,fmaill);
  fread(it5,sizeof(int),taille[6],fmaill); if (ficswap) fic_bin_f__endswap(it5,sizeof(int),taille[6]);


/*    printf(">>> apres lecture i=%d\n",i);
  for (i=0;i<taille[6];i++) printf("i=%d it5=%d\n",i,it5[i]); */

  maillnodes->coord=(double**)malloc(maillnodes->ndim*sizeof(double*));
  for (i=0;i<maillnodes->ndim;i++)  maillnodes->coord[i]=(double*)malloc(maillnodes->npoin*sizeof(double)); 
  verif_alloue_double2d(maillnodes->ndim,"lire_simail",maillnodes->coord);

  maillnodes->node=(rp_int**)malloc(maillnodes->ndmat*sizeof(rp_int*));
  for (i=0;i<maillnodes->ndmat;i++) maillnodes->node[i]=(rp_int*)malloc(maillnodes->nelem*sizeof(rp_int)); 
  verif_alloue_int2d(maillnodes->ndmat,"lire_simail",maillnodes->node);

  maillnodes->nref=(rp_int*)malloc(maillnodes->npoin*sizeof(rp_int));
  verif_alloue_int1d("lire_simail",maillnodes->nref);

  maillnodes->nrefe=(rp_int*)malloc(maillnodes->nelem*sizeof(rp_int));
  verif_alloue_int1d("lire_simail",maillnodes->nrefe);

  for (i=0;i<maillnodes->nelem;i++) maillnodes->nrefe[i]=0;
      
  for (j=0;j<maillnodes->ndim;j++)
    for (i=0;i<maillnodes->npoin;i++)
      maillnodes->coord[j][i] = (double)co[i*maillnodes->ndim+j];

  if (maillnodes->ndim==3) 
    {nare=6; 
     nfsy[0]=0; nfsy[1]=2; nfsy[2]=1; nfsy[3]=3;}
  else 
    {nare=3;}
  
  /* tableau de travail pour les ref des faces */
  nrefac=(rp_int**)malloc((maillnodes->ndim+1)*sizeof(rp_int*));
  for (i=0;i<maillnodes->ndim+1;i++) nrefac[i]=(rp_int*)malloc(maillnodes->nelem*sizeof(rp_int)); 
  verif_alloue_int2d(maillnodes->ndim+1,"lire_simail",nrefac);




/* dans un permier temps, on recupere les references des faces sur chaque element */
  ideb=0;
  for (i=0;i<maillnodes->nelem;i++)
    {
      ideb++;
      nmae=it5[ideb];ideb++;
      maillnodes->nrefe[i]=*(it5+ideb);ideb++;
      nno= it5[ideb];ideb++;
      idebe=ideb;
      for (j=0;j<nno;j++) maillnodes->node[j][i]=it5[ideb+j]-1; 
      ideb+=nno;
      if (nmae) 
	{
	  ining = it5[ideb];ideb++;  

	  if (ining==1 && maillnodes->ndim==3)
	    {
	      for (j=0;j<maillnodes->nbface;j++) nrefac[nfsy[j]][i]=it5[ideb+j];
	      ideb+=maillnodes->nbface;
	      ideb+=nare; 
              /* on met les ref des noeuds sommet */
	      for (j=0;j<maillnodes->ndim+1;j++) maillnodes->nref[maillnodes->node[j][i]]=it5[ideb+j];
	      ideb+=(maillnodes->ndim+1); 
	    }
          else if (ining==2)
	    if (maillnodes->ndim==3) 
	      {
		for (j=0;j<maillnodes->nbface;j++) nrefac[nfsy[j]][i]=0; /* pas de ref de face */
		ideb+=nare; 
		/* on met les ref des noeuds sommet */
		for (j=0;j<maillnodes->ndim+1;j++)  maillnodes->nref[maillnodes->node[j][i]]=it5[ideb+j];
		ideb+=(maillnodes->ndim+1); 
	      }
	    else
	      {
		/* les ref d'aretes sont les ref de face  */
		for (j=0;j<maillnodes->nbface;j++) nrefac[j][i]=it5[ideb+j];
		ideb+=maillnodes->nbface; 
		/* on met les ref des noeuds sommet */
		for (j=0;j<maillnodes->ndim+1;j++)  maillnodes->nref[maillnodes->node[j][i]]=it5[ideb+j];
		ideb+=(maillnodes->ndim+1); 
	      }
	  else if (ining==3)
	    {
	      /* on met les ref des noeuds sommet */
	      for (j=0;j<maillnodes->ndim+1;j++)  maillnodes->nref[maillnodes->node[j][i]]=it5[ideb+j];
	      ideb+=(maillnodes->ndim+1); 
	    }
	}

    }

  fclose(fmaill);
  free(m); free(taille); free(rien); free(it5); free(co);


  /* creation du maillage de bord */
  /* ---------------------------- */
  if (maillnodes->ndim==2)
    extrbord2(*maillnodes,maillnodebord,nrefac);
  else
    extrbord3(*maillnodes,maillnodebord,nrefac);

  for (i=0;i<maillnodes->ndim+1;i++) free(nrefac[i]);
  free(nrefac);

  /* recuperation des infos concernant les references du maillage */
  /* ------------------------------------------------------------ */
  cmptssd=(int*)malloc(MAX_REF*sizeof(int)); 
  for (i=0;i<MAX_REF;i++) cmptssd[i]=0;
  for (i=0;i<maillnodes->nelem;i++) cmptssd[maillnodes->nrefe[i]]++;

  cmptref=(int*)malloc(MAX_REF*sizeof(int)); 
  for (i=0;i<MAX_REF;i++) cmptref[i]=0;
  for (i=0;i<maillnodebord->nelem;i++) cmptref[maillnodebord->nrefe[i]]++;

  if (SYRTHES_LANG == FR) {
    printf("\n\n *** MAILLAGE SIMAIL :\n");
    printf("                           |--------------------|------------------|\n");
    printf("                           | Maillage volumique | Maillage de bord |\n");
    printf("      ---------------------|--------------------|------------------|\n");
    printf("      | Dimension          |    %8d        |    %8d      |\n",maillnodes->ndim,maillnodebord->ndim);
    printf("      | Nombre de noeuds   |    %8d        |         ---      |\n",maillnodes->npoin);
    printf("      | Nombre d'elements  |    %8d        |    %8d      |\n",maillnodes->nelem,maillnodebord->nelem);
    printf("      | Nb noeuds par elt  |    %8d        |    %8d      |\n",maillnodes->ndmat,maillnodebord->ndmat);
    printf("      |--------------------|--------------------|------------------|\n");
    printf("      |    Materiaux       |---------------------------------------|\n");    
    printf("      |--------------------|--------------------|------------------|\n");
  }
  else if (SYRTHES_LANG == EN) {
    printf("\n\n *** SIMAIL MESH :\n");
    printf("                           |--------------------|------------------|\n");
    printf("                           | Volumic mesh       | Surfacic mesh    |\n");
    printf("      ---------------------|--------------------|------------------|\n");
    printf("      | Dimension          |    %8d        |    %8d      |\n",maillnodes->ndim,maillnodebord->ndim);
    printf("      | Number of nodes    |    %8d        |         ---      |\n",maillnodes->npoin);
    printf("      | Number of elements |    %8d        |    %8d      |\n",maillnodes->nelem,maillnodebord->nelem);
    printf("      | Nb nodes per elts  |    %8d        |    %8d      |\n",maillnodes->ndmat,maillnodebord->ndmat);
    printf("      |--------------------|--------------------|------------------|\n");
    printf("      |    Materials       |---------------------------------------|\n");    
    printf("      |--------------------|--------------------|------------------|\n");
  }
  for (i=1;i<MAX_REF;i++)
    if (cmptssd[i]>0)
      printf("      |        %2d          |    %8d        |         ---      |\n",i,cmptssd[i]);
  printf("      |--------------------|--------------------|------------------|\n");
  if (SYRTHES_LANG == FR) printf("      | References de bord  ---------------------------------------|\n");   
  else if (SYRTHES_LANG == EN) printf("      | Surfacic references ---------------------------------------|\n");   
  printf("      |--------------------|--------------------|------------------|\n");
  for (i=1;i<MAX_REF;i++)
    if (cmptref[i]>0)
      printf("      |        %2d          |         ---        |    %8d      |\n",i,cmptref[i]);
  printf("      |--------------------|--------------------|------------------|\n");



  free(cmptssd); free(cmptref);
}

/*|======================================================================|
  | SYRTHES 4.3.0                                      COPYRIGHT EDF 2007|
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture d'un maillage au format Ideas                                |
  |======================================================================| */
void lire_ideas (struct Maillage *maillnodes,struct MaillageBord *maillnodebord)
{
  FILE  *fmaill;
  rp_int i,j,ne,n1,n2,n3,n4,nr,nn,nbnod;
  rp_int n,ne111,ne91,ne21;
  rp_int fincoo,finnod111,finnod91,finnod21;
  rp_int **node91,**node111,**node21,*nref111,*nref91,*nref21;
  double x,y,z,**coo;
  char ch[90],ch1[90],ch2[90],ch3[90];


  if ((fmaill=fopen(nomgeom,"r")) == NULL)
    {
      if (SYRTHES_LANG == FR) printf("Impossible d'ouvrir le fichier %s\n",nomgeom);	
      else if (SYRTHES_LANG == EN) printf("Unable to open file : %s\n",nomgeom);	
      exit(1) ;
    }

  /* Allocations de base avant redimenssionnement en fonction de ce qui est lu */

  finnod111=finnod91=finnod21=10000;
  fincoo=10000;

  coo=(double**)malloc(3*sizeof(double*));
  for (i=0;i<3;i++)  coo[i]=(double*)malloc(fincoo*sizeof(double)); 
  verif_alloue_double2d(3,"lire_ideas",coo);

  node111=(rp_int**)malloc(4*sizeof(rp_int*));
  for (i=0;i<4;i++) node111[i]=(rp_int*)malloc(finnod111*sizeof(rp_int)); 
  verif_alloue_int2d(4,"lire_ideas",node111);

  node91=(rp_int**)malloc(3*sizeof(rp_int*));
  for (i=0;i<3;i++) node91[i]=(rp_int*)malloc(finnod91*sizeof(rp_int)); 
  verif_alloue_int2d(3,"lire_ideas",node91);

  node21=(rp_int**)malloc(2*sizeof(rp_int*));
  for (i=0;i<2;i++) node21[i]=(rp_int*)malloc(finnod21*sizeof(rp_int)); 
  verif_alloue_int2d(2,"lire_ideas",node21);

  nref111=(rp_int*)malloc(finnod111*sizeof(rp_int));   verif_alloue_int1d("lire_ideas",nref111);
  nref91=(rp_int*)malloc(finnod91*sizeof(rp_int));     verif_alloue_int1d("lire_ideas",nref91);
  nref21=(rp_int*)malloc(finnod21*sizeof(rp_int));     verif_alloue_int1d("lire_ideas",nref21);


  /* lecture du maillage */
  n=ne111=ne91=ne21=0;

  while(fgets(ch1,90,fmaill))
    {
      sscanf(ch1,"%s",ch);
      if (strcmp(ch,"-1")==0)
	{
	  if (fgets(ch1,90,fmaill))
	    {
	      sscanf(ch1,"%s",ch);
	      if (strcmp(ch,"-1")==0)
		{
		  fgets(ch1,90,fmaill);
		  sscanf(ch1,"%s",ch);
		}
	      /* lecture des coordonnees des noeuds */
              /* ---------------------------------- */
	      if (strcmp(ch,"2411")==0 || strcmp(ch,"781")==0) 
		{
		  fgets(ch3,90,fmaill);
		  sscanf(ch3,"%s",ch1);
		  while (strcmp(ch1,"-1")!=0)
		    {
		      sscanf(ch3,"%10d%10d%10d%10d",&nn,&nn,&nn,&nr);
		      fgets(ch2,90,fmaill);
		      ch2[21]=ch2[46]='E';
		      sscanf(ch2,"%lf%lf%lf",&x,&y,&z);
		      if (n>fincoo-1)
			{
                            /* le tableau est trop petit, on l'agrandit */
                            fincoo+=20000;
                            coo[0]=(double*)realloc(coo[0],fincoo*sizeof(double));
                            coo[1]=(double*)realloc(coo[1],fincoo*sizeof(double));
                            coo[2]=(double*)realloc(coo[2],fincoo*sizeof(double));
                          }
                        coo[0][n]=x; coo[1][n]=y; coo[2][n]=z; 
                        n += 1;
                        fgets(ch3,90,fmaill);
                        sscanf(ch3,"%s",ch1);
                      }
                  }
                else if  (strcmp(ch,"2412")==0)
                  /* lecture des elements */
		  /* -------------------- */
                  {
                    fgets(ch3,90,fmaill);
                    sscanf(ch3,"%s",ch1);
                    while (strcmp(ch1,"-1")!=0)
                      {
                        sscanf(ch3,"%10d%10d%10d%10d%10d%10d",&n1,&n2,&n3,&n4,&nr,&nbnod);
			if (n2==111) /* tetraedres */
			  {
			    if (ne111>finnod111-1)
			      {
				finnod111+=20000;
				for (i=0;i<nbnod;i++)
				  node111[i]=(rp_int*)realloc(node111[i],finnod111*sizeof(rp_int));
				nref111=(rp_int*)realloc(nref111,finnod111*sizeof(rp_int));
			      }
			    nref111[ne111]=nr;
			    fscanf(fmaill,"%10d%10d%10d%10d",(node111[0]+ne111),(node111[1]+ne111),
				                             (node111[2]+ne111),(node111[3]+ne111));
			    ne111++;
			  }
			else if (n2==91) /* triangles */
			  {
			    if (ne91>finnod91-1)
			      {
				finnod91+=20000;
				for (i=0;i<nbnod;i++)
				  node91[i]=(rp_int*)realloc(node91[i],finnod91*sizeof(rp_int));
				nref91=(rp_int*)realloc(nref91,finnod91*sizeof(rp_int));
			      }
			    nref91[ne91]=nr;
                            fscanf(fmaill,"%10d%10d%10d",(node91[0]+ne91),(node91[1]+ne91),(node91[2]+ne91));
			    ne91++;
			  }
			else if (n2==11 || n2==21) /* poutres */
			  {
			    if (ne21>finnod21-1)
			      {
				finnod21+=20000;
				for (i=0;i<nbnod;i++)
				  node21[i]=(rp_int*)realloc(node21[i],finnod21*sizeof(rp_int));
				nref21=(rp_int*)realloc(nref21,finnod21*sizeof(rp_int));
			      }
			    nref21[ne21]=nr;
			    fgets(ch3,90,fmaill); /* on passe une ligne */
                            fscanf(fmaill,"%10d%10d",(node21[0]+ne21),(node21[1]+ne21));
			    ne21++;
			  }
                        fgets(ch3,90,fmaill);fgets(ch3,90,fmaill);
                        sscanf(ch3,"%s",ch1);
                      }
                  }
	    }
	}
    }


  if (ne111==0)
    {
      maillnodes->ndim=2;      maillnodes->ndmat=3;      maillnodes->ndiele=2;
      maillnodes->npoin=n;     maillnodes->nelem=ne91;  
      maillnodebord->ndim=1;   maillnodebord->ndmat=2;   maillnodebord->ndiele=2;
      maillnodebord->nelem=ne21;
    }
  else 
    {
      maillnodes->ndim=3;      maillnodes->ndmat=4;      maillnodes->ndiele=3;
      maillnodes->npoin=n;     maillnodes->nelem=ne111;  
      maillnodebord->ndim=2;   maillnodebord->ndmat=3;   maillnodebord->ndiele=3;
      maillnodebord->nelem=ne91;
    }

  /* Ajustement de la taille des tableaux */
  for (i=0;i<maillnodes->ndim;i++) coo[i]=(double*)realloc(coo[i],maillnodes->npoin*sizeof(double));

  if (maillnodes->ndim==3)
    {
      for (i=0;i<maillnodes->ndmat;i++) node111[i]=(rp_int*)realloc(node111[i],maillnodes->nelem*sizeof(rp_int));
      for (i=0;i<maillnodebord->ndmat;i++) node91[i]=(rp_int*)realloc(node91[i],maillnodebord->nelem*sizeof(rp_int));
      nref111=(rp_int*)realloc(nref111,maillnodes->nelem*sizeof(rp_int)); 
      nref91=(rp_int*)realloc(nref91,maillnodebord->nelem*sizeof(rp_int));
      maillnodes->coord=coo;  maillnodes->node=node111;    maillnodes->nrefe=nref111;
      maillnodebord->node=(rp_int**)node91;  maillnodebord->nrefe=nref91;
    }
  else
    {
      free(coo[2]);  coo=(double**)realloc(coo,2*sizeof(double*));
      for (i=0;i<maillnodes->ndmat;i++) node91[i]=(rp_int*)realloc(node91[i],maillnodes->nelem*sizeof(rp_int));
      for (i=0;i<maillnodebord->ndmat;i++) node21[i]=(rp_int*)realloc(node21[i],maillnodebord->nelem*sizeof(rp_int));
      nref91=(rp_int*)realloc(nref91,maillnodes->nelem*sizeof(rp_int));
      nref21=(rp_int*)realloc(nref21,maillnodebord->nelem*sizeof(rp_int));
      maillnodes->coord=coo;     maillnodes->node=node91;    maillnodes->nrefe=nref91;
      maillnodebord->node=node21;  maillnodebord->nrefe=nref21;
    }

  fclose(fmaill);


  /*  imprime_maillage_ref(*maillnodes); */
  /* imprime_maillage_ref(*maillnodebord);*/

  /* numerotation des noeuds a partir de 0 */
  for (i=0;i<maillnodes->nelem;i++)
    for (j=0;j<maillnodes->ndmat;j++)
      maillnodes->node[j][i]--;

  for (i=0;i<maillnodebord->nelem;i++)
    for (j=0;j<maillnodebord->ndmat;j++)
      maillnodebord->node[j][i]--;



  if (SYRTHES_LANG == FR) {
    printf("\n\n *** MAILLAGE IDEAS :\n");
    printf("                           |--------------------|------------------|\n");
    printf("                           | Maillage volumique | Maillage de bord |\n");
    printf("      ---------------------|--------------------|------------------|\n");
    printf("      | Dimension          |    %8d        |    %8d      |\n",maillnodes->ndim,maillnodebord->ndim);
    printf("      | Nombre de noeuds   |    %8d        |       ---        |\n",maillnodes->npoin);
    printf("      | Nombre d'elements  |    %8d        |    %8d      |\n",maillnodes->nelem,maillnodebord->nelem);
    printf("      | Nb noeuds par elt  |    %8d        |    %8d      |\n",maillnodes->ndmat,maillnodebord->ndmat);
    printf("      ---------------------|--------------------|------------------|\n");
  }
  else if (SYRTHES_LANG == EN) {
    printf("\n\n *** SIMAIL MESH :\n");
    printf("                           |--------------------|------------------|\n");
    printf("                           | Volumic mesh       | Surfacic mesh    |\n");
    printf("      ---------------------|--------------------|------------------|\n");
    printf("      | Dimension          |    %8d        |    %8d      |\n",maillnodes->ndim,maillnodebord->ndim);
    printf("      | Number of nodes    |    %8d        |         ---      |\n",maillnodes->npoin);
    printf("      | Number of elements |    %8d        |    %8d      |\n",maillnodes->nelem,maillnodebord->nelem);
    printf("      | Nb nodes per elts  |    %8d        |    %8d      |\n",maillnodes->ndmat,maillnodebord->ndmat);
    printf("      |--------------------|--------------------|------------------|\n");
  }
}

/*|======================================================================|
  | SYRTHES 4.3.0                                      COPYRIGHT EDF 2007|
  |======================================================================|
  | AUTEURS  : I. RUPP, C. PENIGUEL                                      |
  |======================================================================|
  | Lecture d'un maillage au format Syrthes                              |
  |======================================================================| */
void lire_syrthes(struct Maillage *maillnodes,struct MaillageBord *maillnodebord)
{
  FILE  *fgeom;
  char ch[200],ch1[200];
  rp_int ndmats_lu,npoin_lu,nelem_lu,nelemb_lu,ndim_lu,bidon;
  rp_int i,j,n,nr,nv,nb;
  rp_int *ne0,*ne1,*ne2,*ne3;
  double x,y,z;
  rp_int form=1;


  if (!strncmp(nomgeom+strlen(nomgeom)-5, ".syrb",5)) form=0;

  if (form)
    {
      if ((fgeom=fopen(nomgeom,"r")) == NULL)
	{
	  if (SYRTHES_LANG == FR) printf("Impossible d'ouvrir le fichier %s\n",nomgeom);	
	  else if (SYRTHES_LANG == EN) printf("Unable to open file : %s\n",nomgeom);	
	  exit(1) ;
	}
    }
  else
    {
      if ((fgeom=fopen(nomgeom,"rb")) == NULL)
	{
	  if (SYRTHES_LANG == FR) printf("Impossible d'ouvrir le fichier %s\n",nomgeom);	
	  else if (SYRTHES_LANG == EN) printf("Unable to open file : %s\n",nomgeom);	
	  exit(1) ;
	}
    }


  if (form)
    {
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      
      if (PP_TYPEWIDTH ==32)
	{
	  fscanf(fgeom,"%s%s%s%d",ch,ch,ch,&(maillnodes->ndim));
	  fscanf(fgeom,"%s%s%s%s%s%d",ch,ch,ch,ch,ch,&(maillnodes->ndiele));
	  fscanf(fgeom,"%s%s%s%s%s%d",ch,ch,ch,ch,ch,&(maillnodes->npoin));
	  fscanf(fgeom,"%s%s%s%s%d",ch,ch,ch,ch,&(maillnodes->nelem));
	  fscanf(fgeom,"%s%s%s%s%s%s%d",ch,ch,ch,ch,ch,ch,&(maillnodebord->nelem));
	  fscanf(fgeom,"%s%s%s%s%s%s%s%d",ch,ch,ch,ch,ch,ch,ch,&(maillnodes->ndmat));
	}
      else
	{
	  fscanf(fgeom,"%s%s%s%ld",ch,ch,ch,&(maillnodes->ndim));
	  fscanf(fgeom,"%s%s%s%s%s%ld",ch,ch,ch,ch,ch,&(maillnodes->ndiele));
	  fscanf(fgeom,"%s%s%s%s%s%ld",ch,ch,ch,ch,ch,&(maillnodes->npoin));
	  fscanf(fgeom,"%s%s%s%s%ld",ch,ch,ch,ch,&(maillnodes->nelem));
	  fscanf(fgeom,"%s%s%s%s%s%s%ld",ch,ch,ch,ch,ch,ch,&(maillnodebord->nelem));
	  fscanf(fgeom,"%s%s%s%s%s%s%s%ld",ch,ch,ch,ch,ch,ch,ch,&(maillnodes->ndmat));
	}
      
      maillnodebord->ndim  = maillnodes->ndim;
      maillnodebord->ndmat = maillnodes->ndmat-1;
      maillnodebord->ndiele= maillnodes->ndiele-1;
      
      if (SYRTHES_LANG == FR){
	if (PP_TYPEWIDTH ==32)
	  {
	    printf("\n\n *** MAILLAGE SYRTHES :\n");
	    printf("                           |--------------------|------------------|\n");
	    printf("                           | Maillage volumique | Maillage de bord |\n");
	    printf("      ---------------------|--------------------|------------------|\n");
	    printf("      | Dimension          |    %12d    |    %12d  |\n",maillnodes->ndim,maillnodebord->ndim);
	    printf("      | Nombre de noeuds   |    %12d    |     inusite      |\n",maillnodes->npoin);
	    printf("      | Nombre d'elements  |    %12d    |    %12d  |\n",maillnodes->nelem,maillnodebord->nelem);
	    printf("      | Nb noeuds par elt  |    %12d    |    %12d  |\n",maillnodes->ndmat,maillnodebord->ndmat);
	    printf("      ---------------------|--------------------|------------------|\n");
	  }
	else
	  {
	    printf("\n\n *** MAILLAGE SYRTHES :\n");
	    printf("                           |--------------------|------------------|\n");
	    printf("                           | Maillage volumique | Maillage de bord |\n");
	    printf("      ---------------------|--------------------|------------------|\n");
	    printf("      | Dimension          |    %12ld    |    %12ld  |\n",maillnodes->ndim,maillnodebord->ndim);
	    printf("      | Nombre de noeuds   |    %12ld    |     inusite      |\n",maillnodes->npoin);
	    printf("      | Nombre d'elements  |    %12ld    |    %12ld  |\n",maillnodes->nelem,maillnodebord->nelem);
	    printf("      | Nb noeuds par elt  |    %12ld    |    %12ld  |\n",maillnodes->ndmat,maillnodebord->ndmat);
	    printf("      ---------------------|--------------------|------------------|\n");
	  }}
      else if (SYRTHES_LANG == EN){
	if (PP_TYPEWIDTH ==32)
	  {
	    printf("\n\n *** SYRTHES MESH :\n");
	    printf("                           |--------------------|------------------|\n");
	    printf("                           | Volumic mesh       | Surfacic mesh    |\n");
	    printf("      ---------------------|--------------------|------------------|\n");
	    printf("      | Dimension          |    %12d    |    %12d  |\n",maillnodes->ndim,maillnodebord->ndim);
	    printf("      | Number of nodes    |    %12d    |       ---        |\n",maillnodes->npoin);
	    printf("      | Number of elements |    %12d    |    %12d  |\n",maillnodes->nelem,maillnodebord->nelem);
	    printf("      | Nb nodes per elt   |    %12d    |    %12d  |\n",maillnodes->ndmat,maillnodebord->ndmat);
	    printf("      ---------------------|--------------------|------------------|\n");
	  }
	else
	  {
	    printf("\n\n *** SYRTHES MESH :\n");
	    printf("                           |--------------------|------------------|\n");
	    printf("                           | Volumic mesh       | Surfacic mesh    |\n");
	    printf("      ---------------------|--------------------|------------------|\n");
	    printf("      | Dimension          |    %12ld    |    %12ld  |\n",maillnodes->ndim,maillnodebord->ndim);
	    printf("      | Number of nodes    |    %12ld    |       ---        |\n",maillnodes->npoin);
	    printf("      | Number of elements |    %12ld    |    %12ld  |\n",maillnodes->nelem,maillnodebord->nelem);
	    printf("      | Nb nodes per elt   |    %12ld    |    %12ld  |\n",maillnodes->ndmat,maillnodebord->ndmat);
	    printf("      ---------------------|--------------------|------------------|\n");
	  }}

      
      
      maillnodes->coord=(double**)malloc(maillnodes->ndim*sizeof(double*));
      for (i=0;i<maillnodes->ndim;i++)  maillnodes->coord[i]=(double*)malloc(maillnodes->npoin*sizeof(double)); 
      verif_alloue_double2d(maillnodes->ndim,"lire_syrthes",maillnodes->coord);
      
      maillnodes->node=(rp_int**)malloc(maillnodes->ndmat*sizeof(rp_int*));
      for (i=0;i<maillnodes->ndmat;i++) maillnodes->node[i]=(rp_int*)malloc(maillnodes->nelem*sizeof(rp_int)); 
      verif_alloue_int2d(maillnodes->ndmat,"lire_syrthes",maillnodes->node);
      
      maillnodes->nref=(rp_int*)malloc(maillnodes->npoin*sizeof(rp_int));
      verif_alloue_int1d("lire_syrthes",maillnodes->nref);
      
      maillnodes->nrefe=(rp_int*)malloc(maillnodes->nelem*sizeof(rp_int));
      verif_alloue_int1d("lire_syrthes",maillnodes->nrefe);
      
      
      maillnodebord->node=(rp_int**)malloc(maillnodebord->ndmat*sizeof(rp_int*));
      for (i=0;i<maillnodebord->ndmat;i++) maillnodebord->node[i]=(rp_int*)malloc(maillnodebord->nelem*sizeof(rp_int)); 
      verif_alloue_int2d(maillnodebord->ndmat,"lire_syrthes",maillnodebord->node);
      
      maillnodebord->nrefe=(rp_int*)malloc(maillnodebord->nelem*sizeof(rp_int));
      verif_alloue_int1d("lire_syrthes",maillnodebord->nrefe);
      
      
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      

      /* coordonnees noeuds maillage SYRTHES */
      if (PP_TYPEWIDTH ==32)
	{
	  if (maillnodes->ndim==2) 
	    {
	      for (i=0;i<maillnodes->npoin;i++)
		fscanf(fgeom,"%d%d%14lf%14lf%14lf",&n,maillnodes->nref+i,(maillnodes->coord[0]+i),
		       (maillnodes->coord[1]+i),&z);
	    }
	  else
	    {
	      for (i=0;i<maillnodes->npoin;i++)
		fscanf(fgeom,"%d%d%14lf%14lf%14lf",&n,maillnodes->nref+i,(maillnodes->coord[0]+i),
		       (maillnodes->coord[1]+i),(maillnodes->coord[2]+i));
	    }
	}
      else
	{
	  if (maillnodes->ndim==2) 
	    {
	      for (i=0;i<maillnodes->npoin;i++)
		fscanf(fgeom,"%ld%ld%14lf%14lf%14lf",&n,maillnodes->nref+i,(maillnodes->coord[0]+i),
		       (maillnodes->coord[1]+i),&z);
	    }
	  else
	    {
	      for (i=0;i<maillnodes->npoin;i++)
		fscanf(fgeom,"%ld%ld%14lf%14lf%14lf",&n,maillnodes->nref+i,(maillnodes->coord[0]+i),
		       (maillnodes->coord[1]+i),(maillnodes->coord[2]+i));
	    }
	}
      
      
      /* connectivite maillage volumique SYRTHES */
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      
      if (PP_TYPEWIDTH ==32)
	{
	  if (maillnodes->ndmat==3)
	    for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],ne2=maillnodes->node[2];
		 i<maillnodes->nelem;
		 i++,ne0++,ne1++,ne2++)
	      fscanf(fgeom,"%d%d%d%d%d",&n,maillnodes->nrefe+i,ne0,ne1,ne2);
	  
	  else
	    for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],ne2=maillnodes->node[2],ne3=maillnodes->node[3];
		 i<maillnodes->nelem;
		 i++,ne0++,ne1++,ne2++,ne3++)
	      fscanf(fgeom,"%d%d%d%d%d%d",&n,maillnodes->nrefe+i,ne0,ne1,ne2,ne3);
	}
      else
	{
	  if (maillnodes->ndmat==3)
	    for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],ne2=maillnodes->node[2];
		 i<maillnodes->nelem;
		 i++,ne0++,ne1++,ne2++)
	      fscanf(fgeom,"%ld%ld%ld%ld%ld",&n,maillnodes->nrefe+i,ne0,ne1,ne2);
	  
	  else
	    for (i=0,ne0=maillnodes->node[0],ne1=maillnodes->node[1],ne2=maillnodes->node[2],ne3=maillnodes->node[3];
		 i<maillnodes->nelem;
		 i++,ne0++,ne1++,ne2++,ne3++)
	      fscanf(fgeom,"%ld%ld%ld%ld%ld%ld",&n,maillnodes->nrefe+i,ne0,ne1,ne2,ne3);
	}
      
      
      /* connectivite maillage de bord SYRTHES */
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      fgets(ch,90,fgeom);
      
      if (PP_TYPEWIDTH ==32)
	{
	  if (maillnodebord->ndmat==3)
	    for (i=0,ne0=maillnodebord->node[0],ne1=maillnodebord->node[1],ne2=maillnodebord->node[2];
		 i<maillnodebord->nelem;
		 i++,ne0++,ne1++,ne2++)
	      fscanf(fgeom,"%d%d%d%d%d",&n,maillnodebord->nrefe+i,ne0,ne1,ne2);
	  
	  else
	    for (i=0,ne0=maillnodebord->node[0],ne1=maillnodebord->node[1];
		 i<maillnodebord->nelem;
		 i++,ne0++,ne1++)
	      fscanf(fgeom,"%d%d%d%d",&n,maillnodebord->nrefe+i,ne0,ne1);  
	}
      else
	{
	  if (maillnodebord->ndmat==3)
	    for (i=0,ne0=maillnodebord->node[0],ne1=maillnodebord->node[1],ne2=maillnodebord->node[2];
		 i<maillnodebord->nelem;
		 i++,ne0++,ne1++,ne2++)
	      fscanf(fgeom,"%ld%ld%ld%ld%ld",&n,maillnodebord->nrefe+i,ne0,ne1,ne2);
	  
	  else
	    for (i=0,ne0=maillnodebord->node[0],ne1=maillnodebord->node[1];
		 i<maillnodebord->nelem;
		 i++,ne0++,ne1++)
	      fscanf(fgeom,"%ld%ld%ld%ld",&n,maillnodebord->nrefe+i,ne0,ne1);  
	}

    } /* fin de la lecture formattee

  /* lecture binaire */
  /* --------------- */
  else
    {

      fread(&(maillnodes->ndim),sizeof(rp_int),1,fgeom);
      fread(&(maillnodes->ndiele),sizeof(rp_int),1,fgeom);
      fread(&(maillnodes->npoin),sizeof(rp_int),1,fgeom);
      fread(&(maillnodes->nelem),sizeof(rp_int),1,fgeom);
      fread(&(maillnodebord->nelem),sizeof(rp_int),1,fgeom);
      fread(&(maillnodes->ndmat),sizeof(rp_int),1,fgeom);

      maillnodebord->ndim  = maillnodes->ndim;
      maillnodebord->ndmat = maillnodes->ndmat-1;
      maillnodebord->ndiele= maillnodes->ndiele-1;

      if (SYRTHES_LANG == FR){
	if (PP_TYPEWIDTH ==32)
	  {
	    printf("\n\n *** MAILLAGE SYRTHES :\n");
	    printf("                           |--------------------|------------------|\n");
	    printf("                           | Maillage volumique | Maillage de bord |\n");
	    printf("      ---------------------|--------------------|------------------|\n");
	    printf("      | Dimension          |    %12d    |    %12d  |\n",maillnodes->ndim,maillnodebord->ndim);
	    printf("      | Nombre de noeuds   |    %12d    |       ---        |\n",maillnodes->npoin);
	    printf("      | Nombre d'elements  |    %12d    |    %12d  |\n",maillnodes->nelem,maillnodebord->nelem);
	    printf("      | Nb noeuds par elt  |    %12d    |    %12d  |\n",maillnodes->ndmat,maillnodebord->ndmat);
	    printf("      ---------------------|--------------------|------------------|\n");
	  }
	else
	  {
	    printf("\n\n *** MAILLAGE SYRTHES :\n");
	    printf("                           |--------------------|------------------|\n");
	    printf("                           | Maillage volumique | Maillage de bord |\n");
	    printf("      ---------------------|--------------------|------------------|\n");
	    printf("      | Dimension          |    %12ld    |    %12ld  |\n",maillnodes->ndim,maillnodebord->ndim);
	    printf("      | Nombre de noeuds   |    %12ld    |      ---        |\n",maillnodes->npoin);
	    printf("      | Nombre d'elements  |    %12ld    |    %12ld  |\n",maillnodes->nelem,maillnodebord->nelem);
	    printf("      | Nb noeuds par elt  |    %12ld    |    %12ld  |\n",maillnodes->ndmat,maillnodebord->ndmat);
	    printf("      ---------------------|--------------------|------------------|\n");
	  }}
      else if (SYRTHES_LANG == EN){
	if (PP_TYPEWIDTH ==32)
	  {
	    printf("\n\n *** MAILLAGE SYRTHES :\n");
	    printf("                           |--------------------|------------------|\n");
	    printf("                           | Volumic mesh       | Surfacic mesh    |\n");
	    printf("      ---------------------|--------------------|------------------|\n");
	    printf("      | Dimension          |    %12d    |    %12d  |\n",maillnodes->ndim,maillnodebord->ndim);
	    printf("      | Number of nodes    |    %12d    |       ---        |\n",maillnodes->npoin);
	    printf("      | Number of elements |    %12d    |    %12d  |\n",maillnodes->nelem,maillnodebord->nelem);
	    printf("      | Nb nodes per elt   |    %12d    |    %12d  |\n",maillnodes->ndmat,maillnodebord->ndmat);
	    printf("      ---------------------|--------------------|------------------|\n");
	  }
	else
	  {
	    printf("\n\n *** SYRTHES MESH :\n");
	    printf("                           |--------------------|------------------|\n");
	    printf("                           | Volumic mesh       | Surfacic mesh    |\n");
	    printf("      ---------------------|--------------------|------------------|\n");
	    printf("      | Dimension          |    %12ld    |    %12ld  |\n",maillnodes->ndim,maillnodebord->ndim);
	    printf("      | Number of nodes    |    %12ld    |       ---        |\n",maillnodes->npoin);
	    printf("      | Number of elements |    %12ld    |    %12ld  |\n",maillnodes->nelem,maillnodebord->nelem);
	    printf("      | Nb nodes per elt   |    %12ld    |    %12ld  |\n",maillnodes->ndmat,maillnodebord->ndmat);
	    printf("      ---------------------|--------------------|------------------|\n");
	  }}

      
      
      maillnodes->coord=(double**)malloc(maillnodes->ndim*sizeof(double*));
      for (i=0;i<maillnodes->ndim;i++)  maillnodes->coord[i]=(double*)malloc(maillnodes->npoin*sizeof(double)); 
      verif_alloue_double2d(maillnodes->ndim,"lire_syrthes",maillnodes->coord);
      
      maillnodes->node=(rp_int**)malloc(maillnodes->ndmat*sizeof(rp_int*));
      for (i=0;i<maillnodes->ndmat;i++) maillnodes->node[i]=(rp_int*)malloc(maillnodes->nelem*sizeof(rp_int)); 
      verif_alloue_int2d(maillnodes->ndmat,"lire_syrthes",maillnodes->node);
      
      maillnodes->nref=(rp_int*)malloc(maillnodes->npoin*sizeof(rp_int));
      verif_alloue_int1d("lire_syrthes",maillnodes->nref);
      
      maillnodes->nrefe=(rp_int*)malloc(maillnodes->nelem*sizeof(rp_int));
      verif_alloue_int1d("lire_syrthes",maillnodes->nrefe);
      
      
      maillnodebord->node=(rp_int**)malloc(maillnodebord->ndmat*sizeof(rp_int*));
      for (i=0;i<maillnodebord->ndmat;i++) maillnodebord->node[i]=(rp_int*)malloc(maillnodebord->nelem*sizeof(rp_int)); 
      verif_alloue_int2d(maillnodebord->ndmat,"lire_syrthes",maillnodebord->node);
      
      maillnodebord->nrefe=(rp_int*)malloc(maillnodebord->nelem*sizeof(rp_int));
      verif_alloue_int1d("lire_syrthes",maillnodebord->nrefe);
      
      

      /* coordonnees */
      fread(maillnodes->nref,sizeof(rp_int),maillnodes->npoin,fgeom);
      for (i=0;i<maillnodes->ndim;i++) fread(maillnodes->coord[i],sizeof(double),maillnodes->npoin,fgeom);
      
      /* connectivite maillage volumique SYRTHES */
      fread(maillnodes->nrefe,sizeof(rp_int),maillnodes->nelem,fgeom);
      for (i=0;i<maillnodes->ndmat;i++) fread(maillnodes->node[i],sizeof(rp_int),maillnodes->nelem,fgeom);
      
      /* connectivite maillage de bord SYRTHES */
      fread(maillnodebord->nrefe,sizeof(rp_int),maillnodebord->nelem,fgeom);
      for (i=0;i<maillnodebord->ndmat;i++) fread(maillnodebord->node[i],sizeof(rp_int),maillnodebord->nelem,fgeom);

    } /* fin de la lecture binaire */





  /*    imprime_maillage_ref(*maillnodes); */
  /* imprime_maillage_ref(*maillnodebord);*/


  /* numerotation des noeuds a partir de 0 */
  for (i=0;i<maillnodes->nelem;i++)
    for (j=0;j<maillnodes->ndmat;j++)
      maillnodes->node[j][i]--;

  for (i=0;i<maillnodebord->nelem;i++)
    for (j=0;j<maillnodebord->ndmat;j++)
      maillnodebord->node[j][i]--;

}

