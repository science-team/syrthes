/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <string.h>
# include "pp_usertype.h"
# include "pp_bd.h"
# include "pp_proto.h"
# include "pp_const.h"

extern struct Performances perfo;

static char ch[CHLONG],motcle[CHLONG];
static double dlist[100];
static rp_int ilist[100];


/*|======================================================================|
  | SYRTHES PARALLELE                                          SEPT 2003 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Creation des tables de correspondances pour la periodicite           |
  |                                                                      |
  |======================================================================|*/
void pp_exist_perio_rc(FILE *fdata,rp_int *nbrrc,rp_int *nbrp)
{			    
		
  rp_int i1,i2,i3,i4,id;


  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"CLIM")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"PERIODICITE_2D") || !strcmp(motcle,"PERIODICITE_3D")) 
		*nbrp=1;
	    }
	  else if(!strcmp(motcle,"CLIM_T"))
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"RES_CONTACT")) *nbrrc=1;
	    }
	}
    }

}



/*|======================================================================|
  | SYRTHES PARALLELE                                          SEPT 2003 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Creation des tables de correspondances pour la periodicite           |
  |                                                                      |
  |======================================================================|*/
void pp_perio(FILE *fdata,
	      struct Maillage maillnodes,struct MaillageBord maillnodebord,
	      struct NodeCouple **listpairesperio)
			    
		
{
  rp_int n,i,k,nbpaires;
  struct NodeCouple *pp,*qp;
  rp_int i1,i2,i3,i4,id,ii,nb,nr,it;
  rp_int *itrav;
  double zero=0.;
  rp_int nn1,nn2,*np1,*np2;
  rp_int type;


  type=TYPRESCON;


  printf("\n *** PERIO : creation des tables de correspondance pour la ");
  printf("prise en compte de la periodicite \n");

  /* on cree la liste des paires */
  *listpairesperio=NULL;  pp=NULL; nbpaires=0;


  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"CLIM")) 
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;

	      if (!strcmp(motcle,"PERIODICITE_2D")) 
		{
		  extr_motcle(motcle,ch+id,&i3,&i4);
		  id+=i4+1;
		  
		  if (!strcmp(motcle,"T")) 
		    {
		      it=0;
		      rep_ndbl(2,dlist,&ii,ch+id);
		      rep_listint(ilist,&nb,ch+id+ii);
		      cree_liste_noeuds_periorc(maillnodebord,maillnodes,type, 
					      ilist,nb,&nn1,&nn2,&np1,&np2);
		      pp_cree_paires_perio(maillnodes,nn1,nn2,np1,np2, 
					   it,dlist[0],dlist[1],zero,
					   zero,zero,zero,zero,
					   &nbpaires,listpairesperio,&pp);
		      free(np1); free(np2);
		    }
		  else if (!strcmp(motcle,"R")) 
		    {
		      it=1;
		      rep_ndbl(3,dlist,&ii,ch+id);
		      rep_listint(ilist,&nb,ch+id+ii);
		      cree_liste_noeuds_periorc(maillnodebord,maillnodes,type, 
					      ilist,nb,&nn1,&nn2,&np1,&np2);
		      pp_cree_paires_perio(maillnodes,nn1,nn2,np1,np2,
					   it,dlist[0],dlist[1],dlist[2],
					   zero,zero,zero,zero,
					   &nbpaires,listpairesperio,&pp);
		      free(np1); free(np2);
		    }
		  else
		    {
		      printf(" !!! ERREUR lors de la lecture des conditions aux limites\n");
		      printf("            Le mot-cle suivant n'est pas reconnu :\n");
		      printf("            --> CLIM=  PERIODICITE_2D %s\n",motcle);
		      exit(1);
		    }   
		}
	      else if (!strcmp(motcle,"PERIODICITE_3D")) 
		{
		  extr_motcle(motcle,ch+id,&i3,&i4);
		  id+=i4+1;
		  
		  if (!strcmp(motcle,"T")) 
		    {
		      it=0;
		      rep_ndbl(3,dlist,&ii,ch+id);
		      rep_listint(ilist,&nb,ch+id+ii);
		      cree_liste_noeuds_periorc(maillnodebord,maillnodes,type, 
					      ilist,nb,&nn1,&nn2,&np1,&np2);
		      pp_cree_paires_perio(maillnodes,nn1,nn2,np1,np2, 
					   it,dlist[0],dlist[1],dlist[2],
					   zero,zero,zero,zero,
					   &nbpaires,listpairesperio,&pp);
		      free(np1); free(np2);
		    }
		  else if (!strcmp(motcle,"R")) 
		    {
		      it=1;
		      rep_ndbl(7,dlist,&ii,ch+id);
		      rep_listint(ilist,&nb,ch+id+ii);
		      cree_liste_noeuds_periorc(maillnodebord,maillnodes,type, 
					      ilist,nb,&nn1,&nn2,&np1,&np2);
		      pp_cree_paires_perio(maillnodes,nn1,nn2,np1,np2,
					   it,dlist[0],dlist[1],dlist[2],
					   dlist[3],dlist[4],dlist[5],dlist[6],
					   &nbpaires,listpairesperio,&pp);
		      free(np1); free(np2);
		    }
		  else
		    {
		      printf(" !!! ERREUR lors de la lecture des conditions aux limites\n");
		      printf("            Le mot-cle suivant n'est pas reconnu :\n");
		      printf("            --> CLIM=  PERIODICITE_3D %s\n",motcle);
		      exit(1);
		    }   
		}
	    }
	  
	}
    }

}

/*|======================================================================|
  | SYRTHES PARALLELE                                          SEPT 2003 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Creation des tables de correspondances pour la periodicite           |
  |                                                                      |
  |======================================================================|*/
void pp_contact(FILE *fdata,
		struct Maillage maillnodes,struct MaillageBord maillnodebord,
		struct NodeCouple **listpairesrc)
			    
		
{
  rp_int n,i,k,nbpaires,type;
  struct NodeCouple *pp,*qp;
  rp_int i1,i2,i3,i4,id,ii,nb,nr,it;
  rp_int *itrav;
  double zero=0.;
  rp_int nn1,nn2,*np1,*np2;

  type=TYPRESCON;

  printf("\n *** CONTACT : creation des tables de correspondance pour la ");
  printf("prise en compte des resistances de contact\n");

  /* on cree la liste des paires */
  *listpairesrc=NULL;  pp=NULL; nbpaires=0;


  fseek(fdata,0,SEEK_SET);

  while (fgets(ch,CHLONG,fdata))
    {
      if (ch[0]!='/' && strlen(ch)>1)
	{
	  extr_motcle_(motcle,ch,&i1,&i2);
	  if (!strcmp(motcle,"CLIM_T"))
	    {
	      extr_motcle(motcle,ch+i2+1,&i3,&i4);
	      id=i2+1+i4+1;
	      
              if (!strcmp(motcle,"RES_CONTACT"))
                {
                  rep_ndbl(1,dlist,&ii,ch+id);
                  rep_listint(ilist,&nb,ch+id+ii);
                  cree_liste_noeuds_periorc(maillnodebord,maillnodes,type,
                                            ilist,nb,&nn1,&nn2,&np1,&np2);
                  pp_cree_paires_rc(maillnodes,nn1,nn2,np1,np2,
				 &nbpaires,listpairesrc,&pp);

                  free(np1); free(np2);
                }
            }
	}
    }
}

/*|======================================================================|
  | SYRTHES 4.3                2008                        COPYRIGHT EDF |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | cree_liste_perio                                                     |
  |         Recherche des faces periodiques correspondantes              |
  |======================================================================| */
void cree_liste_noeuds_periorc(struct MaillageBord maillnodebord,
			       struct Maillage maillnodes,rp_int TYPE,
			       rp_int *iref,rp_int nb,
			       rp_int *nn1,rp_int *nn2,rp_int **np1,rp_int **np2)
{
  /* ici TYPE vaut TYPPERIO ou TYPRESCON */
  rp_int i,j,n,npl1,npl2,nb1,nb2,np,ng,ok;
  rp_int *numel1,*numel2;
  rp_int n1,n2,nr,l1,l2;
  rp_int *per,nn,ng1,ng2;


  /* verif qu'il existe bien un "-1" dans la liste des ref */
  for (ok=0,i=0;i<MAX_REF;i++) if (iref[i]==-1) ok=1;
  if (!ok)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n\n %%%% ERREUR cree_liste_perio : la liste des references ");
	  printf("des elements periodiques est incorrecte\n");
	  printf("                              elle doit avoir la forme suivante :\n");
	  printf("                                    liste_references_1 -1 liste_references_2\n");
	  printf("                                    exemple : 2 34 12 -1 5 32\n");
	  printf("                              --> verifier le fichier fichier de donnees\n");
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n\n %%%% ERROR cree_liste_perio : The list of references ");
	  printf("of periodic element is wrong\n");
	  printf("                              it must have the fllowing form :\n");
	  printf("                                    reference_list_1 -1 reference_list_2\n");
	  printf("                                    example : 2 34 12 -1 5 32\n");
	  printf("                              --> Please, check the data file\n");
	}
      exit(1);
    }
      

  /* creation des 2 groupes en recherchant le -1 dans la liste des references */
  nb1 = 0;
  while (iref[nb1]>0) nb1 += 1;
  nb2=nb-nb1-1;

  /* compte des elements periodiques */
  npl1=npl2=0;
  for (i=0;i<maillnodebord.nelem;i++)
    {
      l1=l2=0; nr=maillnodebord.nrefe[i];
      for (n=0;n<nb1;n++) if (nr==iref[n]) {l1=1;}
      if (!l1) for (n=nb1+1;n<nb1+1+nb2;n++) if (nr==iref[n]) {l2=1;}
      if (l1) {npl1++;}
      else if (l2) {npl2++;}
    }

  if (npl1==0 || npl2==0)
    {
      if (SYRTHES_LANG == FR)
	{
	  printf("\n\n %%%% ERREUR cree_liste_periorc : la liste des references ");
	  printf("des elements periodiques ou RC est incorrecte\n");
	  printf("                              on a trouve :\n");
	  printf("                                  %d elements periodiques sur le cote 1\n",npl1);
	  printf("                                  %d elements periodiques sur le cote 2\n",npl2);
	}
      else if (SYRTHES_LANG == EN)
	{
	  printf("\n\n %%%% ERROR cree_liste_perio : The list of references ");
	  printf("of periodic element or contact resistance is wrong\n");
	  printf("                              one has found :\n");
	  printf("                                  %d periodic elements on side 1\n",npl1);
	  printf("                                  %d periodic elements on side 2\n",npl2);
	}
      exit(1);
    }


  /* ------------------------------------------------------------- */
  /* construction de la liste des noeuds de bord periodiques ou RC */
  /* a partir de la liste des elements de bord periodiques         */
  /* --> np1[], np2[]                                              */
  /* ------------------------------------------------------------- */

  /* creation de la liste des elements de bord periodiques (a partir de nodebord) */
  numel1 = (rp_int*) malloc(npl1*sizeof(rp_int));  verif_alloue_int1d("cree_liste_noeuds_perio",numel1);
  numel2 = (rp_int*) malloc(npl2*sizeof(rp_int));  verif_alloue_int1d("cree_liste_noeuds_perio",numel2);
  perfo.mem_cond+=(npl1+npl2) * sizeof(rp_int);
  if (perfo.mem_cond>perfo.mem_max) perfo.mem_max=perfo.mem_cond;

  npl1=npl2=0;
  for (i=0;i<maillnodebord.nelem;i++)
    {
      l1=l2=0; nr=maillnodebord.nrefe[i];
      for (n=0;n<nb1;n++) if (nr==iref[n])l1=1;
      if (!l1) for (n=nb1+1;n<nb1+1+nb2;n++) if (nr==iref[n]) l2=1;
      if (l1) {numel1[npl1]=i; npl1++;}
      else if (l2) {numel2[npl2]=i; npl2++;}
    }
 
  /* a partir des elts de bord periodiques, on recherche les NOEUDS de bord periodiques */
  per=(rp_int*)malloc(maillnodes.npoin*sizeof(rp_int)); verif_alloue_int1d("cree_liste_noeuds_perio",per);
  perfo.mem_cond += maillnodes.npoin * sizeof(rp_int);
  if (perfo.mem_cond>perfo.mem_max) perfo.mem_max=perfo.mem_cond;

  for (i=0;i<maillnodes.npoin;i++) *(per+i)=-1;
  for (i=0;i<npl1;i++) 
    for (j=0;j<maillnodebord.ndmat;j++) per[maillnodebord.node[j][numel1[i]]]=1;
  for (i=0;i<npl2;i++) 
    for (j=0;j<maillnodebord.ndmat;j++) per[maillnodebord.node[j][numel2[i]]]=2;

  for (i=*nn1=*nn2=0;i<maillnodes.npoin;i++) 
    if (per[i]==1) (*nn1)++;
    else if (per[i]==2) (*nn2)++;

  free(numel1);free(numel2);
  perfo.mem_cond-=(npl1+npl2+maillnodes.npoin) * sizeof(int);




  /* creation des listes des noeuds de bord periodiques des 2 cotes */
  *np1=(rp_int*)malloc(*nn1*sizeof(rp_int));   verif_alloue_int1d("cree_liste_noeuds_perio",*np1);
  *np2=(rp_int*)malloc(*nn2*sizeof(rp_int));   verif_alloue_int1d("cree_liste_noeuds_perio",*np2);
  perfo.mem_cond += (*nn1+*nn2)*sizeof(int);
  if (perfo.mem_cond>perfo.mem_max) perfo.mem_max=perfo.mem_cond;

  for (i=l1=l2=0;i<maillnodes.npoin;i++) 
    if (per[i]==1) {(*np1)[l1]=i;l1++;}
    else if (per[i]==2) {(*np2)[l2]=i;l2++;}

  free(per);
  perfo.mem_cond-=maillnodes.npoin;
  
}

/*|======================================================================|
  | SYRTHES 4.3                                        COPYRIGHT EDF 2007|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | pp_cree_paires_perio                                                 |
  |         creation des paires de noeuds periodiques                    |
  |======================================================================| */

void pp_cree_paires_rc(struct Maillage maillnodes,
		       rp_int nn1,rp_int nn2,rp_int *np1,rp_int *np2,
		       rp_int *nbpairesrc,struct NodeCouple **listpairesrc,
		       struct NodeCouple **pp)
{
  rp_int i,j,n,npl1,npl2,nb1,nb2,np,ng,num;
  rp_int iminmin;
  rp_int n1,n2,nr,l1,l2,err=0,ok,idebp,idebe;
  rp_int nn,ng1,ng2;
  double x1,y1,z1,x2,y2,z2,x3,y3,z3,rien;
  double d1,dmin,dminmin;
  double zero=0.;
  struct NodeCouple *qp;


  /* mise en correspondance des noeuds de bord resitances de contact */
  if (maillnodes.ndim==2)
    {
      dminmin=0.; iminmin=0;
      for (i=0;i<nn1;i++)
	{
	  ng1=np1[i]; x1=maillnodes.coord[0][ng1]; y1=maillnodes.coord[1][ng1]; 
	  ok=0; dmin=1.e8;
	  for (j=0;j<nn2;j++)
	    {
	      ng2=np2[j]; 
	      if (ng2>-1)  /* le noeud n'est pas encore pris */
		{
		  x2=maillnodes.coord[0][ng2]; y2=maillnodes.coord[1][ng2]; 
		  d1=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
		  if (d1<dmin) {ok=j; dmin=d1;}
		}
	    }
	  /* on cree une nouvelle paire */
	  qp=(struct NodeCouple*)malloc(sizeof(struct NodeCouple));
	  qp->num1=ng1;  qp->num2=np2[ok];
	  qp->suivant=NULL;
	  if (!(*nbpairesrc)){*listpairesrc=*pp=qp;}
	  else {(*pp)->suivant=qp;(*pp)=(*pp)->suivant;}
	  (*nbpairesrc)++;
	  /* on "detruit" le noeud pris dans la seconde liste */
	  np2[ok]=-2;
	}
    }
  else
    {
      dminmin=0.; iminmin=0;
      for (i=0;i<nn1;i++)
	{
	  ng1=np1[i]; x1=maillnodes.coord[0][ng1]; y1=maillnodes.coord[1][ng1]; 
	  z1=maillnodes.coord[2][ng1];
	  ok=0; dmin=1.e8;
	  for (j=0;j<nn2;j++)
	    {
	      ng2=np2[j]; 
	      if (ng2>-1)  /* le noeud n'est pas encore pris */
		{
		  x2=maillnodes.coord[0][ng2]; y2=maillnodes.coord[1][ng2]; z2=maillnodes.coord[2][ng2]; 
		  d1=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2);
		  if (d1<dmin) {ok=j; dmin=d1;}
		}
	    }
	  /* on cree une nouvelle paire */
	  qp=(struct NodeCouple*)malloc(sizeof(struct NodeCouple));
	  qp->num1=ng1;  qp->num2=np2[ok];
	  qp->suivant=NULL;
	  if (!(*nbpairesrc)){*listpairesrc=*pp=qp;}
	  else {(*pp)->suivant=qp;(*pp)=(*pp)->suivant;}
	  (*nbpairesrc)++;
	  /* on "detruit" le noeud pris dans la seconde liste */
	  np2[ok]=-2;
	}
    }
      

  /* impressions de controle */
  /* printf("\n Liste des paires resistance de contact\n"); */
  /* qp=*listpairesrc; */
  /* while (qp){ */
  /*   printf("%12d  %12d\n",qp->num1,qp->num2); */
  /*   qp=qp->suivant; */
  /* } */

}  

/*|======================================================================|
  | SYRTHES 4.3                                        COPYRIGHT EDF 2007|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | pp_cree_paires_perio                                                 |
  |         creation des paires de noeuds periodiques                    |
  |======================================================================| */

void pp_cree_paires_perio(struct Maillage maillnodes,
			  rp_int nn1,rp_int nn2,rp_int *np1,rp_int *np2,
			  rp_int ntyper,
			  double tx,double ty,double tz,
			  double ax,double ay,double az,
			  double teta,
			  rp_int *nbpaires,struct NodeCouple **listpaires,
			  struct NodeCouple **pp)
{
  rp_int i,j,n,npl1,npl2,nb1,nb2,np,ng,num;
  rp_int iminmin;
  rp_int n1,n2,nr,l1,l2,err=0,ok,idebp,idebe;
  rp_int nn,ng1,ng2;
  double x1,y1,z1,x2,y2,z2,x3,y3,z3,rien;
  double x11,y11,z11,d1,dmin,dminmin;
  double zero=0.;
  struct NodeCouple *qp;


  /* mise en correspondance des noeuds de bord periodiques */
  if (maillnodes.ndim==2)
    {
      dminmin=0.; iminmin=0;
      for (i=0;i<nn1;i++)
	{
	  ng1=np1[i]; x1=maillnodes.coord[0][ng1]; y1=maillnodes.coord[1][ng1]; 
	  if (ntyper==0)        {x11=x1+tx; y11=y1+ty; z11=0;}
	  else if (ntyper==1)   rotation2d(tx,ty,teta,x1,y1,&x11,&y11);
	  else if (ntyper==2)   {num=0;util_transfo_perio(maillnodes.ndim,num,
							  x1,y1,zero,&x11,&y11,&rien);}

	  ok=0; dmin=1.e8;
	  for (j=0;j<nn2;j++)
	    {
	      ng2=np2[j]; 
	      if (ng2>-1)  /* le noeud n'est pas encore pris */
		{
		  x2=maillnodes.coord[0][ng2]; y2=maillnodes.coord[1][ng2]; 
		  d1=(x11-x2)*(x11-x2)+(y11-y2)*(y11-y2);
		  if (d1<dmin) {ok=j; dmin=d1;}
		}
	    }
	  /* on cree une nouvelle paire */
	  qp=(struct NodeCouple*)malloc(sizeof(struct NodeCouple));
	  qp->num1=ng1;  qp->num2=np2[ok];
	  qp->suivant=NULL;
	  if (!(*nbpaires)){*listpaires=*pp=qp;}
	  else {(*pp)->suivant=qp;(*pp)=(*pp)->suivant;}
	  (*nbpaires)++;
	  /* on "detruit" le noeud pris dans la seconde liste */
	  np2[ok]=-2;
	}
    }
  else
    {
      dminmin=0.; iminmin=0;
      for (i=0;i<nn1;i++)
	{
	  ng1=np1[i]; x1=maillnodes.coord[0][ng1]; y1=maillnodes.coord[1][ng1]; 
	  z1=maillnodes.coord[2][ng1];
	  if (ntyper == 0)     {x11=x1+tx; y11=y1+ty; z11=z1+tz;}
	  else if (ntyper==1)  rotation3d(tx,ty,tz,ax,ay,az,teta,x1,y1,z1,&x11,&y11,&z11);
	  else if (ntyper==2)  util_transfo_perio(maillnodes.ndim,num,
						  x1,y1,z1,&x11,&y11,&z11);
	  ok=0; dmin=1.e8;
	  for (j=0;j<nn2;j++)
	    {
	      ng2=np2[j]; 
	      if (ng2>-1)  /* le noeud n'est pas encore pris */
		{
		  x2=maillnodes.coord[0][ng2]; y2=maillnodes.coord[1][ng2]; z2=maillnodes.coord[2][ng2]; 
		  d1=(x11-x2)*(x11-x2)+(y11-y2)*(y11-y2)+(z11-z2)*(z11-z2);
		  if (d1<dmin) {ok=j; dmin=d1;}
		}
	    }
	  /* on cree une nouvelle paire */
	  qp=(struct NodeCouple*)malloc(sizeof(struct NodeCouple));
	  qp->num1=ng1;  qp->num2=np2[ok];
	  qp->suivant=NULL;
	  if (!(*nbpaires)){*listpaires=*pp=qp;}
	  else {(*pp)->suivant=qp;(*pp)=(*pp)->suivant;}
	  (*nbpaires)++;
	  /* on "detruit" le noeud pris dans la seconde liste */
	  np2[ok]=-2;
	}
    }
      
  /* impressions de controle */
  /* printf("\n Liste des paires periodiques\n"); */
  /* qp=*listpaires; */
  /* while (qp){ */
  /*   printf("%12d  %12d\n",qp->num1,qp->num2); */
  /*   qp=qp->suivant; */
  /* } */

}  

/*|======================================================================|
  | SYRTHES 3.2                MAI 97         COPYRIGHT EDF/SIMULOG 1997 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void extr_motcle(char* mc,char *ch,rp_int *i1,rp_int *i2)
{
  rp_int i=0,j=0;
  char *c;

  c=ch;
  while(!strncmp(c," ",1) && i<strlen(ch)-1) {c++;i++;}
  if (i==strlen(ch)-1) 
    {
      *i1=*i2=0;
    }
  else
    {
      j=i;
      while(strncmp(c," ",1) && j<strlen(ch)-1) {c++;j++;}
      strncpy(mc,ch+i,j-i);
      strcpy(mc+j-i,"\0");
      /*      *i1=i; *i2=j+1;*/
      *i1=i; *i2=j;
    }
  
} 
/*|======================================================================|
  | SYRTHES 3.2                MAI 97         COPYRIGHT EDF/SIMULOG 1997 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void rep_listint(rp_int *ilist,rp_int *nb,char *ch)
{
  rp_int i=0;
  char *c;
  
  *nb=0;
  c=ch;
  while(i<strlen(ch)-1)
    {
      while(!strncmp(c," ",1)) {c++;i++;}
      if (i<strlen(ch)-1)
	{
	  sscanf(c,"%d",ilist+*nb); 
	  (*nb)++;
	  while(strncmp(c," ",1)&&i<strlen(ch)-1) {c++;i++;}
	}
    }
}
/*|======================================================================|
  | SYRTHES 3.2                MAI 97         COPYRIGHT EDF/SIMULOG 1997 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  |                                                                      |
  |======================================================================| */
void rep_ndbl(rp_int nb,double *dlist,rp_int *ifin,char *ch)
{
  rp_int i=0,n=0;
  char *c;

  c=ch;
  while(n<nb)
    {
      while(!strncmp(c," ",1)) {c++;i++;}
      sscanf(c,"%lf",dlist+n);
      n++;
      while(strncmp(c," ",1)&&i<strlen(ch)) {c++;i++;}
    }
  *ifin=i;
}

/*|======================================================================|
  | SYRTHES 0.0                FEVR 96                COPYRIGHT EDF 1996 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | prot2d                                                               |
  |         Rotation d'un point en 2D                                    |
  |======================================================================| */
void rotation2d(double px,double py,double teta,
		double x,double y,double *xt,double *yt)
{
  double Pi,alfa,t[2][2];


  Pi = 3.141592653589793;  	
  alfa = teta*2*Pi/360.;

  t[0][0]=cos(alfa); t[0][1]=-sin(alfa);
  t[1][0]=-t[0][1] ; t[1][1]=t[0][0]   ;

  *xt = t[0][0]*x + t[0][1]*y + px;
  *yt = t[1][0]*x + t[1][1]*y + py;
}

/*|======================================================================|
  | SYRTHES 0.0                FEVR 96                COPYRIGHT EDF 1996 |
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | prot3d                                                               |
  |         Rotation d'un point en 3D                                    |
  |======================================================================| */
void rotation3d(double px, double py,double pz,
		double ax, double ay,double az,double teta,
		double x,  double y, double z, 
		double *xt, double *yt,double *zt)
{
  double an,phi,theta,c,s,c2,s2;
  double aa,bb,cc,dd,ee,ff,gg,hh,ii;
  double Pi,alfa,t[3][3],eps;


  Pi = 3.141592653589793;  	
  alfa = teta*Pi/180.;
  eps=1.e-6;


  if (abs(ax) >  eps)
    {
      an = sqrt( ax* ax+ ay* ay);
      phi = atan2( ay, ax); theta = atan2( az,an) ;
      c=cos(phi) ; s=sin(phi) ; c2=cos(theta) ; s2=sin(theta);
    }
  else if (abs( ay) >  eps)
    {
      an = sqrt( ax* ax+ ay* ay);
      theta=atan2(az,an); c=0.; s=1.; c2=cos(theta); s2=sin(theta);
    }
  else
    {
      c=1 ; s=0 ; c2=0 ; s2=1 ;
    }

  aa =  c2*c ;
  bb = -c2*s ;
  cc =  s2 ;
  dd =  cos(alfa)*s+sin(alfa)*s2*c ;
  ee =  cos(alfa)*c-sin(alfa)*s*s2 ;
  ff =  -sin(alfa)*c2 ;
  gg =  sin(alfa)*s-cos(alfa)*s2*c ;
  hh =  sin(alfa)*c+cos(alfa)*s*s2 ;
  ii =  cos(alfa)*c2 ;

  t[0][0] = aa*aa+s*dd-c*s2*gg;
  t[1][1] = -s*c2*bb+c*ee+s*s2*hh;
  t[2][2] = s2*cc+c2*ii;
  t[1][0] = -s*c2*aa+c*dd+s*s2*gg;
  t[0][1] = aa*bb+s*ee-c*s2*hh;
  t[2][0] = s2*aa+c2*gg;
  t[0][2] = aa*cc+s*ff-c*s2*ii;
  t[2][1] = s2*bb+c2*hh;
  t[1][2] = -s*c2*cc+c*ff+s*s2*ii;

  *xt = t[0][0]*x+t[0][1]*y+t[0][2]*z + px; 
  *yt = t[1][0]*x+t[1][1]*y+t[1][2]*z + py;  
  *zt = t[2][0]*x+t[2][1]*y+t[2][2]*z + pz;

}

