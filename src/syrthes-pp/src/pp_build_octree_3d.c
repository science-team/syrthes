/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# include "pp_usertype.h"
# include "pp_abs.h"
# include "pp_tree.h"
# include "pp_proto.h"

/*|======================================================================|
  | SYRTHES 2.1                JANV 95         COPYRIGHT EDF/SIMULOG 1995|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | build_octree                                                         |
  |         Construction de l'octree                                     |
  |======================================================================| */
void pp_build_octree_3d (struct node *arbre,rp_int npoin,rp_int nelem,
			 rp_int **node,double **coord, double *size_min,
			 double dim_boite[],rp_int nbeltmax_tree)
{
  struct child *p1;
  struct element *f1,*f2;
    
  rp_int i,nbele;
  double dx,dy,dz,dd,ddp;
  double xmin,xmax,ymin,ymax,zmin,zmax;
  
  /* calcul du cube englobant */
  xmin =  1.E10; ymin=  1.E6 ; zmin=  1.E6;
  xmax = -1.E10; ymax= -1.E6 ; zmax= -1.E6;
  
  for (i=0;i<npoin;i++)
    {
      xmin=min(coord[0][i],xmin);
      ymin=min(coord[1][i],ymin);
      zmin=min(coord[2][i],zmin);
      xmax=max(coord[0][i],xmax);
      ymax=max(coord[1][i],ymax);
      zmax=max(coord[2][i],zmax);
    }
  


  /* agrandir legerement la boite englobante */
  dx = xmax-xmin; dy=ymax-ymin; dz=zmax-zmin;
  xmin -= (dx*0.01); ymin -= (dy*0.01); zmin -= (dz*0.01);
  xmax += (dx*0.01); ymax += (dy*0.01); zmax += (dz*0.01); 
  
  
  /* stockage des min et max de la boite */
  dim_boite[0]=xmin; dim_boite[1]=xmax; 
  dim_boite[2]=ymin; dim_boite[3]=ymax; 
  dim_boite[4]=zmin; dim_boite[5]=zmax;     
  
  arbre->xc = (xmin+xmax)*0.5;
  arbre->yc = (ymin+ymax)*0.5;
  arbre->zc = (zmin+zmax)*0.5;
  arbre->sizx = dx*0.5;    
  arbre->sizy = dy*0.5;    
  arbre->sizz = dz*0.5;    
  arbre->lelement = NULL;
  arbre->lfils = NULL;
  *size_min = min(dx,dy); *size_min = min(*size_min,dz);
  
  /* mise en place de la liste des elements dans le champ 'element' */
  f1 = (struct element *)malloc(sizeof(struct element));
  f1->num = 0;
  f1->suivant=NULL;
  arbre->lelement=f1;
  
  for (i=1;i<nelem;i++)
    {
      f2 = (struct element *)malloc(sizeof(struct element));
      f2->num = i;
      f2->suivant=NULL;
      f1->suivant = f2;
      f1 = f2;
    }
  nbele = nelem;
  
  pp_decoupe3d(arbre,node,coord,nelem,npoin,nbele,size_min,nbeltmax_tree);

/*   affiche_tree(arbre,8); */

   elague_tree(arbre); 

/*   printf("\n\n Arbre apres elaguage\n"); */
/*   affiche_tree(arbre,8);  */
  
}

/*|======================================================================|
  | SYRTHES 2.1                JANV 95         COPYRIGHT EDF/SIMULOG 1995|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | decoupe3d                                                            |
  |         Construction de l'octree                                     |
  |======================================================================| */
void pp_decoupe3d(struct node *noeud,rp_int **node,double **coord,
		  rp_int nelem,rp_int npoin,rp_int nbele,double *size_min,
		  rp_int nbeltmax_tree)
{

  double xmin[8],xmax[8],ymin[8],ymax[8],zmin[8],zmax[8];
  double x,y,z,dx,dy,dz ;
  rp_int i,nbfac;
  struct node *n1,*n2,*noeudi;
  struct child *f1,*f2;
  struct element *element1;
  
  if (nbele>nbeltmax_tree)
    {
      
      /* calcul des xmin, xmax,... de chaque sous cube */
      x = noeud->xc; y = noeud->yc; z = noeud->zc; 
      dx = noeud->sizx; dy = noeud->sizy; dz = noeud->sizz;
      
      xmax[0]=xmax[3]=xmax[4]=xmax[7]= x;
      xmin[1]=xmin[2]=xmin[5]=xmin[6]= x;
      xmin[0]=xmin[3]=xmin[4]=xmin[7]= x - dx;
      xmax[1]=xmax[2]=xmax[5]=xmax[6]= x + dx;
      
      ymax[0]=ymax[1]=ymax[2]=ymax[3]= y;
      ymin[4]=ymin[5]=ymin[6]=ymin[7]= y;
      ymin[0]=ymin[1]=ymin[2]=ymin[3]= y - dy;
      ymax[4]=ymax[5]=ymax[6]=ymax[7]= y + dy;
      
      zmax[2]=zmax[3]=zmax[6]=zmax[7]= z;
      zmin[0]=zmin[1]=zmin[4]=zmin[5]= z;
      zmin[2]=zmin[3]=zmin[6]=zmin[7]= z - dz;
      zmax[0]=zmax[1]=zmax[4]=zmax[5]= z + dz;
      
      
      /* generation des fils */
      f1= (struct child *)malloc(sizeof(struct child));
      n1= (struct node *) malloc(sizeof(struct node ));

      noeud->lfils = f1;
      f1->fils = n1;
      f1->suivant = NULL;

/*       for (i=1;i<8;i++) */
/* 	  { */
/* 	      f2= (struct child *)malloc(sizeof(struct child)); */
/* 	      n2= (struct node *) malloc(sizeof(struct node )); */
/* 	      f1->suivant = f2; */
/* 	      f2->fils = n2; */
/* 	      f2->suivant = NULL; */
/* 	      f1 = f2; */
/* 	  } */

      /* remplissage des noeuds lies aux fils crees */

      f1 = noeud->lfils;

      i=0;
      while(f1 && i<8)
	{
	      noeudi = f1->fils;
	      noeudi->xc = (xmin[i]+xmax[i])*0.5;
	      noeudi->yc = (ymin[i]+ymax[i])*0.5;
	      noeudi->zc = (zmin[i]+zmax[i])*0.5;
	      noeudi->sizx = (xmax[i]-xmin[i])*0.5;
	      noeudi->sizy = (ymax[i]-ymin[i])*0.5;
	      noeudi->sizz = (zmax[i]-zmin[i])*0.5;
	      *size_min = min(*size_min,noeudi->sizx);
	      *size_min = min(*size_min,noeudi->sizy);
	      *size_min = min(*size_min,noeudi->sizz);
	      noeudi->lfils = NULL;
	      element1= (struct element *)malloc(sizeof(struct element));
	      noeudi->lelement = element1;

	      pp_tritetra(&(noeud->lelement),noeudi->lelement,
			  &nbfac,nelem,npoin,node,coord,
			  noeudi->xc,noeudi->yc,noeudi->zc,
			  noeudi->sizx,noeudi->sizy,noeudi->sizz);

              if (nbfac>nbeltmax_tree)
		pp_decoupe3d(noeudi,node,coord,nelem,npoin,nbfac,
			     size_min,nbeltmax_tree);
	      else if (nbfac==0)
		{
		  noeudi->lelement = NULL;
		  free(element1);
		}
	      
	      /* s'il reste des elts a caser, on cree le fils suivant */
	      if (noeud->lelement)
		{
		  f2= (struct child *)malloc(sizeof(struct child));
		  n2= (struct node *) malloc(sizeof(struct node ));
		  f1->suivant = f2;
		  f2->fils = n2;
		  f2->suivant = NULL;
		  f1 = f2;
		  i++;
		}
	      else
		f1=NULL;

/* 	      f1 = f1->suivant; */

	  }
		  
    }

}

/*|======================================================================|
  | SYRTHES 2.1                JANV 95         COPYRIGHT EDF/SIMULOG 1995|
  |======================================================================|
  | AUTEURS  : C. PENIGUEL, I. RUPP                                      |
  |======================================================================|
  | tritetra                                                             |
  |         Tri des tetraedres pour les placer dans l'octree             |
  |======================================================================| */
void pp_tritetra( struct element **elt_pere, struct element *elt_fils, 
		  rp_int *nbfac,rp_int nelem,rp_int npoin,rp_int **node,double **coord,
		  double xcc,double ycc,double zcc,double dx,double dy,double dz)
{

    rp_int i,n,prem ;
    double xg,yg,zg,epsi=1.E-5;
    struct element *fp1,*fpprec,*ff1,*ff2;

    prem = 1;
    fp1 = fpprec = *(elt_pere);
    ff1 = elt_fils;
    *nbfac = 0;

   /* pour chaque tetraedre de la liste en cours */
   do
    {
      /* numero des noeuds de l'element  et coordonnees */
      
      n=node[0][fp1->num];  xg =coord[0][n];  yg =coord[1][n]; zg =coord[2][n];
      n=node[1][fp1->num];  xg+=coord[0][n];  yg+=coord[1][n]; zg+=coord[2][n];
      n=node[2][fp1->num];  xg+=coord[0][n];  yg+=coord[1][n]; zg+=coord[2][n];
      n=node[3][fp1->num];  xg+=coord[0][n];  yg+=coord[1][n]; zg+=coord[2][n];

      xg*=0.25;  yg*=0.25;  zg*=0.25;

      /* si le tetraedre appartient au cube                 */
      /* on  la rajoute a la table des tetraedres du fils   */
      /* et on le detruit de la table du pere               */
      if (xg>xcc-dx-epsi && xg<xcc+dx+epsi
	  && yg>ycc-dy-epsi && yg<ycc+dy+epsi
	  && zg>zcc-dz-epsi && zg<zcc+dz+epsi)
	{
	  /* ajout au fils */
	  if (prem)
	    {
	      prem = 0;
	      ff1->num = fp1->num;
	      ff1->suivant = NULL;
	    }
	  else
	    {
	      ff2= (struct element *)malloc(sizeof(struct element));
	      ff2->num = fp1->num;
	      ff2->suivant = NULL;
	      ff1->suivant = ff2;
	      ff1 = ff2;
	    }
	  *nbfac += 1;

	  /* suppression chez le pere */
	  if (fp1 == *(elt_pere))
	    {
	      *(elt_pere)=fp1->suivant;
	      free(fp1);
	      fpprec=fp1=*(elt_pere);
	    }
	  else
	    {
	      fpprec->suivant=fp1->suivant;
	      free(fp1);
	      fp1=fpprec->suivant;
	    }
	  
	}
      else
	{
	  fpprec=fp1;
	  fp1 = fp1->suivant;
	}

    }while (fp1);
   
   
}



