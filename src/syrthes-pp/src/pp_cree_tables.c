/*-----------------------------------------------------------------------

                         SYRTHES version 4.3
                         -------------------

     This file is part of the SYRTHES Kernel, element of the
     thermal code SYRTHES.

     Copyright (C) 2009 EDF S.A., France

     contact: syrthes-support@edf.fr


     The SYRTHES Kernel is free software; you can redistribute it
     and/or modify it under the terms of the GNU General Public License
     as published by the Free Software Foundation; either version 2 of
     the License, or (at your option) any later version.

     The SYRTHES Kernel is distributed in the hope that it will be
     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.


     You should have received a copy of the GNU General Public License
     along with the SYRTHES Kernel; if not, write to the
     Free Software Foundation, Inc.,
     51 Franklin St, Fifth Floor,
     Boston, MA  02110-1301  USA

-----------------------------------------------------------------------*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <math.h>
# include "pp_usertype.h"
# include "pp_bd.h"
# include "pp_proto.h"

  
/*|======================================================================|
  | SYRTHES PARALLELE                                          SEPT 2003 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Creation des divers tables locales et de transferts pour la          |
  | resolution en parallele                                              |
  |                                                                      |
  |======================================================================|*/
void pp_cree_tables_parall(struct Maillage maillnodes,
			   struct MaillageBord maillnodebord,
			   rp_int npart,rp_int *numdome,rp_int *numdomebord,
			   char *nompart,
			   struct NodeCouple* listpairesrc,
			   struct NodeCouple* listpairesperio)

{
  rp_int n,i,j,k,np,ne,neg,s,ff;
  rp_int nligne,nd,ng,nb,*compt,nb1,nb2;
  rp_int *trav,*btrav;
  rp_int *ni,npa;

  rp_int *nelemloc,nelemlocx, **nbnodom;   /* nbres de noeuds */
  rp_int *nelembordloc;
  rp_int **numnog, **numeleg, **numelebordg,nelemlocmax; 
  rp_int **nfoisfront;

  rp_int *compt_gint,*compt_gfront;

  FILE *fsyrg;
  char *nomfich[50000],chnum[30]; 
  rp_int nbpartmax=50000;


  struct NodeDom **tnodedom,*p,*p2,*q,*r;
  struct NodeDom ***tcommun;
  rp_int **nbcommun;

  struct NodeCouple *pp,*qq,*qp;

  struct NodeRCP ***tcommunrc,*prc,*qrc;
  rp_int **nbcommunrc;
  rp_int rescontact=0;

  struct NodeRCP ***tcommunperio;
  rp_int **nbcommunperio;
  rp_int perio=0;

  rp_int form;
  rp_int affiche_info=0;
  rp_int nb_echange_proc;
  rp_int nb_echange_val;
  rp_int nb_echange_procn;
  rp_int nb_echange_valn;

  if (SYRTHES_LANG == FR){
    printf("\n *** CREE_TABLES_PARALL : creations des tables intermediaires pour ");
    printf("l'execution en parallele \n");}
  else if (SYRTHES_LANG == EN)
    printf("\n *** CREE_TABLES_PARALL : creation of temporary tables for parallel execution\n");
  fflush(stdout);

  /* indicateur de presence des RC */
  if (listpairesrc) rescontact=1;

  /* indicateur de presence de periodicite */
  if (listpairesperio) perio=1;


  /* impression de controle en entree */
/*   printf("\n numeros de parts des elts vol\n"); */
/*   for (i=0;i<maillnodes.nelem;i++)  */
/*     printf(" %d",numdome[i]); */
/*   printf("\n\n numeros de parts des elts de bord\n"); */
/*   for (i=0;i<maillnodebord.nelem;i++)  */
/*     printf(" %d",numdomebord[i]); */




  for (i=0;i<nbpartmax;i++) nomfich[i]=(char*)malloc(200*sizeof(char));
      
  


  /* initialisation des noms des fichiers */
  for (i=0;i<npart;i++)
    {
      strcpy(nomfich[i],nompart);
      if (!strncmp(nompart+strlen(nompart)-4,".syr",4)){
	form=1; 
	sprintf(chnum,"_%05dpart%05d.syr\0",npart,i);
	strcpy(nomfich[i]+strlen(nompart)-4,chnum);
      }
      else if (!strncmp(nompart+strlen(nompart)-5,".syrb",5)){
	form=0; 
	sprintf(chnum,"_%05dpart%05d.syrb\0",npart,i);
	strcpy(nomfich[i]+strlen(nompart)-5,chnum);
      }
      else {
	form=0; 
	sprintf(chnum,"_%05dpart%05d\0",npart,i);
	strcpy(nomfich[i],chnum);
      }
    }



  /* compte du nombre d'elements de bord par partition */
  /* ------------------------------------------------- */
  nelembordloc=(rp_int*)malloc((npart)*sizeof(rp_int));
  if (!nelembordloc) {printf(" ERROR pp_cree_tables : allocation memory error (nelembordloc)\n");exit(1);}
  for (i=0;i<npart;i++) nelembordloc[i]=0;
  for (i=0;i<maillnodebord.nelem;i++) nelembordloc[numdomebord[i]]++;
  

  /* compte du nombre d'elements volumique par partition */
  /* --------------------------------------------------- */
  nelemloc=(rp_int*)malloc((npart)*sizeof(rp_int));
  if (!nelemloc) {printf(" ERROR pp_cree_tables : allocation memory error (nelemloc)\n");exit(1);}
  for (i=0;i<npart;i++) nelemloc[i]=0;
  for (i=0;i<maillnodes.nelem;i++) nelemloc[numdome[i]]++;
  

  /* nombre max d'elements par partition */
  /* ----------------------------------- */
  nelemlocmax=0;
  for (i=0;i<npart;i++) 
    if (nelemloc[i]>nelemlocmax) nelemlocmax=nelemloc[i];
  if (SYRTHES_LANG == FR) printf("         Nombre d'elements par partition :\n");
  else if (SYRTHES_LANG == EN) printf("         Elements number per partition :\n");
  for (i=0;i<npart;i++) printf("         - Partition %d : %d\n",i,nelemloc[i]);
  if (SYRTHES_LANG == FR) printf("         Nombre maximum d'elements par partition : %d\n",nelemlocmax);
  else if (SYRTHES_LANG == EN) printf("         Maximun number of elements per partition : %d\n",nelemlocmax);
  fflush(stdout);


  /* tri des noeuds pour savoir a quelle partition ils appartiennent */
  /* --------------------------------------------------------------- */

  tnodedom=(struct NodeDom**)malloc(maillnodes.npoin*sizeof(struct NodeDom*)); /* tableau de nodedom */
  if (!tnodedom) {printf(" ERROR pp_cree_tables : allocation memory error (tnodedom)\n");exit(1);}
  for (i=0;i<maillnodes.npoin;i++) {
    tnodedom[i]=(struct NodeDom*)malloc(sizeof(struct NodeDom));
    tnodedom[i]->num=-1; tnodedom[i]->suivant=NULL;
  }

  for (i=0;i<maillnodes.nelem;i++)
    {
      ne=numdome[i]; 
      for (j=0;j<maillnodes.ndmat;j++)
	{
	  np=maillnodes.node[j][i];
	  if (tnodedom[np]->num==-1)
	    {
	      tnodedom[np]->num=ne;
	      tnodedom[np]->suivant=NULL;
	    }
	  else
	    {
	      p=tnodedom[np];
	      s=0;if (p->num==ne) s++;
	      while(p->suivant){
		p=p->suivant;
		if (p->num==ne) s++; 
	      }
	      if (s==0){
		p->suivant=(struct NodeDom*)malloc(sizeof(struct NodeDom));
		if (!p->suivant) {printf(" ERROR pp_cree_tables : allocation memory error (p->suivant)\n");exit(1);}
		p=p->suivant;
		p->num=ne;
		p->suivant=NULL;
	      }
	    }
	}
    }

  /* Impression de controle */
/*   printf(" Pour chaque noeud, liste des parts d'appartenance\n"); */
/* /\*   for (i=0;i<maillnodes.npoin;i++) *\/ */
/*     { */
/*       printf("Noeud %d --> parts ",i); */
/*       p=tnodedom[i];s=0; */
/*       while(p){ */
/* 	printf("%d ",p->num); */
/* 	p=p->suivant; */
/*       } */
/*       printf("\n"); */
/*     } */


  /* on compte combien on a de noeuds internes/frontieres par partition                     */
  /* ------------------------------------------------------------------                     */
  /*  nbnodom[i][0]=nombre de  noeuds internes de la partition i                            */
  /*  nbnodom[i][1]=nombre de  noeuds frontieres de la partition i                          */
  /*  nbnodom[i][2]=nombre de  noeuds total (nbnodom[i][0]+nbnodom[i][1]) de la partition i */
  nbnodom=(rp_int**)malloc(npart*sizeof(rp_int*)); 
  for (i=0;i<npart;i++){
    nbnodom[i]=(rp_int*)malloc(3*sizeof(rp_int));
    nbnodom[i][0]=nbnodom[i][1]=nbnodom[i][2]=0;
  }


  for (i=0;i<maillnodes.npoin;i++)
    {
      p=tnodedom[i]; 
      if (!(p->suivant))  /* le noeud n'appartient qu'a 1 seule part */
	{
	  nbnodom[p->num][0]++;
	}
      else
	{
	  nbnodom[p->num][1]++;
	  while(p->suivant){
	    p=p->suivant;
	    nbnodom[p->num][1]++;
	  }
	}
    }

  for (i=0;i<npart;i++) nbnodom[i][2]=nbnodom[i][0]+nbnodom[i][1];



  /* numeros globaux des noeuds par partition */
  numnog=(rp_int**)malloc(npart*sizeof(rp_int*));
  if (!numnog) {printf(" ERROR pp_cree_tables : allocation memory error (numnog)\n");exit(1);}
  for (i=0;i<npart;i++) 
    {
      numnog[i]=(rp_int*)malloc(nbnodom[i][2]*sizeof(rp_int));
      if (!numnog[i]) {printf(" ERROR pp_cree_tables : allocation memory error (numnog[i])\n");exit(1);}
    }

   /* nombre de frontieres auxquelles appartient un noeud frontiere */
   nfoisfront=(rp_int**)malloc(npart*sizeof(rp_int*));
   for (i=0;i<npart;i++)  {
     nfoisfront[i]=(rp_int*)malloc(nbnodom[i][1]*sizeof(rp_int));
     if (!nfoisfront[i]) {printf(" ERROR pp_cree_tables : allocation memory error (nfoisfront)\n");exit(1);}
     for (n=0;n<nbnodom[i][1];n++) nfoisfront[i][n]=0;
   }
     

   /* tableaux de travail (compteurs) */
   compt_gint=(rp_int*)malloc(npart*sizeof(rp_int));
   compt_gfront=(rp_int*)malloc(npart*sizeof(rp_int));
   if (!compt_gint || !compt_gfront) {printf(" ERROR pp_cree_tables : allocation memory error (compt_gint,compt_gfront)\n");exit(1);}
   for (i=0;i<npart;i++) 
     {
       compt_gint[i]=0;                 /* compteur demarre a 0 */
       compt_gfront[i]=nbnodom[i][0];   /* les noeuds front sont ajoutes a la suite des noeuds internes */
     }


  for (i=0;i<maillnodes.npoin;i++)
    {
      p=tnodedom[i]; 
      if (!(p->suivant))  /* le noeud n'appartient qu'a 1 seule part (noeud interne) */
	{
	  numnog[p->num][compt_gint[p->num]]=i;
	  compt_gint[p->num]++;
	}
      else                /* le noeud est sur une frontiere */
	{

	  numnog[p->num][compt_gfront[p->num]]=i;       /* ajout en fin de liste des numeraux globaux */
	  compt_gfront[p->num]++;

	  while(p->suivant){                         /* on fait la meme chose pour les autres noeuds de la liste */
	    p=p->suivant;
	    numnog[p->num][compt_gfront[p->num]]=i;
	    compt_gfront[p->num]++;
	  }
	}
    }

 /* remplissage de nfoisfront */
  for (n=0;n<npart;n++)
    for (i=0;i<nbnodom[n][1];i++)
      {
	ng=numnog[n][nbnodom[n][0]+i];
	p=tnodedom[ng];
	nb=1;
	while(p->suivant) {p=p->suivant;nb++;}
	nfoisfront[n][i]=nb;
      }
 
  

  /* impression de controle */
/*   printf("\n\n  liste des noeuds numeros globaux des noeuds de chaque part\n"); */
/*   for (n=0;n<npart;n++) */
/*     { */
/*       printf("\n --> partition %d\n",n); */
/*       for (j=0;j<nbnodom[n][2];j++) printf(" %d",numnog[n][j]); */
/*       printf("\n"); */
/*     } */




   /* ------------------------------------------------------------- */
   /* ------------------------------------------------------------- */
   /* ------------------------------------------------------------- */
   /* construction des listes de bord ordonnees en fonction des proc */
   /* ------------------------------------------------------------- */


  tcommun=(struct NodeDom***)malloc(npart*sizeof(struct NodeDom**));
  if (!tcommun) {printf(" ERROR pp_cree_tables : allocation memory error (tcommun)\n");exit(1);}

  nbcommun=(rp_int**)malloc(npart*sizeof(rp_int*));
  if (!nbcommun) {printf(" ERROR pp_cree_tables : allocation memory error (nbcommun)\n");exit(1);}
  for (i=0;i<npart;i++) 
    {
      nbcommun[i]=(rp_int*)malloc(npart*sizeof(rp_int));
      if (!nbcommun[i]) {printf(" ERROR pp_cree_tables : allocation memory error (nbcommun[i])\n");exit(1);}
      tcommun[i]=(struct NodeDom**)malloc(npart*sizeof(struct NodeDom*));
      if (!tcommun[i]) {printf(" ERROR pp_cree_tables : allocation memory error (tcommun[i])\n");exit(1);}
      for (j=0;j<npart;j++) 
	{
	  nbcommun[i][j]=0;
	  tcommun[i][j]=(struct NodeDom*)malloc(sizeof(struct NodeDom));
	  if (!tcommun[i][j]) {printf(" ERROR pp_cree_tables : allocation memory error (tcommun[i][j])\n");exit(1);}
	  tcommun[i][j]=NULL;
	}
    }

  if (SYRTHES_LANG == FR) printf("         Creation des tables de frontieres communes... \n");
  else if (SYRTHES_LANG == EN) printf("         Creation of the common boundary tables... \n");
  fflush(stdout);

  for (i=0;i<maillnodes.npoin;i++)
    {
      p=tnodedom[i]; 
      if (p->suivant)  /* le noeud appartient a plusieurs part) */
	{
	  do {
	    p2=p->suivant; /* on memorise le suivant */
	    /* on parcours le restant de la liste a partir de p2 pour avoir les couples d'appartenance */
	    do {
	      /* on cree un nouvel elt correspondant au noeud courant */
	      q=(struct NodeDom*)malloc(sizeof(struct NodeDom));
	      if (!q) {printf(" ERROR pp_cree_tables : allocation memory error (q)\n");exit(1);}
	      q->num=i; q->suivant=NULL;
	      /* on rajouter ce noeud dans la liste de chacune des part auxquelles il appartient */
	      if (!nbcommun[p->num][p2->num]) /* s'il n'y a pas encore de noeuds */
		{
		  tcommun[p->num][p2->num]=q;
		  nbcommun[p->num][p2->num]++;
		}
	      else
		{
		  r=tcommun[p->num][p2->num];   /* on rajoute en tete de liste */
		  tcommun[p->num][p2->num]=q;
		  tcommun[p->num][p2->num]->suivant=r;
		  nbcommun[p->num][p2->num]++;
		}
	      q=(struct NodeDom*)malloc(sizeof(struct NodeDom));
	      if (!q) {printf(" ERROR pp_cree_tables : allocation memory error (q)\n");exit(1);}
	      q->num=i; q->suivant=NULL;
	      if (!nbcommun[p2->num][p->num]) /* s'il n'y a pas encore de noeuds */
		{
		  tcommun[p2->num][p->num]=q;
		  nbcommun[p2->num][p->num]++;
		}
	      else
		{
		  r=tcommun[p2->num][p->num];   /* on rajoute en tete de liste */
		  tcommun[p2->num][p->num]=q;
		  tcommun[p2->num][p->num]->suivant=r;
		  nbcommun[p2->num][p->num]++;
		}
	      p2=p2->suivant;
	    }
	    while (p2);
	    p=p->suivant;
	  }
	  while (p && p->suivant);
	}
    }

 /* impressions de controle */
  /* ici, on a la liste des noeuds communs en numeros globaux */
/*   for (i=0;i<npart;i++) */
/*     for (j=0;j<npart;j++) */
/*       { */
/* 	p=tcommun[i][j]; */
/* 	printf("noeuds (glob) de %d commun a %d =",i,j); */
/* 	while (p) */
/* 	  { */
/* 	    printf(" %d",p->num); */
/* 	    p=p->suivant; */
/* 	  } */
/* 	printf("\n"); */
/*       } */


  /* resistances de contact : a quel domaine elles appartiennent RCRCRCRC */
  if (rescontact)
    pp_mise_a_jour_domaine(&listpairesrc,tnodedom);
  
  /* periodicite : a quel domaine appartiennent les noeuds periodiques PERPERPER */
  if (perio)
    pp_mise_a_jour_domaine(&listpairesperio,tnodedom);

  /* ------------------------------------------------------------------------------------- */
  /* ---      Passage aux numerotations locales                                        --- */
  /* ------------------------------------------------------------------------------------- */
  /* inversion de la table des noeuds pour passer a la num loc dans tcommun */
  /* on veut : tcommun[i][j]= noeuds de i communs avec j dans la numerotation de i */
  trav=(rp_int*)malloc(maillnodes.npoin*sizeof(rp_int));
  if (!trav) {printf(" ERROR pp_cree_tables : allocation memory error (trav)\n");exit(1);}
 for (n=0;n<npart;n++)
    {
      for (i=0;i<maillnodes.npoin;i++) trav[i]=-1;
      for (i=0;i<nbnodom[n][2];i++) trav[numnog[n][i]]=i+1; /* dans trav la numerotation demarre a 1 */
      for (i=0;i<npart;i++)
	{
	  p=tcommun[n][i];
	  while (p)
	    {
	      p->num=trav[p->num];
	      p=p->suivant;
	    }
	}

      /* passage a la num locale dans la liste des RC  RCRCRCRCRC */
      if (rescontact)
	{
	  pp=listpairesrc;
	  while(pp)
	    {
	      if (pp->dom1==n) pp->num1=trav[pp->num1];
	      if (pp->dom2==n) pp->num2=trav[pp->num2];
	      pp=pp->suivant;
	    }
	}

      /* passage a la num locale dans la liste des noeuds periodiques  PERPERPER */
      if (perio)
	{
	  pp=listpairesperio;
	  while(pp)
	    {
	      if (pp->dom1==n) pp->num1=trav[pp->num1];
	      if (pp->dom2==n) pp->num2=trav[pp->num2];
	      pp=pp->suivant;
	    }
	}
    }
 
  free(trav);
  
  /* impressions de controle */
  /* ici, on a la liste des noeuds communs en numeros locaux */
/*   for (i=0;i<npart;i++) */
/*     for (j=0;j<npart;j++) */
/*       { */
/* 	p=tcommun[i][j]; */
/* 	printf("noeuds (loc) de %d commun a %d =",i,j); */
/* 	while (p) */
/* 	  { */
/* 	    printf(" %d",p->num); */
/* 	    p=p->suivant; */
/* 	  } */
/* 	printf("\n"); */
/*       } */

  /* impressions de controle */
  /* if (rescontact) */
  /*   { */
  /*     pp=listpairesrc; */
  /*     while(pp){ */
  /* 	printf("RC %d (%d)    %d (%d)\n",pp->num1,pp->dom1,pp->num2,pp->dom2); */
  /* 	pp=pp->suivant; */
  /*     } */
  /*   }   */
  /* impressions de controle */
  /* if (perio) */
  /*   { */
  /*     pp=listpairesperio; */
  /*     while(pp){ */
  /* 	printf("PER %d (%d)    %d (%d)\n",pp->num1,pp->dom1,pp->num2,pp->dom2); */
  /* 	pp=pp->suivant; */
  /*     } */
  /*   }   */

  /* construction des listes ordonnees pour les resistances de contact */
  /* RCRCRCRCRC------------------------------------------------------- */
  if (rescontact)
    pp_cree_liste_ordo(npart,&tcommunrc,&nbcommunrc,&listpairesrc,1);
 

  /* construction des listes ordonnees pour la periodicite */
  /* PERPERPER ------------------------------------------- */
  if (perio)
    pp_cree_liste_ordo(npart,&tcommunperio,&nbcommunperio,&listpairesperio,1);


   /* construction de la liste des numeros d'elements par partition */
   /* ------------------------------------------------------------- */
   compt=(rp_int*)malloc(npart*sizeof(rp_int));
   if (!compt) {printf(" ERROR pp_cree_tables : allocation memory error (compt)\n");exit(1);}
 
   /* pour les elts volumiques */
   for (i=0;i<npart;i++) compt[i]=0;

   numeleg=(rp_int**)malloc(npart*sizeof(rp_int*));
   if (!numeleg) {printf(" ERROR pp_cree_tables : allocation memory error (numeleg)\n");exit(1);}
  for (i=0;i<npart;i++) 
    {
      numeleg[i]=(rp_int*)malloc(nelemloc[i]*sizeof(rp_int));
      if (!numeleg[i]) {printf(" ERROR pp_cree_tables : allocation memory error (numeleg[i])\n");exit(1);}
    }
   for (j=0;j<maillnodes.nelem;j++)
     {
       nd=numdome[j];
       numeleg[nd][compt[nd]]=j;
       compt[nd]++;
     }


   /* impression de la liste des elts de chaque partition */
/*    for (i=0;i<npart;i++)  */
/*      { */
/*        printf("elements de la partition %d\n",i); */
/*        for (j=0;j<nelemloc[i];j++) */
/* 	 printf(" %d",numeleg[i][j]); */
/*        printf("\n"); */
/*      } */



   /* pour les elements de bord */
   for (i=0;i<npart;i++) compt[i]=0;

   numelebordg=(rp_int**)malloc(npart*sizeof(rp_int*));
   if (!numelebordg) {printf(" ERROR pp_cree_tables : allocation memory error (numelebordg)\n");exit(1);}
   for (i=0;i<npart;i++) 
     {
       numelebordg[i]=(rp_int*)malloc(nelembordloc[i]*sizeof(rp_int));
       if (!numelebordg[i]) {printf(" ERROR pp_cree_tables : allocation memory error (numelebordg[i])\n");exit(1);}
     }
   
   for (j=0;j<maillnodebord.nelem;j++)
     {
       nd=numdomebord[j];
       numelebordg[nd][compt[nd]]=j;
       compt[nd]++;
     }

   free(compt);

   
   /* -------------------------------------------------------------------------- */
   /*                           ecriture des fichiers                            */
   /* -------------------------------------------------------------------------- */

   /* impression des informations de couplage des partitions */
   /* pour profiling uniquement                              */
   if (affiche_info)
     {
       nb_echange_proc=0;
       nb_echange_val=0;
       for (n=0;n<npart;n++)
	 {
	   nb_echange_procn=0;
	   nb_echange_valn=0;
	   for (j=0;j<npart;j++)
	     {
	       if (nbcommun[n][j]!=0)nb_echange_proc+=1;
	       if (nbcommun[n][j]!=0)nb_echange_procn+=1;
	       if (nbcommun[n][j]!=0)nb_echange_valn+=nbcommun[n][j];
	       if (nbcommun[n][j]!=0)nb_echange_val+=nbcommun[n][j];
	     }
	   printf("proc= %d  nb_proc= %d nb_val= %d \n",n,nb_echange_procn,nb_echange_valn);
	 }
       printf("\n nb_echange_proc=%d \n",nb_echange_proc);
       printf("nb_echange_val=%d \n",nb_echange_val);

       printf("\n Table pour visualisation graphique\n");
       for (n=0;n<npart;n++)
	 {
	   printf(" \n");
	   for (j=0;j<npart;j++)
	     {
	       printf(" %d %d %d\n",n,j,nbcommun[n][j]);
	     }
	 }


     }

   /* ecritures coordonnees/connectivite/ref de face */
   if (SYRTHES_LANG == FR) printf("\n ***  ECRITURE DES FICHIERS : %s --> %s \n",nomfich[0],nomfich[npart-1]);
   else if (SYRTHES_LANG == EN) printf("\n ***  WRITING FILES :  %s --> %s \n",nomfich[0],nomfich[npart-1]);
   fflush(stdout);


   trav=(rp_int*)malloc(maillnodes.npoin*sizeof(rp_int));

   for (n=0;n<npart;n++)
     {

       if (form==1)
	 {
     
	   if ((fsyrg=fopen(nomfich[n],"w")) == NULL)
	     {
	       if (SYRTHES_LANG == FR) printf("Impossible d'ouvrir le fichier numero %d de nom %s\n",n,nomfich[n]);
	       else if (SYRTHES_LANG == EN) printf("Unable to open file number %d named %d : %s\n",n,nomfich[n]);
	       exit(1) ;
	     }
	   
	   pp_ecrire_syrthes_ascii(fsyrg,n,
				   maillnodes,maillnodebord,
				   npart,
				   numnog,numeleg,numelebordg,
				   nbnodom,nelemloc,nelembordloc,
				   nfoisfront,
				   nbcommun,tnodedom,tcommun,
				   rescontact,nbcommunrc,tcommunrc,
				   perio,nbcommunperio,tcommunperio,
				   trav);
	 }
       else
	 {
	   if ((fsyrg=fopen(nomfich[n],"wb")) == NULL)
	     {
	       if (SYRTHES_LANG == FR) printf("Impossible d'ouvrir le fichier numero %d de nom %s\n",n,nomfich[n]);
	       else if (SYRTHES_LANG == EN) printf("Unable to open file number %d named %d : %s\n",n,nomfich[n]);
	       exit(1) ;
	     }
	   
	   pp_ecrire_syrthes_bin(fsyrg,n,
				 maillnodes,maillnodebord,
				 npart,
				 numnog,numeleg,numelebordg,
				 nbnodom,nelemloc,nelembordloc,
				 nfoisfront,
				 nbcommun,tnodedom,tcommun,
				 rescontact,nbcommunrc,tcommunrc,
				 perio,nbcommunperio,tcommunperio,
				 trav);
	 }


       /* fermeture du fichier courant */
       fclose(fsyrg);


     }

   
   free(trav);

}
/*|======================================================================|
  | SYRTHES PARALLELE                                          SEPT 2003 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Mise a jour des domaines d'appartenance des noeuds RC et perio       |
  |                                                                      |
  |======================================================================|*/
void pp_mise_a_jour_domaine(struct NodeCouple **listpaires,
			    struct NodeDom **tnodedom)
{
  rp_int nb1,nb2;
  struct NodeDom *p;
  struct NodeCouple *pp,*qq,*qp;

  pp=*listpaires;
  while (pp){
    /* a combien de domaines appartiennent les RC */
    p=tnodedom[pp->num1];	nb1=1;if (p->suivant){nb1++;p=p->suivant;}
    p=tnodedom[pp->num2];	nb2=1;if (p->suivant){nb2++;p=p->suivant;}
    
    pp->dom1=tnodedom[pp->num1]->num;     pp->nfois1=nb2; /* c'est le nbre de voisins du corresp */
    pp->dom2=tnodedom[pp->num2]->num;     pp->nfois2=nb1; /* c'est le nbre de voisins du corresp */ 
    
    
    /* si le noeud 1 appartient a plusieurs domaines il faut une paire de RC avec chaque domaine */
    p=tnodedom[pp->num1];
    while (p->suivant)
      {
	p=p->suivant;
	/* on cree une nouvelle paire */
	qp=(struct NodeCouple*)malloc(sizeof(struct NodeCouple));
	qp->num1=pp->num1;  qp->num2=pp->num2;
	qp->dom1=p->num;    qp->dom2=pp->dom2;
	qp->nfois1=nb2;     qp->nfois2=nb1;
	/* on rajoute la paire en tete de liste */
	qp->suivant=*listpaires;
	*listpaires=qp;
      }
    /* si le noeud 2 appartient a plusieurs domaine il faut une paire de RC avec chaque domaine */
    p=tnodedom[pp->num2];
    while (p->suivant)
      {
	p=p->suivant;
	/* on cree une nouvelle paire */
	qp=(struct NodeCouple*)malloc(sizeof(struct NodeCouple));
	qp->num1=pp->num1;  qp->num2=pp->num2;
	qp->dom1=pp->dom1;  qp->dom2=p->num;
	qp->nfois1=nb2;     qp->nfois2=nb1;
	/* on rajoute la paire en tete de liste */
	qp->suivant=*listpaires;
	*listpaires=qp;
      }
    
    pp=pp->suivant;
  }
}


/*|======================================================================|
  | SYRTHES PARALLELE                                          SEPT 2003 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Mise a jour des domaines d'appartenance des noeuds RC et perio       |
  |                                                                      |
  |======================================================================|*/
void pp_cree_liste_ordo(rp_int npart,struct NodeRCP ****tcommunx,rp_int ***nbcommunx,
			struct NodeCouple **listpaires,rp_int reciproque)

{
  rp_int i,j;
  struct NodeRCP *prc,*qrc;
  struct NodeCouple *pp,*qq;

  *tcommunx=(struct NodeRCP***)malloc(npart*sizeof(struct NodeRCP**));
  
  *nbcommunx=(rp_int**)malloc(npart*sizeof(rp_int*));
  for (i=0;i<npart;i++) 
    {
      (*nbcommunx)[i]=(rp_int*)malloc(npart*sizeof(rp_int));
      (*tcommunx)[i]=(struct NodeRCP**)malloc(npart*sizeof(struct NodeRCP*));
      for (j=0;j<npart;j++) 
	{
	  (*nbcommunx)[i][j]=0;
	  (*tcommunx)[i][j]=(struct NodeRCP*)malloc(sizeof(struct NodeRCP));
	  (*tcommunx)[i][j]=NULL;
	}
    }
  
  
  pp=*listpaires;
  while(pp)
    {
      /* on cree une nouvelle paire */
      qrc=(struct NodeRCP*)malloc(sizeof(struct NodeRCP));
      qrc->num1=pp->num1; qrc->num2=pp->num2; qrc->nfois=pp->nfois1; qrc->suivant=NULL;
      
      /* on examine la liste des paires proc_dom1/proc_dom2 */
      prc=(*tcommunx)[pp->dom1][pp->dom2];
      if (!prc)                              /* il n'y a pas encore d'elt dans la liste */
	{ (*tcommunx)[pp->dom1][pp->dom2]=qrc;
	  (*nbcommunx)[pp->dom1][pp->dom2]++;
	}
      else
	{
	  while(prc->suivant) prc=prc->suivant;
	  prc->suivant=qrc;
	  (*nbcommunx)[pp->dom1][pp->dom2]++;
	}

      if (reciproque)
	{
	  qrc=(struct NodeRCP*)malloc(sizeof(struct NodeRCP));
	  qrc->num1=pp->num2; qrc->num2=pp->num1; qrc->nfois=pp->nfois2; qrc->suivant=NULL;
	  prc=(*tcommunx)[pp->dom2][pp->dom1];
	  if (!prc)                              /* il n'y a pas encore d'elt dans la liste */
	    { (*tcommunx)[pp->dom2][pp->dom1]=qrc;
	      (*nbcommunx)[pp->dom2][pp->dom1]++;
	    }
	  else
	    {
	      while(prc->suivant) prc=prc->suivant;
	      prc->suivant=qrc;
	      (*nbcommunx)[pp->dom2][pp->dom1]++;
	    }
	}
      
      qq=pp;
      pp=pp->suivant;
      free(qq);  /* on detruit listpaires au fur et a mesure */
    }

 
  /* impression de controle */
  /* for (i=0;i<npart;i++) */
  /*   for (j=0;j<npart;j++) */
  /*     { */
  /* 	printf("paires partition %d %d :",i,j); */
  /* 	qrc=(*tcommunx)[i][j]; */
  /* 	while (qrc) */
  /* 	  { */
  /* 	    printf(" (%d,%d)",qrc->num1,qrc->num2); */
  /* 	    qrc=qrc->suivant; */
  /* 	  } */
  /* 	printf("\n"); */
  /*     } */
}



/*|======================================================================|
  | SYRTHES PARALLELE                                          SEPT 2003 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Ecriture du fichier geometrique d'une partition pour Syrthes         |
  |                                                                      |
  |======================================================================|*/
void pp_ecrire_syrthes_ascii(FILE *fsyrg,rp_int n,
			     struct Maillage maillnodes,struct MaillageBord maillnodebord,
			     rp_int npart,
			     rp_int **numnog,rp_int **numeleg,rp_int **numelebordg,
			     rp_int **nbnodom,rp_int *nelemloc,rp_int *nelembordloc,
			     rp_int **nfoisfront,
			     rp_int **nbcommun, struct NodeDom **tnodedom,struct NodeDom ***tcommun,
			     rp_int rescontact, rp_int **nbcommunrc,struct NodeRCP ***tcommunrc,
			     rp_int perio, rp_int **nbcommunperio,struct NodeRCP ***tcommunperio,
			     rp_int *trav)
{

  rp_int i,j,k,np,ne,neg;
  rp_int nligne,ng,nb,*compt;
  rp_int *btrav;
  double z=0;

  struct NodeDom *p,*p2,*q,*r;

  struct NodeRCP *prc;

  fprintf(fsyrg,"C*V4.0*******************************************C\n");
  fprintf(fsyrg,"C            FICHIER GEOMETRIQUE SYRTHES         C\n");
  fprintf(fsyrg,"C************************************************C\n");
  fprintf(fsyrg,"C  DIMENSION = %1d\n",maillnodes.ndim);
  fprintf(fsyrg,"C  DIMENSION DES ELTS = %1d\n",maillnodes.ndim);
  fprintf(fsyrg,"C  NOMBRE DE NOEUDS = %12d\n",nbnodom[n][2]);
  fprintf(fsyrg,"C  NOMBRE D'ELEMENTS =%12d\n",nelemloc[n]);
  fprintf(fsyrg,"C  NOMBRE D'ELEMENTS DE BORD =%12d\n",nelembordloc[n]); 
  fprintf(fsyrg,"C  NOMBRE DE NOEUDS PAR ELEMENT = %3d\n",maillnodes.ndmat);
  fprintf(fsyrg,"C************************************************C\n");
  
  /* ecriture des noeuds */
  fprintf(fsyrg,"C\n");
  fprintf(fsyrg,"C$ RUBRIQUE = NOEUDS\n");
  fprintf(fsyrg,"C\n");
  if (maillnodes.ndim==2)
    for (i=0;i<nbnodom[n][2];i++)
      {
	ng=numnog[n][i];
	fprintf(fsyrg,"%10d%4d %14.7e %14.7e %14.7e\n",
		i+1,*(maillnodes.nref+ng),maillnodes.coord[0][ng],maillnodes.coord[1][ng],z);
      }
  else
    for (i=0;i<nbnodom[n][2];i++)
      {
	ng=numnog[n][i];
	fprintf(fsyrg,"%10d%4d %14.7e %14.7e %14.7e\n",
		i+1,*(maillnodes.nref+ng),maillnodes.coord[0][ng],
		maillnodes.coord[1][ng],maillnodes.coord[2][ng]);
	
	
      }
  
  /* preparation a l'ecriture des elements */
  /* inversion de la table des noeuds pour passer a la num loc de nodes */
  for (i=0;i<maillnodes.npoin;i++) trav[i]=-1;
  for (i=0;i<nbnodom[n][2];i++) trav[numnog[n][i]]=i+1; /* dans trav la numerotation demarre a 1 */
  
  
  
  /* ecriture des elements */
  fprintf(fsyrg,"C\nC$ RUBRIQUE = ELEMENTS\nC\n");
  
  if (maillnodes.ndim==2)
    for (i=0;i<nelemloc[n];i++)
      {
	neg=numeleg[n][i];
	fprintf(fsyrg,"%12d%4d%10d%10d%10d\n",i+1,*(maillnodes.nrefe+neg),
		trav[maillnodes.node[0][neg]],trav[maillnodes.node[1][neg]],trav[maillnodes.node[2][neg]]);
      }
  else if (maillnodes.ndim==3)
    for (i=0;i<nelemloc[n];i++)
      {
	neg=numeleg[n][i];
	fprintf(fsyrg,"%12d%4d%10d%10d%10d%10d\n",
		i+1,*(maillnodes.nrefe+neg),
		trav[maillnodes.node[0][neg]],trav[maillnodes.node[1][neg]],trav[maillnodes.node[2][neg]],
		trav[maillnodes.node[3][neg]]);
      }
  
  /* ecriture des elements  de bord (s'il y en a)*/
  if (nelembordloc[n]>0) {

    fprintf(fsyrg,"C\nC$ RUBRIQUE = ELEMENTS DE BORD\nC\n");
    
    if (maillnodes.ndim==2)
      for (i=0;i<nelembordloc[n];i++)
	{
	  neg=numelebordg[n][i];
	  fprintf(fsyrg,"%12d%4d%10d%10d\n",i+1,*(maillnodebord.nrefe+neg),
		  trav[maillnodebord.node[0][neg]],trav[maillnodebord.node[1][neg]]);
	}
    else if (maillnodes.ndim==3)
      for (i=0;i<nelembordloc[n];i++)
	{
	  neg=numelebordg[n][i];
	  fprintf(fsyrg,"%12d%4d%10d%10d%10d\n",
		  i+1,*(maillnodebord.nrefe+neg),
		  trav[maillnodebord.node[0][neg]],trav[maillnodebord.node[1][neg]],
		  trav[maillnodebord.node[2][neg]]);
	}
  }
  
  /* ecriture specifique au parallelisme */
  /* ----------------------------------- */
  fprintf(fsyrg,"C\n");
  fprintf(fsyrg,"C$ RUBRIQUE = PARALLELISME - NBNO\n");
  fprintf(fsyrg,"C\n");
  fprintf(fsyrg,"nbno_interne      =%12d\n",nbnodom[n][0]);
  fprintf(fsyrg,"nbno_frontiere    =%12d\n",nbnodom[n][1]);
  fprintf(fsyrg,"nbno_global       =%12d\n",maillnodes.npoin);
  
  fprintf(fsyrg,"C\n");
  fprintf(fsyrg,"C$ RUBRIQUE =  PARALLELISME - NFOISFRONT\n");
  fprintf(fsyrg,"C\n");
  nligne=nbnodom[n][1]/12;
  for (i=0;i<nligne;i++)
    {
      for (j=0;j<12;j++) fprintf(fsyrg,"%5d",nfoisfront[n][i*12+j]);
      fprintf(fsyrg,"\n");
    }
  for (i=nligne*12;i<nbnodom[n][1];i++) fprintf(fsyrg,"%5d",nfoisfront[n][i]);
  if (nbnodom[n][1]%12 != 0) fprintf(fsyrg,"\n");
  
  
  /* ecriture des correspondances des frontieres */
  
  fprintf(fsyrg,"C\n");
  fprintf(fsyrg,"C$ RUBRIQUE = PARALLELISME  - NOMBRE DE NOEUDS COMMUNS AVEC LES AUTRES PARTITIONS\n");
  fprintf(fsyrg,"C\n");
  nligne=npart/6;
  for (i=0;i<nligne;i++)
    {
      for (j=0;j<6;j++) fprintf(fsyrg,"%10d",nbcommun[n][i*6+j]);
      fprintf(fsyrg,"\n");
    }
  for (i=nligne*6;i<npart;i++) fprintf(fsyrg,"%10d",nbcommun[n][i]);
  if (npart%6 != 0) fprintf(fsyrg,"\n");
  
  fprintf(fsyrg,"C\n");
  fprintf(fsyrg,"C$ RUBRIQUE = PARALLELISME  - NOEUDS COMMUNS AVEC LES AUTRES PARTITIONS\n");
  fprintf(fsyrg,"C\n");
  for (nb=0,i=0;i<npart;i++) nb+=nbcommun[n][i];
  btrav=(rp_int*)malloc(nb*sizeof(rp_int));
  for (j=0,i=0;i<npart;i++)
    {
      p=tcommun[n][i];
      while (p)
	{
	  btrav[j]=p->num;
	  j++;
	  p=p->suivant;
	}
    }
  nligne=nb/6;
  for (i=0;i<nligne;i++)
    {
      for (j=0;j<6;j++) fprintf(fsyrg,"%10d",btrav[i*6+j]);
      fprintf(fsyrg,"\n");
    }
  for (i=nligne*6;i<nb;i++) fprintf(fsyrg,"%10d",btrav[i]);
  if (nb%6 != 0) fprintf(fsyrg,"\n");
  free(btrav);
  
  
  /* ecriture des numeros globaux des noeuds */
  fprintf(fsyrg,"C\n");
  fprintf(fsyrg,"C$ RUBRIQUE = NUMEROS GLOBAUX DES NOEUDS\n");
  fprintf(fsyrg,"C\n");
  nligne=nbnodom[n][2]/6;
  for (i=0;i<nligne;i++)
    {
      for (j=0;j<6;j++) fprintf(fsyrg,"%10d",numnog[n][i*6+j]+1);
      fprintf(fsyrg,"\n");
    }
  for (i=nligne*6;i<nbnodom[n][2];i++) fprintf(fsyrg,"%10d",numnog[n][i]+1);
  if (nbnodom[n][2]%6 != 0) fprintf(fsyrg,"\n");
  
  
  /* Cas des resistances de contact */
  /* ------------------------------ */
  if (rescontact)
    {
      fprintf(fsyrg,"C\n");
      fprintf(fsyrg,"C$ RUBRIQUE = PARALLELISME  - RESISTANCES DE CONTACT - NBNO AVEC AUTRES PARTITIONS\n");
      fprintf(fsyrg,"C\n");
      nligne=npart/6;
      for (i=0;i<nligne;i++)
	{
	  for (j=0;j<6;j++) fprintf(fsyrg,"%10d",nbcommunrc[n][i*6+j]);
	  fprintf(fsyrg,"\n");
	}
      for (i=nligne*6;i<npart;i++) fprintf(fsyrg,"%10d",nbcommunrc[n][i]);
      if (npart%6 != 0) fprintf(fsyrg,"\n");
      
      fprintf(fsyrg,"C\n");
      fprintf(fsyrg,"C$ RUBRIQUE = PARALLELISME  - RESISTANCES DE CONTACT\n");
      fprintf(fsyrg,"C\n");
      for (nb=0,i=0;i<npart;i++) nb+=nbcommunrc[n][i];
      btrav=(rp_int*)malloc(nb*3*sizeof(rp_int));
      for (j=0,i=0;i<npart;i++)
	{
	  prc=tcommunrc[n][i];
	  while (prc)
	    {
	      btrav[j]=prc->num1; btrav[j+1]=prc->num2; btrav[j+2]=prc->nfois;
	      j+=3;
	      prc=prc->suivant;
	    }
	}
      nligne=nb/2;
      for (i=0;i<nligne;i++)
	{
	  for (j=0;j<6;j+=3) fprintf(fsyrg,"%10d%10d%10d",btrav[i*6+j],btrav[i*6+j+1],btrav[i*6+j+2]);
	  fprintf(fsyrg,"\n");
	}
      for (i=nligne*6;i<nb*3;i+=3) fprintf(fsyrg,"%10d%10d%10d",btrav[i],btrav[i+1],btrav[i+2]);
      if (nb%2 != 0) fprintf(fsyrg,"\n");
      free(btrav);
    }


  
  /* Cas de la periodicite */
  /* --------------------- */
  if (perio)
    {
      fprintf(fsyrg,"C\n");
      fprintf(fsyrg,"C$ RUBRIQUE = PARALLELISME  - PERIODICITE - NBNO AVEC AUTRES PARTITIONS\n");
      fprintf(fsyrg,"C\n");
      nligne=npart/6;
      for (i=0;i<nligne;i++)
	{
	  for (j=0;j<6;j++) fprintf(fsyrg,"%10d",nbcommunperio[n][i*6+j]);
	  fprintf(fsyrg,"\n");
	}
      for (i=nligne*6;i<npart;i++) fprintf(fsyrg,"%10d",nbcommunperio[n][i]);
      if (npart%6 != 0) fprintf(fsyrg,"\n");
      
      fprintf(fsyrg,"C\n");
      fprintf(fsyrg,"C$ RUBRIQUE = PARALLELISME  - PERIODICITE\n");
      fprintf(fsyrg,"C\n");
      for (nb=0,i=0;i<npart;i++) nb+=nbcommunperio[n][i];
      btrav=(rp_int*)malloc(nb*3*sizeof(rp_int));
      for (j=0,i=0;i<npart;i++)
	{
	  prc=tcommunperio[n][i];
	  while (prc)
	    {
	      btrav[j]=prc->num1; btrav[j+1]=prc->num2; btrav[j+2]=prc->nfois;
	      j+=3;
	      prc=prc->suivant;
	    }
	}
      nligne=nb/2;
      for (i=0;i<nligne;i++)
	{
	  for (j=0;j<6;j+=3) fprintf(fsyrg,"%10d%10d%10d",btrav[i*6+j],btrav[i*6+j+1],btrav[i*6+j+2]);
	  fprintf(fsyrg,"\n");
	}
      for (i=nligne*6;i<nb*3;i+=3) fprintf(fsyrg,"%10d%10d%10d",btrav[i],btrav[i+1],btrav[i+2]);
      if (nb%2 != 0) fprintf(fsyrg,"\n");
      free(btrav);
    }
}


/*|======================================================================|
  | SYRTHES PARALLELE                                          SEPT 2003 |
  |======================================================================|
  | AUTEURS  : I. RUPP                                                   |
  |======================================================================|
  | Ecriture du fichier geometrique d'une partition pour Syrthes         |
  |                                                                      |
  |======================================================================|*/
void pp_ecrire_syrthes_bin(FILE *fsyrg,rp_int n,
			   struct Maillage maillnodes,struct MaillageBord maillnodebord,
			   rp_int npart,
			   rp_int **numnog,rp_int **numeleg,rp_int **numelebordg,
			   rp_int **nbnodom,rp_int *nelemloc,rp_int *nelembordloc,
			   rp_int **nfoisfront,
			   rp_int **nbcommun, struct NodeDom **tnodedom,struct NodeDom ***tcommun,
			   rp_int rescontact, rp_int **nbcommunrc,struct NodeRCP ***tcommunrc,
			   rp_int perio, rp_int **nbcommunperio,struct NodeRCP ***tcommunperio,
			   rp_int *trav)
{

  rp_int i,j,k,np,ne,neg;
  rp_int nligne,ng,nb,*compt;
  rp_int *btrav,itrav[100];
  double z=0,*coo;

  struct NodeDom *p,*p2,*q,*r;

  struct NodeRCP *prc;

  fwrite(&(maillnodes.ndim),sizeof(rp_int),1,fsyrg);
  fwrite(&(maillnodes.ndim),sizeof(rp_int),1,fsyrg);
  fwrite(&(nbnodom[n][2]),sizeof(rp_int),1,fsyrg);
  fwrite(&(nelemloc[n]),sizeof(rp_int),1,fsyrg);
  fwrite(&(nelembordloc[n]),sizeof(rp_int),1,fsyrg);
  fwrite(&(maillnodes.ndmat),sizeof(rp_int),1,fsyrg);

  
  /* ecriture des references des noeuds */
  btrav=(rp_int*)malloc(nbnodom[n][2]*sizeof(rp_int));
  for (i=0;i<nbnodom[n][2];i++) btrav[i]=maillnodes.nref[numnog[n][i]];
  fwrite(btrav,sizeof(rp_int),nbnodom[n][2],fsyrg);
  free(btrav);


  /* ecriture des coordonnees des noeuds */
  coo=(double*)malloc(nbnodom[n][2]*sizeof(double));
  for (j=0;j<maillnodes.ndim;j++)
    {
      for (i=0;i<nbnodom[n][2];i++) coo[i]=maillnodes.coord[j][numnog[n][i]];
      fwrite(coo,sizeof(double),nbnodom[n][2],fsyrg);
    }
  free(coo);


  
  /* preparation a l'ecriture des elements */
  /* inversion de la table des noeuds pour passer a la num loc de nodes */
  for (i=0;i<maillnodes.npoin;i++) trav[i]=-1;
  for (i=0;i<nbnodom[n][2];i++) trav[numnog[n][i]]=i+1; /* dans trav la numerotation demarre a 1 */
  
  
  /* references des elements et connectivite */
  btrav=(rp_int*)malloc(nelemloc[n]*sizeof(rp_int));

  for (i=0;i<nelemloc[n];i++) btrav[i]=maillnodes.nrefe[numeleg[n][i]];
  fwrite(btrav,sizeof(rp_int),nelemloc[n],fsyrg);
  
  for (j=0;j<maillnodes.ndmat;j++)
    {
      for (i=0;i<nelemloc[n];i++) btrav[i]=trav[maillnodes.node[j][numeleg[n][i]]];
      fwrite(btrav,sizeof(rp_int),nelemloc[n],fsyrg);
    }
  
  
  /* references des elements et connectivite des elements  de bord */
  for (i=0;i<nelembordloc[n];i++) btrav[i]=maillnodebord.nrefe[numelebordg[n][i]];
  fwrite(btrav,sizeof(rp_int),nelembordloc[n],fsyrg);
  
  for (j=0;j<maillnodebord.ndmat;j++)
    {
      for (i=0;i<nelembordloc[n];i++) btrav[i]=trav[maillnodebord.node[j][numelebordg[n][i]]];
      fwrite(btrav,sizeof(rp_int),nelembordloc[n],fsyrg);
    }
  
  free(btrav);

  
  /* ecriture specifique au parallelisme */
  /* ----------------------------------- */
  itrav[0]=npart;
  itrav[1]=nbnodom[n][0];
  itrav[2]=nbnodom[n][1];
  itrav[3]=maillnodes.npoin;
  itrav[4]=0; if (rescontact) itrav[3]=1;
  itrav[5]=0; if (perio) itrav[4]=1;
  fwrite(itrav,sizeof(rp_int),6,fsyrg);
  
  /* NFOISFRONT */
  fwrite(nfoisfront[n],sizeof(rp_int),nbnodom[n][1],fsyrg);

  /* NOMBRE DE NOEUDS COMMUNS AVEC LES AUTRES PARTITIONS */
  fwrite(nbcommun[n],sizeof(rp_int),npart,fsyrg);
  
  
  /* NOEUDS COMMUNS AVEC LES AUTRES PARTITIONS */
  for (nb=0,i=0;i<npart;i++) nb+=nbcommun[n][i];
  btrav=(rp_int*)malloc(nb*sizeof(rp_int));
  for (j=0,i=0;i<npart;i++)
    {
      p=tcommun[n][i];
      while (p)
	{
	  btrav[j]=p->num;
	  j++;
	  p=p->suivant;
	}
    }
  if (nb>0) fwrite(btrav,sizeof(rp_int),nb,fsyrg);
  free(btrav);
  
  
  /* NUMEROS GLOBAUX DES NOEUDS */
  btrav=(rp_int*)malloc((nbnodom[n][0]+nbnodom[n][1])*sizeof(rp_int));
  for (i=0;i<nbnodom[n][0]+nbnodom[n][1];i++) btrav[i]=numnog[n][i]+1;
  fwrite(btrav,sizeof(rp_int),nbnodom[n][0]+nbnodom[n][1],fsyrg);   /* dans btrav, la numerotation part de 1 */
  free(btrav);



  /* Cas des resistances de contact */
  /* ------------------------------ */
  if (rescontact)
    {
      /* RESISTANCES DE CONTACT - NBNO AVEC AUTRES PARTITIONS */
      fwrite(nbcommunrc[n],sizeof(rp_int),npart,fsyrg);

      /* RUBRIQUE = PARALLELISME  - RESISTANCES DE CONTACT */
      for (nb=0,i=0;i<npart;i++) nb+=nbcommunrc[n][i];
      btrav=(rp_int*)malloc(nb*3*sizeof(rp_int));
      for (j=0,i=0;i<npart;i++)
	{
	  prc=tcommunrc[n][i];
	  while (prc)
	    {
	      btrav[j]=prc->num1; btrav[j+1]=prc->num2; btrav[j+2]=prc->nfois;
	      j+=3;
	      prc=prc->suivant;
	    }
	}
      fwrite(btrav,sizeof(rp_int),nb,fsyrg); 
      free(btrav);
    }


  
  /* Cas de la periodicite */
  /* --------------------- */
  if (perio)
    {
      /* RUBRIQUE = PARALLELISME  - PERIODICITE - NBNO AVEC AUTRES PARTITIONS */
      fwrite(nbcommunperio[n],sizeof(rp_int),npart,fsyrg);

      
      /* RUBRIQUE = PARALLELISME  - PERIODICITE */
      for (nb=0,i=0;i<npart;i++) nb+=nbcommunperio[n][i];
      btrav=(rp_int*)malloc(nb*3*sizeof(rp_int));
      for (j=0,i=0;i<npart;i++)
	{
	  prc=tcommunperio[n][i];
	  while (prc)
	    {
	      btrav[j]=prc->num1; btrav[j+1]=prc->num2; btrav[j+2]=prc->nfois;
	      j+=3;
	      prc=prc->suivant;
	    }
	}
      fwrite(btrav,sizeof(rp_int),nb,fsyrg); 
      free(btrav);
    }
}


