#ifndef _PPBD_H
#define _PPBD_H

#include "pp_usertype.h"

#define TYPRESCON   100
#define TYPPERIO 2000000


  struct NodeDom{
    rp_int num;
    struct NodeDom *suivant;
  };


  struct NodeRCP{
    rp_int num1,num2,nfois;
    struct NodeRCP *suivant;
  };

  struct NodeCouple{
    rp_int num1,num2;
    rp_int dom1,dom2;
    rp_int nfois1,nfois2;
    struct NodeCouple *suivant;
  };

#define MAX_REF 100

struct Maillage
{
  rp_int ndim,ndiele,nelem,npoin,ndmat,nbface;
  rp_int ncoema,iaxisy;
  double **coord,*volume,**xnf;
  rp_int **node;
  rp_int *nref,*nrefe,*type;
  rp_int **nvoisin; /* seulement pour les mst */
  rp_int nbarete,**arete, **eltarete;   /* seulement pour maillnodes */
};


struct MaillageBord
{
  rp_int ndim,ndiele,nelem,ndmat;
  double *volume;
  rp_int **node;
  rp_int *nrefe,**type;
};

struct Cperio
{
  rp_int existglob;
  rp_int nelem,ndmat,npoin;
  rp_int *nump,*numc;
  double *trav;
};

struct Performances
{
  double mem_max,mem_cond;
  double cpu_deb;
};


#endif
