#ifndef _USERTYPE_H
#define _USERTYPE_H

#define PP_TYPEWIDTH 32

#define SYRTHES_LANG 1


/* ------------------------------------------- */

#define FR 0
#define EN 1

/* ------------------------------------------- */


#include <stdint.h>

#if PP_TYPEWIDTH == 32
  typedef int32_t rp_int;
#elif PP_TYPEWIDTH == 64
  typedef int64_t rp_int;
#else
  #error "Incorrect user-supplied value fo PP_TYPEWIDTH"
#endif


#endif

