#include "pp_usertype.h"
#include "pp_tree.h"
#include "pp_bd.h"

void rep_listint(rp_int*,rp_int*,char*);
void rep_ndbl(rp_int,double*,rp_int*,char*);
void rotation2d(double,double,double,double,double,double*,double*);
void rotation3d(double,double,double,double,double,double,double,
		double,double,double,double*,double*,double*);

void extr_motcle_(char*,char*,rp_int*,rp_int*);
void extr_motcle(char*,char*,rp_int*,rp_int*);
void pp_metis2(struct Maillage,rp_int,rp_int*);
void pp_gateau(rp_int,rp_int,rp_int*);
void pp_decoupbord(struct Maillage,struct MaillageBord,rp_int*,rp_int*);
rp_int pp_egalele2d(rp_int,rp_int,rp_int,rp_int);
rp_int pp_egalele3d(rp_int,rp_int,rp_int,rp_int,rp_int,rp_int);
void pp_cree_tables_parall(struct Maillage,struct MaillageBord,rp_int,rp_int*,rp_int*,char*,
			   struct NodeCouple*,struct NodeCouple*);
void pp_ecrire_ensight (struct Maillage,struct MaillageBord,rp_int,rp_int,rp_int*,rp_int*);

void pp_ecrire_ensight_vol (struct Maillage,rp_int,rp_int,rp_int*);

void pp_contact(FILE*,struct Maillage,struct MaillageBord,struct NodeCouple**);
void pp_perio(FILE*,struct Maillage,struct MaillageBord,struct NodeCouple**);

void cree_liste_noeuds_periorc(struct MaillageBord,struct Maillage,rp_int,rp_int*,
			       rp_int,rp_int*,rp_int*,rp_int**,rp_int**);
void pp_cree_paires_perio(struct Maillage,rp_int,rp_int,rp_int*,rp_int*,  
			  rp_int,double,double,double,double,double,double,double,
			  rp_int*,struct NodeCouple**,struct NodeCouple**);
void pp_cree_paires_rc(struct Maillage,rp_int,rp_int,rp_int*,rp_int*,
		       rp_int*,struct NodeCouple**,struct NodeCouple **);



void pp_ecrire_syrthes_ascii(FILE *,rp_int ,
			   struct Maillage,struct MaillageBord,
			   rp_int,
			   rp_int**,rp_int**,rp_int**,
			   rp_int**,rp_int*,rp_int*,
			   rp_int**,
			   rp_int**, struct NodeDom **,struct NodeDom***,
			   rp_int,rp_int**,struct NodeRCP***,
			   rp_int,rp_int**,struct NodeRCP***,
		           rp_int *);
void pp_ecrire_syrthes_bin(FILE *,rp_int ,
			   struct Maillage,struct MaillageBord,
			   rp_int,
			   rp_int**,rp_int**,rp_int**,
			   rp_int**,rp_int*,rp_int*,
			   rp_int**,
			   rp_int**, struct NodeDom **,struct NodeDom***,
			   rp_int,rp_int**,struct NodeRCP***,
			   rp_int,rp_int**,struct NodeRCP***,
		           rp_int *);
void pp_mise_a_jour_domaine(struct NodeCouple **, struct NodeDom **);
void pp_cree_liste_ordo(rp_int,struct NodeRCP****,rp_int***,struct NodeCouple**,rp_int);

void pp_lire_perio(FILE*,struct Maillage,struct Maillage,struct Cperio*);
void pp_exist_perio_rc(FILE*,rp_int*,rp_int*);
void lire_simail(struct Maillage*,struct MaillageBord*);
void lire_ideas(struct Maillage*,struct MaillageBord*);
void lire_syrthes(struct Maillage*,struct MaillageBord*);

void extrbord2(struct Maillage,struct MaillageBord*,rp_int**);
void extrbord3(struct Maillage,struct MaillageBord*,rp_int**);
void extrvois2(struct Maillage,rp_int**);
void extrvois3(struct Maillage,rp_int**);

void elague_tree(struct node*);

void pp_geopart(rp_int,rp_int,rp_int,double**,rp_int**,rp_int,rp_int*,rp_int*); 
void pp_attribue_part(struct node*,rp_int*,rp_int*);


void pp_tritria(struct element*,struct element*,rp_int*,rp_int,rp_int,
		rp_int**,double**,double,double,double,double);
void pp_tritetra(struct element**,struct element*,rp_int*,rp_int,rp_int,
		 rp_int**,double**,double,double,double,double,double,double);
void pp_build_quadtree_2d (struct node*,rp_int,rp_int,
			   rp_int**,double**,double*,double[],rp_int);
void pp_build_octree_3d (struct node*,rp_int,rp_int,
			 rp_int**,double**,double*,double[],rp_int);
void pp_decoupe2d(struct node*,rp_int**,double**,rp_int,rp_int,rp_int,double*,rp_int);
void pp_decoupe3d(struct node*,rp_int**,double**,rp_int,rp_int,rp_int,double*,rp_int);
void pp_lire_domaines(char*,rp_int,rp_int,rp_int*,rp_int*);


double cpusyrt();
