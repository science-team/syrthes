import os

def substitute_data(dirCase,h):
	from string import Template
	dic = {}
	
	dic['h']=h
 	f1=open(os.path.join(dirCase,'box_holes_template.syd'))
	s1=f1.read()
	f1.close()
	tpl = Template(s1)
	s2 = tpl.substitute(dic)
	f2=open(os.path.join(dirCase,'box_holes.syd'), 'w')
	f2.write(s2)
	f2.close()
	pass

