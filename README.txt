#-------------------------------------------------------------------------
#
#                         SYRTHES version 4.3.x
#                         -------------------
#
#     This file is part of the SYRTHES Kernel, element of the
#     thermal code SYRTHES.
#
#     Copyright (C) 2012 EDF S.A., France
#
#     contact: syrthes-support@edf.fr
#
#
#     The SYRTHES Kernel is free software; you can redistribute it
#     and/or modify it under the terms of the GNU General Public License
#     as published by the Free Software Foundation; either version 2 of
#     the License, or (at your option) any later version.
#
#     The SYRTHES Kernel is distributed in the hope that it will be
#     useful, but WITHOUT ANY WARRANTY; without even the implied warranty
#     of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#
#     You should have received a copy of the GNU General Public License;
#     if not, write to the
#     Free Software Foundation, Inc.,
#     51 Franklin St, Fifth Floor,
#     Boston, MA  02110-1301  USA
#
#-------------------------------------------------------------------------





SUMMERY
     INTRODUCTION
     INSTALLATION PROCEDURE FOR SYRTHES
     BEFORE USING SYRTHES	    
     TUTORIAL
     CONTACT INFORMATION
     





================================================================================================
INTRODUCTION
================================================================================================

For more information about the different modules and external libraries
necessary or compliant with SYRTHES, refer to the COMPATIBILITY file
in the Kernel distribution (syrthes-kernel)

SYRTHES is generally installed in a specific directory which is /home/syrthes
Then the current version of the code will be installed in /home/syrthes/syrthesx.y.z-n or 
/home/toto/syrthesx.y.z-n


In order to build SYRTHES you need
  - a C compiler that is ANSI compatible
  - python > 2.3
  - make
  - cmake ( > 2.8) for METIS 5.0.2 install

When installing SYRTHES, a specific directory syrthes4.0.1-n/arch is built.
    

The elements (libsyrthes_seq.a, libsyrthes_mpi.a, syrthes-pp,
syrthes-post, syrthes-ppfunc, convert2syrthes4, syrthes4ensight) should 
be compiled and copied in syrthes4.1.x/arch/$NOM_ARCH/*
(where NOM_ARCH is the architecture name Linux, IRIX64, ... or 
other user-defined name).

The others  libraries in syrthes4.1.x/extern-librairies/src, should be compiled
and will be put in syrthes4.1.x/extern-librairies/opt/arch/NOM_ARCH 
  
Other locations are possible but will need adapting the install scripts.



================================================================================================
INSTALLATION PROCEDURE FOR SYRTHES
================================================================================================


Section I   gives information on the automatic installer
Section II  gives information for manual installation
Section III gives information for using the code


I) AUTOMATIC INSTALL
====================

1) Go to the directory src/syrthes-install
2) Update the file setup.ini   (see below)
3) Type syrthes_install.py (or python syrthes_install.py)
That's all !


4) Don't forget to source the /.../syrthes4.1.x/arch/your_arch/bin/syrthes.profile file
   before using SYRTHES
5) Test your installation : 
       - go to the directory /.../syrthes4.1.x/arch/your_arch/share/syrthes/tests/3rond2d/step1
       - run the GUI : syrthes.gui
         and/or run directly the SYRTHES code : syrthes.py -n 1 -d syrthes_data.syd


Details of the installation procedure : 
---------------------------------------

The src/syrthes-install directory contains a python script for automatic
installation of SYRTHES elements and associated routines.
In most cases, it will be enough. In case of problems, switch to
section II for element by element install.
These scripts are given in the hope that they will be useful, but
WITHOUT ANY WARRANTY.

On some architectures and for some elements (MED and FVM for instance)
it is preferable if the "make" command refers to the GNU "make". Otherwise
some problems can occur (problem with libtool, need to copy the sources in
the build directory, ...)

* syrthes_install.py:
  This python script will install the different elements of SYRTHES and
  associated libraries. Due to dependencies between the different modules, the
  order of install should be the following:
  - HDF5 
  - MED 
  - MPI    
  - Metis
  - syrthesseq
  - SYRTHES tools (syrthes4ensight, convert2syrthes4, ...)
  - ppfunc
  - syrthesmpi
  - syrthes-pp
  - syrthes-post



  The following packages cannot be installed 
  - Zlib
  - Tcl/Tk/Tix
  - Python
  - libxml2


  The install script uses the "setup.ini" file to determine which library to
  install or to use. Edit the setup.ini file and check the install options.
  For each element, there are three options:

  - to install and use a library
     In this case, specify "USE=yes"  "INSTALL=yes"

  - to not use the element (for optional libraries like MPI)
     In this case, specify  "USE=no" and "INSTALL=no". The other
     elements will be installed in accordance. ("PATH" is not used)

  - to use a pre-installed library
     In this case, specify "USE=yes" and  "PATH=..." the location of the library
     example : PATH=/home/hdf5-1.8.8/arch/Linux_x86_64


     After each element has been installed, a "setup" file is created with
     the path of the libraries installed. This file is not used but can be an example
     for a next installation. 

   Before using the "syrthes_install.py" script, the C  compiler
   to be used should be specified next to the CompC  keywords.
   The nom_arch keyword refers to the architecture of the machine. Leaving it
   blank will make it automatically detected with the "uname" command.
   nom_arch should be specified if you want different implementations on the
   same architecture (for instance Linux_LAM and Linux_MPICH).

   Commonly used compilers for different architectures :
   SunOS : compC = cc -Xa

   
   IRIX64 : compC = cc -64
   

Language
--------
Language can be set to french or english for each composant of the SYRTHES package
(default is english)

#define SYRTHES_LANG 1 --> english version
#define SYRTHES_LANG 0 --> french version

files to be edited :
syrthes4.1.x/src/syrthes-post/include/pp_usertype.h 
syrthes4.1.x/src/syrthes-ppfunc/include/ppfunc_usertype.h 
syrthes4.1.x/src/syrthes-post/include/syr_usertype.h 
syrthes4.1.x/src/syrthes-post/include/post_usertype.h 


Integer size issues
-------------------

By default, all integers used in SYRTHES are of the "int" C type.
For the pre-processing and post-processing steps, you can
coerce the size of the syrthes-pp and syrthes-post  integer type to 32 or 64 bits :
--> for syrthes-pp
    edit syrthes4.1.x/src/syrthes-pp/include/pp_usertype.h 
    #define PP_TYPEWIDTH 32 or #define PP_TYPEWIDTH 64
--> for syrthes-post
    edit syrthes4.1.x/src/syrthes-post/include/post_usertype.h 
    #define POST_TYPEWIDTH 32 or #define POST_TYPEWIDTH 64

    
II) MANUAL INSTALL
==================
If the automatic install script fails, you should install each module
individually. 

For HDF5, MED, MPI, Metis, you must go in extern-librairies/src. You must 
read README or INTALL.txt file.

For everyone else, in general, all you need to do is expand the tarball,
run the provided configure script, and then run "make all install". 

For example:
shell$ gunzip -c openmpi-1.2.6.tar.gz | tar xf -
shell$ cd openmpi-1.2.6
shell$ ./configure --prefix=/usr/local
<...lots of output...>
shell$ make all install


  Installing hdf5-1.8.8
  ---------------------
  - create a "build" directory (usually extern-librairies/src/hdf5-1.8.8/build)

  - from within the build directory, run the configure command:

      ../hdf5-1.6.7/configure \
         --prefix=/home/.../syrthes4.1.x/extern-librairies/opt/hdf5-1.8.8/arch/$NOM_ARCH

  - run the "make" commands:
      make
      make install

    For further information, refer to the INSTALL file in the hdf5-1.8.8 directory.
  
  For example directory /home/toto/syrthes4.1.x on Linux_x86_64:
  shell$ cd extern-librairies/src
  shell$ tar -zxf hdf5-1.8.8.tar.gz
  shell$ mkdir hdf5-1.8.8.build
  shell$ cd hdf5-1.8.8.build
  shell$ ../hdf5-1.8.8/configure  --prefix=/home/toto/syrthes4.1.x/extern-librairies/opt/hdf5-1.8.8/arch/Linux_x86_64
  shell$ make
  shell$ make install   
           
    
  Installing med-3.0.6
  --------------------
  - create a "build" directory (usually )

  - from within the build directory, run the configure command:

      ../med-3.0.6/configure \
         --prefix=/home/.../syrthes4.1.x/extern-librairies/opt/med-3.0.6/arch/$NOM_ARCH \
         --with-hdf5=/home/.../syrthes4.1.x/extern-librairies/opt/hdf5-1.8.8/arch/$NOM_ARCH 
  - run the "make" commands:
      make
      make install

    For further information, refer to the INSTALL file in the med-3.0.6 directory. 
    
  For example directory /home/toto/syrthes4.1.x on Linux_x86_64:
  shell$ cd extern-librairies/src
  shell$ tar -zxf med-3.0.6.tar.gz
  shell$ mkdir med-3.0.6.build
  shell$ cd med-3.0.6.build
  shell$ ../med-3.0.6/configure  --prefix=/home/toto/syrthes4.1.x/extern-librairies/opt/med-3.0.6/arch/Linux_x86_64 \
         --with-hdf5=/home/toto/syrthes4.1.x/extern-librairies/opt/hdf5-1.8.8/arch/Linux_86_64
  shell$ make
  shell$ make install      
    
    
  Installing openmpi-1.4.3
  ------------------------
  - create a "build" directory (usually extern-librairies/src/openmpi-1.4.3.build)

  - from within the build directory, run the configure command:

      ../openmpi-1.4.3/configure \
         --prefix=/home/.../syrthes4.1.x/extern-librairies/opt/openmpi-1.4.3/arch/$NOM_ARCH

  - run the "make" commands:
      make
      make install

    For further information, refer to the INSTALL file in the openmpi-1.4.3 directory.
    
  For example directory /home/toto/syrthes4.1.x on Linux_x86_64:
  shell$ cd extern-librairies/src
  shell$ tar -zxf openmpi-1.4.3.tar.gz
  shell$ mkdir openmpi-1.4.3.build
  shell$ cd openmpi-1.4.3.build
  shell$ ../openmpi-1.4.3/configure  --prefix=/home/toto/syrthes4.1.x/extern-librairies/opt/openmpi-1.4.3/arch/Linux_x86_64
  shell$ make
  shell$ make install     
    
    
    
   Installing metis-5.0.2
  -------------------------

  For example directory /home/toto/syrthes4.1.x on Linux_x86_64:
  shell$ cd extern-librairies/src
  shell$ tar -zxf metis-5.0.2.tar.gz
  shell$ cd metis-5.0.2
  shell$ make config prefix=/home/toto/syrthes4.1.x/extern-librairies/opt/metis-5.0.2/arch/Linux_x86_64
  shell$ make
  shell$ make install
         

   Installing scotch_5.1.12b
  --------------------------
  For example directory /home/toto/syrthes4.1.x on Linux_x86_64:
  shell$ cd extern-librairies/src
  shell$ tar -zxf scotch_5.1.12b.tar.gz
  shell$ cd scotch_5.1.12b/src
  shell$ cp Make.inc/Makefile.inc.x86-64_pc_linux2 Makefile.inc
  shell$ make scotch 
  shell$ make install prefix=/home/toto/syrthes4.1.x/extern-librairies/opt/scotch_5.1.12b/arch/Linux_x86_64
  shell$ make
  shell$ make install
 

  Some trouble with METIS or SCOTCH install ?
  ------------------------------------------
If you have any problem with METIS or SCOTCH install :
  - For serial calculations, you don't need METIS or SCOTCH : don't install them ! (see setup.ini file)
  - For parall calculations you need METIS _OR_ SCOTCH
       -> in case of problems with METIS install, don't install it, edit the file syrthes4.1.x/src/syrthes-pp/src/Makefile
          and remove the following directives : -D_METIS_ , $(INC_METISvi zz),  $(LIBMETIS) 
       -> in case of problems with SCOTCH install, don't install it, edit the file syrthes4.1.x/src/syrthes-pp/src/Makefile
          and remove the compilation option -D_SCOTCH_ ,$(INC_SCOTCH),  $(LIBSCOTCH) 


Next steps to install SYRTHES
-----------------------------
Go to the directory where you want to perform SYRTHES calcultation.
In the directory  syrthes4.1.x/bin
  
 1. Edit the file syrthes.profile and check the following variables :
      --> the SYRTHES4_HOME path of syrthes4.1.x (SYRTHES4_HOME=/home/user/syrthes4.1.x)
      --> the NOM_ARCH variable according your own system (if not detected automatically)
      --> the LD_LIBRARY_PATH pour HDF5, MED, MPI, Metis
          
	  #HDF5
          LD_LIBRARY_PATH=/home/user/syrthes4.1.x/extern-librairies/opt/hdf5-1.8.8/arch/Linux_x86_64:$LD_LIBRARY_PATH
          #MED
          LD_LIBRARY_PATH=/home/user/syrthes4.1.x/extern-librairies/opt/med-3.0.6/arch/Linux_x86_64:$LD_LIBRARY_PATH
          #MPI
          LD_LIBRARY_PATH=/home/user/syrthes4.1.x/extern-librairies/opt/openmpi-1.4.3/arch/Linux_x86_64:$LD_LIBRARY_PATH
          #METIS
	  LD_LIBRARY_PATH=/home/user/syrthes4.1.x/extern-librairies/opt/metis-5.0.2/arch/Linux_x86_64:$LD_LIBRARY_PATH 
	  
	       
 2. source the file syrthes.profile


 3. Edit the file Makefile.in and check the compilers names and options
    In particular you may need to modify the following:
       a. The CC variable, to be the name of the ANSI C compiler
          in your system. The GNU C compiler (gcc) will do.
       b. You can use CFLAGS, FCFLAGS and LDFLAGS to provide additional 
          command line options that are required by your compilers and linker.


 4. build the libraries and utilities : type 'make install'

    make syrthesmpi    : builds the SYRTHES MPI library
    make syrthesseq    : builds the SYRTHES sequential library
    make pp            : builds the SYRTHES preprocessor syrthes-pp
	                 (only required for MPI computations)
    make ppfunc        : builds the SYRTHES user functions interpretor syrthes-ppfunc
    make post          : builds the SYRTHES postprocessor syrthes-post	  
	                 (only required for MPI computations)
    make syrthestools  : builds the SYRTHES tools
                         (syrthes4ensight, convert2syrthes4)



===============================================================================================
BEFORE USING SYRTHES
================================================================================================

Each user of SYTHES must source the syrthes.profile file before using
the code. The easiest way is to put the following lines in each of the
users ".profile" or ".bash_profile" (depending on the shell)

For example:

syrthesprofile=/home/user/syrthes4.1.x/bin/syrthes.profile
#(adjust path to your system)
if [ -f $syrthesprofile ] ; then  
  . $syrthesprofile
fi



Doing this will set the environment variables correctly.
After changing the syrthesprofile path, it is advised to logout and login,
so that there is no mix-up in the PATH variable.

For more information refer to the SYRTHES documentation,



1. make sure that the environment variable $SYRTHES_HOME is OK
   (if not, source the file syrthes4.1.x/arch/$NOM_ARCH/bin/syrthes.profile)

2. create a new case with the command : syrthes_create_case mycase

3. cd mycase

4. prepare the data :
       - create a mesh
       - launch the command  : syrthes.gui
         update the data files syrthes.data,
       - if needed, move some user files (usr/*.c) in the current directory and
         programm them
	 
5. Without syrthes.gui, launch the command  : syrthes.py -n 1 -d syrthes.data 

6. Without syrthes.gui, convert the result files for Ensight use : type 'syrthes4ensight -h'
   for help about this utility


===============================================================================================
TUTORIAL
===============================================================================================

A tutorial is provided with SYRTHES : /.../syrthes4.1.x/arch/calibre7/share/syrthes/doc/syrthes4.1_tutorial.pdf
Corresponding data files will be find in :  /.../syrthes4.1.x/arch/calibre7/share/syrthes/tests

===============================================================================================
CONTACT INFORMATION
===============================================================================================


If you have any comments, suggestions, or bug reports, please send them
to syrthes-support@edf.fr

Any bug fixes and upgrades of the SYRTHES packages is available on WWW at
URL: http://rd.edf.com/syrthes


