#!/usr/bin/make -f
# -*- makefile -*-

DPKG_EXPORT_BUILDFLAGS := yes
include /usr/share/dpkg/buildflags.mk

# Which MPI implementation (requested by libhdf5-mpi-dev)
ifneq (,$(shell ls /usr/share/mpi-default-dev/debian_defaults))
include /usr/share/mpi-default-dev/debian_defaults
INCMED+=-I/usr/include/$(ARCH_DEFAULT_MPI_IMPL)
endif

ifneq ($(wildcard /usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/$(ARCH_DEFAULT_MPI_IMPL)/libhdf5.so),)
  HDF5_DIR := /usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/$(ARCH_DEFAULT_MPI_IMPL)
else
  HDF5_DIR := /usr
endif

# Disable libsyrthes_cfd on s390x where openmpi isn't available
ifneq (s390x,$(DEB_HOST_ARCH))
  CFD := yes
else
  CFD := no
endif

ERRLOG := src/syrthes-install/ERREUR.log

SYRTHES_HOME=usr/lib/syrthes
INSTALL_DIR=$(CURDIR)/debian/syrthes

ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
	NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
endif	

%:
	dh $@ --with python3

override_dh_auto_configure:
	sed -i.orig src/syrthes-install/setup.ini \
		-e 's/@MPI@/$(ARCH_DEFAULT_MPI_IMPL)/g' \
		-e 's/@MULTIARCH@/$(DEB_HOST_MULTIARCH)/g' \
		-e 's:@HDF5_DIR@:$(HDF5_DIR):g' \
		-e 's:@CFD@:$(CFD):g'
ifneq (,$(NUMJOBS))
	sed -i src/syrthes-install/setup.ini \
		-e 's/^#specific_makej.*/specific_makej= -j$(NUMJOBS)/'
endif

override_dh_auto_build:
	cd src/syrthes-gui/src && ./Install.sh
	cd src/syrthes-install && ./syrthes_install.py
	# ERREUR.log should be empty
	if [ -s "$(ERRLOG)" ]; then \
	  cat "$(ERRLOG)" && false; \
	fi
	# Fixes to allow running tests from the build directory
	sed -Ei 's/^(SYRTHES4_HOME)=(.*)$$/\1=$${\1:-\2}/' arch/Debian/bin/syrthes.profile
	sed -Ei 's/^(BUILD_SYRTHES)=.*$$/\1=$$(SYRTHES4_HOME)/' arch/Debian/share/syrthes/Makefile.in
	sed -i 's:$(CURDIR)/arch/Debian:/usr/lib/syrthes:;s/-I -I/-I/;s/-L -L/-L/' \
		arch/Debian/bin/syrthes.profile \
		arch/Debian/share/syrthes/Makefile.in

override_dh_auto_clean:
	[ ! -f src/syrthes-install/setup.ini.orig ] || \
	  mv src/syrthes-install/setup.ini.orig src/syrthes-install/setup.ini
	rm -fr arch
	rm -f src/*.log $(ERRLOG)

override_dh_auto_install:
	mv src/syrthes-gui/src/install arch/Debian/syrthes-gui
	# Fix unwanted exec flags (not catched by dh_fixperms)
	chmod -R a-x arch/Debian/syrthes-gui/*
	chmod -R a+X arch/Debian/syrthes-gui/*
	chmod -R a-x arch/Debian/share/syrthes/tests/*
	chmod -R a+X arch/Debian/share/syrthes/tests/*
	chmod a+x arch/Debian/share/syrthes/tests/*/syrthes/syrthes.py
	chmod a-x arch/Debian/share/syrthes/*.c
	chmod a-x arch/Debian/include/*
	# Remove extra license file
	rm arch/Debian/syrthes-gui/22x22/license

define syrthes_test
	cd $(1) && \
	SYRTHES4_HOME=$(CURDIR)/arch/Debian PATH=$$PATH:$(CURDIR)/arch/Debian/bin ./syrthes.py -d *.syd | tee syrthes.trace && \
	grep -q '\*\*\*\*\* Successful execution \*\*\*\*\*' syrthes.trace

endef
# The empty line above is important

override_dh_auto_test:
	mkdir -p debian/tmp
	cp -a src/tests debian/tmp/
	$(foreach testcase,$(wildcard src/tests/*/syrthes),$(call syrthes_test,$(subst src/tests/,debian/tmp/tests/,$(testcase))))

override_dh_python3:
	dh_python3 -N syrthes-tests --shebang=/usr/bin/python3
